@ECHO OFF
SET mypath="%~dp0"
(
	ECHO ^<?xml version="1.0" encoding="utf-8"?^>
	ECHO ^<config^>
		ECHO ^<results-path^>%mypath:~1,-1%Report\allure-results^</results-path^>
		ECHO ^<write-output-to-attachment^>true^</write-output-to-attachment^>
		ECHO ^<take-screenshots-after-failed-tests^>true^</take-screenshots-after-failed-tests^>
	ECHO ^</config^>
)>%mypath:~0,-1%NUnit 2.6.3\bin\addins\config.xml"

start .\"NUnit 2.6.3\bin\nunit-console.exe" /runlist:.\demo_incident_list.txt .\Run\Knowledge.dll /work:.\Report /out:log.txt