﻿using SNow;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using Auto;

namespace ServicePortal
{
    [TestFixture]
    public class SP_approval_overide_35
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            
            System.Console.WriteLine("Finished - Request 1 Id: " + REQiD1);
            System.Console.WriteLine("Finished - Requested 1 Item Id: " + RITMiD1);
            System.Console.WriteLine("Finished - Request 2 Id: " + REQiD2);
            System.Console.WriteLine("Finished - Requested 2 Item Id: " + RITMiD2);
            System.Console.WriteLine("Finished - Request 3 Id: " + REQiD3);
            System.Console.WriteLine("Finished - Requested 3 Item Id: " + RITMiD3);
      
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        
        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snoelement ele;
       
        //------------------------------------------------------------------
        Login login;
        Home home;
        SPortal sportal;
        Itil itil;
        //------------------------------------------------------------------        
        string REQiD1, RITMiD1, REQiD2, RITMiD2, REQiD3, RITMiD3, CTaskiD;
        string descriptionItem;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                sportal = new SPortal(Base, "Portal Home");
                itil = new Itil(Base, "Itil");
                //------------------------------------------------------------------                
                REQiD1 = string.Empty;
                RITMiD1 = string.Empty;
                REQiD2 = string.Empty;
                RITMiD2 = string.Empty;
                REQiD3 = string.Empty;
                RITMiD3 = string.Empty;
                CTaskiD = string.Empty;
                descriptionItem = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer");
                
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Verify_Customer_FullName()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                snoelement ele = sportal.UserFullName();
                flag = ele.Existed;
                if (flag)
                {
                    Console.WriteLine("Run time:(" + ele.MyText.Trim() + ")");
                    if (ele.MyText.Trim().ToLower() != temp.Trim().ToLower())
                    {
                        flag = false; error = "Invalid customer name.";
                    }
                }
                else error = "Cannot get control user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_ClickOn_Catalog_Link()
        {
            try
            {
                snoelement ele = sportal.GHeaderMenu("Catalog");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Catalog");
                        if (!flag)
                            error = "Cannot open page [Catalog]";
                    }
                    else
                        error = "Error when click on menu [Catalog].";
                }
                else error = "Not found [Catalog] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Select_Catalog_Item_1()
        {
            try
            {

                string item = Base.GData("Item1");

                flag = sportal.Search_Open_CatalogItem(item);

                if (!flag)
                    error = "Cannot select catalog item.";
                else
                {
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_Verify_UserName_Field()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                lookup = sportal.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user name value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_04_Verify_Email_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Email");
                textbox = sportal.Textbox_EmailAddress();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid email value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_06_Verify_Phone_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Phone");
                textbox = sportal.Textbox_TelephoneNumber();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user phone value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox user phone.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_07_Verify_Location_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Location");
                lookup = sportal.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user location value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Input_MandatoryFields()
        {
            try
            {
                flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_OrderItem()
        {
            try
            {
                button = sportal.Button_Submit();
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_Get_Request_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Number();

                flag = ele.Existed;
                if (flag)
                {
                    REQiD1 = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_Get_Request_Item_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Item_Number();

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD1 = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_011_01_Validate_Thank_You_Message()
        //{
        //    try
        //    {
        //        ele = sportal.ThankYouMessage();
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            if (!ele.MyText.Trim().Contains("REQ"))
        //            {
        //                flag = false;
        //                flagExit = false;
        //            }
        //        }
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid message.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_Validate_Item_Stage()
        {
            try
            {
                string temp = Base.GData("Item_Stage");

                flag = sportal.TKP_Validate_ItemStage(temp);

                if (!flag)
                {
                    error = "Invalid stage or stage is not in blue color";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_ClickOn_Catalog_Link()
        {
            try
            {
                snoelement ele = sportal.GHeaderMenu("Catalog");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Catalog");
                        if (!flag)
                            error = "Cannot open page [Catalog]";
                    }
                    else
                        error = "Error when click on menu [Catalog].";
                }
                else error = "Not found [Catalog] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Select_Catalog_Item_2()
        {
            try
            {

                string item = Base.GData("Item2");

                flag = sportal.Search_Open_CatalogItem(item);

                if (!flag)
                    error = "Cannot select catalog item.";
                else
                {
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Input_MandatoryFields()
        {
            try
            {
                flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_OrderItem()
        {
            try
            {
                button = sportal.Button_Submit();
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_01_Get_Request_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Number();

                flag = ele.Existed;
                if (flag)
                {
                    REQiD2 = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_02_Get_Request_Item_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Item_Number();

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD2 = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_01_Validate_Thank_You_Message()
        {
            try
            {
                ele = sportal.ThankYouMessage();
                flag = ele.Existed;
                if (flag)
                {
                    if (!ele.MyText.Trim().Contains("REQ"))
                    {
                        flag = false;
                        flagExit = false;
                    }
                }
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_02_Validate_Item_Stage()
        {
            try
            {
                string temp = Base.GData("Item_Stage");

                flag = sportal.TKP_Validate_ItemStage(temp);

                if (!flag)
                {
                    error = "Invalid stage or stage is not in blue color";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_ClickOn_Catalog_Link()
        {
            try
            {
                snoelement ele = sportal.GHeaderMenu("Catalog");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Catalog");
                        if (!flag)
                            error = "Cannot open page [Catalog]";
                    }
                    else
                        error = "Error when click on menu [Catalog].";
                }
                else error = "Not found [Catalog] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Select_Catalog_Item_1()
        {
            try
            {

                string item = Base.GData("Item1");

                flag = sportal.Search_Open_CatalogItem(item);

                if (!flag)
                    error = "Cannot select catalog item.";
                else
                {
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_SelectCustomer_CustManager()
        {
            try
            {
                lookup = sportal.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SP_Select(Base.GData("CustManager"));
                    if (!flag)
                        error = "Cannot select user name.";
                }
                else
                    error = "Cannot get user name lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Input_MandatoryFields()
        {
            try
            {
                flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_OrderItem()
        {
            try
            {
                button = sportal.Button_Submit();
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_Get_Request_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Number();

                flag = ele.Existed;
                if (flag)
                {
                    REQiD3 = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Get_Request_Item_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Item_Number();

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD3 = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_Validate_Thank_You_Message()
        {
            try
            {
                ele = sportal.ThankYouMessage();
                flag = ele.Existed;
                if (flag)
                {
                    if (!ele.MyText.Trim().Contains("REQ"))
                    {
                        flag = false;
                        flagExit = false;
                    }
                }
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_Validate_Item_Stage()
        {
            try
            {
                string temp = Base.GData("Item_Stage");

                flag = sportal.TKP_Validate_ItemStage(temp);

                if (!flag)
                {
                    error = "Invalid stage or stage is not in blue color";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_025_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_026_ImpersonateUser_Customer_Manager()
        {
            try
            {
                string temp = Base.GData("CustManager");

                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_027_Open_RITM_1()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD1 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item 1 Id.");
                    addPara.ShowDialog();
                    RITMiD1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = itil.GlobalSearchItem(RITMiD1, true);
                if (flag)
                {
                    itil.WaitLoading();
                }
                else error = "Cannot open RITM 1";
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_028_Approve_RITM_1()
        {
            try
            {
                bool flagF = true;
                while (flagF && flag)
                {
                    flagF = itil.Search_Verify_RelatedTable_Row("Approvers","State", "Requested", "State=Requested");
                    if (flagF)
                    {
                        itil.WaitLoading();
                        flag = itil.RelatedTableOpenRecord("Approvers","State=Requested", "State");
                        if (flag)
                        {
                            itil.WaitLoading();
                            button = itil.Button_Approve();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                itil.WaitLoading();
                            }
                            else error = "Button Approve NOT existed.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_029_Open_RITM_2()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD2 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item 2 Id.");
                    addPara.ShowDialog();
                    RITMiD2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = itil.GlobalSearchItem(RITMiD2, true);
                if (flag)
                {
                    itil.WaitLoading();
                }
                else error = "Cannot open RITM 2";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_030_Reject_RITM_2()
        {
            try
            {
                flag = itil.RelatedTableOpenRecord("Approvers", "State=Requested", "State");
                if (flag)
                {
                    itil.WaitLoading();
                    textarea = itil.Textarea_Comments();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(Base.GData("CommentRITM02"));
                        if (flag)
                        {
                            button = itil.Button_Reject();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                itil.WaitLoading();
                            }
                            else error = "Button Approve NOT existed.";
                        }
                    }
                    else error = "Cannot get textarea comments.";
                    
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_031_Open_RITM_3()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD3 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item 3 Id.");
                    addPara.ShowDialog();
                    RITMiD3 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = itil.GlobalSearchItem(RITMiD3, true);
                if (flag)
                {
                    itil.WaitLoading();
                }
                else error = "Cannot open RITM 3";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Verify_No_ApprovalState_Requested_RITM_3()
        {
            try
            {

                flag = itil.Search_Verify_RelatedTable_Row("Approvers", "State", "Requested", "State=Requested", true);
                if (!flag)
                {
                    error = "Cannot open record in Approvers tab.";
                }  

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_033_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer");

                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Verify_Customer_FullName()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                snoelement ele = sportal.UserFullName();
                flag = ele.Existed;
                if (flag)
                {
                    Console.WriteLine("Run time:(" + ele.MyText.Trim() + ")");
                    if (ele.MyText.Trim().ToLower() != temp.Trim().ToLower())
                    {
                        flag = false; error = "Invalid customer name.";
                    }
                }
                else error = "Cannot get control user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_ClickOn_CheckStatus()
        {
            try
            {
                ele = sportal.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else
                {
                    error = "Not found My Requests or Check Status on dashboard menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_ClickOn_MyRequests()
        {
            try
            {
                ele = sportal.GLeftMenu("My Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else error = "Not found My Requests on left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_ClickOn_MyActiveRequests()
        {
            try
            {
                ele = sportal.GLinkByText("My Active Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag) sportal.WaitLoading();
                }
                else error = "Not found menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Search_Open_Request_Item_1()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD1 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item 1 Id.");
                    addPara.ShowDialog();
                    RITMiD1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = sportal.SearchAndOpen(RITMiD1, "Number=" + RITMiD1, "Number");
                if (flag)
                    sportal.WaitLoading();
                else error = "Cannot open request item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Verify_Approval_Approved()
        {
            try
            {
                combobox = sportal.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("Approved");
                    if (!flag)
                        error = "Invalid stage.";
                }
                else error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Verify_Stage_Fulfilment()
        {
            try
            {
                combobox = sportal.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("Fulfilment");
                    if (!flag)
                        error = "Invalid stage.";
                }
                else error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_ClickOn_CheckStatus()
        {
            try
            {
                ele = sportal.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else
                {
                    error = "Not found My Requests or Check Status on dashboard menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_ClickOn_MyRequests()
        {
            try
            {
                ele = sportal.GLeftMenu("My Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else error = "Not found My Requests on left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_ClickOn_MyPriorRequests()
        {
            try
            {
                ele = sportal.GLinkByText("My Prior Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag) sportal.WaitLoading();
                }
                else error = "Not found menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Search_Open_Request_Item_2()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD2 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item 2 Id.");
                    addPara.ShowDialog();
                    RITMiD2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = sportal.SearchAndOpen(RITMiD2, "Number=" + RITMiD2, "Number");
                if (flag)
                    sportal.WaitLoading();
                else error = "Cannot open request item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Verify_Approval_Rejected()
        {
            try
            {
                combobox = sportal.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("Rejected");
                    if (!flag)
                        error = "Invalid stage.";
                }
                else error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Verify_Stage_CloudRequestCancelled()
        {
            try
            {
                combobox = sportal.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("Cloud Request Cancelled");
                    if (!flag)
                        error = "Invalid stage.";
                }
                else error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //***********************************************************************************************************************************

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
