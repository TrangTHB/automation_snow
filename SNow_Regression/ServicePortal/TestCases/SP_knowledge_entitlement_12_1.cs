﻿using Auto;
using NUnit.Framework;
using SNow;
using System;
using System.Reflection;
using System.Threading;

namespace ServicePortal
{
    [TestFixture]
    public class SP_knowledge_entitlement_12_1
    {
        #region Define default variables for test case (No need to update)

        //***********************************************************************************************************************************
        public bool flagC;

        public bool flag, flagExit, flagW;
        private string caseName, error;
        private SNow.snobase Base;

        //***********************************************************************************************************************************

        #endregion Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)

        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }

        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }

        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }

        //***********************************************************************************************************************************

        #endregion Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)

        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Knowledge Article 01's Id: " + knowledgeId01);
            System.Console.WriteLine("Finished - Knowledge Article 02's Id: " + knowledgeId02);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }

        //***********************************************************************************************************************************

        #endregion Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        //***********************************************************************************************************************************

        //olabel label;
        private olookup lookup;

        //------------------------------------------------------------------
        private Login login = null;

        private Home home = null;
        private Itil kb_general = null;
        private ItilList kb_list_general = null;

        private Knowledge kb = null;

        private SPortal sphome = null;
        //SPortal pBrowseKnowledge = null;
        //SPortal pGlobalSearch = null;
        //SPortal pSearchResult = null;
        //SPortal pArticleDetail = null;

        //------------------------------------------------------------------
        private string knowledgeId01, knowledgeId02;

        //***********************************************************************************************************************************

        #endregion Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                //phome = new PHome(Base);
                kb = new Knowledge(Base, "Knowledge");
                kb_general = new Itil(Base, "Knowledge");
                kb_list_general = new ItilList(Base, "Knowledge List");
                sphome = new SPortal(Base, "Service Portal");
                //pBrowseKnowledge = new PBrowseKnowledge(Base, "Browse Knowledge");
                //pGlobalSearch = new PGlobalSearch(Base);
                //pSearchResult = new PSearchResult(Base, "Search Result");
                //pArticleDetail = new PArticleDetail(Base);

                //------------------------------------------------------------------
                knowledgeId01 = string.Empty;
                knowledgeId02 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                    //string temp = home.Label_UserFullName().Text;
                    //flag = Base.GData("UserFullName").Equals(temp);
                    //if (!flag)
                    //{
                    //    flagExit = false;
                    //    error = "Welcome login account is NOT correct.";
                    //}
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Knowledge Manager 01 create Knowledge Article 01 for Company A

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_Impersonate_KnowledgeManager()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("KnowledgeManager01"));
                if (!flag)
                {
                    error = "Cannot impersonate Knowledge Manager 01";
                }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_Open_NewKnowledge_Company1()
        {
            try
            {
                // Open New Knowledge page
                Console.WriteLine("*******Open New Knowledge page");
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                {
                    kb_general.WaitLoading();

                    int count = 10;
                    while (!kb_general.Textbox_Number().Existed && count > 0)
                    {
                        Thread.Sleep(1000);
                        count--;
                    }
                    flag = kb_general.Textbox_Number().Existed;
                    if (!flag)
                    {
                        error = "Cannot open New Knowledge page";
                    }
                }
                else
                {
                    error = "Error when create new Knowledge.";
                }

                // Validate default value
                Console.WriteLine("*******Validate default value");
                if (flag)
                {
                    error = string.Empty;

                    // Verify Number
                    kb_general.Textbox_Number().Click();
                    Thread.Sleep(1000);

                    /* Save the Knowlege01 Id */
                    knowledgeId01 = kb_general.Textbox_Number().Text;

                    // Verify Knowledge Base is blank
                    if (!kb_general.Lookup_Knowledge_base().Text.Trim().Equals(""))
                    {
                        error += "Default Knowledge Base is NOT blank. Its value = [" + kb_general.Lookup_Knowledge_base().Text + "]. ";
                    }

                    // Verify Category is read-only
                    if (!kb_general.Lookup_Kb_Category().ReadOnly)
                    {
                        error += "Category field is NOT read-only";
                    }

                    // Verify Category is blank
                    if (!kb_general.Lookup_Knowledge_base().Text.Trim().Equals(""))
                    {
                        error += "Default Category is NOT blank. Its value = [" + kb_general.Lookup_Kb_Category().Text + "]. ";
                    }

                    // Verify Article Type is HTML
                    if (!kb_general.Combobox_KB_Article_type().VerifyCurrentValue("HTML"))
                    {
                        error += "Default Article Type is NOT [HTML]. Its value = [" + kb_general.Combobox_KB_Article_type().Text + "]. ";
                    }

                    if (error != string.Empty) { flag = false; flagExit = false; }
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_Input_MandatoryFields()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 01's Id value.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                temp = string.Empty;
                error = string.Empty;

                #region Select Domain
                Console.WriteLine("*******Select Domain");

                flag = kb_general.Lookup_Kb_Domain().Select(Base.GData("Domain01"));
                if (!flag)
                {
                    error += "Cannot select Domain";
                }

                #endregion Select Domain

                #region Input Knowledge Base
                Console.WriteLine("*******Input Knowledge Base");

                flag = kb_general.Lookup_Knowledge_base().Select(Base.GData("KnowledgeBase01"));
                if (!flag)
                {
                    error += "Cannot select Knowledge Base";
                }

                #endregion Input Knowledge Base

                #region Input Short Description
                Console.WriteLine("*******Input Short Description");

                temp = knowledgeId01 + " - " + Base.GData("ShortDescription");
                flag = kb_general.Textbox_ShortDescription().SetText(temp);
                if (!flag)
                {
                    error += "Cannot input Short description";
                }

                #endregion Input Short Description

                #region Add assignment group
                Console.WriteLine("*******Add assignment group");

                lookup = kb_general.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assignment group value.";
                    }
                }
                else { error = "Cannot get lookup Assignment group."; }

                #endregion Add assignment group

                #region Select a Category
                Console.WriteLine("*******Select a Category");

                flag = kb_general.Lookup_Kb_Category().Select(Base.GData("Category01"));
                if (!flag)
                {
                    error += "Cannot select Category";
                }

                #endregion Select a Category

                #region Check on Display Attachment
                Console.WriteLine("*******Check on Display Attachment");

                flag = kb_general.Checkbox_KB_Display_attachments().Checked;

                if (flag)
                {
                    flag = false;
                    error += "[Display Attachment] has already CHECKED";
                }
                else
                {
                    flag = kb_general.Checkbox_KB_Display_attachments().Click();
                    if (!flag) error += "CANNOT check [Display Attachment] box";
                }

                #endregion Check on Display Attachment

                //#region Attach an Image into Text field
                //string fileName = Base.GData("Image1_Name");;
                //string tooltip = Base.GData("Image1_Tooltip");
                //string alt = Base.GData("Image1_Alt");
                //flag = kb.AttachImageIntoTextfield_NoBrowserBtn(fileName, tooltip, alt);
                ////string version = Base.GData("Version");
                ////if(version.ToLower() != "jakarta" || version == string.Empty || version == null)
                ////{
                ////    flag = kb.AttachImageIntoTextfield(fileName, tooltip, alt);
                ////}
                ////else
                ////{
                ////    flag = kb.AttachImageIntoTextfield_NoBrowserBtn(fileName, tooltip, alt);
                ////}

                //if (flag)
                //{
                //    kb.WaitLoading();
                //    temp = "alt=" + alt + ";title=" + tooltip;
                //    flag = kb.CheckAttachment("img", temp);
                //    if (!flag)
                //    {
                //        error += "Not found any image in Text field. ";
                //    }
                //}
                //else
                //{
                //    error += "Cannot attach image file. ";
                //}
                //#endregion

                #region Attach an attachment into Knowledge Article
                Console.WriteLine("*******Attach an attachment into Knowledge Article");

                flag = kb_general.Add_AttachmentFile(Base.GData("Attachment1_Name"));
                if (flag)
                {
                    kb_general.WaitLoading();
                }
                else
                {
                    error += "Cannot attach file. ";
                }

                #endregion Attach an attachment into Knowledge Article

                if (error != string.Empty) { flag = false; flagExit = false; }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_Submit_Knowledge()
        {
            try
            {
                /* Save Knowledge */
                flag = kb_general.Save();
                if (!flag)
                {
                    error = "Cannot save Knowledge";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_Publish_Knowledge()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 01's Id value.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                flag = kb.Review_KA(knowledgeId01);
                if (flag)
                {
                    flag = kb.Publish_KA(knowledgeId01);

                    if (!flag) error = "Cannot Publish the Article 01: " + knowledgeId01;
                }
                else error = "Cannot Review the Article 01: " + knowledgeId01;
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        #endregion Knowledge Manager 01 create Knowledge Article 01 for Company A

        #region Knowledge Manager 02 create Knowledge Article 02 for Company B

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_Impersonate_KnowledgeManager02()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("KnowledgeManager02"));
                if (!flag)
                {
                    error = "Cannot impersonate Knowledge Manager 01";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_Open_NewKnowledge_Company2()
        {
            try
            {
                string temp = knowledgeId01;
                Step_005_Open_NewKnowledge_Company1();
                /* return Knowledge01 ID and Save the Knowlege02 Id */
                knowledgeId01 = temp;
                knowledgeId02 = kb_general.Textbox_Number().Text;
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_Input_MandatoryFields()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 02's Id value.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                temp = string.Empty;
                error = string.Empty;

                #region Select Domain
                Console.WriteLine("*******Select Domain");

                flag = kb_general.Lookup_Kb_Domain().Select(Base.GData("Domain02"));
                if (!flag)
                {
                    error += "Cannot select Domain";
                }

                #endregion Select Domain

                #region Input Knowledge Base
                Console.WriteLine("*******Input Knowledge Base");

                flag = kb_general.Lookup_Knowledge_base().Select(Base.GData("KnowledgeBase02"));
                if (!flag)
                {
                    error += "Cannot select Knowledge Base";
                }

                #endregion Input Knowledge Base

                #region Input Short Description
                Console.WriteLine("*******Input Short Description");

                temp = knowledgeId02 + " - " + Base.GData("ShortDescription");
                flag = kb_general.Textbox_ShortDescription().SetText(temp);
                if (!flag)
                {
                    error += "Cannot input Short description";
                }

                #endregion Input Short Description

                #region Add assignment group
                Console.WriteLine("*******Add assignment group");

                lookup = kb_general.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assignment group value.";
                    }
                }
                else { error = "Cannot get lookup Assignment group."; }

                #endregion Add assignment group

                #region Select a Category
                Console.WriteLine("*******Select a Category");

                flag = kb_general.Lookup_Kb_Category().Select(Base.GData("Category02"));
                if (!flag)
                {
                    error += "Cannot select Category";
                }

                #endregion Select a Category

                #region Check on Display Attachment
                Console.WriteLine("*******Check on Display Attachment");

                flag = kb_general.Checkbox_KB_Display_attachments().Checked;

                if (flag)
                {
                    flag = false;
                    error += "[Display Attachment] has already CHECKED";
                }
                else
                {
                    flag = kb_general.Checkbox_KB_Display_attachments().Click();
                    if (!flag) error += "CANNOT check [Display Attachment] box";
                }

                #endregion Check on Display Attachment

                //#region Attach an Image into Text field
                //string fileName = Base.GData("Image2_Name"); ;
                //string tooltip = Base.GData("Image2_Tooltip");
                //string alt = Base.GData("Image2_Alt");
                //string version = Base.GData("Version");
                //if (version.ToLower() != "jakarta" || version == string.Empty || version == null)
                //{
                //    flag = kb.AttachImageIntoTextfield(fileName, tooltip, alt);
                //}
                //else
                //{
                //    flag = kb.AttachImageIntoTextfield_NoBrowserBtn(fileName, tooltip, alt);
                //}
                //if (flag)
                //{
                //    kb.WaitLoading();
                //    temp = "alt=" + alt + ";title=" + tooltip;
                //    flag = kb.CheckAttachment("img", temp);
                //    if (!flag)
                //    {
                //        error += "Not found any image in Text field. ";
                //    }
                //}
                //else
                //{
                //    error += "Cannot attach image file. ";
                //}
                //#endregion

                #region Attach an attachment into Knowledge Article
                Console.WriteLine("*******Attach an attachment into Knowledge Article");

                flag = kb_general.Add_AttachmentFile(Base.GData("Attachment2_Name"));
                if (flag)
                {
                    kb_general.WaitLoading();
                }
                else
                {
                    error += "Cannot attach file. ";
                }

                #endregion Attach an attachment into Knowledge Article

                if (error != string.Empty) { flag = false; flagExit = false; }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_Submit_Knowledge()
        {
            try
            {
                /* Save Knowledge */
                flag = kb_general.Save();
                if (!flag)
                {
                    error = "Cannot save Knowledge";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Publish_Knowledge()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 02's Id value.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                flag = kb.Review_KA(knowledgeId02);
                if (flag)
                {
                    flag = kb.Publish_KA(knowledgeId02);

                    if (!flag) error = "Cannot Publish the Article 02: " + knowledgeId02;
                }
                else error = "Cannot Review the Article 02: " + knowledgeId02;
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        #endregion Knowledge Manager 02 create Knowledge Article 02 for Company B

        #region Customer 01 can see Article 01, cannot see Article 02

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_Impersonate_Customer01()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("Customer01"), true, Base.GData("UserFullName"),true);
                if (!flag)
                {
                    error = "Cannot impersonate Customer 01";
                }
                else Thread.Sleep(1000);

            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                        flag = sphome.Verify_Page_Open("Home;Knowledge");
                        if (!flag) error = "Cannot open page [Knowledge]";
                    }
                    else
                    {
                        error = "Error when click on menu issue";
                    }
                }
                else
                {
                    error = "Cannot get menu issue.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_Search_KA_01()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Article 01's Id value.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                string searchInput = knowledgeId01 + " - " + Base.GData("ShortDescription");

                // Search with Knowledge Search
                flag = sphome.Search_KA(knowledgeId01);
                if (flag)
                {
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_Open_KA_01()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 01's Id value.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
              
                flag = sphome.Open_Item_From_Knowledge_Search_Result(knowledgeId01, true);
                
                if (flag)
                {
                    sphome.WaitLoading();
                }
                else
                {
                    error = "CANNOT open KA from list result";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_BackTo_KnowledgeSearchPage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_Search_Knowledge02()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 02's Id value.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                string searchInput = knowledgeId02;// + " - " + Base.GData("ShortDescription");

                // Search with Knowledge Search
                flag = sphome.Search_KA(searchInput);
                if (flag)
                {
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        #endregion Customer 01 can see Article 01, cannot see Article 02

        #region Customer 02 can see Article 02, cannot see Article 01

        [Test]
        public void Step_021_End_Impersonating_Customer_01()
        {
            try
            {
                //End Impersonating Customer_01 and return to ITIL View as the original logged in User
                flag = sphome.End_Impersonating(Base.GData("UserFullName"),true);
                if (flag)
                {
                    home.WaitLoading();
                    flag = home.Verify_User_FullName(Base.GData("UserFullName"));
                    if (!flag)
                    {
                        error = "User is NOT correct. Please check your data";
                    }
                }
                else
                {
                    error = "Cannot get Logout button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_Impersonate_Customer02()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("Customer02"), false, null, true);
                if (flag)
                {
                    Thread.Sleep(2000);
                    sphome.WaitLoading();
                }
                else
                {
                    error = "Cannot impersonate Customer 02";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_Open_KnowledgePage()
        {
            try
            {
                Step_016_Open_BrowseKnowledgePage();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_Search_Knowledge02()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Article 02's Id value.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                string searchInput = knowledgeId02;// + " - " + Base.GData("ShortDescription");

                // Search with Knowledge Search
                flag = sphome.Search_KA(searchInput);
                if (flag)
                {
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_Open_Knowledge02()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 02's Id value.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
               

                flag = sphome.Open_Item_From_Knowledge_Search_Result(knowledgeId02,true);
                if (flag)
                {
                    sphome.WaitLoading();
                }
                else
                {
                    error = "CANNOT open KA02 from list result";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_BackTo_KnowledgeSearchPage()
        {
            try
            {
                Step_016_Open_BrowseKnowledgePage();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_Search_Knowledge01_NotFound_01()
        {
            try
            {
                //------------------------------------------------------------------------------------------
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Article 01's Id value.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                string searchInput = knowledgeId01;// + " - " + Base.GData("ShortDescription");

                // Search with Knowledge Search
                flag = sphome.Search_KA(searchInput);
                if (flag)
                {
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_Search_Knowledge01_NotFound_02()
        {
            try
            {
                snoelement ele = sphome.No_Article();

                if (!ele.Existed) Console.WriteLine("[Article 01] may be found. Please check result list.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        #endregion Customer 02 can see Article 02, cannot see Article 01

        [Test]
        public void Step_032_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //***********************************************************************************************************************************

        #endregion Scenario of test case (NEED TO UPDATE)
    }
}