﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using SNow;
using System;
using System.Reflection;
using System.Threading;

namespace ServicePortal
{
    [TestFixture]
    public class SP_no_role_knowledge_user_12_2
    {
        #region Define default variables for test case (No need to update)

        //***********************************************************************************************************************************
        public bool flagC;

        public bool flag, flagExit, flagW;
        private string caseName, error;
        private SNow.snobase Base;

        //***********************************************************************************************************************************

        #endregion Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)

        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }

        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }

        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }

        //***********************************************************************************************************************************

        #endregion Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)

        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Knowledge feedback Id: " + kbFeedbackID);
            System.Console.WriteLine("Finished - Current date time: " + currentDateTime);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }

        //***********************************************************************************************************************************

        #endregion Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        //***********************************************************************************************************************************


        //------------------------------------------------------------------
        private Login login = null;

        private Home home = null;
        private ItilList list = null;
        private Itil itil = null;
        private SPortal sphome = null;
        

        //------------------------------------------------------------------
        private string knId01, knId02, knId03, knId04, knId05, knId06, knId07, knId08, knId09;
        private string sdkn01, sdkn02, sdkn03, sdkn04, sdkn05, sdkn06, sdkn07, sdkn08, sdkn09;
        private string kbFeedbackID;
        private string currentDateTime;
        //***********************************************************************************************************************************

        #endregion Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)



        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);        
                sphome = new SPortal(Base, "Service Portal");
                list = new ItilList(Base, "List");
                itil = new Itil(Base, "Knowledge");

                kbFeedbackID = string.Empty;

                knId01 = Base.GData("Article01");
                knId02 = Base.GData("Article02");
                knId03 = Base.GData("Article03");
                knId04 = Base.GData("Article04");
                knId05 = Base.GData("Article05");
                knId06 = Base.GData("Article06");
                knId07 = Base.GData("Article07");
                knId08 = Base.GData("Article08");
                knId09 = Base.GData("Article09");

                sdkn01 = Base.GData("ShortDes1");
                sdkn02 = Base.GData("ShortDes2");
                sdkn03 = Base.GData("ShortDes3");
                sdkn04 = Base.GData("ShortDes4");
                sdkn05 = Base.GData("ShortDes5");
                sdkn06 = Base.GData("ShortDes6");
                sdkn07 = Base.GData("ShortDes7");
                sdkn08 = Base.GData("ShortDes8");
                sdkn09 = Base.GData("ShortDes9");

                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sphome.WaitLoading();
                Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                        flag = sphome.Verify_Page_Open("Home;Knowledge");
                        if (!flag) error = "Cannot open page [Knowledge]";
                    }
                    else
                    {
                        error = "Error when click on menu issue";
                    }
                }
                else
                {
                    error = "Cannot get menu issue.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Search_Found_Knowledge01()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId01);
                if (flag)
                {
                    sphome.WaitLoading();
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Open_Knowledge01()
        {
            try
            {
                flag = sphome.Open_Item_From_Knowledge_Search_Result(sdkn01, true);

                if (flag)
                {
                    sphome.WaitLoading();
                    string temp = sphome.KB_Number().MyText;
                    if (temp.Trim() != knId01)
                    {
                        flag = false;
                        error = "Invalid KB number.";
                    }
                }
                else
                {
                    error = "CANNOT open KA from list result";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Search_Found_Knowledge06()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId06);
                if (flag)
                {
                    sphome.WaitLoading();
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Open_Knowledge06()
        {
            try
            {

                flag = sphome.Open_Item_From_Knowledge_Search_Result(sdkn06, true);

                if (flag)
                {
                    sphome.WaitLoading();
                    string temp = sphome.KB_Number().MyText;
                    if (temp.Trim() != knId06)
                    {
                        flag = false;
                        error = "Invalid KB number.";
                    }
                }
                else
                {
                    error = "CANNOT open KA from list result";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_Search_NotFound_Knowledge08()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId08);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_Search_NotFound_Knowledge09()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId09);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_Search_NotFound_Knowledge02()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId02);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_Search_NotFound_Knowledge03()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId03);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_Search_NotFound_Knowledge04()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId04);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_Search_NotFound_Knowledge05()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId05);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_Search_NotFound_Knowledge07()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId07);
                if (flag)
                {
                    sphome.WaitLoading();
                    snoelement notfound = sphome.No_Article();
                    flag = notfound.Existed;
                    if (!flag)
                    {
                        error = "Found KB. Expected NOT found.";
                    }
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_Open_BrowseKnowledgePage()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        sphome.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on menu [Knowledge]";
                    }
                }
                else
                {
                    error = "Cannot get menu [Knowledge].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_Search_Found_Knowledge01()
        {
            try
            {
                // Search with Knowledge Search
                flag = sphome.Search_KA(knId01);
                if (flag)
                {
                    sphome.WaitLoading();
                    flag = sphome.Verify_Page_Open("Home;Knowledge;Knowledge Search");
                    if (!flag) error = "Cannot open page [Knowledge Search]";
                }
                else
                {
                    error = "Cannot search using Knowledge search field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_Open_Knowledge01()
        {
            try
            {
                flag = sphome.Open_Item_From_Knowledge_Search_Result(sdkn01, true);

                if (flag)
                {
                    sphome.WaitLoading();
                    string temp = sphome.KB_Number().MyText;
                    if (temp.Trim() != knId01)
                    {
                        flag = false;
                        error = "Invalid KB number.";
                    }
                }
                else
                {
                    error = "CANNOT open KA from list result";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_Click_Helpful_YesButton()
        {
            try
            {
                snobutton button = sphome.Button_Yes();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot click Helpful Yes button.";
                    }
                }
                else { error = "Cannot get Helpful Yes button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_Rate_Star()
        {
            try
            {
                string star = Base.GData("Rate_Star");
                flag = sphome.KN_Rate_Star(star);
                if (!flag)
                    error = "Error when rate KN.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Populate_Comment()
        {
            try
            {
                string temp = Base.GData("Comment");
                currentDateTime = DateTime.Now.ToString("yyyMMddhhmm");
                //snotextarea textarea = sphome.Textarea_Knowledge_Add_Comment();
                //flag = textarea.Existed;
                //if (flag)
                //{
                //    flag = textarea.SetText(temp);
                //    if (!flag) { error = "Cannot populate Comment value."; }
                //}
                //else { error = "Cannot get Leave a Comment textare."; }
                snoelement link = sphome.GLinkByText("Click here to provide feedback on this article...");
                flag = link.Existed;
                if (flag)
                {
                    flag = link.Click();
                    Thread.Sleep(2000);
                    if (flag)
                    {
                        snoelement fr = new snoelement("iframe", Base.Driver, OpenQA.Selenium.By.TagName("iframe"));
                        string id = fr.MyElement.GetAttribute("id");
                        Console.WriteLine("//-- Frame id:" + id);

                        snoelement body = new snoelement("body", Base.Driver, OpenQA.Selenium.By.CssSelector("body[id='tinymce']"), null, id);
                        flag = body.Click();
                        body.MyElement.SendKeys(temp + " " + currentDateTime);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Submit()
        {
            try
            {
                snobutton button = sphome.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        sphome.WaitLoading();
                    else
                        error = "Error when click on submit button.";
                }
                else
                {
                    error = "Cannot get button submit.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Refresh()
        {
            try
            {
                Base.Driver.Navigate().Refresh();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_Return_LoginPage()
        {
            try
            {
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_ReLogin()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_ImpersonateUser_ServiceDeskAgent()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_SystemSetting()
        {
            try
            {
                //temp = Base.GData("FullPathDomain");
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_Open_KnowledgeFeedback_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge Feedback Tasks", "Knowledge Feedback Tasks");
                if (flag)
                    list.WaitLoading();
                else
                    error = "Error open Knowledge Feedback List.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_SearchAndOpen_Knowledge_Feedback()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (currentDateTime == null || currentDateTime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current date time.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                // [Linh 17 Sep 2019 - custom condition filter]
                string condition = "Created;on;Today|or|Created;on;Yesterday|and|Description;contains;" + Base.GData("Comment") + " " + currentDateTime;

                flag = list.Filter(condition);
                if (flag)
                {
                    //string con = "Feedback="+ Base.GData("Article01") + "|Description=" + Base.GData("Comment");
                    string con = "Feedback=" + knId01;
                    flag = list.Open(con, "Number");
                    if (flag)
                    {
                        list.WaitLoading();
                    }
                    else { error = "Error when search and open Knowledge Feedback"; }
                }
                else { error = "Error when filter condition"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_Verify_Information()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (currentDateTime == null || currentDateTime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current date time.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                snotextbox textbox = itil.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    kbFeedbackID = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

                snolookup lookup = itil.Lookup_Feedback();
                flag = lookup.Existed;
                if (flag)
                {
                    
                    flag = lookup.VerifyCurrentValue(knId01);
                    if (flag)
                    {
                        snotextarea textarea = itil.Textarea_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Comment") + " " + currentDateTime;
                            flag = textarea.VerifyCurrentValue("@@" + temp);
                            if (flag)
                            {
                                textarea = itil.Textarea_Description();
                                flag = textarea.Existed;
                                if (flag)
                                {
                                    flag = textarea.VerifyCurrentValue("@@" + temp);
                                    if (!flag)
                                    {
                                        error = "Cannot verify Description textarea value."; flagExit = false;
                                    }
                                }
                                else { error = "Cannot get Description textarea."; }
                            }
                            else { error = "Cannot verify Comment textarea value."; flagExit = false; }
                        }
                        else { error = "Cannot get Comment textarea."; }
                    }
                    else { error = "Cannot verify Feedback value."; flagExit = false; }
                }
                else { error = "Cannot get Feedback lookup."; }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_Logout()
        {
            try
            {
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
    }
}