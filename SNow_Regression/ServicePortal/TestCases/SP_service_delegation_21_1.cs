﻿using SNow;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using Auto;
namespace ServicePortal
{
    [TestFixture]
    public class SP_service_delegation_21_1
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Request Id: " + REQiD);
            System.Console.WriteLine("Finished - Requested Item Id: " + RITMiD);
            System.Console.WriteLine("Finished - Catalog Task: " + CTaskiD);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snoelement ele;
        snodatetime datetime;
        snocheckbox checkbox;
        
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Itil itil = null;
        ItilList list = null;
        SPortal sportal = null;
        EmailList emailList = null;
        Email email = null;
        
        //------------------------------------------------------------------
        string REQiD, RITMiD, CTaskiD;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                itil = new Itil(Base, "Itil");
                sportal = new SPortal(Base, "Service Portal");
                list = new ItilList(Base, "List");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                
                //------------------------------------------------------------------
                REQiD = string.Empty;
                RITMiD = string.Empty;
                CTaskiD = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA1()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("SDA1"));
                if (!flag)
                { error = "Cannot impersonate SDA1"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenProfilePage()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Self-Service", "My Profile");
                if (flag)
                {
                    itil.WaitLoading();
                    Thread.Sleep(3000);
                }
                else
                { error = "Cannot open the Profile page"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Delete_Existing_Delegates()
        {
            try
            {
                temp = Base.GData("Delegate_User");
                bool flagV = itil.Verify_RelatedTable_Row("Delegates", "Delegate=" + temp, true);             
                while (!flagV)
                {
                    flag = itil.Search_Open_RelatedTable_Row("Delegates", "Delegate", temp, "Delegate=" + temp, "Starts");
                    if (flag)
                    {
                        itil.WaitLoading();
                        button = itil.Button_Delete();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                string str = "button[id='ok_button']";
                                snobutton bt = (snobutton)Base.SNGObject("Button", "button", OpenQA.Selenium.By.CssSelector(str), null, snobase.MainFrame);
                                flag = bt.Existed;
                                if (flag)
                                {
                                    flag = bt.Click();
                                    if (!flag)
                                        System.Console.WriteLine("-*-[ERROR]: Error when click on button.");
                                    else
                                    {
                                        flag = home.LeftMenuItemSelect("Self-Service", "My Profile");
                                        if (flag)
                                        {
                                            itil.WaitLoading();
                                            Thread.Sleep(3000);
                                            flagV = itil.Verify_RelatedTable_Row("Delegates", "Delegate=" + temp, true);
                                        }
                                        else
                                        { error = "Cannot open the Profile page"; }
                                    }
                                }
                                else System.Console.WriteLine("-*-[ERROR]: Cannot get button.");
                            }
                            else error = "Invalid confirm dialog.";
                        }
                        else error = "Cannot get button Delete.";
                    }
                    else error = "Cannot open record.";
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Create_a_New_Delegate()
        {
            try
            {
                flag = itil.RelatedTab_Click_Button("Delegates", "New");
                if (flag)
                    itil.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Validate_Prepopulated_User_field()
        {
            try
            {
                temp = Base.GData("SDA1");
                lookup = itil.Lookup_Delegate_User();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                }
                else error = "Cannot get lookup User.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Validate_Approvals_Checkbox_Checked()
        {
            try
            {
                checkbox = itil.Checkbox_Approvals();
                flag = checkbox.Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "Approvals checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_03_Validate_Assignments_Checkbox_Checked()
        {
            try
            {
                checkbox = itil.Checkbox_Assignments();
                flag = checkbox.Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "Assignments checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_04_Validate_CCNotifications_Checkbox_Checked()
        {
            try
            {
                checkbox = itil.Checkbox_CCNotifications();
                flag = checkbox.Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "CC notifications checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_05_Validate_MeetingInvitations_Checkbox_Checked()
        {
            try
            {
                checkbox = itil.Checkbox_MeetingInvitations();
                flag = checkbox.Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "Meeting invitations checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        [Test]
        public void Step_009_Populate_Delegate_User()
        {
            try
            {
                temp = Base.GData("Delegate_User");
                lookup = itil.Lookup_Delegate();
                if (lookup.Existed)
                {
                    flag = lookup.Select(temp);
                    if (flag == false)
                    { error = "Cannot select Delegate user."; }
                }
                else
                {
                    flag = false;
                    error = "Cannot get Delegate lookup.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Populate_Start_End_Time()
        {
            try
            {

                //Get current Delegate start time and parse to datetime
                datetime = itil.Datetime_Delegate_Starts();
                temp = datetime.Text;
                var startTime = DateTime.Parse(temp);

                //Get current Delegate end time and parse to datetime
                datetime = itil.Datetime_Delegate_Ends();
                temp = datetime.Text;
                var endTime = DateTime.Parse(temp);

                //Set Delegate end time = start time + 120 minutes
                if (startTime < endTime)
                {
                    string d_endTime = startTime.AddMinutes(120).ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(d_endTime);
                    if (!flag)
                    {
                        error = "Unable to populate Delegate End Time";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_01_Uncheck_Approvals_Checkbox()
        {
            try
            {
                checkbox = itil.Checkbox_Approvals();
                flag = checkbox.Checked;
                if (flag)
                {
                    flag = checkbox.Click(true);

                    if (!flag)
                    {
                        error = "Unable to uncheck Approvals_box";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_Uncheck_CC_Notification_box()
        {
            try
            {
                checkbox = itil.Checkbox_CCNotifications();
                flag = checkbox.Checked;
                if (flag)
                {
                    flag = checkbox.Click(true);

                    if (!flag)
                    {
                        error = "Unable to uncheck CC_Notification checkbox";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_03_Uncheck_Meeting_Invitations_box()
        {
            try
            {
                checkbox = itil.Checkbox_MeetingInvitations();
                flag = checkbox.Checked;
                if (flag)
                {
                    flag = checkbox.Click(true);
                    if (!flag)
                    {
                        error = "Unable to uncheck Meeting_Invitations checkbox";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Submit_Delegate()
        {
            try
            {
                button = itil.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        itil.WaitLoading();
                    }
                    else
                    {
                        error = "Can't click Submit button";
                    }
                }
                else
                {
                    error = "Submit button is NOT existed";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Impersonate_User_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_ClickOn_Catalog_Link()
        {
            try
            {
                snoelement ele = sportal.GHeaderMenu("Catalog");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Catalog");
                        if (!flag)
                            error = "Cannot open page [Catalog]";
                    }
                    else
                        error = "Error when click on menu [Catalog].";
                }
                else error = "Not found [Catalog] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Select_Catalog_Item()
        {
            try
            {

                string item = Base.GData("Item");

                flag = sportal.Search_Open_CatalogItem(item);

                if (!flag)
                    error = "Cannot select catalog item.";
                else
                {
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_01_Verify_UserName_Field()
        {
            try
            {
                string temp = Base.GData("Customer");
                lookup = sportal.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user name value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_04_Verify_Email_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Email");
                textbox = sportal.Textbox_EmailAddress();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid email value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_06_Verify_Phone_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Phone");
                textbox = sportal.Textbox_TelephoneNumber();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user phone value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox user phone.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_07_Verify_Location_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Location");
                lookup = sportal.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user location value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Input_MandatoryFields()
        {
            try
            {
                flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_OrderItem()
        {
            try
            {
                button = sportal.Button_Submit();
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_Get_Request_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Number();

                flag = ele.Existed;
                if (flag)
                {
                    REQiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Get_Request_Item_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Item_Number();

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_020_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_021_ImpersonateUser_Manager()
        {
            try
            {
                temp = Base.GData("Manager");
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_ClickOn_Approvals()
        {
            try
            {
                ele = sportal.GHeaderMenu("Approvals");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag)
                        error = "Error when click on approvals.";
                }
                else
                {
                    error = "Not found My Approvals on dashboard menu.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Open_Item()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                Thread.Sleep(1000);
                flag = sportal.Select_Approvals_Item(RITMiD);
                if (!flag)
                    error = "Cannot open item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Add_Approval_Comment()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                textarea = sportal.Textarea_Approval_Active_Stream_Comment();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(RITMiD);
                    if (flag)
                    {
                        button = sportal.Button_Send();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag)
                                error = "Error when click on send button.";
                        }
                        else error = "Cannot get button send.";
                    }
                    else error = "Cannot populate comment.";
                }
                else error = "Cannot get textarea comment.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_ClickOn_Button_Approve()
        {
            try
            {
                button = sportal.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                        sportal.WaitLoading();
                    else
                        error = "Error when click approve button.";
                }
                else error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_026_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_ImpersonateUser_SDA2()
        {
            try
            {
                string temp = Base.GData("SDA2");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else itil.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_SearchAndOpenAssignedItem()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + RITMiD;
                    flag = list.Filter("Parent;contains;" + RITMiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Populate_AssignedTo_SDA1()
        {
            try
            {
                lookup = itil.Lookup_AssignedTo();
                if (lookup.Existed)
                {
                    temp = Base.GData("SDA1");
                    flag = lookup.Select(temp);
                    if (!flag)
                    { error = "Cannot populate Assigned to"; }
                    else
                    {
                        flag = itil.Save();
                        if (!flag)
                        { error = "Unable to save the task"; }
                    }
                }
                else
                {
                    flag = false;
                    error = "Not found lookup Assigned to";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_ImpersonateUser_Delegate()
        {
            try
            {
                temp = Base.GData("Delegate_User");
                flag = home.ImpersonateUser(temp);
                if (!flag)
                { error = "Cannot impersonate Delegate User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Open_MyWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Work");
                if (!flag)
                { error = "Cannot open My Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_SearchAndOpenAssignedItem()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + RITMiD;
                    flag = list.Filter("Parent;contains;" + RITMiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Store_CTask_Id()
        {
            try
            {
                textbox = itil.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    CTaskiD = textbox.Text;
                }
                else { error = "Not found textbox Number"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_01_Verify_AssignmentGroup_Field()
        {
            try
            {
                temp = Base.GData("CTask_AssignmentGrp");
                lookup = itil.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid assignment group value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup assignment group.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_02_Verify_RequestItem_Field()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                
                lookup = itil.Lookup_RequestItem();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.VerifyCurrentValue(RITMiD, true);
                    if (!flag)
                    {
                        error = "Invalid request item value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup request item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_03_Verify_RequestedFor_Field()
        {
            try
            {
                temp = Base.GData("Customer");
                lookup = itil.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid requested for value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup requested for.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_04_Verify_CTask_State_Field_Open()
        {
            try
            {
                temp = "Open";
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {

                    flag = combobox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid state value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Add_CTask_WorkNote()
        {
            try
            {
                temp = Base.GData("CTask_WorkNote");
                flag = itil.Add_Worknotes(temp);
                if (!flag)
                    error = "Error when add work note.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Save_CTask()
        {
            try
            {
                flag = itil.Save();
                if (!flag) { error = "Error when save ctask."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Close_CTask_ClosedCommplete()
        {
            try
            {
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        flag = itil.Save();
                        if (!flag)
                        { error = "Cannot save Catalog Task."; }
                    }
                    else { error = "Cannot update Catalog Task's State"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Impersonate_User_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_ClickOn_CheckStatus()
        {
            try
            {
                ele = sportal.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else
                {
                    error = "Not found My Requests or Check Status on dashboard menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_ClickOn_MyRequests()
        {
            try
            {
                ele = sportal.GLeftMenu("My Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else error = "Not found My Prior Approvals on left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_ClickOn_MyPriorRequests()
        {
            try
            {
                ele = sportal.GLinkByText("My Prior Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag) sportal.WaitLoading();
                }
                else error = "Not found menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_SearchAndOpen_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------
                string condition = "Stage=Completed";
                flag = sportal.SearchAndOpen(RITMiD, condition, "Number");
                if (flag)
                {
                    sportal.WaitLoading();
                }
                else { error = "Not found Requested Item"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Verify_Stage_Completed()
        {
            try
            {
                string temp = "Completed";
                combobox = sportal.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {

                    flag = combobox.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid stage value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_047_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_ImpersonateUser_CSCSupport()
        {
            try
            {
                temp = Base.GData("Support_User");
                flag = home.ImpersonateUser(temp);
                if (!flag)
                { error = "Cannot impersonate CSC Support User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_02_Filter_EmailSentToCustomer_HasBeenSubmited()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been submitted|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_03_Open_EmailSentToCustomer_HasBeenSubmited()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been submited).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_04_Verify_Email_Submitted_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                string item = Base.GData("Item");
                temp = "INFO: " + item + " (" + RITMiD + ") has been submitted.";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: (" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_05_Verify_Email_Submitted_Recipient()
        {
            try
            {
                string recipient = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(recipient);
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_07_Verify_EmailHtmlBody_Note()
        {
            try
            {
                string item = Base.GData("Item");
                temp = item + " has been successfully submitted and is awaiting approval.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_049_08_Verify_EmailHtmlBody_TicketId()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        //--------------------------------------------------------------------------------
        //        temp = "Requested Item Number: " + RITMiD;
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Requested Item id.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_049_09_Verify_EmailHtmlBody_ItemName()
        //{
        //    try
        //    {
        //        temp = "Item: " + Base.GData("Item");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Item name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_049_10_Verify_EmailHtmlBody_Description()
        //{
        //    try
        //    {
        //        temp = Base.GData("Item_Description");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid description.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_11_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flag = false;
                    flagExit = false;
                    error = "Invalid description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_12_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_02_Filter_EmailSentToManager_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Manager_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;Approval request|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_03_Open_EmailSentToManager_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to manager (approval request).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_04_Verify_Email_Approval_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Requested Item " + RITMiD + " Approval request";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_05_Verify_Email_Approval_Recipient()
        {
            try
            {
                temp = Base.GData("Manager_Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flag = false;
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_07_Verify_EmailHtmlBody_Note()
        {
            try
            {
                temp = "Please Approve or Reject the request by 1) selecting the appropriate link below AND 2) manually sending the email that is generated. You must send the auto-generated email to complete the processing.";
                flag = email.VerifyEmailBody(temp);
                if (flag == false)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_08_Verify_EmailHtmlBody_ApproveTicketId()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to approve " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approve message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_09_Verify_EmailHtmlBody_RejectTicketId()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to reject " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Reject message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_10_Verify_EmailHtmlBody_ApprovalLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Approval Request: Requested Item: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approval link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_11_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_02_Filter_EmailSentToCustomer_HasBeenApproved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been approved|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_03_Open_EmailSentToCustomer_HasBeenApproved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been submited).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_04_Verify_Email_Item_Approved_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Item " + RITMiD + " has been approved.";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_05_Verify_Email_Item_Approved_Recipient()
        {
            try
            {
                temp = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_07_Verify_EmailHtmlBody_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Requested item " + RITMiD + " " + Base.GData("Item_ShortDes") + " has been approved and will be fulfilled.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_08_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "To view the item, click here: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Link.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_09_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_02_Filter_EmailSentToCustomer_HasBeenCompleted()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been completed|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_03_Open_EmailSentToCustomer_HasBeenCompleted()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been completed).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_04_Verify_Email_Item_Completed_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Requested Item " + RITMiD + " has been completed.";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_05_Verify_Email_Item_Completed_Recipient()
        {
            try
            {
                temp = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_07_Verify_EmailHtmlBody_Content()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "This Requested Item has been completed.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_08_Verify_EmailHtmlBody_ItemName()
        {
            try
            {
                temp = "Item: " + Base.GData("Item");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_09_Verify_EmailHtmlBody_ItemState()
        {
            try
            {
                temp = "State: Closed Complete";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_10_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Link.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_11_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
