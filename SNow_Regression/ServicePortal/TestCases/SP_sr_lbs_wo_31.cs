﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
using SNow;
namespace ServicePortal
{
    [TestFixture]
    public class SP_sr_lbs_wo_31
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Request Order Id: " + REQiD);
            System.Console.WriteLine("Finished - Request Item Id: " + RITMiD);
            System.Console.WriteLine("Finished - Catalog Task: " + CTaskID);
            System.Console.WriteLine("Finished - Work Order Id: " + WOiD);
            System.Console.WriteLine("Finished - Work Order Task Id: " + WOTiD);
            System.Console.WriteLine("Finished - New today 1: " + newToday1);
            System.Console.WriteLine("Finished - Assigned 1: " + assigned1);
            
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snoelement ele;
        snocheckbox checkbox;
        snodatetime datetime;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SPortal sportal = null;
        EmailList emailList = null;
        Email email = null;
        Itil itil = null;
        ItilList list = null;
        WorkOrder wo = null;
        WorkOrderTask wot = null;
        WorkOrder_AssignedToSearch woAssigned = null;
        
        //------------------------------------------------------------------
        string REQiD, RITMiD, RITMiD2, CTaskID, WOiD, WOTiD;
        int newToday1, assigned1, accepted1, wip1, awaitingcustomer1;
        
        string actTravelStart;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                sportal = new SPortal(Base, "sportal");
                wo = new WorkOrder(Base, "Work Order");
                wot = new WorkOrderTask(Base, "Work Order Task");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                woAssigned = new WorkOrder_AssignedToSearch(Base, "WO assigned to search");
                itil = new Itil(Base, "Itil");
                list = new ItilList(Base, "List");
                //------------------------------------------------------------------
                REQiD = string.Empty;
                RITMiD = string.Empty;
                RITMiD2 = string.Empty;
                CTaskID = string.Empty;
                WOiD = string.Empty;
                WOTiD = string.Empty;

                newToday1 = -1;
                assigned1 = -1;
                accepted1 = -1;
                wip1 = -1;
                awaitingcustomer1 = -1;

                actTravelStart = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Customer submit Catalog Item
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------    
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------    
        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer");             
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_ClickOn_Catalog_Link()
        {
            try
            {
                snoelement ele = sportal.GHeaderMenu("Catalog");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Catalog");
                        if (!flag)
                            error = "Cannot open page [Catalog]";
                    }
                    else
                        error = "Error when click on menu [Catalog].";
                }
                else error = "Not found [Catalog] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Select_Catalog_Item()
        {
            try
            {

                string item = Base.GData("Catalog_Item");

                flag = sportal.Search_Open_CatalogItem(item);

                if (!flag)
                    error = "Cannot select catalog item.";
                else
                {
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_Verify_UserName_Field()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                lookup = sportal.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user name value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_006_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_006_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_04_Verify_Email_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Email");
                textbox = sportal.Textbox_EmailAddress();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid email value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_006_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_06_Verify_Phone_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Phone");
                textbox = sportal.Textbox_TelephoneNumber();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user phone value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox user phone.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_07_Verify_Location_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Location");
                lookup = sportal.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user location value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Input_MandatoryFields()
        {
            try
            {
                flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_OrderItem()
        {
            try
            {
                button = sportal.Button_Submit();
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_Get_Request_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Number();

                flag = ele.Existed;
                if (flag)
                {
                    REQiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Get_Request_Item_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Item_Number();

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_010_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Manager gives approval
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_011_ImpersonateUser_Approver()
        {
            try
            {
                temp = Base.GData("Approver");
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_ClickOn_Approvals()
        {
            try
            {
                ele = sportal.GHeaderMenu("Approvals");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag)
                        error = "Error when click on approvals.";
                }
                else
                {
                    error = "Not found My Approvals on dashboard menu.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Open_Item()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                Thread.Sleep(1000);
                flag = sportal.Select_Approvals_Item(RITMiD);
                if (!flag)
                    error = "Cannot open item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Add_Approval_Comment()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                textarea = sportal.Textarea_Approval_Active_Stream_Comment();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(RITMiD);
                    if (flag)
                    {
                        button = sportal.Button_Send();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag)
                                error = "Error when click on send button.";
                        }
                        else error = "Cannot get button send.";
                    }
                    else error = "Cannot populate comment.";
                }
                else error = "Cannot get textarea comment.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_ClickOn_Button_Approve()
        {
            try
            {
                button = sportal.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                        sportal.WaitLoading();
                    else
                        error = "Error when click approve button.";
                }
                else error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_016_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Customer track the Service Request

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_ImpersonateUser_Customer()
        {
            try
            {
                temp = Base.GData("Customer");
                flag = home.ImpersonateUser(temp, false, null, true);
                if (flag)
                {
                    sportal.WaitLoading();
                }
                else
                { error = "Cannot impersonate Customer"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_ClickOn_CheckStatus()
        {
            try
            {
                ele = sportal.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else
                {
                    error = "Not found My Requests or Check Status on dashboard menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_ClickOn_MyRequests()
        {
            try
            {
                ele = sportal.GLeftMenu("My Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else error = "Not found My Prior Approvals on left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_ClickOn_MyActiveRequests()
        {
            try
            {
                ele = sportal.GLinkByText("My Active Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag) sportal.WaitLoading();
                }
                else error = "Not found menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_Item_With_Stage_Fulfilment()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = sportal.SearchAndVerifyRow(RITMiD, "Number=" + RITMiD + "|Stage=Fulfilment");
                if (!flag)
                    error = "NOT FOUND request item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_022_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Support Staff verify the Catalog task 1
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_023_ImpersonateUser_SupportUser()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("Support_User"));
                if (!flag)
                { error = "Cannot impersonate Support User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                    string title = list.List_Title().MyText;
                    if (title != "Tasks")
                    {
                        flag = false;
                        flagExit = false;
                        error = "The title in incorrect";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Search_And_Open_CTask_Of_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + RITMiD;
                    flag = list.Filter("Parent;contains;" + RITMiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Validate_CTask_Page()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------------------
                error = string.Empty;
                //--Get Task ID
                textbox = itil.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    CTaskID = textbox.Text;
                }
                else { error = "Not found textbox Number"; }
                //-----------------
                //Validate pre-populated Assignment Group is "Location Based Services"
                temp = Base.GData("CTask_AssignmentGroup");
                lookup = itil.Lookup_AssignmentGroup();
                if (!lookup.Existed || lookup.Text.Trim() != temp)
                {
                    flag = false;
                    flagExit = false;
                    error += "Incorrect default value for Assignment Group field populated. Actual is: [" + lookup.Text + "]";
                }
                //Validate Requested Item 
                lookup = itil.Lookup_RequestItem();
                if (!lookup.Existed || lookup.Text.Trim() != RITMiD)
                {
                    flag = false;
                    flagExit = false;
                    error += "Incorrect Requested Item ID. Actual is: [" + lookup.Text + "]";
                }
                //Validate Requested For 
                lookup = itil.Lookup_RequestedFor();
                temp = Base.GData("Customer");
                if (!lookup.Existed || lookup.Text.Trim() != temp)
                {
                    flag = false;
                    flagExit = false;
                    error += "Incorrect Requested For populated. Actual is: [" + lookup.Text + "";
                }
                //Validate State
                combobox = itil.Combobox_State();
                if (combobox.Text != "Open")
                {
                    flag = false;
                    flagExit = false;
                    error += "Incorrect State populated. Actual is: [" + combobox.Text + "";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Add_CTask_WorkNote()
        {
            try
            {
                temp = Base.GData("CTask_WorkNote");
                flag = itil.Add_Worknotes(temp);
                if (!flag)
                    error = "Error when add work note.";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Save_CTask()
        {
            try
            {
                flag = itil.Save();
                if (!flag) { error = "Error when save ctask."; }
            }
            catch (Exception ex)                                
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #endregion

        #region Support Staff works on the work order
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Open_Generated_WO()
        {
            try
            {
                string conditions = "Number=@@WO";
                flag = itil.RelatedTableOpenRecord("Work Orders", conditions, "Number");
                if (!flag)
                {
                    error = "Error when click on record";
                }
                else
                {
                    wo.WaitLoading();
                    textbox = wo.Textbox_Number();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        textbox.Click();
                        //-- Store work order id
                        WOiD = textbox.Text;
                        Console.WriteLine("-*-[Store]: Work Order Id:(" + WOiD + ")");
                    }
                    else
                    {
                        error = "Cannot get texbox number.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------
        [Test]
        public void Step_030_01_Verify_Company()
        {
            try
            {
                temp = Base.GData("Company");
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Company is not correct";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup company.";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }  
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_02_Validate_Initiated_From()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && CTaskID == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Catalog Task number.");
                    addPara.ShowDialog();
                    CTaskID = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                lookup = wo.Lookup_Initiatedfrom();

                flag = lookup.Existed;

                if (!flag)
                {
                    error = "Cannot get lookup initiated from.";
                }
                else 
                {
                    flag = lookup.VerifyCurrentValue(CTaskID);
                    if(!flag)
                        error = "'Initiated from' is NOT correct. Expect: [" + CTaskID + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_03_Validate_State_Is_Draft()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (!flag)
                    {
                        error = "State is NOT correct.";
                    }
                }
                else error = "Cannot get combobox State.";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_04_Validate_Qualification_Group()
        {
            try
            {
                //Qualification Group is auto-populated based on Location of Caller
                temp = Base.GData("WO_Qualification_Group");
                lookup = wo.Lookup_QualificationGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "[Qualification group] is NOT correct. Expect: [" + temp + "]";
                    }
                }
                else
                {
                    error = "[Qualification group] field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Select_WO_Category()
        {
            try
            {
                temp = Base.GData("WO_Category");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                }
                else
                {
                    error = "Cannot select Category";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Add_Asset_Usages()
        {
            try
            {
                //Add Manual asset tag
                flag = wo.RelatedTable_Edit_Cell("Assets", null, "Manual asset tag", "Asset tag test" + DateTime.Now.Millisecond + 1, "0");
                if (flag)
                {
                    //Add Manual type
                    flag = wo.RelatedTable_Edit_Cell("Assets", null, "Manual type", "Monitor", "0");
                    if (flag)
                    {
                        //Add Manual model
                        flag = wo.RelatedTable_Edit_Cell("Assets", null, "Manual model", "Asset manual model" + DateTime.Now.Millisecond + 1, "0");

                        if (flag)
                        {
                            //Add Comments
                            flag = wo.RelatedTable_Edit_Cell("Assets", null, "Comments", "Asset comments" + DateTime.Now.Millisecond + 1, "0");
                            if (flag)
                            {
                                //Add Status
                                flag = wo.RelatedTable_Edit_Cell("Assets", null, "Status", "Validated", "0");
                                if (!flag)
                                {
                                    error = "'Validate' option is NOT existing or can't be select";
                                }
                            }
                            else
                            {
                                flagExit = false;
                                error = "Can't add Comments";
                            }
                        }
                        else
                        {
                            flagExit = false;
                            error = "Can't add manual model.";
                        }
                    }
                    else
                    {
                        flagExit = false;
                        error = "Can't add Manual model";
                    }
                }
                else
                {
                    flagExit = false;
                    error = "Cannot add manual asset tag."; 
                }    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Save_WO()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save ctask."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Click_Ready_for_Qualification_button()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;

                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wot.WaitLoading();
                    }
                    else
                    {
                        error = "Can't click 'Ready for Qualification' button";

                    }
                }
                else
                {
                    error = "There is no 'Ready for Qualification' button ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_01_Store_WOT_ID()
        {
            try
            {
                Thread.Sleep(2000);
                textbox = wot.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    WOTiD = textbox.Text;
                }
                else
                {
                    error = "Cannot get textbox Number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_02_Verify_WOT_State_Draft()
        {
            try
            {
                combobox = wot.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft", true);
                    if(!flag)
                        error = "State of WOT is NOT correct. Expectation is [Draft]";
                }
                else
                {
                    error = "Cannot get combobox State.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_03_Verify_Caller()
        {
            try
            {
                lookup = wot.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("Customer");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Caller's information is NOT matched";
                    }
                }
                else
                {
                    error = "There is no Caller's information";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_04_Verify_Location()
        {
            try
            {
                lookup = wot.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("Customer_Location");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Caller's location is NOT matched";
                    }
                }
                else
                {
                    error = "There is no Caller's location";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_05_Verify_WOT_Dispatch_Group()
        {
            try
            {
                temp = Base.GData("WOT_DispatchGroup");
                lookup = wot.Lookup_DispatchGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);

                    if (!flag)
                    {
                        error = "Dispatch Group is NOT as expected. Runtime [" + lookup.Text + "]. Expected [" + temp + "]";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Click_Qualified_button()
        {
            try
            {
                button = wot.Button_Qualified();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else
                    {
                        error = "Can't click 'Qualified' button";
                    }
                }
                else
                {
                    error = "There is no 'Qualified' button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Verify_WOT_Assignment_Group()
        {
            try
            {
                temp = Base.GData("WOT_Assignment_Group");
                lookup = wot.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);

                    if (!flag)
                    {
                        error = "Assignment Group is NOT as expected. Runtime [" + lookup.Text + "]. Expected [" + temp + "]";
                    }
                }
                else
                {
                    error = "Assignment Group is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_01_Click_Skill_Button()
        {
            try
            {
                ele = wot.Button_Skill();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag)
                    {
                        error = "Can't click 'Skill' button";
                    }
                }
                else error = "Cannot get skill button.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_02_Add_Skill()
        {
            try
            {
                //Add Skill = IT
                lookup = wot.Lookup_WO_Task_Skill();
                flag = lookup.SetText("IT", true);
                if (!flag)
                {
                    error = "Can't select Skill = IT.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_01_Click_SelectAssignee_Button()
        {
            try
            {
                ele = wot.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        wot.WaitLoading();
                    }
                    else
                        error = "Can't click 'Select Assignee Button";
                }
                else error = "Cannot get select assignee button.";         
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_02_Switch_to_Select_Assignee_Page()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page select assignee."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_GetInfo_Of_Assignee_1()
        {
            try
            {
                woAssigned.WaitLoading();
                string assignee = Base.GData("WOT_Technician_1");
                string conditions = "Name=" + assignee;
                newToday1 = int.Parse(woAssigned.RelatedTableGetCell(conditions, "New Today", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));
                assigned1 = int.Parse(woAssigned.RelatedTableGetCell(conditions, "Assigned", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));
                accepted1 = int.Parse(woAssigned.RelatedTableGetCell(conditions, "Accepted", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));
                wip1 = int.Parse(woAssigned.RelatedTableGetCell(conditions, "Work In Progress", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));
                awaitingcustomer1 = int.Parse(woAssigned.RelatedTableGetCell(conditions, "Awaiting Customer", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));     
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_Select_WOT_Technician_1()
        {
            try
            {
                string temp = Base.GData("WOT_Technician_1");
                string conditions = "Name=" + temp;
                flag = woAssigned.Open(conditions, "Name", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine);
                if (!flag)
                {
                    error = "Cannot choose WO Technician with condition: " + temp;
                }           
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_Switch_to_WOT_page()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page WO Task."; }
                else wo.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Save_WOT()
        {
            try
            {
                flag = wot.Save(true);
                if (flag)
                {
                    wot.WaitLoading();
                }
                else
                {
                    error = "Can't save WOT";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_01_Click_SelectAssignee_Button()
        {
            try
            {
                ele = wot.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        wot.WaitLoading();
                    }
                    else
                        error = "Can't click 'Select Assignee Button";
                }
                else error = "Cannot get select assignee button.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_Switch_to_Select_Assignee_Page()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page select assignee."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_03_Verify_NewToday_Assigned_Of_Assignee_1()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && newToday1 == -1)
                {
                    AddParameter addPara = new AddParameter("Please input new today 1.");
                    addPara.ShowDialog();
                    newToday1 = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }

                if (temp == "yes" && assigned1 == -1)
                {
                    AddParameter addPara = new AddParameter("Please input assigned 1.");
                    addPara.ShowDialog();
                    assigned1 = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------
                woAssigned.WaitLoading();
                string assignee = Base.GData("WOT_Technician_1");
                string conditions = "Name=" + assignee;
                int newToday1Runtime = int.Parse(woAssigned.RelatedTableGetCell(conditions, "New Today", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));
                int assigned1Runtime = int.Parse(woAssigned.RelatedTableGetCell(conditions, "Assigned", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine));
                
                if (newToday1Runtime != newToday1 + 1)
                {
                    flag = false;
                    flagExit = false;
                    error = "New Today number is NOT correct. Expect = [" + (newToday1 + 1) + "], while Runtime =[" + newToday1Runtime + "]";
                }

                if (assigned1Runtime != (assigned1 + 1))
                {
                    flagExit = false;
                    error = "Assigned number is NOT correct. Expect = [" + (assigned1 + 1) + "], while Runtime =[" + assigned1Runtime + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_04_Close_SelectAssignee_Window()
        {
            try
            {
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_05_Switch_to_WOT_page()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page WO Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Verify_WOT_State_Assigned()
        {
            try
            {
                combobox = wot.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Assigned");
                    if (!flag)
                    {
                        error = "State field is NOT correct. Expect [Assigned]. Runtime [" + combobox.Text + "]";
                    }
                }
                else
                {
                    error = "State field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_Update_SubState_to_Awaiting_Customer()
        {
            try
            {
                combobox = wot.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting customer");
                    if (!flag)
                    {
                        error = "Can't select item 'Awaiting customer'";
                    }
                }
                else
                {
                    error = "Sub State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_Populate_Follow_Up_Date()
        {
            try
            {
                datetime = wot.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    temp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(temp);
                    if (!flag)
                    {
                        error = "Can't populate follow up date.";
                    }
                }
                else
                {
                    error = "Cannot get datetime follow up date.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Populate_Additional_Comments()
        {
            try
            {
                flag = wot.Add_AdditionComments("Change State to [Awaiting Customer] - Automation testing");
                if (!flag)
                    error = "Error when populate additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Save_WOT()
        {
            try
            {
                flag = wot.Save(true);
                if (flag)
                {
                    wot.WaitLoading();
                }
                else
                {
                    error = "Can't save WOT";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Search_And_Open_CTask_Of_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + RITMiD;
                    flag = list.Filter("Parent;contains;" + RITMiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------
        [Test]
        public void Step_049_Verify_CTask_State_Pending()
        {
            try
            {
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending");
                    if (!flag)
                    {
                        error = "State of Catalog Task is NOT correct. Expect [Pending]";
                    }
                }
                else
                {
                    error = "State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Search_And_Open_WOTask()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && WOiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Work Order Id.");
                    addPara.ShowDialog();
                    WOiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + WOiD;
                    flag = list.Filter("Parent;contains;" + WOiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            wot.WaitLoading();
                        }
                        else { error = "Cannot find Work Order Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_Update_SubState_to_None()
        {
            try
            {
                combobox = wot.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("-- None --");
                    if (!flag)
                    {
                        error = "Can't select item '-- None --'";
                    }
                }
                else
                {
                    error = "Sub State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_02_Save_WOT()
        {
            try
            {
                flag = wot.Save(true);
                if (flag)
                {
                    wot.WaitLoading();
                }
                else
                {
                    error = "Can't save WOT";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Search_And_Open_CTask_Of_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + RITMiD;
                    flag = list.Filter("Parent;contains;" + RITMiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            itil.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------
        [Test]
        public void Step_055_Verify_CTask_State_WorkInProgress()
        {
            try
            {
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Work in Progress");
                    if (!flag)
                    {
                        error = "State of Catalog Task is NOT correct. Expect [Work in Progress]";
                    }
                }
                else
                {
                    error = "State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #endregion

        #region Verify email assignment for WO Technician 1

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_02_Filter_Email_WOT_Assign_to_Technician_1()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && WOTiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------
                string tech1_email = Base.GData("WOT_Technician_1_Email");
                temp = "Subject;contains;" + WOTiD + "|and|Subject;contains;has been assigned to you|and|Recipients;contains;" + tech1_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_03_Validate_EmailSentTo_Technician_1_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && WOTiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + WOTiD;
                flag = emailList.VerifyRow(conditions);
                if (!flag) error = "Not found email sent to technician 1 (Assigned).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Impersonate_Tecnician_1()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("WOT_Technician_1"));
                if (!flag)
                { error = "Cannot impersonate WOT Technician 1"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_Open_Assigned_to_me_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Field Service", "Assigned to me");
                if (!flag)
                { error = "Cannot open Assigned to me link."; }
                else
                {
                    list.WaitLoading();
                    string label = list.List_Title().MyText;
                    if (label != "Work Order Tasks")
                    {
                        flag = false;
                        flagExit = false;
                        error = "The title in incorrect";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Search_And_Open_Assigned_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && WOTiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                temp = "Number=" + WOTiD;
                flag = list.Filter("Number;is;" + WOTiD);
                if (flag)
                {
                    flag = list.Open(temp, "Number");
                    if (flag)
                    {
                        wot.WaitLoading();
                    }
                    else { error = "Cannot find WO Task"; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_01_Check_Follow_up_checkbox()
        {
            try
            {
                checkbox = wot.Checkbox_FollowUp();
                flag = checkbox.Existed;
                if (flag)
                {
                    if (!checkbox.Checked) 
                    {
                        flag = checkbox.Click(true);
                        if (!flag)
                        {
                            error = "Can't check Follow up box";
                        }
                    }          
                }
                else
                {
                    error = "Follow up checkbox is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_02_Add_Additional_Comment()
        {
            try
            {
                flag = wot.Add_AdditionComments("Check Follow up checkbox");
                if (!flag)
                    error = "Error when add additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_03_Save_WOT()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save wot."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_061_ImpersonateUser_SupportUser()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("Support_User"));
                if (!flag)
                { error = "Cannot impersonate Support User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_062_Open_My_Dispatch_Queue()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Field Service", "My Dispatch Queue");
                if (flag)
                {
                    list.WaitLoading();

                    if (!list.List_Title().MyText.Contains("Work Order Tasks"))
                    {
                        flag = false;
                        error = "Cannot open Work Order Tasks list.";
                    }
                }
                else error = "Error when open My Dispatch Queue.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_063_Search_the_Follow_up_WOT_And_Open()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (WOTiD == null || WOTiD == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WOT Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = "Number=" + WOTiD;
                flag = list.Filter("Dispatch group;is;" + Base.GData("WOT_DispatchGroup") + "|or|Dispatch group;is;" + Base.GData("WO_Qualification_Group") + "|and|Follow up;is;true");
                if (flag)
                {
                    flag = list.Open(temp, "Number");
                    if (flag)
                    {
                        wot.WaitLoading();
                    }
                    else { error = "Cannot find Work Order Task"; }
                }
                else { error = "Error when execute filter"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_01_UnCheck_Follow_up_checkbox()
        {
            try
            {
                checkbox = wot.Checkbox_FollowUp();
                flag = checkbox.Existed;
                if (flag)
                {
                    if (checkbox.Checked) 
                    {
                        flag = checkbox.Click(true);
                        if (!flag)
                        {
                            error = "Can't check Follow up box";
                        }
                    }        
                }
                else
                {
                    error = "Follow up checkbox is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_02_Add_Additional_Comment()
        {
            try
            {
                flag = wot.Add_AdditionComments("UnCheck Follow up checkbox");
                if (!flag)
                    error = "Error when add additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_03_Save_WOT()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save wot."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #region Technician 2 Close WO Task as complete
        
        [Test]
        public void Step_065_ImpersonateUser_WO_Technician_2()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("WOT_Technician_2"), true, Base.GData("UserFullName"));
                if (!flag)
                { error = "Cannot impersonate WOT_Technician_2"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_Search_And_Open_WOT()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && WOTiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input WOT Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------------------                
                if (flag)
                {
                    flag = list.Filter("Number;is;" + WOTiD);
                    if (flag)
                    {
                        temp = "Number=" + WOTiD;
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            wo.WaitLoading();
                        }
                        else { error = "Cannot find WOT"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_068_Click_AssigntoMe_Button()
        {
            try
            {
                button = wot.Button_AssignToMe();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        wot.WaitLoading();
                        Thread.Sleep(3000);
                    }
                    else
                    {
                        error = "Can't click 'Assign to me' button";
                    }
                }
                else
                {
                    error = "Assign to me button is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_01_Verify_WOT_Assignment_Group()
        {
            try
            {
                temp = Base.GData("WOT_Assignment_Group");
                lookup = wot.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);

                    if (!flag)
                    {
                        error = "Assignment Group is NOT as expected. Runtime [" + lookup.Text + "]. Expected [" + temp + "]";
                    }
                }
                else
                {
                    error = "Assignment Group is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_069_02_Verify_Assigned_to()
        {
            try
            {
                Thread.Sleep(2000);
                lookup = wot.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("WOT_Technician_2"));
                    if (!flag)
                    {
                        error = "Assigned to is NOT corect";
                    }
                }
                else
                {
                    error = "The Assignment Group is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_069_03_Verify_State_Accepted()
        {
            try
            {
                combobox = wot.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Accepted");
                    if (!flag)
                    {
                        error = "State is NOT corect";
                    }
                }
                else
                {
                    error = "State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_04_Add_Additional_Comment()
        {
            try
            {
                flag = wot.Add_AdditionComments("Assign to me");
                if (!flag)
                    error = "Error when add additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_05_Save_WOT()
        {
            try
            {
                flag = wot.Save();
                if (!flag) { error = "Error when save wot."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_01_Open_Quick_Comment()
        {
            try
            {
                flag = wot.Select_Tab("Notes");
                if (flag)
                {
                    ele = wot.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag)
                        {
                            error = "Can not open Quick Comment.";
                        }
                    }
                    else
                    {
                        error = "Cannot get look up Quick Comment.";
                    }
                }
                else error = "Cannot select tab Notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_02_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_03_Select_Comment()
        {
            try
            {
                string temp = Base.GData("QuickComment");
                string conditions = "Label=" + temp;
                flag = list.Open(conditions, "Label", true, null, "thead th a", "tbody>tr", "tbody>tr>td:not([class])");
                if (!flag)
                {
                    error = "Cannot choose Quick Comment.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_04_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_070_05_Verify_QuickComment_Added()
        {
            try
            {
                textarea = wot.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(Base.GData("QuickComment"));
                    if (!flag)
                    {
                        error = "QuickComment was not add to additional comment.";
                    }
                }
                else
                {
                    error = "Cannot get textarea additional comment.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_01_Select_Actual_tab()
        {
            try
            {
                flag = wot.Select_Tab("Actual", true);
                if (!flag)
                    error = "Error when click on Actual tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_02_Click_Start_Travel_WOT()
        {
            try
            {
                ele = wot.Button_StartTravel();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag)
                    {
                        error = "Cannot click on Start Travel button.";
                    }
                    else { wo.WaitLoading(); Thread.Sleep(3000); }
                }
                else
                {
                    error = "Cannot get button Start Travel.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_072_01_Verify_State_WorkInProgress()
        {
            try
            {
                combobox = wot.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Work In Progress");
                    if (!flag)
                    {
                        error = "State is NOT corect";
                    }
                }
                else
                {
                    error = "State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------
        [Test]
        public void Step_072_02_Verify_Sub_State_None()
        {
            try
            {
                combobox = wot.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("-- None --");
                    if (!flag)
                    {
                        error = "Sub State is NOT corect";
                    }
                }
                else
                {
                    error = "Sub State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_073_01_Update_Actual_work_start()
        {
            try
            {
                flag = wot.Select_Tab("Actual");
                if (flag)
                {
                    datetime = wot.Datetime_ActualTravelStart();
                    temp = datetime.Text;
                    DateTime d = Convert.ToDateTime(temp);
                    actTravelStart = d.AddSeconds(5).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = wot.Datetime_ActualWorkStart();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(actTravelStart, true);
                        if (!flag)
                        {
                            error = "Cannot populate Actual work start value.";
                        }
                    }
                    else error = "Cannot get Actual work start field.";
                }
                else error = "Error when select tab Actual.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_02_Save_WOT()
        {
            try
            {
                flag = wot.Save();
                if (!flag) { error = "Error when save wot."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_074_01_Select_Closure_Information_tab()
        {
            try
            {
                flag = wot.Select_Tab("Closure Information");
                if (!flag)
                { error = "Cannot select tab (Closure Information)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_074_02_Add_CI_Update_Status()
        {
            try
            {
                combobox = wot.Combobox_CI_Update_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("CI Updated");
                    if (!flag)
                    {
                        error = "Can't select item from (CI Update Status) dropdown";
                    }
                }
                else
                {
                    error = "(CI Update Status) dropdown is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_074_03_Add_Close_Notes()
        {
            try
            {
                textarea = wo.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText("Closing notes");
                    if (!flag)
                    {
                        error = "Can't set text for (Close notes) text";
                    }
                }
                else
                {
                    error = "(Close notes) text is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_074_04_Add_Work_Notes()
        {
            try
            {
                flag = wot.Select_Tab("Notes");
                if (flag) 
                {
                    flag = wot.Add_Worknotes("Closing wot");
                    if (!flag)
                    {
                        error = "Can't add work notes";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_074_05_Click_Closed_Complete_button()
        {
            try
            {
                button = wo.Button_CloseComplete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        list.WaitLoading();
                        Thread.Sleep(3000);
                    }
                    else
                    {
                        error = "Can't click Closed Complete button";

                    }
                }
                else
                {
                    error = "Closed Complete button is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        //[Test]
        //public void Step_075_Verify_WOT_State()
        //{
        //    try
        //    {
        //        combobox = wo.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            flag = combobox.VerifyCurrentValue("Closed Complete");
        //            if (!flag)
        //            {
        //                error = "State field is NOT correct. Expect [Closed Complete]. Runtime [" + combobox.Text + "]";
        //            }
        //        }
        //        else
        //        {
        //            error = "State field is NOT existed";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_01_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_076_02_Open_WO()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && WOiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input WO Id.");
                    addPara.ShowDialog();
                    WOiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------------------                
                if (flag)
                {
                    flag = list.Filter("Number;is;" + WOiD);
                    if (flag)
                    {
                        temp = "Number=" + WOiD;
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            wo.WaitLoading();
                        }
                        else { error = "Cannot find WO"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_076_03_Verify_WO_State_ClosedComplete()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Closed Complete");
                    if (!flag)
                    {
                        error = "State field is NOT correct. Expect [Closed Complete]. Runtime [" + combobox.Text + "]";
                    }
                }
                else
                {
                    error = "State field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_01_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_077_02_Open_CTask()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && CTaskID == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Catalog Task Id.");
                    addPara.ShowDialog();
                    CTaskID = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------------------                
                if (flag)
                {
                    flag = list.Filter("Number;is;" + CTaskID);
                    if (flag)
                    {
                        temp = "Number=" + CTaskID;
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            itil.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------
        [Test]
        public void Step_077_03_Verify_CTask_State_ClosedComplete()
        {
            try
            {
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Closed Complete");

                    if (!flag)
                    {
                        error = "State of Catalog Task is NOT correct. Expect [Closed Complete]";
                    }
                }
                else
                {
                    error = "State is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #endregion

        #region Customer track the Service Request
        [Test]
        public void Step_078_ImpersonateUser_Customer()
        {
            try
            {
                string rootuser = Base.GData("UserFullName");
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, true, rootuser, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_ClickOn_CheckStatus()
        {
            try
            {
                ele = sportal.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else
                {
                    error = "Not found My Requests or Check Status on dashboard menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_ClickOn_MyRequests()
        {
            try
            {
                ele = sportal.GLeftMenu("My Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else error = "Not found My Prior Approvals on left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_ClickOn_MyPriorRequests()
        {
            try
            {
                ele = sportal.GLinkByText("My Prior Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag) sportal.WaitLoading();
                }
                else error = "Not found menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_082_Open_Item_With_Stage_Complete()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                flag = sportal.SearchAndOpen(RITMiD, "Number=" + RITMiD + "|Stage=Completed", "Number");
                if (!flag)
                    error = "NOT FOUND request item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------
        [Test]
        public void Step_083_01_Verify_Approval_field()
        {
            try
            {
                combobox = sportal.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("Approved");
                    if (!flag)
                    {
                        error = "Approval field is NOT correct";
                    }
                }
                else
                {
                    error = "Approval field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------
        [Test]
        public void Step_083_02_Verify_Stage_field()
        {
            try
            {
                combobox = sportal.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("Completed");
                    if (!flag)
                    {
                        error = "Stage field is NOT correct";
                    }
                }
                else
                {
                    error = "Stage field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_084_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_085_ImpersonateUser_SupportUser()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("Support_User"));
                if (!flag)
                { error = "Cannot impersonate Support User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_02_Filter_EmailSentToCustomer_HasBeenSubmited()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been submitted|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_03_Open_EmailSentToCustomer_HasBeenSubmited()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been submited).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_04_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_05_Verify_HTML_Body_Note()
        {
            try
            {
                string item = Base.GData("Item");
                temp = item + " has been successfully submitted and is awaiting approval.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_086_06_Verify_HTML_Body_Item()
        //{
        //    try
        //    {
        //        temp = "Item: " + Base.GData("Item");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Requested Item Number. Expect [" + temp + "]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_086_07_Verify_HTML_Body_Description()
        //{
        //    try
        //    {
        //        temp = Base.GData("Item_Description");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Requested Item Description. Expect [" + temp + "]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_08_Verify_HTML_Body_Click_here()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_09_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_02_Filter_EmailSentToCustomer_HasBeenApproved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been approved|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_03_Open_EmailSentToCustomer_HasBeenApproved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been approved).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_04_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_05_Verify_HTML_Body_Content_1()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                temp = "Requested item " + RITMiD + " " + Base.GData("Item_Description") + " has been approved and will be fulfilled.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_06_Verify_HTML_Body_Content_2()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                temp = "To view the item, click here: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_07_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_02_Filter_EmailSentToCustomer_HasBeenCompleted()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been completed|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_03_Open_EmailSentToCustomer_HasBeenCompleted()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been completed).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_04_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_05_Verify_HTML_Body_Content_1()
        {
            try
            {
                temp = "This Requested Item has been completed.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_06_Verify_HTML_Body_Content_2()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_07_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_02_Filter_EmailSentToApprover_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Approver_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;Approval request|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_03_Open_EmailSentToApprover_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (approval request).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_04_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_05_Verify_HTML_Body_Content_1()
        {
            try
            {
                temp = "Please Approve or Reject the request by 1) selecting the appropriate link below AND 2) manually sending the email that is generated. You must send the auto-generated email to complete the processing.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_06_Verify_HTML_Body_Content_2()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                temp = "Click here to approve " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_07_Verify_HTML_Body_Content_3()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                temp = "Click here to reject " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_08_Verify_HTML_Body_Content_4()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                temp = "Click here to view Approval Request: Requested Item: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_09_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_02_Filter_EmailSentToWOTTechnician1_HasBeenAssinged()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && WOTiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO Task Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("WOT_Technician_1_Email");
                temp = "Subject;contains;" + WOTiD + "|and|Subject;contains;has been assigned to you|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_03_Open_EmailSentToWOTTechnician1_HasBeenAssinged()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && WOTiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO Task Id.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + WOTiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been assigned).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_04_Open_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_05_Verify_HTML_Body_Content_1()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && WOTiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Work Order Task.");
                    addPara.ShowDialog();
                    WOTiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------
                temp = WOTiD + " - has been assigned to you - " + WOTiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Requested Item Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_06_Verify_HTML_Body_Content_2()
        {
            try
            {

                temp = "Customer name: " + Base.GData("Customer");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Technician name. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_07_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_02_Filter_EmailSentToAssignmentGroup_HasBeenAssingedToGroup()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && CTaskID == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Catalog Task ID.");
                    addPara.ShowDialog();
                    CTaskID = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                
                temp = "Subject;contains;Catalog Task " + CTaskID + "|and|Subject;contains;has been assigned to group Location Based Services";
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_03_Open_EmailSentToAssignmentGroup_HasBeenAssingedToGroup()
        {
            try
            {
                string conditions = "Subject=@@Location Based Services";
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been assigned to group).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_04_Open_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_05_Verify_HTML_Body_Content_1()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && CTaskID == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Catalog Task ID.");
                    addPara.ShowDialog();
                    CTaskID = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------
                temp = "Click here to view Task: " + CTaskID;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Catalog Task Number. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_06_Verify_HTML_Body_Content_2()
        {
            try
            {

                temp = "Short Description: " + Base.GData("Item_Description");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Short Description. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_07_Verify_HTML_Body_Content_3()
        {
            try
            {

                temp = "Assignment group: Location Based Services";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Assingment Group. Expect [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_08_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_02_Filter_Email_CTask_SentToCustomer()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && CTaskID == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Catalog Task ID.");
                    addPara.ShowDialog();
                    CTaskID = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string customer_email = Base.GData("Customer_Email");
                temp = "Subject;contains;Catalog Task " + CTaskID + "|and|Recipients;contains;" + customer_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_03_Verify_NO_Email_CTask_SentTo_Customer()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && CTaskID == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Catalog Task ID.");
                    addPara.ShowDialog();
                    CTaskID = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                 //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + CTaskID;
                flag = emailList.VerifyRow(conditions, true);
                if (!flag) error = "Found email sent to customer. Expected NOT found.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_093_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_093_02_Filter_Email_Request_SentToCustomer()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && REQiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Request ID.");
                    addPara.ShowDialog();
                    REQiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------
                string customer_email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + REQiD + "|and|Recipients;contains;" + customer_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_093_03_Verify_NO_Email_Request_SentTo_Customer()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && REQiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Request ID.");
                    addPara.ShowDialog();
                    REQiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + REQiD;
                flag = emailList.VerifyRow(conditions, true);
                if (!flag) error = "Found email sent to customer. Expected NOT found.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_094_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}