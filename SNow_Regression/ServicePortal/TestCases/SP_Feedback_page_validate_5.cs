﻿using SNow;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using Auto;

namespace ServicePortal
{
    [TestFixture]
    public class SP_Feedback_page_validate_5
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            
            System.Console.WriteLine("Finished - Current date tiem: " + currentDateTime);
            
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        
        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snoelement ele;
        snodatetime datetime;
        //------------------------------------------------------------------
        Login login;
        Home home;
        SPortal sportal;
        Itil itil;
        //------------------------------------------------------------------        
        string currentDateTime;
        string descriptionItem;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                sportal = new SPortal(Base, "Portal Home");
                itil = new Itil(Base, "Itil");
                //------------------------------------------------------------------                
                currentDateTime = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer");
                
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Verify_Customer_FullName()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                snoelement ele = sportal.UserFullName();
                flag = ele.Existed;
                if (flag)
                {
                    Console.WriteLine("Run time:(" + ele.MyText.Trim() + ")");
                    if (ele.MyText.Trim().ToLower() != temp.Trim().ToLower())
                    {
                        flag = false; error = "Invalid customer name.";
                    }
                }
                else error = "Cannot get control user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Verify_Header_Menu()
        {
            try
            {
                string temp = Base.GData("Header_Menu");
                flag = sportal.Verify_Header_Menu(temp);
                if (!flag)
                    error = "Invalid header menu items.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Verify_Footer()
        {
            try
            {
                string temp = Base.GData("Footer");
                flag = sportal.Verify_Footer(temp);
                if (!flag)
                    error = "Invalid footer.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_ClickOn_HelpUsImproveThisSite_Link()
        {
            try
            {
                snoelement ele = sportal.GLinkByText("Help Us Improve this Site");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Surveys;Take survey - Portal Satisfaction Feedback");
                        if (!flag)
                            error = "Cannot open page [Feedback]";
                        else
                            sportal.WaitLoading();
                    }
                    else
                        error = "Error when click on link [Help Us].";
                }
                else error = "Not found link [Help Us].";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Verify_Feedback_Page_Title()
        {
            try
            {
                ele = sportal.FeedbackPageTitle();
                flag = ele.Existed;
                if (!flag)
                    error = "Not found title.";
                else
                {
                    if (ele.MyText.Trim().ToLower() != "portal satisfaction feedback")
                    {
                        flag = false;
                        error = "Invalid title of Feedback page.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Click_Get_Started()
        {
            try
            {
                button = sportal.Button_GetStarted();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        sportal.WaitLoading();
                    else
                        error = "Error when click on button.";
                }
                else error = "Cannot get button Get Started.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Populate_Rating()
        {
            try
            {
                string temp = Base.GData("Rating");
                combobox = sportal.Combobox_Rating();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_SelectItem(temp);
                    if (!flag)
                        error = "Cannot populate rating.";
                }
                else
                    error = "Cannot get combobox Rating.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Populate_Comments()
        {
            try
            {
                currentDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                string temp = Base.GData("Rating");
                temp = currentDateTime + "-Test " + temp;
                textarea = sportal.Textarea_Comments();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                        error = "Cannot populate comments.";
                }
                else
                    error = "Cannot get textarea comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_009_Verify_Browse_Catalog_Left_Menu_Items()
        //{
        //    try
        //    {
        //        Thread.Sleep(5000);
        //        string temp = Base.GData("Catalog_Left_Menu_Items");
        //        if (temp.ToLower() != "no" && temp != string.Empty)
        //        {
        //            flag = sportal.Verify_Catalog_Left_Menu_Items(temp);
        //            if (!flag)
        //                error = "Invalid catalog left menu items.";
        //        }
        //        else Console.WriteLine("No verify.");
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_010_ClickOn_Catalog_Link()
        //{
        //    try
        //    {
        //        snoelement ele = sportal.GHeaderMenu("Catalog");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag)
        //            {
        //                sportal.WaitLoading();
        //                flag = sportal.Verify_Page_Open("Home;Catalog");
        //                if (!flag)
        //                    error = "Cannot open page [Catalog]";
        //            }
        //            else
        //                error = "Error when click on menu [Catalog].";
        //        }
        //        else error = "Not found [Catalog] in header menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_011_Select_Catalog_Item()
        //{
        //    try
        //    {

        //        string item = Base.GData("Catalog_Item");

        //        flag = sportal.Search_Open_CatalogItem(item);

        //        if (!flag)
        //            error = "Cannot select catalog item.";
        //        else
        //        {
        //            sportal.WaitLoading();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_01_Verify_UserName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_FullName");
        //        lookup = sportal.Lookup_UserName();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {

        //            flag = lookup.SP_VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get lookup user name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_04_Verify_Email_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_Email");
        //        textbox = sportal.Textbox_EmailAddress();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid email value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox email.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_06_Verify_Phone_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_Phone");
        //        textbox = sportal.Textbox_UserTelephoneNumber();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user phone value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user phone.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_07_Verify_Location_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer1_Location");
        //        lookup = sportal.Lookup_UserLocation();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {

        //            flag = lookup.SP_VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user location value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get lookup user location.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_08_Verify_Button_AddToCart_And_OrderNow_Visible()
        //{
        //    try
        //    {
        //        button = sportal.Button_Submit();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            button = sportal.Button_AddToCart();
        //            flag = button.Existed;
        //            if (!flag)
        //                error = "Button Add To Cart is NOT visible.";
        //        }
        //        else error = "Button Order Now is NOT visible.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_013_Populate_Customer2()
        //{
        //    try
        //    {
        //        lookup = sportal.Lookup_UserName();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {
        //            string temp = Base.GData("Customer2");
        //            flag = lookup.SP_Select(temp);
        //            if (!flag)
        //            {
        //                error = "Cannot populate Customer 2 value.";
        //            }
        //        }
        //        else { error = "Cannot get User name lookup."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_01_Verify_UserName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_FullName");
        //        lookup = sportal.Lookup_UserName();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {

        //            flag = lookup.SP_VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get lookup user name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_04_Verify_Email_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Email");
        //        textbox = sportal.Textbox_EmailAddress();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid email value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox email.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_06_Verify_Phone_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Phone");
        //        textbox = sportal.Textbox_UserTelephoneNumber();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user phone value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user phone.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_07_Verify_Location_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Location");
        //        lookup = sportal.Lookup_UserLocation();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {

        //            flag = lookup.SP_VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user location value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get lookup user location.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_015_Input_MandatoryFields()
        //{
        //    try
        //    {
        //        flag = sportal.Input_Mandatory_Fields("auto");
        //        if (!flag)
        //        {
        //            error = "Have error when auto fill mandatory fields.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_OrderItem()
        //{
        //    try
        //    {
        //        button = sportal.Button_Submit();
        //        if (button.Existed)
        //        {
        //            flag = button.Click(true);
        //            if (flag)
        //            {
        //                sportal.WaitLoading();        
        //            }
        //            else { error = "Cannot click button Order Now"; }
        //        }
        //        else
        //        {
        //            flag = false;
        //            error = "Order Now button does NOT exist";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_017_01_Get_Request_Number()
        //{
        //    try
        //    {
        //        ele = sportal.TKP_Request_Number();

        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            REQiD = ele.MyText.Trim();
        //        }
        //        else error = "Cannot get elemnet request number.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_017_02_Get_Request_Item_Number()
        //{
        //    try
        //    {
        //        ele = sportal.TKP_Request_Item_Number();

        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            RITMiD = ele.MyText.Trim();
        //        }
        //        else error = "Cannot get elemnet request item number.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //////-------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_018_01_Validate_Thank_You_Message()
        ////{
        ////    try
        ////    {
        ////        ele = sportal.ThankYouMessage();
        ////        flag = ele.Existed;
        ////        if (flag)
        ////        {
        ////            if (!ele.MyText.Trim().Contains("REQ"))
        ////            {
        ////                flag = false;
        ////                flagExit = false;
        ////            }
        ////        }
        ////        if (!flag)
        ////        {
        ////            flagExit = false;
        ////            error = "Invalid message.";
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        flag = false;
        ////        error = ex.Message;
        ////    }
        ////}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_018_02_Validate_Item_Stage()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Item_Stage_1");

        //        flag = sportal.TKP_Validate_ItemStage(temp);

        //        if (!flag)
        //        {
        //            error = "Invalid stage or stage is not in blue color";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------      
        //[Test]
        //public void Step_019_Goto_Login()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);
        //        Base.Driver.Navigate().GoToUrl(temp);

        //        login.WaitLoading();

        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");

        //        flag = login.LoginToSystem(user, pwd);

        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------     
        //[Test]
        //public void Step_020_ImpersonateUser_Customer2()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");

        //        flag = home.ImpersonateUser(temp, false, null, true);
        //        if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
        //        else sportal.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_021_ClickOn_CheckStatus()
        //{
        //    try
        //    {
        //        ele = sportal.GHeaderMenu("Check Status");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Not found My Requests or Check Status on dashboard menu";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_022_Verify_Check_Status_Left_Menu_Items()
        //{
        //    try
        //    {
        //        Thread.Sleep(10000);
        //        string temp = Base.GData("Check_Status_Left_Menu_Items");
        //        if (temp.ToLower() != "no" && temp != string.Empty)
        //        {
        //            flag = sportal.Verify_Check_Status_Left_Menu(temp);
        //            if (!flag)
        //                error = "Invalid check status left menu items.";
        //        }
        //        else Console.WriteLine("No verify.");
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}     
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_023_01_ClickOn_MyRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLeftMenu("My Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else error = "Not found My Requests on left menu";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_023_02_Verify_MyRequests_Tab_Menu_Items()
        //{
        //    try
        //    {
        //        Thread.Sleep(5000);
        //        string temp = Base.GData("MyRequests_Tab_Menu_Items");
        //        if (temp.ToLower() != "no" && temp != string.Empty)
        //        {
        //            flag = sportal.Verify_Check_Status_Tab_Menu(temp);
        //            if (!flag)
        //                error = "Invalid my requests tab menu items.";
        //        }
        //        else Console.WriteLine("No verify.");
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}  
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_01_ClickOn_MyActiveRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("My Active Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag) sportal.WaitLoading();
        //        }
        //        else error = "Not found menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_02_Verify_Stage_Completed_NotFound()
        //{
        //    try
        //    {
        //        string condition = "Stage=Completed";

        //        flag = sportal.SearchAndVerifyRow("Completed", condition, true);

        //        if (flag)
        //        {
        //            System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
        //        }
        //        else
        //        {
        //            error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_03_Verify_Stage_RequestCancelled_NotFound()
        //{
        //    try
        //    {
        //        string condition = "Stage=Request Cancelled";
        //        flag = sportal.SearchAndVerifyRow("Request Cancelled", condition, true);

        //        if (flag)
        //        {
        //            System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
        //        }
        //        else
        //        {
        //            error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_04_ClickOn_MyPriorRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("My Prior Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag) sportal.WaitLoading();
        //        }
        //        else error = "Not found menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_05_Verify_Stage_Fulfillment_NotFound()
        //{
        //    try
        //    {
        //        string condition = "Stage=Fulfillment";

        //        flag = sportal.SearchAndVerifyRow("Fulfillment", condition, true);

        //        if (flag)
        //        {
        //            System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
        //        }
        //        else
        //        {
        //            error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_06_Verify_Stage_RequestedForApproval_NotFound()
        //{
        //    try
        //    {
        //        string condition = "Stage=Requested For Approval";

        //        flag = sportal.SearchAndVerifyRow("Requested For Approval", condition, true);

        //        if (flag)
        //        {
        //            System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
        //        }
        //        else
        //        {
        //            error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_07_Verify_Stage_WaitingForApproval_NotFound()
        //{
        //    try
        //    {
        //        string condition = "Stage=Waiting for Approval";

        //        flag = sportal.SearchAndVerifyRow("Waiting for Approval", condition, true);

        //        if (flag)
        //        {
        //            System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
        //        }
        //        else
        //        {
        //            error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_025_01_ClickOn_MyActiveRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("My Active Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag) sportal.WaitLoading();
        //        }
        //        else error = "Not found menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_025_02_Open_Request_Item()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        flag = sportal.SearchAndOpen(RITMiD, "Number=" + RITMiD, "Number");
        //        if (flag)
        //            sportal.WaitLoading();
        //        else error = "Cannot open request item.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_01_Verify_UserName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_FullName");
        //        lookup = sportal.Lookup_UserName();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {

        //            flag = lookup.SP_VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get lookup user name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_04_Verify_Email_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Email");
        //        textbox = sportal.Textbox_EmailAddress();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid email value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox email.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_06_Verify_Phone_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Phone");
        //        textbox = sportal.Textbox_UserTelephoneNumber();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user phone value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user phone.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_07_Verify_Location_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Location");
        //        lookup = sportal.Lookup_UserLocation();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {

        //            flag = lookup.SP_VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user location value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get lookup user location.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_08_Verify_Number_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        textbox = sportal.Textbox_Number();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(RITMiD, true);
        //            if (!flag)
        //            {
        //                error = "Invalid number value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox number.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_09_Verify_Button_CancelRequest_And_Save_Visible()
        //{
        //    try
        //    {
        //        button = sportal.Button_CancelRequest();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            button = sportal.Button_Save();
        //            flag = button.Existed;
        //            if (!flag)
        //                error = "Button Save is NOT visible.";
        //        }
        //        else error = "Button Cancel Request is NOT visible.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_027_Verify_Stage_RequestedForApproval()
        //{
        //    try
        //    {
        //        combobox = sportal.Combobox_Stage();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            flag = combobox.SP_VerifyCurrentValue("Requested For Approval");
        //            if (!flag)
        //                error = "Invalid stage.";
        //        }
        //        else error = "Cannot get combobox stage.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_028_Verify_Related_Link()
        //{
        //    try
        //    {
        //        string exp = Base.GData("Item_Related_Link");
        //        flag = sportal.Verify_Related_Link(exp);
        //        if (!flag)
        //        {
        //            error = "Invalid related link.";
        //            flagExit = false;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_029_ClickOn_Approvals_Link()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("@@Approvals");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (!flag)
        //                error = "Error when click on approvals link.";
        //            else
        //                sportal.WaitLoading();
        //        }
        //        else error = "Cannot get link approvals.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_030_Verify_Approvals_List()
        //{
        //    try
        //    {
        //        string approverList = Base.GData("Approver_List");
        //        string state = "Requested";
        //        string requestedFor = Base.GData("Customer2");

        //        string[] array = null;

        //        if (approverList.Contains("|"))
        //            array = approverList.Split('|');
        //        else
        //            array = new string[] { approverList };

        //        foreach (string apr in array) 
        //        {
        //            bool flagF = true;
        //            string conditions = "State=" + state + "|Requested For=" + requestedFor + "|Approver=" + apr;
        //            flagF = sportal.VerifyRow(conditions);
        //            if (!flagF) 
        //            {
        //                if (flag)
        //                    flag = false;
        //                Console.WriteLine("NOT FOUND row:" + conditions);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------      
        //[Test]
        //public void Step_031_Goto_Login()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);
        //        Base.Driver.Navigate().GoToUrl(temp);

        //        login.WaitLoading();

        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");

        //        flag = login.LoginToSystem(user, pwd);

        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------     
        //[Test]
        //public void Step_032_ImpersonateUser_Approver()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Approver");

        //        flag = home.ImpersonateUser(temp, false, null, true);
        //        if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
        //        else sportal.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_033_ClickOn_Approvals()
        //{
        //    try
        //    {
        //        ele = sportal.GHeaderMenu("Approvals");
        //        flag = ele.Existed; 
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (!flag)
        //                error = "Error when click on approvals.";
        //        }
        //        else
        //        {
        //            error = "Not found My Approvals on dashboard menu.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_034_Open_Item()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------------------
        //        Thread.Sleep(1000);
        //        flag = sportal.Select_Approvals_Item(RITMiD);
        //        if (!flag)
        //            error = "Cannot open item.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_01_Verify_Approval_Form_UserName()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        flag = sportal.Verify_Approval_Form_Field("User name:", temp);
        //        if (!flag)
        //            error = "Invalid user name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_02_Verify_Approval_Form_FirstName()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_FirstName");
        //        flag = sportal.Verify_Approval_Form_Field("First Name:", temp);
        //        if (!flag)
        //            error = "Invalid first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_03_Verify_Approval_Form_LastName()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_LastName");
        //        flag = sportal.Verify_Approval_Form_Field("Last Name:", temp);
        //        if (!flag)
        //            error = "Invalid last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_04_Verify_Approval_Form_Email()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Email");
        //        flag = sportal.Verify_Approval_Form_Field("Email Address:", temp);
        //        if (!flag)
        //            error = "Invalid email.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_05_Verify_Approval_Form_UserID()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_UserID");
        //        flag = sportal.Verify_Approval_Form_Field("User ID:", temp);
        //        if (!flag)
        //            error = "Invalid user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_06_Verify_Approval_Form_Location()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2_Location");
        //        flag = sportal.Verify_Approval_Form_Field("User Location:", temp);
        //        if (!flag)
        //            error = "Invalid location.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_036_Verify_State_Requested()
        //{
        //    try
        //    {
        //        string temp = "Requested";
        //        flag = sportal.Verify_Approval_Form_Field("State", temp);
        //        if (!flag)
        //            error = "Invalid state.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_037_Add_Approval_Comment()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------------------
        //        textarea = sportal.Textarea_Approval_Active_Stream_Comment();
        //        flag = textarea.Existed;
        //        if (flag)
        //        {
        //            flag = textarea.SetText(RITMiD);
        //            if (flag)
        //            {
        //                button = sportal.Button_Send();
        //                flag = button.Existed;
        //                if (flag)
        //                {
        //                    flag = button.Click();
        //                    if (!flag)
        //                        error = "Error when click on send button.";
        //                }
        //                else error = "Cannot get button send.";
        //            }
        //            else error = "Cannot populate comment.";
        //        }
        //        else error = "Cannot get textarea comment.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_038_ClickOn_Button_Approve()
        //{
        //    try
        //    {
        //        button = sportal.Button_Approve();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            flag = button.Click(true);
        //            if (flag)
        //                sportal.WaitLoading();
        //            else
        //                error = "Error when click approve button.";
        //        }
        //        else error = "Cannot get button approve.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_039_Verify_State_Approved()
        //{
        //    try
        //    {
        //        string temp = "Approved";
        //        flag = sportal.Verify_Approval_Form_Field("State", temp);
        //        if (!flag)
        //            error = "Invalid state.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_040_ClickOn_CheckStatus()
        //{
        //    try
        //    {
        //        ele = sportal.GHeaderMenu("Check Status");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Not found My Requests or Check Status on dashboard menu";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_041_01_ClickOn_MyPriorApprovals()
        //{
        //    try
        //    {
        //        ele = sportal.GLeftMenu("My Prior Approvals");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else error = "Not found My Prior Approvals on left menu";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_041_02_Verify_MyPriorApprovals_Tab_Menu_Items()
        //{
        //    try
        //    {
        //        Thread.Sleep(5000);
        //        string temp = Base.GData("MyPriorApprovals_Tab_Menu_Items");
        //        if (temp.ToLower() != "no" && temp != string.Empty)
        //        {
        //            flag = sportal.Verify_Check_Status_Tab_Menu(temp);
        //            if (!flag)
        //                error = "Invalid my requests tab menu items.";
        //        }
        //        else Console.WriteLine("No verify.");
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_042_Verify_Found_Item_In_My_Prior_Approvals()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        flag = sportal.SearchAndVerifyRow(RITMiD, "State=Approved|Comments=@@" + RITMiD);
        //        if (!flag)
        //          error = "NOT FOUND request item.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------      
        //[Test]
        //public void Step_043_01_Goto_Login()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);
        //        Base.Driver.Navigate().GoToUrl(temp);

        //        login.WaitLoading();

        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");

        //        flag = login.LoginToSystem(user, pwd);

        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------     
        //[Test]
        //public void Step_043_02_ImpersonateUser_Customer2()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");

        //        flag = home.ImpersonateUser(temp, false, null, true);
        //        if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
        //        else sportal.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_044_ClickOn_CheckStatus()
        //{
        //    try
        //    {
        //        ele = sportal.GHeaderMenu("Check Status");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Not found My Requests or Check Status on dashboard menu";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_045_01_ClickOn_MyRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLeftMenu("My Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else error = "Not found My Prior Approvals on left menu";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_045_02_ClickOn_MyActiveRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("My Active Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag) sportal.WaitLoading();
        //        }
        //        else error = "Not found menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_046_Verify_Item_With_Stage_Fulfilment()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        flag = sportal.SearchAndVerifyRow(RITMiD, "Number=" + RITMiD + "|Stage=Fulfilment");
        //        if (!flag)
        //            error = "NOT FOUND request item.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------      
        //[Test]
        //public void Step_047_01_Goto_Login()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);
        //        Base.Driver.Navigate().GoToUrl(temp);

        //        login.WaitLoading();

        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");

        //        flag = login.LoginToSystem(user, pwd);

        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------     
        //[Test]
        //public void Step_047_02_ImpersonateUser_SupportUser()
        //{
        //    try
        //    {
        //        flag = home.ImpersonateUser(Base.GData("Support_User"));
        //        if (!flag)
        //        { error = "Cannot impersonate Support User"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_048_GlobalSearch_RequestOrder()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (REQiD == null || REQiD == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Request ID.");
        //            addPara.ShowDialog();
        //            REQiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------                      
        //        flag = itil.GlobalSearchItem(REQiD, true);
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Cannot search Request Order via Global Search field "; }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_049_Open_Requested_Item()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        string conditions = "Number=" + RITMiD;
        //        flag = itil.RelatedTableOpenRecord("Requested Items", conditions, "Number");
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Error when open request item."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_050_Open_CatalogTask()
        //{
        //    try
        //    {
        //        string conditions = "Number=@@TASK";
        //        flag = itil.RelatedTableOpenRecord("Catalog Tasks", conditions, "Number");
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Error when open request item."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_051_01_Populate_CatalogTask_State_To_Pending()
        //{
        //    try
        //    {
        //        combobox = itil.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            string temp = "Pending";
        //            flag = combobox.SelectItem(temp);
        //            if (!flag) { error = "Cannot populate State value."; }
        //            else 
        //            {
        //                button = itil.Button_Yes();
        //                flag = button.Existed;
        //                if (flag)
        //                {
        //                    flag = button.Click();
        //                    if (!flag)
        //                        error = "Error when click on button Yes";
        //                    else 
        //                    {
        //                        datetime = itil.Datetime_Followupdate();
        //                        flag = datetime.Existed;
        //                        if (flag)
        //                        {
        //                            string d = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
        //                            flag = datetime.SetText(d);
        //                            if (!flag)
        //                                error = "Cannot populate follow up date.";
        //                        }
        //                        else
        //                            error = "Cannot get datetime follow up date.";
        //                    }
        //                }
        //                else error = "Cannot get button Yes.";
        //            }
        //        }
        //        else { error = "Cannot get State combobox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_051_02_Update_CatalogTask()
        //{
        //    try
        //    {
        //        flag = itil.Update();
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Unable to save the task"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_051_03_Verify_State_Pending()
        //{
        //    try
        //    {
        //        combobox = itil.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            string temp = "Pending";
        //            flag = combobox.VerifyCurrentValue(temp);
        //            if (!flag)
        //            {
        //                error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
        //                flagExit = false;
        //            }
        //        }
        //        else { error = "Cannot get State combobox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_052_Open_CatalogTask()
        //{
        //    try
        //    {
        //        string conditions = "Number=@@TASK";
        //        flag = itil.RelatedTableOpenRecord("Catalog Tasks", conditions, "Number");
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Error when open request item."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_053_01_Populate_CatalogTask_State_To_WorkInProgress()
        //{
        //    try
        //    {
        //        combobox = itil.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            string temp = "Work in Progress";
        //            flag = combobox.SelectItem(temp);
        //            if (!flag) { error = "Cannot populate State value."; }
        //        }
        //        else { error = "Cannot get State combobox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_053_02_Update_CatalogTask()
        //{
        //    try
        //    {
        //        flag = itil.Update();
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Unable to save the task"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_053_03_Verify_State_WorkInProgress()
        //{
        //    try
        //    {
        //        combobox = itil.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            string temp = "Work in Progress";
        //            flag = combobox.VerifyCurrentValue(temp);
        //            if (!flag)
        //            {
        //                error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
        //                flagExit = false;
        //            }
        //        }
        //        else { error = "Cannot get State combobox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_054_Open_CatalogTask()
        //{
        //    try
        //    {
        //        string conditions = "Number=@@TASK";
        //        flag = itil.RelatedTableOpenRecord("Catalog Tasks", conditions, "Number");
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Error when open request item."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_055_01_Populate_CatalogTask_State_To_ClosedComplete()
        //{
        //    try
        //    {
        //        combobox = itil.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            string temp = "Closed Complete";
        //            flag = combobox.SelectItem(temp);
        //            if (!flag) { error = "Cannot populate State value."; }
        //        }
        //        else { error = "Cannot get State combobox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_055_02_Update_CatalogTask()
        //{
        //    try
        //    {
        //        flag = itil.Update();
        //        if (flag)
        //        {
        //            itil.WaitLoading();
        //        }
        //        else { error = "Unable to save the task"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_055_03_Verify_State_ClosedComplete()
        //{
        //    try
        //    {
        //        combobox = itil.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            string temp = "Closed Complete";
        //            flag = combobox.VerifyCurrentValue(temp);
        //            if (!flag)
        //            {
        //                error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
        //                flagExit = false;
        //            }
        //        }
        //        else { error = "Cannot get State combobox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------     
        //[Test]
        //public void Step_056_ImpersonateUser_Customer2()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        string user = Base.GData("UserFullName");
        //        flag = home.ImpersonateUser(temp, true, user, true);
        //        if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
        //        else sportal.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_057_ClickOn_CheckStatus()
        //{
        //    try
        //    {
        //        ele = sportal.GHeaderMenu("Check Status");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Not found My Requests or Check Status on dashboard menu";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_058_01_ClickOn_MyRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLeftMenu("My Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            sportal.WaitLoading();
        //        }
        //        else error = "Not found My Prior Approvals on left menu";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_058_02_ClickOn_MyActiveRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("My Active Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag) sportal.WaitLoading();
        //        }
        //        else error = "Not found menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_059_Verify_Not_Found_Item_InList()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        flag = sportal.SearchAndVerifyRow(RITMiD, "Number=" + RITMiD, true);
        //        if (!flag)
        //            error = "FOUND request item. Expected NOT FOUND.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_060_ClickOn_MyPriorRequests()
        //{
        //    try
        //    {
        //        ele = sportal.GLinkByText("My Prior Requests");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click();
        //            if (flag) sportal.WaitLoading();
        //        }
        //        else error = "Not found menu.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_061_Verify_Found_Item_With_Stage_Completed()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //------------------------------------------------------------------------------
        //        flag = sportal.SearchAndVerifyRow(RITMiD, "Number=" + RITMiD + "|Stage=Completed");
        //        if (!flag)
        //            error = "NOT FOUND request item";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_062_Logout()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);
        //        Base.Driver.Navigate().GoToUrl(temp);
        //        Thread.Sleep(2000);
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //***********************************************************************************************************************************

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
