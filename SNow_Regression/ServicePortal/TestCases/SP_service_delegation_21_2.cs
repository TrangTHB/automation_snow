﻿using SNow;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using Auto;
namespace ServicePortal
{
    [TestFixture]
    public class SP_service_delegation_21_2
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Request Id: " + REQiD);
            System.Console.WriteLine("Finished - Requested Item Id: " + RITMiD);
            System.Console.WriteLine("Finished - Catalog Task: " + CTaskiD);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snoelement ele;
        snodatetime datetime;
        snocheckbox checkbox;
        
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Itil itil = null;
        ItilList list = null;
        SPortal sportal = null;
        EmailList emailList = null;
        Email email = null;
        
        //------------------------------------------------------------------
        string REQiD, RITMiD, CTaskiD;
        string isAutoApprove;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                itil = new Itil(Base, "Itil");
                sportal = new SPortal(Base, "Service Portal");
                list = new ItilList(Base, "List");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                
                //------------------------------------------------------------------
                REQiD = string.Empty;
                RITMiD = string.Empty;
                CTaskiD = string.Empty;
                isAutoApprove = Base.GData("IsAutoApprove").Trim().ToLower();
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_003_ImpersonateUser_Manager()
        {
            try
            {
                temp = Base.GData("Manager");
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else
                {
                    temp = Base.GData("Manager_Role");
                    if (temp.Trim().ToLower() == "itil")
                    {
                        itil.WaitLoading();
                        flag = home.SwitchToPortal();
                    }
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Open_Profile()
        {
            try
            {
                flag = sportal.User_Profile_link_Click("Profile");
                if (!flag)
                {
                    error = "Error when open profile.";
                }
                else sportal.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_006_Verify_Existing_Delegates()
        //{
        //    try
        //    {
        //        temp = Base.GData("Delegate_User");
        //        bool flagV = sportal.VerifyRow("Delegate=" + temp, true);
        //        if (!flagV) isUpdateDelegate = "yes";
        //        else isUpdateDelegate = "no";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Click_On_New_Delegate()
        {
            try
            {
                button = sportal.Button_New();
                flag = button.Existed;
                if (flag)
                    button.Click();
                else error = "Cannot get button New.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_Validate_Prepopulated_User_field()
        {
            try
            {
                temp = Base.GData("Manager");
                lookup = sportal.Lookup_Delegate_User();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SP_VerifyCurrentValue(temp);
                }
                else error = "Cannot get lookup User.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_Validate_Approvals_Checkbox_Checked()
        {
            try
            {
                checkbox = sportal.Checkbox_Approvals();
                flag = checkbox.SP_Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "Approvals checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_03_Validate_Assignments_Checkbox_Checked()
        {
            try
            {
                checkbox = sportal.Checkbox_Assignments();
                flag = checkbox.SP_Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "Assignments checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_04_Validate_CCNotifications_Checkbox_Checked()
        {
            try
            {
                checkbox = sportal.Checkbox_CCNotifications();
                flag = checkbox.SP_Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "CC notifications checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_05_Validate_MeetingInvitations_Checkbox_Checked()
        {
            try
            {
                checkbox = sportal.Checkbox_MeetingInvitations();
                flag = checkbox.SP_Checked;
                if (!flag)
                {
                    flagExit = false;
                    error += "Meeting invitations checkbox is NOT checked by default";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Populate_Delegate_User()
        {
            try
            {
                temp = Base.GData("Delegate_User");
                lookup = sportal.Lookup_Delegate();
                if (lookup.Existed)
                {
                    flag = lookup.SP_Select(temp);
                    if (flag == false)
                    { error = "Cannot select Delegate user."; }
                }
                else
                {
                    flag = false;
                    error = "Cannot get Delegate lookup.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Populate_Start_End_Time()
        {
            try
            {
                //Get current Delegate start time and parse to datetime
                datetime = sportal.Datetime_Delegate_Starts();
                temp = datetime.Text;
                var startTime = DateTime.Parse(temp);

                //Get current Delegate end time and parse to datetime
                datetime = sportal.Datetime_Delegate_Ends();
                temp = datetime.Text;
                var endTime = DateTime.Parse(temp);
                string d_startTime = startTime.ToString("yyyy-MM-dd HH:mm:ss");
                string d_endTime = startTime.AddMinutes(120).ToString("yyyy-MM-dd HH:mm:ss");
                Console.WriteLine("///- Starts:" + d_startTime);
                Console.WriteLine("///- Ends:" + d_endTime);
                //if (isUpdateDelegate.ToLower() == "no")
                //{
                //    //Set Delegate end time = start time + 120 minutes
                //    if (startTime < endTime)
                //    {                    
                //        flag = datetime.SetText(d_endTime);
                //        if (!flag)
                //        {
                //            error = "Unable to populate Delegate End Time";
                //        }
                //    }
                //}
                //else
                //{
                //    Base.Driver.Navigate().Back();
                //    sportal.WaitLoading();
                //    temp = Base.GData("Delegate_User");
                //    flag = sportal.Open("Delegate=" + temp, "Starts");
                //    if (flag)
                //    {
                //        sportal.WaitLoading();
                //        datetime = sportal.Datetime_Delegate_Starts();
                //        flag = datetime.Existed;
                //        if (flag)
                //        {
                //            flag = datetime.SetText(d_startTime);
                //            if (flag)
                //            {
                //                datetime = sportal.Datetime_Delegate_Ends();
                //                flag = datetime.Existed;
                //                if (flag)
                //                {
                //                    flag = datetime.SetText(d_endTime);
                //                    if(!flag)
                //                        error = "Unable to populate Delegate End Time";
                //                }
                //                else error = "Cannot get datetime Ends.";
                //            }
                //            else error = "Unable to populate Delegate Start Time";
                //        }
                //        else error = "Cannot get datetime Starts.";
                //    }                   
                //    else
                //        error = "Cannot open delegate.";
                //}
                flag = datetime.SetText(d_endTime);
                if (!flag)
                {
                    error = "Unable to populate Delegate End Time";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Uncheck_Checkbox_Assignment_MeetingInvitations()
        {
            try
            {
                checkbox = sportal.Checkbox_MeetingInvitations();
                flag = checkbox.SP_Checked;
                if (flag)
                {
                    flag = checkbox.Click();
                }

                checkbox = sportal.Checkbox_Assignments();
                flag = checkbox.SP_Checked;
                if (flag)
                {
                    flag = checkbox.Click();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Save_Delegate()
        {
            try
            {
                button = sportal.Button_Save();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else
                    {
                        error = "Can't click Save button";
                    }
                }
                else
                {
                    error = "Submit save is NOT existed";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_012_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Impersonate_User_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else
                {
                    temp = Base.GData("Customer_Role");
                    if (temp.Trim().ToLower() == "itil")
                    {
                        itil.WaitLoading();
                        flag = home.SwitchToPortal();
                    }
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_ClickOn_Catalog_Link()
        {
            try
            {
                snoelement ele = sportal.GHeaderMenu("Catalog");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sportal.WaitLoading();
                        flag = sportal.Verify_Page_Open("Home;Catalog");
                        if (!flag)
                            error = "Cannot open page [Catalog]";
                    }
                    else
                        error = "Error when click on menu [Catalog].";
                }
                else error = "Not found [Catalog] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Select_Catalog_Item()
        {
            try
            {

                string item = Base.GData("Item");

                flag = sportal.Search_Open_CatalogItem(item);

                if (!flag)
                    error = "Cannot select catalog item.";
                else
                {
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_01_Verify_UserName_Field()
        {
            try
            {
                string temp = Base.GData("Customer");
                lookup = sportal.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user name value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_FirstName");
        //        textbox = sportal.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid first name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_03_Verify_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_LastName");
        //        textbox = sportal.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid last name value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_04_Verify_Email_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Email");
                textbox = sportal.Textbox_EmailAddress();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid email value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer_UserID");
        //        textbox = sportal.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {

        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                error = "Invalid user id value.";
        //                flagExit = false;
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_06_Verify_Phone_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Phone");
                textbox = sportal.Textbox_TelephoneNumber();
                flag = textbox.Existed;
                if (flag)
                {

                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user phone value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox user phone.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_07_Verify_Location_Field()
        {
            try
            {
                string temp = Base.GData("Customer_Location");
                lookup = sportal.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid user location value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup user location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_08_Change_Customer2_IfNeed()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    lookup = sportal.Lookup_RequestedFor();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.SP_Select(temp);
                        if (!flag)
                            error = "Error when select value.";
                    }
                    else error = "Cannot get lookup user name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_09_Verify_Cus2_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        if (temp.ToLower() != "no" && temp != "" && temp != null)
        //        {
        //            textbox = sportal.Textbox_FirstName();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Customer2_FirstName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag) error = "Invalid user first name value.";
        //            }
        //            else error = "Cannot get textbox first name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_10_Verify_Cus2_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        if (temp.ToLower() != "no" && temp != "" && temp != null)
        //        {
        //            textbox = sportal.Textbox_LastName();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Customer2_LastName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag) error = "Invalid user last name value.";
        //            }
        //            else error = "Cannot get textbox last name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_11_Verify_Cus2_Email_Field()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    textbox = sportal.Textbox_EmailAddress();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Customer2_Email");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user email value.";
                    }
                    else error = "Cannot get textbox email.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_12_Verify_Cus2_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        if (temp.ToLower() != "no" && temp != "" && temp != null)
        //        {
        //            textbox = sportal.Textbox_UserID();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Customer2_UserID");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag) error = "Invalid user ID value.";
        //            }
        //            else error = "Cannot get textbox User ID.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_13_Verify_Cus2_Phone_Field()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    textbox = sportal.Textbox_TelephoneNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Customer2_Phone");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user phone value.";
                    }
                    else error = "Cannot get textbox Phone.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_14_Verify_Cus2_Location_Field()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    temp = Base.GData("Customer2_Location");
                    lookup = sportal.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.SP_VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user location value.";
                    }
                    else error = "Cannot get lookup location.";

                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Input_MandatoryFields()
        {
            try
            {
                flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_OrderItem()
        {
            try
            {
                button = sportal.Button_Submit();
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_Get_Request_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Number();

                flag = ele.Existed;
                if (flag)
                {
                    REQiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Get_Request_Item_Number()
        {
            try
            {
                ele = sportal.TKP_Request_Item_Number();

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_Open_CheckStatus_MyRequests()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    ele = sportal.GHeaderMenu("Check Status");
                    if (ele.Existed)
                    {
                        ele.Click();
                        Thread.Sleep(5000);
                        sportal.WaitLoading();
                        Thread.Sleep(1000);
                        ele = sportal.GLeftMenu("My Requests");
                        flag = ele.Existed;
                        if (flag)
                        {
                            ele.Click();
                            sportal.WaitLoading();
                        }
                        else error = "Not found My Requests on left menu";
                    }
                    else
                    {
                        flag = false;
                        error = "Not found Check Status on dashboard menu";
                    }
                }
                else
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_04_ClickOn_MyActiveRequests()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    ele = sportal.GLinkByText("My Active Requests");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (flag) sportal.WaitLoading();
                    }
                    else error = "Not found menu.";
                }
                else
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_05_Search_And_Open_RITM()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //--------------------------------------------------------------
                    flag = sportal.SearchAndOpen(RITMiD, "Number=" + RITMiD, "Number");
                    if (!flag)
                        error = "Error when open RITM.";
                    else sportal.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_06_Verify_Activity_Delegate_Auto_Approval()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    string temp = Base.GData("Customer");
                    string expected = temp + "|Delegate approval Auto-approved";
                    flag = sportal.Verify_Activity(expected);
                    if (!flag)
                        error = "Invalid activity delegate approval Auto-approved";
                }
                else
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_020_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_021_ImpersonateUser_Delegate_User()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
                else
                {
                    string temp = Base.GData("Delegate_User");
                    flag = home.ImpersonateUser(temp, false, null, true);
                    if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                    else
                    {
                        temp = Base.GData("Delegate_User_Role");
                        if (temp.Trim().ToLower() == "itil")
                        {
                            itil.WaitLoading();
                            flag = home.SwitchToPortal();
                        }
                        sportal.WaitLoading();
                    }
                }         
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_ClickOn_Approvals()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
                else
                {
                    ele = sportal.GHeaderMenu("Approvals");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag)
                            error = "Error when click on approvals.";

                    }
                    else
                    {
                        error = "Not found My Approvals on dashboard menu.";
                    }
                }            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Open_Item()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
                else
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //------------------------------------------------------------------------------------------
                    Thread.Sleep(1000);
                    flag = sportal.Select_Approvals_Item(RITMiD);
                    if (!flag)
                        error = "Cannot open item.";
                    else sportal.WaitLoading();
                }       
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Add_Approval_Comment()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
                else
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //------------------------------------------------------------------------------------------
                    textarea = sportal.Textarea_Approval_Active_Stream_Comment();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(RITMiD);
                        if (flag)
                        {
                            button = sportal.Button_Send();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                if (!flag)
                                    error = "Error when click on send button.";
                            }
                            else error = "Cannot get button send.";
                        }
                        else error = "Cannot populate comment.";
                    }
                    else error = "Cannot get textarea comment.";
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_ClickOn_Button_Approve()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
                else
                {
                    button = sportal.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                            sportal.WaitLoading();
                        else
                            error = "Error when click approve button.";
                    }
                    else error = "Cannot get button approve.";
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_026_Goto_Login()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
                else
                {
                    string temp = Base.GData("Url");
                    Base.ClearCache();
                    Thread.Sleep(2000);
                    Base.Driver.Navigate().GoToUrl(temp);

                    login.WaitLoading();

                    string user = Base.GData("User");
                    string pwd = Base.GData("Pwd");

                    flag = login.LoginToSystem(user, pwd);

                    if (flag)
                    {
                        home.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot login to system.";
                    }
                }         
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else itil.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_01_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_02_Search_RITM_Use_Global()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }

                    flag = itil.GlobalSearchItem(RITMiD, true);
                    if (flag)
                        itil.WaitLoading();
                    else
                        error = "Cannot open RITM.";
                }
                else
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_03_Verify_Activity_Delegate_Auto_Approval()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    temp = Base.GData("Customer");
                    string expected = temp + "|Delegate approval Auto-approved";
                    flag = itil.Verify_Activity(expected);
                    if (!flag)
                        error = "Invalid activity.";
                }
                else
                {
                    Console.WriteLine("*** This case no need to run this step. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Open_MyGroupsWork_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (!flag)
                { error = "Cannot open My Groups Work link."; }
                else
                {
                    list.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_SearchAndOpenAssignedItem()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----Add Parent column to the list---------------------------------
                flag = list.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + RITMiD;
                    flag = list.Filter("Parent;contains;" + RITMiD);
                    if (flag)
                    {
                        flag = list.Open(temp, "Number");
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot find Catalog Task"; }
                    }
                    else { error = "Error when execute filter"; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Store_CTask_Id()
        {
            try
            {
                textbox = itil.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    CTaskiD = textbox.Text;
                }
                else { error = "Not found textbox Number"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_Verify_AssignmentGroup_Field()
        {
            try
            {
                temp = Base.GData("CTask_AssignmentGrp");
                if (temp.ToLower() != "no")
                {
                    lookup = itil.Lookup_AssignmentGroup();
                    flag = lookup.Existed;
                    if (flag)
                    {

                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            error = "Invalid assignment group value.";
                            flagExit = false;
                        }
                    }
                    else error = "Cannot get lookup assignment group.";
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Verify_RequestItem_Field()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------

                lookup = itil.Lookup_RequestItem();
                flag = lookup.Existed;
                if (flag)
                {

                    flag = lookup.VerifyCurrentValue(RITMiD, true);
                    if (!flag)
                    {
                        error = "Invalid request item value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup request item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Verify_RequestedFor_Field()
        {
            try
            {
                temp = Base.GData("Customer2");
                if(temp.Trim().ToLower() == "no")
                    temp = Base.GData("Customer");
                lookup = itil.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid requested for value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup requested for.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_04_Verify_CTask_State_Field_Open()
        {
            try
            {
                temp = "Open";
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {

                    flag = combobox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid state value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Add_CTask_WorkNote()
        {
            try
            {
                temp = Base.GData("CTask_WorkNote");
                flag = itil.Add_Worknotes(temp);
                if (!flag)
                    error = "Error when add work note.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Save_CTask()
        {
            try
            {
                flag = itil.Save();
                if (!flag) { error = "Error when save ctask."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Close_CTask_ClosedCommplete()
        {
            try
            {
                combobox = itil.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        flag = itil.Save();
                        if (!flag)
                        { error = "Cannot save Catalog Task."; }
                    }
                    else { error = "Cannot update Catalog Task's State"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Impersonate_User_Customer()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() == "no")
                {
                    temp = Base.GData("Customer_UserID");
                    if (temp.ToLower() != "no")
                        temp = Base.GData("Customer") + ";" + temp;
                }
                else
                {
                    temp = Base.GData("Customer2_UserID");
                    if (temp.ToLower() != "no")
                        temp = Base.GData("Customer2") + ";" + temp;
                }
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else
                {
                    temp = Base.GData("Customer2_Role");
                    if(temp.ToLower() == "no")
                        temp = Base.GData("Customer_Role");
                    if (temp.Trim().ToLower() == "itil")
                    {
                        itil.WaitLoading();
                        flag = home.SwitchToPortal();
                    }
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_ClickOn_CheckStatus()
        {
            try
            {
                ele = sportal.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else
                {
                    error = "Not found My Requests or Check Status on dashboard menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_ClickOn_MyRequests()
        {
            try
            {
                ele = sportal.GLeftMenu("My Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    sportal.WaitLoading();
                }
                else error = "Not found My Prior Approvals on left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_ClickOn_MyPriorRequests()
        {
            try
            {
                ele = sportal.GLinkByText("My Prior Requests");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag) sportal.WaitLoading();
                }
                else error = "Not found menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SearchAndOpen_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------
                string condition = "Stage=Completed";
                flag = sportal.SearchAndOpen(RITMiD, condition, "Number");
                if (flag)
                {
                    sportal.WaitLoading();
                }
                else { error = "Not found Requested Item"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Verify_Stage_Completed()
        {
            try
            {
                string temp = "Completed";
                combobox = sportal.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {

                    flag = combobox.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid stage value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_042_Goto_Login()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_ImpersonateUser_CSCSupport()
        {
            try
            {
                temp = Base.GData("Support_User");
                flag = home.ImpersonateUser(temp);
                if (!flag)
                { error = "Cannot impersonate CSC Support User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_02_Filter_EmailSentToCustomer_HasBeenSubmited()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer2_Email");
                if(email.Trim().ToLower() == "no")
                    email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been submitted|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_03_Open_EmailSentToCustomer_HasBeenSubmited()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been submited).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_04_Verify_Email_Submitted_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                string item = Base.GData("Item");
                temp = "INFO: " + item + " (" + RITMiD + ") has been submitted.";
                
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: (" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_05_Verify_Email_Submitted_Recipient()
        {
            try
            {
                string recipient = Base.GData("Customer2_Email");
                if (recipient.Trim().ToLower() == "no")
                    recipient = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(recipient);
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_07_Verify_EmailHtmlBody_Note()
        {
            try
            {
                string item = Base.GData("Item");
                temp = item + " has been successfully submitted and is awaiting approval.";
                
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_044_08_Verify_EmailHtmlBody_TicketId()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        //--------------------------------------------------------------------------------
        //        temp = "Requested Item Number: " + RITMiD;
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Requested Item id.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_044_09_Verify_EmailHtmlBody_ItemName()
        //{
        //    try
        //    {
        //        temp = "Item: " + Base.GData("Item");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Item name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_044_10_Verify_EmailHtmlBody_Description()
        //{
        //    try
        //    {
        //        temp = Base.GData("Item_Description");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid description.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_11_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flag = false;
                    flagExit = false;
                    error = "Invalid description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_12_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_OpenEmailLog()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                    if (flag)
                    {
                        emailList.WaitLoading();

                        if (!emailList.List_Title().MyText.Contains("Emails"))
                        {
                            flag = false;
                            error = "Cannot open email list.";
                        }
                    }
                    else error = "Error when open email log.";
                }          
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_Filter_EmailSentToManager_And_DelegateUser_ApprovalRequest()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string mng_email = Base.GData("Manager_Email");
                    string dlg_email = Base.GData("Delegate_User_Email");
                    temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;Approval request|and|Recipients;contains;" + mng_email + "|and|Recipients;contains;" + dlg_email;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Open_EmailSentToManager_And_DelegateUser_ApprovalRequest()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string conditions = "Subject=@@" + RITMiD;
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to manager (approval request).";
                    else email.WaitLoading();
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_04_Verify_Email_Approval_Subject()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }

                    //--------------------------------------------------------------------------------

                    temp = "Requested Item " + RITMiD + " Approval request";
                    flag = email.VerifySubject(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid subject.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_05_Verify_Email_Approval_Recipient()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    temp = Base.GData("Manager_Email");
                    bool flagT = email.VerifyRecipient(temp);
                    if (!flagT)
                    {
                        error = "Invalid recipient manager.";
                        if (flag)
                            flag = false;
                    }

                    temp = Base.GData("Delegate_User_Email");
                    flagT = email.VerifyRecipient(temp);
                    if (!flagT)
                    {
                        error += "Invalid recipient manager.";
                        if (flag)
                            flag = false;
                    }

                    if (!flag)
                        flagExit = false;
                }  
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_06_Click_PreviewHTLMBody()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    flag = email.ClickOnPreviewHtmlBody();
                    if (flag)
                    {
                        error = "Cannot click on Preview HTLM Body.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_07_Verify_EmailHtmlBody_Note()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    temp = "Please Approve or Reject the request by 1) selecting the appropriate link below AND 2) manually sending the email that is generated. You must send the auto-generated email to complete the processing.";
                    flag = email.VerifyEmailBody(temp);
                    if (flag == false)
                    {
                        flagExit = false;
                        error = "Invalid note for the Requested Item.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_08_Verify_EmailHtmlBody_ApproveTicketId()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }

                    //--------------------------------------------------------------------------------

                    temp = "Click here to approve " + RITMiD;
                    flag = email.VerifyEmailBody(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Approve message.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_09_Verify_EmailHtmlBody_RejectTicketId()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //--------------------------------------------------------------------------------

                    temp = "Click here to reject " + RITMiD;
                    flag = email.VerifyEmailBody(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Reject message.";
                    }
                }
                    

                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_10_Verify_EmailHtmlBody_ApprovalLink()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }

                    //--------------------------------------------------------------------------------
                    temp = "Click here to view Approval Request: Requested Item: " + RITMiD;
                    flag = email.VerifyEmailBody(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Approval link detail.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_11_Close_PreviewEmailHtml()
        {
            try
            {
                if (isAutoApprove == "yes")
                {
                    Console.WriteLine("*** This case no need to run this step ***");
                }
                else
                {
                    flag = email.ClickOnCloseButton();
                    if (!flag) error = "Error when close email body.";
                }
                    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_Filter_EmailSentToCustomer_HasBeenApproved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer2_Email");
                if(email.Trim().ToLower() == "no")
                    email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been approved|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_03_Open_EmailSentToCustomer_HasBeenApproved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been submited).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_04_Verify_Email_Item_Approved_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Item " + RITMiD + " has been approved.";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_05_Verify_Email_Item_Approved_Recipient()
        {
            try
            {
                temp = Base.GData("Customer2_Email");
                if (temp.Trim().ToLower() == "no")
                    temp = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_07_Verify_EmailHtmlBody_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Requested item " + RITMiD + " " + Base.GData("Item_ShortDes") + " has been approved and will be fulfilled.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_08_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "To view the item, click here: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Link.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_09_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Filter_EmailSentToCustomer_HasBeenCompleted()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Customer2_Email");
                if (email.Trim().ToLower() == "no")
                    email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been completed|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_03_Open_EmailSentToCustomer_HasBeenCompleted()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + RITMiD;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to customer (has been completed).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_04_Verify_Email_Item_Completed_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Requested Item " + RITMiD + " has been completed.";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_05_Verify_Email_Item_Completed_Recipient()
        {
            try
            {
                string temp = Base.GData("Customer2_Email");
                if (temp.Trim().ToLower() == "no")
                    temp = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_07_Verify_EmailHtmlBody_Content()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "This Requested Item has been completed.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_08_Verify_EmailHtmlBody_ItemName()
        {
            try
            {
                temp = "Item: " + Base.GData("Item");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_09_Verify_EmailHtmlBody_ItemState()
        {
            try
            {
                temp = "State: Closed Complete";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_10_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Link.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_11_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
