﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;

namespace ServicePortal
{
    [TestFixture]
    public class SP_INC_Create_3
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Current date time: " + currentDateTime);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        string incidentId, currentDateTime;
        Login login;
        Home home;
        SPortal sphome;
        ItilList incList;
        Incident inc;
        Email email;
        EmailList emailList;
        //-----------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        
        snobutton button = null;
        snoelement ele = null;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                sphome = new SPortal(Base, "SP Home");
                incList = new ItilList(Base, "Incident List");
                inc = new SNow.Incident(Base, "Incident");
                email = new Email(Base, "Email");
                emailList = new EmailList(Base, "Email List");
                //-----------------------------------------------------
                incidentId = string.Empty;
                currentDateTime = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sphome.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Verify_Customer_FullName()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                snoelement ele = sphome.UserFullName();
                flag = ele.Existed;
                if (flag)
                {
                    Console.WriteLine("Run time:(" + ele.MyText.Trim() + ")");
                    if (ele.MyText.Trim().ToLower() != temp.Trim().ToLower())
                    {
                        flag = false; error = "Invalid customer name.";
                    }
                }
                else error = "Cannot get control user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_ClickOn_Report_an_Issue_Link()
        {
            try
            {
                snoelement ele = sphome.GHeaderMenu("Report an Issue");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        sphome.WaitLoading();
                        flag = sphome.Verify_Page_Open("Home;Report an Issue");
                        if (!flag)
                            error = "Cannot open page [Report an Issue]";
                    }
                    else
                        error = "Error when click on menu [Report and Issue].";
                }
                else error = "Not found [Report an Issue] in header menu.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Verify_UserName()
        {
            try
            {
                lookup = sphome.Lookup_RequestedFor();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_FullName");
                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid user name value.";
                    }
                }
                else error = "Cannot get lookup user name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_Verify_FirstName()
        //{
        //    try
        //    {
        //        textbox = sphome.Textbox_FirstName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            string temp = Base.GData("Customer_FirstName");
        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                flagExit = false;
        //                error = "Invalid first name value.";
        //            }
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_008_Verify_LastName()
        //{
        //    try
        //    {
        //        textbox = sphome.Textbox_LastName();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            string temp = Base.GData("Customer_LastName");
        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                flagExit = false;
        //                error = "Invalid last name value.";
        //            }
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Verify_Email()
        {
            try
            {
                textbox = sphome.Textbox_EmailAddress();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_Email");
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid email value.";
                    }
                }
                else error = "Cannot get textbox email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_010_Verify_UserID()
        //{
        //    try
        //    {
        //        textbox = sphome.Textbox_UserID();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            string temp = Base.GData("Customer_UserID");
        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag)
        //            {
        //                flagExit = false;
        //                error = "Invalid user id value.";
        //            }
        //        }
        //        else error = "Cannot get textbox user id.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Verify_User_Telephone_Number()
        {
            try
            {
                textbox = sphome.Textbox_TelephoneNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_Phone");
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid user telephone number value.";
                    }
                }
                else error = "Cannot get textbox user telephone number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_UserLocation()
        {
            try
            {
                lookup = sphome.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_Location");
                    flag = lookup.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid user location value.";
                    }
                }
                else error = "Cannot get lookup user location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Verify_Default_value_of_Priority_Assessment()
        {
            try
            {
                snocombobox combobox = sphome.Combobox_PriorityAssessment();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("-- None --", true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid default value of combobox [Priority Assessment].";
                    }
                }
                else error = "Cannot get combobox [Priority Assessment].";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //This is combobox 'What kind of issue are you having?'
        [Test]
        public void Step_014_Verify_Default_value_of_Type_1()
        {
            try
            {
                snocombobox combobox = sphome.Combobox_Type_1();
                string temp = Base.GData("Default_Type_Level_1");

                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid default value of combobox [What kind of issue are you having?].";
                    }
                }
                else error = "Cannot get combobox [What kind of issue are you having?].";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //This is combobox 'What is the nature of the issue?'
        [Test]
        public void Step_015_Verify_Default_value_of_Type_2()
        {
            try
            {
                snocombobox combobox = sphome.Combobox_Type_2();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_VerifyCurrentValue("-- None --", true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid default value of combobox [What is the nature of the issue?].";
                    }
                }
                else error = "Cannot get combobox [What is the nature of the issue?].";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Attach_a_File()
        {
            try
            {
                string attachmentFile = "chgAttachment_2.pdf";

                flag = sphome.AttachmentFile(attachmentFile);
                if (!flag)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Validate_Attachment_file()
        {
            try
            {
                string attachmentFile = "chgAttachment_2.pdf";

                flag = sphome.VerifyAttachmentFile(attachmentFile,"incident");
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Populate_BriefDescription()
        {
            try
            {
                currentDateTime = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                string temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                textbox = sphome.Textbox_BriefDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) error = "Error when populate brief description.";
                }
                else error = "Cannot get textbox brief description.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_1_Populate_TypeLevel_1()
        {
            try
            {
                string temp = Base.GData("Type_Level_1");
                combobox = sphome.Combobox_Type_1();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_SelectItem(temp);
                    if (!flag) error = "Error when populate type level 1";
                }
                else error = "Cannot get combobox type level 1.";
                sphome.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_2_Populate_TypeLevel_2()
        {
            try
            {
                string temp = Base.GData("Type_Level_2");
                combobox = sphome.Combobox_Type_2();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_SelectItem(temp);
                    if (!flag) error = "Error when populate type level 2";
                }
                else error = "Cannot get combobox type level 2.";
                sphome.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_Populate_Detailed_Information()
        {
            try
            {
                string input = "chgAttachment_2.pdf";

                snotextarea textarea = sphome.Textarea_DetailedInformation();

                flag = textarea.Existed;

                if (flag)
                {
                    flag = textarea.SetText(input);
                    if (!flag)
                    {
                        error = "Can't set text [Detailed_Information]";
                    }
                }
                else
                {
                    error = "The field [Detailed_Information] is NOT found";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_Populate_PriorityAssessment()
        {
            try
            {
                string temp = Base.GData("PriorityAssessment");
                combobox = sphome.Combobox_PriorityAssessment();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SP_SelectItem(temp);
                    if (!flag) error = "Error when populate priority assessment";
                }
                else error = "Cannot get combobox priority assessment.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_03_Populate_other_required_fields()
        {
            try
            {
                flag = sphome.Input_Mandatory_Fields("Auto fill");
                if (!flag)
                {
                    error = "Error filling required fields automatically.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Click_Submit()
        {
            try
            {
                button = sphome.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Error when click on button submit.";
                    }
                    else
                    {
                        sphome.WaitLoading();
                    }
                }
                else error = "Cannot get button submit.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Save_Incident_ID()
        {
            try
            {
                incidentId = sphome.INC_Record_Details("Number");
                if (incidentId.Equals(String.Empty))
                {
                    flag = false;
                    error = "INCIDENT number is NOT found";
                }
                else
                {
                    incidentId = sphome.INC_Record_Details("Number").Trim();
                    Console.WriteLine("******INCIDENT NUMBER [" + incidentId + "] IS SAVED*******");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Click_Options_Toggle()
        {
            try
            {
                button = sphome.Button_Toggle("Options");
                flag = button.Existed;

                if (flag)
                {
                    flag = button.Click();
                    if (!flag) error = "Cannot click toggle button [Options]";
                }
                else error = "Cannot get toggle button [Options]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Verify_Caller()
        {
            try
            {
                string caller = sphome.INC_Options("User name:");
                string temp = Base.GData("Customer_FullName");

                if (caller.Equals(String.Empty))
                {
                    error = "Cannot get [Caller].";
                }
                else
                {
                    flag = caller.Equals(temp);
                    if (!flag)
                    {
                        error = "Invalid [Caller] value. Run time [" + caller + "]";
                        flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Verify_Brief_Description()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && currentDateTime == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input date time value.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------
                string brief_description = sphome.INC_Options("Brief Description");
                if (brief_description.Equals(String.Empty))
                {
                    error = "Cannot get textbox brief description.";
                }
                else
                {
                    temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                    flag = brief_description.Equals(temp);
                    if (!flag)
                    {
                        error = "Invalid [Brief Description] value. Run time [" + brief_description + "]";

                        flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Verify_State()
        {
            try
            {
                string state = sphome.INC_Record_Details("State");
                string temp = "New";

                if (state.Equals(String.Empty))
                {
                    flag = state.Equals(temp);
                    if (!flag)
                    {
                        error = "Invalid state value. Run time [" + state + "]";
                        flagExit = false;
                    }
                }
                else error = "Cannot get [State] label.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Remove_Attachment_file()
        {
            try
            {
                string attachmentFile = "chgAttachment_2.pdf";
                flag = sphome.Remove_Attachment(attachmentFile, "incident");
                if (flag)
                {
                    sphome.WaitLoading();
                }
                else
                {
                    error = "Can not remove attachment";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Click_Check_Status()
        {
            try
            {
                ele = sphome.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag) error = "Error when click on Check Status header menu.";
                    else sphome.WaitLoading();
                }
                else error = "Cannot get link check status on header.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Search_and_Open_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = sphome.SearchAndOpen(incidentId, "Number=" + incidentId, "Number");

                if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                else sphome.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Verify_Resolve_Button()
        {
            try
            {
                button = sphome.Button_Resolve();
                flag = button.Existed;
                if (!flag)
                    error = "Button Resolve is NOT visible. Expected visible.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Click_Home_Link()
        {
            try
            {
                ele = sphome.GHeaderItemMenu("Home");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag) error = "Error when click on menu home";
                    else sphome.WaitLoading();
                }
                else error = "Cannot get menu home.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void Step_032_Verify_Global_Search_Textbox()
        {
            try
            {
                textbox = sphome.Textbox_Global_Search();
                flag = textbox.Existed;
                if (!flag)
                    error = "Global search textbox is NOT visible.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Global_Search_INC_by_ID()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                textbox = sphome.Textbox_Global_Search();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(incidentId, true);
                    if (!flag) error = "Error when populate global search value.";
                    else sphome.WaitLoading();
                }
                else error = "Cannot get textbox global search.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Open_INC_from_Search_Result()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = sphome.OpenItemFromGlobalSearchResult("issue", incidentId);
                if (!flag) error = "Error when open item from global search result.";
                else sphome.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Navigate_to_Itil_view()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Impersonate_SDA()
        {
            try
            {
                string temp = Base.GData("SDA");

                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        [Test]
        public void Step_039_01_Search_and_open_INC()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                    if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                    else inc.WaitLoading();

                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_02_Resolve_INC()
        {
            try
            {
                snobutton button = inc.Button_ResolveIncident();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                }
                else error = "Cannot get button Resolve Incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_Add_Additional_Comment()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.Select_Tab("Notes");

                if (flag)
                {
                    button = inc.Button_Showfields();
                    flag = button.Existed;
                    if (flag)
                    {
                        if (button.MyElement.GetAttribute("title").ToLower() == "show all journal fields")
                        {
                            flag = button.Click(true);
                        }

                        if (flag)
                        {
                            temp = Base.GData("Inc_AdditionalComment") + " " + incidentId;
                            textarea = inc.Textarea_AdditionalCommentsCustomerVisible();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate additional comment value.";
                            }
                            else { error = "Cannot get textarea additional comment."; }
                        }
                        else error = "Cannot click on button show fields.";
                    }
                    else { error = "Cannot get button show fields."; }
                }
                else { error = "Cannot select tab Notes."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Populate_CloseCode_and_CloseNotes()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Inc_CloseCode");
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("Inc_CloseNote");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Save_INC()
        {
            try
            {
                flag = inc.Save();
                if (!flag) error = "Error when save incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_1_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else sphome.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_2_Validate_Landing_Page()
        {
            try
            {
                string temp = Base.GData("Customer_FullName");
                snoelement ele = sphome.UserFullName();
                flag = ele.Existed;
                if (flag)
                {
                    Console.WriteLine("Run time:(" + ele.MyText.Trim() + ")");
                    if (ele.MyText.Trim().ToLower() != temp.Trim().ToLower())
                    {
                        flag = false; error = "Invalid customer name.";
                    }
                }
                else error = "Cannot get label user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_1_Click_on_MyIssues()
        {
            try
            {
                ele = sphome.GHeaderMenu("Check Status");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag) error = "Error when click on my issues.";
                    else sphome.WaitLoading();
                }
                else error = "Cannot get link my issues.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_2_Verify_IncidentNotFound_In_MyIssues_List()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + incidentId;
                flag = sphome.SearchAndVerifyRow(incidentId, conditions, true);
                if (flag) error = "Found ticket (" + incidentId + "). Expected NOT found.";
                else
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Navigate_to_Itil_view()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(temp);
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_1_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_2_ImpersonateUser_SupportUser()
        {
            try
            {
                string temp = Base.GData("UserFullName");

                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Open_Email_Log()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_Filter_Email_sent_to_Caller_Opened()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + incidentId + "|and|Subject;contains;opened|and|" + "Recipients;contains;" + Base.GData("CallerEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_02_Validate_and_Open_EmailSentToCaller_Opened()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + incidentId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to caller (opened).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_03_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = Base.GData("Email_Notification_Subject");
                string expected = "Incident " + incidentId + " " + temp;
                flag = email.VerifySubject(expected);
                if (!flag) error = "Invalid subject value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_04_Verify_Email_Recipient_Customer()
        {
            try
            {
                string recipient = Base.GData("Customer_Email");
                flag = email.VerifyRecipient(recipient);
                if (!flag) error = "Invalid recipient value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_05_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                Thread.Sleep(1500);
                if (!flag) error = "CANNOT click 'Priview html body' link.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_06_Verify_EmailBody_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (currentDateTime == null || currentDateTime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current date time.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string expected = "Short Description: " + Base.GData("Inc_ShortDescription") + currentDateTime;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_07_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Return_Login_Page()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
    }
}
