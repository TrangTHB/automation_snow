﻿using Auto;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace Release
{
    [TestFixture]
    public class REL_e2e_1
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Product Id: " + productId);
            System.Console.WriteLine("Finished - Release Id: " + releaseId);
            System.Console.WriteLine("Finished - Release Phase 1 Id: " + rePhaseId1);
            System.Console.WriteLine("Finished - Release Phase 2 Id: " + rePhaseId2);
            System.Console.WriteLine("Finished - Release Task 1 Id: " + reTaskId1);
            System.Console.WriteLine("Finished - Release Task 2 Id: " + reTaskId2);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;

        SNow.snobutton button;
        SNow.snotextarea textarea;
        
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.ItilList approvalList = null;
        SNow.Itil release = null;
        SNow.ItilList relist = null;
        SNow.Itil products = null;
        SNow.ItilList productsList = null;
        SNow.Member member = null;
        SNow.EmailList emailList = null;
        //------------------------------------------------------------------
        string releaseId;
        string productId;
        string rePhaseId1, rePhaseId2;
        string reTaskId1, reTaskId2;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                release = new SNow.Itil(Base, "Release");
                relist = new SNow.ItilList(Base,"Release List");
                approvalList = new SNow.ItilList(Base, "Approval list");
                products = new SNow.Itil(Base, "Products");
                productsList = new SNow.ItilList(Base, "Products List");             
                member = new SNow.Member(Base);
                emailList = new SNow.EmailList(Base, "Email List");
                //------------------------------------------------------------------
                releaseId = string.Empty;
                productId = string.Empty;
                rePhaseId1 = string.Empty;
                rePhaseId2 = string.Empty;
                reTaskId1 = string.Empty;
                reTaskId2 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Can not login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_ImpersonateUser_ReleaseUser()
        {
            try
            {
                string temp = Base.GData("ReleaseManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_005_Select_ReleaseTaskModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Tasks");
                if (flag)
                    productsList.WaitLoading();
                else
                    error = "Error when create new release.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_006_Create_NewReleaseTask()
        {
            try
            {
                button = relist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        error = "Cannot click New button.";
                    }
                }
                else
                    error = "-*- Error: Cannot get new button on Release Task list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_01_Get_ReleaseTask_Number()
        {
            try
            {
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    reTaskId2 = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_02_Populate_ShortDescription()
        {
            try
            {
                string temp = Base.GData("Task_ShortDescription");
                textbox = release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_Save_ReleaseTask()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Can not Save Release";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Select_ProductModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Products");
                if (flag)
                    productsList.WaitLoading();
                else
                    error = "Error when create new product.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Create_NewProduct()
        {
            try
            {
                button = productsList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        error = "Cannot click New button.";
                    }
                }
                else
                    error = "-*- Error: Cannot get new button on product list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Get_ProductNumber()
        {
            try
            {
                textbox = products.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    productId = textbox.Text;
                }
                else
                    error = "-*- Error: Cannot get Number textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_SelectCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = products.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "-*- Error: Cannot select company.";
                }
                else
                    error = "-*- Error: Cannot get company lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("Product_ShortDescription");
                textbox = products.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_03_PopulateDescription()
        {
            try
            {
                string temp = Base.GData("Product_Description");
                textarea = products.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input Description value..";
                }
                else
                    error = "-*- Error: Cannot get Description textarea.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Save_Product()
        {
            try
            {
                flag = products.Save();
                if (!flag)
                    error = "-*- Error: Cannot save product.";
                else products.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Select_ReleasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                    productsList.WaitLoading();
                else
                    error = "Error when create new release.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Create_NewRelease()
        {
            try
            {
                button = relist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        error = "Cannot click New button.";
                    }
                    else relist.WaitLoading();
                }
                else
                    error = "-*- Error: Cannot get new button on release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_Get_ReleaseNumber()
        {
            try
            {
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    releaseId = textbox.Text;
                }
                else
                    error = "-*- Error: Cannot get Number textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Populate_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = release.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "-*- Error: Cannot select company.";
                }
                else
                    error = "-*- Error: Cannot get company lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_03_Populate_ShortDescription()
        {
            try
            {
                string temp = Base.GData("Release_ShortDescription");
                textbox = release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Populate_Parent()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (productId == null || productId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Product Id.");
                    addPara.ShowDialog();
                    productId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = release.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(productId);
                    if (!flag)
                    {
                        error = "Cannot populate Parent value.";
                    }
                }
                else { error = "Cannot get Parent lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Populate_Description()
        {
            try
            {
                string temp = Base.GData("Release_Description");
                textarea = release.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input description.";
                }
                else
                    error = "-*- Error: Cannot get description textarea.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Save_Release()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                    error = "-*- Error: Cannot save release.";
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_01_Select_ReleasePhasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("release phases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Release Phases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Release Phases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_02_Create_NewPhase01()
        {
            try
            {
                button = relist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        error = "Cannot click New button.";
                    }
                }
                else
                    error = "-*- Error: Cannot get new button on release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_03_Get_PhaseNumber()
        {
            try
            {
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    rePhaseId1 = textbox.Text;
                }
                else
                    error = "-*- Error: Cannot get Number textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_04_Populate_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = release.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "-*- Error: Cannot select company.";
                }
                else
                    error = "-*- Error: Cannot get company lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_05_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId1 == null || rePhaseId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 1 Id");
                    addPara.ShowDialog();
                    rePhaseId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Phase_ShortDescription");
                string shortDes = temp + " - " + rePhaseId1;
                //-----------------------------------------------------------------------
                textbox = release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(shortDes);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_06_Populate_Description()
        {
            try
            {
                string temp = Base.GData("Release_Description");
                textarea = release.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input description.";
                }
                else
                    error = "-*- Error: Cannot get description textarea.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_07_Save_ReleasePhase01()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                    error = "-*- Error: Cannot save Phase.";
                else { release.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_08_Select_ReleasePhasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("release phases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Release Phases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Release Phases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_09_Create_NewPhase02()
        {
            try
            {
                button = relist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        error = "Cannot click New button.";
                    }
                }
                else
                    error = "-*- Error: Cannot get new button on release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_10_Get_PhaseNumber()
        {
            try
            {
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    rePhaseId2 = textbox.Text;
                }
                else
                    error = "-*- Error: Cannot get Number textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_11_Populate_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = release.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "-*- Error: Cannot select company.";
                }
                else
                    error = "-*- Error: Cannot get company lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_12_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId2 == null || rePhaseId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 2 Id");
                    addPara.ShowDialog();
                    rePhaseId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Phase_ShortDescription");
                string shortDes = temp + " - " + rePhaseId2;
                //-----------------------------------------------------------------------
                textbox = release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(shortDes);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_13_Populate_Description()
        {
            try
            {
                string temp = Base.GData("Release_Description");
                textarea = release.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input description.";
                }
                else
                    error = "-*- Error: Cannot get description textarea.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_14_Save_ReleasePhase02()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                    error = "-*- Error: Cannot save Phase.";
                else { release.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        ////Workaround Add Phase 01 & 02 to Release 01. Follow step 17////
        //Due to DFCT0015757//
        [Test]
        public void Step_017_01_Select_ReleasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("releases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Releases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Releases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_02_SearchAndOpen_ReleaseID()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + releaseId + "|Short description=" + Base.GData("Release_ShortDescription");
                flag = relist.SearchAndOpen("Number", releaseId, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release ID (" + releaseId + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_03_Click_Edit_ReleaseTasksTab()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Release Phases", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_04_Add_Phase1_Phase2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId1 == null || rePhaseId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 1 Id");
                    addPara.ShowDialog();
                    rePhaseId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId2 == null || rePhaseId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 2 Id");
                    addPara.ShowDialog();
                    rePhaseId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Phase_ShortDescription");
                string phase1 = temp + " - " + rePhaseId1;
                string phase2 = temp + " - " + rePhaseId2;
                string total = phase1 + ";" + phase2;
                //-----------------------------------------------------------------------
                flag = member.Add_Members(total);
                if (!flag)
                {
                    error = "Cannot add 2 Phases.";
                }
                else { release.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_Select_ReleasePhaseModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("release phases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Releases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Releases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_02_SearchAndOpen_ReleasePhase01()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId1 == null || rePhaseId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 01");
                    addPara.ShowDialog();
                    rePhaseId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + rePhaseId1;
                flag = relist.SearchAndOpen("Number", rePhaseId1, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release Phase 01 (" + rePhaseId1 + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_Click_New_ReleaseTasks_Tab()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Release Tasks", "New");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Get_ReleaseTask_Number()
        {
            try
            {
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    reTaskId1 = textbox.Text;
                    release.WaitLoading();
                }
                else { error = "Cannot get Number textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_03_Populate_ShortDescription()
        {
            try
            {
                string temp = Base.GData("Task_ShortDescription");
                textbox = release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Save_ReleaseTask()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                    error = "-*- Error: Cannot save release task.";
                else { release.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_01_Select_ReleasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("release phases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Releases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Release Phases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_02_SearchAndOpen_ReleasePhases02()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId2 == null || rePhaseId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phases 02");
                    addPara.ShowDialog();
                    rePhaseId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + rePhaseId2;
                flag = relist.SearchAndOpen("Number", rePhaseId2, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release Phases(" + rePhaseId2 + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Click_Edit_ReleaseTasks_Tab()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Release Tasks", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_024_Add_ReleaseTask2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (reTaskId2 == null || reTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Task 2 Id");
                    addPara.ShowDialog();
                    reTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = member.AddMember(reTaskId2);
                if (!flag)
                {
                    error = "Cannot add Release Task 2.";
                }
                else { release.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Click_Save()
        {
            try
            {
                button = member.Button_Save();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on New button";
                    }
                    else member.WaitLoading();
                }
                else
                {
                    error = "Can not get on New button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_Select_ReleasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("releases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Releases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Releases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_SearchAndOpen_ReleaseID()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + releaseId + "|Short description=" + Base.GData("Release_ShortDescription");
                flag = relist.SearchAndOpen("Number", releaseId, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release ID (" + releaseId + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_03_Verify_State_Options()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("State_Options");
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot verify items in State conbobox.";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Select_StateAwaitingApproval()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Awaiting Approval";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "-*- Error: Cannot select State value";
                    }
                }
                else
                    error = "-*- Error: Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_01_Save_Release()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_02_Verify_ApprovalField()
        {
            try
            {
                combobox = release.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Requested";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                        error = "-*- Error: Incorrect approval value. [Runtime]:" + combobox.Text + ". [Expected]:" + temp;
                }
                else
                    error = "-*- Error: Cannot get approval combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_Verify_ApproverTab()
        {
            try
            {
                flag = release.Search_Verify_RelatedTable_Row("Approvers", "Approver", Base.GData("Approver"), "State=Requested|Assignment group=" + Base.GData("ApprovalGroup"));
                if (!flag)
                    error = "-*-Error: Cannot find any record as conditions.";
                else
                {
                    flag = release.Search_Verify_RelatedTable_Row("Approvers", "Group Assignment group", "!=" + Base.GData("ApprovalGroup"), "State=Requested", true);
                    if (!flag)
                        error = "-*- Error: Could find approval request of another group";
                    else
                    {
                        flag = release.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", "State=Requested");
                        if (!flag)
                        error = "Cannot find any record in requested state.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_ImpersonateUser_CABApproverUser()
        {
            try
            {
                string temp = Base.GData("Approver");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Open_MyApprovals()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                    approvalList.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_SearchAndOpenReleaseApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = approvalList.SearchAndOpen("Approval for", releaseId, "State=Requested", "State");
                if (!flag)
                    error = "-*-Error = Cannot fine release approval as condition.";
                else
                    release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_01_Verify_Approver_Value()
        {
            try
            {
                lookup = release.Lookup_Approver();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Approver");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + lookup.Text + "].";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Approver lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_02_Verify_State_Value()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Requested";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "].";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_03_Verify_Update_Button()
        {
            try
            {
                button = release.Button_Update();
                flag = button.Existed;
                if (!flag)
                {
                    error = "Cannot get Update button.";
                    flagExit = false;
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_04_Verify_Reject_Button()
        {
            try
            {
                button = release.Button_Reject();
                flag = button.Existed;
                if (!flag)
                {
                    error = "Cannot get Reject button.";
                    flagExit = false;
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_05_Click_Approve_Button()
        {
            try
            {
                button = release.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) { error = "Cannot click Approve button."; }
                    else release.WaitLoading();
                }
                else
                {
                    error = "Cannot get Approve button.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_SearchAndVerify_Release_State()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "State=Approved|Approval for=" + releaseId;
                flag = approvalList.SearchAndVerify("Approval for", releaseId, condition);
                if (!flag)
                    error = "-*-Error = Cannot find release approval as condition.";
                else approvalList.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_ImpersonateUser_ReleaseUser()
        {
            try
            {
                string temp = Base.GData("ReleaseManager");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_Select_ReleasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                    relist.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_01_SearchAndOpen_ReleaseId()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + releaseId + "|Short description=" + Base.GData("Release_ShortDescription");
                flag = relist.SearchAndOpen("Number", releaseId, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release ID (" + releaseId + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_02_Click_ReleasePhases_Tab()
        {
            try
            {
                flag = release.Select_Tab("Release Phases");
                if (!flag)
                    error = "Error when select (Schedule) tab";
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_Open_Phase1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId1 == null || rePhaseId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 1 Id");
                    addPara.ShowDialog();
                    rePhaseId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string shortDes = temp = Base.GData("Phase_ShortDescription");
                string phase1 = temp + " - " + rePhaseId1;
                string condition = "Number=" + rePhaseId1 + "|Short description=" + phase1;
                flag = release.RelatedTableOpenRecord("Release Phases", condition, "Number");
                if (!flag)
                {
                    error = "Cannot open record with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Open_Task1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (reTaskId1 == null || reTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Task 1 Id");
                    addPara.ShowDialog();
                    reTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string shortDes = temp = Base.GData("Task_ShortDescription");
                string condition = "Number=" + reTaskId1 + "|Short description=" + shortDes;
                flag = release.RelatedTableOpenRecord("Release Tasks", condition, "Number");
                if (!flag)
                {
                    error = "Cannot open record with condition: " + condition;
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_01_PopulateState_WorkInProgress()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Work in Progress";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_Populate_AssignmentGroup()
        {
            try
            {
                lookup = release.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Assignment group lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_03_Populate_AssignTo()
        {
            try
            {
                lookup = release.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Assignee");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Assigned to lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_01_Save_ReleaseTask()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release Task.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_02_VerifyState_WorkInProgress()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Work in Progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_01_Populate_WorkNote()
        {
            try
            {
                textarea = release.Textarea_Worknotes_REL();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WorkNote");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Work Note value.";
                    }
                }
                else { error = "Cannot get Work Note textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_PopulateState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_01_Save_ReleaseTask()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release Task.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_02_VerifyState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_01_Populate_WorkNote()
        {
            try
            {
                textarea = release.Textarea_Worknotes_REL();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WorkNote");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Work Note value.";
                    }
                }
                else { error = "Cannot get Work Note textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_PopulateState_ClosedIncomplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Incomplete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Save_ReleaseTask()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release Task.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_04_VerifyState_ClosedIncomplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Incomplete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_05_Populate_WorkNote()
        {
            try
            {
                textarea = release.Textarea_Worknotes_REL();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WorkNote");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Work Note value.";
                    }
                }
                else { error = "Cannot get Work Note textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_06_PopulateState_ClosedSkipped()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Skipped";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_07_Save_ReleaseTask()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release Task.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_08_VerifyState_ClosedSkipped()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Skipped";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_01_Select_PhasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower(); ;
                    if (!temp.Contains("release phases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Release Phases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Release Phases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_02_SearchAndOpen_ReleasePhase1Id()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId1 == null || rePhaseId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 1 Id");
                    addPara.ShowDialog();
                    rePhaseId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Phase_ShortDescription");
                string phase1 = temp + " - " + rePhaseId1;
                //-----------------------------------------------------------------------
                string condition = "Number=" + rePhaseId1 + "|Short description=" + phase1;
                flag = relist.SearchAndOpen("Number", rePhaseId1, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release ID (" + rePhaseId1 + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_01_Populate_WorkNote()
        {
            try
            {
                textarea = release.Textarea_Worknotes_REL();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WorkNote");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Work Note value.";
                    }
                }
                else { error = "Cannot get Work Note textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_02_PopulateState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_01_Save_ReleasePhase()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release Phase.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_02_VerifyState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_01_Select_PhasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("release phases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Release Phases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Release Phases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_02_SearchAndOpen_ReleasePhase2Id()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rePhaseId2 == null || rePhaseId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase 2 Id");
                    addPara.ShowDialog();
                    rePhaseId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Phase_ShortDescription");
                string phase2 = temp + " - " + rePhaseId2;
                //-----------------------------------------------------------------------
                string condition = "Number=" + rePhaseId2 + "|Short description=" + phase2;
                flag = relist.SearchAndOpen("Number", rePhaseId2, condition, "Number");
                if (!flag)
                {
                    error = "Cannot search and open Release ID (" + rePhaseId2 + ") with condition: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_03_Populate_WorkNote()
        {
            try
            {
                textarea = release.Textarea_Worknotes_REL();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WorkNote");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Work Note value.";
                    }
                }
                else { error = "Cannot get Work Note textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_04_PopulateState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_05_Save_ReleasePhase()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release Phase.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_06_VerifyState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_01_Select_ReleasesModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("releases"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid title value. Expected: [Releases]. Runtime: [" + temp + "].";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when open Releases list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_02_SearchAndOpen_ReleaseId()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + releaseId + "|Short description=" + Base.GData("Release_ShortDescription");
                flag = approvalList.SearchAndOpen("Number", releaseId, condition, "Number");
                if (!flag)
                    error = "-*-Error = Cannot find release as condition.";
                else
                    release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_03_PopulateState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_01_Save_Release()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Cannot save Release.";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_02_VerifyState_ClosedComplete()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_01_ImpersonateUser_SupportUser()
        {
            try
            {
                string temp = Base.GData("SupportUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_02_Open_EmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_01_Filter_Email_ReleaseId_ApprovalRequest()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (releaseId == null || releaseId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id");
                    addPara.ShowDialog();
                    releaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string appr_email = Base.GData("Approver_Email");
                temp = "Subject;contains;" + releaseId + "|and|Subject;contains;Approval Request" + "|and|Recipients;contains;" + appr_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    string subject = "Release " + releaseId + " Approval Request";
                    string condition = "Recipients=" + appr_email + "|Subject=" + subject;
                    flag = emailList.VerifyRow(condition);
                    if (!flag)
                    {
                        error = "Cannot verify email with condition: " + condition;
                    }
                }
                else { error = "Cannot filter email with condition: " + temp; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_02_AccessEmailList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_03_Filter_Email_ReTaskId1_Assignee()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (reTaskId1 == null || reTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Task 1 Id");
                    addPara.ShowDialog();
                    reTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string assignee_email = Base.GData("Assignee_Email");
                temp = "Subject;contains;" + reTaskId1 + "|and|Recipients;contains;" + assignee_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    string subject = "Release Task " + reTaskId1 + " notification -- " + Base.GData("Task_ShortDescription");
                    string condition = "Recipients=" + assignee_email + "|Subject=" + subject;
                    flag = emailList.VerifyRow(condition);
                    if (!flag)
                    {
                        error = "Cannot verify email with condition: " + condition;
                    }
                }
                else { error = "Cannot filter email with condition: " + temp; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
