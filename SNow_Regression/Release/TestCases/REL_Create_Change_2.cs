﻿using Auto;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace Release
{
    [TestFixture]
    public class REL_Create_change_2
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

          
            System.Console.WriteLine("Finished - Change Id01: " + chgId01);
            System.Console.WriteLine("Finished - Change Id02: " + chgId02);
            System.Console.WriteLine("Finished - Change Id03: " + chgId03);
            System.Console.WriteLine("Finished - Change Id04: " + chgId04);
            System.Console.WriteLine("Finished - Change Id05: " + chgId05);
            System.Console.WriteLine("Finished - Change Id06: " + chgId06);
            System.Console.WriteLine("Finished - Change Id07: " + chgId07);
            System.Console.WriteLine("Finished - Change Id08: " + chgId08);
            System.Console.WriteLine("Finished - Release Id: " + releaseid);
            System.Console.WriteLine("Finished - Feature Id: " + fetid);
            System.Console.WriteLine("Finished - Release Phase Id: " + phaseId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;

        SNow.snobutton button;
        SNow.snotextarea textarea;
        SNow.snodatetime datetime;


        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Itil release = null;
        SNow.ItilList relist = null;
        SNow.Change change = null;
        SNow.ChangeList changelist = null;
        SNow.Member mb = null;
        SNow.Itil fet = null;
        SNow.ItilList fetlist = null;
        SNow.Itil  phase = null;
        SNow.ItilList phlist = null;

        //------------------------------------------------------------------
        string releaseid; string chgId01; string chgId02; string chgId03; string chgId04; string fetid; string chgId05; string chgId06; string chgId07; string chgId08; string phaseId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                release = new SNow.Itil(Base, "Release");
                relist = new SNow.ItilList(Base,"Release List");
                change = new SNow.Change(Base, "Change");
                changelist = new SNow.ChangeList(Base, "Change List");
                mb = new SNow.Member(Base);
                fet = new SNow.Itil(Base, "Feature");
                fetlist = new SNow.ItilList(Base, "Feature List");
                phase = new SNow.Itil(Base, "Phase");
                phlist = new SNow.ItilList(Base, "Phase List");
                //------------------------------------------------------------------
                releaseid = string.Empty; chgId01 = string.Empty; chgId02 = string.Empty; chgId03 = string.Empty; chgId04 = string.Empty;
                fetid = string.Empty; chgId05 = string.Empty; chgId06 = string.Empty; chgId07 = string.Empty; chgId08 = string.Empty; phaseId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Can not login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_003_ImpersonateUser_Changerequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_005_Open_Change_Module()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    change.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = change.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else change.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_01_Create_New_Change03()
        {
            try
            {
                //Save Change ID03
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId03 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 03 id:(" + chgId03 + ")");
                }
                else error = "Cannot get textbox number.";

                //Populate Company
                lookup = change.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company in lookup";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_02_Verify_Change_Type()
        {
            try
            {
                combobox = change.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Normal";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change Type is not correct";
                    }
                }
                else
                {
                    error = "Can not get Change Type combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_03_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_04_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_05_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_06_Populate_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description textbox";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_07_Populate_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description textarea";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_08_Populate_Assignment_Group()
        {
            try
            {
                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeAssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate Assignment Group lookup";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_09_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_10_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_006_11_Submit_Change03()
        {
            try
            {
                button = change.Button_Submit();
                flag = button.Existed;
                if (flag) 
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on Submit button";
                    }
                    else change.WaitLoading();
                }
                else 
                {
                    error = "Can not get Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_01_Create_New_Change04()
        {
            try
            {

                //Open Create New Normal Change
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    change.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = change.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else change.WaitLoading();
                }
                else
                    error = "Error when create new change.";
                
                //Save Change ID04
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId04 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 04 id:(" + chgId04 + ")");
                }
                else error = "Cannot get textbox number.";

               
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_02_Verify_Change_Type()
        {
            try
            {
                combobox = change.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Normal";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        //Populate Company
                        lookup = change.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (!flag)
                            {
                                error = "Can not select Company in lookup";
                            }
                        }
                        else
                        {
                            error = "Can not get Company lookup";
                        }
                    }
                    else
                    {
                        error = "Change Type is not correct";
                    }
                }
                else
                {
                    error = "Can not get Change Type combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_03_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_04_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_05_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_06_Populate_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description textbox";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_07_Populate_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description textarea";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_08_Populate_Assignment_Group()
        {
            try
            {
                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeAssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate Assignment Group lookup";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_09_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_10_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Prestep_007_11_Submit_Change04()
        {
            try
            {
                button = change.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on Submit button";
                    }
                    else change.WaitLoading();
                }
                else
                {
                    error = "Can not get Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_ImpersonateUser_ReleaseCoordinator()
        {
            try
            {
                string temp = Base.GData("ReleaseManager");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Verify_Release_Modules()
        {
            try
            {
                string temp = Base.GData("Release_modules");
                flag = home.LeftMenuItemValidate("Release v2",temp);
                if (!flag)
                {
                    error = "Modules in Release v2 is not correct";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_01_Select_Release_Module()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (!flag)
                {
                    error = "Can not open Release";
                }
                else relist.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_02_Verify_Release_Filter()
        {
            try
            {
                flag = relist.Filter("Task type;is;Release");
                if (!flag)
                {
                    error = "Default Filter condition is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Create_New_Release()
        {
            try
            {
                button = relist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on New button";
                    }
                    else release.WaitLoading();
                }
                else
                {
                    error = "Can not get New button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Verify_Release_Form()
        {
            try
            {
                //Save Release Id
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    releaseid = textbox.Text;
                    Console.WriteLine("-*-[Store]: Release id:(" + releaseid + ")");
                }
                else error = "Cannot get textbox number.";

                //Verify Current State
                flag = release.Verify_CurrentProcess("Draft");
                if (!flag)
                {
                    error = "Default current State is not correct";
                }

                //Verify Priority
                combobox = release.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "3 - Medium";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Priority is not correct";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }


                //Verify State
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "State is not correct";
                    }
                }
                else
                {
                    error = "Can not get State combobox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_Populate_Company()
        {
            try
            {
                lookup = release.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate Company lookup";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Populate_Priorty()
        {
            try
            {
                combobox = release.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Release_Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not populate Priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Populate_Short_Description()
        {
            try
            {
                textbox = release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Release_ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate ShortDescription";
                    }
                }
                else
                {
                    error = "Can not get ShortDescription textbox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_04_Populate_Description()
        {
            try
            {
                textarea = release.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Release_Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_05_Populate_Assignment_Group()
        {
            try
            {

                lookup = release.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate AssignmentGroup lookup";
                    }
                }
                else
                {
                    error = "Can not get AssignmentGroup lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Click_Save_button()
        {
            try
            {
                flag = release.Save();
                if (!flag)
                {
                    error = "Can not Save Release";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Create_Change01()
        {
            try
            {
                flag = release.CreateNormalChange();
                if (!flag)
                {
                    error = "Can not click on Create Normal Change";
                }
                else release.WaitLoading();
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Verify_Change01_Creation_Message()
        {
            try
            {
                //Save Change ID01
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId01 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 01 id:(" + chgId01 + ")");
                }
                else error = "Cannot get textbox number.";

                //Verify creation message

                flag = change.VerifyMessageInfo("Change " + chgId01 + " created");
                if (!flag)
                {
                    error = "Change creation message is not correct";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_03_Verify_Change01_Parent()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && releaseid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id.");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------

                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = releaseid;
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Release Parent is not correct";
                    }
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_04_Verify_Change01_Requested_By()
        {
            try
            {
                lookup = change.Lookup_RequestedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ReleaseManager");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Requested By is not correct";
                    }
                }
                else
                {
                    error = "Can not get Requested By lookup";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_05_Verify_Change01_State()
        {
            try
            {
                combobox = change.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "State is not correct";
                    }
                }
                else
                {
                    error = "Can not get State combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_06_Verify_Change01_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Release_ShortDescription");
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Short Description is not correct";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_07_Verify_Change01_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Release_Description");
                    flag = textarea.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Description is not correct";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_08_Verify_Change01_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Release_Priority");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change riority is not correct";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_09_Verify_Change01_Assignment_Group()
        {
            try
            {

                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Assignment Group is not correct";
                    }
                }
                else
                {
                    error = "Can not get AssignmentGroup lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_03_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Click_Save_button()
        {
            try
            {
                flag = change.Save();
                if (!flag)
                {
                    error = "Can not Save Release";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Search_And_Open_Release()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (!flag)
                {
                    error = "Can not open Release";
                }
                else relist.WaitLoading();

                // Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && releaseid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id.");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + releaseid;                 
                flag = relist.SearchAndOpen("Number",releaseid,temp,"Number");
                if (!flag)
                {
                    error = "Can not open Release Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_Change_Requests_Tab()
        {
            try
            {
                
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 01 Id.");
                    addPara.ShowDialog();
                    chgId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                string condition = "Number=" + chgId01;
                flag = release.Verify_RelatedTable_Row("Change Requests",condition);
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_01_Click_New_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "New");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_02_Create_Change02()
        {
            try
            {
                //Select Normal Change type
                string temp = Base.GData("ChangeType");
                flag = change.Select_Change_Type(temp);
                if (!flag) error = "Error when select change type.";
                else change.WaitLoading();

                //Save Change ID02
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId02 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 02 id:(" + chgId02 + ")");
                }
                else error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_03_Populate_Company()
        {
            try
            {

                lookup = change.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company in lookup";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_04_Verify_Change_Type()
        {
            try
            {
                combobox = change.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Normal";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change Type is not correct";
                    }
                }
                else
                {
                    error = "Can not get Change Type combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_05_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_06_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_07_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_08_Populate_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description textbox";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_09_Populate_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description textarea";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_10_Populate_Assignment_Group()
        {
            try
            {
                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeAssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate Assignment Group lookup";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_11_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_12_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_13_Submit_Change02()
        {
            try
            {
                button = change.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on Submit button";
                    }
                    else change.WaitLoading();
                }
                else
                {
                    error = "Can not get Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Click_Edit_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_01_Relate_Change03()
        {
            try
            {
                //---------------------------------------------------
                 string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                flag = mb.AddMember(chgId03);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not relate Changes to Release";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_02_Relate_Change04()
        {
            try
            {

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
               
                //------------------------------------------------

                flag = mb.Add_Members(chgId04);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not relate Changes to Release";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Verify_Change_Requests_Tab()
        {
            try
            {

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 01 Id.");
                    addPara.ShowDialog();
                    chgId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 02 Id.");
                    addPara.ShowDialog();
                    chgId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId01;
                flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId02;
                    flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                    if (flag)
                    {
                        condition = "Number=" + chgId03;
                        flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                        if (flag)
                        {
                            condition = "Number=" + chgId04;
                            flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                            if (!flag)
                            {
                                error = "Change 04 in related list is not correct";
                            }
                        }
                        else
                        {
                            error = "Change 03 in related list is not correct";
                        }
                    }
                    else
                    {
                        error = "Change 02 in related list is not correct";
                    }
                }
                else
                {
                    error = "Change 01 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Open_Change03()
        {
            try
            {

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId03;
                flag = release.RelatedTableOpenRecord("Change Requests", condition, "Number");
                if (!flag)
                {
                    error = "Can not open Change 03 in Change Requests related tab";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Remove_Parent()
        {
            try
            {
                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText("", true);
                    if (!flag)
                    {
                        error = "Can not remove Parent of Change 02";
                    }
                    else release.WaitLoading();
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_Click_Save_Button()
        {
            try
            {
                flag = change.Save();
                if (!flag)
                {
                    error = "Can not Save Change 03";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Search_And_Open_Release()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (!flag)
                {
                    error = "Can not open Release";
                }
                else relist.WaitLoading();

                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && releaseid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Id.");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + releaseid;
                flag = relist.SearchAndOpen("Number", releaseid, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Release Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 01 Id.");
                    addPara.ShowDialog();
                    chgId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 02 Id.");
                    addPara.ShowDialog();
                    chgId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId01;
                flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId02;
                    flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                    if (flag)
                    {
                        condition = "Number=" + chgId04;
                        flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                        if (flag)
                        {
                            condition = "Number=" + chgId03;
                            flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                            if (flag)
                            {
                                error = "Change 03 should not be displayed";
                            }
                            else
                            {
                                flag = true;
                            }
                        }
                        else
                        {
                            error = "Change 04 in related list is not correct";
                        }
                    }
                    else
                    {
                        error = "Change 02 in related list is not correct";
                    }
                }
                else
                {
                    error = "Change 01 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_Click_Edit_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_Remove_Change04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                flag = mb.Delete_Members(chgId04);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not remove Changes to Release";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 01 Id.");
                    addPara.ShowDialog();
                    chgId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 02 Id.");
                    addPara.ShowDialog();
                    chgId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId01;
                flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId02;
                    flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                    if (!flag)
                    {
                        error = "Change 02 in related list is not correct";
                    }
                    else
                    {
                        error = "Change 01 in related list is not correct";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_01_Search_And_Open_Change04()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (!flag)
                {
                    error = "Can not open Change";
                }
                else changelist.WaitLoading();
                //Search and open Change 04

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + chgId04;
                flag = changelist.SearchAndOpen("Number", chgId04, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Change 04 Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_02_Verify_Change04_Parent()
        {
            try
            {
                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Text.Equals("");
                    if (!flag)
                    {
                        error = "Parent should be null";
                    }
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Open_Feature_Module()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Features");
                if (!flag)
                {
                    error = "Can not open Features";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_Create_New_Feature()
        {
            try
            {
                button = fetlist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on New button";
                    }
                    else fetlist.WaitLoading();
                }
                else
                {
                    error = "Can not get New button";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_Verify_Progress_Status()
        {
            try
            {
                //Save Feature ID
                textbox = fet.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    fetid = textbox.Text;
                    Console.WriteLine("-*-[Store]: Feature id:(" + fetid + ")");
                }
                else error = "Cannot get textbox number.";

                //-------------------------------------------

                flag = fet.Verify_CurrentProcess("Draft");
                if (!flag)
                {
                    error = "Default State of Feature is not correct";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_03_Verify_State()
        {
            try
            {
                combobox = fet.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (!flag)
                    {
                        error = "Default State of Feature is not correct";
                    }
                }
                else
                {
                    error = "Can not get State combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_04_Verify_Priority()
        {
            try
            {
                combobox = fet.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("3 - Medium");
                    if (!flag)
                    {
                        error = "Default Priority of Feature is not correct";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_Populate_ShortDescription()
        {
            try
            {
                textbox = fet.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Feature_ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_Populate_Description()
        {
            try
            {
                textarea = fet.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Feature_Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description";
                    }
                }
                else
                {
                    error = "Can not get Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_Populate_Priority()
        {
            try
            {
                combobox = fet.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Feature_Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_04_Populate_AssignmentGroup()
        {
            try
            {
                lookup = fet.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Assignment Group";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_05_Click_Save_Button()
        {
            try
            {
                flag = fet.Save();
                if (!flag)
                {
                    error = "Can not Save Feature";
                }
                else release.WaitLoading();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_Create_Change05()
        {
            try
            {
                flag = fet.CreateNormalChange();
                if (!flag)
                {
                    error = "Can not create Normal Change";
                }
                else fet.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_Verify_Change05_Creation_Message()
        {
            try
            {
                //Save Change ID05
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId05 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 05 id:(" + chgId05 + ")");
                }
                else error = "Cannot get textbox number.";

                //Verify creation message

                flag = change.VerifyMessageInfo("Change " + chgId05 + " created");
                if (!flag)
                {
                    error = "Change creation message is not correct";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_03_Verify_Change05_Parent()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && fetid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feature Id.");
                    addPara.ShowDialog();
                    fetid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------

                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = fetid;
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Feature Parent is not correct";
                    }
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_04_Verify_Change05_Requested_By()
        {
            try
            {
                lookup = change.Lookup_RequestedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ReleaseManager");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Requested By is not correct";
                    }
                }
                else
                {
                    error = "Can not get Requested By lookup";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_05_Verify_Change05_State()
        {
            try
            {
                combobox = change.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "State is not correct";
                    }
                }
                else
                {
                    error = "Can not get State combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_06_Verify_Change05_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Feature_ShortDescription");
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Short Description is not correct";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_07_Verify_Change05_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Feature_Description");
                    flag = textarea.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Description is not correct";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_08_Verify_Change05_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Feature_Priority");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change Priority is not correct";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_09_Verify_Change05_Assignment_Group()
        {
            try
            {

                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Assignment Group is not correct";
                    }
                }
                else
                {
                    error = "Can not get AssignmentGroup lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_01_Populate_Company()
        {
            try
            {
                lookup = change.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company";
                    }
                    else change.WaitLoading();
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_02_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                    else release.WaitLoading();
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_03_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_04_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_05_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_06_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Click_Save_button()
        {
            try
            {
                flag = change.Save();
                if (!flag)
                {
                    error = "Can not Save Release";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Search_And_Open_Feature()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Features");
                if (!flag)
                {
                    error = "Can not open Features";
                }
                else fetlist.WaitLoading();

                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && fetid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feature Id.");
                    addPara.ShowDialog();
                    fetid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + fetid;
                flag = fetlist.SearchAndOpen("Number", fetid, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Feature Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId05 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 05 Id.");
                    addPara.ShowDialog();
                    chgId05 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId05;
                flag = fet.Verify_RelatedTable_Row("Change Requests", condition);
                if (!flag)
                {
                    error = "Change 05 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Click_New_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "New");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_Create_Change06()
        {
            try
            {
                //Select Normal Change type
                string temp = Base.GData("ChangeType");
                flag = change.Select_Change_Type(temp);
                if (!flag) error = "Error when select change type.";
                else change.WaitLoading();

                //Save Change ID06
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId06 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 06 id:(" + chgId06 + ")");
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Populate_Company()
        {
            try
            {

                lookup = change.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company in lookup";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Verify_Change_Type()
        {
            try
            {
                combobox = change.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Normal";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change Type is not correct";
                    }
                }
                else
                {
                    error = "Can not get Change Type combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_04_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_05_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_06_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_07_Populate_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description textbox";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_08_Populate_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description textarea";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_09_Populate_Assignment_Group()
        {
            try
            {
                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeAssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate Assignment Group lookup";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_10_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_11_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_12_Submit_Change06()
        {
            try
            {
                button = change.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on Submit button";
                    }
                    else change.WaitLoading();
                }
                else
                {
                    error = "Can not get Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Search_And_Open_Feature()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Features");
                if (!flag)
                {
                    error = "Can not open Features";
                }
                else fetlist.WaitLoading();

                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && fetid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feature Id.");
                    addPara.ShowDialog();
                    fetid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + fetid;
                flag = fetlist.SearchAndOpen("Number", fetid, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Feature Id";
                }
                else change.WaitLoading();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId06 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 06 Id.");
                    addPara.ShowDialog();
                    chgId06 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId06;
                flag = fet.Verify_RelatedTable_Row("Change Requests", condition);
                if (!flag)
                {
                    error = "Change 06 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Click_Edit_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_01_Relate_Change03()
        {
            try
            {
                //---------------------------------------------------
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                flag = mb.Add_Members(chgId03);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not relate Changes to Release";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_02_Relate_Change04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
                //------------------------------------------------

                flag = mb.Add_Members(chgId04);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not relate Changes to Release";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId05 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 05 Id.");
                    addPara.ShowDialog();
                    chgId05 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId06 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 06 Id.");
                    addPara.ShowDialog();
                    chgId06 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId03;
                flag = fet.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId04;
                    flag = fet.Verify_RelatedTable_Row("Change Requests", condition);
                    if (flag)
                    {
                        condition = "Number=" + chgId05;
                        flag = fet.Verify_RelatedTable_Row("Change Requests", condition);
                        if (flag)
                        {
                            condition = "Number=" + chgId06;
                            flag = fet.Verify_RelatedTable_Row("Change Requests", condition);
                            if (!flag)
                            {
                                error = "Change 06 in related list is not correct";
                            }
                        }
                        else
                        {
                            error = "Change 05 in related list is not correct";
                        }
                    }
                    else
                    {
                        error = "Change 04 in related list is not correct";
                    }
                }
                else
                {
                    error = "Change 03 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Open_Change03()
        {
            try
            {

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId03;
                flag = fet.RelatedTableOpenRecord("Change Requests", condition, "Number");
                if (!flag)
                {
                    error = "Can not open Change 03 in Change Requests related tab";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Remove_Parent()
        {
            try
            {
                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText("", true);
                    if (!flag)
                    {
                        error = "Can not remove Parent of Change 03";
                    }
                    else release.WaitLoading();
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_Click_Save_Button()
        {
            try
            {
                flag = change.Save();
                if (!flag)
                {
                    error = "Can not Save Change 03";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Search_And_Open_Feature()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Features");
                if (!flag)
                {
                    error = "Can not open Feature";
                }
                else relist.WaitLoading();

                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && fetid == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feature Id.");
                    addPara.ShowDialog();
                    fetid = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + fetid;
                flag = relist.SearchAndOpen("Number", fetid, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Feature Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_Click_Edit_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_Remove_Change04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                flag = mb.Delete_Members(chgId04);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not remove Changes to Release";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId05 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 05 Id.");
                    addPara.ShowDialog();
                    chgId05 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId06 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 06 Id.");
                    addPara.ShowDialog();
                    chgId06 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId05;
                flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId06;
                    flag = release.Verify_RelatedTable_Row("Change Requests", condition);
                    if (!flag)
                    {
                        error = "Change 06 in related list is not correct";
                    }
                    else
                    {
                        error = "Change 05 in related list is not correct";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_01_Search_And_Open_Change04()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (!flag)
                {
                    error = "Can not open Change";
                }
                else changelist.WaitLoading();

                //Search and open Change 04

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                temp = "Number=" + chgId04;
                flag = changelist.SearchAndOpen("Number", chgId04, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Change 04 Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_Verify_Change04_Parent()
        {
            try
            {
                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Text.Equals("");
                    if (!flag)
                    {
                        error = "Parent should be null";
                    }
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Open_Release_Phase()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (!flag)
                {
                    error = "Can not open Release Phases";
                }
                else relist.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_Create_New_Release_Phase()
        {
            try
            {
                button = phlist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on New button";
                    }
                    else phlist.WaitLoading();
                }
                else
                {
                    error = "Can not get on New button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_Verify_State()
        {
            try
            {
                //Save Release Phase ID

                textbox = phase.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    phaseId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Release Phase id:(" + phaseId + ")");
                }
                else error = "Cannot get textbox number.";

                //----------------------------------------------
                combobox = phase.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (!flag)
                    {
                        error = "Default State of Release Phase is not correct";
                    }
                }
                else
                {
                    error = "Can not get State combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Verify_Priority()
        {
            try
            {
                combobox = phase.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("3 - Medium");
                    if (!flag)
                    {
                        error = "Default Priority of Release Phase is not correct";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_Populate_ShortDescription()
        {
            try
            {
                textbox = phase.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Phase_ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_Populate_Description()
        {
            try
            {
                textarea = phase.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Phase_Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description";
                    }
                }
                else
                {
                    error = "Can not get Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_03_Populate_Priority()
        {
            try
            {
                combobox = phase.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Phase_Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_04_Populate_AssignmentGroup()
        {
            try
            {
                lookup = phase.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Assignment Group";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_05_Populate_Company()
        {
            try
            {

                lookup = phase.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_06_Click_Save_Button()
        {
            try
            {
                flag = phase.Save();
                if (!flag)
                {
                    error = "Can not Save Release Phase";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_Create_Change07()
        {
            try
            {
                flag = phase.CreateNormalChange();
                if (!flag)
                {
                    error = "Can not create Normal Change";
                }
                else phase.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Verify_Change07_Creation_Message()
        {
            try
            {
                //Save Change ID07
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId07 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 07 id:(" + chgId07 + ")");
                }
                else error = "Cannot get textbox number.";

                //Verify creation message

                flag = change.VerifyMessageInfo("Change " + chgId07 + " created");
                if (!flag)
                {
                    error = "Change creation message is not correct";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_03_Verify_Change07_Parent()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && phaseId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase Id.");
                    addPara.ShowDialog();
                    phaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------

                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = phaseId;
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Release Phase Parent is not correct";
                    }
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_04_Verify_Change07_Requested_By()
        {
            try
            {
                lookup = change.Lookup_RequestedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ReleaseManager");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Requested By is not correct";
                    }
                }
                else
                {
                    error = "Can not get Requested By lookup";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_05_Verify_Change07_State()
        {
            try
            {
                combobox = change.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "State is not correct";
                    }
                }
                else
                {
                    error = "Can not get State combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_06_Verify_Change07_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Phase_ShortDescription");
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Short Description is not correct";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_07_Verify_Change07_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Phase_Description");
                    flag = textarea.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Description is not correct";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_08_Verify_Change07_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Phase_Priority");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change riority is not correct";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_09_Verify_Change07_Assignment_Group()
        {
            try
            {

                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Assignment Group is not correct";
                    }
                }
                else
                {
                    error = "Can not get AssignmentGroup lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_01_Populate_Company()
        {
            try
            {
                lookup = change.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_02_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_03_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_04_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_05_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_06_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_Click_Save_button()
        {
            try
            {
                flag = change.Save();
                if (!flag)
                {
                    error = "Can not Save Release";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Search_And_Open_Release_Phase()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (!flag)
                {
                    error = "Can not open Phases";
                }
                else relist.WaitLoading();

                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && phaseId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase Id.");
                    addPara.ShowDialog();
                    phaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + phaseId;
                flag = phlist.SearchAndOpen("Number", phaseId, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Release Phase Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId07 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 07 Id.");
                    addPara.ShowDialog();
                    chgId07 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId07;
                flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                if (!flag)
                {
                    error = "Change 07 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Click_New_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "New");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_01_Create_Change08()
        {
            try
            {
                //Select Normal Change type
                string temp = Base.GData("ChangeType");
                flag = change.Select_Change_Type(temp);
                if (!flag) error = "Error when select change type.";
                else change.WaitLoading();

                //Save Change ID08
                textbox = change.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    chgId08 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id 08 id:(" + chgId08 + ")");
                }
                else error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_02_Populate_Company()
        {
            try
            {

                lookup = change.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not select Company in lookup";
                    }
                }
                else
                {
                    error = "Can not get Company lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_03_Verify_Change_Type()
        {
            try
            {
                combobox = change.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Normal";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Change Type is not correct";
                    }
                }
                else
                {
                    error = "Can not get Change Type combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_04_Populate_Category()
        {
            try
            {
                combobox = change.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Category";
                    }
                }
                else
                {
                    error = "Can not get Category combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_05_Populate_Priority()
        {
            try
            {
                combobox = change.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Priority");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select priority";
                    }
                }
                else
                {
                    error = "Can not get Priority combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_06_Populate_Urgency()
        {
            try
            {
                combobox = change.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Can not select Urgency";
                    }
                }
                else
                {
                    error = "Can not get Urgency combobox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_07_Populate_Short_Description()
        {
            try
            {
                textbox = change.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ShortDescription");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Short Description textbox";
                    }
                }
                else
                {
                    error = "Can not get Short Description textbox";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_08_Populate_Description()
        {
            try
            {
                textarea = change.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("Description");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not populate Description textarea";
                    }
                }
                else
                {
                    error = "Can not get Description textarea";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_09_Populate_Assignment_Group()
        {
            try
            {
                lookup = change.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeAssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Can not populate Assignment Group lookup";
                    }
                }
                else
                {
                    error = "Can not get Assignment Group lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_10_Populate_Planning_Dates()
        {
            try
            {
                flag = change.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = change.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = change.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_11_Populate_Justification()
        {
            try
            {
                flag = change.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = change.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_12_Submit_Change08()
        {
            try
            {
                button = change.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Can not click on Submit button";
                    }
                    else change.WaitLoading();
                }
                else
                {
                    error = "Can not get Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Search_And_Open_Release_Phase()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (!flag)
                {
                    error = "Can not open Phases";
                }
                else phlist.WaitLoading();

                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && phaseId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase Id.");
                    addPara.ShowDialog();
                    phaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + phaseId;
                flag = phlist.SearchAndOpen("Number", phaseId, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Release Phase Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId08 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 08 Id.");
                    addPara.ShowDialog();
                    chgId08 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId08;
                flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                if (!flag)
                {
                    error = "Change 08 in related list is not correct";
                }
                phase.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_01_Click_Edit_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_02_Relate_Change03()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                flag = mb.Add_Members(chgId03);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not relate Changes to Release";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_03_Relate_Change04()
        {
            try
            {

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
                //------------------------------------------------

                flag = mb.Add_Members(chgId04);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not relate Changes to Release";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId07 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 07 Id.");
                    addPara.ShowDialog();
                    chgId07 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId08 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 08 Id.");
                    addPara.ShowDialog();
                    chgId08 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId03;
                flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId04;
                    flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                    if (flag)
                    {
                        condition = "Number=" + chgId07;
                        flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                        if (flag)
                        {
                            condition = "Number=" + chgId08;
                            flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                            if (!flag)
                            {
                                error = "Change 08 in related list is not correct";
                            }
                        }
                        else
                        {
                            error = "Change 07 in related list is not correct";
                        }
                    }
                    else
                    {
                        error = "Change 04 in related list is not correct";
                    }
                }
                else
                {
                    error = "Change 03 in related list is not correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_Open_Change03()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId03 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 03 Id.");
                    addPara.ShowDialog();
                    chgId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId03;
                flag = phase.RelatedTableOpenRecord("Change Requests", condition, "Number");
                if (!flag)
                {
                    error = "Can not open Change 03 in Change Requests related tab";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Remove_Parent()
        {
            try
            {
                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText("", true);
                    if (!flag)
                    {
                        error = "Can not remove Parent of Change 03";
                    }
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_01_Click_Save_Button()
        {
            try
            {
                flag = change.Save();
                if (!flag)
                {
                    error = "Can not Save Change 03";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_02_Search_And_Open_Release_Phase()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Phases");
                if (!flag)
                {
                    error = "Can not open Phases";
                }
                else phlist.WaitLoading();
                //Search and open Release

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && phaseId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Release Phase Id.");
                    addPara.ShowDialog();
                    phaseId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                temp = "Number=" + phaseId;
                flag = phlist.SearchAndOpen("Number", phaseId, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Release Phase Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_01_Click_Edit_Button()
        {
            try
            {
                flag = release.RelatedTab_Click_Button("Change Requests", "Edit...");
                if (!flag)
                {
                    error = "Change in related list is not correct";
                }
                else release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_02_Remove_Change04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                flag = mb.Delete_Members(chgId04);
                if (flag)
                {
                    mb.WaitLoading();
                }
                else
                {
                    error = "Can not remove Changes to Release";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Verify_Change_Requests_Tab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId07 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 07 Id.");
                    addPara.ShowDialog();
                    chgId07 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId08 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 08 Id.");
                    addPara.ShowDialog();
                    chgId08 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                string condition = "Number=" + chgId07;
                flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                if (flag)
                {
                    condition = "Number=" + chgId08;
                    flag = phase.Verify_RelatedTable_Row("Change Requests", condition);
                    if (!flag)
                    {
                        error = "Change 08 in related list is not correct";
                    }
                    else
                    {
                        error = "Change 07 in related list is not correct";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_01_Search_And_Open_Change04()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (!flag)
                {
                    error = "Can not open Change";
                }
                else changelist.WaitLoading();
                //Search and open Change 04

                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && chgId04 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change 04 Id.");
                    addPara.ShowDialog();
                    chgId04 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                temp = "Number=" + chgId04;
                flag = changelist.SearchAndOpen("Number", chgId04, temp, "Number");
                if (!flag)
                {
                    error = "Can not open Change 04 Id";
                }
                else change.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_02_Verify_Change04_Parent()
        {
            try
            {
                lookup = change.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Text.Equals("");
                    if (!flag)
                    {
                        error = "Parent should be null";
                    }
                    else change.WaitLoading();
                }
                else
                {
                    error = "Can not get Parent lookup";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_64_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
