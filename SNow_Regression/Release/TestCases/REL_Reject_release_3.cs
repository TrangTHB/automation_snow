﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace Release
{
    [TestFixture]
    public class REL_Reject_release_3
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Release Id: " + releaseid);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;

        SNow.snobutton button;
        SNow.snotextarea textarea;
        

        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.ItilList approvalList = null;
        SNow.Itil release = null;
        SNow.ItilList relist = null;
        //------------------------------------------------------------------
        string releaseid;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                release = new SNow.Itil(Base, "Release");
                relist = new SNow.ItilList(Base,"Release List");
                approvalList = new SNow.ItilList(Base, "Approval list");


                //------------------------------------------------------------------
                releaseid = string.Empty;

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Can not login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ReleaseUser()
        {
            try
            {
                string temp = Base.GData("ReleaseManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_SelectReleaseModule()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                {
                    relist.WaitLoading();
                    string temp = relist.List_Title().MyText.ToLower();
                    if (!temp.Contains("releases"))
                    {
                        flag = false;
                        error = "-*- Error: The list's name is incorrect";
                    }
                    else
                        flag = true;
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_CreateNewRelease()
        {
            try
            {
                button = relist.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    release.WaitLoading();
                }
                else
                    error = "-*- Error: Cannot get new button on release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_VerifyApprovalState()
        {
            try
            {
                combobox = release.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Not Yet Requested");
                    if (flag)
                        System.Console.WriteLine("-*-Passed: The Approval value is correct - " + combobox.Text);
                    else
                        error = "-*- Error: The Approval value is incorrect - "+ combobox.Text;
                }
                else
                    error = "-*- Error: Cannot get approval combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_VerifyReleaseState()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (flag)
                        System.Console.WriteLine("-*-Passed: The state value is correct - " + combobox.Text);
                    else
                        error = "-*- Error: The state value is incorrect - " + combobox.Text;
                }
                else
                    error = "-*- Error: Cannot get state combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
       
        [Test]
        public void Step_008_SelectCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = release.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "-*- Error: Cannot select company.";
                }
                else
                    error = "-*- Error: Cannot get company lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_InputShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox= release.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input short description.";
                }
                else
                    error = "-*- Error: Cannot get short description textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SelectAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = release.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "-*- Error: Cannot input assignment group.";
                }
                else
                    error = "-*- Error: Cannot get assignment lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SaveRelease()
        {
            try
            {
                textbox = release.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    releaseid = textbox.Text;
                    flag = release.Save();
                    if (!flag)
                        error = "-*- Error: Cannot save release.";
                    else release.WaitLoading();
                }
                else
                    error = "Cannot get number textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SelectStateAwaitingApproval()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Approval");
                    if (!flag)
                        error = "-*- Error: Cannot select state value";
                    else
                    {
                        flag = release.Save();
                        release.WaitLoading();
                    }
                        
                }
                else
                    error = "-*- Error: Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_VerifyApprovalField()
        {
            try
            {
                combobox = release.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Requested");
                    if (!flag)
                        error = "-*- Error: Incorrect approval value. [Runtime]:"+ combobox.Text;
                }
                else
                    error = "-*- Error: Cannot get approval combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_VerifyApproverTab_Requested()
        {
            try
            {
                flag = release.Search_Verify_RelatedTable_Row("Approvers", "Approver", Base.GData("Approver"), "State=Requested|Assignment group=" + Base.GData("ApprovalGroup"));
                if (!flag)
                    error = "-*-Error: Cannot find any record as conditions.";
                else
                {
                    flag = release.Search_Verify_RelatedTable_Row("Approvers", "Group Assignment group", "!=" + Base.GData("ApprovalGroup"), "State=Requested", true);
                    if (!flag)
                        error = "-*- Error: Could find approval request of another group";
                    else
                    {
                        flag = release.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", "State=Requested");
                        if (!flag)
                            error = "Cannot find any record in requested state.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_ImpersonateUser_ApproverUser()
        {
            try
            {
                string temp = Base.GData("Approver");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_NavigateToMyApprovals()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                    approvalList.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_SearchAndOpenReleaseApproval()
        {
            try
            {
                string temp = Base.GData("Debug");

                if(temp.ToLower() == "yes" && (releaseid == string.Empty || releaseid == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input release id:");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                    //----------------------------------------------------------------------------------
                }
                flag = approvalList.SearchAndOpen("Approval for",releaseid,"State=Requested","State");
                if (!flag)
                    error = "-*-Error = Cannot find release approval as condition.";
                else
                    release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_AddRejectComment()
        {
            try
            {
                textarea = release.Textarea_AdditionalComments();
                flag = textarea.Existed;
                if (!flag)
                    error = "-*-Error: Cannot get comment textarea.";
                else
                {
                    flag = textarea.SetText(Base.GData("Comment"));
                    if (!flag)
                        error = "-*- Error: Cannot not add comment.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_ClickRejectButton()
        {
            try
            {
                button = release.Button_Reject();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    approvalList.WaitLoading();
                }
                else
                    error = "Cannot get reject button.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_NavigateToMyApprovals()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                    approvalList.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_03_VerifyNORequestedApproval()
        {
            try
            {
                string temp = Base.GData("Debug");

                if (temp.ToLower() == "yes" && (releaseid == string.Empty || releaseid == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input release id:");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                    //----------------------------------------------------------------------------------
                }
                flag = approvalList.SearchAndOpen("Approval for", releaseid, "State=Requested", "State");
                if (flag)
                { error = "-*-Error = Can find release approval as condition."; flag = false; }
                else
                {
                    release.WaitLoading(); flag = true;
                    System.Console.WriteLine("Passed: No requested record found.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_ImpersonateUser_ReleaseUser()
        {
            try
            {
                string temp = Base.GData("ReleaseManager");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_NavigateToReleases()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                    relist.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_SearchAndOpenRelease()
        {
            try
            {
                string temp = Base.GData("Debug");

                if (temp.ToLower() == "yes" && (releaseid == string.Empty || releaseid == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input release id:");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                    //----------------------------------------------------------------------------------
                }
                flag = relist.SearchAndOpen("Number", releaseid, "Number="+releaseid, "Number");
                if (!flag)
                    error = "-*-Error = Cannot fine release as condition.";
                else
                    release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_VerifyState()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Scoping");
                    if (!flag)
                        error = "The current state is incorrect.";
                }
                else
                    error = "Cannot get state combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_VerifyApproval()
        {
            try
            {
                combobox = release.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Rejected");
                    if (!flag)
                        error = "The current approval value is incorrect.";
                }
                else
                    error = "Cannot get approval combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_VerifyApproversTab_NotRequest()
        {
            try
            {
                flag = release.Search_Verify_RelatedTable_Row("Approvers", "State", "=not requested", "State=Not Yet Requested");
               if (!flag)
                { error = "Cannot find any record in Not Yet Requested state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SelectStateAwaitingApproval()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Approval");
                    if (!flag)
                        error = "-*- Error: Cannot select state value";
                    else
                    {
                        flag = release.Save();
                        release.WaitLoading();
                    }

                }
                else
                    error = "-*- Error: Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_VerifyApprovalField()
        {
            try
            {
                combobox = release.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Requested");
                    if (!flag)
                        error = "-*- Error: Incorrect approval value. [Runtime]:" + combobox.Text;
                }
                else
                    error = "-*- Error: Cannot get approval combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_VerifyApproverTab_Requested()
        {
            try
            {
                flag = release.Search_Verify_RelatedTable_Row("Approvers", "Approver", Base.GData("Approver"), "State=Requested|Assignment group=" + Base.GData("ApprovalGroup"));
                if (!flag)
                    error = "-*-Error: Cannot find any record as conditions.";
                else
                {
                    flag = release.Search_Verify_RelatedTable_Row("Approvers", "Group Assignment group", "!=" + Base.GData("ApprovalGroup"), "State=Requested", true);
                    if (!flag)
                        error = "-*- Error: Could find approval request of another group";
                    else
                    {
                        flag = release.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", "State=Requested");
                        if (!flag)
                        {
                            error = "Cannot find any record in requested state.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_ImpersonateUser_ApproverUser()
        {
            try
            {
                string temp = Base.GData("Approver");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_NavigateToMyApprovals()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                    approvalList.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_SearchAndOpenReleaseApproval()
        {
            try
            {
                string temp = Base.GData("Debug");

                if (temp.ToLower() == "yes" && (releaseid == string.Empty || releaseid == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input release id:");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                    //----------------------------------------------------------------------------------
                }
                flag = approvalList.SearchAndOpen("Approval for", releaseid, "State=Requested", "State");
                if (!flag)
                    error = "-*-Error = Cannot fine release approval as condition.";
                else
                    release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_ApproveTheRequest()
        {
            try
            {
                button = release.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    approvalList.WaitLoading();
                }
                else
                    error = "Cannot get approve button.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_ImpersonateUser_ReleaseUser()
        {
            try
            {
                string temp = Base.GData("ReleaseManager");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_NavigateToReleases()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Release v2", "Releases");
                if (flag)
                    relist.WaitLoading();
                else
                    error = "-*- Error: Cannot navigate to release list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_SearchAndOpenRelease()
        {
            try
            {
                string temp = Base.GData("Debug");

                if (temp.ToLower() == "yes" && (releaseid == string.Empty || releaseid == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input release id:");
                    addPara.ShowDialog();
                    releaseid = addPara.value;
                    addPara.Close();
                    addPara = null;
                    //----------------------------------------------------------------------------------
                }
                flag = relist.SearchAndOpen("Number", releaseid, "Number=" + releaseid, "Number");
                if (!flag)
                    error = "-*-Error = Cannot fine release as condition.";
                else
                    release.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_VerifyState()
        {
            try
            {
                combobox = release.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Work in Progress");
                    if (!flag)
                        error = "The current state is incorrect.";
                }
                else
                    error = "Cannot get state combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_VerifyApproval()
        {
            try
            {
                combobox = release.Combobox_Approval();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Approved");
                    if (!flag)
                        error = "The current approval value is incorrect.";
                }
                else
                    error = "Cannot get approval combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_VerifyApproversTab_NoRequired()
        {
            try
            {
                flag = release.Search_Verify_RelatedTable_Row("Approvers", "Approver", Base.GData("Approver"), "State=Approved");
                if (!flag)
                    error = "-*-Error: Cannot find any record as conditions.";
                else
                {
                    flag = release.Search_Verify_RelatedTable_Row("Approvers", "State", "=not_required", "State=No Longer Required");
                    if (!flag)
                    {
                        error = "Cannot find any record in no longer required state.";
                    }    
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
