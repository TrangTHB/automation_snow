﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Incident
{
    [TestFixture]
    public class Demand_e2e_5
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Idea Id: " + ideaId);
            System.Console.WriteLine("Finished - Demand Id: " + demandId);
            System.Console.WriteLine("Finished - Resource Plan Id: " + resourcePlanId);
            System.Console.WriteLine("Finished - Planned Cost: " + plannedCost);
            System.Console.WriteLine("Finished - Current Datetime: " + currentDateTime);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        
        string ideaId, demandId, resourcePlanId, plannedCost, currentDateTime;
        Login login;
        Home home;
        Itil demand;
        ItilList list;
        EmailList emailList;
        Email email;
        //-----------------------------
        snotextbox textbox = null;    
        snolookup lookup = null;
        snocombobox combobox = null; 
        snobutton button = null;
        
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                demand = new Itil(Base, "Demand");
                list = new ItilList(Base, "List");
                email = new Email(Base, "Email");
                emailList = new EmailList(Base, "Email list");
                //-----------------------------------------------------
                ideaId = string.Empty;
                demandId = string.Empty;
                resourcePlanId = string.Empty;
                currentDateTime = string.Empty;
                plannedCost = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else 
                {
                    login.WaitLoading();
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                
                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_Requester()
        {
            try
            {
                string temp = Base.GData("Requester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Open_New_Idea()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Ideas", "Create New");
                if (!flag)
                    error = "Error when create new idea.";
                else
                {
                    demand.WaitLoading();
                    string type = Base.GData("Idea_Type");
                    flag = demand.Select_Ideas_Type(type);
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Cannot select Idea type.";
                }
                    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Populate_Short_Description()
        {
            try
            {
                currentDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                string value = "Request Name:=Auto_" + currentDateTime;
                flag = demand.Input_Value_For_Controls(value);
                if (!flag)
                    error = "Error when input optional data.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Input_Optional_Data()
        {
            try
            {
                string value = Base.GData("Optional_Data");
                flag = demand.Input_Value_For_Controls(value);
                if (!flag)
                    error = "Error when input optional data.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Input_Mandatory_Data()
        {
            try
            {
                flag = demand.Input_Mandatory_Fields("auto");
                if (!flag)
                    error = "Error when input mandatory data.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Submit_Idea()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (currentDateTime == null || currentDateTime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current datetime.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                snobutton button = demand.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        list.WaitLoading();
                        string condition = "Short Description=Auto_" + currentDateTime;
                        flag = list.SearchAndVerify("Short Description", "Auto_" + currentDateTime, condition);
                        ideaId = list.RelatedTableGetCell(condition, "Number");
                    }
                    else error = "Error when submit idea.";
                }
                    
                else error = "Cannot get button submit.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Open_Idea()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (ideaId == null || ideaId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input idea Id.");
                    addPara.ShowDialog();
                    ideaId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = list.Open("Number=" + ideaId, "Number");
                if (!flag) error = "Error when search and open idea (id:" + ideaId + ")";
                else demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Verify_Activity_Note_11()
        {
            try
            {
                string temp = Base.GData("Activity_11");
                flag = demand.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_Request_Type_ReadOnly()
        {
            try
            {
                Thread.Sleep(10000);
                combobox = demand.Combobox_RequestType();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.ReadOnly;
                    if (!flag)
                        error = "Request Type is NOT readonly. Expected is readonly.";
                }
                else error = "Cannot get combobox Request Type.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_ImpersonateUser_Demand_Manager()
        {
            try
            {
                string temp = Base.GData("Manager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Search_And_Open_Idea()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (ideaId == null || ideaId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input idea Id.");
                    addPara.ShowDialog();
                    ideaId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Ideas", "All");
                if (flag)
                {
                    list.WaitLoading();
                    flag = list.SearchAndOpen("Number", ideaId, "Number=" + ideaId, "Number");
                    if (!flag) error = "Error when search and open idea (id:" + ideaId + ")";
                    else demand.WaitLoading();
                }
                else error = "Error when select open idea.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Click_Accept_Button()
        {
            try
            {
                button = demand.Button_Accept();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Accept.";
                }
                else error = "Cannot get button Accept.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Verify_Demand_Auto_Created()
        {
            try
            {
                string define = ".outputmsg.outputmsg_info.notification.notification-info";
                snoelement ele = (snoelement)Base.SNGObject("info", "element", By.CssSelector(define), null, snobase.MainFrame);
                flag = ele.Existed;
                if (flag)
                {
                    string content = ele.MyText.Trim();
                    if (content != null && content != string.Empty)
                    {
                        string[] arr = content.Split(' ');
                        foreach (string str in arr)
                        {
                            if (str.Contains("DMND"))
                            {
                                demandId = str;
                                break;
                            }
                        }

                        if (demandId == null || demandId == string.Empty)
                        {
                            flag = false;
                            error = "No demand auto created.";
                        }
                    }

                    else { flag = false; error = "Invalid message info."; }
                }
                else error = "Not found message info.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Back_To_List()
        {
            try
            {
                button = demand.Button_Back();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        list.WaitLoading();
                }
                else error = "Cannot get button Back.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Verify_Stage_State_Of_Idea_DemandDraf_Accepted()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (ideaId == null || ideaId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input idea Id.");
                    addPara.ShowDialog();
                    ideaId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + ideaId + "|Stage=@@Demand Draft|State=Accepted";
                flag = list.VerifyRow(condition);
                if (!flag)
                    error = "Invalide idea stage and state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Open_Demand_Form_By_Name()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (currentDateTime == null || currentDateTime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current date time.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Demand=Auto_" + currentDateTime;
                flag = list.Open(condition, "Demand");
                if (!flag)
                    error = "Cannot open demand.";
                else
                    demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_Number()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (demandId == null || demandId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                textbox = demand.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(demandId, true);
                    if (!flag)
                    {
                        error = "Invalid demand number.";
                        flagExit = false;
                    }
                }
                else
                    error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Populate_Demand_Manager_Assignment_Group()
        {
            try
            {
                string temp = Base.GData("Assignment_Group");
                lookup = demand.Lookup_DemandManagerAssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Cannot populate demand manager assignment group";
                }
                else error = "Cannot get lookup demand manager assignment group.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Save_Demand()
        {
            try
            {
                flag = demand.Save();
                if (!flag) { error = "Error when save demand."; }
                else demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Populate_Demand_Manager()
        {
            try
            {
                string temp = Base.GData("Demand_Manager");
                lookup = demand.Lookup_DemandManager();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Cannot populate demand manager";
                }
                else error = "Cannot get lookup demand manager.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Save_Demand()
        {
            try
            {
                flag = demand.Save();
                if (!flag) { error = "Error when save demand."; }
                else demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Click_New_Resource_Plan()
        {
            try
            {
                flag = demand.RelatedTab_Click_Button("Resource Plans", "New");
                if (!flag) error = "Cannot click New button on Resource Plans.";
                else demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Get_Resource_Plan_Number()
        {
            try
            {
                textbox = demand.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    resourcePlanId = textbox.Text;
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Verify_Default_Value_Resouce_Type_Is_Group()
        {
            try
            {
                combobox = demand.Combobox_ResourceType();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Group");
                    if (!flag)
                        error = "Invalid default value of resource type.";
                }
                else error = "Cannot get combobox Resource Type.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Verify_Default_Value_Task()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (demandId == null || demandId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = demand.Lookup_Task();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(demandId);
                    if (!flag)
                        error = "Invalid default value of task.";
                }
                else error = "Cannot get lookup task.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Populate_Group()
        {
            try
            {
                string temp = Base.GData("Assignment_Group");
                lookup = demand.Lookup_Group();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Cannot populate group value.";
                }
                else
                {
                    error = "Cannot get combobox group.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Populate_Planned_Hours()
        {
            try
            {
                string temp = Base.GData("Planned_Hours");
                textbox = demand.Textbox_PlannedHours();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = "Cannot populate planned hours value."; 
                }
                else
                {
                    error = "Cannot get textbox planned hours.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Populate_StartDate_EndDate()
        {
            try
            {
                string start = DateTime.Now.ToString("yyyy-MM-dd");
                string end = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                snodate date = demand.Date_StartDate();
                flag = date.Existed;
                if (flag)
                {
                    flag = date.SetText(start);
                    if (!flag)
                        error = "Cannot populate start date.";
                    else
                    {
                        date = demand.Date_EndDate();
                        flag = date.Existed;
                        if (flag)
                        {
                            flag = date.SetText(end);
                            if (!flag)
                                error = "Cannot populate end date.";
                        }
                        else error = "Cannot get date End date.";
                    }
                }
                else error = "Cannot get date start date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Save_Resource_Plan()
        {
            try
            {
                flag = demand.Save();
                if (!flag) { error = "Error when save resource plan."; }
                else demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Verify_State_Planning()
        {
            try
            {
                combobox = demand.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Planning");
                    if (!flag)
                        error = "Invalid State value.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Click_Request_Button()
        {
            try
            {
                button = demand.Button_Request();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Request.";
                }
                else error = "Cannot get button Request.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Verify_State_Requested()
        {
            try
            {
                combobox = demand.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Requested");
                    if (!flag)
                        error = "Invalid State value.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_ImpersonateUser_Resource_Manager()
        {
            try
            {
                string temp = Base.GData("Resource_Manager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Search_And_Open_Demand()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (demandId == null || demandId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Demand", "All");
                if (flag)
                {
                    list.WaitLoading();
                    flag = list.SearchAndOpen("Number", demandId, "Number=" + demandId, "Number");
                    if (!flag) error = "Error when search and open demand (id:" + demandId + ")";
                    else demand.WaitLoading();
                }
                else error = "Error when select open demand.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Open_Resource_Plan_Record()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (resourcePlanId == null || resourcePlanId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Resource Plan Id.");
                    addPara.ShowDialog();
                    resourcePlanId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = demand.RelatedTableOpenRecord("Resource Plans", "Number=" + resourcePlanId + "|State=Requested", "Number");
                if (!flag)
                    error = "Cannot open resource plan record.";
                else demand.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Click_Confirm_Button()
        {
            try
            {
                button = demand.Button_Confirm();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Confirm.";
                }
                else error = "Cannot get button Confirm.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Verify_State_Confirmed()
        {
            try
            {
                combobox = demand.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Confirmed");
                    if (!flag)
                        error = "Invalid State value.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Click_Allocate_Button()
        {
            try
            {
                button = demand.Button_Allocate();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Allocate.";
                }
                else error = "Cannot get button Allocate.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Verify_State_Allocated()
        {
            try
            {
                combobox = demand.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Allocated");
                    if (!flag)
                        error = "Invalid State value.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_045_Click_Complete_Button()
        //{
        //    try
        //    {
        //        button = demand.Button_Complete();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            flag = button.Click();
        //            if (flag)
        //                demand.WaitLoading();
        //            else
        //                error = "Error when click on button Complete.";
        //        }
        //        else error = "Cannot get button Complete.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_046_Verify_State_Completed()
        //{
        //    try
        //    {
        //        combobox = demand.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            flag = combobox.VerifyCurrentValue("Completed");
        //            if (!flag)
        //                error = "Invalid State value.";
        //        }
        //        else error = "Cannot get combobox State.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_ImpersonateUser_Demand_Manager()
        {
            try
            {
                string temp = Base.GData("Demand_Manager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_Search_And_Open_Demand()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (demandId == null || demandId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Demand", "All");
                if (flag)
                {
                    list.WaitLoading();
                    flag = list.SearchAndOpen("Number", demandId, "Number=" + demandId, "Number");
                    if (!flag) error = "Error when search and open demand (id:" + demandId + ")";
                    else demand.WaitLoading();
                }
                else error = "Error when select open demand.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Get_Planned_Cost_From_Resource_Plan_Record()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (resourcePlanId == null || resourcePlanId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Resource Plan Id.");
                    addPara.ShowDialog();
                    resourcePlanId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                plannedCost = demand.RelatedTable_Get_Cell_Text("Resource Plans", "Number=" + resourcePlanId, "Planned cost", "div[class='item_currency']");
                if (plannedCost == null || plannedCost == string.Empty)
                    flag = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Verify_PlannedCost_AllocatedCost_On_ResoureCostTab()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (plannedCost == null || plannedCost == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Planned Cost.");
                    addPara.ShowDialog();
                    plannedCost = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = demand.Select_Tab("Resource Cost");
                if (flag)
                {
                    snocurrency currency = demand.Textbox_PlannedCost();
                    flag = currency.Existed;
                    if (flag)
                    {
                        if (currency.Textbox_Value().Text.Trim() == plannedCost.Trim())
                        {
                            currency = demand.Textbox_AllocatedCost();
                            flag = currency.Existed;
                            if (flag)
                            {
                                if (currency.Textbox_Value().Text.Trim() != plannedCost.Trim())
                                {
                                    flag = false;
                                    error = "Invalid allocated cost on Resource Cost Tab.";
                                }

                            }
                            else error = "Cannot get textbox allocated cost.";
                        }
                        else { flag = false; error = "Invalid planned cost on Resource Cost Tab."; }
                    }
                    else error = "Cannot get textbox Planned Cost.";
                }
                else error = "Cannot select Resource Cost tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Click_SubmitDemand_Button()
        {
            try
            {
                button = demand.Button_SubmitDemand();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button submit demand.";
                }
                else error = "Cannot get button submit demand.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Verify_ProgressBar_Submitted()
        {
            try
            {
                flag = demand.Verify_CurrentProcess("Submitted");
                if (!flag)
                    error = "Invalid current process.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Click_Screen_Button()
        {
            try
            {
                button = demand.Button_Screen();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Screen.";
                }
                else error = "Cannot get button Screen.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_Verify_ProgressBar_Screening()
        {
            try
            {
                flag = demand.Verify_CurrentProcess("Screening");
                if (!flag)
                    error = "Invalid current process.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_Click_Qualify_Button()
        {
            try
            {
                button = demand.Button_Qualify();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Qualify.";
                }
                else error = "Cannot get button Qualify.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Verify_ProgressBar_Qualified()
        {
            try
            {
                flag = demand.Verify_CurrentProcess("Qualified");
                if (!flag)
                    error = "Invalid current process.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_Click_Approve_Button()
        {
            try
            {
                button = demand.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                        demand.WaitLoading();
                    else
                        error = "Error when click on button Approve.";
                }
                else error = "Cannot get button Approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Verify_ProgressBar_Approved()
        {
            try
            {
                flag = demand.Verify_CurrentProcess("Approved");
                if (!flag)
                    error = "Invalid current process.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_ImpersonateUser_Support_User()
        {
            try
            {
                string temp = Base.GData("Support_User");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_02_Filter_Email_Assignment_Sent_To_Group()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && demandId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Demand_Manager_Email");
                temp = "Subject;contains;" + demandId + "|and|Subject;contains;has been assigned to your team|and|" + "Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_03_Open_Email_Assignment_Sent_To_Group()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && demandId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + demandId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to group (assigned)."; flagExit = false; }
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_04_Verify_Subject()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && demandId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input demand Id.");
                    addPara.ShowDialog();
                    demandId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------   
                temp = "Demand " + demandId + " has been assigned to your team";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject value. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_05_Verify_Recipients()
        {
            try
            {

                string mail = Base.GData("Demand_Manager_Email");
                flag = email.VerifyRecipient(mail);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient value. Expected: [" + mail + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_07_Verify_Name()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && currentDateTime == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current date time.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                temp = "Name: Auto_" + currentDateTime;

                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Name value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_08_Verify_Assignment_Group()
        {
            try
            {
                string group = Base.GData("Assignment_Group");

                string temp = "Demand Manager Group: " + group;

                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid assignment group value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_09_Verify_Click_Here_To_View()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && currentDateTime == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input current date time.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                temp = "Click here to view Demand: Auto_" + currentDateTime;

                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid click here value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_10_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag)
                {
                    error = "Error when close email body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_02_Filter_Email_Assignment_Sent_To_Resource_Manager()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && resourcePlanId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Resource plan Id.");
                    addPara.ShowDialog();
                    resourcePlanId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("Resource_Manager_Email");
                temp = "Subject;contains;" + resourcePlanId + "|and|Subject;contains;has been assigned to you|and|" + "Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_03_Open_Email_Assignment_Sent_To_Resource_Manager()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && resourcePlanId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Resource plan Id.");
                    addPara.ShowDialog();
                    resourcePlanId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + resourcePlanId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to resource manager (assigned)."; flagExit = false; }
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_04_Verify_Subject()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && resourcePlanId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Resource plan Id.");
                    addPara.ShowDialog();
                    resourcePlanId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------   
                temp = "Resource Plan " + resourcePlanId + " has been assigned to you";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject value. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_05_Verify_Recipients()
        {
            try
            {

                string mail = Base.GData("Resource_Manager_Email");
                flag = email.VerifyRecipient(mail);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient value. Expected: [" + mail + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_063_06_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_07_Verify_Manager()
        {
            try
            {
                string manager = Base.GData("Resoure_Manager");

                string temp = "Manager: " + manager;

                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid manager value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_08_Verify_Note()
        {
            try
            {
                string temp = "Please review and take appropriate action";

                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid manager value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_09_Verify_Click_Here_To_View()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && resourcePlanId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Resource plan Id.");
                    addPara.ShowDialog();
                    resourcePlanId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                temp = "Click here to view Resource Plan: " + resourcePlanId;

                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid click here value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_10_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag)
                {
                    error = "Error when close email body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
    }
}
