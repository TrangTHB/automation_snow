﻿using SNow;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace Report
{
    #region TestFixture
    [TestFixture("001")]
    [TestFixture("002")]
    [TestFixture("003")]
    [TestFixture("004")]
    [TestFixture("005")]
    [TestFixture("006")]
    [TestFixture("007")]
    [TestFixture("008")]
    [TestFixture("009")]
    [TestFixture("010")]
    [TestFixture("011")]
    [TestFixture("012")]
    [TestFixture("013")]
    [TestFixture("014")]
    [TestFixture("015")]
    [TestFixture("016")]
    [TestFixture("017")]
    [TestFixture("018")]
    [TestFixture("019")]
    [TestFixture("020")]
    [TestFixture("021")]
    [TestFixture("022")]
    [TestFixture("023")]
    [TestFixture("024")]
    [TestFixture("025")]
    [TestFixture("026")]
    [TestFixture("027")]
    [TestFixture("028")]
    [TestFixture("029")]
    [TestFixture("030")]
    [TestFixture("031")]
    [TestFixture("032")]
    [TestFixture("033")]
    [TestFixture("034")]
    [TestFixture("035")]
    [TestFixture("036")]
    [TestFixture("037")]
    [TestFixture("038")]
    [TestFixture("039")]
    [TestFixture("040")]
    [TestFixture("041")]
    [TestFixture("042")]
    [TestFixture("043")]
    [TestFixture("044")]
    [TestFixture("045")]
    [TestFixture("046")]
    [TestFixture("047")]
    [TestFixture("048")]
    [TestFixture("049")]
    [TestFixture("050")]
    [TestFixture("051")]
    [TestFixture("052")]
    [TestFixture("053")]
    [TestFixture("054")]
    [TestFixture("055")]
    [TestFixture("056")]
    [TestFixture("057")]
    [TestFixture("058")]
    [TestFixture("059")]
    [TestFixture("060")]
    [TestFixture("061")]
    [TestFixture("062")]
    [TestFixture("063")]
    [TestFixture("064")]
    [TestFixture("065")]
    [TestFixture("066")]
    [TestFixture("067")]
    [TestFixture("068")]
    [TestFixture("069")]
    [TestFixture("070")]
    [TestFixture("071")]
    [TestFixture("072")]
    [TestFixture("073")]
    [TestFixture("074")]
    [TestFixture("075")]
    [TestFixture("076")]
    [TestFixture("077")]
    [TestFixture("078")]
    [TestFixture("079")]
    [TestFixture("080")]
    [TestFixture("081")]
    [TestFixture("082")]
    [TestFixture("083")]
    [TestFixture("084")]
    [TestFixture("085")]
    [TestFixture("086")]
    [TestFixture("087")]
    [TestFixture("088")]
    [TestFixture("089")]
    [TestFixture("090")]
    [TestFixture("091")]
    [TestFixture("092")]
    [TestFixture("093")]
    [TestFixture("094")]
    [TestFixture("095")]
    [TestFixture("096")]
    [TestFixture("097")]
    [TestFixture("098")]
    [TestFixture("099")]
    [TestFixture("100")]
    [TestFixture("101")]
    [TestFixture("102")]
    [TestFixture("103")]
    [TestFixture("104")]
    [TestFixture("105")]
    [TestFixture("106")]
    [TestFixture("107")]
    [TestFixture("108")]
    [TestFixture("109")]
    [TestFixture("110")]
    [TestFixture("111")]
    [TestFixture("112")]
    [TestFixture("113")]
    [TestFixture("114")]
    [TestFixture("115")]
    [TestFixture("116")]
    [TestFixture("117")]
    [TestFixture("118")]
    [TestFixture("119")]
    [TestFixture("120")]
    [TestFixture("121")]
    [TestFixture("122")]
    [TestFixture("123")]
    [TestFixture("124")]
    [TestFixture("125")]
    [TestFixture("126")]
    [TestFixture("127")]
    [TestFixture("128")]
    [TestFixture("129")]
    [TestFixture("130")]
    [TestFixture("131")]
    [TestFixture("132")]
    [TestFixture("133")]
    [TestFixture("134")]
    [TestFixture("135")]
    [TestFixture("136")]
    [TestFixture("137")]
    [TestFixture("138")]
    [TestFixture("139")]
    [TestFixture("140")]
    [TestFixture("141")]
    [TestFixture("142")]
    [TestFixture("143")]
    [TestFixture("144")]
    [TestFixture("145")]
    [TestFixture("146")]
    [TestFixture("147")]
    [TestFixture("148")]
    [TestFixture("149")]
    [TestFixture("150")]
    [TestFixture("151")]
    [TestFixture("152")]
    [TestFixture("153")]
    [TestFixture("154")]
    [TestFixture("155")]
    [TestFixture("156")]
    [TestFixture("157")]
    [TestFixture("158")]
    [TestFixture("159")]
    [TestFixture("160")]
    [TestFixture("161")]
    #endregion
    class report_validation_can_open_b_main
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        string sRow = string.Empty;
        public report_validation_can_open_b_main(string _Row)
        {
            if (_Row != string.Empty)
                this.sRow = (int.Parse(_Row)).ToString();
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC, sRow);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        
        
        //------------------------------------------------------------------
        Home home = null;
        ReportList reportList = null;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                home = new Home(Base);
                reportList = new ReportList(Base, "Report list");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenReportList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Reports", "View / Run");
                if (flag == true)
                {
                    Thread.Sleep(2000);
                }
                else
                {
                    error = "Cannot open the report list";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_01_ClickAll()
        {
            try
            {
                snoelement ele = reportList.Element_AllReport();
                flag = ele.Existed;

                if(flag)
                {
                    ele.Click();
                    Thread.Sleep(2000);
                }
                else { error = "Cannot get all."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_02_Search_Report()
        {
            try
            {
                string temp = Base.GData("Report Name");
                snotextbox textbox = reportList.Textbox_ReportSearch();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp, true);
                    reportList.WaitLoading(true);
                    Thread.Sleep(10000);
                }
                else { error = "Cannot get textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_Verify_Duplicate_Report()
        {
            try
            {
                string temp = Base.GData("Report Name");
                flag = reportList.Verify_D_Report(temp);
                if (!flag)
                {
                    error = "Report duplicated.";
                    flagExit = false;
                }        

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_ClickOn_Report()
        {
            try
            {
                string temp = Base.GData("Report Name");
                snoelement ele = reportList.Element_ReportLink(temp);
                flag = ele.Existed;
                if (flag)
                {
                    ele.Click();
                    reportList.WaitLoading(true);
                }
                else {error = "Not found report in list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_Verify_Report_Tilte()
        {
            try
            {
                string temp = Base.GData("Report Name");
                flag = reportList.VerifyReportTitle(temp);
                if (!flag)
                    error = "Invalid report title or cannot open report.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
