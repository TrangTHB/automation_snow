﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
using System.Windows.Forms;

namespace Configuration
{
    [TestFixture]
    class CONF_up_down_stream_relationship_27
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Please note the Postfix: " + postfix);
            System.Console.WriteLine("Finished - Configuration Item 1: " + CI01Name);
            System.Console.WriteLine("Finished - Configuration Item 2: " + CI02Name);
            System.Console.WriteLine("Finished - Configuration Item 3: " + CI03Name);
            System.Console.WriteLine("Finished - Configuration Item 4: " + CI04Name);
            System.Console.WriteLine("Finished - Configuration Item 5: " + CI05Name);
            System.Console.WriteLine("Finished - Configuration Item 6: " + CI06Name);


            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snobutton button;
        snocombobox combobox;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        ItilList list = null;
        Itil gui = null;
        Member member = null;
        //------------------------------------------------------------------
        string postfix, CI01Name, CI02Name, CI03Name, CI04Name, CI05Name, CI06Name;
        Dictionary <string, oelement> listOfControl = null;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                list = new ItilList(Base, "List");
                gui = new Itil(Base, "Gui");
                member = new Member(Base);
                //------------------------------------------------------------------
                postfix = string.Empty;
                listOfControl = new Dictionary<string, oelement>();
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                postfix = System.DateTime.Now.ToString("yyMMdd_HHmmss");
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else { error = "Cannot login to system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_Impersonate_AssetManager()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("AssetMgr"));
                if (!flag) { error = "Cannot impersonate Asset Manager"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_005_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_01_CI01_Open_NewBusinessService()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Configuration", "Services");
                if (flag)
                {
                    list.WaitLoading();

                    // Click on New button
                    button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot click on New button"; }
                    }
                    else { error = "Cannot get button New"; }
                }
                else { error = "Cannot open business services."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_02_CI01_PopulateMandatoryFields()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //CI Name                 
                temp = Base.GData("CI Prefix Name");
                CI01Name = temp + "01 " + postfix;
                //**************************************************************
                textbox = gui.Textbox_Name_CI();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(CI01Name);
                    if (flag)
                    {
                        lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (!flag)
                            {
                                error = "Cannot populate company value.";
                            }
                        }
                        else { error = "Cannot get lookup company."; }
                    }
                    else { error = "Cannot populate name value."; }
                }
                else { error = "Cannot get textbox name."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_03_CI01_SubmitCI()
        {
            try
            {
                flag = gui.Save(true);
                if (!flag)
                {
                    CI01Name = "";
                    error = "Cannot save CI01";
                    flagExit = false;
                }
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_01_CI02_Open_NewBusinessService()
        {
            try
            {
                // Open  Computer list page
                flag = home.LeftMenuItemSelect("Configuration", "Services");
                if (flag)
                {
                    list.WaitLoading();

                    // Click on New button
                    button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot click on New button"; }
                    }
                    else { error = "Cannot get button New"; }
                }
                else { error = "Error when create new Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_02_CI02_PopulateMandatoryFields()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //CI Name
                temp = Base.GData("CI Prefix Name");
                CI02Name = temp + "02 " + postfix;
                //**************************************************************
                textbox = gui.Textbox_Name_CI();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(CI02Name);
                    if (flag)
                    {
                        lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (!flag)
                            {
                                error = "Cannot populate company value.";
                            }
                        }
                        else { error = "Cannot get lookup company."; }
                    }
                    else { error = "Cannot populate name value."; }
                }
                else { error = "Cannot get textbox name."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_03_CI02_SubmitCI()
        {
            try
            {
                flag = gui.Save(true);
                if (!flag)
                {
                    CI02Name = "";
                    error = "Cannot save CI02";
                    flagExit = false;
                }
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_01_CI03_Open_NewApplicationCI()
        {
            try
            {
                // Open  Computer list page
                flag = home.LeftMenuItemSelect("Application", "Applications");
                if (flag)
                {
                    list.WaitLoading();

                    // Click on New button
                    button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot click on New button"; }
                    }
                    else { error = "Cannot get button New"; }
                }
                else { error = "Error when create new Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_02_CI03_PopulateMandatoryFields()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //CI Name
                temp = Base.GData("CI Prefix Name");
                CI03Name = temp + "03 " + postfix;
                //**************************************************************
                textbox = gui.Textbox_Name_CI();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(CI03Name);
                    if (flag)
                    {
                        lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (flag)
                            {
                                //Populate Asset Tag
                                temp = "ASSTAG_03_" + postfix;
                                //**************************************************************
                                textbox = gui.Textbox_AssetTag();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    flag = textbox.SetText(temp);
                                    if (!flag)
                                    {
                                        error = "Cannot populate asset tag.";
                                    }
                                }
                                else { error = "Cannot get textbox asset tag."; }
                            }
                            else { error = "Cannot populate company value."; }
                        }
                        else { error = "Cannot get lookup company."; }
                    }
                    else { error = "Cannot populate name value."; }
                }
                else { error = "Cannot get textbox name."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_03_CI03_SubmitCI()
        {
            try
            {
                flag = gui.Save(true);
                if (!flag)
                {
                    CI03Name = "";
                    error = "Cannot save CI03";
                    flagExit = false;
                }
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_01_CI04_Open_NewLinuxServerCI()
        {
            try
            {
                // Open  Computer list page
                flag = home.LeftMenuItemSelect("Configuration", "Linux");
                if (flag)
                {
                    list.WaitLoading();

                    // Click on New button
                    button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot click on New button"; }
                    }
                    else { error = "Cannot get button New"; }
                }
                else { error = "Error when create new Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_02_CI04_PopulateMandatoryFields()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //CI Name
                temp = Base.GData("CI Prefix Name");
                CI04Name = temp + "04 " + postfix;
                //**************************************************************
                textbox = gui.Textbox_Name_CI();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(CI04Name);
                    if (flag)
                    {
                        lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (flag)
                            {
                                //Populate Asset Tag
                                temp = "ASSTAG_04_" + postfix;
                                //**************************************************************
                                textbox = gui.Textbox_AssetTag();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    flag = textbox.SetText(temp);
                                    if (flag)
                                    {
                                        //Populate Serial Number
                                        temp = "SN_04_" + postfix;
                                        //**************************************************************
                                        textbox = gui.Textbox_SerialNumber();
                                        flag = textbox.Existed;
                                        if (flag)
                                        {
                                            flag = textbox.SetText(temp);
                                            if (!flag)
                                            {
                                                error = "Cannot popualte serial number value.";
                                            }
                                        }
                                        else { error = "Cannot get textbox serial number."; }
                                    }
                                    else { error = "Cannot populate asset tag."; }
                                }
                                else { error = "Cannot get textbox asset tag."; }
                            }
                            else { error = "Cannot populate company value."; }
                        }
                        else { error = "Cannot get lookup company."; }
                    }
                    else { error = "Cannot populate name value."; }
                }
                else { error = "Cannot get textbox name."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_03_CI04_SubmitCI()
        {
            try
            {
                flag = gui.Save(true);
                if (!flag)
                {
                    CI04Name = "";
                    error = "Cannot save CI04";
                    flagExit = false;
                }
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_01_CI05_Open_NewDatabaseServerCI()
        {
            try
            {
                // Open  Computer list page
                flag = home.LeftMenuItemSelect("Database Servers", "All");
                if (flag)
                {
                    list.WaitLoading();

                    // Click on New button
                    button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot click on New button"; }
                    }
                    else { error = "Cannot get button New"; }
                }
                else { error = "Error when create new Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_02_CI05_PopulateMandatoryFields()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //CI Name
                temp = Base.GData("CI Prefix Name");
                CI05Name = temp + "05 " + postfix;
                //**************************************************************
                textbox = gui.Textbox_Name_CI();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(CI05Name);
                    if (flag)
                    {
                        lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (flag)
                            {
                                //Populate Asset Tag
                                temp = "ASSTAG_05_" + postfix;
                                //**************************************************************
                                textbox = gui.Textbox_AssetTag();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    flag = textbox.SetText(temp);
                                    if (flag)
                                    {
                                        //Populate Serial Number
                                        temp = "SN_05_" + postfix;
                                        //**************************************************************
                                        textbox = gui.Textbox_SerialNumber();
                                        flag = textbox.Existed;
                                        if (flag)
                                        {
                                            flag = textbox.SetText(temp);
                                            if (!flag)
                                            {
                                                error = "Cannot popualte serial number value.";
                                            }
                                        }
                                        else { error = "Cannot get textbox serial number."; }
                                    }
                                    else { error = "Cannot populate asset tag."; }
                                }
                                else { error = "Cannot get textbox asset tag."; }
                            }
                            else { error = "Cannot populate company value."; }
                        }
                        else { error = "Cannot get lookup company."; }
                    }
                    else { error = "Cannot populate name value."; }
                }
                else { error = "Cannot get textbox name."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_03_CI05_SubmitCI()
        {
            try
            {
                flag = gui.Save(true);
                if (!flag)
                {
                    CI05Name = "";
                    error = "Cannot save CI05";
                    flagExit = false;
                }
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_01_CI06_Open_NewServerCI()
        {
            try
            {
                // Open  Computer list page
                flag = home.LeftMenuItemSelect("Servers", "All", 1);
                if (flag)
                {
                    list.WaitLoading();

                    // Click on New button
                    button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            list.WaitLoading();
                        }
                        else { error = "Cannot click on New button"; }
                    }
                    else { error = "Cannot get button New"; }
                }
                else { error = "Error when create new Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_02_CI06_PopulateMandatoryFields()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //CI Name
                temp = Base.GData("CI Prefix Name");
                CI06Name = temp + "06 " + postfix;
                //**************************************************************
                textbox = gui.Textbox_Name_CI();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(CI06Name);
                    if (flag)
                    {
                        lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Company");
                            flag = lookup.Select(temp);
                            if (flag)
                            {
                                //Populate Asset Tag
                                temp = "ASSTAG_06_" + postfix;
                                //**************************************************************
                                textbox = gui.Textbox_AssetTag();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    flag = textbox.SetText(temp);
                                    if (flag)
                                    {
                                        //Populate Serial Number
                                        temp = "SN_06_" + postfix;
                                        //**************************************************************
                                        textbox = gui.Textbox_SerialNumber();
                                        flag = textbox.Existed;
                                        if (flag)
                                        {
                                            flag = textbox.SetText(temp);
                                            if (!flag)
                                            {
                                                error = "Cannot popualte serial number value.";
                                            }
                                        }
                                        else { error = "Cannot get textbox serial number."; }
                                    }
                                    else { error = "Cannot populate asset tag."; }
                                }
                                else { error = "Cannot get textbox asset tag."; }
                            }
                            else { error = "Cannot populate company value."; }
                        }
                        else { error = "Cannot get lookup company."; }
                    }
                    else { error = "Cannot populate name value."; }
                }
                else { error = "Cannot get textbox name."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_03_CI06_SubmitCI()
        {
            try
            {
                flag = gui.Save(true);
                if (!flag)
                {
                    CI06Name = "";
                    error = "Cannot save CI06";
                    flagExit = false;
                }
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_01_Open_CI01()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI01Name == null || CI01Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI01Name.");
                    addPara.ShowDialog();
                    CI01Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Configuration", "Services");
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Business Services");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI01Name, "Name=" + CI01Name, "Name");
                        if (!flag) error = "Error when search and open B/S CI (id:" + CI01Name + ")";
                        else gui.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Business Services)"; }
                }
                else { error = "Error when open business services list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_02_ClickAddCIRelationship()
        {
            try
            {
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_AddCIRelationship();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Cannot click Add CI Relationship button.";
                        }
                        else { gui.WaitLoading(); }
                    }
                    else { error = "Cannot get Add CI Relationship button."; }
                }
                else { error = "Cannot click on tab (System)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_03_AddRelationship_CI01()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI02Name == null || CI02Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI02Name.");
                    addPara.ShowDialog();
                    CI02Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                gui.WaitFilterLoading();
                combobox = gui.List_CI_AvailableRelationships();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Depends on";
                    flag = combobox.SelectItem("@@" + temp);
                    if (flag)
                    {
                        gui.WaitFilterLoading();
                        flag = member.Add_Members(CI02Name);
                        if (flag) gui.WaitLoading();
                        else System.Console.WriteLine("-*-[ERROR]: Error when add relationship CI.");
                    }
                    else { error = "Cannot select item have: " + temp; }
                }
                else { error = "Cannot get list available relationships."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_01_Open_CI02()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI02Name == null || CI02Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI02.");
                    addPara.ShowDialog();
                    CI02Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Configuration", "Services");
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Business Services");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI02Name, "Name=" + CI02Name, "Name");
                        if (!flag) error = "Error when search and open B/S CI (id:" + CI02Name + ")";
                        else gui.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Business Services)"; }
                }
                else { error = "Error when open business services list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_02_ClickAddCIRelationship()
        {
            try
            {
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_AddCIRelationship();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Cannot click Add CI Relationship button.";
                        }
                        else { gui.WaitLoading(); }
                    }
                    else { error = "Cannot get Add CI Relationship button."; }
                }
                else { error = "Cannot click on tab (System)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_03_AddRelationship_CI02()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03.");
                    addPara.ShowDialog();
                    CI03Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                gui.WaitFilterLoading();
                combobox = gui.List_CI_AvailableRelationships();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Receives data";
                    flag = combobox.SelectItem("@@" + temp);
                    if (flag)
                    {
                        gui.WaitFilterLoading();
                        flag = member.Add_Members(CI03Name);
                        if (flag) gui.WaitLoading();
                        else System.Console.WriteLine("-*-[ERROR]: Error when add relationship CI.");
                    }
                    else { error = "Cannot select item have: " + temp; }
                }
                else { error = "Cannot get list available relationships."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_01_Open_CI03()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03.");
                    addPara.ShowDialog();
                    CI03Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Application", "Applications");
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Applications");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI03Name, "Name=" + CI03Name, "Name");
                        if (!flag) error = "Error when search and open B/S CI (id:" + CI03Name + ")";
                        else gui.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Applications)"; }
                }
                else { error = "Error when open business services list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_02_ClickAddCIRelationship()
        {
            try
            {
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_AddCIRelationship();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Cannot click Add CI Relationship button.";
                        }
                        else { gui.WaitLoading(); }
                    }
                    else { error = "Cannot get Add CI Relationship button."; }
                }
                else { error = "Cannot click on tab (System)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_03_AddRelationship_CI03()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI04Name == null || CI04Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI04.");
                    addPara.ShowDialog();
                    CI04Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                gui.WaitFilterLoading();
                combobox = gui.List_CI_AvailableRelationships();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Runs on";
                    flag = combobox.SelectItem("@@" + temp);
                    if (flag)
                    {
                        gui.WaitFilterLoading();
                        flag = member.Add_Members(CI04Name);
                        if (flag) gui.WaitLoading();
                        else System.Console.WriteLine("-*-[ERROR]: Error when add relationship CI.");
                    }
                    else { error = "Cannot select item have: " + temp; }
                }
                else { error = "Cannot get list available relationships."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_01_Open_CI03()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03.");
                    addPara.ShowDialog();
                    CI03Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Application", "Applications");
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Applications");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI03Name, "Name=" + CI03Name, "Name");
                        if (!flag) error = "Error when search and open B/S CI (id:" + CI03Name + ")";
                        else gui.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Application)"; }
                }
                else { error = "Error when open business services list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_02_ClickAddCIRelationship()
        {
            try
            {
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_AddCIRelationship();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Cannot click Add CI Relationship button.";
                        }
                        else { gui.WaitLoading(); }
                    }
                    else { error = "Cannot get Add CI Relationship button."; }
                }
                else { error = "Cannot click on tab (System)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_03_AddRelationship_CI03()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI05Name == null || CI05Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI05.");
                    addPara.ShowDialog();
                    CI05Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                gui.WaitFilterLoading();
                combobox = gui.List_CI_AvailableRelationships();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Depends on";
                    flag = combobox.SelectItem("@@" + temp);
                    if (flag)
                    {
                        gui.WaitFilterLoading();
                        flag = member.Add_Members(CI05Name);
                        if (flag) gui.WaitLoading();
                        else System.Console.WriteLine("-*-[ERROR]: Error when add relationship CI.");
                    }
                    else { error = "Cannot select item have: " + temp; }
                }
                else { error = "Cannot get list available relationships."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_016_01_Open_CI06()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI06Name == null || CI06Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI06.");
                    addPara.ShowDialog();
                    CI06Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Servers", "All", 1);
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Servers");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI06Name, "Name=" + CI06Name, "Name");
                        if (!flag) error = "Error when search and open B/S CI (id:" + CI06Name + ")";
                        else gui.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Servers)"; }
                }
                else { error = "Error when open business services list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_016_02_ClickAddCIRelationship()
        {
            try
            {
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_AddCIRelationship();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Cannot click Add CI Relationship button.";
                        }
                        else { gui.WaitLoading(); }
                    }
                    else { error = "Cannot get Add CI Relationship button."; }
                }
                else { error = "Cannot click on tab (System)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_016_03_AddRelationship_CI06()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI05Name == null || CI05Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI05.");
                    addPara.ShowDialog();
                    CI05Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                gui.WaitFilterLoading();
                combobox = gui.List_CI_AvailableRelationships();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Hosts";
                    flag = combobox.SelectItem("@@" + temp);
                    if (flag)
                    {
                        gui.WaitFilterLoading();
                        flag = list.SearchByColumnAndChoose("Name", CI05Name);
                        if (flag)
                        {
                            gui.WaitLoading();
                            gui.SaveAndExit();
                        }     
                        else System.Console.WriteLine("-*-[ERROR]: Error when add relationship CI.");
                    }
                    else { error = "Cannot select item have: " + temp; }
                }
                else { error = "Cannot get list available relationships."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_SearchAndOpen_CI03()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03Name.");
                    addPara.ShowDialog();
                    CI03Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Application", "Applications");
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Applications");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI03Name, "Name=" + CI03Name, "Name");
                        if (!flag) error = "Error when search and open Application CI (id:" + CI03Name + ")";
                        else list.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Applications)"; }
                }
                else { error = "Error when open application list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_VerifyDetailRelationship_CI03()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI01Name == null || CI01Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI01Name.");
                    addPara.ShowDialog();
                    CI01Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI02Name == null || CI02Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI02Name.");
                    addPara.ShowDialog();
                    CI02Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI04Name == null || CI04Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI04Name.");
                    addPara.ShowDialog();
                    CI04Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI05Name == null || CI05Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI05Name.");
                    addPara.ShowDialog();
                    CI05Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI06Name == null || CI06Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI06Name.");
                    addPara.ShowDialog();
                    CI06Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_CI_Split();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = gui.CI_Split_Button_Select();
                        if (flag)
                        {
                            flag = gui.CI_VerifyRelationshipTable(CI04Name, "Downstream relationships");
                            if (flag)
                            {
                                flag = gui.CI_VerifyRelationshipTable(CI05Name, "Downstream relationships");
                                if (flag)
                                {
                                    flag = gui.CI_VerifyRelationshipTable(CI06Name, "Downstream relationships");
                                    if (flag)
                                    {
                                        flag = gui.CI_VerifyRelationshipTable(CI01Name, "Upstream relationships");
                                        if (flag)
                                        {
                                            flag = gui.CI_VerifyRelationshipTable(CI02Name, "Upstream relationships");
                                            if (!flag)
                                            {
                                                error = CI02Name + " does not exist in Upstream relationships.";
                                            }
                                        }
                                        else { error = CI01Name + " does not exist in Upstream relationships."; }
                                    }
                                    else { error = CI06Name + " does not exist in Downstream relationships."; }
                                }
                                else { error = CI05Name + " does not exist in Downstream relationships."; }
                            }
                            else { error = CI04Name + " does not exist in Downstream relationships."; }
                        }
                        else { error = "The button Split is not selected."; }
                    }
                    else { error = "Cannot get Split button."; }
                }
                else { error = "Cannot click on tab (System)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------              
        [Test]
        public void Step_003_SearchAndOpen_CI05()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI05Name == null || CI05Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI05Name.");
                    addPara.ShowDialog();
                    CI05Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Database Servers", "All");
                if (flag)
                {
                    list.WaitLoading();
                    temp = list.List_Title().MyText;
                    flag = temp.Equals("Databases");
                    if (flag)
                    {
                        flag = list.SearchAndOpen("Name", CI05Name, "Name=" + CI05Name, "Name");
                        if (!flag) error = "Error when search and open Database Server CI (id:" + CI05Name + ")";
                        else list.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Databases)"; }
                }
                else { error = "Error when open Database Server list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_VerifyDetailRelationship_CI05()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI01Name == null || CI01Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI01Name.");
                    addPara.ShowDialog();
                    CI01Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI02Name == null || CI02Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI02Name.");
                    addPara.ShowDialog();
                    CI02Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03Name.");
                    addPara.ShowDialog();
                    CI03Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI06Name == null || CI06Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI06Name.");
                    addPara.ShowDialog();
                    CI06Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_CI_Split();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = gui.CI_Split_Button_Select();
                        if (flag)
                        {
                            flag = gui.CI_VerifyRelationshipTable(CI06Name, "Downstream relationships");
                            if (flag)
                            {
                                flag = gui.CI_VerifyRelationshipTable(CI01Name, "Upstream relationships");
                                if (flag)
                                {
                                    flag = gui.CI_VerifyRelationshipTable(CI02Name, "Upstream relationships");
                                    if (flag)
                                    {
                                        flag = gui.CI_VerifyRelationshipTable(CI03Name, "Upstream relationships");
                                        if (flag)
                                        {
                                            error = CI03Name + " does not exist in Upstream relationships.";
                                        }
                                    }
                                    else { error = CI02Name + " does not exist in Upstream relationships."; }
                                }
                                else { error = CI01Name + " does not exist in Upstream relationships."; }
                            }
                            else { error = CI06Name + " does not exist in Downstream relationships."; }
                        }
                        else { error = "The button Split is not selected."; }
                    }
                    else { error = "Cannot get Split button."; }
                }
                else { error = "Cannot click on tab (System)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_ClickBSMButton()
        {
            try
            {
                button = gui.Button_CI_BSMMapView();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        flag = Base.SwitchToPage(1);
                        if (flag)
                        {
                            Thread.Sleep(5000);
                        }
                        else { error = "Cannot switch to new page."; }
                    }
                    else { error = "Cannot click BSM button."; }
                }
                else { error = "Cannot get BSM button"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_VerifyCI04_NOT_Shown()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI04Name == null || CI04Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI04Name.");
                    addPara.ShowDialog();
                    CI04Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                bool flagTemp;
                flagTemp = gui.CI_CheckCIInBSM(CI04Name);
                if (flagTemp)
                {
                    flag = false;
                    error = CI04Name + " still exist in BSM Maps";
                }
                else
                {
                    Base.Driver.Close();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_SearchAndOpen_CI06()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI06Name == null || CI06Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI06Name.");
                    addPara.ShowDialog();
                    CI06Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = Base.SwitchToPage(0);
                if (flag)
                {
                    flag = home.LeftMenuItemSelect("Servers", "All", 1);
                    if (flag)
                    {
                        list.WaitLoading();
                        temp = list.List_Title().MyText;
                        flag = temp.Equals("Servers");
                        if (flag)
                        {
                            flag = list.SearchAndOpen("Name", CI06Name, "Name=" + CI06Name, "Name");
                            if (!flag) error = "Error when search and open Server CI (id:" + CI06Name + ")";
                            else list.WaitLoading();
                        }
                        else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Servers)"; }
                    }
                    else { error = "Error when open server list."; }
                }
                else { error = "Cannot switch to new page."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_ClickDeleteButton()
        {
            try
            {
                button = gui.Button_Delete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        gui.WaitLoading();
                        string temp = Base.GData("Confirm_Message") ;
                        flag = gui.VerifyConfirmDialog(temp, "yes");
                        if (!flag)
                        {
                            error = "The confirmation message display is not correct or can NOT delete CI.";
                        }
                        gui.WaitLoading();
                    }
                    else { error = "Cannot click Delete button."; }
                }
                else { error = "Cannot get Delete button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_SearchAndOpen_CI03()
        {
            try
            {
                try
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03Name.");
                        addPara.ShowDialog();
                        CI03Name = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = home.LeftMenuItemSelect("Application", "Applications");
                    if (flag)
                    {
                        Thread.Sleep(3000);
                        list.WaitLoading();
                        temp = list.List_Title().MyText;
                        flag = temp.Equals("Applications");
                        if (flag)
                        {
                            flag = list.SearchAndOpen("Name", CI03Name, "Name=" + CI03Name, "Name");
                            if (!flag) error = "Error when search and open Applications CI (id:" + CI03Name + ")";
                            else list.WaitLoading();
                        }
                        else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Applications)"; }
                    }
                    else { error = "Error when open applications list."; }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_ClickBSMButton()
        {
            try
            {
                flag = gui.Select_Tab("System");
                if (flag) 
                {
                    button = gui.Button_CI_BSMMapView();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            flag = Base.SwitchToPage(1);
                            if (flag)
                            {
                                Thread.Sleep(5000);
                            }
                            else { error = "Cannot switch to new page."; }
                        }
                        else { error = "Cannot click BSM button."; }
                    }
                    else { error = "Cannot get BSM button"; }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_VerifyCI06_NOT_Shown()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI06Name == null || CI06Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI06Name.");
                    addPara.ShowDialog();
                    CI06Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                bool flagTemp;
                flagTemp = gui.CI_CheckCIInBSM(CI06Name);
                if (flagTemp)
                {
                    flag = false;
                    error = CI06Name + " still exist in BSM Maps";
                }
                else
                {
                    Base.Driver.Close();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SearchAndOpen_CI04()
        {
            try
            {
                try
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (CI04Name == null || CI04Name == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input CI04Name.");
                        addPara.ShowDialog();
                        CI04Name = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = Base.SwitchToPage(0);
                    if (flag)
                    {
                        flag = home.LeftMenuItemSelect("Servers", "Linux");
                        if (flag)
                        {
                            list.WaitLoading();
                            temp = list.List_Title().MyText;
                            flag = temp.Equals("Linux Servers");
                            if (flag)
                            {
                                flag = list.SearchAndOpen("Name", CI04Name, "Name=" + CI04Name, "Name");
                                if (!flag) error = "Error when search and open Linux Server CI (id:" + CI04Name + ")";
                                else list.WaitLoading();
                            }
                            else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Linux Servers)"; }
                        }
                        else { error = "Error when open Linux Servers list."; }
                    }
                    else { error = "Cannot switch to new page."; }

                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_ClickDeleteButton()
        {
            try
            {
                button = gui.Button_Delete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        gui.WaitLoading();
                        string temp = Base.GData("Confirm_Message");
                        flag = gui.VerifyConfirmDialog(temp, "yes");
                        if (!flag)
                        {
                            error = "The confirmation message display is not correct or can NOT delete CI.";
                        }
                        gui.WaitLoading();
                    }
                    else { error = "Cannot click Delete button."; }
                }
                else { error = "Cannot get Delete button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_SearchAndOpen_CI03()
        {
            try
            {
                try
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (CI03Name == null || CI03Name == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input CI03Name.");
                        addPara.ShowDialog();
                        CI03Name = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = home.LeftMenuItemSelect("Application", "Applications");
                    if (flag)
                    {
                        Thread.Sleep(3000);
                        list.WaitLoading();
                        temp = list.List_Title().MyText;
                        flag = temp.Equals("Applications");
                        if (flag)
                        {
                            flag = list.SearchAndOpen("Name", CI03Name, "Name=" + CI03Name, "Name");
                            if (!flag) error = "Error when search and open Applications CI (id:" + CI03Name + ")";
                            else list.WaitLoading();
                        }
                        else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Applications)"; }
                    }
                    else { error = "Error when open applications list."; }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_ClickBSMButton()
        {
            try
            {
                Base.SwitchToPage(0);
                flag = gui.Select_Tab("System");
                if (flag)
                {
                    button = gui.Button_CI_BSMMapView();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            flag = Base.SwitchToPage(1);
                            if (flag)
                            {
                                Thread.Sleep(5000);
                            }
                            else { error = "Cannot switch to new page."; }
                        }
                        else { error = "Cannot click BSM button."; }
                    }
                    else { error = "Cannot get BSM button"; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_02_VerifyCI04_NOT_Shown()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (CI04Name == null || CI04Name == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input CI04Name.");
                    addPara.ShowDialog();
                    CI04Name = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                bool flagTemp;
                flagTemp = gui.CI_CheckCIInBSM(CI04Name);
                if (flagTemp)
                {
                    flag = false;
                    error = CI04Name + " still exist in BSM Maps";
                }
                Base.Driver.Close();
                Base.SwitchToPage(0);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
