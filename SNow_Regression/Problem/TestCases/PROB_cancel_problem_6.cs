﻿using Auto;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace Problem
{
    class PROB_cancel_problem_6
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            
            System.Console.WriteLine("Finished - Problem Id: " + problemId);
            System.Console.WriteLine("Finished - Problem Task Id: " + problemTaskId); 
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snocheckbox checkbox;
        SNow.snotextarea textarea;
        SNow.snodatetime datetime;
        SNow.snobutton button;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Problem prb = null;
        SNow.ProblemTask ptask = null;
                //------------------------------------------------------------------
        string problemId,problemTaskId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        [Test]
        public void ClassInit()
        {
            try
            {
                //get instance for each item
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                prb = new SNow.Problem(Base, "Problem");
                ptask = new SNow.ProblemTask(Base, "Problem Task");
                //
                problemId = string.Empty;
                problemTaskId = string.Empty;
                
            }catch(Exception ex)
            { 
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                string driver = Base.GData("Url");
                Base.Driver.Navigate().GoToUrl(driver);
                login.WaitLoading();
            }catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_002_LoginToSystem()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if(flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag=false;
                    error = "Can not login to system.";
                }
                
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_003_Impersonate_ProManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                flag = home.ImpersonateUser(temp);
                if(!flag)
                {
                    error = " Cannot impersonate user (" + temp + ")";
                }
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_004_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                    error = "Can not create new problem.";
                    
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_005_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    problemId = textbox.Text;
                    //+++++
                    string temp = Base.GData("Company");
                    lookup = prb.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.SetText(temp);
                        if (!flag)
                            error = "Can not populate company.";
                    }
                    else
                    {
                        error = "Can not get lookup company.";
                    }
                }
                else
                    error = "Can not get problem number.";
                
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_006_01_PopulateProStatement()
        {
            try
            {
                string temp = Base.GData("ProStatement");
                textbox = prb.Textbox_ProblemStatement();
                flag = textbox.Existed;
                if (flag)
                {
                    flag=textbox.SetText(temp);
                    if (!flag)
                        error = "Can not populate problem statement.";

                }
                else
                    error = "Can not get textbox problem statement.";
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_006_02_PopulateProImpact()
        {
            try
            {
                string temp = Base.GData("ProImpact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Cannot populate impact value.";
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_007_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Can not populate assigned group.";
                }
                else
                    error = "Can not get lookup assignment group.";
            }catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_008_01_PopulateAssignTo()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Can not populate assigned to.";
                }
                else
                    error = "Can not get lookup assigned to";
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_009_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                    error = " Problem can not save.";
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
                 
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_010_OpenNewProblemTask()
        {
            try
            {
                flag = prb.Select_Tab("Problem Tasks");
                //---------------------------------------
                if (flag)
                {
                    button = ptask.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            ptask.WaitLoading();
                        }
                        else { error = "Cannot click new button."; }
                    }
                    else { error = "Cannot get new button."; }
                }
                else { error = "Cannot click tab (Problem Tasks)."; }

            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_011_PopulateProblemTaskAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("ProTask_AssignmentGroup");
                lookup = ptask.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Can not populate problem task assignment group.";
                }
                else
                    error = "Can not get lookup problem task assignment group. ";
            }             
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_012_PopulateProblemTaskShortDescription()
        {
            try
            {
                string stemp = Base.GData("ProTask_ShortDescription");
                Console.WriteLine("ProTask_ShortDescription= " + stemp);
                textbox = ptask.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(stemp);
                    if (!flag)
                        error = "Can not populate problem task short description.";
                }
                else
                    error = "Can not get textbox problem task short description.";
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

           
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_013_PopulateProblemTaskDueDate()
        {
            try
            {
                string date = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                datetime = ptask.Datetime_DueDate();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.SetText(date);
                    if (!flag)
                        error = "Can not populate due date.";
                }
                else
                    error = "Can not get problem task due date.";
                
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_014_SaveProblemTask()
        {
            try
            {
                problemTaskId = ptask.Textbox_Number().Text;
                flag = ptask.Button_Submit().Click();
                if (!flag)
                    error = "Can not submit problem task.";
                else
                    prb.WaitLoading();
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_015_ResolveProblem()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = prb.Combobox_State().SelectItem("Closed/Resolved");
                    if (flag)
                    {
                        // populate ProCloseCode
                        string temp = Base.GData("ProCloseCode");
                        combobox = prb.Combobox_CauseCode();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                temp = Base.GData("ProCloseNote");
                                textarea = prb.Textarea_CloseNotes();
                                flag = textarea.Existed;
                                if(flag)
                                {
                                    flag = textarea.SetText(temp);
                                    if (!flag)
                                        error = "Can not populate close notes.";
                                    else 
                                    {
                                        combobox = prb.Combobox_ClosureCode();
                                        flag = combobox.Existed;
                                        if (flag)
                                        {
                                            flag = combobox.SelectItem("Cancelled Invalid");
                                            if (!flag)
                                                error = "Cannot populate closure code.";
                                        }
                                        else error = "Can not get combobox closure code.";
                                    }
                                }
                                else
                                {
                                    error = "Can not get textarea close notes.";
                                }
                            }
                            else
                                error = "Can not pupulate proble close code.";
                                
                        }
                        else
                            error = "Can not get combobox close code.";

                    }
                    else
                        error = "Can not populate closed/resolved value.";

                }
                else
                    error = "Can not get combobox problem state.";
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
             
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_016_SaveProblem_CannotSave()
        {
            try
            {
                flag = prb.Save(false, true);

                if (flag)
                {
                    flag = prb.Verify_ExpectedErrorMessages_Existed("Cannot close Problem with active Problem Tasks");
                    if (!flag)
                        error = "Invalid error message.";
                }
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_017_OpenProblemTask()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemTaskId == null || problemTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please problem task Id.");
                    addPara.ShowDialog();
                    problemTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + problemTaskId;
                flag = prb.RelatedTableOpenRecord("Problem Tasks", condition, "Number");
                if(!flag)
                {
                    error = "can not open problem tasks tab.";
                }
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_018_ResolveProblemTask()
        {
            try
            {
                combobox = ptask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Skipped");
                    if (!flag)
                        error = "Can not populate problem task state value.";
                }
                else
                    error = "Can not get combobox problem task state.";
            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_019_UpdateProblemTask()
        {
            try
            {
                flag = ptask.Update();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_020_ResolveProblem()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = prb.Combobox_State().SelectItem("Closed/Resolved");
                    if (flag)
                    {
                        // populate ProCloseCode
                        string temp = Base.GData("ProCloseCode");
                        combobox = prb.Combobox_CauseCode();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                temp = Base.GData("ProCloseNote");
                                textarea = prb.Textarea_CloseNotes();
                                flag = textarea.Existed;
                                if (flag)
                                {
                                    flag = textarea.SetText(temp);
                                    if (!flag)
                                        error = "Can not populate close notes.";
                                    else
                                    {
                                        combobox = prb.Combobox_ClosureCode();
                                        flag = combobox.Existed;
                                        if (flag)
                                        {
                                            flag = combobox.SelectItem("Cancelled Invalid");
                                            if (!flag)
                                                error = "Cannot populate closure code.";
                                        }
                                        else error = "Can not get combobox closure code.";
                                    }
                                }
                                else
                                {
                                    error = "Can not get textarea close notes.";
                                }
                            }
                            else
                                error = "Can not pupulate proble close code.";

                        }
                        else
                            error = "Can not get combobox close code.";

                    }
                    else
                        error = "Can not populate closed/resolved value.";

                }
                else
                    error = "Can not get combobox problem state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_021_CheckRCA()
        {
            try
            {
                checkbox=prb.Checkbox_RCADelivered();
                flag=checkbox.Existed;
                if(flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                        flag = checkbox.Click();

                    if (!flag)
                        error = "Cannot check RCA Delivered.";
                }
                else
                    error="Can not get checkbox RCA Delivered.";

            }
            catch(Exception ex)
            {
                flag=false;
                error=ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_022_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                    error = "Can not save problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //++++++++++++++++++++++++++++++++++++++
        [Test]
        public void Step_End_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }
    
}
