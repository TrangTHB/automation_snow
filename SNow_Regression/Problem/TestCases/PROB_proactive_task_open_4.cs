﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace Problem
{
    class PROB_proactive_task_open_4
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Problem Id: " + ProblemId);
            System.Console.WriteLine("Problem Task 01 Id: " + ProTask01);
            System.Console.WriteLine("Problem Task 02 Id: " + ProTask02);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snocheckbox checkbox;
        SNow.snotextarea textarea;
        SNow.snoelement ele;
        SNow.snodatetime datetime;
        SNow.snobutton button;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Incident inc = null;
        SNow.Problem prb = null;
        SNow.ProblemTask proTask;
        SNow.ProblemList prblist = null;
        SNow.Member member = null;
        SNow.ItilList tsklist = null;
        SNow.EmailList emailList = null;

        //------------------------------------------------------------------
        string incidentId, ProblemId, ProTask01, ProTask02;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                member = new SNow.Member(Base);
                prb = new SNow.Problem(Base, "Problem");
                proTask = new SNow.ProblemTask(Base, "Problem Task");
                prblist = new SNow.ProblemList(Base, "Problem List");
                tsklist = new SNow.ItilList(Base, "Task list");
                emailList = new SNow.EmailList(Base, "Email List");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                ProblemId = string.Empty;
                ProTask01 = string.Empty;
                ProTask02 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                {
                    inc.WaitLoading();
                }
                else
                {
                    error = "Cannot select create new incident";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("INC_Caller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("INC_ShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("INC_Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("INC_Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_OpenProblemTask_List()
        {
            try
            {
                textbox = home.Textbox_Filter();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = "problem_task.list";
                    flag = textbox.SetText(temp, true);
                    if (!flag) { error = "Cannot set text problem task list."; }
                    else 
                    { 
                        tsklist.WaitLoading();
                        button = tsklist.Button_New();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag) error = "Error when click on button New.";
                            else prb.WaitLoading();
                        }
                        else { error = "Cannot get button new"; }
                    }
                }
                else { error = "Cannot get textbox Filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_ProblemTask_Populate_ShortDescription()
        {
            try
            {
                temp = Base.GData("ProTask_2_ShortDes");
                textbox = prb.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    { error = "Cannot input problem task Short Description"; }
                }
                else
                { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_ProblemTask_Populate_AssignmentGroup()
        {
            try
            {
                temp = Base.GData("ProTask_2_AssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    { error = "Cannot populate problem task Assignment Group"; }
                }
                else
                { error = "Cannot get lookup Assignment Group"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_ProblemTask_Populate_DueDate()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                datetime = prb.Datetime_DueDate();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.SetText(temp, true);
                    if (!flag)
                    { error = "Cannot populate Due date"; }
                }
                else { error = "Cannot get datetime field Due date"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_ProblemTask_Submit()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag) 
                {
                    ProTask02 = textbox.Text;
                    Console.WriteLine("Store problem task id 2:" + ProTask02);
                    //--------------------------------------------------------
                    flag = prb.Button_Submit().Click();
                    if (!flag)
                    {
                        error = "Error when submit problem task.";
                    }
                    else tsklist.WaitLoading();
                } 
                else 
                {
                    error = "Cannot get textbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_ImpersonateUser_ProblemManager()
        {
            try
            {
                temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("ProblemManager"), true, temp, false);
                if (!flag)
                {
                    error = "Cannot impersonate Problem Manager";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    inc.WaitLoading();
                }
                else
                {
                    error = "Cannot select create new problem";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "PRB*");
                        if (!flag) error = "Invalid format of Problem number.";
                        else { ProblemId = temp; Console.WriteLine("-*-[STORE]: Problem Id:(" + ProblemId + ")"); }
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }

                //-- Input company
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string company = Base.GData("Company");
                    flag = lookup.Select(company);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else
                { error = "Cannot get company field."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_PopulateProblemStatement()
        {
            try
            {
                textbox = prb.Textbox_ProblemStatement();
                temp = Base.GData("ProStatement") + " - " + ProblemId;
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_AddACI()
        {
            try
            {
                temp = Base.GData("ProCI01");
                lookup = prb.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate CI."; }

                }
                else { error = "Cannot get lookup Configuration Item."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateImpact()
        {
            try
            {
                temp = Base.GData("ProImpact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Cannot update impact.";

                }
                else
                {
                    error = "Cannot found impact combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_UpdatePriority()
        {
            try
            {
                temp = Base.GData("ProPriority");
                combobox = prb.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Cannot update priority.";

                }
                else
                {
                    error = "Cannot found Priority combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateCategoryAndSubcategory()
        {
            try
            {
                temp = Base.GData("ProCat");
                combobox = prb.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                        string sub_cat = Base.GData("ProSubCat");
                        combobox = prb.Combobox_Subcategory();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(sub_cat);
                            if (!flag) error = "Cannot update Problem Subcategory.";
                        }
                        else
                        { error = "Cannot found Subcategory combobox."; }
                    }
                    else
                    {
                        error = "Cannot update Problem Category.";
                    }

                }
                else
                { error = "Cannot found Category combobox."; }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_PopulateAssignmentGroup()
        {
            try
            {
                temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group."; }

                }
                else { error = "Cannot get lookup Assignment Group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_01_PopulateDescription()
        {
            try
            {
                textarea = prb.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    temp = Base.GData("ProDescription");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    { error = "Cannot populate problem description."; }
                }
                else
                { error = "Cannot found problem description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_Populate_More_Fields_If_Need()
        {
            try
            {
                temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_VerifySLATasks()
        {
            try
            {
                string condition = Base.GData("SLA_13");
                if (condition != string.Empty && condition.ToLower() != "no")
                {
                    flag = prb.Verify_RelatedTable_Row("Task SLAs", condition);
                    if (!flag)
                    {
                        flagExit = false;
                        error = error + "Not found SLA: " + condition;
                    }
                }
                else
                    System.Console.WriteLine("No need check SLAs.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_OpenMyGroupsWork()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (flag)
                {
                    tsklist.WaitLoading();
                    ele = tsklist.List_Title();
                    flag = ele.Existed;
                    if (!flag || ele.MyText != "Tasks")
                    {
                        flag = false;
                        error = "Invalid tasks title or cannot open task form.";
                    }
                }
                else { error = "Cannot select My Groups Work."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_SearchAndOpenAssignedItem()
        {
            try
            {
                temp = Base.GData("Debug");
                if (temp == "yes" && ProblemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    ProblemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = "Number=" + ProblemId + "|Assignment group=" + Base.GData("ProAssignmentGroup");
                flag = tsklist.SearchAndOpen("Number", ProblemId, temp, "Number");
                if (!flag)
                { error = "Cannot find problem."; }
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_AssignedTo_Problem()
        {
            try
            {
                temp = Base.GData("ProblemManager");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    { error = "Cannot reassign problem."; }
                }
                else { error = "Cannot get lookup Assigned to"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Open_MyWorkList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Work");
                if (flag)
                {
                    tsklist.WaitLoading();
                    ele = tsklist.List_Title();
                    flag = ele.Existed;
                    if (!flag || ele.MyText != "Tasks")
                    {
                        flag = false;
                        error = "Invalid tasks title or cannot open task form.";
                    }
                }
                else { error = "Cannot select My Work."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SearchAndOpenAssignedItem()
        {
            try
            {
                temp = Base.GData("Debug");
                if (temp == "yes" && ProblemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    ProblemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = "Number=" + ProblemId + "|Assignment group=" + Base.GData("ProAssignmentGroup");
                flag = tsklist.SearchAndOpen("Number", ProblemId, temp, "Number");
                if (!flag)
                { error = "Cannot find problem."; }
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_SearchASelectIncOnEditMember()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = prb.Add_Related_Members("Incidents", incidentId);
                if (!flag)
                {
                    error = "Cannot add 2nd CI.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_Verify_IncidentRelated_Existed()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + incidentId + "|State=New";
                flag = prb.Search_Verify_RelatedTable_Row("Incidents", "Number", "=" + incidentId, conditions);
                if (!flag) error = "Not found incident related. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_3_PopulateState_To_PendingChange()
        {
            try
            {
                string temp = "Pending Change";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_PopulateWorkNotes()
        {
            try
            {
                textarea = prb.Textarea_Problem_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProworkNotes") + " - Step 21";
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate work note value.";
                    }
                }
                else { error = "Cannot get textarea worknote."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_PopulateState_To_Open()
        {
            try
            {
                string temp = "Open";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_PopulateWorkNotes()
        {
            try
            {
                textarea = prb.Textarea_Problem_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProworkNotes") + " - Step 24";
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate work note value.";
                    }
                }
                else { error = "Cannot get textarea worknote."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Create_1st_ProblemTask()
        {
            try
            {
                flag = prb.Select_Tab("Problem Tasks");
                //---------------------------------------
                if (flag)
                {
                    button = proTask.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            proTask.WaitLoading();
                        }
                        else { error = "Cannot click new button."; }
                    }
                    else { error = "Cannot get new button."; }
                }
                else { error = "Cannot click tab (Problem Tasks)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ProTask_1_ShortDes");
                textbox = proTask.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate short description value.";
                    }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_1_PopulateAssignmentGroup()
        {
            try
            {
                //Get Problem Task Id number
                textbox = proTask.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    ProTask01 = textbox.Text;
                    string temp = Base.GData("ProTask_1_AssignmentGroup");
                    lookup = proTask.Lookup_AssignmentGroup();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag)
                        {
                            error = "Cannot populate assignment group value.";
                        }
                    }
                    else { error = "Cannot get lookup assignment group."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_2_PopulateAssignedTo()
        {
            try
            {
                string temp = Base.GData("ProTask_1_Assignee");
                lookup = proTask.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assigned to value.";
                    }
                }
                else { error = "Cannot get lookup assigned to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_3_PopulateDueDate()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                datetime = proTask.Datetime_DueDate();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.SetText(temp, true);
                    if (!flag)
                    { error = "Cannot populate Due date"; }
                }
                else { error = "Cannot get datetime field Due date"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_1_Submit_ProblemTask_1st()
        {
            try
            {
                button = proTask.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else error = "Error when click on submit button.";
                }
                else 
                {
                    error = "Cannot get button submit.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_2_Verify_ProblemTask_1st_OnProblem()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task id 1.");
                    addPara.ShowDialog();
                    ProTask01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + ProTask01 + "|State=Open";
                flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions);
                if (!flag) error = "Not found problem task 1. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_1_Add_2st_ProblemTask()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task Id 2.");
                    addPara.ShowDialog();
                    ProTask02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                flag = prb.Add_Related_Members("Problem Tasks", ProTask02);
                if(!flag)
                    error = "Cannot add 2rd task [" + ProTask02 + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_2_Verify_ProblemTask_2rd_OnProblem()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task id 2.");
                    addPara.ShowDialog();
                    ProTask02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + ProTask02 + "|State=Open";
                flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions);
                if (!flag) error = "Not found problem task 2. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_1_Remove_ProblemTask_2rd()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task Id 2.");
                    addPara.ShowDialog();
                    ProTask02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                flag = prb.Delete_Related_Members("Problem Tasks", ProTask02);
                if (!flag)
                    error = "Cannot delete 2rd task [" + ProTask02 + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_2_Verify_ProblemTask_2rd_NotFound()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask02 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task id 2.");
                    addPara.ShowDialog();
                    ProTask02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + ProTask02 + "|State=Open";
                flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions, true);
                if (!flag) error = "Found problem task 2. Expected: Not found (" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_3_Verify_ProblemTask_1st_OnProblem()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task id 1.");
                    addPara.ShowDialog();
                    ProTask01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + ProTask01 + "|State=Open";
                flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions);
                if (!flag) error = "Not found problem task 1. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_PopulateWorkAround()
        {
            try
            {
                textarea = prb.Textarea_Workaround();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProworkAround");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate work around value.";
                    }
                }
                else { error = "Cannot get textarea around."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_1_ClickOn_Link_Communicate_Workaround()
        {
            try
            {
                ele = prb.GRelatedLink("Communicate Workaround");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag) error = "Error when click on link.";
                }
                else error = "Cannot get link communicate workaround.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_2_Verify_Message()
        {
            try
            {
                flag = prb.VerifyMessageInfo("Workaround Communicated");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_3_VerifyActivity_33()
        {
            try
            {
                temp = Base.GData("Activity_33");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 33. Expected:(" + temp + ")";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_OpenIncident_FromRelatedTable()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + incidentId;
                flag = prb.RelatedTableOpenRecord("Incidents", conditions, "Number");
                if (!flag) error = "Error when open incident from related table.";
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_1_VerifyActivity_35()
        {
            try
            {
                flag = inc.Select_Tab("Notes");
                if (flag)
                {
                    temp = Base.GData("Activity_35");
                    flag = inc.Verify_Activity(temp);
                    if (!flag)
                    {
                        error = "Invalid activity note 33. Expected:(" + temp + ")";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_2_Backto_Problem()
        {
            try
            {
                button = prb.Button_Back();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag) prb.WaitLoading();
                    else error = "Error when click on button back.";
                }
                else error = "Cannot get button back.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_PopulateCloseCodeAndCloseNotes()
        {
            try
            {
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProCloseCode");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        textarea = prb.Textarea_CloseNotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            temp = Base.GData("ProCloseNote");
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Cannot populate close notes.";
                            else
                            {
                                combobox = prb.Combobox_ClosureCode();
                                flag = combobox.Existed;
                                if (flag)
                                {
                                    flag = combobox.SelectItem("Cancelled Invalid");
                                    if (!flag)
                                        error = "Cannot populate closure code.";
                                }
                                else error = "Can not get combobox closure code.";
                            }
                        }
                        else error = "Cannot get textarea close notes.";
                    }
                    else error = "Cannot populate close code value.";
                }
                else error = "Cannot get combobox close code.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Resolve_Problem_WithOpenTask()
        {
            try
            {
                string temp = "Closed/Resolved";
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                        button = prb.Button_Update();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                prb.WaitLoading();
                            }
                            else error = "Error when click on button update.";
                        }
                        else error = "Cannot get button update.";
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_VerifyErrorMessage()
        {
            try
            {
                flag = prb.Verify_ExpectedErrorMessages_Existed("Cannot close Problem with active Problem Tasks");
                if (!flag)
                    error = "Invalid error message.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_ImpersonateUser_ProblemTask_Assignee()
        {
            try
            {
                temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("ProTask_1_Assignee"), true, temp, false);
                if (!flag)
                {
                    error = "Cannot impersonate Problem Task Assignee.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Open_MyWorkList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Work");
                if (flag)
                {
                    tsklist.WaitLoading();
                    ele = tsklist.List_Title();
                    flag = ele.Existed;
                    if (!flag || ele.MyText != "Tasks")
                    {
                        flag = false;
                        error = "Invalid tasks title or cannot open task form.";
                    }
                }
                else { error = "Cannot select My Work."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_SearchAndOpenAssignedItem()
        {
            try
            {
                temp = Base.GData("Debug");
                if (temp == "yes" && ProTask01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task Id 1.");
                    addPara.ShowDialog();
                    ProTask01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------
                temp = "Number=" + ProTask01;
                flag = tsklist.SearchAndOpen("Number", ProTask01, temp, "Number");
                if (!flag)
                { error = "Cannot find problem task."; }
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_PopulateState_To_ClosedComplete()
        {
            try
            {
                string temp = "Closed Complete";
                combobox = proTask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Populate_Task_WorkNotes()
        {
            try
            {
                textarea = proTask.Textarea_Problem_Task_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProTask_1_WorkNotes") + " - 03";
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate task work note value.";
                    }
                }
                else { error = "Cannot get textarea task work note."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Update_ProblemTask()
        {
            try
            {
                flag = prb.Update();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot update problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_ImpersonateUser_Problem_Assignee()
        {
            try
            {
                temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("ProblemManager"), true, temp, false);
                if (!flag)
                {
                    error = "Cannot impersonate Problem Manager";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_SearchProblem_UseGlobalSearch()
        {
            try
            {
                temp = Base.GData("Debug");
                if (temp == "yes" && ProblemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    ProblemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------

                flag = prb.GlobalSearchItem(ProblemId, true);
                if (!flag) error = "Cannot set text.";
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_PopulateCloseCodeAndCloseNotes()
        {
            try
            {
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProCloseCode");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        textarea = prb.Textarea_CloseNotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            temp = Base.GData("ProCloseNote");
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Cannot populate close notes.";
                            else
                            {
                                combobox = prb.Combobox_ClosureCode();
                                flag = combobox.Existed;
                                if (flag)
                                {
                                    flag = combobox.SelectItem("Cancelled Invalid");
                                    if (!flag)
                                        error = "Cannot populate closure code.";
                                }
                                else error = "Can not get combobox closure code.";
                            }
                        }
                        else error = "Cannot get textarea close notes.";
                    }
                    else error = "Cannot populate close code value.";
                }
                else error = "Cannot get combobox close code.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Check_RCA_Delivered()
        {
            try
            {
                checkbox = prb.Checkbox_RCADelivered();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Click();
                    if (!flag) error = "Error when click on checkbox.";
                }
                else error = "Cannot get checkbox RCA.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_1_PopulateStatus_CloseResolved()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed/Resolved");
                    if (!flag) error = "Error when select item.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_2_Save_Problem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) error = "Error when save problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_VerifySLATasks()
        {
            try
            {
                string condition = Base.GData("SLA_52");
                if (condition != string.Empty && condition.ToLower() != "no")
                {
                    flag = prb.Verify_RelatedTable_Row("Task SLAs", condition);
                    if (!flag)
                    {
                        flagExit = false;
                        error = error + "Not found SLA: " + condition;
                    }
                }
                else
                    System.Console.WriteLine("No need check SLAs.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_ImpersonateUser_SupportUser()
        {
            try
            {
                temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("SupportUser"), true, temp, false);
                if (!flag)
                {
                    error = "Cannot impersonate Problem Manager";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_2_Filter_Email_Workaround_SentTo_IncidentCaller()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProblemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    ProblemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string email = Base.GData("INC_Caller_Email");

                temp = "Subject;contains;" + ProblemId + "|and|Subject;contains;workaround|and|" + "Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_3_Validate_NoEmail_Workaround_SentTo_IncidentCaller()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProblemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    ProblemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + ProblemId;
                flag = emailList.VerifyRow(conditions, true);
                if (!flag) error = "Found email workaround sent incident caller. Expected not found.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_2_Filter_Email_ProblemTaskClosed_SentTo_ProblemAssignee()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task Id 1.");
                    addPara.ShowDialog();
                    ProTask01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string email = Base.GData("ProManagerEmail");

                temp = "Subject;contains;" + ProTask01 + "|and|Subject;contains;has been closed|and|" + "Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_3_Validate_HaveEmail_ProblemTaskClosed_SentTo_ProblemAssignee()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && ProTask01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task Id 1.");
                    addPara.ShowDialog();
                    ProTask01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + ProTask01;
                flag = emailList.VerifyRow(conditions);
                if (!flag) error = "NOT found email problem task closed sent to problem assignee.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_57_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }
    
}
