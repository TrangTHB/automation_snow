﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Problem
{
    [TestFixture]
    public class Problem_Create_Chg_using_Hdr_11
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Problem Id: " + problemId);



            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************



        string changeId;
        string problemId;

        Login login;
        Home home;
        SNow.Problem prb;
        SNow.ProblemList prolist;
        SNow.Change chg;
        SNow.ChangeList chglist;
        SNow.Search proSearch;       

        //-----------------------------
        
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snodatetime datetime = null;
       
       //**********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                prb = new SNow.Problem(Base, "Problem");
                prolist = new SNow.ProblemList(Base, "Problem list");
                chg = new SNow.Change(Base, "Change");
                chglist = new SNow.ChangeList(Base, "Change list");
                proSearch = new SNow.Search(Base, "Search problem");

                //-----------------------------------------------------

                problemId = string.Empty;
                changeId = string.Empty;
                
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_003_Impersonate_ProblemManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_004_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                    prb.WaitLoading();
                else
                    error = "Error when create new Problem.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Populate_Company()
        {
            try
            {
                //Get Problem Ticket
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    problemId = textbox.Text;
                }
                else { error = "Cannot get textbox Number."; }

                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_Populate_ProblemStatement()
        {
            try
            {
                string temp = Base.GData("ProStatement");
                textbox = prb.Textbox_ProblemStatement();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate problem statement value..";
                    }
                }
                else { error = "Cannot get textbox problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Populate_ProblemImpact()
        {
            try
            {
                string temp = Base.GData("ProImpact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Cannot populate impact value.";
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------  
        [Test]
        public void Step_007_01_Populate_AssigmentGroup()
        {
            try
            {
                string temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assignment group value..";
                    }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_02_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_008_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                {
                    error = "Error when save new problem.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_SearchAndOpenProblem()
        {
            try
            {
                //input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter para = new Auto.AddParameter("Please input problem ID.");
                    para.ShowDialog();
                    problemId = para.value;
                    para.Close();
                    para = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prolist.WaitLoading();
                    temp = prolist.List_Title().MyText.ToLower();
                    flag = temp.Equals("problems");
                    if (flag)
                    {
                        flag = prolist.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                        if (!flag) error = "Error when search and open problem (id:" + problemId + ")";
                        else prb.WaitLoading();
                    }
                    else
                    {
                        flagExit = false;
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problems list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Populate_ProAssignee()
        {
            try
            {
                string temp = Base.GData("ProAssignee");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assigned to value.";
                    }
                }
                else { error = "Cannot get lookup assinged to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_011_OpenNewNormalChange()
        {
            try
            {
                flag = prb.CreateNormalChange();
                if (!flag)
                {
                    error = "Cannot create normal change.";
                } else { Thread.Sleep(10000); chg.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_AutoPopulatedFields()
        {
            try
            {
                //Get Change Number
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeId = textbox.Text;
                }
                else { error = "Cannot get textbox Number."; }

                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        textbox = chg.Textbox_ShortDescription();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            temp = Base.GData("ProStatement");
                            flag = textbox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify short description value. Expected: " + temp;
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get textbox short description."; }
                    }
                    else
                    {
                        error = "Cannot verify company value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Populate_Category()
        {
            try
            {
                // populate change category
                string temp = Base.GData("ChgCategory");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate category value.";
                    }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Populate_Justification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                //---------------------------------------
                
                if (flag)
                {
                    // populate change justification 
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot populate justification value.";
                        }
                    }
                    else { error = "Can not get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Populate_PlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                //---------------------------------------
               
                if (flag)
                {
                    string startDate = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");
                    string endDate = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Schedule).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_00_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Populate priority*/
        public void Step_016_01_Populate_Priority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("4 - Low");
                    if (!flag) error = "Cannot populate priority";
                }
                else error = "Cannot get combobox priority";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_016_02_VerifyProblemInChange()
        {
            try
            {
                //input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter para = new Auto.AddParameter("Please input problem ID.");
                    para.ShowDialog();
                    problemId = para.value;
                    para.Close();
                    para = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + problemId;
                flag = chg.Verify_RelatedTable_Row("Problems", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found Problem response: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_017_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                { error = "Can not save change."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------        
        [Test]
        public void Step_018_ImpersonateProAssignee()
        {
            try
            {
                string temp = Base.GData("ProAssignee");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SearchAndOpenProblem()
        {
            try
            {
                //input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter para = new Auto.AddParameter("Please input problem ID.");
                    para.ShowDialog();
                    problemId = para.value;
                    para.Close();
                    para = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prolist.WaitLoading();
                    flag = prolist.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                    if (!flag) error = "Error when search and open problem (id:" + problemId + ")";
                    else prb.WaitLoading();
                }
                else error = "Error when select open problems list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_PopulateState()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed/Resolved");
                    if (!flag)
                    {
                       error = "Cannot populate state value.";
                    }
                        
                }
                else
                    error = "Cannot get combobox state.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_UpdateCloseNotes()
        {
            try
            {
                string temp = Base.GData("ProCloseNote");
                textarea = prb.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_03_CauseCode()
        {
            try
            {
                string temp = Base.GData("ProCauseCode");
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Close Code value."; }
                }
                else
                    error = "Cannot get combobox Close Code.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_021_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                    error = "Can not save problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_021_SearchAndOpenChange()
        {
            try
            {
                //input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter para = new Auto.AddParameter("Please input change ID.");
                    para.ShowDialog();
                    changeId = para.value;
                    para.Close();
                    para = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText.ToLower();
                    flag = temp.Equals("change requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        flagExit = false;
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Change Requests)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Veify_ChangeState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChgState");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify combobox State. Expected: (" + combobox.Text + ")";
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------.
        [Test]
        public void Step_023_Verify_ProblemRelatedToChange()
        {
            try
            {
                //input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter para = new Auto.AddParameter("Please input problem ID.");
                    para.ShowDialog();
                    problemId = para.value;
                    para.Close();
                    para = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + problemId + "|State=Closed/Resolved";
                flag = chg.Verify_RelatedTable_Row("Problems", conditions, false);
                if (!flag)
                {
                    error = "There is no problem related to this change.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_29_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }
}




    

