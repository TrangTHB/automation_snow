﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Problem
{
    [TestFixture]
    public class Problem_relate_change_9
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Problem Id: " + problemId);



            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************



        string changeId;
        string problemId;

        Login login;
        Home home;
        SNow.Problem prb;
        SNow.ProblemList prolist;
        SNow.Change chg;
        SNow.ChangeList chglist;
        SNow.RiskAssessment riskAss;
       

        //-----------------------------
        snocheckbox checkbox = null;
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snobutton button = null;
        snodatetime datetime = null;
       
       //**********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                prb = new SNow.Problem(Base, "Problem");
                prolist = new SNow.ProblemList(Base, "Problem list");
                chg = new SNow.Change(Base, "Change");
                chglist = new SNow.ChangeList(Base, "Change list");
                riskAss = new SNow.RiskAssessment(Base);
             
                //-----------------------------------------------------

                problemId = string.Empty;
                changeId = string.Empty;
                
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_00_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_Create_Change()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_Get_TicketNumber()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "CHG*");
                        if (!flag) error = "Invalid format of change number.";
                        else { changeId = temp; Console.WriteLine("-*-[STORE]: Change Id:(" + changeId + ")"); }
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_005_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("ChgCompany");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("ChgCategory");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ChgShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                //---------------------------------------
               
                if (flag)
                {
                    string startDate = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");
                    string endDate = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("ChgAssignmentGroup");
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_01_PopulateAssignedTo_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_02_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Moderate");
                    if (!flag) { error = "Cannot populate priority value."; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_03_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText.ToLower();
                    flag = temp.Equals("change requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Change Requests)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_01_ClickOn_Tab_Risk_Assessment()
        {
            try
            {
                flag = chg.Select_Tab("Risk Assessment");
                if (!flag)
                {
                    error = "Cannot click Risk Assessment Tab.";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_02_Open_RiskAssessmentForm()
        {
            try
            {
                button = chg.Button_FillOutRiskAssessment();
                flag = button.Existed;
                if (!flag)
                    error = "Cannot get button fill out risk assessment.";
                else
                {
                    flag = button.Click();
                    if (flag)
                    {
                        riskAss.WaitLoading();

                        Thread.Sleep(2000);

                        // Verify Dialog title
                        SNow.snoelement label = riskAss.Title();
                        flag = label.Existed;
                        if (flag)
                        {
                            flag = label.MyText.Equals("Risk Assessment");
                            if (!flag)
                            {
                                error = "Invalid title.";
                            }
                        }
                        else
                        {
                            error = "Cannot get label.";
                        }
                    }
                    else error = "Error when click on button fill out risk assessment";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_03_Fill_RiskAssessmentForm()
        {
            try
            {
                flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                if (!flag)
                {
                    error = "Cannot answer Risk Assessment questions";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_04_Submit_RiskAssessmentForm()
        {
            try
            {
                //button = riskAss.Button_Submit();
                //flag = button.Existed;
                //if (flag)
                //{
                //    flag = button.Click(true);
                //    if (!flag)
                //    {
                //        error = "Cannot click Submit on Risk Assessment form1";
                //    }
                //    else chg.WaitLoading();
                //}
                //else
                //{
                //    error = "Cannot get button submit.";
                //}

                SNow.snoelement ele = riskAss.Form_Submit();
                ele.MyElement.Submit();
                Thread.Sleep(2000);
                button = riskAss.Button_Close();
                flag = button.Existed;
                if (flag)
                    button.Click();
                else
                {
                    error = "Cannot click submit";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_05_Reload_Form()
        {
            try
            {
                flag = chg.ReloadForm();
                if (flag)
                    chg.WaitLoading();
                else
                {
                    error = "Cannot Reload form.";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_06_Verify_ExecuteRiskCalculation_Completed()
        {
            try
            {
                datetime = chg.Datetime_Risk_Assessment_Last_Run();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.Text.Trim() != "";
                    if (!flag)
                        error = "Risk calculation NOT completed.";
                }
                else { error = "Cannot get datetime last run."; }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_07_RequestReview()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else error = "Error when click on request approval button.";
                }
                else error = "Cannot get button request approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_08_Verify_State_Review()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Review");
                    if (!flag) error = "Invalid init state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_09_RequestApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else error = "Error when click on request approval button.";
                }
                else error = "Cannot get button request approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_016_Verify_Change_State()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Approval");
                    if (!flag) error = "Invalid init state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_ImpersonateUser_ProblemManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                {
                    error = "Cannot select create new problem";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "PRB*");
                        if (!flag) error = "Invalid format of Problem number.";
                        else { problemId = temp; Console.WriteLine("-*-[STORE]: Problem Id:(" + problemId + ")"); }
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }

                //-- Input company
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string company = Base.GData("Company");
                    flag = lookup.Select(company);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else
                { error = "Cannot get company field."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_01_PopulateProblemImpact()
        {
            try
            {
                combobox = prb.Combobox_Impact();
                string temp = Base.GData("ProImpact");
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Problem impact."; }
                }
                else { error = "Cannot get textbox Problem impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_02_PopulateProblemStatement()
        {
            try
            {
                textbox = prb.Textbox_ProblemStatement();
                string temp = Base.GData("ProStatement");
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group."; }

                }
                else { error = "Cannot get lookup Assignment Group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp); 
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_ImpersonateProblemAssignee()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("ProAssignee"), true, temp, false);
                if (!flag)
                {
                    error = "Cannot impersonate Problem Assignee";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_011_SearchAOpen_Problem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------
                temp = "Number=" + problemId;
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    flag = prolist.SearchAndOpen("Number", problemId, temp, "Number");
                    if (!flag) error = "Error when search and open problem (Id:" + problemId + ")";
                    else
                    {
                        prb.WaitLoading();
                    }
                }
                else { error = "Cannot open Problem list"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
       
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_PopulateAssignedTo()
        {
            try
            {
               string temp = Base.GData("ProAssignee");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    { error = "Cannot input Assigned To."; }
                }
                else { error = "Cannot get lookup Assigned To"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_PopulateChangeRelate()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------
                lookup = prb.Lookup_ChangeRequest();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(changeId);
                    if (!flag)
                    { error = "Cannot input Change Request."; }
                }
                else { error = "Cannot get lookup Change Request"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_016_SearchAOpen_Change()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------
                string conditions = "Number=" + changeId;
                flag = home.LeftMenuItemSelect("Change", "Open");

                if (flag)
                {
                    prolist.WaitLoading();
                    flag = prolist.SearchAndOpen("Number", changeId, conditions, "Number");
                    if (!flag)
                        error = "Error when search and open change (Id:" + changeId + ")";
                    else
                    {
                        chg.WaitLoading();
                    }
                }
                else { error = "Cannot open Change list"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_ValidateProblemRelateToChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------
                flag = chg.Verify_RelatedTable_Row("Problems", "Number=" + problemId);
                if (!flag)
                {
                    error = "There is NO problem id: " + problemId + " related.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_ResolveTheTicket()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                flag = chg.RelatedTableOpenRecord("Problems", "Number=" + problemId, "Number");
                if (!flag)
                    error = "Cannot open record.";
                else
                    prb.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_ChangeStateFromOpenToClose()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = prb.Combobox_State().SelectItem("Closed/Resolved");
                    if (!flag)
                    {
                        error = "Cannot select [Closed/Resolved] in State combobox";
                    }
                }
                else
                {
                    error = "Can not find combobox State";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_UpdateCloseNotes()
        {
            try
            {
                string temp = Base.GData("ProCloseNote");
                textarea = prb.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_CauseCode()
        {
            try
            {
                string temp = Base.GData("ProCauseCode");
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Close Code value."; }
                }
                else
                    error = "Cannot get combobox Close Code.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_CheckKnowdlge()
        {
            try
            {
                checkbox = prb.Checkbox_Knowledge();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click(true);
                        if (!flag)
                        { error = "Cannot check Knowledge checkbox"; }
                    }
                }
                else { error = "Cannot find Knowledge checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Update()
        {
            try
            {
                flag = prb.Update();
                if (!flag) error = "Error when update Problem.";
                else
                    chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_VerifyProblemTicket()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------
                flag = chg.Verify_RelatedTable_Row("Problems", "Number=" + problemId + "|State=Closed/Resolved", false);
                if (!flag)
                {
                    error = "There is NO problem id: " + problemId + " related.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_VerifyChangeState()
        {
            try
            {
                combobox = chg.Combobox_State();
                if (combobox.Existed)
                {
                    flag = combobox.VerifyCurrentValue("Approval");
                    if (!flag)
                    {
                        error += "The value of State is not [Approval]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else
                {
                    error += "Cannot get State combobox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_29_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }
}




    

