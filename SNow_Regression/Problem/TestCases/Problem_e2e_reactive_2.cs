﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Problem
{
    [TestFixture]
    public class Problem_e2e_reactive_2
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Problem Id: " + problemId);
            System.Console.WriteLine("Finished - Problem Task Id: " + problemTaskId);
            System.Console.WriteLine("Finished - Root Cause Identified Id: " + rootCauseIdentified);
            System.Console.WriteLine("Finished - Workaround Identified Id: " + workaroundIdentified);



            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************


        string problemId, incidentId, problemTaskId;
        string rootCauseIdentified, workaroundIdentified;

        Login login;
        Home home;
        SNow.Incident inc;
        SNow.IncidentList incList;
        SNow.Problem prb;
        SNow.ProblemList prolist;
        SNow.ProblemTask proTask;
        SNow.Member member;
        SNow.Search proSearch;
        SNow.Email email;
        SNow.EmailList emailList;
        

       

        //-----------------------------
        snocheckbox checkbox = null;
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snobutton button = null;
        snodatetime datetime=null;
        snoelement ele = null;
       
       //**********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                prb = new SNow.Problem(Base, "Problem");
                prolist = new SNow.ProblemList(Base, "Problem list");
                proTask = new SNow.ProblemTask(Base, "Problem Task");
                member = new SNow.Member(Base);
                proSearch = new SNow.Search(Base, "Problem search");
                email = new SNow.Email(Base, "Email");
                emailList = new SNow.EmailList(Base, "Email list");
             
                //-----------------------------------------------------

                incidentId = string.Empty;
                problemId = string.Empty;
                problemTaskId = string.Empty;
                rootCauseIdentified = string.Empty;
                workaroundIdentified = string.Empty;
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_ImpersonateUser_ServiceDeskAgent()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_01_PopulateCaller()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate subcategory value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_PopulateImpact()
        {
            try
            {
                string temp = Base.GData("Impact");
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Impact combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_PopulateUrgency()
        {
            try
            {
                string temp = Base.GData("Urgency");
                combobox = prb.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Urgency combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_Verify_PriorityValue()
        {
            try
            {
                Thread.Sleep(2000);
                string temp = Base.GData("Priority");
                combobox = prb.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid Prioriy value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    }
                }
                else { error = "Cannot get Priority combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_012_SaveIncident()
        {
            try
            {
                flag = inc.Save(false,true);
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Handle alert when create Incident with Priority = 1 or 2
        /// Loc Truong Update 11.10/11.11
        /// </summary>
        [Test]
        public void Pre_013_Handle_Alert()
        {
            try
            {
                flag = prb.HandleAlert("accept");
                if (!flag)
                {
                    error = "Cannot Hanle Alert.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_ImpersonateUser_ProblemManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_to_006_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_CreateProblem()
        {
            try
            {
                flag = inc.CreateProblem();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Cannot click to create problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_02_Verify_RelatedP1P2_Checkbox()
        //{
        //    try
        //    {
        //        checkbox = prb.Checkbox_RelatedP1P2_Incident();
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            flag = checkbox.Checked;
        //            if (!flag)
        //            {
        //                error = "Checkbox is NOT checked. Expected: Checked.";
        //                flagExit = false;
        //            }
        //        }
        //        else { error = "Cannot get Related P1 P2 Incident checkbox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_VerifyInformation()
        {
            try
            {
                Thread.Sleep(5000);
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    //Get Problem Id number
                    problemId = textbox.Text;
                    lookup = prb.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Company");
                        if (lookup.VerifyCurrentValue(temp, true))
                        {
                            lookup = prb.Lookup_Location();
                            flag = lookup.Existed;
                            if (flag)
                            {
                                temp = Base.GData("IncLocation");
                                if (lookup.VerifyCurrentValue(temp, true))
                                {
                                    textbox = prb.Textbox_ProblemStatement();
                                    flag = textbox.Existed;
                                    if (flag)
                                    {
                                        temp = Base.GData("IncShortDescription");
                                        if (textbox.VerifyCurrentValue(temp))
                                        {
                                            flagExit = false;
                                            error = "The value of Problem Statement is not correct.";
                                        }
                                    }
                                    else { error = "Cannot get Problem Statement control."; }
                                }
                                else { error = "The value of location is not correct."; flagExit = false; }
                            }
                            else { error = "Cannot get location control."; }
                        }
                        else { error = "The value of caller is not correct."; flagExit = false; }
                    }
                    else { error = "Cannot get caller control."; }
                }
                else { error = "Cannot get textbox number control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateImpact()
        {
            try
            {
                string temp = "1 - High";
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Impact combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_ClickUpdateButton()
        {
            try
            {
                button = prb.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot click update button."; }
                }
                else { error = "Cannot get update button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Verify_ProblemField()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = prb.Select_Tab("Related Records");
                //---------------------------------------
                
                if (flag)
                {
                    lookup = inc.Lookup_Problem();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(problemId);
                        if (!flag)
                        {
                            error = "Invalid Problem ID value. Expected: [" + problemId + "]. Runtime: [" + lookup.Text + "]";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get Problem lookup."; }
                }
                else { error = "Cannot click tab (Related Records)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_011_SearchAndOpenProblem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prolist.WaitLoading();
                    temp = prolist.List_Title().MyText.ToLower(); 
                    flag = temp.Equals("problems");
                    if (flag)
                    {
                        flag = prolist.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                        if (!flag) error = "Error when search and open problem (id:" + problemId + ")";
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_01_Verify_DeleteButton_Disappeared()
        //{
        //    try
        //    {
        //        button = prb.Button_Delete();
        //        flag = button.Existed;
        //        if (!flag)
        //        {
        //            flag = true;
        //            System.Console.WriteLine("***PASS: Expected disappeared.");
        //        }
        //        else
        //        {
        //            flag = false;
        //            flagExit = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_ValidateIncidentRelated()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + incidentId;
                flag = prb.Verify_RelatedTable_Row("Incidents", condition);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found Incident response: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_PopulateCI()
        {
            try
            {
                string temp = Base.GData("ProCI");
                lookup = prb.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate CI value.";
                    }
                }
                else { error = "Cannot get CI lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_PopulatePriority()
        {
            try
            {
                string temp = "1 - Critical";
                combobox = prb.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate priority value.";
                    }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("ProCat");
                combobox = prb.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("ProSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate subcategory value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_PopulateAssignee()
        {
            try
            {
                string temp = Base.GData("ProAssignee");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment to value."; }
                }
                else { error = "Cannot get lookup assignment to field."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_PopulateDescription()
        {
            try
            {
                textarea = prb.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProDescription");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate description value.";
                    }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_PopulateWorkNotes()
        {
            try
            {
                string temp = Base.GData("ProworkNotes");
                flag = prb.Add_Worknotes(temp);
                if(!flag)
                    error = "Cannot populate work note value.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_AttachAFile()
        {
            try
            {
                string attachmentFile = "problemAttachment.txt";
                flag = prb.Add_AttachmentFile(attachmentFile);
                if (!flag)
                {
                    error = "Cannot attach file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_AttachmentFile()
        {
            try
            {
                string attachmentFile = "problemAttachment.txt";
                flag = prb.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Delete_AttachmentFile()
        {
            try
            {
                string attachmentFile = "problemAttachment.txt";
                flag = prb.Delete_AttachmentFile(attachmentFile);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Verify_AttachmentFileDeleted()
        {
            try
            {
                string attachmentFile = "problemAttachment.txt";
                bool flagTemp = prb.Verify_Attachment_File(attachmentFile, true);
                if (flagTemp)
                {
                    error = "Found attachment file (" + attachmentFile + ") in attachment container. Expected: NOT found.";
                    flag = false;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_VerifyGlobalOLA()
        {
            try
            {
                string temp = Base.GData("GlobalOLA");
                if (temp.ToLower() != "no") 
                {
                    string condition = "SLA=" + temp + "|Stage=In progress";

                    flag = prb.Verify_RelatedTable_Row("Task SLAs", condition);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found Task SLA response: " + condition;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("ProCat");
                combobox = prb.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("ProSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate subcategory value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_04_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_025_05_Verify_MissingCICheckbox_Uncheck()
        //{
        //    try
        //    {
        //        checkbox = prb.Checkbox_MissingConfigurationItem();
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            flag = checkbox.Checked;
        //            if (flag)
        //            {
        //                flag = false;
        //                flagExit = false;
        //                error = "Missing CI checkbox is checked. Expected: NOT checked.";
        //            }
        //            else
        //            {
        //                flag = true;
        //                System.Console.WriteLine("***(PASSED): Missing CI checkbox is not checked");
        //            }
        //        }
        //        else { error = "Cannot get Missing CI checkbox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_CreateNewProblemTask()
        {
            try
            {
                flag = prb.Select_Tab("Problem Tasks");
                //---------------------------------------
                
                if (flag)
                {
                    button = proTask.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            proTask.WaitLoading();
                        }
                        else { error = "Cannot click new button."; }
                    }
                    else { error = "Cannot get new button."; }
                }
                else { error = "Cannot click tab (Problem Tasks)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_PopulateAssignmentGroup()
        {
            try
            {
                //Get Problem Task Id number
                textbox = proTask.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    problemTaskId = textbox.Text;
                    string temp = Base.GData("ProTask_AssignmentGroup");
                    lookup = proTask.Lookup_AssignmentGroup();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag)
                        {
                            error = "Cannot populate assignment group value.";
                        }
                    }
                    else { error = "Cannot get lookup assignment group."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ProTask_ShortDescription");
                textbox = proTask.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate short description value.";
                    }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_PopulateDescription()
        {
            try
            {
                string temp = Base.GData("ProTask_Description");
                textarea = proTask.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate description value.";
                    }
                }
                else { error = "Cannot get texarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_PopulateDueDate()
        {
            try
            {
                string temp = System.DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd HH:mm:ss");
                datetime = proTask.Datetime_DueDate();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.SetText(temp, true);
                    if (!flag)
                    { error = "Cannot populate Due date"; }
                }
                else { error = "Cannot get datetime field Due date"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_AttachAFile()
        {
            try
            {
                string attachmentFile = "problemAttachment_1.txt";
                flag = proTask.Add_AttachmentFile(attachmentFile);
                if (!flag)
                {
                    error = "Cannot attach file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Verify_AttachmentFile()
        {
            try
            {
                string attachmentFile = "problemAttachment_1.txt";
                flag = proTask.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_01_Submit_ProblemTask()
        {
            try
            {
                button = proTask.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot click submit button."; }
                }
                else { error = "Cannot get submit button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;

            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_033_02_Verify_TaskPassedDueDate_Checked()
        //{
        //    try
        //    {
        //        checkbox = prb.Checkbox_TaskPassedDueDate();
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            flag = checkbox.Checked;
        //            if (!flag)
        //            {
        //                error = "Task Passed Due Date is NOT checked. Expected: Checked.";
        //                flagExit = false;
        //            }
        //        }
        //        else { error = "Cannot get Task Passed Due Date checkbox."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_03_Verify_ProblemTask_ProblemTaskTable()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemTaskId == null || problemTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task Id.");
                    addPara.ShowDialog();
                    problemTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + problemTaskId + "|Short description=" + Base.GData("ProTask_ShortDescription");
                flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions);
                if (!flag)
                {
                    error = "Cannot verify row with condition: " + conditions;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_ImpersonateUser_TaskResolver()
        {
            try
            {
                string temp = Base.GData("TaskResolver");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_037_SearchAndOpenProblemTask()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemTaskId == null || problemTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem task Id.");
                    addPara.ShowDialog();
                    problemTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (flag)
                {
                    prolist.WaitLoading();
                    temp = prolist.List_Title().MyText.ToLower();
                    flag = temp.Equals("tasks");
                    if (flag)
                    {
                        flag = prolist.SearchAndOpen("Number", problemTaskId, "Number=" + problemTaskId, "Number");
                        if (!flag) error = "Error when search and open problem task (id:" + problemTaskId + ")";
                        else prb.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Tasks)";
                    }
                }
                else error = "Error when select open problem task.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_38");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 38. Expected:(" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_PopulateAssignTo()
        {
            try
            {
                string temp = Base.GData("TaskResolver");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment to value."; }
                }
                else { error = "Cannot get lookup assignment to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SaveProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_41");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity note 41. Expected:(" + temp + ")";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_PopulateCI()
        {
            try
            {
                string temp = Base.GData("ProTask_CI");
                lookup = prb.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate CI value.";
                    }
                }
                else { error = "Cannot get CI lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_PopulateWorkNotes()
        {
            try
            {
                textarea = prb.Textarea_Problem_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProTask_WorkNotes");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate work note value.";
                    }
                }
                else { error = "Cannot get textarea worknote."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_SaveProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_45_01");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 45_01. Expected:(" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_45_02");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 45_02. Expected:(" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Delete_AttachmentFile()
        {
            try
            {
                string attachmentFile = "problemAttachment_1.txt";
                flag = prb.Delete_AttachmentFile(attachmentFile);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Verify_AttachmentFileDeleted()
        {
            try
            {
                string attachmentFile = "problemAttachment_1.txt";
                bool flagTemp = prb.Verify_Attachment_File(attachmentFile, true);
                if (flagTemp)
                {
                    error = "Found attachment file (" + attachmentFile + ") in attachment container. Expected: NOT found.";
                    flag = false;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_PopulateState()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                   string temp = "Work in Progress";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    { error = "Cannot populate state value."; }
                }
                else { error = "Cannot get combobox state"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_SaveProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_50");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 50. Expected:(" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_PopulateState()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    { error = "Cannot populate state value."; }
                }
                else { error = "Cannot get combobox state"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_SaveProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_53");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 53. Expected:(" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_PopulateState()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    { error = "Cannot populate state value."; }
                }
                else { error = "Cannot get combobox state"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_SaveProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_VerifyActivitySection()
        {
            try
            {
                string temp = Base.GData("Activity_56");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    error = "Invalid activity note 56. Expected:(" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_to_064_ImpersonateUser_ProblemAssignee()
        {
            try
            {
                string temp = Base.GData("ProAssignee");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_067_SearchAndOpenProblem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prolist.WaitLoading();
                    temp = prolist.List_Title().MyText.ToLower();
                    flag = temp.Equals("problems");
                    if (flag)
                    {
                        flag = prolist.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                        if (!flag) error = "Error when search and open problem (id:" + problemId + ")";
                        else prb.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_CheckMeetRCA()
        {
            try
            {
                checkbox = prb.Checkbox_RCADelivered();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click(true);
                        if (!flag)
                        { error = "Cannot check RCA Delivered checkbox"; }
                    }
                }
                else { error = "Cannot find RCA Delivered checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_01_Click_ViewRCA_Link()
        {
            try
            {
                ele = prb.GRelatedLink("View RCA");
                flag = ele.Click();
                if (!flag)
                {
                    error = "Cannot click View RCA link.";
                }
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_02_Verify_Tabs_Displayed()
        {
            try
            {
                string temp = Base.GData("ViewRCA_Tabs");
                flag = prb.VerifyTabHeader(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid tab list. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_01_Populate_RootCauseStatement()
        {
            try
            {
                flag = prb.Select_Tab("RCA");

                if (flag)
                {
                    textbox = prb.Textbox_RootCauseStatement();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("RootCauseStatement");
                        flag = textbox.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot populate Root Cause Statement value.";
                        }
                    }
                    else { error = "Cannot get Root Cause Statement textbox."; }
                }
                else { error = "Cannot click tab (RCA)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_02_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                {
                    error = "Cannot save Problem.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_03_Verify_RootCauseIdentified_Autopopulated()
        {
            try
            {
                flag = prb.Select_Tab("Compliance");
               
                if (flag)
                {
                    datetime = prb.Datetime_RootCauseIdentified();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        if (datetime.Text == string.Empty)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid Root Cause Identified value. Expected: Has value.";
                        }
                        else
                        {
                            rootCauseIdentified = datetime.Text;
                        }
                    }
                    else { error = "Cannot get Root Cause Identified"; }
                }
                else { error = "Cannot click tab (Compliance)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_01_Populate_RootCauseStatement()
        {
            try
            {
                flag = prb.Select_Tab("RCA");
               
                if (flag)
                {
                    textbox = prb.Textbox_RootCauseStatement();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("RootCauseStatement_Update");
                        flag = textbox.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot populate Root Cause Statement value.";
                        }
                    }
                    else { error = "Cannot get Root Cause Statement textbox."; }
                }
                else { error = "Cannot click tab (RCA)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_02_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                {
                    error = "Cannot save Problem.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_03_Verify_RootCauseIdentified_Unchanged()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (rootCauseIdentified == null || rootCauseIdentified == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Root Cause Identified.");
                    addPara.ShowDialog();
                    rootCauseIdentified = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = prb.Select_Tab("Compliance");
                //---------------------------------------
                
                if (flag)
                {
                    datetime = prb.Datetime_RootCauseIdentified();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.VerifyCurrentValue(rootCauseIdentified);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid Root Cause Identified value. Expected: [" + rootCauseIdentified + "]. Runtime: [" + datetime.Text + "]";
                        }
                    }
                    else { error = "Cannot get Root Cause Identified"; }
                }
                else { error = "Cannot click tab (Compliance)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_01_Clear_RootCauseStatement()
        {
            try
            {
                flag = prb.Select_Tab("RCA");
                //---------------------------------------
               
                if (flag)
                {
                    textbox = prb.Textbox_RootCauseStatement();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText("");
                        if (!flag)
                        {
                            error = "Cannot clear Root Cause Statement value.";
                        }
                    }
                    else { error = "Cannot get Root Cause Statement textbox."; }
                }
                else { error = "Cannot click tab (RCA)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_02_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                {
                    error = "Cannot save Problem.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_03_Verify_RootCauseIdentified_Blank()
        {
            try
            {
                flag = prb.Select_Tab("Compliance");
                //---------------------------------------
                
                if (flag)
                {
                    datetime = prb.Datetime_RootCauseIdentified();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        string temp = datetime.Text;
                        if (temp != string.Empty)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid Root Cause Identified value. Expected: BLANK.";
                        }
                    }
                    else { error = "Cannot get Root Cause Identified"; }
                }
                else { error = "Cannot click tab (Compliance)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_01_Populate_Workaround()
        {
            try
            {
                flag = prb.Select_Tab("Resolution Information");
               
                if (flag)
                {
                    textarea = prb.Textarea_Workaround();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Workaround");
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot populate Workaround value.";
                        }
                    }
                    else { error = "Cannot get Workaround textarea."; }
                }
                else { error = "Cannot click tab (Resolution Information)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_02_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                {
                    error = "Cannot save Problem.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_03_Verify_WorkaroundIdentified_Autopopulated()
        {
            try
            {
                flag = prb.Select_Tab("Compliance");
                //---------------------------------------

                if (flag)
                {
                    datetime = prb.Datetime_WorkaroundIdentified();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        string temp = datetime.Text;
                        if (temp == string.Empty)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid Workaround value. Expected: Has value.";
                        }
                        else
                        {
                            workaroundIdentified = datetime.Text;
                        }
                    }
                    else { error = "Cannot get Workaround Identified"; }
                }
                else { error = "Cannot click tab (Compliance)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_066_01_Populate_Workaround_Again()
        //{
        //    try
        //    {
        //        tab = prb.GTab("Resolution Information");
        //        //---------------------------------------
        //        int i = 0;
        //        while (tab == null && i < 5)
        //        {
        //            Thread.Sleep(2000);
        //            tab = prb.GTab("Resolution Information", true);
        //            i++;
        //        }
        //        //---------------------------------------
        //        flag = tab.Header.Click(true);
        //        if (flag)
        //        {
        //            textarea = prb.Textarea_Workaround;
        //            flag = textarea.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Workaround_Update");
        //                flag = textarea.SetText(temp);
        //                if (!flag)
        //                {
        //                    error = "Cannot populate Workaround value.";
        //                }
        //            }
        //            else { error = "Cannot get Workaround textarea."; }
        //        }
        //        else { error = "Cannot click tab (Resolution Information)."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_066_02_SaveProblem()
        //{
        //    try
        //    {
        //        flag = prb.Save();
        //        if (!flag)
        //        {
        //            error = "Cannot save Problem.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_066_03_Verify_WorkaroundIdentified_Unchanged()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (workaroundIdentified == null || workaroundIdentified == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Workaround Identified.");
        //            addPara.ShowDialog();
        //            workaroundIdentified = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        tab = prb.GTab("Compliance");
        //        //---------------------------------------
        //        int i = 0;
        //        while (tab == null && i < 5)
        //        {
        //            Thread.Sleep(2000);
        //            tab = prb.GTab("Compliance", true);
        //            i++;
        //        }
        //        //---------------------------------------
        //        flag = tab.Header.Click(true);
        //        if (flag)
        //        {
        //            datetime = prb.Datetime_WorkaroundIdentified;
        //            flag = datetime.Existed;
        //            if (flag)
        //            {
        //                flag = datetime.VerifyCurrentText(workaroundIdentified);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid Workaround Identified value. Expected: [" + workaroundIdentified + "]. Runtime: [" + datetime.Text + "] ";
        //                }
        //            }
        //            else { error = "Cannot get Workaround Identified"; }
        //        }
        //        else { error = "Cannot click tab (Compliance)."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_01_Populate_RootCause_Statement_Description()
        {
            try
            {
                flag = prb.Select_Tab("RCA");
                //---------------------------------------
              
                if (flag)
                {
                    textbox = prb.Textbox_RootCauseStatement();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("RootCauseStatement");
                        flag = textbox.SetText(temp);
                        if (flag)
                        {
                            textarea = prb.Textarea_RootCauseDescription();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("RootCauseDescription");
                                flag = textarea.SetText(temp);
                                if (!flag)
                                {
                                    error = "Cannot populate Root Cause Description value.";
                                }
                            }
                            else { error = "Cannot get Root Cause Description textarea."; }
                        }
                        else { error = "Cannot populate Root Cause Statement value."; }
                    }
                    else { error = "Cannot get Root Cause Statement textbox."; }
                }
                else { error = "Cannot click tab (RCA)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_02_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag)
                {
                    error = "Cannot save Problem.";
                }
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_03_Verify_RootCauseIdentified_Autopopulated()
        {
            try
            {
                flag = prb.Select_Tab("Compliance");
                //---------------------------------------
                
                if (flag)
                {
                    datetime = prb.Datetime_RootCauseIdentified();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        string temp = datetime.Text;
                        if (temp == string.Empty)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid Root Cause Identified value. Expected: Has value.";
                        }
                    }
                    else { error = "Cannot get Root Cause Identified"; }
                }
                else { error = "Cannot click tab (Compliance)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_04_BackTo_DefaultView()
        {
            try
            {
                string temp = Base.GData("View");
                flag = prb.SelectView(temp);
                if (!flag)
                {
                    error = "Cannot select view: [" + temp + "]";
                }
                else prb.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_076_PopulateState()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed/Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    { error = "Cannot populate state value."; }
                    else prb.WaitLoading();
                }
                else { error = "Cannot get combobox state"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_01_UpdateCloseCodeAndCloseNote()
        {
            try
            {
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProCauseCode");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        textarea = prb.Textarea_CloseNotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string close_note = Base.GData("ProCloseNote");
                            flag = textarea.SetText(close_note);
                            if (!flag)
                            { error = "Cannot input Problem Close Notes"; }
                        }
                        else { error = "Not found Textarea Close Notes"; }
                    }
                    else { error = "Cannot select Problem Close Code"; }
                }
                else { error = "Not found Combobox Close Code"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_02_Input_ClosureCode()
        {
            try
            {
                combobox = prb.Combobox_ClosureCode();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("No Cause Identified");
                    if (!flag)
                    { error = "Cannot select Problem Closure Code"; }
                }
                else { error = "Not found Combobox Closure Code"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_VerifyGlobalOLA()
        {
            try
            {
                string temp = Base.GData("GlobalOLA");
                if (temp.ToLower() != "no") 
                {
                    string condition = "SLA=" + temp + "|Stage=Completed";
                    flag = prb.Verify_RelatedTable_Row("Task SLAs", condition);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found Task SLA response: " + condition;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_ImpersonateUser_TestUser()
        {
            try
            {
                string temp = Base.GData("TestUser");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_082_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_083_Verify_Email_Problem_Assignee_When_AssignToAssignee()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && problemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                string group = Base.GData("ProAssignmentGroup");
                string email = Base.GData("ProAssigneeEmail");
                if (email.ToLower() == "no" || email.ToLower() == "empty")
                {
                    email = Base.GData("ProAssignmentGroupEmail");
                }
                temp = "Subject;contains;" + problemId + " has been assigned to you" + "|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + problemId;
                    flag = emailList.VerifyRow(conditions);
                    if (!flag) error = "Not found email sent to Problem Assignee.";
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_00_Open_Email_Problem_Assignee_When_ProTaskClosed()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";

                string temp = Base.GData("Debug");
                if (temp == "yes" && problemTaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task Id.");
                    addPara.ShowDialog();
                    problemTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------
                string recipientEmail = Base.GData("ProAssigneeEmail");
                temp = "Subject;contains;" + problemTaskId + " has been Closed" + "|and|Recipients;contains;" + recipientEmail;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + problemTaskId;
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Cannot open email problem Task Id.";
                    else email.WaitLoading();
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_01_Verify_Subject()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && problemTaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task Id.");
                    addPara.ShowDialog();
                    problemTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------   
                temp = "Problem task " + problemTaskId + " has been Closed";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject value. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_02_Verify_Recipients()
        {
            try
            {
                
                string group = Base.GData("ProAssigneeEmail");
                flag = email.VerifyRecipient(group);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient value. Expected: [" + group + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_084_03_Click_PreviewHTLMBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (flag)
                {
                    error = "Cannot click on Preview HTLM Body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_084_04_Verify_ShortDescription()
        {
            try
            {
                //string shortDes = Base.GData("ProTask_ShortDescription");
                string temp = "Short description: Test Problem Task 01";
                
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid short description value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_05_Verify_ProblemTask()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && problemTaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task Id.");
                    addPara.ShowDialog();
                    problemTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------- 
                temp = "Click here to view Problem Task: " + problemTaskId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid short description value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_06_Verify_State()
        {
            try
            {
                string temp = "State: Closed Complete";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid State value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_07_Verify_ConfigurationItem()
        {
            try
            {
                string ci = Base.GData("ProTask_CI");
                string temp = "Configuration item: " + ci;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid CI value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_08_Verify_AssignmentGroup()
        {
            try
            {
                string group = Base.GData("ProAssignmentGroup");
                string temp = "Assignment group: " + group;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Assignment group value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_09_Verify_AssignmentTo()
        {
            try
            {
                string resolver = Base.GData("TaskResolver");
                string temp = "Assigned to: " + resolver;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Assigned to value. Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_10_Verify_Description()
        {
            try
            {
                string description = Base.GData("ProTask_Description");
                flag = email.VerifyEmailBody(description);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Description value. Expected: [" + description + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_85_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }
}




    

