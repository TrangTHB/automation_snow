﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Problem
{
    [TestFixture]
    public class Problem_relate_incident_12
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Finished - Problem Id: " + problemId);
            System.Console.WriteLine("Finished - Incident 1 Id: " + incidentId1);
            System.Console.WriteLine("Finished - Incident 2 Id: " + incidentId2);
            System.Console.WriteLine("Defects - Existing defect: " + Base.GData("Existing Defect"));


            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        
        string incidentId1;
        string incidentId2;
        string problemId;

        Login login;
        Home home;
        SNow.Incident inc;
        SNow.IncidentList incList;
        SNow.Problem prb;
        SNow.ProblemList prolist;
        SNow.Member member;
       

        //-----------------------------
        snocheckbox checkbox = null;
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snobutton button = null;
       
       //**********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                prb = new SNow.Problem(Base, "Problem");
                prolist = new SNow.ProblemList(Base, "Problem list");
                member = new SNow.Member(Base);
             
                //-----------------------------------------------------

                problemId = string.Empty;
                incidentId1 = string.Empty;
                incidentId2 = string.Empty;
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_005_OpenNewIncident1()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId1 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId1 + ")");
                    string temp = Base.GData("Caller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_PopulateImpact()
        {
            try
            {
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "1 - Critical";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                    }
                    else { error = "Cannot populate (" + temp + ") value."; }
                }
                else
                {
                    error = "Cannot get Impact combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_PopulateUrgency()
        {
            try
            {
                combobox = inc.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "1 - Critical";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                    }
                    else { error = "Cannot populate (" + temp + ") value."; }
                }
                else
                {
                    error = "Cannot get Urgency combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_SaveIncident()
        {
            try
            {
                flag = inc.Save(false, true);
                if (!flag) 
                { 
                    error = "Error when save incident."; 
                }
                else
                {
                    IAlert alert = Base.Driver.SwitchTo().Alert();
                    string temp = "This Incident Record is now a Major Incident!Please contact your Service Desk to make sure it gets the attention it needs.";
                    string runtime = alert.Text.Replace("\r\n", "");
                    if (!runtime.Equals(temp))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid alert message. Runtime:(" + runtime + "). Expexted:(" + temp + ")";
                    }
                    alert.Accept();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_OpenNewIncident2()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId2 = textbox.Text;
                    string temp = Base.GData("Caller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_016_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_017_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_018_PopulateImpact()
        {
            try
            {
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "2 - High";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                    }
                    else { error = "Cannot populate (" + temp + ") value."; }
                }
                else
                {
                    error = "Cannot get Impact combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_019_PopulateUrgency()
        {
            try
            {
                combobox = inc.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "2 - High";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                    }
                    else { error = "Cannot populate (" + temp + ") value."; }
                }
                else
                {
                    error = "Cannot get Urgency combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_020_SaveIncident()
        {
            try
            {
                flag = inc.Save(false, true);
                if (!flag)
                {
                    error = "Error when save incident.";
                }
                else
                {
                    IAlert alert = Base.Driver.SwitchTo().Alert();
                    string temp = "This Incident Record is now a Major Incident!Please contact your Service Desk to make sure it gets the attention it needs.";
                    string runtime = alert.Text.Replace("\r\n", "");
                    if (!runtime.Equals(temp))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid alert message. Runtime:(" + runtime + "). Expexted:(" + temp + ")";
                    }
                    alert.Accept();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_ImpersonateUser_ProblemManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_004_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    prb.WaitLoading();
                }
                else
                {
                    error = "Cannot select create new problem";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "PRB*");
                        if (!flag) error = "Invalid format of Problem number.";
                        else { problemId = temp; Console.WriteLine("-*-[STORE]: Problem Id:(" + problemId + ")"); }
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }

                //-- Input company
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string company = Base.GData("Company");
                    flag = lookup.Select(company);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else
                { error = "Cannot get company field."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_01_PopulateProblemImpact()
        {
            try
            {
                combobox = prb.Combobox_Impact();
                string temp = Base.GData("ProImpact");
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Problem impact."; }
                }
                else { error = "Cannot get textbox Problem impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_PopulateProblemStatement()
        {
            try
            {
               string  temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && problemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                textbox = prb.Textbox_ProblemStatement();
                temp = Base.GData("ProStatement") + " - " + problemId;
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_PopulateAssigmentGroup()
        {
            try
            {
                string temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group."; }

                }
                else { error = "Cannot get lookup Assignment Group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_01_AssignTicket()
        {
            try
            {
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                   string  temp = Base.GData("ProAssignee");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assigned to.";
                    }
                }
                else
                    error = "Cannot find element Assigned To.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_02_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp); 
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

       
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                { prb.WaitLoading(); }
                else
                {
                    error = "Cannot save problem.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_ClickEditbutton()
        {
            try
            {
                flag = prb.Select_Tab("Incidents");
               
                if (flag)
                {
                    button = prb.Button_Edit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                    }

                    else {
                        error = "Cannot click Edit button"; 
                    }
                }
                else { error = "Cannot select Incidents tab"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_01_AddMember_Incident1()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId1 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id 1.");
                    addPara.ShowDialog();
                    incidentId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                flag = member.AddMember(incidentId1);
                if (!flag)
                {
                    error = "Cannot add incident Id (" + incidentId1 + ").";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_SaveMember()
        {
            try
            {
                button = member.Button_Save();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                }
                else
                {
                    error = " Cannot save member.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_RelatedTable_OpenIncident1()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId1 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id 1.");
                    addPara.ShowDialog();
                    incidentId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                string conditions = "Number=" + incidentId1 + "|State=New|Priority=1 - Critical";
                flag = prb.RelatedTableOpenRecord("Incidents", conditions, "Number");
                if (!flag)
                {
                    error = "Cannot search and open record with condition (" + conditions + ")";
                }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_01_Change_State_To_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_02_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag)
                    error = "Cannot save incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_03_PopulateState_AdditionComment()
        {
            try
            {
                flag = prb.Add_AdditionComments("Auto comments");
                if (!flag)
                {
                    error = "Error when add comments.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }  

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_04_Change_State_To_Resolved()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Resolved");
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_05_PopulateCloseCodeAndCloseNotes()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");

                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("CloseCode");
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("CloseNote");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_06_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag)
                    error = "Cannot save incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_PopulateState_Closed()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        Thread.Sleep(1000);
                    }
                    else { error = "Cannot populate (" + temp + ") value."; }
                }
                else
                {
                    error = "Cannot get Urgency combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Click_UpdateButton()
        {
            try
            {
                flag = prb.Update();
                if (!flag)
                {
                    error = "Cannot click Update button.";
                }
                else prolist.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_SearchAndOpenProblem()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && problemId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                temp = "Number=" + problemId;
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prolist.WaitLoading();
                    flag = prolist.SearchAndOpen("Number", problemId, temp, "Number");
                    if (!flag) error = "Error when search and open problem (Id:" + problemId + ")";
                    else
                    {
                        prb.WaitLoading();
                    }
                }
                else { error = "Cannot open Problem list"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Verify_RelateP1P2_Checked()
        {
            try
            {
                checkbox = prb.Checkbox_RelatedP1P2_Incident();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Checkbox is not checked. Expected: Checked";
                    }
                }
                else 
                {
                    flagExit = false;
                    error = "Cannot get Related P1 P2 checkbox."; 
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_ClickEditbutton()
        {
            try
            {
                flag = prb.Select_Tab("Incidents");

                if (flag)
                {
                    button = prb.Button_Edit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                    }

                    else
                    {
                        error = "Cannot click Edit button";
                    }
                }
                else { error = "Cannot select Incidents tab"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_RemoveMember_Incident1()
        {
            try
            {
               string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId1 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id 1.");
                    addPara.ShowDialog();
                    incidentId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                flag = member.Delete_Members(incidentId1);
                if (!flag)
                {
                    error = "Cannot remove incident Id (" + incidentId1 + ").";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Verify_RelateP1P2_UnChecked()
        {
            try
            {
                checkbox = prb.Checkbox_RelatedP1P2_Incident();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = true;
                        System.Console.WriteLine("***PASSED: Checkbox is unchecked as expected.");
                    }
                    else
                    {
                        flag = false;
                        flagExit = false;
                        error = "Checkbox is checked. Expected: Not checked.";
                    }
                }
                else { flagExit = false; error = "Cannot get Related P1 P2 checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_ClickEditbutton()
        {
            try
            {
                flag = prb.Select_Tab("Incidents");

                if (flag)
                {
                    button = prb.Button_Edit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                    }

                    else
                    {
                        error = "Cannot click Edit button";
                    }
                }
                else { error = "Cannot select Incidents tab"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_AddMember_Incident2()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId2 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id 2.");
                    addPara.ShowDialog();
                    incidentId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                flag = member.Add_Members(incidentId2);
                if (!flag)
                {
                    error = "Cannot add incident Id (" + incidentId2 + ").";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
      
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Verify_RelateP1P2_Checked()
        {
            try
            {
                checkbox = prb.Checkbox_RelatedP1P2_Incident();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Checkbox is not checked. Expected: Checked";
                    }
                }
                else { flagExit = false; error = "Cannot get Related P1 P2 checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_00_ClickEditbutton()
        {
            try
            {
                flag = prb.Select_Tab("Incidents");

                if (flag)
                {
                    button = prb.Button_Edit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                    }

                    else
                    {
                        error = "Cannot click Edit button";
                    }
                }
                else { error = "Cannot select Incidents tab"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_RemoveMember_Incident2()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId2 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id 2.");
                    addPara.ShowDialog();
                    incidentId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------
                flag = member.Delete_Members(incidentId2);
                if (!flag)
                {
                    error = "Cannot remove incident Id (" + incidentId1 + ").";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Verify_RelateP1P2_UnChecked()
        {
            try
            {
                checkbox = prb.Checkbox_RelatedP1P2_Incident();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = true;
                        System.Console.WriteLine("***PASSED: Checkbox is unchecked as expected.");
                    }
                    else
                    {
                        flag = false;
                        flagExit = false;
                        error = "Checkbox is checked. Expected: Not checked.";
                    }
                }
                else { flagExit = false; error = "Cannot get Related P1 P2 checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot Save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_Update_State_ProlemS()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed/Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    { error = "Cannot select state <" + temp + ">"; }
                }
                else
                { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_PopulateCloseNote()
        {
            try
            {
                string temp = Base.GData("ProCloseNote");
                textarea = prb.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_PopulateCloseCode()
        {
            try
            {
                string temp = Base.GData("ProCauseCode");
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Close Code value."; }
                }
                else
                    error = "Cannot get combobox Close Code.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_UpdateProblem()
        {
            try
            {
                flag = prb.Update();
                if (!flag)
                {
                    error = "Problem save unsuccessfully.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_29_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }
}




    

