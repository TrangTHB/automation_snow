﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Problem
{
    [TestFixture]
    public class Problem_Flag_for_Knowledge_8
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Finished - Problem Id: " + problemId);
            System.Console.WriteLine("Finished - Knowledge Submission Id: " + kbSubmissionId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************



        string problemId, kbSubmissionId;

        Login login;
        Home home;
        
        SNow.Problem prb;
        SNow.ProblemList prolist;
     
      
       

        //-----------------------------
      
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snocheckbox checkbox = null;
       
       //**********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                prb = new SNow.Problem(Base, "Problem");
                prolist = new SNow.ProblemList(Base, "Problem list");
               
             
                //-----------------------------------------------------

                problemId = string.Empty;
               
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /* Impersonate problem manager*/
        public void Step_003_ImpersonateUser_ProblemManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Select Problem >> Create New on left navigatior*/
        public void Step_005_Open_NewProlem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    prb.WaitLoading();
                    if (!flag) error = "Error When create new problem";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Populate company*/
        public void Step_006_Populate_Company()
        {
            try
            {
                /*Get Problem ID*/
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    problemId = textbox.Text;
                }
                else { error = "Cannot get textbox Number."; }
                /*Populate company*/
                string temp = Base.GData("Company");
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) error = "Can not populate company";
                }
                else error = "Can not get lookup company";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Populate the problem statement*/
        public void Step_007_001_Populate_ProblemImpact()
        {
            try
            {
                string temp = Base.GData("ProblemImpact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Can not populate problem impact";
                }
                else error = "Can not get combobox Problem impact";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Populate the problem statement*/
        public void Step_007_002_Populate_ProblemStatement()
        {
            try
            {
                string temp = Base.GData("ProblemStatement");
                textbox = prb.Textbox_ProblemStatement();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) error = "Can not populate problem statement";
                }
                else error = "Can not get textbox Problem statement";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Populate Assignment group*/
        public void Step_008_Populate_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) error = "Can not populate Assignment Group";
                }
                else error = "Can not get lookup Assignment Group";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Assign ticket to me (problem managerr)*/
        public void Step_009_01_Populate_AssignedTo()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) error = "Can not populate Assigned to";
                }
                else error = "can not get lookup Assigned to";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_02_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Save Problem*/
        public void Step_010_Save_Problem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) error = "Can not save problem";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Resolve ticket*/
        public void Step_011_ChangeState()
        {
            try
            {
                string temp = Base.GData("State");
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Can not select" + temp + "option";
                }
                else error = "Can not get combobox State";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Populate Close code*/
        public void Step_012_Populate_CauseCode()
        {
            try
            {
                string temp = Base.GData("CauseCode");
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Can not populate Close code";
                }
                else error = "Can not get combobox Close Code";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Populate Close notes*/
        public void Step_013_Populate_CloseNotes()
        {
            try
            {
                string temp = Base.GData("CloseNotes");
                textarea = prb.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) error = "Can not populate Close Notes";
                }
                else error = "can not get textarea close notes";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /* Check the Create Knowledge checkbox */
        public void Step_014_Check_CreateKnowledge()
        {
            try
            {
                checkbox = prb.Checkbox_Create_Knowledge();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click(true);
                        if (!flag) { error = "Cannot click Checkbox Create Knowledge."; }
                    }
                    else { error = "Create knowledge checkbox is already checked."; }
                }
                else error = "Cannot get checkbox Create knowledge";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Click Update button*/
        public void Step_015_UpdateProblem()
        {
            try
            {
                flag = prb.Update();
                if (!flag) error = "Cannot update problem";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify The Knowledge Submission # is generated immediately*/
        public void Step_016_Verify_KnowledgeSubmission()
        {
            try
            {
                string temp = Base.GData("MessageInfo");
                flag = prb.VerifyMessageInfo(temp);
                if (!flag) error = "Not found message (" + temp + ")";
                kbSubmissionId = prb.GetMessageInfo_TicketNumber();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_17_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }
}




    

