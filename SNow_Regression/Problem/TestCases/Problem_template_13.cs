﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Problem
{
    [TestFixture]
    public class Problem_template_13
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Problem Id: " + problemId);
            System.Console.WriteLine("Finished - Problem Task 1 Id: " + pTaskId1);
            System.Console.WriteLine("Finished - Problem Task 2 Id: " + pTaskId2);



            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************




        string problemId, pTaskId1, pTaskId2;

        Login login;
        Home home;
        SNow.Problem prb;
        SNow.ProblemList prolist;
        SNow.Email email;
        SNow.EmailList emailList;
       
          

        //-----------------------------
        
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snodatetime datetime = null;
        snoelement ele = null;
        snocheckbox checkbox = null;
       
       //**********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)


       
        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                prb = new SNow.Problem(Base, "Problem");
                prolist = new SNow.ProblemList(Base, "Problem list");
                email = new SNow.Email(Base, "Email");
                emailList = new SNow.EmailList(Base, "Email List");
               

                //-----------------------------------------------------

                problemId = string.Empty;
                pTaskId1 = string.Empty;
                pTaskId2 = string.Empty;
            
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

       

    
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_01_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_02_ImpersonateUser_ProblemManger()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_03_SystemSetting()
        {
            try
            {
                string domain = Base.GData("FullPathDomain");
                flag = home.SystemSetting(domain);
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                    prolist.WaitLoading();
                else
                    error = "Error when create new Problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_00_ApplyTemplate()
        {
            try
            {
                string problemTemplate = Base.GData("ProblemTemplate");
                string templateMenu = "Templates;Apply Template;" + problemTemplate;
                string template = problemTemplate;

                if (templateMenu.ToLower() != "no")
                {
                    flag = prb.SelectTemplate(templateMenu);
                    if (!flag) error = "Error when select template.";
                    else prb.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "***still not implement this case yet:)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_01_Get_ProblemId()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    problemId = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_02_Verify_Template()
        {
            try
            {
                lookup = prb.Lookup_Template();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProblemTemplate");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid Template value. Runtime: [" + lookup.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Template lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_03_Verify_ProblemStatement()
        {
            try
            {
                textbox = prb.Textbox_ProblemStatement();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProblemStatement");
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid Problem Statement value. Runtime: [" + textbox.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Problem Statement textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_04_Verify_Company()
        {
            try
            {
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid Company value. Runtime: [" + lookup.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Company lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_05_Verify_AssignmentGroup()
        {
            try
            {
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid Assignment Group value. Runtime: [" + lookup.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Assignment Group lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_06_Populate_Problem_Source()
        {
            try
            {
                string temp = "Problem source=Change";
                if (temp.Trim().ToLower() != "no")
                {
                    flag = prb.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_01_Save_Problem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_02_Verify_DueDateMessage()
        {
            try
            {
                string temp = "Please ensure that you add a Due date for each Problem task created from this template.";

                flag = prb.VerifyMessageInfo(temp);
                if (!flag)
                {
                    error = "Invalid Due Date Message. Expected: [" + temp + "]";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_Verify_PTask_Generated()
        {
            try
            {
                //Get Problem Task 1 Id
                string conditions = "Short description=" + Base.GData("ShortDescriptionTask1");
                flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions);
                if (flag)
                {
                    pTaskId1 = prb.RelatedTable_Get_Cell_Text("Problem Tasks", conditions, "Number");

                    //Get Problem Task 2 Id
                    conditions = "Short description=" + Base.GData("ShortDescriptionTask2");
                    flag = prb.Verify_RelatedTable_Row("Problem Tasks", conditions);
                    if (flag)
                    {
                        pTaskId2 = prb.RelatedTable_Get_Cell_Text("Problem Tasks", conditions, "Number");
                    }
                    else
                    {
                        error = "Cannot verify row with conditon: " + conditions;
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot verify row with conditon: " + conditions;
                    flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_Populate_AssignedTo()
        {
            try
            {
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProblemAssignee");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assigned to value.";
                    }
                }
                else { error = "Cannot get Assigned to lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_Populate_Description()
        {
            try
            {
                textarea = prb.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProblemDescription");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Description value.";
                    }
                }
                else { error = "Cannot get Description lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_00_Save_Problem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_01_Verify_Activity_9()
        {
            try
            {
                string temp = Base.GData("Activity9");
                flag = prb.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        /*Work around from Step 010_00 to 013 to add assignee to problem task 2*/
        [Test]
        public void Step_010_00_Open_ProblemTask2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + pTaskId2 + "|Short description=" + Base.GData("ShortDescriptionTask2");
                flag = prb.RelatedTableOpenRecord("Problem Tasks", condition, "Number");
                if (!flag)
                {
                    error = "Cannot open Problem Task with conditon: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_01_Populate_AssignedTo()
        {
            try
            {
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("TaskResolver1");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assigned to value.";
                    }
                }
                else { error = "Cannot get Assigned to lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_02_Populate_Duedate()
        {
            try
            {
                datetime = prb.Datetime_DueDate();
                flag = datetime.Existed;
                if (flag)
                {
                    string temp = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(temp, true);
                    if (!flag)
                    {
                        error = "Cannot populate Duedate value.";
                    }
                }
                else { error = "Cannot get Due date datetime."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_03_Save_ProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_ImpersonateUser_TaskResolver()
        {
            try
            {
                string temp = Base.GData("TaskResolver1");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SystemSetting()
        {
            try
            {
                //string domain = Base.GData("FullPathDomain");
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_00_Open_MyGroupsWork()
        {
            try
            {
                flag = home.LeftMenuItemSelect("My Groups Work", "My Groups Work");
                if (flag)
                    prolist.WaitLoading();
                else
                    error = "Error when open My Groups Work.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_01_Verify_ProblemTask1_ProblemTask2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                string condition = "Assignment group;is;" + Base.GData("ProblemTaskGroup");
                flag = prolist.Filter(condition);
                if (flag)
                {
                    prolist.WaitLoading();
                    //-------------------------------------------------------------------
                    condition = "Number=" + pTaskId1 + "|Short description=" + Base.GData("ShortDescriptionTask1");
                    flag = prolist.SearchAndVerify("Number", pTaskId1, condition);
                    if (flag)
                    {
                        condition = "Number=" + pTaskId2 + "|Short description=" + Base.GData("ShortDescriptionTask2");
                        flag = prolist.SearchAndVerify("Number", pTaskId2, condition);
                        if (!flag)
                        {
                            error = "Error when search and verify row with conditon: " + condition;
                            flagExit = false;
                        }
                    }
                    else
                    {
                        error = "Error when search and verify row with conditon: " + condition;
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot filter with conditon: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Open_MyWork()
        {
            try
            {
                flag = home.LeftMenuItemSelect("My Work", "My Work");
                if (flag)
                    prolist.WaitLoading();
                else
                    error = "Error when open My Work.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_SearchAndOpen_ProblemTask2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + pTaskId2 + "|Short description=" + Base.GData("ShortDescriptionTask2");
                flag = prolist.SearchAndOpen("Number", pTaskId2, condition, "Number");
                if (!flag)
                {
                    error = "Error when search and open row with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_PopulateCI()
        {
            try
            {
                lookup = prb.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ConfigurationItem");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate CI.";
                    }
                }
                else { error = "Cannot get Configuration Item lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_Populate_WorkNotes()
        {
            try
            {
                string temp = Base.GData("WorkNotes");
                snotextarea textarea = prb.Textarea_Problem_Worknotes();
                flag = prb.Add_Worknotes(temp, true, textarea);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
            
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_Reassign_to_TaskResolver2()
        {
            try
            {
                lookup = prb.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("TaskResolver2");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assigned to value.";
                    }
                }
                else { error = "Cannot get Assigned to lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_Save_ProblemTask2()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_Verify_Reassignment_Activity()
        {
            try
            {

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_ImpersonateUser_TaskResolver2()
        {
            try
            {
                string temp = Base.GData("TaskResolver2");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_Open_MyWork()
        {
            try
            {
                flag = home.LeftMenuItemSelect("My Work", "My Work");
                if (flag)
                    prolist.WaitLoading();
                else
                    error = "Error when open My Work.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_SearchAndOpen_ProblemTask2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + pTaskId2 + "|Short description=" + Base.GData("ShortDescriptionTask2");
                flag = prolist.SearchAndOpen("Number", pTaskId2, condition, "Number");
                if (!flag)
                {
                    error = "Error when search and open row with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_Populate_State()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Work in Progress";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_Save_ProblemTask2()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem Task."; }
                else
                {
                    prb.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_Verify_State_WorkinProgress()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Work in Progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid State value. Runtime: [" + combobox.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_Populate_State()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_Save_ProblemTask2()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem Task."; }
                else
                {
                    prb.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_Verify_State_ClosedComplete()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid State value. Runtime: [" + combobox.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_Populate_WorkNotes()
        {
            try
            {
                string temp = Base.GData("WorkNotes");
                snotextarea textarea = prb.Textarea_Problem_Worknotes();
                flag = prb.Add_Worknotes(temp, true, textarea);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_Save_ProblemTask2()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem Task."; }
                else
                {
                    prb.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_Open_MyGroupsWork()
        {
            try
            {
                flag = home.LeftMenuItemSelect("My Groups Work", "My Groups Work");
                if (flag)
                    prolist.WaitLoading();
                else
                    error = "Error when open My Groups Work.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_SearchAndOpen_ProblemTask1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + pTaskId1 + "|Short description=" + Base.GData("ShortDescriptionTask1");
                flag = prolist.SearchAndOpen("Number", pTaskId1, condition, "Number");
                if (!flag)
                {
                    error = "Error when search and open row with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_Populate_Duedate()
        {
            try
            {
                datetime = prb.Datetime_DueDate();
                flag = datetime.Existed;
                if (flag)
                {
                    string temp = DateTime.Today.AddDays(2).ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(temp, true);
                    if (!flag)
                    {
                        error = "Cannot populate Duedate value.";
                    }
                }
                else { error = "Cannot get Due date datetime."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_Populate_State()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Skipped";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_Populate_WorkNotes()
        {
            try
            {
                string temp = Base.GData("WorkNotes");
                snotextarea textarea = prb.Textarea_Problem_Worknotes();
                flag = prb.Add_Worknotes(temp, true, textarea);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
            
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_00_Save_ProblemTask()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem Task."; }
                else
                {
                    prb.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_01_Verify_State_ClosedSkipped()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Skipped";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid State value. Runtime: [" + combobox.Text + "]. Expected: [" + temp + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_ImpersonateUser_ProblemAssignee()
        {
            try
            {
                string temp = Base.GData("ProblemAssignee");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_GlobalSearch_Problem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = prb.GlobalSearchItem(problemId, true);
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_Click_ViewRCA_RelatedLink()
        {
            try
            {
                ele = prb.GRelatedLink("View RCA");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (!flag)
                    {
                        error = "Cannot click View RCA link.";
                    }
                    else { prb.WaitLoading(); }
                }
                else { error = "Cannot get View RCA link."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_Click_ViewProblem_RelatedLink()
        {
            try
            {
                ele = prb.GRelatedLink("View Problem");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (!flag)
                    {
                        error = "Cannot click View Problem link.";
                    }
                    else { prb.WaitLoading(); }
                }
                else { error = "Cannot get View Problem link."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_Check_RCADeliverd_Checkbox()
        {
            try
            {
                checkbox = prb.Checkbox_RCADelivered();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Click();
                    if (!flag) { error = "Cannot clicl RCA Deliverd checkbox."; }
                }
                else { error = "Cannot get RCA Deliverd checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_Save_Problem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem."; }
                else
                {
                    prb.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_Populate_State_ClosedResolved()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed/Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_Populate_CloseCode_CloseNote()
        {
            try
            {
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("CauseCode");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        textarea = prb.Textarea_CloseNotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            temp = Base.GData("CloseNotes");
                            flag = textarea.SetText(temp);
                            if (!flag)
                            {
                                error = "Cannot populate Close Notes value.";
                            }
                        }
                        else { error = "Cannot get Close Notes textarea."; }
                    }
                    else { error = "Cannot populate Closed Code value."; }
                }
                else { error = "Cannot get Close Code combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_Save_Problem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Cannot save Problem."; }
                else
                {
                    prb.WaitLoading();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_VerifyGlobalOLA()
        {
            try
            {
                string temp = Base.GData("SLADefinition");
                string condition = "SLA=" + temp;
                flag = prb.Verify_RelatedTable_Row("Task SLAs", condition);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found Task SLA response: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_ImpersonateUser_TestUser()
        {
            try
            {
                string temp = Base.GData("TestUser");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_00_Verify_Email_Problem_Assigned_ProblemAssignee()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string email = Base.GData("ProAssigneeEmail");
                temp = "Subject;contains;" + problemId + "|and|Subject;contains;has been assigned to you" + "|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + problemId + "|Recipients=" + Base.GData("ProAssigneeEmail");
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to Problem Assignee.";
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_01_Verify_Subject_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = "Problem " + problemId + " has been assigned to you";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_02_Verify_Recipient_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("ProAssigneeEmail");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_00_Verify_Open_Email_ProblemTask1_Notification()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string email = Base.GData("TaskResolver1Email");
                temp = "Subject;contains;" + pTaskId1 + "|and|Subject;contains;" + Base.GData("ShortDescriptionTask1") + "|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + pTaskId1 + "|Recipients=@@" + email;
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to Assignment group members.";
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_01_Verify_Subject_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = "Problem Task " + pTaskId1 + " notification -- " + Base.GData("ShortDescriptionTask1");
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_02_Verify_Recipient_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("TaskResolver1Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_055_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_00_Verify_Open_Email_ProblemTask2_Notification()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string email = Base.GData("TaskResolver2Email");
                temp = "Subject;contains;" + pTaskId2 + "|and|Subject;contains;" + Base.GData("ShortDescriptionTask2") + "|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + pTaskId2 + "|Recipients=@@" + Base.GData("TaskResolver2Email");
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to Assignment group members.";
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_01_Verify_Subject_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = "Problem Task " + pTaskId2 + " notification -- " + Base.GData("ShortDescriptionTask2");
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_02_Verify_Recipient_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("TaskResolver2Email");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_057_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_00_Verify_Open_Email_ProblemTask1_Closed()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string email = Base.GData("ProAssigneeEmail");
                temp = "Subject;contains;" + pTaskId1 + "|and|Subject;contains;has been Closed" + "|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + pTaskId1 + "|Recipients=@@" + Base.GData("ProAssigneeEmail");
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to Problem Assignee (Closed).";
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_01_Verify_Subject_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = "Problem task " + pTaskId1 + " has been Closed";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_02_Verify_Recipient_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId1 == null || pTaskId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 1 Id.");
                    addPara.ShowDialog();
                    pTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("ProAssigneeEmail");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_059_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_00_Verify_Open_Email_ProblemTask2_Closed()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string email = Base.GData("ProAssigneeEmail");
                temp = "Subject;contains;" + pTaskId2 + "|and|Subject;contains;has been Closed" + "|and|Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string conditions = "Subject=@@" + pTaskId2 + "|Recipients=@@" + Base.GData("ProAssigneeEmail");
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to Problem Assignee (Closed).";
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_01_Verify_Subject_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = "Problem task " + pTaskId2 + " has been Closed";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_02_Verify_Recipient_Email()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (pTaskId2 == null || pTaskId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Task 2 Id.");
                    addPara.ShowDialog();
                    pTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("ProAssigneeEmail");
                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + temp;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }



        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_611_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }
}




    

