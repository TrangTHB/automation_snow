﻿using NUnit.Framework;
using SNow;
using System;
using System.Reflection;
using System.Threading;
namespace SecurityIncident
{
    [TestFixture]
    public class SIR_Investigation_02
	{
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Security Incident Id: " + SecIncidentId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        //Auto.otextbox textbox;
        //Auto.olookup lookup;
        //Auto.ocombobox combobox;
        //Auto.obutton button;
        //Auto.otextarea textarea;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
		Itil alert = null;
		ItilList alertlist = null;
		Itil sir = null;
        SecurityIncidentList sirlist = null;
        PostIncidentReviewList revlist = null;
        PostIncidentReview rev = null;
        //------------------------------------------------------------------
        string SecIncidentId, AlertId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
				alert = new Itil(Base, "Alert");
				alertlist = new ItilList(Base, "Alert list");
				sir = new Itil(Base, "Security Incident");
                sirlist = new SecurityIncidentList(Base, "Security Incident list");
                revlist = new PostIncidentReviewList(Base, "Post Incident Review list");
                rev = new PostIncidentReview(Base, "Post Incident Review");
                //------------------------------------------------------------------
                SecIncidentId = string.Empty;
				AlertId = string.Empty;
			}
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login_as_Analisyst()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		//[Test]
		//public void Step_003_1_ImpersonateUser_Analyst()
		//{
		//	try
		//	{
		//		string temp = Base.GData("SocAnalyst");
		//		flag = home.ImpersonateUser(temp);
		//		if (!flag) error = "Error when impersonate assignee user (" + temp + ")";
		//		else home.WaitLoading();
		//	}
		//	catch (Exception ex)
		//	{
		//		flag = false;
		//		error = ex.Message;
		//	}
		//}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_004_OpenNewAlert()
        //{
        //    try
        //    {
        //        flag = home.LeftMenuItemSelect("Security Incident", "All Alerts");
        //        if (flag)
        //        {
        //            alertlist.WaitLoading();
        //            button = alertlist.Button_New();
        //            if (button.Existed)
        //            {
        //                flag = button.Click();
        //                if (!flag) error = "Could not click New button";
        //                else
        //                {
        //                    alert.WaitLoading();
        //                    //Store Alert Id
        //                    AlertId = alert.Textbox_Number().Text;
        //                }
        //            }
        //            else error = "Not found New button";
        //        }
        //        else
        //            error = "Error when navigating to Security Incident > Show All Incidents";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_005_Populate_Source()
        //{
        //	try
        //	{
        //		string temp = Base.GData("AlertSource");

        //		textbox = alert.Textbox_Source;
        //		flag = textbox.Existed;
        //		if (flag)
        //		{
        //			flag = textbox.SetText(temp);
        //			if (!flag)
        //			{
        //				error = "Cannot input Source value.";
        //				flagExit = false;
        //			}
        //		}
        //		else { error = "Cannot get Source textbox."; }
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //      public void Step_006_Populate_Node()
        //      {
        //          try
        //          {
        //              string temp = Base.GData("AlertNode");

        //              textbox = alert.Textbox_Node;
        //              flag = textbox.Existed;
        //              if (flag)
        //              {
        //                  flag = textbox.SetText(temp);
        //                  if (!flag)
        //                  {
        //                      error = "Could not input Node value";
        //                      flagExit = false;
        //                  }
        //              }
        //              else
        //                  error = "Cannot get Node textbox.";

        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_Populate_Type()
        //{
        //	try
        //	{
        //		string temp = Base.GData("AlertType");

        //		lookup = alert.Lookup_Type;
        //		flag = lookup.Existed;
        //		if (flag)
        //		{
        //			flag = lookup.Select(temp);
        //			if (!flag)
        //			{
        //				error = "Could not select Type";
        //				flagExit = false;
        //			}
        //		}
        //		else
        //			error = "Cannot get Type lookup";

        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //      public void Step_008_PopulateResource()
        //      {
        //          try
        //          {
        //              string temp = Base.GData("AlertResource");
        //              textbox = alert.Textbox_Resource;
        //              flag = textbox.Existed;
        //              if (flag)
        //              {
        //                  flag = textbox.SetText(temp);
        //                  if (!flag) { error = "Cannot input Resource value."; }
        //              }
        //              else { error = "Cannot get resource textbox"; }
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_009_PopulateConfigurationItem()
        //      {
        //          try
        //          {
        //              string temp = Base.GData("ConfigurationItem");
        //              lookup = alert.Lookup_ConfigurationItem;
        //              flag = lookup.Existed;
        //              if (flag)
        //              {
        //                  flag = lookup.Select(temp);
        //                  if (!flag) { error = "Cannot select CI"; }
        //              }
        //              else
        //                  error = "Cannot get CI lookup";
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_010_PopulateSeverity()
        //{
        //	try
        //	{
        //		string temp = Base.GData("AlertSeverity");
        //		combobox = alert.Combobox_Severity;
        //		flag = combobox.Existed;
        //		if (flag)
        //		{
        //			flag = combobox.SelectItem(temp);
        //			if (!flag) { error = "Cannot select Severity"; }
        //		}
        //		else
        //			error = "Cannot get Severity lookup";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_011_PopulateDescription()
        //{
        //	try
        //	{
        //		string temp = Base.GData("AlertDescription");
        //		textarea = alert.Textarea_Description;
        //		flag = textarea.Existed;
        //		if (flag)
        //		{
        //			flag = textarea.SetText(temp);
        //			if (!flag) { error = "Cannot set Description textarea"; }
        //		}
        //		else
        //			error = "Cannot get Description textarea";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //      public void Step_012_SaveAlert()
        //      {
        //          try
        //          {
        //              flag = alert.Save();
        //              if (!flag) { error = "Error when save alert."; }
        //              else { alert.WaitLoading(); }
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_013_CreateSecurityIncidentFromAlert()
        //      {
        //          try
        //          {
        //		button = alert.Button_CreateSecurityIncident;
        //		if (button.Existed)
        //		{
        //			flag = button.Click();
        //			if (flag)
        //			{
        //				sir.WaitLoading();
        //				//Capture the security incident number
        //				SecIncidentId = sir.Textbox_Number.Text;
        //			}
        //			else error = "Cannot click Create Security Incident button";
        //		}
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_PopulateShortDescription()
        //{
        //	try
        //	{
        //		string temp = Base.GData("SIRShortDescription");
        //		textbox = sir.Textbox_ShortDescription;
        //		flag = textbox.Existed;
        //		if (flag)
        //		{
        //			flag = textbox.SetText(temp);
        //			if (!flag) { error = "Cannot populate short description value."; }
        //		}
        //		else { error = "Cannot get textbox short description."; }
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_015_SaveSIR()
        //{
        //	try
        //	{
        //		flag = sir.Save();
        //		if (!flag) { error = "Error when saving SIR"; }
        //		else { sir.WaitLoading(); }
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //      public void Step_016_Update_StateToRecover()
        //      {
        //          try
        //          {
        //              combobox = sir.Combobox_State;
        //              flag = combobox.Existed;
        //              if (flag)
        //              {
        //                  string temp = Base.GData("SIRRecoverState");
        //                  flag = combobox.SelectItem(temp);
        //                  if (!flag)
        //                      error = "Error when select State.";
        //              }
        //              else error = "Cannot get combobox State.";
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_017_SaveSIR()
        //      {
        //          try
        //          {
        //              flag = sir.Save();
        //              if (!flag) { error = "Error when saving SIR"; }
        //              else { sir.WaitLoading(); }
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_018_Update_StateToClosed()
        //      {
        //          try
        //          {
        //              combobox = sir.Combobox_State;
        //              flag = combobox.Existed;
        //              if (flag)
        //              {
        //                  string temp = Base.GData("SIRClosedState");
        //                  flag = combobox.SelectItem(temp);
        //                  if (!flag)
        //                      error = "Error when select State.";
        //              }
        //              else error = "Cannot get combobox State.";
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_019_SaveSIR()
        //      {
        //          try
        //          {
        //              button = sir.Button_Update;
        //              if (button.Existed)
        //              {
        //                  button.Click();
        //                  sir.WaitLoading();
        //              }
        //              else error = "Not found Update button";
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_020_VerifyErrorMessage()
        //      {
        //          try
        //          {
        //              string temp = Base.GData("MandatoryErrorMessage");
        //              flag = sir.VerifyErrorMessage(temp);
        //              if (!flag) { error = "No error message displays"; }
        //              else { sir.WaitLoading(); }
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_021_Populate_CloseCodeAndCloseNotes()
        //      {
        //          try
        //          {
        //              string tempclosecode = Base.GData("SIRCloseCode");
        //              string tempclosenotes = Base.GData("SIRCloseNotes");

        //              tab = sir.GTab("Closure Information");
        //              //---------------------------------------
        //              int i = 0;
        //              while (tab == null && i < 5)
        //              {
        //                  Thread.Sleep(2000);
        //                  tab = sir.GTab("Closure Information", true);
        //                  i++;
        //              }
        //              flag = tab.Header.Click(true);
        //              if (flag)
        //              {

        //                  combobox = sir.Combobox_CloseCode;
        //                  flag = combobox.Existed;
        //                  if (flag)
        //                  {
        //                      flag = combobox.SelectItem(tempclosecode);
        //                      if (flag)
        //                      {
        //                          textarea = sir.Textarea_Closenotes;
        //                          flag = textarea.Existed;
        //                          if (flag)
        //                          {
        //                              flag = textarea.SetText(tempclosenotes);
        //                              if (!flag) error = "Could not input close notes.";
        //                          }
        //                          else error = "Cannot get textarea close notes.";
        //                      }
        //                      else error = "Could not select close code.";
        //                  }
        //                  else error = "Cannot get combobox close code.";
        //              }
        //              else error = "Cannot select tab Closure Information.";
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //      //-----------------------------------------------------------------------------------------------------------------------------------
        //      [Test]
        //      public void Step_022_SaveSIR()
        //      {
        //          try
        //          {
        //              flag = sir.Save();
        //              if (!flag) { error = "Error when saving SIR"; }
        //              else { sir.WaitLoading(); }
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_023_VerifyInfoMessage()
        ////{
        ////	try
        ////	{
        ////		string temp = Base.GData("AssessmentRequiredMessage");
        ////		flag = sir.VerifyMessageInfo(temp);
        ////		if (!flag)
        ////		{
        ////			error = "No info message displays";
        ////			flagExit = false;
        ////		}
        ////		else { sir.WaitLoading(); }
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_024_Verify_StateIsChangedToReview()
        ////{
        ////	try
        ////	{
        ////		combobox = sir.Combobox_State;
        ////		flag = combobox.Existed;
        ////		if (flag)
        ////		{
        ////			string temp = Base.GData("SIRReviewState");
        ////			flag = combobox.VerifyCurrentText(temp);
        ////			if (!flag)
        ////				error = "State has not changed to Review.";
        ////		}
        ////		else error = "Cannot get combobox State.";
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_025_OpenAssessmentForm()
        ////{
        ////	try
        ////	{
        ////		//-- Input information
        ////		string temp = Base.GData("Debug").ToLower();
        ////		if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
        ////		{
        ////			Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
        ////			addPara.ShowDialog();
        ////			SecIncidentId = addPara.value;
        ////			addPara.Close();
        ////			addPara = null;
        ////		}
        ////		//-----------------------------------------------------------------------
        ////		flag = home.LeftMenuItemSelect("Security Incident", "My Pending Reviews");
        ////		if (flag)
        ////		{
        ////			revlist.WaitLoading();
        ////			button = revlist.Button_TakeAssessment(SecIncidentId);
        ////			if (button.Existed)
        ////			{
        ////				button.Click();
        ////				rev.WaitLoading();
        ////			}
        ////		}
        ////		else error = "Error when opening assesment for " + SecIncidentId;
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_026_FulfillAssessment()
        ////{
        ////	try
        ////	{
        ////		flag = rev.InputAllAssessmentFields("Test Automation", "Yes");
        ////		if (!flag)
        ////		{
        ////			error = "Have error when auto fill assessment fields.";
        ////		}

        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_027_SubmitAssessment()
        ////{
        ////	try
        ////	{
        ////		button = rev.Button_SubmitAssessment;
        ////		if (button.Existed)
        ////		{
        ////			button.Click();
        ////			revlist.WaitLoading();
        ////		}
        ////		else error = "Not found Submit button";
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_028_SearchAndOpenSecurityIncident()
        ////{
        ////	try
        ////	{
        ////		//-- Input information
        ////		string temp = Base.GData("Debug").ToLower();
        ////		if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
        ////		{
        ////			Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
        ////			addPara.ShowDialog();
        ////			SecIncidentId = addPara.value;
        ////			addPara.Close();
        ////			addPara = null;
        ////		}
        ////		//-----------------------------------------------------------------------
        ////		flag = home.LeftMenuItemSelect("Security Incident", "Assigned To Me");
        ////		if (flag)
        ////		{
        ////			sirlist.WaitLoading();
        ////			temp = sirlist.Label_Title.Text;
        ////			flag = temp.Equals("Security Incidents");
        ////			if (flag)
        ////			{
        ////				flag = sirlist.SearchAndOpen("Number", SecIncidentId, "Number=" + SecIncidentId, "Number");
        ////				if (!flag) error = "Error when search and open security incident (id:" + SecIncidentId + ")";
        ////				else sir.WaitLoading();
        ////			}
        ////			else
        ////			{
        ////				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Security Incidents)";
        ////			}
        ////		}
        ////		else error = "Error when select open security incident.";
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_029_UpdateIncidentStateToClosed()
        ////{
        ////	try
        ////	{
        ////		string temp = Base.GData("SIRClosedState");
        ////		combobox = sir.Combobox_State;
        ////		if (combobox.Existed)
        ////		{
        ////			combobox.SelectItem(temp);
        ////			sir.WaitLoading();
        ////		}
        ////		else
        ////		{
        ////			flag = false;
        ////			error = "Not found State combobox";
        ////		}
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}

        //////-----------------------------------------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_030_SaveSIR()
        ////{
        ////	try
        ////	{
        ////		flag = sir.Save();
        ////		if (!flag) { error = "Error when saving SIR"; }
        ////		else { sir.WaitLoading(); }
        ////	}
        ////	catch (Exception ex)
        ////	{
        ////		flag = false;
        ////		error = ex.Message;
        ////	}
        ////}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //      public void Step_031_Verify_StateIsChangedToClosed()
        //      {
        //          try
        //          {
        //              combobox = sir.Combobox_State;
        //              flag = combobox.Existed;
        //              if (flag)
        //              {
        //                  string temp = Base.GData("SIRClosedState");
        //                  flag = combobox.VerifyCurrentText(temp);
        //                  if (!flag)
        //                      error = "State has not changed to Closed.";
        //              }
        //              else error = "Cannot get combobox State.";
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_032_SearchAndOpenAlert()
        //{
        //	try
        //	{
        //		//-- Input information
        //		string temp = Base.GData("Debug").ToLower();
        //		if (temp == "yes" && (AlertId == null || AlertId == string.Empty))
        //		{
        //			Auto.AddParameter addPara = new Auto.AddParameter("Please input Alert Id.");
        //			addPara.ShowDialog();
        //			SecIncidentId = addPara.value;
        //			addPara.Close();
        //			addPara = null;
        //		}
        //		//-----------------------------------------------------------------------
        //		flag = home.LeftMenuItemSelect("Security Incident", "All Alerts");
        //		if (flag)
        //		{
        //			alertlist.WaitLoading();
        //			temp = alertlist.Label_Title.Text;
        //			flag = temp.Equals("Alerts");
        //			if (flag)
        //			{
        //				flag = alertlist.SearchAndOpen("Number", AlertId, "Number=" + AlertId, "Number");
        //				if (!flag) error = "Error when search and open Alert (id:" + AlertId + ")";
        //				else alert.WaitLoading();
        //			}
        //			else
        //			{
        //				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Alerts)";
        //			}
        //		}
        //		else error = "Error when select & open alert";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_033_Verify_AlertStateIsChangedToClosed()
        //{
        //	try
        //	{
        //		combobox = alert.Combobox_State;
        //		flag = combobox.Existed;
        //		if (flag)
        //		{
        //			string temp = Base.GData("AlertClosedState");
        //			flag = combobox.VerifyCurrentText(temp);
        //			if (!flag)
        //				error = "State has not changed to Closed.";
        //		}
        //		else error = "Cannot get combobox State.";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //      public void Step_034_Logout()
        //      {
        //          try
        //          {
        //              flag = home.Logout();
        //              if (!flag)
        //              {
        //                  error = "Error when logout system.";
        //              }
        //              else
        //                  login.WaitLoading();
        //          }
        //          catch (Exception ex)
        //          {
        //              flag = false;
        //              error = ex.Message;
        //          }
        //      }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
