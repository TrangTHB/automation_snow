﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace SecurityIncident
{
    [TestFixture]
    public class SIR_e2e_01
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Security Incident Id: " + SecIncidentId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snobutton button = null;
        snotextarea textarea = null;
        snoelement ele = null;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Itil sir = null;
        SecurityIncidentList sirlist = null;
        PostIncidentReviewList revlist = null;
        PostIncidentReview rev = null;
        EmailList emailList;
        Email email;
        //------------------------------------------------------------------
        string SecIncidentId;
        string assignmentGroupCreated;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************
        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                sir = new Itil(Base, "Security Incident");
                sirlist = new SecurityIncidentList(Base, "Security Incident list");
                revlist = new PostIncidentReviewList(Base, "Post Incident Review list");
                rev = new PostIncidentReview(Base, "Post Incident Review");
                emailList = new EmailList(Base, "Email List");
                email = new Email(Base, "Email");
                //-----------------------------------------------------
                SecIncidentId = string.Empty;
                assignmentGroupCreated = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_OpenNewSecurityIncidentForm()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Security Incident", "Show All Incidents");
                if (flag)
                {
                    sirlist.WaitLoading();
                    button = sirlist.Button_New();
                    if (button.Existed)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Could not click New button";
                        }
                        else
                        {
                            sir.WaitLoading();
                            SecIncidentId = sir.Textbox_Number().Text;
                        }
                    }
                    else
                    {
                        error = "Not found New button";
                    }
                }
                else
                    error = "Error when navigating to Security Incident > Show All Incidents";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Populate_Company()
        {
            try
            {
                string temp = Base.GData("Company");

                lookup = sir.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Invalid company value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category");

                combobox = sir.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Could not select category";
                        flagExit = false;
                    }
                    else sir.WaitLoading();
                }
                else
                    error = "Cannot get Category combobox.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Populate_Subcategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");

                combobox = sir.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Could not select category";
                        flagExit = false;
                    }
                }
                else
                    error = "Cannot get Category combobox.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = sir.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SaveSIR()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else 
                { 
                    sir.WaitLoading();
                    assignmentGroupCreated = sir.Lookup_AssignmentGroup().Text;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Verify_State_Draft()
        {
            try
            {
                lookup = sir.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Text.Equals("");
                    if (flag)
                    {
                        combobox = sir.Combobox_State();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.VerifyCurrentValue("Draft", true);
                            if (!flag) { error = "Invalid state value."; flagExit = false; }
                            
                        }
                        else { error = "State is not Draft."; }
                    }
                    else { error = "The 'Assigned To' is not empty."; }
                }
                else { error = "Cannot get lookup Assigned To."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_Click_ShowAllRelatedLists()
        {
            try
            {
                ele = sir.GRelatedLink("Show All Related Lists");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (!flag)
                    {
                        error = "Cannot click Show All Related Lists.";
                    }
                    else { sir.WaitLoading(); }
                }
                else { error = "Cannot get Show All Related Lists."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Verify_Task_SLAs()
        {
            try
            {
                string temp = Base.GData("SLA");
                string condition = "SLA=" + temp + "|Stage=In progress";
                flag = sir.Verify_RelatedTable_Row("Task SLAs", condition);
                if (!flag)
                {
                    error = "Cannot verify Task SLAs with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_01_Update_SubStateToPendingCustomer()
        {
            try
            {
                combobox = sir.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("SubState01");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select Substate.";
                }
                else error = "Cannot get combobox Substate.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_02_Save_SecurityIncident()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_03_Verify_Task_SLAs()
        {
            try
            {
                string temp = Base.GData("SLA");
                string condition = "SLA=" + temp + "|Stage=Paused";
                flag = sir.Verify_RelatedTable_Row("Task SLAs", condition);
                if (!flag)
                {
                    error = "Cannot verify Task SLAs with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_Update_SubStateToPendingProblem()
        {
            try
            {
                combobox = sir.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("SubState02");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select Substate.";
                }
                else error = "Cannot get combobox Substate.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_02_Save_SecurityIncident()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_03_Verify_Task_SLAs()
        {
            try
            {
                string temp = Base.GData("SLA");
                string condition = "SLA=" + temp + "|Stage=In progress";
                flag = sir.Verify_RelatedTable_Row("Task SLAs", condition);
                if (!flag)
                {
                    error = "Cannot verify Task SLAs with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_01_AssignToAnalystGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = sir.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot select Assignment Group."; }
                }
                else
                    error = "Cannot get Assignment Group.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_02_AssignToAnalyst()
        {
            try
            {
                string temp = Base.GData("AssignedTo");
                lookup = sir.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot select Assigned To."; }
                }
                else
                    error = "Cannot get Assigned To.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_03_Save_SecurityIncident()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_SearchAndOpenSecurityIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Security Incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Security Incident", "Show All Incidents");
                if (flag)
                {
                    sirlist.WaitLoading();
                    temp = sirlist.List_Title().MyText;
                    flag = temp.Equals("Security Incidents");
                    if (flag)
                    {
                        flag = sirlist.SearchAndOpen("Number", SecIncidentId, "Number=" + SecIncidentId, "Number");
                        if (!flag) error = "Error when search and open security incident (id:" + SecIncidentId + ")";
                        else sir.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Security Incidents)";
                    }
                }
                else error = "Error when select open security incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Verify_RiskScore()
        {
            try
            {
                textbox = sir.Textbox_Riskscore();
                flag = textbox.VerifyCurrentValue("52");
                if (!flag)
                {
                    error = "Cannot verify Risk score value. Expected: [52]. Runtime: [" + combobox.Text + "].";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_Verify_StateIsChangedToAnalysis()
        {
            try
            {
                string temp = Base.GData("UpdatedState");
                combobox = sir.Combobox_State();
                flag = combobox.VerifyCurrentValue(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current state or Cannot check current state. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_Verify_Task_SLAs()
        {
            try
            {
                string temp = Base.GData("SLA");
                string condition = "SLA=" + temp + "|Stage=Completed";
                flag = sir.Verify_RelatedTable_Row("Task SLAs", condition);
                if (!flag)
                {
                    error = "Cannot verify Task SLAs with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category01");

                combobox = sir.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Could not select category";
                        flagExit = false;
                    }
                    else sir.WaitLoading();
                }
                else
                    error = "Cannot get Category combobox.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Populate_Subcategory()
        {
            try
            {
                string temp = Base.GData("Subcategory01");

                combobox = sir.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Could not select Subcategory";
                        flagExit = false;
                    }
                }
                else
                    error = "Cannot get Subcategory combobox.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_Populate_CI()
        {
            try
            {
                string temp = Base.GData("CI");

                lookup = sir.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Could not select Subcategory";
                        flagExit = false;
                    }
                    else { sir.WaitLoading(); }
                }
                else
                    error = "Cannot get Subcategory combobox.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_SaveSIR()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_RiskScore()
        {
            try
            {
                textbox = sir.Textbox_Riskscore();
                flag = textbox.VerifyCurrentValue("92");
                if (!flag)
                {
                    error = "Cannot verify Risk score value. Expected: [52]. Runtime: [" + combobox.Text + "].";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Logout()
        {
            try
            {
                home.Logout();
                home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }       
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_Login_as_Analyst()
        {
            try
            {
                string user = Base.GData("AnalystUser");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_SearchAndOpenSecurityIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Security Incident", "Assigned To Me");
                if (flag)
                {
                    sirlist.WaitLoading();
                    temp = sirlist.List_Title().MyText;
                    flag = temp.Equals("Security Incidents");
                    if (flag)
                    {
                        flag = sirlist.SearchAndOpen("Number", SecIncidentId, "Number=" + SecIncidentId, "Number");
                        if (!flag) error = "Error when search and open security incident (id:" + SecIncidentId + ")";
                        else sir.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Security Incidents)";
                    }
                }
                else error = "Error when select open security incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Update_StateToRecover()
        {
            try
            {
                combobox = sir.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("RecoverState");
                    Thread.Sleep(2000);
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select State.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_SaveSIR()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Verify_StateIsChangedToRecover()
        {
            try
            {
                combobox = sir.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("RecoverState");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                        error = "State has not changed to Recover.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Update_StateToClosed()
        {
            try
            {
                combobox = sir.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ClosedState");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select State.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_SaveSIR()
        {
            try
            {
                button = sir.Button_Update();
                if (button.Existed)
                {
                    button.Click();
                    sir.WaitLoading();
                }
                else error = "Not found Update button";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_VerifyErrorMessage()
        {
            try
            {
                string temp = Base.GData("MandatoryErrorMessage");
                flag = sir.Verify_ExpectedErrorMessages_Existed(temp);
                if (!flag) { error = "No error message displays"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Update_StateToReview()
        {
            try
            {
                combobox = sir.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ReviewState");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select State.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Add_Request_Assessments()
        {
            try
            {
                flag = sir.Select_Tab("Post Incident Review");
                if (flag)
                {
                    string temp = Base.GData("AssessmentUser");
                    button = sir.Button_UnlockRequestAssessmentList();
                    if (button.Existed)
                    {
                        button.Click();
                        sir.WaitLoading();
                        Thread.Sleep(2000);
                        lookup = sir.Lookup_AddRequestAssessmentName();
                        if (lookup.Existed)
                        {
                            string[] arr = null;
                            if (temp.Contains("|"))
                                arr = temp.Split('|');
                            else
                                arr = new string[] { temp };

                            foreach (string user in arr)
                            {
                                bool flagTemp = true;
                                flagTemp = lookup.SetText(user, true);
                                if (!flagTemp && flag)
                                    flag = false;
                                Thread.Sleep(1000);
                            }

                            if (!flag)
                            {
                                error = "Error when add user to request assessment list";
                            }
                            else
                            {
                                Thread.Sleep(2000);
                                button = sir.Button_LockRequestAssessmentList();
                                if (button.Existed)
                                {
                                    flag = button.Click();
                                    if (flag)
                                        sir.WaitLoading();
                                }
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "Not found Request Assessment lookup";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Not found Unlock Request Assessment button";
                    }
                }
                else error = "Cannot select tab Post Incident Review.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_SaveSIR()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Update_SIR_StateToClosed()
        {
            try
            {
                string temp = Base.GData("ClosedState");
                combobox = sir.Combobox_State();
                if (combobox.Existed)
                {
                    combobox.SelectItem(temp);
                    sir.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Not found State combobox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Populate_CloseCodeAndCloseNotes()
        {
            try
            {
                string tempclosecode = Base.GData("CloseCode");
                string tempclosenotes = Base.GData("CloseNotes");

                flag = sir.Select_Tab("Closure Information");
                if (flag)
                {

                    combobox = sir.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(tempclosecode);
                        if (flag)
                        {
                            textarea = sir.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                flag = textarea.SetText(tempclosenotes);
                                if (!flag) error = "Could not input close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Could not select close code.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_SaveSIR()
        {
            try
            {
                button = sir.Button_Update();
                if (button.Existed)
                {
                    button.Click();
                    sir.WaitLoading();
                }
                else error = "Not found Update button";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_VerifyInfoMessage()
        {
            try
            {
                string temp = Base.GData("AssessmentRequiredMessage");
                flag = sir.VerifyMessageInfo(temp);
                if (!flag) { error = "No info message displays"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_OpenAssessmentForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Security Incident", "My Pending Reviews");
                if (flag)
                {
                    revlist.WaitLoading();
                    button = revlist.Button_TakeAssessment(SecIncidentId);
                    if (button.Existed)
                    {
                        button.Click();
                        rev.WaitLoading();
                    }
                }
                else error = "Error when opening assesment for " + SecIncidentId;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_1stRequestorFulfillAssessment()
        {
            try
            {
                flag = rev.InputAllAssessmentFields("Test Automation", "Yes");
                if (!flag)
                {
                    error = "Have error when auto fill assessment fields.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SubmitAssessment()
        {
            try
            {
                button = rev.Button_SubmitAssessment();
                if (button.Existed)
                {
                    button.Click();
                    revlist.WaitLoading();
                }
                else error = "Not found Submit button";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_ReLogin()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);
                if (flag) home.WaitLoading();
                else error = "Cannot login to system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_03_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_OpenAssessmentForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Security Incident", "My Pending Reviews");
                if (flag)
                {
                    revlist.WaitLoading();
                    button = revlist.Button_TakeAssessment(SecIncidentId);
                    if (button.Existed)
                    {
                        button.Click();
                        rev.WaitLoading();
                    }
                }
                else error = "Error when opening assesment for " + SecIncidentId;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_2ndRequestorFulfillAssessment()
        {
            try
            {
                flag = rev.InputAllAssessmentFields("Test Automation", "Yes");
                if (!flag)
                {
                    error = "Have error when auto fill assessment fields.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_SubmitAssessment()
        {
            try
            {
                button = rev.Button_SubmitAssessment();
                if (button.Existed)
                {
                    button.Click();
                    revlist.WaitLoading();
                }
                else error = "Not found Submit button";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Login_as_Manager()
        {            
            try
            {
                home.Logout();
                home.WaitLoading();
                string user = Base.GData("ManagerUser");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_OpenAssessmentForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Security Incident", "My Pending Reviews");
                if (flag)
                {
                    revlist.WaitLoading();
                    button = revlist.Button_TakeAssessment(SecIncidentId);
                    if (button.Existed)
                    {
                        button.Click();
                        rev.WaitLoading();
                    }
                }
                else error = "Error when opening assesment for " + SecIncidentId;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_3rdRequestorFulfillAssessment()
        {
            try
            {
                flag = rev.InputAllAssessmentFields("Test Automation", "Yes");
                if (!flag)
                {
                    error = "Have error when auto fill assessment fields.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_SubmitAssessment()
        {
            try
            {
                button = rev.Button_SubmitAssessment();
                if (button.Existed)
                {
                    button.Click();
                    revlist.WaitLoading();
                }
                else error = "Not found Submit button";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_SearchAndOpenSecurityIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Security Incident", "Show All Incidents");
                if (flag)
                {
                    sirlist.WaitLoading();
                    temp = sirlist.List_Title().MyText;
                    flag = temp.Equals("Security Incidents");
                    if (flag)
                    {
                        flag = sirlist.SearchAndOpen("Number", SecIncidentId, "Number=" + SecIncidentId, "Number");
                        if (!flag) error = "Error when search and open security incident (id:" + SecIncidentId + ")";
                        else sir.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Security Incidents)";
                    }
                }
                else error = "Error when select open security incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_UpdateIncidentStateToClosed()
        {
            try
            {
                string temp = Base.GData("ClosedState");
                combobox = sir.Combobox_State();
                if (combobox.Existed)
                {
                    Thread.Sleep(2000);
                    combobox.SelectItem(temp);
                    sir.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Not found State combobox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Populate_CloseCodeAndCloseNotes()
        {
            try
            {
                string tempclosecode = Base.GData("CloseCode");
                string tempclosenotes = Base.GData("CloseNotes");

                flag = sir.Select_Tab("Closure Information");
                if (flag)
                {

                    combobox = sir.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(tempclosecode);
                        if (flag)
                        {
                            textarea = sir.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                flag = textarea.SetText(tempclosenotes);
                                if (!flag) error = "Could not input close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Could not select close code.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_SaveSIR()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else { sir.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Verify_StateIsChangedToClosed()
        {
            try
            {
                combobox = sir.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ClosedState");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                        error = "State has not changed to Closed.";
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_043_Verify_ReportIsAttachedToIncidentAfterClosed()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
        //            addPara.ShowDialog();
        //            SecIncidentId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        string fileName = SecIncidentId + " post incident report.pdf";
        //        flag = sir.VerifyAttachmentFile(fileName);
        //        if (!flag) error = "No report was generated and attached to the incident";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_Login_as_TestUser()
        {
            try
            {
                string user = Base.GData("Test User");
                string pwd = Base.GData("TU_Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Impersonate_SupportStaff()
        {
            try
            {
                string temp = Base.GData("Support_User");
                flag = home.ImpersonateUser(temp);
                if (!flag)
                {
                    error = " Cannot impersonate user (" + temp + ")";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_01_Filter_Email_For_SIR_Created()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && SecIncidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + SecIncidentId + "|and|Subject;contains;has been created";
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_02_Open_Email_SIR_Created()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && SecIncidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + SecIncidentId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to caller."; flagExit = false; }
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_03_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else sir.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_04_Verify_Email_Content()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && SecIncidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Click here to view the Security Incident: " + SecIncidentId;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_05_Verify_Email_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && assignmentGroupCreated == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Input assignment group when SIR created.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Assignment group: " + assignmentGroupCreated;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_06_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_01_Filter_Email_For_SIR_Updated()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && SecIncidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + SecIncidentId + "|and|Subject;contains;has been updated";
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_02_Open_Email_SIR_Updated()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && SecIncidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + SecIncidentId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to caller."; flagExit = false; }
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_03_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else sir.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_04_Verify_Email_Content()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && SecIncidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
                    addPara.ShowDialog();
                    SecIncidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Click here to view the Security Incident: " + SecIncidentId;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_05_Verify_Email_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                string assignmentGroupUpdate = Base.GData("AssignmentGroup").ToLower();
                if (assignmentGroupUpdate == string.Empty || assignmentGroupUpdate == null)
                {
                    error = "Please input the assignment group to current testing data.";
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Assignment group: " + assignmentGroupUpdate;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_06_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
