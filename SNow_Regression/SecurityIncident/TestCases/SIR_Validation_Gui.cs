﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using SNow;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace SecurityIncident
{
    class SIR_Validation_Gui
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Description:" + Base.GData("TS_Description"));
            System.Console.WriteLine("Known defect:" + Base.GData("KnownDefect"));
            if (!isCI)
                System.Console.WriteLine("Finished - [" + form + "] number: " + id);
            else
            {
                System.Console.WriteLine("Finished - Current date time: " + currentDateTime);
                System.Console.WriteLine("Finished - [" + form + " - " + platform + "] name: " + id);
            }
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Login login = null;
        Home home = null;
        Itil gui = null;
        ItilList list = null;
        SNow.SecurityIncident inc = null;
        //------------------------------------------------------------------
        string form, platform, id, currentDateTime, isCreation;
        bool isCI;
        //ocombobox cb;
        obutton button;
        //oelement ele;
        Dictionary<string, snoelement> listOfControl = null;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                gui = new Itil(Base, "Gui");
                list = new ItilList(Base, "List");
                inc = new SNow.SecurityIncident(Base, "Security Incident");
                //------------------------------------------------------------------
                id = string.Empty;
                form = Base.GData("FormName");
                platform = Base.GData("PlatformName");
                listOfControl = new Dictionary<string, snoelement>();
                currentDateTime = string.Empty;
                isCreation = Base.GData("isCreation");

                switch (form.ToLower())
                {
                    case "service desk":
                    case "security incident":
                    case "event management":
                        isCI = false;
                        break;
                    default:
                        isCI = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.GData("UseGlobalPass");
                if (temp.ToLower() == "yes")
                {

                    Thread.Sleep(5000);
                }
                else login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_003_Impersonate_SecurityAnalyst()
        //{
        //	try
        //	{
        //		flag = home.ImpersonateUser(Base.GData("ImpersonateUser"));
        //		if (!flag)
        //		{
        //			error = "Cannot impersonate user";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Open_List()
        {
            try
            {
                string temp = "Show All" + form;
                if (form == "Security Incident")
                    temp = "Show All Incidents";
                if (platform.ToLower() != "no" && platform.ToLower() != "")
                    temp = platform;
                flag = home.LeftMenuItemSelect(form, temp);
                if (!flag)
                    error = "Error when open list.";
                else list.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Verify_List_Title()
        {
            try
            {
                string temp = Base.GData("List_Title");
                string label = list.List_Title().MyText;
                if (label.ToLower() != temp.ToLower())
                {
                    flag = false;
                    flagExit = false;
                    error = "Invalid list tilte. Runtime: [" + label + "] - Expected: [" + temp + "]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Reset_List_Columns_To_Default_Value()
        {
            try
            {
                flag = list.ResetColumnToDefaultValue();
                if (!flag)
                    error = "Error when reset list columns to default value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Verify_List_Columns_Default_Value()
        {
            try
            {
                string expectedColumns = Base.GData("List_Default_Columns");
                //flag = list.CheckColumnHeaderExist(expectedColumns);
                flag = list.VerifyColumnHeader(expectedColumns, ref error);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid default value of columns list.";
                }

                //Verify Priority colors
                //if (form == "Security Incident" && !platform.Contains("Requests"))
                //{
                //    System.Console.WriteLine("-----------------------Verify Priority colors---------------------");

                //    //Verify Critical Priority is red
                //    flag = list.SearchAndVerifyColor("Priority", "1 - Critical", "Priority=1 - Critical", "Priority", "red");
                //    if (!flag) error = "Critical Priority is not red";

                //    //Verify High Priority is orange
                //    flag = list.SearchAndVerifyColor("Priority", "2 - High", "Priority=2 - High", "Priority", "orange");
                //    if (!flag) error = "High Priority is not orange";

                //    //Verify Medium Priority is yellow
                //    flag = list.SearchAndVerifyColor("Priority", "3 - Medium", "Priority=3 - Medium", "Priority", "yellow");
                //    if (!flag) error = "Medium Priority is not yellow";

                //    //Verify Low Priority is blue
                //    flag = list.SearchAndVerifyColor("Priority", "4 - Low", "Priority=4 - Low", "Priority", "blue");
                //    if (!flag) error = "Medium Priority is not blue";
                //}

                ////Verify Hierarchical List
                //if (form == "Security Incident" && !platform.Contains("Requests"))
                //{
                //    System.Console.WriteLine("-----------------------Verify Hierarchical List---------------------");
                //    //Verify SIR with no Alerts
                //    string temp = Base.GData("SIRnoAlerts");
                //    flag = list.Filter("Number;is;" + temp);
                //    if (flag)
                //    {
                //        button = list.Button_Display_HierarchicalList();
                //        if (button.Existed)
                //        {
                //            flag = button.Click();
                //            if (flag)
                //            {
                //                ele = (Auto.oelement)Base.GObject("", "Number of record in table", By.XPath("//table[@hier_list_name='sn_si_incident.em_alert.incident']/tbody/tr/td[text()='No records to display']"));
                //                if (!ele.Existed)
                //                {
                //                    flag = false;
                //                    error = "There is some records in Hierarchical list. Expected: There should be no records in the list.";
                //                }
                //                //Verify Hide Hierarchical List button
                //                button = list.Button_Hide_HierarchicalList();
                //                if (!button.Existed)
                //                {
                //                    flag = false;
                //                    error = "There is no hide hierarchical list button";
                //                }
                //                else flag = true;
                //            }
                //            else
                //            {
                //                flag = false;
                //                error = "Hierarchical List button does not exist";
                //            }
                //        }
                //    }

                //    //Verify SIR with Alerts
                //    temp = Base.GData("SIRwithAlerts");
                //    flag = list.Filter("Number;is;" + temp);
                //    if (flag)
                //    {
                //        button = list.Button_Display_HierarchicalList();
                //        if (button.Existed)
                //        {
                //            flag = button.Click();
                //            if (flag)
                //            {
                //                ele = (Auto.oelement)Base.GObject("", "Number of record in table", By.XPath("//table[@hier_list_name='sn_si_incident.em_alert.incident']/tbody/tr/td[text()='No records to display']"));
                //                if (ele.Existed)
                //                {
                //                    flag = false;
                //                    error = "There is no records in Hierarchial list. Expected: There should be some records in the list";
                //                }
                //                //Verify Hide Hierarchical List button
                //                button = list.Button_Hide_HierarchicalList();
                //                if (!button.Existed)
                //                {
                //                    flag = false;
                //                    error = "There is no hide hierarchical list button";
                //                }
                //                else flag = true;
                //            }
                //            else
                //            {
                //                flag = false;
                //                error = "Hierarchical List button does not exist";
                //            }
                //        }
                //    }
                //}
                ////Verify default sort order of all SIR lists is descending order of Created column
                //if (form == "Security Incident" && !platform.Contains("Requests"))
                //{
                //    System.Console.WriteLine("-----------------------Verify default sort order of all SIR lists---------------------");
                //    flag = home.LeftMenuItemSelect(form, "Assigned to Me");
                //    if (flag)
                //    {
                //        list.WaitLoading();
                //        flag = list.VerifyColumnSortOrder("Updated", "Descending");
                //        if (!flag)
                //        {
                //            flagExit = false;
                //            error = "Default sort order is not correct";
                //        }
                //    }

                //    flag = home.LeftMenuItemSelect(form, "Assigned to Team");
                //    if (flag)
                //    {
                //        list.WaitLoading();
                //        flag = list.VerifyColumnSortOrder("Updated", "Descending");
                //        if (!flag)
                //        {
                //            flagExit = false;
                //            error = "Default sort order is not correct";
                //        }
                //    }

                //    flag = home.LeftMenuItemSelect(form, "Show Open Incidents");
                //    if (flag)
                //    {
                //        list.WaitLoading();
                //        flag = list.VerifyColumnSortOrder("Updated", "Descending");
                //        if (!flag)
                //        {
                //            flagExit = false;
                //            error = "Default sort order is not correct";
                //        }
                //    }

                //    flag = home.LeftMenuItemSelect(form, "Show All Incidents");
                //    if (flag)
                //    {
                //        list.WaitLoading();
                //        flag = list.VerifyColumnSortOrder("Updated", "Descending");
                //        if (!flag)
                //        {
                //            flagExit = false;
                //            error = "Default sort order is not correct";
                //        }
                //    }

                //    flag = home.LeftMenuItemSelect(form, "Unassigned Incidents");
                //    if (flag)
                //    {
                //        list.WaitLoading();
                //        flag = list.VerifyColumnSortOrder("Updated", "Descending");
                //        if (!flag)
                //        {
                //            flagExit = false;
                //            error = "Default sort order is not correct";
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Verify_Item_List_Combobox_Goto()
        {
            try
            {
                string expectedColumns = Base.GData("Items_Of_Combobox_Goto");
                flag = list.VerifyComboboxGoto(expectedColumns,ref error);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid item list value of combobox goto.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_ClickOn_New_Button()
        {
            try
            {
                if (isCreation == "yes")
                {
                    obutton button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            Thread.Sleep(2000);
                            gui.WaitLoading();
                        }
                        else
                            error = "Error when click on button New.";
                    }
                    else error = "Cannot get button New.";
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_01_ClickOn_Submit_Button()
        {
            try
            {
                if (isCreation == "yes")
                {
                    obutton button = gui.Button_Submit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                            error = "Error when click on submit button.";
                    }
                    else error = "Cannot get button submit.";
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_Verify_Message_Require_Mandatory_Field()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string message = Base.GData("Message_Mandatory_Fields_Not_Filled");
                    flag = gui.Verify_ExpectedErrorMessages_Existed(message);
                    if (!flag)
                    {

                        error = "Invalid message error.";
                        flagExit = false;
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_GET_ALL_TEXTBOX_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("TextboxFields_Visible").ToLower();
                    string str2 = Base.GData("TextboxFields_Readonly").ToLower();
                    if (str1 != "no" || str2 != "no")
                        listOfControl = gui.GetControlOnForm("textbox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Verify_Textbox_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("TextboxFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("textbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_03_Verify_Textbox_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("TextboxFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("textbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_01_GET_ALL_COMBOBOX_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("ComboboxFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("ComboboxFields_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("combobox");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_02_Verify_Combobox_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("ComboboxFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("combobox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_03_Verify_Combobox_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("ComboboxFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("combobox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_GET_ALL_TEXTAREA_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("TextareaFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("TextareaFields_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("textarea");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_02_Verify_Textarea_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("TextareaFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("textarea", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_03_Verify_Textarea_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("TextareaFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("textarea", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_01_GET_ALL_DATETIME_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("DatetimeFields_After_Save_Visible").Trim().ToLower();
                    string str2 = Base.GData("DatetimeFields_After_Save_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("datetime");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_02_Verify_Datetime_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("DatetimeFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("datetime", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_03_Verify_Datetime_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("DatetimeFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("datetime", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_01_GET_ALL_LOOKUP_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("LookupFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("LookupFields_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("lookup");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_02_Verify_Lookup_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("LookupFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("lookup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_03_Verify_Lookup_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("LookupFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("lookup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_01_GET_ALL_CHECKBOX_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("CheckboxFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("CheckboxFields_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" )
                    {
                        listOfControl = gui.GetControlOnForm("checkbox");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_02_Verify_Checkbox_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("CheckboxFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("checkbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_03_Verify_Checkbox_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("CheckboxFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("checkbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_GET_ALL_MANDATORY_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("MandatoryFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("MandatoryFields_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetManatoryControlOnForm();
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_Verify_Mandatory_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("MandatoryFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("mandatory", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_03_Verify_Mandatory_ReadOnly()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("MandatoryFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("mandatory", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_GET_ALL_BUTTON_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("Button_Visible").Trim().ToLower();
                    string str2 = Base.GData("Button_Not_Visible").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetButtonControlOnForm();
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Verify_Button_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("Button_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Button_Controls(false, expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_Verify_Button_Not_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("Button_Not_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Button_Controls(true, expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_04_GET_ALL_BUTTON_LIST_ON_FORM()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string str1 = Base.GData("Button_List_Visible").Trim().ToLower();
                    string str2 = Base.GData("Button_List_Not_Visible").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetButtonControlOnForm();
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_05_Verify_Button_List_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("Button_List_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Button_Controls(false, expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }                   
                }               
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_06_Verify_Button_List_Not_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("Button_List_Not_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Button_Controls(true, expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_Verify_Colums_Search_Table_Of_Lookup()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string expectedControlList = Base.GData("Column_Search_Table_Of_Lookup");
                    if (expectedControlList.ToLower() != "no")
                    {
                        flag = gui.Verify_Lookup_Columns_Search_Table(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_01_Add_Attachment_File()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string attachmentFile = Base.GData("Attachment_File");
                    if (attachmentFile.ToLower() != "no")
                    {
                        flag = gui.Add_AttachmentFile(attachmentFile);
                        if (flag == false)
                        {
                            error = "Error when attachment file.";
                            flagExit = false;
                        }
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_02_Verify_AttachmentFile_Added()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string attachmentFile = Base.GData("Attachment_File");
                    if (attachmentFile.ToLower() != "no")
                    {
                        flag = gui.Verify_Attachment_File(attachmentFile);
                        if (!flag)
                        {
                            error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                            flagExit = false;
                        }
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_020_05_Verify_Subcategory()
        //{
        //    try
        //    {
        //        if (isCreation == "yes")
        //        {
        //            string expectedControlList = Base.GData("SubcategoryField");
        //            if (expectedControlList.ToLower() != "no")
        //            {
        //                flag = gui.VerifySubcategory(expectedControlList);
        //                if (!flag)
        //                {
        //                    error = "There is something wrong when checking Category and Subcategory";
        //                    flagExit = false;
        //                }
        //            }
        //        }
        //        else flag = true;
        //    }
        //    catch (Exception wex)
        //    {
        //        flag = false;
        //        error = wex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_021_Input_OptionalFields()
        //{
        //    try
        //    {
        //        if (isCreation == "yes")
        //        {
        //            string optionfield = Base.GData("OptionalFields");
        //            if (optionfield.ToLower() != "no")
        //            {
        //                flag = gui.set(Base.GData("OptionalFields"), true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Cannot fill optional fields.";
        //                }
        //            }
        //        }
        //        else flag = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Input_MandatoryFields()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string caller = Base.GData("Caller");
                    string agroup = Base.GData("AssignmentGroup");
                    string company = Base.GData("Company");
                    string auser = Base.GData("AssignedTo");
                    if (caller.ToLower() == "no")
                    {
                        olookup lookup = gui.Lookup_Company();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.Select(company);
                            if (!flag)
                                error = "Error when select company.";
                        }
                        else error = "Cannot get lookup Company.";
                    }
                    else
                    {

                        olookup lookup = gui.Lookup_Caller();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.Select(caller);
                            if (flag)
                            {
                                gui.WaitLoading();
                                lookup = gui.Lookup_Company();
                                flag = lookup.Existed;
                                if (flag)
                                {
                                    flag = lookup.VerifyCurrentValue(company, true);
                                    if (!flag)
                                        error = "Company is not as expected.";
                                }
                                else error = "Cannot get lookup Company.";
                            }
                            else error = "Error when select Caller.";
                        }
                        else error = "Cannot get lookup Caller.";
                    }

                    if (agroup.ToLower() != "no")
                    {
                        olookup lookup = gui.Lookup_AssignmentGroup();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.Select(agroup);
                            if (flag)
                            {
                                if (auser.ToLower() != "no")
                                {
                                    lookup = gui.Lookup_AssignedTo();
                                    flag = lookup.Existed;
                                    if (flag)
                                    {
                                        flag = lookup.Select(auser);
                                        if (!flag)
                                            error = "Error when select assigned to.";
                                    }
                                    else error = "Cannot get lookup Assigned To.";
                                }
                            }
                            else error = "Error when select assignment group.";
                        }
                        else error = "Cannot get lookup Assignment Group.";
                    }


                    if (flag)
                    {
                        if (!isCI)
                            flag = gui.Input_Mandatory_Fields("Test Automation");
                        else
                        {
                            currentDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
                            flag = gui.Input_Mandatory_Fields("Test Automation_" + currentDateTime);
                        }

                        if (!flag)
                        {
                            error = "Have error when auto fill mandatory fields.";
                        }
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_StoreId_Submit()
        {
            try
            {
                if (isCreation == "yes")
                {
                    if (isCI)
                        id = gui.Textbox_Name().Text;
                    else
                        id = gui.Textbox_Number().Text;

                    button = gui.Button_Submit();
                    flag = button.Click();
                    if (flag)
                        list.WaitLoading();
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Verify_Back_To_List()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string temp = Base.GData("List_Title");
                    string label = list.List_Title().MyText;
                    flag = list.List_Title().Existed;
                    if (flag)
                    {
                        string runtimeTitle = label;
                        if (runtimeTitle.ToLower() != temp.ToLower())
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid list tilte. Runtime: [" + runtimeTitle + "] - Expected: [" + temp + "]";
                        }
                    }
                    else
                    {
                        flag = false;
                        flagExit = false;
                        error = "Not found title label";

                        temp = "Show All" + form;
                        if (form == "Security Incidents")
                            temp = "Show All Incidents";
                        if (platform.ToLower() != "no" && platform.ToLower() != "")
                            temp = platform;
                        flag = home.LeftMenuItemSelect(form, temp);
                        if (!flag)
                            error = "Error when open list.";
                        else list.WaitLoading();
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_03_Verify_Actions_Items_Visible()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string temp = Base.GData("List_Action_Items_Visible");
                    if (temp.ToLower() != "no")
                    {
                        flag = list.Verify_Action_Items_Visible(temp);
                    }

                    if (!flag)
                    {
                        error = "Invalid action items visible.";
                        flagExit = false;
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_04_Verify_Actions_Items_Enable()
        {
            try
            {
                if (isCreation == "yes")
                {
                    string temp = Base.GData("List_Action_Items_Enable");
                    if (temp.ToLower() != "no")
                    {
                        flag = list.Verify_Action_Items_Enable(temp);
                    }

                    if (!flag)
                    {
                        error = "Invalid action items enable.";
                        flagExit = false;
                    }
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_SearchAndOpen()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (id == string.Empty)
                    id = Base.GData("Id/Name");
                if (temp == "yes" && (id == null || id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Id or Name.");
                    addPara.ShowDialog();
                    id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                if (!isCI)
                {
                    switch (form.ToLower())
                    {
                        case "demands":
                            flag = list.SearchAndOpen("Number", id, "Number=" + id, "Demand");
                            break;
                        case "event management":
                            if (platform == "All Events")
                            {
                                flag = list.SearchAndOpen("Alert", "=" + id, "Alert=" + id, "Time of event");
                            }
                            else
                                flag = list.SearchAndOpen("Number", id, "Number=" + id, "Number");
                            break;
                        case "security incident":
                            if (!platform.Contains("Requests"))
                            {
                                flag = list.SearchAndOpen("Number", id, "Number=" + id, "Number");

                                //Verify Security Tag combobox
                                //System.Console.WriteLine("-----------------------Verify Security Tag combobox---------------------");
                                //string secTaglist = Base.GData("SecTagList");
                                //Thread.Sleep(5000);
                                //cb = inc.Combobox_SecurityTag();
                                //if (cb.Existed)
                                //{
                                //    flag = cb.VerifyItemList(secTaglist);
                                //    if (!flag)
                                //    {
                                //        flagExit = false;
                                //        error = "One of Security Tag Items in the list is missing or incorrect.";
                                //    }
                                //}
                                //else
                                //{
                                //    flag = false;
                                //    error = "Not found Security Tag combobox";
                                //}
                                break;
                            }
                            else
                            {
                                flag = list.SearchAndOpen("Number", id, "Number=" + id, "Number");
                                break;
                            }
                        default:
                            flag = list.SearchAndOpen("Number", id, "Number=" + id, "Number");
                            break;
                    }
                }
                else
                    flag = list.SearchAndOpen("Name", id, "Name=" + id, "Name");

                Thread.Sleep(2000);

                if (!flag) error = "Error when search and open record : [" + id + "]";
                else gui.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_01_Rename_Attachment_File()
        {
            try
            {
                string attachmentFile = Base.GData("Attachment_File");
                if (attachmentFile.ToLower() != "no")
                {

                    flag = gui.Rename_AttachmentFile(attachmentFile);
                    if (flag == false)
                    {
                        error = "Error when rename attachment file.";
                        flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_02_Verify_Attachment_File_Renamed()
        {
            try
            {
                string attachmentFile = Base.GData("Attachment_File");
                if (attachmentFile.ToLower() != "no")
                {
                    string[] arr = attachmentFile.Split('.');
                    string str = arr[0] + "_rename" + "." + arr[1];
                    flag = gui.Verify_Attachment_File(str);
                    if (!flag)
                    {
                        error = "Not found attachment file (" + str + ") in attachment container.";
                        flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_03_01_Delete_Attachment_File()
        {
            try
            {
                string attachmentFile = Base.GData("Attachment_File");
                if (attachmentFile.ToLower() != "no")
                {
                    string[] arr = attachmentFile.Split('.');
                    string str = arr[0] + "_rename" + "." + arr[1];
                    flag = gui.Delete_AttachmentFile(str);
                    if (flag == false)
                    {
                        error = "Error when delete attachment file.";
                        flagExit = false;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_03_02_Verify_Attachment_File_Deleted()
        {
            try
            {
                string attachmentFile = Base.GData("Attachment_File");
                if (attachmentFile.ToLower() != "no")
                {
                    string[] arr = attachmentFile.Split('.');
                    string str = arr[0] + "_rename" + "." + arr[1];
                    flag = gui.Verify_Attachment_File(str);
                    if (flag)
                    {
                        error = "Found attachment file (" + str + ") in attachment container.";
                        flag = false;
                        flagExit = false;
                    }
                    else flag = true;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_04_Update_State_If_Need()
        {
            try
            {
                string temp = Base.GData("Update_State");
                string[] arr = null;

                if (temp.ToLower() != "no")
                {
                    if (temp.Contains(";"))
                    {
                        arr = temp.Split(';');
                        foreach (string state in arr)
                        {
                            ocombobox combos = gui.Combobox_State();
                            flag = combos.Existed;
                            if (flag)
                            {
                                flag = combos.SelectItem(state);
                                if (!flag)
                                    error = "Error when select state.";
                                else
                                {
                                    gui.Save();
                                    gui.WaitLoading();
                                }
                            }
                            else error = "Cannot get combobox state.";
                        }
                    }
                    else
                    {
                        ocombobox combo = gui.Combobox_State();
                        flag = combo.Existed;
                        if (flag)
                        {
                            flag = combo.SelectItem(temp);
                            if (!flag)
                                error = "Error when select state.";
                            else
                            {
                                gui.WaitLoading();
                            }
                        }
                        else error = "Cannot get combobox state.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_GET_ALL_TEXTBOX_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalTextboxFields_Visible").ToLower();
                string str2 = Base.GData("AdditionalTextboxFields_Readonly").ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("textbox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_Verify_Additional_Textbox_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalTextboxFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("textbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_Verify_Additional_Textbox_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalTextboxFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("textbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_GET_ALL_COMBOBOX_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalComboboxFields_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalComboboxFields_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("combobox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }               
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_Verify_Additional_Combobox_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalComboboxFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("combobox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_03_Verify_Additional_Combobox_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalComboboxFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("combobox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_01_GET_ALL_TEXTAREA_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalTextareaFields_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalTextareaFields_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("textarea");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_02_Verify_Additional_Textarea_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalTextareaFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("textarea", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_03_Verify_Additional_Textarea_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalTextareaFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("textarea", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_01_GET_ALL_DATETIME_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalDatetimeFields_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalDatetimeFields_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("datetime");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_02_Verify_Additional_Datetime_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalDatetimeFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("datetime", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_03_Verify_Additional_Datetime_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalDatetimeFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("datetime", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_01_GET_ALL_LOOKUP_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalLookupFields_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalLookupFields_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("lookup");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_02_Verify_Additional_Lookup_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalLookupFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("lookup", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_03_Verify_Additional_Lookup_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalLookupFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("lookup", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_01_GET_ALL_CHECKBOX_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalCheckboxFields_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalCheckboxFields_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("checkbox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_02_Verify_Additional_Checkbox_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalCheckboxFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("checkbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_03_Verify_Additional_Checkbox_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalCheckboxFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("checkbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_01_GET_ALL_MANDATORY_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalMandatoryFields_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalMandatoryFields_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetManatoryControlOnForm();
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_02_Verify_Additional_Mandatory_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalMandatoryFields_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("mandatory", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_03_Verify_Additional_Mandatory_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalMandatoryFields_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("mandatory", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_GET_ALL_BUTTON_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("AdditionalButton_Visible").Trim().ToLower();
                string str2 = Base.GData("AdditionalButton_Not_Visible").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetButtonControlOnForm();
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Verify_Additional_Button_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalButton_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Button_Controls(false, expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Verify_Additional_Button_Not_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("AdditionalButton_Not_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Button_Controls(true, expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_032_04_GET_ALL_BUTTON_LIST_ON_FORM()
        //{
        //    try
        //    {
        //        string str1 = Base.GData("AdditionalButton_List_Visible").ToLower();
        //        string str2 = Base.GData("AdditionalButton_List_Not_Visible").ToLower();
        //        if (str1 != "no" || str2 != "no")
        //            listOfControl = gui.GControlByType("buttonlist");

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_032_05_Verify_Additional_Button_List_Visible()
        //{
        //    try
        //    {
        //        string expectedControlList = Base.GData("AdditionalButton_List_Visible");
        //        if (expectedControlList.ToLower() != "no")
        //        {
        //            flag = gui.VerifyControls(expectedControlList, "buttonlist", listOfControl, ref error);
        //            if (!flag)
        //                flagExit = false;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_032_06_Verify_Additional_Button_List_Not_Visible()
        //{
        //    try
        //    {
        //        string expectedControlList = Base.GData("AdditionalButton_List_Not_Visible");
        //        if (expectedControlList.ToLower() != "no")
        //        {
        //            flag = gui.VerifyControls(expectedControlList, "buttonlist", listOfControl, ref error, "", false, true);
        //            if (!flag)
        //                flagExit = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Input_MandatoryFields_IfNeed()
        {
            try
            {
                string auser = Base.GData("AssignedTo");
                if (auser.ToLower() != "no")
                {
                    olookup lookup = gui.Lookup_AssignedTo();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(auser);
                        if (!flag)
                            error = "Error when select Assigned To.";
                    }
                    else error = "Cannot get lookup Assigned To.";
                }

                if (!isCI)
                    flag = gui.Input_Mandatory_Fields("Test Automation");
                else
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (currentDateTime == null || currentDateTime == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input string current date time.");
                        addPara.ShowDialog();
                        currentDateTime = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = gui.Input_Mandatory_Fields("Test Automation_" + currentDateTime);
                }

                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Save()
        {
            try
            {
                if (form.ToLower() != "event management")
                {
                    flag = gui.Save(true);
                    if (flag)
                        gui.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_Verify_RelatedList()
        //{
        //    try
        //    {
        //        string relatedlist = Base.GData("Related_List");
        //        flag = gui.related
        //        flag = gui.VerifyRelatedTable(relatedlist);
        //        if (!flag)
        //        {
        //            error = "There is something wrong when checking related list";
        //            flagExit = false;
        //        }
        //    }
        //    catch (Exception wex)
        //    {
        //        flag = false;
        //        error = wex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Verify_RelatedTab_DefaultValue_Of_TableColumns()
        {
            try
            {
                string temp = Base.GData("Related_Table_Default_Columns");
                if (temp.ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Default_Columns(temp, ref error);
                    if (!flag)
                    {
                        error = "Error when verify related tab default value of table columns.";
                        flagExit = false;
                    }
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Verify_RelatedTab_DefaultValue_Of_ComboboxGoto_Items()
        {
            try
            {
                string temp = Base.GData("Related_Table_Default_Combobox_Goto_Items");
                if (temp.ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Combobox_Goto(temp, ref error);
                    if (!flag)
                    {
                        error = "Error when verify related tab default value of combobox goto items.";
                        flagExit = false;
                    }

                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Verify_RelatedTab_Button_Visible()
        {
            try
            {
                string temp = Base.GData("Related_Table_Button_Visible");
                if (temp.ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Button_Controls(false, temp, ref error);
                    if (!flag)
                    {
                        error = "Error when verify related tab button visible.";
                        flagExit = false;
                    }

                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
