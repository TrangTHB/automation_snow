﻿using NUnit.Framework;
using SNow;
using System;
using System.Reflection;
using System.Threading;
namespace SecurityIncident
{
    [TestFixture]
    public class SIR_ITSM_Ticket_03
	{
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Security Incident Id: " + SecIncidentId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Auto.otextbox textbox;
        Auto.olookup lookup;
        Auto.ocombobox combobox;
        Auto.obutton button;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
		Itil sir = null;
        SecurityIncidentList sirlist = null;
		Incident inc = null;
		IncidentList inclist = null;
		Problem prb = null;
		ProblemList prblist = null;
		Change chg = null;
		ChangeList chglist = null;
        //------------------------------------------------------------------
        string SecIncidentId, IncidentId, ProblemId, ChangeId;

		//***********************************************************************************************************************************
		#endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

		#region Scenario of test case (NEED TO UPDATE)
		//***********************************************************************************************************************************

		[Test]
		public void ClassInit()
		{
			try
			{
				//------------------------------------------------------------------
				login = new Login(Base);
				home = new Home(Base);
				sir = new Itil(Base, "Security Incident");
				sirlist = new SecurityIncidentList(Base, "Security Incident list");
				inc = new Incident(Base, "Incident");
				inclist = new IncidentList(Base, "Incident list");
				prb = new Problem(Base, "Problem");
				prblist = new ProblemList(Base, "Problem list");
				chg = new Change(Base, "Change");
				chglist = new ChangeList(Base, "Change list");
				//------------------------------------------------------------------
				SecIncidentId = string.Empty;
				IncidentId = string.Empty;
				ProblemId = string.Empty;
				ChangeId = string.Empty;
			}
			catch (Exception ex)
			{
				flag = false;
				error = ex.Message;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		[Test]
		public void Step_001_OpenSystem()
		{
			try
			{
				Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
				login.WaitLoading();
			}
			catch (Exception ex)
			{
				flag = false;
				error = ex.Message;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		[Test]
		public void Step_002_1_Login()
		{
			try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
			catch (Exception ex)
			{
				flag = false;
				error = ex.Message;
			}
		}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_2_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_002_3_ImpersonateUser_Assignee()
        //{
        //    try
        //    {
        //        string temp = Base.GData("SocAnalyst");
        //        flag = home.ImpersonateUser(temp);
        //        if (!flag) error = "Error when impersonate assignee user (" + temp + ")";
        //        else home.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
		public void Step_003_OpenNewSecurityIncidentForm()
		{
			try
			{
				flag = home.LeftMenuItemSelect("Security Incident", "Show All Incidents");
				if (flag)
				{
					sirlist.WaitLoading();
					button = sirlist.Button_New();
					if (button.Existed)
					{
						flag = button.Click();
						if (!flag)
						{
							error = "Could not click New button";
						}
						else
						{
							sir.WaitLoading();
							SecIncidentId = sir.Textbox_Number().Text;
						}
					}
					else
					{
						error = "Not found New button";
					}
				}
				else
					error = "Error when navigating to Security Incident > Show All Incidents";
			}
			catch (Exception ex)
			{
				flag = false;
				error = ex.Message;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		[Test]
		public void Step_004_Populate_Company()
		{
			try
			{
				string temp = Base.GData("Company");

				lookup = sir.Lookup_Company();
				flag = lookup.Existed;
				if (flag)
				{
					flag = lookup.Select(temp);
					if (!flag)
					{
						error = "Invalid company value.";
						flagExit = false;
					}
				}
				else { error = "Cannot get lookup company."; }
			}
			catch (Exception ex)
			{
				flag = false;
				error = ex.Message;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		[Test]
		public void Step_005_Populate_Configuration_Item()
		{
			try
			{
				string temp = Base.GData("CI");

				lookup = sir.Lookup_ConfigurationItem();
				flag = lookup.Existed;
				if (flag)
				{
					flag = lookup.Select(temp);
					if (!flag)
					{
						error = "Invalid CI value.";
						flagExit = false;
					}
				}
				else { error = "Cannot get CI lookup."; }
			}
			catch (Exception ex)
			{
				flag = false;
				error = ex.Message;
			}
		}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_006_Populate_Location()
        //{
        //	try
        //	{
        //		string temp = Base.GData("Location");

        //		lookup = sir.Lookup_Location();
        //		flag = lookup.Existed;
        //		if (flag)
        //		{
        //			flag = lookup.Select(temp);
        //			if (!flag)
        //			{
        //				error = "Invalid Location value.";
        //				flagExit = false;
        //			}
        //		}
        //		else { error = "Cannot get Location lookup."; }
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Populate_Requested_By()
        {
            try
            {
                string temp = Base.GData("RequestedBy");

                //lookup = sir.Lookup_RequestedBySIR();
                lookup = sir.Lookup_RequestedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Invalid Requested By value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Requested By lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Populate_Source()
        {
            try
            {
                string temp = Base.GData("Source");

                combobox = sir.Combobox_Source();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Invalid Source value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Source combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = sir.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SaveSIR()
        {
            try
            {
                flag = sir.Save();
                if (!flag) { error = "Error when saving SIR"; }
                else
                {
                    sir.WaitLoading();

                    //Get parent incident number
                    SecIncidentId = sir.Textbox_Number().Text;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_011_CreateIncidentFromSIR()
        //{
        //    try
        //    {
        //        flag = sir.CreateIncident();
        //        if (flag)
        //        {
        //            //Get parent incident number
        //            tab = sir.GTab("Related Records");
        //            //---------------------------------------
        //            int i = 0;
        //            while (tab == null && i < 5)
        //            {
        //                Thread.Sleep(2000);
        //                tab = sir.GTab("Related Records", true);
        //                i++;
        //            }
        //            flag = tab.Header.Click(true);
        //            if (flag) IncidentId = sir.Lookup_RelatedIncident.Text;
        //            else error = "Cannot select Related Records tab.";

        //            flag = sir.VerifyMessageInfo("Incident " + IncidentId + " was created.");
        //            if (!flag) error = "Failed to create incident from SIR";
        //        }
        //        else error = "Cannot select context menu: Create Incident";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_012_SearchAndOpenIncident()
        //{
        //	try
        //	{
        //		//-- Input information
        //		string temp = Base.GData("Debug").ToLower();
        //		if (temp == "yes" && (IncidentId == null || IncidentId == string.Empty))
        //		{
        //			Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident number.");
        //			addPara.ShowDialog();
        //			IncidentId = addPara.value;
        //			addPara.Close();
        //			addPara = null;
        //		}
        //		//-----------------------------------------------------------------------
        //		flag = home.LeftMenuItemSelect("Incident", "Open");
        //		if (flag)
        //		{
        //			inclist.WaitLoading();
        //			temp = inclist.Label_Title.Text;
        //			flag = temp.Equals("Incidents");
        //			if (flag)
        //			{
        //				flag = inclist.SearchAndOpen("Number", IncidentId, "Number=" + IncidentId, "Number");
        //				if (!flag) error = "Error when search and open incident (id:" + IncidentId + ")";
        //				else inc.WaitLoading();
        //			}
        //			else
        //			{
        //				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
        //			}
        //		}
        //		else error = "Error when select open incident.";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_013_Verify_Company()
        //{
        //	try
        //	{
        //		string temp = Base.GData("Company");
        //		if (inc.Lookup_Company.Text != temp)
        //		{
        //			flag = false;
        //			error = "Company value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}

        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_Verify_Short_Description()
        //{
        //	try
        //	{
        //		string temp = Base.GData("ShortDescription");
        //		if (inc.Textbox_ShortDescription.Text != temp)
        //		{
        //			flag = false;
        //			error = "Short Description value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_015_Verify_Caller()
        //{
        //	try
        //	{
        //		string temp = Base.GData("RequestedBy");
        //		if (inc.Lookup_Caller.Text != temp)
        //		{
        //			flag = false;
        //			error = "Caller value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_Verify_Configuration_Item()
        //{
        //	try
        //	{
        //		string temp = Base.GData("CI");
        //		if (inc.Lookup_ConfigurationItem.Text != temp)
        //		{
        //			flag = false;
        //			error = "CI value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_017_Verify_Location()
        //{
        //	try
        //	{
        //		string temp = Base.GData("Location");
        //		if (inc.Lookup_Location.Text != temp)
        //		{
        //			flag = false;
        //			error = "Location value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_018_Verify_Category()
        //{
        //	try
        //	{
        //		if (inc.Combobox_Category.Text != "Security")
        //		{
        //			flag = false;
        //			error = "Category value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_019_SearchAndOpenSecurityIncident()
        //{
        //	try
        //	{
        //		//-- Input information
        //		string temp = Base.GData("Debug").ToLower();
        //		if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
        //		{
        //			Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
        //			addPara.ShowDialog();
        //			SecIncidentId = addPara.value;
        //			addPara.Close();
        //			addPara = null;
        //		}
        //		//-----------------------------------------------------------------------
        //		flag = home.LeftMenuItemSelect("Security Incident", "Show All Incidents");
        //		if (flag)
        //		{
        //			sirlist.WaitLoading();
        //			temp = sirlist.Label_Title.Text;
        //			flag = temp.Equals("Security Incidents");
        //			if (flag)
        //			{
        //				flag = sirlist.SearchAndOpen("Number", SecIncidentId, "Number=" + SecIncidentId, "Number");
        //				if (!flag) error = "Error when search and open security incident (id:" + SecIncidentId + ")";
        //				else sir.WaitLoading();
        //			}
        //			else
        //			{
        //				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Security Incidents)";
        //			}
        //		}
        //		else error = "Error when select open security incident.";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_020_CreateProblemFromSIR()
        //{
        //	try
        //	{
        //		flag = sir.CreateProblem();
        //		if (flag)
        //		{
        //			//Get parent problem number
        //			tab = sir.GTab("Related Records");
        //			//---------------------------------------
        //			int i = 0;
        //			while (tab == null && i < 5)
        //			{
        //				Thread.Sleep(2000);
        //				tab = sir.GTab("Related Records", true);
        //				i++;
        //			}
        //			flag = tab.Header.Click(true);
        //			if (flag) ProblemId = sir.Lookup_RelatedProblem.Text;
        //			else error = "Cannot select Related Records tab.";

        //			//Verify successful messsages
        //			flag = sir.VerifyMessageInfo("Problem " + ProblemId + " was created.");
        //			if (!flag) error = "Failed to create incident from SIR";
        //		}
        //		else error = "Cannot select context menu: Create Incident";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_021_SearchAndOpenProblem()
        //{
        //	try
        //	{
        //		//-- Input information
        //		string temp = Base.GData("Debug").ToLower();
        //		if (temp == "yes" && (ProblemId == null || ProblemId == string.Empty))
        //		{
        //			Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
        //			addPara.ShowDialog();
        //			ProblemId = addPara.value;
        //			addPara.Close();
        //			addPara = null;
        //		}
        //		//-----------------------------------------------------------------------
        //		flag = home.LeftMenuItemSelect("Problem", "Open");
        //		if (flag)
        //		{
        //			prblist.WaitLoading();
        //			temp = prblist.Label_Title.Text;
        //			flag = temp.Equals("Problems");
        //			if (flag)
        //			{
        //				flag = prblist.SearchAndOpen("Number", ProblemId, "Number=" + ProblemId, "Number");
        //				if (!flag) error = "Error when search and open problem (id:" + ProblemId + ")";
        //			}
        //			else
        //			{
        //				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
        //			}
        //		}
        //		else error = "Error when select open problem.";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_022_Verify_Company()
        //{
        //	try
        //	{
        //		string temp = Base.GData("Company");
        //		if (prb.Lookup_Company.Text != temp)
        //		{
        //			flag = false;
        //			error = "Company value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_023_Verify_Short_Description()
        //{
        //	try
        //	{
        //		string temp = Base.GData("ShortDescription");
        //		if (prb.Textbox_ShortDescription.Text != temp)
        //		{
        //			flag = false;
        //			error = "Short Description value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_Verify_Configuration_Item()
        //{
        //	try
        //	{
        //		string temp = Base.GData("CI");
        //		if (prb.Lookup_ConfigurationItem.Text != temp)
        //		{
        //			flag = false;
        //			error = "CI value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_025_Verify_Location()
        //{
        //	try
        //	{
        //		string temp = Base.GData("Location");
        //		if (prb.Lookup_Location.Text != temp)
        //		{
        //			flag = false;
        //			error = "Location value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_Verify_Category()
        //{
        //	try
        //	{
        //		if (prb.Combobox_Category.Text != "Security")
        //		{
        //			flag = false;
        //			error = "Category value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_027_SearchAndOpenSecurityIncident()
        //{
        //	try
        //	{
        //		//-- Input information
        //		string temp = Base.GData("Debug").ToLower();
        //		if (temp == "yes" && (SecIncidentId == null || SecIncidentId == string.Empty))
        //		{
        //			Auto.AddParameter addPara = new Auto.AddParameter("Please input security incident Id.");
        //			addPara.ShowDialog();
        //			SecIncidentId = addPara.value;
        //			addPara.Close();
        //			addPara = null;
        //		}
        //		//-----------------------------------------------------------------------
        //		flag = home.LeftMenuItemSelect("Security Incident", "Show All Incidents");
        //		if (flag)
        //		{
        //			sirlist.WaitLoading();
        //			temp = sirlist.Label_Title.Text;
        //			flag = temp.Equals("Security Incidents");
        //			if (flag)
        //			{
        //				flag = sirlist.SearchAndOpen("Number", SecIncidentId, "Number=" + SecIncidentId, "Number");
        //				if (!flag) error = "Error when search and open security incident (id:" + SecIncidentId + ")";
        //				else sir.WaitLoading();
        //			}
        //			else
        //			{
        //				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Security Incidents)";
        //			}
        //		}
        //		else error = "Error when select open security incident.";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_028_CreateChangeFromSIR()
        //{
        //	try
        //	{
        //		flag = sir.CreateNormalChange();
        //		if (flag)
        //		{
        //			//Get parent change number
        //			tab = sir.GTab("Related Records");
        //			//---------------------------------------
        //			int i = 0;
        //			while (tab == null && i < 5)
        //			{
        //				Thread.Sleep(2000);
        //				tab = sir.GTab("Related Records", true);
        //				i++;
        //			}
        //			flag = tab.Header.Click(true);
        //			if (flag) ChangeId = sir.Lookup_RelatedChange.Text;
        //			else error = "Cannot select Related Records tab.";

        //			//Verify successful messages
        //			flag = sir.VerifyMessageInfo("Change request " + ChangeId + " was created.");
        //			if (!flag) error = "Failed to create Change from SIR";
        //		}
        //		else error = "Cannot select context menu: Create Normal Change";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_029_SearchAndOpenChange()
        //{
        //	try
        //	{
        //		//-- Input information
        //		string temp = Base.GData("Debug").ToLower();
        //		if (temp == "yes" && (ChangeId == null || ChangeId == string.Empty))
        //		{
        //			Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
        //			addPara.ShowDialog();
        //			ChangeId = addPara.value;
        //			addPara.Close();
        //			addPara = null;
        //		}
        //		//-----------------------------------------------------------------------
        //		flag = home.LeftMenuItemSelect("Change", "Open");
        //		if (flag)
        //		{
        //			chglist.WaitLoading();
        //			temp = chglist.Label_Title.Text;
        //			flag = temp.Equals("Change Requests");
        //			if (flag)
        //			{
        //				flag = chglist.SearchAndOpen("Number", ChangeId, "Number=" + ChangeId, "Number");
        //				if (!flag) error = "Error when search and open change (id:" + ChangeId + ")";
        //				else chg.WaitLoading();
        //			}
        //			else
        //			{
        //				error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
        //			}
        //		}
        //		else error = "Error when select open change.";
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_030_Verify_Company()
        //{
        //	try
        //	{
        //		string temp = Base.GData("Company");
        //		if (chg.Lookup_Company.Text != temp)
        //		{
        //			flag = false;
        //			error = "Company value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_031_Verify_RequestedBy()
        //{
        //	try
        //	{
        //		string temp = Base.GData("UserFullName");
        //		if (chg.Lookup_RequestedBy().Text != temp)
        //		{
        //			flag = false;
        //			error = "Caller value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_032_Verify_Configuration_Item()
        //{
        //	try
        //	{
        //		string temp = Base.GData("CI");
        //		if (chg.Lookup_ConfigurationItem.Text != temp)
        //		{
        //			flag = false;
        //			error = "CI value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_033_Verify_Short_Description()
        //{
        //	try
        //	{
        //		string temp = Base.GData("ShortDescription");
        //		if (chg.Textbox_ShortDescription.Text != temp)
        //		{
        //			flag = false;
        //			error = "Short Description value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_034_Verify_Category()
        //{
        //	try
        //	{
        //		if (chg.Combobox_Category.Text != "Security")
        //		{
        //			flag = false;
        //			error = "Category value does not match with SIR";
        //		}
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_Logout()
        //{
        //	try
        //	{
        //		flag = home.Logout();
        //		if (!flag)
        //		{
        //			error = "Error when logout system.";
        //		}
        //		else
        //			login.WaitLoading();
        //	}
        //	catch (Exception ex)
        //	{
        //		flag = false;
        //		error = ex.Message;
        //	}
        //}
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}