﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
using SNow;
namespace WorkOrder
{
    [TestFixture]
    public class WO_follow_up_flag_8
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workorderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + wotaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snocheckbox checkbox;
        

        //------------------------------------------------------------------

        Login login = null;
        Home home = null;
        Incident inc = null;
        SNow.WorkOrder wo = null;
        IncidentList inclist = null;
        WorkOrderList woList = null;
        WorkOrderTask woTask = null;
        ItilList woQualifierList = null;
        EmailList emailList = null;
        Email email = null;

        //------------------------------------------------------------------
        string incidentId, workorderId, wotaskId;
        string wotShortdesc, wotDesc;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                inclist = new IncidentList(Base, "Incident list");
                woList = new WorkOrderList(Base, "Work Order List");
                woTask = new WorkOrderTask(Base, "Work Order Task");
                woQualifierList = new ItilList(Base, "Work Order Qualifier List");
                emailList = new EmailList(Base, "Email List");
                email = new Email(Base,"Email");

                //------------------------------------------------------------------
                incidentId = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDeskAgent()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulateDescription()
        {
            try
            {
                string temp = Base.GData("IncDescription");
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textbox description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_GlobalSearch_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(incidentId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_CreateWorkOrder()
        {
            try
            {
                flag = inc.CreateWorkOrder();
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot click to create work order"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_VerifyInformation()
        {
            try
            {
                error = "";
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Company"));
                    if (!flag) { error = "Incorrect company value."; }
                }
                else
                    error = "Cannot get lookup company.";

                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncCaller"));
                    if (!flag) { error = "Incorrect caller value."; }
                }
                else
                    error = "Cannot get lookup caller.";

                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;

                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(Base.GData("IncShortDescription"));
                    if (!flag) { error = "Incorrect short description value."; }
                }
                else
                    error = "Cannot get textbox short description.";

                lookup = wo.Lookup_Location();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncLocation"));
                    if (!flag) { error = "Incorrect location value."; }
                }
                else
                    error = "Cannot get lookup location.";

                if (!error.Equals(""))
                {
                    flag = false;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_01_VerifyCatalogOptions()
        {
            try
            {
                string temp = Base.GData("WOCatOptions");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                    {
                        error = "Category list is incorrect.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_2_PopulateCategory()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCat");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_ValidateQualificationGroup()
        {
            try
            {
                lookup = wo.Lookup_QualificationGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOQualification");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "The value of qualification group is not correct. Expected: " + lookup.Text;
                    }
                }
                else { error = "Cannot get lookup qualification group control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.Text != incidentId || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect value. This should be incident Id.";
                    }
                }
                else { error = "Cannot get textbox initiated from control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_ClickReadyForQualification()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification."; }
                }
                else { error = "Cannot get ready for qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_019_02_Open_WorkOrderTask()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Task Id.");
        //            addPara.ShowDialog();
        //            wotaskId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        string condition = "Number=" + wotaskId;
        //        flag = woList.SearchAndOpen("Number", wotaskId, condition, "Number");
        //        if (!flag)
        //        {
        //            error = "Cannot open Work Order Task (id: " + wotaskId + " )";
        //        }
        //        else
        //        {
        //            wo.WaitLoading();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_ClickQualified()
        {
            try
            {
                /*Get Work Order Task number*/
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store work order task id
                    wotaskId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Work Order Task Id:(" + wotaskId + ")");
                }
                else { error = "Cannot get textbox number."; }
                //---------------------------------------------------------
                button = woTask.Button_Qualified();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    wo.WaitLoading();
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;

                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Pending Dispatch");
                        if (!flag) { error = "The value of state is not correct."; }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else { error = "Cannot get button qualified."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_VerifyWOTaskGroup()
        {
            try
            {
                string temp = Base.GData("WOTAssignmentGroup");
                lookup = wo.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify WOT Assignment group value. Expected: [" + temp + "]. Runtime: [" + lookup.Text + "]";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get Assignment Group lookup.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Click_FollowupCheckbox()
        {
            try
            {
                checkbox = woTask.Checkbox_FollowUp();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click();
                        if (!flag) { error = "Cannot click Follow up checkbox"; }
                    }
                    else { error = "Follow up checkbox is already checked."; }
                }
                else { error = "Cannot get Follow up checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_AddWorkNote()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (!flag)
                    error = "Cannot select 'Notes' Tab.";
                else
                {
                    string temp = Base.GData("WOTWorkNotes");
                    textarea = wo.Textarea_Worknotes();
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot add work notes.";
                        }
                    }
                    else { error = "Cannot get Work Notes textarea."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_PopulateAssignedTo()
        {
            try
            {
                lookup = wo.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTechnician");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else { error = "Cannot get lookup assigned to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_VerifyWOTaskIsAssigned()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Assigned");
                    if (!flag) { error = "The value of state is not correct."; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_Verify_FollowupChecked()
        {
            try
            {
                checkbox = woTask.Checkbox_FollowUp();
                flag = checkbox.Checked;
                if (!flag)
                {
                    flag = false;
                    error = "The follow up checkbox is not checked.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_ImpersonateUser_WOTechnician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                string rootUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_029_SearchAndOpenTaskAssignedToMe()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Field Service", "Assigned to me");
                if (flag)
                {
                    woList.WaitLoading();
                    temp = woList.List_Title().MyText;
                    flag = temp.Equals("Work Order Tasks");
                    if (flag)
                    {
                        flag = woList.SearchAndOpen("Number", wotaskId, "Number=" + wotaskId, "Number");
                        if (!flag) error = "Error when search and open work order task (id:" + wotaskId + ")";
                    }
                    else
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Tasks)";
                }
                else
                    error = "Cannot select My Group";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_ClickAcceptButton()
        {
            try
            {
                button = wo.Button_Accept();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag)
                    {
                        error = "Can not click on Accept button.";
                    }
                    else { wo.WaitLoading(); }
                }
                else
                {
                    error = "Cannot get button Accept.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_VerifyWOTState()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Accepted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; flagExit = false; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_AddWorkNote()
        {
            try
            {
                textarea = wo.Textarea_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTWorkNotes");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not set text for WOT Work Notes.";
                    }
                }
                else
                {
                    error = "Can not get textarea Work Notes.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Verify_SwivelChecked()
        {
            try
            {
                checkbox = woTask.Checkbox_Swivel();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        error = "The Swivel checkbox is not checked.";
                    }
                }
                else { error = "Cannot get Swivel checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user: " + temp;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_036_SearchAndOpen_WOTask()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Field Service", "Qualifier - Dispatcher");
                if (flag)
                {
                    woQualifierList.WaitLoading();
                    flag = wo.TableSelectRow("TASK - Requires Swivel", "Number=" + wotaskId, "Number");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_01_AddWorkNotes()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (!flag)
                    error = "Cannot select 'Notes' Tab.";
                else
                {
                    string temp = Base.GData("WOTWorkNotes");
                    textarea = wo.Textarea_Worknotes();
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot add work notes.";
                        }
                    }
                    else { error = "Cannot get Work Notes textarea."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_02_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (flag)
                {
                    wotShortdesc = wo.Textbox_ShortDescription().Text;
                    wotDesc = wo.Textarea_Description().Text;
                }
                else
                { error = "Error when save Work Order Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_03_Verify_SwivelUnchecked()
        {
            try
            {
                checkbox = woTask.Checkbox_Swivel();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (flag)
                    {
                        flag = false;
                        error = "The Swivel checkbox is checked.";
                    }
                    else
                    {
                        //The checkbox is automatically unchecked.
                        flag = true;
                    }
                }
                else { error = "Cannot get Swivel checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_ImpersonateUser_Support()
        {
            try
            {
                string temp = Base.GData("SupportUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Search_Email()
        {
            try
            {
                //Fist email will be sent to WO Technician
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                string wotech_email = Base.GData("WOTechnician_Email");
                temp = "Subject;contains;" + wotaskId + "|and|Subject;contains;has been assigned to you|and|Recipients;contains;" + wotech_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    flag = emailList.Open("Recipients=" + wotech_email, "Created");
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found email sent to Requested For (request submitted)";
                    }
                    else
                    { emailList.WaitLoading(); }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_Open_Html_Review_Body()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_Validate_HTML_Body_Customer_Name()
        {
            try
            {
                string temp1 = "Customer name: ";
                string temp2 = Base.GData("IncCaller");
                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer name.";
                    }
                }
                else
                {
                    error = "Customer name is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_03_Validate_HTML_Body_Business_Phone()
        {
            try
            {
                string temp1 = "Business phone: ";
                string temp2 = Base.GData("IncCaller_BusinessPhone");
                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer's Business phone.";
                    }
                }
                else
                {
                    error = "Customer's Business phone is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_04_Validate_HTML_Body_Mobile_Phone()
        {
            try
            {
                string temp1 = "Mobile phone: ";
                string temp2 = Base.GData("IncCaller_MobilePhone");

                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer's Mobile phone.";
                    }
                }
                else
                {
                    error = "Customer's Mobile phone is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_05_Validate_HTML_Body_Short_Description()
        {
            try
            {
                string temp = "Short description: " + wotShortdesc;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid WOT's Short description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_06_Validate_HTML_Body_Description()
        {
            try
            {
                string temp = "Description: " + wotDesc;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid WOT's Description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Logout()
        {
            try
            {
                home.Logout();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //***********************************************************************************************************************************

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
