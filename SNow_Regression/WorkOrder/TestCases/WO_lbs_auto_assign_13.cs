﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using SNow;
namespace WorkOrder
{
    [TestFixture]
    public class WO_lbs_auto_assign_13
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workorderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + wotaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Auto.otextbox textbox;
        Auto.olookup lookup;
        Auto.ocombobox combobox;
        SNow.snocheckbox checkbox;
        Auto.obutton button;
        Auto.otextarea textarea;
       
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Incident inc = null;
        SNow.WorkOrder wo = null;
        SNow.IncidentList inclist = null;
        SNow.Member member = null;
        SNow.Email email = null;
        SNow.EmailList emaillist = null;
        SNow.WorkOrderTask wotask = null;
        SNow.WorkOrderList wolist = null;
        //------------------------------------------------------------------
        string incidentId, workorderId, wotaskId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                inclist = new IncidentList(Base, "Incident list");
                member = new Member(Base);
                email = new Email(Base, "Email");
                emaillist = new EmailList(Base, "Email list");
                wotask = new WorkOrderTask(Base, "Work Order Task");
                wolist = new WorkOrderList(Base, "Tasks list");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDesk()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_PopulateLocation()
        {
            try
            {
                string temp = Base.GData("IncLocation");
                lookup = inc.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    if(lookup.Text != temp.Trim() || lookup.Text == string.Empty || lookup.Text == null)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate location value."; }
                    }
                    
                }
                else
                {
                    error = "Cannot get lookup location.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_PopulateAssignmentGroup_LBS()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_AddAffectedCIs()
        {
            try
            {
                flag = inc.Select_Tab("Affected CIs");
                if (flag)
                {
                    string temp = Base.GData("AffectedCI");
                    flag = inc.Add_Related_Members("Affected CIs", temp);
                    if (!flag)
                    {
                        error = "Cannot add Affected CI";
                    }
                    else
                    {
                        inc.WaitLoading();
                    }
                }                      
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_01_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_AttachAFile()
        {
            try
            {
                string attachmentFile = "wordAttachment.docx";
                flag = inc.Add_AttachmentFile(attachmentFile);
                Base.SwitchToPage(0);
                Base.ActiveWindow();
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Verify_AttachmentFile_INC()
        {
            try
            {
                string attachmentFile = "wordAttachment.docx";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user sda2.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    temp = inclist.List_Title().MyText;
                    flag = temp.Equals("Incidents");
                    if (flag)
                    {
                        flag = inclist.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_OpenGeneratedWO()
        {
            try
            {
                string conditions = "Number=@@WO";
                flag = inc.RelatedTableOpenRecord("Work Orders", conditions, "Number");
                if(!flag)
                {
                    error = "Error when click on record";
                }
                else
                {
                    wo.WaitLoading();
                    textbox = wo.Textbox_Number();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        textbox.Click();
                        //-- Store work order id
                        workorderId = textbox.Text;
                        Console.WriteLine("-*-[Store]: Work Order Id:(" + workorderId + ")");
                    }
                    else
                    {
                        error = "Cannot get texbox number.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_01_VerifyCICapturedFromINC()
        {
            try
            {
                string temp = Base.GData("AffectedCI");
                flag = inc.Search_Verify_RelatedTable_Row("Affected CIs", "Configuration Item", temp, "Configuration Item = " + temp);
                if (!flag)
                {
                    error = "Cannot find record as conditions.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_02_ValidateQualificationGroup()
        {
            try
            {
                string temp = Base.GData("WOQualificationGroup");
                lookup = wo.Lookup_QualificationGroup();
                flag = lookup.Existed;

                if(flag)
                {
                    if(lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect qualification group.";
                    }
                }
                else
                {
                    error = "Cannot get lookup qualification group.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void Step_018_01_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else
                {
                    error = "Cannot get lookup company.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_PopulateCallerName()
        {
            try
            {
                string temp = Base.GData("IncCaller");
                lookup = inc.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate caller value."; }
                }
                else { error = "Cannot get lookup caller."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_1_VerifyCatalogOptions()
        {
            try
            {
                string temp = Base.GData("WOCatOptions");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;

                if(flag)
                {
                    flag = combobox.VerifyActualItemsExisted(temp);
                    if(!flag)
                    {
                        error = "Category list is incorrect.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_2_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("WOCat");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if(flag)
                {
                    if(lookup.Text != incidentId || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect value. This should be incident Id.";
                        flagExit = false;
                    }

                }
                else
                {
                    error = "Cannot get textbox initiated from.";
                }
 
           }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_EnteringCorrelationId()
        {
            try
            {
                string temp = Base.GData("CorrelationID");
                textbox = wo.Textbox_CorrelationId();
                flag = textbox.Existed;

                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Correlation Id value."; }
                }
                else
                {
                    error = "Cannot get textbox correlation id.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_EnteringCorrelationId2()
        {
            try
            {
                string temp = Base.GData("CorrelationID2");
                textbox = wo.Textbox_CorrelationId2();
                flag = textbox.Existed;

                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Correlation Id 2 value."; }
                }
                else
                {
                    error = "Cannot get textbox correlation id 2.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_EnteringDescription()
        {
            try
            {
                string temp = Base.GData("WODescription");
                textarea = wo.Textarea_Description();
                flag = textarea.Existed;

                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate Description value."; }
                }
                else
                {
                    error = "Cannot get textarea description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_EnteringWorkNotes()
        {
            try
            {
                string temp = Base.GData("WOWorkNotes");
                flag = wo.Add_Worknotes(temp);
                if (flag)
                {
                    if (!flag) { error = "Cannot populate work notes value"; }
                }
                else
                {
                    error = "Cannot get textarea work notes.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_ClickReadyForQualification()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;

                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification button."; }
                }
                else { error = "Cannot get Ready for Qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_VerifyInitiatedFrom_OnWOTForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.ReadOnly;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Initiated From textbox is NOT readony. Expected: [Readonly]";
                    }

                    flag = lookup.Text.Equals(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error += "Incorrect Initiated From value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "].";
                    }
                }
                else
                {
                    error = "Cannot get textbox initiated from.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_1_VerifyCallerName()
        {
            try
            {
                /*Get Work Order Task number*/
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store work order task id
                    wotaskId = textbox.Text;
                }
                else { error = "Cannot get textbox number."; }
                //--------------------------------------------------------------
                string temp = Base.GData("IncCaller");
                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;

                if (flag)
                {
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect caller name. This should inherit incident's caller";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get lookup caller.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_2_VerifyCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;

                if (flag)
                {
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect company. This should inherit incident's company";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get lookup company.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_3_VerifyLocation()
        {
            try
            {
                string temp = Base.GData("IncLocation");
                lookup = wo.Lookup_Location();
                flag = lookup.Existed;

                if (flag)
                {
                    if (!lookup.VerifyCurrentValue(temp))
                    {
                        flag = false;
                        error = "Incorrect location. This should inherit incident's location.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get lookup location.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_02_QualifyTheTask()
        {
            try
            {
                button = wotask.Button_Qualified();
                flag = button.Existed;

                if (flag)
                {
                    button.Click(true);
                    wo.WaitLoading();
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;

                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Pending Dispatch");
                        if (!flag) { error = "Invalid state value."; }
                    }
                    else
                    {
                        error = "Cannot get combobox state.";
                    }
                }
                else
                {
                    error = "Cannot get button qualified.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_VerifyWOTaskGroup()
        {
            try
            {

                string temp = Base.GData("WOTAssigmentGroup");
                lookup = wo.Lookup_AssignmentGroup();
                flag = lookup.Existed;

                if (flag)
                {
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        error = "Incorrect Assignment group. The group should be [" + temp + "]";
                    }
                }
                else
                {
                    error = "Cannot get lookup assignment group.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_CheckFollowUpBox()
        {
            try
            {
                checkbox = wotask.Checkbox_FollowUp();
                flag = checkbox.Existed;

                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        checkbox.Click(true);
                        flag = checkbox.Checked;
                        if (!flag)
                        {
                            error = "Cannot check in follow up box.";
                        }
                    }
                    else
                    {
                        Console.WriteLine("Follow up box is already checked.");
                    }
                }
                else
                {
                    error = "Cannot get checkbox follow up.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_1_PopulateAssignedTo()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                lookup = wo.Lookup_AssignedTo();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.Select(temp);

                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else
                {
                    error = "Cannot get lookup assigned to.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_2_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_VerifyWOTaskIsAssigned()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Assigned");
                    if (!flag) { error = "Invalid state. This should be Assigned"; }
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_ImpersonateUser_Technician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_SearchAndOpenTaskUnderMyGroupWorks()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                if (flag)
                {
                    wo.WaitLoading();
                    temp = wolist.List_Title().MyText;
                    flag = temp.Equals("Tasks");
                    if (flag)
                    {
                        temp = "Active;is;true|and|Assigned to;is;" + Base.GData("WOTechnician") + "|and|State;is not;Pending|and|Assignment group;is;" + Base.GData("WOTAssigmentGroup");
                        flag = wolist.Filter(temp);
                        if (flag)
                        {
                            flag = wolist.SearchAndOpen("Number", wotaskId, "Number=" + wotaskId, "Number");
                            if (!flag) error = "Error when search and open work order task (id:" + wotaskId + ")";
                        }
                        else
                        {
                            error = "Cannot filter the condition.";
                        }
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Tasks)";
                    }
                }
                else
                {
                    error = "Cannot select My Group";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_Verify_AttachmentFile_WOT()
        {
            try
            {
                string attachmentFile = "wordAttachment.docx";
                flag = wo.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Impersonate_Support_User()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Change_Domain_If_Need()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Open_Email_Log()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emaillist.WaitLoading();

                    if (!emaillist.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_01_Filter_Email_Sent_to_Assignee()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------

                temp = "Subject;contains;" + wotaskId + "|and|Subject;contains;has been assigned" + "|and|Recipients;contains;" + Base.GData("WOTechnicianEmail");
                flag = emaillist.EmailFilter(temp);

                if (flag)
                {
                    emaillist.WaitLoading();
                }
                else
                {
                    error = "Error when filter email.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_02_OpenEmail_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------
                string conditions = "Subject=@@" + wotaskId;
                flag = emaillist.Open(conditions, "Created");
                if (!flag) error = "Cannot open email sent to work order task assignee.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_03_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_04_Verify_EmailBody_CustomerInfo()
        {
            try
            {
                string temp = "Customer name: " + Base.GData("IncCaller") + "|Business phone:|Mobile phone:|Short description: " + Base.GData("IncShortDescription") + "|Description: " + Base.GData("WODescription");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                    error = "Incorrect knowledge information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_05_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
