﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using SNow;
namespace WorkOrder
{
    [TestFixture]
    public class WO_LC_Dashboard_4
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        
        
        Login login = null;
        Home home = null;
        Incident inc = null;
        SNow.WorkOrder wo = null;
        IncidentList inclist = null;
        WorkOrderList woList = null;
        ItilList woQualifierList = null;

        //------------------------------------------------------------------
        string incidentId, workorderId, wotaskId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                inclist = new IncidentList(Base, "Incident list");
                woList = new WorkOrderList(Base, "Work Order List");
                woQualifierList = new ItilList(Base, "Work Order Qualifier List");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                flag = home.ImpersonateUser(temp);
                if (!flag) error = "Error when impersonate user Logistic coordinator.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_NavigateTo_QualifierDispatcher()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                flag = home.LeftMenuItemSelect("Field Service", "Qualifier - Dispatcher");
                if (flag)
                {
                    woQualifierList.WaitLoading();
                }
                else error = "Error when select open Qualifier - Dispatcher.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Verify_Tables_OnPage()
        {
            try
            {
                string temp = Base.GData("Tables_Name");
                string[] tablelist;
                if (temp.Contains(";"))
                {
                    tablelist = temp.Split(';');
                }
                else
                    tablelist = new string[] { temp};

                foreach(string t in tablelist)
                {
                    bool flagF = true;
                    flagF = wo.WO_Verify_Table_Title(t);
                    
                    if (!flagF && flag)
                        flag = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
