﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
namespace WorkOrder
{
    [TestFixture]
    public class WO_cancelling_task_7
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workorderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + wotaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Auto.otextbox textbox;
        Auto.olookup lookup;
        Auto.ocombobox combobox;        
        Auto.obutton button;
        Auto.otextarea textarea;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Incident inc = null;
        SNow.WorkOrder wo = null;
        SNow.IncidentList inclist = null;        
        SNow.EmailList emailList = null;
        SNow.Email email = null;
        SNow.ItilList wotasklist = null;
        SNow.WorkOrderTask wotask = null;
        SNow.snoelement ele = null;          
        //------------------------------------------------------------------
        string incidentId, workorderId, wotaskId;
        string wotShortdesc, wotDesc;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                wotask = new SNow.WorkOrderTask(Base, "Work Order Task");
                inclist = new SNow.IncidentList(Base, "Incident list");
                emailList = new SNow.EmailList(Base, "Email List");
                wotasklist = new SNow.ItilList(Base, "Task list");
                email = new SNow.Email(Base, "Email");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;
                wotShortdesc = string.Empty;
                wotDesc = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDeskAgent()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                //string tDomain = Base.GData("FullPathDomain");
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            inc.WaitLoading();
                        }
                        else { error = "Cannot populate sub category value."; }
                    }
                    else error = "Not found item (" + temp + ") in sub category list.";
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_008_PopulateLocation()
        //{
        //    try
        //    {
        //        string temp = Base.GData("IncLocation");
        //        lookup = inc.Lookup_Location;
        //        flag = lookup.Existed;
        //        if (flag)
        //        {
        //            flag = lookup.Select(temp);
        //            if (!flag) { error = "Cannot populate location value."; }               
        //        }
        //        else { error = "Cannot get Location lookup."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_PopulateAssignmentGroup_LBS()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
         [Test]
        public void Step_012_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user sda2.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_013_SystemSetting()
        //{
        //    try
        //    {
        //        flag = home.SystemSetting();
        //        if (!flag) error = "Error when config system.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    temp = inclist.List_Title().MyText;
                    flag = temp.Equals("Incidents");
                    if (flag)
                    {
                        flag = inclist.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)"; }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_CreateWorkOrder()
        {
            try
            {
                flag = inc.CreateWorkOrder();
                if (flag)
                {
                    wo.WaitLoading();                    
                }
                else { error = "Cannot click to create work order"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_VerifyInformation()
        {
            try
            {
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    //Get work order Id number
                    workorderId = textbox.Text;
                    lookup = wo.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("IncCaller");
                        if (lookup.Text == temp)
                        {
                            lookup = wo.Lookup_Location();
                            flag = lookup.Existed;
                            if (flag)
                            {
                                temp = Base.GData("IncLocation");
                                if (lookup.Text == temp)
                                {
                                    textbox = wo.Textbox_ShortDescription();
                                    flag = textbox.Existed;
                                    if (flag)
                                    {
                                        temp = Base.GData("IncShortDescription");
                                        if (textbox.Text != temp)
                                        {
                                            flagExit = false;
                                            error = "The value of short description is not correct.";
                                        }
                                    }
                                    else { error = "Cannot get short description control."; }
                                }
                                else { error = "The value of location is not correct."; flagExit = false; }
                            }
                            else { error = "Cannot get location control."; }
                        }
                        else { error = "The value of caller is not correct."; flagExit = false; }
                    }
                    else { error = "Cannot get caller control."; }
                }
                else { error = "Cannot get textbox number control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
       
        [Test]
        public void Step_017_ValidateQualificationGroup()
        {
            try
            {                
                lookup = wo.Lookup_QualificationGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WO_Qualification_Group");
                    if(lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        flag = false;
                        flagExit = false;
                        error = "The value of qualification group is not correct.";
                    }
                }
                else { error = "Cannot get lookup qualification group control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_VerifyCatalogOptions()
        {
            try
            {                
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCatOptions");
                    flag = combobox.VerifyActualItemsExisted(temp);
                    if(!flag) {error = "Category list is incorrect.";}
                }
                else { error = "Cannot get combobox category control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_PopulateCategory()
        {
            try
            {                
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCat");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if(flag)
                {
                    if(lookup.Text != incidentId || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect value. This should be incident Id.";
                    }
                }
                else { error = "Cannot get textbox initiated from control."; } 
           }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_01_ClickReadyForQualification()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification."; } 
                }
                else { error = "Cannot get ready for qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_02_VerifyInitiatedFrom_OnWOTForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.ReadOnly;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Initiated From lookup is NOT readony. Expected: [Readonly]";
                    }

                    flag = lookup.Text.Equals(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error += "Incorrect Initiated From value. Expected: [" + incidentId + "]. Runtime: [" + textbox.Text + "].";
                    }
                }
                else
                {
                    error = "Cannot get lookup initiated from.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_ClickQualified()
        {
            try
            {
                /*Get Work Order Task number*/
                textbox = wo.Textbox_Number();
                if (flag)
                {
                    wotaskId = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }
                //----------------------------------------------------------

                button = wotask.Button_Qualified();
                flag = button.Existed;                
                if (flag)
                {
                    button.Click();
                    wo.WaitLoading();
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;

                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Pending Dispatch");
                        if (!flag) { error = "The value of state is not correct."; }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else { error = "Cannot get button qualified."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_VerifyWOTaskGroup()
        {
            try
            {                
                lookup = wo.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssigmentGroup");
                    if(lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        flag = false;
                        flagExit = false;
                        error = "The value of Assignment group is not correct. The group should be ["+ temp +"]";
                    }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_PopulateAssignedTo()
        {
            try
            {                
                lookup = wo.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTechnician");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else { error = "Cannot get lookup assigned to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (flag)
                {
                    wotShortdesc = wo.Textbox_ShortDescription().MyText;
                    wotDesc = wo.Textarea_Description().MyText;
                }
                else
                { error = "Error when save work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_VerifyWOTaskIsAssigned()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Assigned");
                    if (!flag) { error = "The value of state is not correct."; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_ImpersonateUser_WOTechnician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_031_SearchAndOpenTaskAssignedToMe()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Field Service", "Assigned to me");
                if (flag)
                {
                    wotasklist.WaitLoading();
                    temp = wotasklist.List_Title().MyText;
                    flag = temp.Equals("Work Order Tasks");
                    if (flag)
                    {
                        flag = wotasklist.SearchAndOpen("Number", wotaskId, "Number=" + wotaskId, "Number");
                        if (!flag) error = "Error when search and open work order task (id:" + wotaskId + ")";
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Work Order Tasks)"; }
                }
                else { error = "Cannot select Assigned to me link"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_ClickAcceptButton()
        {
            try
            {
                button = wo.Button_Accept();
                flag = button.Existed;
                if (flag)
                {
                    button.Click(true);
                    wo.WaitLoading();
                }
                else { error = "Cannot get button accept"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_VerifyWOTState()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Accepted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_01_ClickCancelButton()
        {
            try
            {
                button = wotask.Button_CancelTask();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Error when click on Cancel button.";
                    }
                    else
                    {
                        wo.WaitLoading();
                    }
                }
                else { error = "Cannot get cancel button"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_02_Verify_WarningMessage()
        {
            try
            {
                string message = "Provide a reason for cancellation in Work notes";
                flag = wo.Verify_ExpectedErrorMessages_Existed(message);
                if (!flag)
                {
                    error = "Invalid message. Expected: [" + message + "]";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_03_AddWorkNote()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    string temp = Base.GData("WorkNote");
                    textarea = wo.Textarea_Worknotes();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate work notes value."; }
                    }
                    else { error = "Cannot get textarea work notes."; }
                }
                else { error = "Cannot click on tab (Notes)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_04_Click_CancelTaskButton_AndCancel()
        {
            try
            {
                button = wo.Button_CancelTask();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        IAlert alert = Base.Driver.SwitchTo().Alert();
                        string temp = "Warning! Are you sure you want to cancel?";
                        if (!alert.Text.Equals(temp))
                        {
                            flagExit = false;
                            error = "Invalid alert message. Runtime:(" + alert.Text + "). Expexted:(" + temp + ")";
                        }
                        alert.Dismiss();
                    }
                    else
                    {
                        error = "Error when click on Cancel button.";
                    }
                }
                else { error = "Cannot get cancel button"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_05_Click_CancelTaskButton_AndAccept()
        {
            try
            {
                button = wo.Button_CancelTask();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        IAlert alert = Base.Driver.SwitchTo().Alert();
                        string temp = "Warning! Are you sure you want to cancel?";
                        if (!alert.Text.Equals(temp))
                        {
                            flagExit = false;
                            error = "Invalid alert message. Runtime:(" + alert.Text + "). Expexted:(" + temp + ")";
                        }
                        alert.Accept();
                        wo.WaitLoading();
                    }
                    else
                    {
                        error = "Error when click on Cancel button.";
                    }
                }
                else { error = "Cannot get cancel button"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_06_VerifyWOTState()
        {
            try
            {
                //Verify Cancelled state
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Cancelled";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_GlobalSearch_WorkOrderTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------

                flag = wotask.GlobalSearchItem(wotaskId, true);

                if (flag)
                {
                    wotask.WaitLoading();
                }
                else { error = "Cannot search Work Order Task via Global Search field "; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_VerifyWOTState()
        {
            try
            {
                //Verify Cancelled state
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Cancelled";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_01_ClickWorkOrderParentButton()
        {
            try
            {
                ele = wo.Button_WO_Parent();
                flag = ele.Existed;
                if (flag)
                {
                    ele.Click(true);
                    wo.WaitLoading();
                }
                else { error = "Cannot get work order parent button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_02_VerifyWOState()
        {
            try
            {
                //Verify Cancelled state
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Cancelled";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_01_ClickInitiatedFromButton()
        {
            try
            {                
                // [Linh Le 4 Dec] update code
                ele = wo.Button_WO_Initiatedfrom();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);

                    if (flag)
                    {
                        Thread.Sleep(1000);
                        button = wo.Button_OpenRecord();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            inc.WaitLoading();
                        }
                        else
                        {
                            error = "Can not get Open Record button";
                        }

                    }
                    else
                    {
                        error = "Can not click on initiated from button";
                    }


                }
                else { error = "Cannot get initiated from button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_02_VerifyIncidentState()
        {
            try
            {
                //Verify Resolved state
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Resolved";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #region Verify emails of WOT were sent to WO Technician

        [Test]
        public void Step_040_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Search_Email()
        {
            try
            {
                //Fist email will be sent to WO Technician
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                string wotech_email = Base.GData("WOTechnician_Email");
                temp = "Subject;contains;" + wotaskId + "|and|Subject;contains;has been assigned to you|and|Recipients;contains;" + wotech_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    flag = emailList.Open("Subject=" + wotaskId + " - has been assigned to you", "Created");
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found email sent to Requested For (request submitted)";
                    }
                    else
                    { emailList.WaitLoading(); }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_01_Open_Html_Review_Body()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_02_Validate_HTML_Body_Customer_Name()
        {
            try
            {
                string temp1 = "Customer name: ";
                string temp2 = Base.GData("IncCaller");
                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer name.";
                    }
                }
                else
                {
                    error = "Customer name is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_03_Validate_HTML_Body_Business_Phone()
        {
            try
            {
                string temp1 = "Business phone: ";
                string temp2 = Base.GData("IncCaller_BusinessPhone");
                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer's Business phone.";
                    }
                }
                else
                {
                    error = "Customer's Business phone is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_04_Validate_HTML_Body_Mobile_Phone()
        {
            try
            {
                string temp1 = "Mobile phone: ";
                string temp2 = Base.GData("IncCaller_MobilePhone");

                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer's Mobile phone.";
                    }
                }
                else
                {
                    error = "Customer's Mobile phone is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_05_Validate_HTML_Body_Short_Description()
        {
            try
            {
                string temp = "Short description: " + wotShortdesc;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid WOT's Short description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_06_Validate_HTML_Body_Description()
        {
            try
            {
                string temp = "Description: " + wotDesc;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid WOT's Description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Logout()
        {
            try
            {
                home.Logout();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
        //-------------------------------------------------------------------------------------------------
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}