﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace WorkOrder
{
    [TestFixture]
    public class WO_cancelling_wo_5
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workOrderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + workOrderTaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
                
        Login login;
        Home home;
        SNow.Incident inc;
        IncidentList incList;
        SNow.WorkOrder wo;
        SNow.WorkOrderTask woTask;
        SNow.WorkOrderList woList; 
        //-----------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;        
        snobutton button = null;
        //-----------------------------
        string workOrderId;
        string incidentId;
        string workOrderTaskId;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                wo = new SNow.WorkOrder(Base, "Work Order");
                woTask = new SNow.WorkOrderTask(Base, "Work Order Task");
                woList = new SNow.WorkOrderList(Base, "Work Order List");
                //-----------------------------------------------------
                workOrderId = string.Empty;
                workOrderTaskId = string.Empty;
                incidentId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else 
                {
                    login.WaitLoading();
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");                
                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDesk()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Open_NewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (!flag)
                    error = "Error when create new incident.";
                else
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Store_Incident_Id()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    incidentId = textbox.Text;
                }
                else
                {
                    error = "Canno get Number textbox";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Populate_Caller()
        {
            try
            {
                string caller = Base.GData("IncCaller");
                lookup = inc.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {                    
                    flag = lookup.Select(caller);
                    if (!flag)
                    {
                        error = "Cannot populate Caller value.";
                    }
                }
                else error = "Cannot get Caller lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_PopulateAssignmentGroup_NOTLBS()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_01_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        [Test]
        public void Step_011_02_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                    if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_CreateWorkOrder()
        {
            try
            {
                flag = inc.CreateWorkOrder();
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot click to create work order"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_VerifyInheritedIformation()
        {
            try
            {
                error = "";
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Company"));
                    if (!flag) { error = "Incorrect company value."; }
                }
                else
                    error = "Cannot get lookup company.";

                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncCaller"));
                    if (!flag) { error = "Incorrect caller value."; }
                }
                else
                    error = "Cannot get lookup caller.";

                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;

                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(Base.GData("IncShortDescription"));
                    if (!flag) { error = "Incorrect short description value."; }
                }
                else
                    error = "Cannot get textbox short description.";

                lookup = wo.Lookup_Location();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncLocation"));
                    if (!flag) { error = "Incorrect location value."; }
                }
                else
                    error = "Cannot get lookup location.";

                if (!error.Equals(""))
                {
                    flag = false;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_VerifyCatalogOptions()
        {
            try
            {
                string temp = Base.GData("WOCatOptions");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                    {
                        error = "Category list is incorrect.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("WOCat");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid initiated from value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "]";
                    }
                }
                else
                {
                    error = "Cannot get Initiated from lookup.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Get_WorkOrderNumber()
        {
            try
            {
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    workOrderId = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_ClickReadyForQualification()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;

                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();                        
                    }
                    else { error = "Cannot click Ready for Qualification button."; }
                }
                else { error = "Cannot get Ready for Qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_Get_WorkOrderTaskNumber()
        {
            try
            {
                textbox = woTask.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    workOrderTaskId = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_VerifyInitiatedFrom_OnWOTForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.ReadOnly;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Initiated From lookup is NOT readony. Expected: [Readonly]";
                    }

                    flag = lookup.Text.Equals(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error += "Incorrect Initiated From value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "].";
                    }
                }
                else
                {
                    error = "Cannot get lookup initiated from.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_Select_AwaitingQualification()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Field Service", "Awaiting Qualification");
                if (flag)
                    woList.WaitLoading();
                else
                    error = "Error when create open (Awaiting Qualification).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_SearchAndOpenWO()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderId == null || workOrderId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order id.");
                    addPara.ShowDialog();
                    workOrderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = woList.SearchAndOpen("Number", workOrderId, "Number=" + workOrderId, "Number");
                if (!flag) error = "Error when search and open work order (id:" + workOrderId + ")";
                else
                    wo.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_AddWorkNotes()
        {
            try
            {
                textarea = wo.Textarea_Worknotes();
                flag = textarea.Existed;

                if (flag)
                {
                    string temp = Base.GData("WOWorkNotes");
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot input work notes."; }
                }
                else
                    error = "Cannot get textarea work notes.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_Click_CancelButton()
        {
            try
            {
                button = wo.Button_Cancel();
                flag = button.Existed;

                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        woList.WaitLoading();
                    }
                    else { error = "Cannot click Cancel button."; }
                }
                else
                    error = "Cannot get button cancel.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_GlobalSearch_WorkOrder()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderId == string.Empty || workOrderId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Id.");
                    addPara.ShowDialog();
                    workOrderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                flag = wo.GlobalSearchItem(workOrderId, true);
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot search Work Order via Global Search field."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_VerifyWOState_Cancelled()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    string temp = "Cancelled";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Incorrect work order state. Expected: [" + temp + "]. Actual: [" + combobox.Text + "]";
                    }
                    else
                    {
                        temp = "Complete";
                        flag = wo.CheckCurrentState(temp);
                        if (!flag)
                        {
                            error = "Incorrect current state in process bar. [" + temp + "].";
                        }
                    }
                }
                else
                    error = "Cannot get State combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Open_WorkOrderTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == string.Empty || workOrderTaskId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                string conditions = "Number=" + workOrderTaskId;
                flag = wo.RelatedTableOpenRecord("Work Order Tasks", conditions, "Number");
                if (!flag) error = "Error when search and open work order task";
                else { woTask.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_VerifyWOTState_Cancelled()
        {
            try
            {
                combobox = woTask.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    string temp = "Cancelled";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Incorrect work order state. Expected: [" + temp + "]. Actual: [" + combobox.Text + "]";
                    }
                    else
                    {
                        temp = "Complete";
                        flag = woTask.CheckCurrentState(temp);
                        if (!flag)
                        {
                            error = "Incorrect current state in process bar. [" + temp + "].";
                        }
                    }
                }
                else
                    error = "Cannot get State combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
    }
}
