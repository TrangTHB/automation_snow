﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace WorkOrder
{
    [TestFixture]
    public class WO_tech_backup_2
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workOrderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + workOrderTaskId);
            System.Console.WriteLine("Finished - Unavailable Start Date: " + unAvailStartDate);
            System.Console.WriteLine("Finished - Unavailable End Date: " + unAvailEndDate);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
           
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Incident inc = null;
        SNow.IncidentList incList = null;
        SNow.WorkOrder wo = null;
        SNow.WorkOrderTask woTask = null;
        SNow.WorkOrderList woList = null;
       
        SNow.Member member = null;

        //-----------------------------
        SNow.snotextbox textbox;
        SNow.snotextarea textarea;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snobutton button;
        
        SNow.snodatetime datetime;
        //-----------------------------
        string workOrderId;
        string incidentId;
        string workOrderTaskId;
        string unAvailStartDate, unAvailEndDate;
        string actTravelStart;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                woList = new WorkOrderList(Base, "Work Order List");
                incList = new IncidentList(Base, "Incident list");
                woTask = new WorkOrderTask(Base, "Work Order Task");
                //tasklist = new TaskList(Base, "Task list");
                //wmAvail = new WorkManagementAvailabilities(Base, "Work Management Availabilities");
                //wmAvailList = new WorkManagementAvailabilitiesList(Base, "Work Management Availabilities List");
                //globalSearch = new GlobalSearch(Base);
                member = new Member(Base);
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workOrderId = string.Empty;
                workOrderTaskId = string.Empty;
                unAvailStartDate = Base.GData("UnAvailStartDate");
                unAvailEndDate = Base.GData("UnAvailEndDate");
                //actTravelStart = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Log into SNOW*/
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDeskAgent()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("IncCompany");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {

                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_PopulateCI()
        {
            try
            {
                string temp = Base.GData("IncCI");
                lookup = inc.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot add CI."; }
                }
                else
                    error = "Cannot get lookup CI.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = inc.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText;
                    flag = temp.Equals("Incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)"; }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_CreateWorkOrder()
        {
            try
            {
                flag = inc.CreateWorkOrder();
                if (!flag)
                {
                     error = "Cannot click to create work order"; 
                }
               
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_01_VerifyInformation()
        {
            try
            {
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    //Get work order Id number
                    workOrderId = textbox.Text;
                    lookup = wo.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("IncCaller");
                        if (lookup.Text == temp)
                        {
                            lookup = wo.Lookup_Location();
                            flag = lookup.Existed;
                            if (flag)
                            {
                                temp = Base.GData("IncLocation");
                                if (lookup.Text == temp)
                                {
                                    textbox = wo.Textbox_ShortDescription();
                                    flag = textbox.Existed;
                                    if (flag)
                                    {
                                        temp = Base.GData("IncShortDescription");
                                        if (textbox.Text != temp)
                                        {
                                            flagExit = false;
                                            error = "The value of short description is not correct.";
                                        }
                                    }
                                    else { error = "Cannot get short description control."; }
                                }
                                else { error = "The value of location is not correct."; flagExit = false; }
                            }
                            else { error = "Cannot get location control."; }
                        }
                        else { error = "The value of caller is not correct."; flagExit = false; }
                    }
                    else { error = "Cannot get caller control."; }
                }
                else { error = "Cannot get textbox number control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_02_VerifyAffectedCI()
        {
            try
            {
                string temp = Base.GData("IncCI");
                flag = wo.Search_Verify_RelatedTable_Row("Affected CIs", "Configuration Item", temp, "Configuration Item=" + temp);
                if (!flag)
                {
                    error = "Error when Verify Affected CIs";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void Step_017_Add2ndCI()
        {
            try
            {
                string temp = Base.GData("WOCI");
                flag = wo.Add_Related_Members("Affected CIs", temp);
                if (!flag)
                {
                    error = "Error when add Affected CIs";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Delete2ndCI()
        {
            try
            {
                string temp = Base.GData("WOCI");
                flag = wo.Delete_Related_Members("Affected CIs", temp);
                if (!flag)
                {
                    error = "Error when delete Affected CIs";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_01_VerifyItemsInCategory()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCatOptions");
                    flag = combobox.VerifyActualItemsExisted(temp);
                    if (!flag)
                    {
                        error = "The items in the category combox is not correct.";
                    }
                }
                else { error = "Cannot get Category combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_PopulateCategory()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCat");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate category value.";
                    }
                }
                else { error = "Cannot get category combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.Text != incidentId || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect value. This should be incident Id.";
                    }
                }
                else { error = "Cannot get textbox initiated from control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_01_ClickReadyForQualification()
        {
            try
            {
                //System move to Work Order Task Page
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification button."; }
                }
                else { error = "Cannot get ready for qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_02_VerifyInitiatedFrom_OnWOTForm()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.ReadOnly;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Initiated From textbox is NOT readony. Expected: [Readonly]";
                    }

                    flag = lookup.Text.Equals(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error += "Incorrect Initiated From value. Expected: [" + incidentId + "]. Runtime: [" + textbox.Text + "].";
                    }
                }
                else
                {
                    error = "Cannot get textbox initiated from.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_00_Get_WOT_Number()
        {
            try
            {
                textbox = woTask.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store work order task id
                    workOrderTaskId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Work Order Task Id:(" + workOrderTaskId + ")");
                }
                else { error = "Cannot get textbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_01_ClickQualified()
        {
            try
            {
                //Populate Dispatch Group
                lookup = woTask.Lookup_DispatchGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTDispatchGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Could not select Dispatch Group.";
                    }
                }
                else error = "Could not get lookup Dispatch Group.";

                button = woTask.Button_Qualified();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        wo.WaitLoading();
                        Thread.Sleep(2000);
                    }
                    else { error = "Cannot click Qualified button."; }
                }
                else { error = "Cannot get Qualified butoon,."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_02_VerifyWOTState()
        {
            try
            {
                wo.WaitLoading();
                combobox = woTask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending Dispatch";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_03_PopulateScheduleStart()
        {
            try
            {
                flag = woTask.Select_Tab("Planned");
                if (flag)
                {
                    datetime = woTask.Datetime_Scheduledstart();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        string startDate = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                        flag = datetime.SetText(startDate, true);
                        if (!flag)
                        {
                            error = "Cannot populate Schedule start date";
                        }
                    }
                    else { error = "Cannot get Schedule start control."; }
                }
                else error = "Cannot select tab (Planned).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_VerifyWOTaskGroup()
        {
            try
            {
                lookup = woTask.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        error = "The value of Assignment group is not correct. The group should be [" + temp + "].";
                    }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_PopulateAssignedTo()
        {
            try
            {
                lookup = woTask.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("UnavailableTechnician");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else { error = "Cannot get lookup assigned to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_01_SaveWOTask()
        {
            try
            {
                flag = woTask.Save(true,true);
                if (!flag) { error = "Error when save work order task."; }
                else wo.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_02_VerifyErrorMessage()
        {
            try
            {
                //-- Input information Unavailable Start Date
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (unAvailStartDate == null || unAvailStartDate == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input startDate.");
                    addPara.ShowDialog();
                    unAvailStartDate = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------     
                //-- Input information Unavailable End Date
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (unAvailEndDate == null || unAvailEndDate == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input unAvailEndDate.");
                    addPara.ShowDialog();
                    unAvailEndDate = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------     
                temp = Base.GData("UnavailableTechnician");
                string errMessage = temp + " is scheduled for time off between " + unAvailStartDate + " to " + unAvailEndDate;
                flag = wo.Verify_ExpectedErrorMessages_Existed(errMessage);
                if (flag)
                {
                    temp = Base.GData("BackupTechnician");
                    errMessage = "re-assigning this ticket to: " + temp;
                    flag = wo.Verify_ExpectedErrorMessages_Existed(errMessage);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "There is no error message about the reasign ticket to backup technician";
                    }

                }
                else
                {
                    flagExit = false;
                    error = "There is no error message about the unavailable technician";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_VerifyAssignedTo()
        {
            try
            {
                wo.WaitLoading();
                lookup = woTask.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("BackupTechnician");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of Assigned To is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_ImpersonateUser_BackupTechnician()
        {
            try
            {
                string temp = Base.GData("BackupTechnician");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_GlobalSearch_WorkOrderTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && workOrderTaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                flag = woTask.GlobalSearchItem(workOrderTaskId, true);
                if (flag)
                {
                    woTask.WaitLoading();
                }
                else { error = "Cannot search Work Order Task via Global Search field "; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_01_ClickAcceptButton()
        {
            try
            {
                button = woTask.Button_Accept();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    wo.WaitLoading();
                }
                else { error = "Cannot get button accept"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_02_VerifyWOTState()
        {
            try
            {
                wo.WaitLoading();
                combobox = woTask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Accepted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_01_Populate_ActualTravelStart()
        {
            try
            {
                flag = woTask.Select_Tab("Actual");
                if (flag)
                {
                    actTravelStart = DateTime.Now.AddDays(4).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = woTask.Datetime_ActualTravelStart();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(actTravelStart, true);
                        if (!flag)
                        {
                            error = "Cannot populate Actual travel start value.";
                        }
                    }
                    else error = "Cannot get Actual travel start field.";
                }
                else { error = "Cannot click on tab (Actual)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_02_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Error when save work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_VerifyWOTState()
        {
            try
            {
                wo.WaitLoading();
                combobox = woTask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Work in Progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_ClickClosedComplete()
        {
            try
            {
                button = woTask.Button_CloseComplete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                        string errMessage = Base.GData("ErrorMessage_32");
                        flag = wo.Verify_ExpectedErrorMessages_Existed(errMessage);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "There is no error message about the mandatory fields";
                        }

                        else
                        {
                            flagExit = false;
                            error = "There is no error message about the unavailable technician";
                        }
                    }
                    else { error = "Cannot click Close complete button."; }
                }
                else { error = "Cannot get Close complete button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_01_AddWorkNote()
        {
            try
            {
                flag = woTask.Select_Tab("Notes");
                if (flag)
                {
                    button = woTask.Button_Showfields();
                    flag = button.Existed;
                    if (flag)
                    {
                        if ((button.MyElement.GetAttribute("data-original-title") != null && button.MyElement.GetAttribute("data-original-title").ToLower() == "show all journal fields") || (button.MyElement.GetAttribute("title") != null && button.MyElement.GetAttribute("title").ToLower() == "show all journal fields"))
                        {
                            flag = button.Click(true);
                            Thread.Sleep(2000);
                        }

                        if (flag)
                        {
                            string temp = Base.GData("WorkNote");
                            textarea = woTask.Textarea_Worknotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                flag = textarea.SetText(temp);
                                if (!flag) { error = "Cannot populate work notes value."; }
                            }
                            else
                                error = "Cannot get textarea work notes.";
                        }
                        else { error = "Cannot click on button show fields."; }
                    }
                    else { error = "Cannot get button show fields."; }
                }
                else error = "Cannot select tab (Notes).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_02_UpdateCIUpdateStatus()
        {
            try
            {
                flag = woTask.Select_Tab("Closure Information");
                if (flag)
                {
                    string temp = Base.GData("CIUpdateStatus");
                    combobox = wo.Combobox_CIUpdate_Status();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            wo.WaitLoading();
                        }
                        else { error = "Cannot select CI Status item."; }
                    }
                    else { error = "Cannot get CI Update Status."; }
                }
                else error = "Cannot select tab (Closure Information).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_03_InputCloseNotes()
        {
            try
            {
                string temp = Base.GData("CloseNotes");
                textarea = woTask.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot input Close Notes"; }
                }
                else { error = "Cannot get Close Notes field."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_04_ClickClosedComplete()
        {
            try
            {
                button = woTask.Button_CloseComplete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Close complete button."; }
                }
                else { error = "Cannot get Close complete button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_05_GlobalSearch_WorkOrderTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && workOrderTaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------

                flag = woTask.GlobalSearchItem(workOrderTaskId, true);
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot search Work Order Task via Global Search field "; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_06_VerifyWOTState()
        {
            try
            {
                Thread.Sleep(2000);
                wo.WaitLoading();
                combobox = woTask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_GlobalSearch_WorkOrder()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && workOrderId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order Id.");
                    addPara.ShowDialog();
                    workOrderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------

                flag = wo.GlobalSearchItem(workOrderId, true);
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot search Work Order via Global Search field "; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_VerifyWOState()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_VerifyAffectedCI()
        {
            try
            {

                flag = wo.Select_Tab("Affected CIs");
                if (flag)
                {
                    string temp = Base.GData("IncCI");
                    flag = wo.Search_Verify_RelatedTable_Row("Affected CIs", "Configuration Item", temp, "Configuration Item=" + temp);
                    if (!flag)
                    {
                        error = "Error when Verify Affected CIs";
                        flagExit = false;
                    }
                }
                else { error = "Cannot click on tab (Affected CIs)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion End - Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************
    }
}
