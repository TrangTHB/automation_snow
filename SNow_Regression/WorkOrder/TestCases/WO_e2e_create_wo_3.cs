﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace WorkOrder
{
    [TestFixture]
    public class WO_e2e_create_wo_3
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workOrderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + workOrderTaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
                
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Incident inc = null;
        SNow.IncidentList incList = null;
        SNow.WorkOrder wo = null;
        SNow.WorkOrderTask woTask = null;
        SNow.WorkOrderList woList = null;
        SNow.WorkOrder_AssignedToSearch woAssigned = null;
        SNow.EmailList emailList = null;
        SNow.Email email = null;
        //-----------------------------
        SNow.snotextbox textbox;
        SNow.snotextarea textarea;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snobutton button;
        SNow.snolist list;
        SNow.snocheckbox checkbox;
        //-----------------------------
        string workOrderId;
        string incidentId;
        string workOrderTaskId;
        int newToday1, assigned1, accepted1, wip1, awaitingcustomer1;
        int newToday2, assigned2, accepted2, wip2, awaitingcustomer2;
        string retrieveData1;
        string retrieveData2;
        string wotShortdesc, wotDesc;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                wo = new SNow.WorkOrder(Base, "Work Order");
                woTask = new SNow.WorkOrderTask(Base, "Work Order Task");
                woList = new SNow.WorkOrderList(Base, "Work Order List");
                woAssigned = new SNow.WorkOrder_AssignedToSearch(Base, "Work Order Assigned_to Search");
                emailList = new SNow.EmailList(Base, "Email List");
                email = new SNow.Email(Base, "Email");

                //-----------------------------------------------------
                workOrderId = string.Empty;
                workOrderTaskId = string.Empty;
                incidentId = string.Empty;

                newToday1 = -1;
                assigned1 = -1;
                accepted1 = -1;
                wip1 = -1;
                awaitingcustomer1 = -1;

                newToday2 = -1;
                assigned2 = -1;
                accepted2 = -1;
                wip2 = -1;
                awaitingcustomer2 = -1;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else 
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");                
                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_003_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_004_OpenAllWorkOrderTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Field Service", "All Work Order Tasks");
                if (flag)
                {
                    woList.WaitLoading();
                }
                else
                    error = "Cannot select Field Service > All Work Order Tasks";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_005_RetrieveData_1()
        {
            try
            {
                string temp = Base.GData("WOTechnician1");
                //Count NewToday
                string condition = "Assigned to;is;" + temp + "|and|Created;on;Today";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    newToday1 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot filter with condition " + condition;
                }

                //Count Assigned
                condition = "Assigned to;is;" + temp + "|and|State;is;Assigned";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    assigned1 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot filter with condition " + condition;
                }

                //Count Accepted
                condition = "Assigned to;is;" + temp + "|and|State;is;Accepted";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    accepted1 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot filter with condition " + condition;
                }

                //Count Work In Progress
                condition = "Assigned to;is;" + temp + "|and|State;is;Work In Progress";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    wip1 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                }

                //Count Awaiting Customer
                condition = "Assigned to;is;" + temp + "|and|Substate(u_substate);is;Awaiting customer";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    awaitingcustomer1 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                }

                retrieveData1 = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", temp, newToday1, assigned1, accepted1, wip1, awaitingcustomer1);
                System.Console.WriteLine("Data Retrieved: " + retrieveData1);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_006_RetrieveData_2()
        {
            try
            {
                string temp = Base.GData("WOTechnician2");
                //Count NewToday
                string condition = "Assigned to;is;" + temp + "|and|Created;on;Today";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    newToday2 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot filter with condition " + condition;
                }

                //Count Assigned
                condition = "Assigned to;is;" + temp + "|and|State;is;Assigned";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    assigned2 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot filter with condition " + condition;
                }

                //Count Accepted
                condition = "Assigned to;is;" + temp + "|and|State;is;Accepted";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    accepted2 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot filter with condition " + condition;
                }

                //Count Work In Progress
                condition = "Assigned to;is;" + temp + "|and|State;is;Work In Progress";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    wip2 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                }

                //Count Awaiting Customer
                condition = "Assigned to;is;" + temp + "|and|Substate(u_substate);is;Awaiting customer";
                flag = woList.Filter(condition);
                if (flag)
                {
                    woList.WaitLoading();
                    awaitingcustomer2 = woList.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                }

                retrieveData2 = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", temp, newToday2, assigned2, accepted2, wip2, awaitingcustomer2);
                System.Console.WriteLine("Data Retrieved: " + retrieveData2);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_007_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_Impersonate_Service_Desk_Agent()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                {
                    inc.WaitLoading();
                }
                else
                    error = "Error when create new incident";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    Thread.Sleep(1000);
                    //-- Store incident id
                    incidentId = textbox.Text;

                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else { error = "Cannot get texbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            inc.WaitLoading();
                        }
                        else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_Populate_Short_Description()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_1_AttachAFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_2_Verify_AttachmentFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_03_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = inc.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_GlobalSearch_Incident()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------


                flag = inc.GlobalSearchItem(incidentId, true);
                inc.WaitLoading();

                if (!flag)
                {
                    error = "INC# is NOT found by global search";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_CreateWorkOrder()
        {
            try
            {
                flag = inc.CreateWorkOrder();
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot click to create work order"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_VerifyInformation()
        {
            try
            {
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    //Get work order Id number
                    workOrderId = textbox.Text;
                    lookup = wo.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("IncCaller");
                        if (lookup.Text == temp)
                        {
                            lookup = wo.Lookup_Location();
                            flag = lookup.Existed;
                            if (flag)
                            {
                                temp = Base.GData("IncLocation");
                                if (lookup.Text == temp)
                                {
                                    textbox = wo.Textbox_ShortDescription();
                                    flag = textbox.Existed;
                                    if (flag)
                                    {
                                        temp = Base.GData("IncShortDescription");
                                        if (textbox.Text != temp)
                                        {
                                            flagExit = false;
                                            error = "The value of short description is not correct.";
                                        }
                                    }
                                    else { error = "Cannot get short description control."; }
                                }
                                else { error = "The value of location is not correct."; flagExit = false; }
                            }
                            else { error = "Cannot get location control."; }
                        }
                        else { error = "The value of caller is not correct."; flagExit = false; }
                    }
                    else { error = "Cannot get caller control."; }
                }
                else { error = "Cannot get textbox number control."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_Verify_Initiated_From()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.Text != incidentId || lookup.Text == string.Empty)
                    {
                        flag = false;
                        error = "Incorrect value. This should be incident Id.";
                    }
                }
                else { error = "Cannot get textbox initiated from control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Verify_AttachmentFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_01_VerifyCatalogOptions()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCatOptions");
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag) { error = "Category list is incorrect."; }
                }
                else { error = "Cannot get combobox category control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_2_PopulateCategory()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOCat");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_Click_ReadyForQualification()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for qualification button."; }
                }
                else { error = "Cannot get ready for qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_Verify_Information()
        {
            try
            {
                /*Get Work Order Task Ticket number*/
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store work order task id
                    workOrderTaskId = textbox.Text;
                }
                else { error = "Cannot get textbox number."; }

                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderId == null || workOrderId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Id.");
                    addPara.ShowDialog();
                    workOrderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                /*Verify Parent field*/
                lookup = wo.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(workOrderId);
                    if (!flag)
                    {
                        error = "Cannot verify Parent value or Invalid value. Expected: " + workOrderId;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Parent lookup."; }

                /*Verify Short description field*/
                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("WODescription");
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Short description value or Invalid Short description. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Short description textbox."; }

                /*Verify Caller field*/
                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("IncCaller");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Caller value or Invalid value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Caller lookup."; }

                /*Verify Company field*/
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("Company");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Company value or Invalid value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Company lookup."; }

                /*Verify Dispatch Group field*/
                lookup = wo.Lookup_DispatchGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("DispatchGroup");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Dispatch group value or Invalid value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Dispatch group lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_01_ClickQualified()
        {
            try
            {
                button = woTask.Button_Qualified();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Qualified button."; }
                }
                else { error = "Cannot get button qualified."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------       
        [Test]
        public void Step_022_02_Verify_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending Dispatch";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify State value or Invalid value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------       

        [Test]
        public void Step_023_Validate_Qualification_Group()
        {
            try
            {
                lookup = wo.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "The value of qualification group is not correct. Expected: " + lookup.Text;
                    }
                }
                else { error = "Cannot get lookup qualification group control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Add_Skill()
        {
            try
            {
                string temp = Base.GData("Skill");
                list = wo.List_Skills();
                flag = wo.Add_List_Value(list, temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Click_Blue_AssignedTo_Button()
        {
            try
            {
                snoelement ele = woTask.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Assigned To Pick Up button."; }
                }
                else { error = "Cannot get Assigned To Pick Up button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Verify_SelectAssignee_Window()
        {
            try
            {
                string expectedColumn = Base.GData("Colheader");
                flag  = woAssigned.VerifyColumnHeader(expectedColumn, ref error, true, null, woAssigned.colDefine);
                if (!flag)
                {
                    error = "The column header is not displayed as expected. Expected: " + expectedColumn;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Choose_WOTechnician1()
        {
            try
            {
                string temp = Base.GData("WOTechnician1");
                string conditions = "Name=" + temp;
                flag = woAssigned.Open(conditions, "Name", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine);
                if (!flag)
                {
                    error = "Cannot choose WO Technician with condition: " + temp;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Save_WOTask()
        {
            try
            {
                flag = wo.Save(false, true);
                if (!flag)
                {
                    error = "Cannot save WO Task.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Verify_WOTask_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Assigned";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot verify Satate value. Expected: " + temp;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Impersonate_WOTechnician1()
        {
            try
            {
                string temp = Base.GData("WOTechnician1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Open_WorkOrderTask_List()
        {
            try
            {

                flag = home.LeftMenuItemSelect("Field Service", "Assigned to me");
                if (flag)
                {

                    if (flag)
                    {
                        woList.WaitLoading();
                    }
                }
                else { error = "Cannot select Assigned to me link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_01_Verify_ColumnHeader()
        {
            try
            {
                string temp = Base.GData("ColumnHeader");
                flag = woList.VerifyColumnHeader(temp, ref error);
                if (!flag)
                {
                    error = "Invalid column header.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_02_Add_Column()
        {
            try
            {
                string temp = Base.GData("AddColumn");
                flag = woList.Add_More_Columns(temp);
                if (!flag)
                {
                    error = "Cannot add column header.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_03_Verify_ColumnHeader_AfterAdded()
        {
            try
            {
                string temp = Base.GData("ColumnAfterAdded");
                flag = woList.VerifyColumnHeader(temp, ref error);
                if (!flag)
                {
                    error = "Invalid column header.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_04_Reset_ColumnDefaults()
        {
            try
            {
                flag = woList.ResetColumnToDefaultValue();
                if (!flag)
                {
                    error = "Cannot reset column defaults.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_05_Open_WorkOrderTask()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == null || workOrderTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + workOrderTaskId;
                flag = woList.SearchAndOpen("Number", workOrderTaskId, condition, "Number");
                if (!flag)
                {
                    error = "Cannot open Work Order Task (id: " + workOrderTaskId + " )";
                }
                else
                {
                    wo.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_Verify_AttachmentFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Verify_HaveSLAs_InParentSLAs()
        {
            try
            {
                string parent_slas = Base.GData("Parent_SLAs");
                if (parent_slas.Trim().ToLower() == "yes")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    string conditions = "Task=@@" + incidentId;
                    flag = wo.Verify_RelatedTable_Row("Parent SLAs", conditions);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "There is no Task SLAs in Parent SLAs.";
                    }
                }
                else Console.WriteLine("No need verify this step.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Click_Assigned_To_Button()
        {
            try
            {
                snoelement ele = woTask.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Assigned To Pick Up button."; }
                }
                else { error = "Cannot get Assigned To Pick Up button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_02_Verify_NewToday_Column()
        {
            try
            {
                //Input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && newToday1 == -1)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input New Today 1 value.");
                    addPara.ShowDialog();
                    newToday1 = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                string woTechnician = Base.GData("WOTechnician1");
                string conditions = "Name=" + woTechnician;
                string result = woAssigned.RelatedTableGetCell(conditions, "New Today", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine);
                int tempF = newToday1 + 1;
                if (result != tempF.ToString())
                {
                    flag = false;
                    flagExit = false;
                    error = "The value is not incorrect. Expected: " + tempF;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Close_SelectAssignee_Window()
        {
            try
            {
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_01_Click_FollowUp_Checkbox()
        {
            try
            {
                checkbox = woTask.Checkbox_FollowUp();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click();
                        if (!flag) { error = "Cannot click Follow up checkbox"; }
                    }
                    else { error = "Follow up checkbox is already checked."; }
                }
                else { error = "Cannot get Follow up checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_AddWorkNote()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (!flag)
                    error = "Cannot select 'Notes' Tab.";
                else
                {
                    string temp = Base.GData("WOTWorkNotes");
                    textarea = wo.Textarea_Worknotes();
                    
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot add work notes.";
                        }
                    }
                    else { error = "Cannot get Work Notes textarea."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Save_WOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Cannot Save Work Order Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Impersonate_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Search_and_Open_WO_Task()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == null || workOrderTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //Global Search WOT
                button = wo.Button_Search();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        textbox = wo.Textbox_Search();
                        flag = textbox.Existed;
                        //textbox.MyElement.Clear();
                        //button.Click();

                        if (flag)
                        {
                            flag = textbox.SetText(workOrderTaskId,true,true);
                            if (flag)
                            {
                                wo.WaitLoading();
                            }
                            else { error = "Error when set text into textbox search."; }
                        }
                        else { error = "Cannot get textbox search."; }
                    }
                    else { error = "Error when click on search button."; }
                }
                else { error = "Cannot get button search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Add_WorkNotes()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (!flag)
                    error = "Cannot select 'Notes' Tab.";
                else
                {
                    string temp = Base.GData("WOTWorkNotes");
                    textarea = wo.Textarea_WO_Task_Worknotes();
                    if (flag)
                    {
                        flag = textarea.SetText(temp, true, true);
                        if (!flag)
                        {
                            error = "Cannot add work notes.";
                        }
                    }
                    else { error = "Cannot get Work Notes textarea."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Uncheck_FollowUp()
        {
            try
            {
                checkbox = woTask.Checkbox_FollowUp();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Click();
                    if (!flag) { error = "Cannot click Follow up checkbox"; }
                }
                else { error = "Cannot get Follow up checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Save_WOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Cannot Save Work Order Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_Impersonate_WOTechnician2()
        {
            try
            {
                string temp = Base.GData("WOTechnician2");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_GlobalSearch_WorkOrderTask()
        {
            try
            {
                Step_045_Search_and_Open_WO_Task();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Click_AssignToMe()
        {
            try
            {
                button = woTask.Button_AssignToMe();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) { error = "Cannot click Assign to me button."; }
                    else
                    {
                        snoelement ele = woTask.Dialog_SelectAssignmentGroup(true);
                        int count = 0;
                        while (count < 5 && !ele.Existed)
                        {
                            Thread.Sleep(2000);
                            count++;
                        }
                    }
                }
                else { error = "Cannot get Assign to me button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_01_Select_AssignmentGroup()
        {
            try
            {
                lookup = woTask.Lookup_Dialog_SelectAssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");
                    lookup.MyElement.SendKeys(temp);
                    lookup.MyElement.SendKeys(Keys.Enter);
                    Thread.Sleep(2000);
                    button = woTask.Button_Dialog_OK();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            wo.WaitLoading(); Thread.Sleep(5000);
                        }
                        else { error = "Error when click OK button"; }
                    }
                    else { error = "Cannot get button OK."; }
                }
                else { error = "Cannot get lookup select assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }//-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_02_Verify_State()
        {
            try
            {
                wo.WaitLoading();
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Accepted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify State value or Invalid value. Expected: " + temp; flagExit = false; }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_03_Verify_Assignee()
        {
            try
            {
                lookup = wo.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTechnician2");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify State value or Invalid value. Expected: " + temp; flagExit = false; }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_01_Click_BlueAssignedTo_Button()
        {
            try
            {
                snoelement ele = woTask.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Assigned To Pick Up button."; }
                }
                else { error = "Cannot get Assigned To Pick Up button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_02_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_03_Verify_NewToday_Column()
        {
            try
            {
                //Input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && newToday2 == -1)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input New Today 2 value.");
                    addPara.ShowDialog();
                    newToday2 = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                string woTechnician = Base.GData("WOTechnician2");
                string conditions = "Name=" + woTechnician;
                string result = woAssigned.RelatedTableGetCell(conditions, "New Today", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine);
                int tempF = newToday2 + 1;
                if (result != tempF.ToString())
                {
                    flag = false;
                    flagExit = false;
                    error = "The value is not incorrect. Expected: " + tempF;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_01_Close_SelectAssignee_Window()
        {
            try
            {
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_02_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_01_AddWorkNote()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (!flag)
                    error = "Cannot select 'Notes' Tab.";
                else
                {
                    string temp = Base.GData("WOTWorkNotes");
                    textarea = wo.Textarea_Worknotes();
                    if (flag)
                    {
                        flag = textarea.SetText(temp, true, true);
                        if (!flag)
                        {
                            error = "Cannot add work notes.";
                        }
                    }
                    else { error = "Cannot get Work Notes textarea."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_02_Save_WOTask()
        {
            try
            {
                flag = wo.Save();
                if (flag)
                {
                    wotShortdesc = wo.Textbox_ShortDescription().Text;
                    wotDesc = wo.Textarea_Description().Text;
                }
                else { error = "Cannot Save Work Order Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Click_QuickComment()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_01_Select_Comment_1()
          {
            try
            {
                string temp = Base.GData("QuickComment1");
                string conditions = "Label=" + temp;
                
                flag = woList.Open(conditions, "Label", true,null, "thead a[class^='column_head']");
                if (!flag)
                {
                    error = "Cannot choose Quick Comment with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_02_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_03_Verify_Comment_Added_to_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment1");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_04_Click_QuickComment()
        {
            try
            {
                
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele= woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_05_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_06_Select_Comment_2()
        {
            try
            {
                string temp = Base.GData("QuickComment2");
                string conditions = "Label=" + temp;
                flag = woList.Open(conditions, "Label", true, null, "thead a[class^='column_head']");
                if (!flag)
                {
                    error = "Cannot choose WO Technician with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_07_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_08_Verify_Comment_Addedto_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment2");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_09_Click_QuickComment()
        {
            try
            {                
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_10_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_11_Select_Comment_3()
        {
            try
            {
                string temp = Base.GData("QuickComment3");
                string conditions = "Label=" + temp;
                flag = woList.Open(conditions, "Label", true, null, "thead a[class^='column_head']");
                if (!flag)
                {
                    error = "Cannot choose WO Technician with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_12_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_13_Verify_Comment_Added_to_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment3");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_14_Click_QuickComment()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_15_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_16_Select_Comment_4()
        {
            try
            {
                string temp = Base.GData("QuickComment4");
                string conditions = "Label=" + temp;
                flag = woList.Open(conditions, "Label", true, null, "thead a[class^='column_head']");
                if (!flag)
                {
                    error = "Cannot choose WO Technician with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_17_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_18_Verify_Comment_Added_to_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment4");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_19_Click_QuickComment()
        {
            try
            {
                
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_20_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_21_Select_Comment_5()
        {
            try
            {
                string temp = Base.GData("QuickComment5");
                string conditions = "Label=" + temp;
                flag = woList.Open(conditions, "Label", true, null, "thead a[class^='column_head']");
                {
                    error = "Cannot choose WO Technician with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_22_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_23_Verify_Comment_Added_to_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment5");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_24_Click_QuickComment()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_25_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_26_Select_Comment_6()
        {
            try
            {
                string temp = Base.GData("QuickComment6");
                string conditions = "Label=" + temp;
                flag = woList.Open(conditions, "Label", true, null, "thead a[class^='column_head']");
                {
                    error = "Cannot choose WO Technician with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_27_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_28_Verify_Comment_Added_to_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment6");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_29_Click_QuickComment()
        {
            try
            {
                
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    snoelement ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) { error = "Cannot click Click Quick Comment button."; }
                    }
                    else { error = "Cannot get Quick Comment lookup."; }
                }
                else { error = "Cannot select tab (Notes)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_30_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_31_Select_Comment_7()
        {
            try
            {
                string temp = Base.GData("QuickComment7");
                string conditions = "Label=" + temp;
                flag = woList.Open(conditions, "Label", true, null, "thead a[class^='column_head']");
                {
                    error = "Cannot choose WO Technician with condition: ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_32_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_33_Verify_Comment_Addedto_AdditionalComment()
        {
            try
            {
                textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("QuickComment7");
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Cannot verify Additional Commments value. Expected: " + temp; }
                }
                else { error = "Cannot get Additional Comments textarea."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_Click_StartWork_Button()
        {
            try
            {
                button = wo.Button_StartWork();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) { error = "Cannot click Start Work button."; }
                    else
                    {
                        wo.WaitLoading();
                    }
                }
                else { error = "Cannot get Start Work button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_Click_BlueAssignedTo_Button()
        {
            try
            {
                Step_037_Click_Assigned_To_Button();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_02_Verify_WIP_Column()
        {
            try
            {
                //Input information
                string temp = Base.GData("Debug");
                if (temp == "yes" && wip2 == -1)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WIP 2 value.");
                    addPara.ShowDialog();
                    wip2 = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------
                string woTechnician = Base.GData("WOTechnician2");
                string conditions = "Name=" + woTechnician;
                string result = woAssigned.RelatedTableGetCell(conditions, "Work In Progress",true,null,woAssigned.colDefine,woAssigned.rowDefine,woAssigned.cellDefine);
                int tempF = wip2 + 1;
                if (result != tempF.ToString())
                {
                    flag = false;
                    flagExit = false;
                    error = "The value is not incorrect. Expected: " + tempF;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_03_Close_SelectAssignee_Window()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_01_AddWorkNotes()
        {
            try
            {
                Step_041_02_AddWorkNote();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_02_Populate_CI_UpdateStatus()
        {
            try
            {
                
                flag = wo.Select_Tab("Closure Information");
                if (flag)
                {
                    string temp = "CI Updated";
                    combobox = wo.Combobox_CIUpdate_Status();
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select item: " + temp;
                        }
                    }
                    else { error = "Cannot get CI Updated Status combobox."; }
                }
                else { error = "Cannot select tab (Closure Information)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_03_Populate_CloseNote()
        {
            try
            {
                string temp = Base.GData("CloseNote");
                textarea = wo.Textarea_CloseNotes();
                if (flag)
                {
                    flag = textarea.SetText(temp, true);
                    if (!flag)
                    {
                        error = "Cannot populate Close note value";
                    }
                }
                else { error = "Cannot get Close note textare."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_05_Click_Closed_Complete()
        {
            try
            {
               button = wo.Button_CloseComplete();
               flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) { error = "Cannot click Closed Completed button."; }
                    else { wo.WaitLoading(); Thread.Sleep(3000); }
                }
                else { error = "Cannot get Closed Completed button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_06_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify State value or Invalid value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_Global_Search_WO()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderId == null || workOrderId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order Id.");
                    addPara.ShowDialog();
                    workOrderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //Global Search WOT

                        textbox = woTask.Textbox_Search();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            flag = textbox.SetText(workOrderId, true, true);
                            if (flag)
                            {
                                wo.WaitLoading();
                            }
                            else { error = "Error when set text into textbox search."; }
                        }
                        else { error = "Cannot get textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_Verify_WO_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify State value or Invalid value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_Global_Seach_INC()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input INCIDENT Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //Global Search INC

                        textbox = wo.Textbox_Search();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            flag = textbox.SetText(incidentId, true, true);
                            if (flag)
                            {
                                inc.WaitLoading();
                            }
                            else { error = "Error when set text into textbox search."; }
                        }
                        else { error = "Cannot get textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_Verify_Incident_State()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Resolved";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; flagExit = false; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_Verify_ClosedCode()
        {
            try
            {

                flag = wo.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = wo.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Solved (Permanently)";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag)
                        {
                            error = "Cannot verify Closed Code value. Expected: " + temp;
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get Closed Code combobox."; }
                }
                else { error = "Cannot select tab (Closure Information)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #region Verify emails of WOT were sent to WO Technician 1 and WO Technician 2

        [Test]
        public void Step_071_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_Search_First_Email()
        {
            try
            {
                //Fist email will be sent to WO Technician 1
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && workOrderTaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Task Id.");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------
                string wotech_email = Base.GData("WOTechnician1_Email");
                temp = "Subject;contains;" + workOrderTaskId + "|and|Subject;contains;has been assigned to you|and|Recipients;contains;" + wotech_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    flag = emailList.Open("Recipients=" + wotech_email, "Created");
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found email sent to Requested For (request submitted)";
                    }
                    else
                    { email.WaitLoading(); }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_01_Open_Html_Review_Body()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_02_Validate_HTML_Body_Customer_Name()
        {
            try
            {
                string temp1 = "Customer name: ";
                string temp2 = Base.GData("IncCaller");
                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer name.";
                    }
                }
                else
                {
                    error = "Customer name is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_03_Validate_HTML_Body_Business_Phone()
        {
            string temp1 = "Business phone: ";
            string temp2 = Base.GData("IncCaller_BusinessPhone");
            flag = temp2.Equals(string.Empty);
            if (!flag)
            {
                flag = email.VerifyEmailBody(temp1 + temp2);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Customer's Business phone.";
                }
            }
            else
            {
                error = "Customer's Business phone is missing, please check data file";
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_04_Validate_HTML_Body_Mobile_Phone()
        {
            try
            {
                string temp1 = "Mobile phone: ";
                string temp2 = Base.GData("IncCaller_MobilePhone");

                flag = temp2.Equals(string.Empty);
                if (!flag)
                {
                    flag = email.VerifyEmailBody(temp1 + temp2);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Customer's Mobile phone.";
                    }
                }
                else
                {
                    error = "Customer's Mobile phone is missing, please check data file";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_05_Validate_HTML_Body_Short_Description()
        {
            try
            {
                string temp = "Short description: " + wotShortdesc;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid WOT's Short description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_06_Validate_HTML_Body_Description()
        {
            try
            {
                string temp = "Description: " + wotDesc;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid WOT's Description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_Logout()
        {
            try
            {
                home.Logout();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion
        //***********************************************************************************************************************************
    }
}
