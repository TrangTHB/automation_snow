﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using SNow;
namespace WorkOrder
{
    [TestFixture]
    public class WO_asset_swivel_11
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workorderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + wotaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox = null;
        snolookup lookup;
        snocombobox combobox;
        snocheckbox checkbox;
        snobutton button;
        snotextarea textarea;
        

        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Incident inc = null;
        SNow.WorkOrder wo = null;
        IncidentList inclist = null;
        WorkOrderTask woTask = null;
        WorkOrderList woList = null;

        ItilList tasklist = null;
        //GlobalSearch globalSearch = null;
        ItilList woQualifierList = null;
        ItilList wmAssetussageslist = null;

        //------------------------------------------------------------------
        string incidentId, workorderId, wotaskId, workOrderTaskId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                inclist = new IncidentList(Base, "Incident list");
                woTask = new WorkOrderTask(Base, "Work Order Task");
                woList = new WorkOrderList(Base, "Work Order List");
                tasklist = new ItilList(Base, "Task list");
                //globalSearch = GlobalSearch(Base);
                woQualifierList = new ItilList(Base,"Work Order Qualifier List");
                wmAssetussageslist = new ItilList(Base, "Work Management Asset Usages List");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;
                workOrderTaskId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDesk()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id :(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_1_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_2_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_PopulateLocation()
        {
            try
            {
                string temp = Base.GData("IncLocation");
                lookup = inc.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty || lookup.Text == null)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate location value."; }
                    }

                }
                else
                {
                    error = "Cannot get lookup location.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_PopulateDescription()
        {
            try
            {
                string temp = Base.GData("IncDescription");
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_PopulateAssignmentGroup_NOTLBS()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    flag = inclist.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                    if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_CreateWO()
        {
            try
            {
                flag = inc.CreateWorkOrder();
                if (flag)
                {
                    wo.WaitLoading();
                }
                else { error = "Cannot click to create work order"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_VerifyInheritedIformation()
        {
            try
            {
                error = "";
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Company"));
                    if (!flag) { error = "Incorrect company value."; }
                }
                else
                    error = "Cannot get lookup company.";

                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncCaller"));
                    if (!flag) { error = "Incorrect caller value."; }
                }
                else
                    error = "Cannot get lookup caller.";

                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;

                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(Base.GData("IncShortDescription"));
                    if (!flag) { error = "Incorrect short description value."; }
                }
                else
                    error = "Cannot get textbox short description.";

                lookup = wo.Lookup_Location();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncLocation"));
                    if (!flag) { error = "Incorrect location value."; }
                }
                else
                    error = "Cannot get lookup location.";

                if (!error.Equals(""))
                {
                    flag = false;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_VerifyCatalogOptions()
        {
            try
            {
                string temp = Base.GData("WOCatOptions");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                    {
                        error = "Category list is incorrect.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("WOCat");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid initiated from value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "]";
                    }
                }
                else
                    error = "Cannot get textbox initiated from.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_ClickReadyForQualification()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;

                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification."; }
                }
                else { error = "Cannot get Ready for Qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_020_02_Open_WorkOrderTask()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (workOrderTaskId == string.Empty || workOrderTaskId == null))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Task Id.");
        //            addPara.ShowDialog();
        //            workOrderTaskId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //----------------------------------------------------------------------------------------------
        //        string conditions = "Number=" + workOrderTaskId;
        //        flag = wo.RelatedTableOpenRecord("Work Order Tasks", conditions, "Number");
        //        if (!flag) error = "Error when search and open work order task";
        //        else { woTask.WaitLoading(); }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_QualifyTheTask()
        {
            try
            {
                /* Get Work Order Task number */
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store work order task id
                    wotaskId = textbox.Text;
                }
                else { error = "Cannot get textbox number."; }
                //-----------------------------------------------------------
                button = woTask.Button_Qualified();
                flag = button.Existed;

                if (flag)
                {
                    button.Click(true);
                    wo.WaitLoading();
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;

                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Pending Dispatch");
                        if (!flag) { error = "Invalid state value."; }
                    }
                    else
                    {
                        error = "Cannot get combobox state.";
                    }
                }
                else
                {
                    error = "Cannot get button qualified.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_VerifyWOTaskGroup()
        {
            try
            {

                string temp = Base.GData("WOTAssigmentGroup");
                lookup = wo.Lookup_AssignmentGroup();
                flag = lookup.Existed;

                if (flag)
                {
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty)
                    {
                        error = "Incorrect Assignment group. The group should be [" + temp + "]";
                    }
                }
                else
                {
                    error = "Cannot get lookup assignment group.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_1_CheckFollowUpBox()
        {
            try
            {
                checkbox = woTask.Checkbox_FollowUp();
                flag = checkbox.Existed;

                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        checkbox.Click(true);
                        flag = checkbox.Checked;
                        if (!flag)
                        {
                            error = "Cannot check in follow up box.";
                        }
                    }
                    else
                    {
                        Console.WriteLine("Follow up box is already checked.");
                    }
                }
                else
                {
                    error = "Cannot get checkbox follow up.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_2_EnteringWorkNotes()
        {
            try
            {
                string temp = Base.GData("WOTWorkNotes");
                textarea = wo.Textarea_Worknotes();
                flag = textarea.Existed;

                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate work notes value"; }
                }
                else
                {
                    error = "Cannot get textarea work notes.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_1_PopulateAssignedTo()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                lookup = wo.Lookup_AssignedTo();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.Select(temp);

                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else
                {
                    error = "Cannot get lookup assigned to.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_2_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Cannot Save Work Order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_VerifyWOTaskIsAssigned()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Assigned");
                    if (!flag) { error = "Invalid state. This should be Assigned"; }
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_ImpersonateUser_Technician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                flag = home.ImpersonateUser(temp, true, Base.GData("UserFullName"));
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SearchAndOpenTaskUnderAssignedToMe()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Field Service", "Assigned to me");
                if (flag)
                {
                    tasklist.WaitLoading();
                    temp = woList.List_Title().MyText;
                    flag = temp.Equals("Work Order Tasks");
                    if (flag)
                    {
                        flag = tasklist.SearchAndOpen("Number", wotaskId, "Number=" + wotaskId, "Number");
                        if (!flag) error = "Error when search and open work order task (id:" + wotaskId + ")";
                    }
                    else
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Tasks)";
                }
                else
                    error = "Cannot select My Group";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_ClickAcceptButton()
        {
            try
            {
                button = wo.Button_Accept();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag)
                    {
                        error = "Can not click on Accept button.";
                    }
                    else { wo.WaitLoading(); }
                }
                else
                {
                    error = "Cannot get button Accept.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_VerifyWOTState()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Accepted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "The value of state is not correct. Expected: " + temp; }
                }
                else { error = "Cannot get state control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_1_AddWorkNote_WOT()
        {
            try
            {
                textarea = wo.Textarea_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTWorkNotes");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Can not set text for WOT Work Notes.";
                    }
                }
                else
                {
                    error = "Can not get textarea Work Notes.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_2_SaveWOTask()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save work order task."; }
                else { wo.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Select_AssetUsages_ClickNew()
        {
            try
            {
                // Open Asset Usages tab
                flag = wo.Select_Tab("Closure Information");
                if (flag)
                {
                    // Click on New button
                    //flag = tab.ClickNew(true);
                    //if (flag)
                    //    wo.WaitLoading();
                    //else
                    //    error = "Cannot click on New button";
                }
                else
                    error = "Cannot open Asset Usages tab";
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Fillin_AssetUsages01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WorkOrderTask Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------------------------

                #region Populate Manual Asset Tag

                textbox = wo.Textbox_AssetTag();
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("ManualAssetTag") + "01_" + wotaskId);
                    if (!flag)
                        error += "Cannot populate Manual Asset Tag value.";
                }
                else
                    error += "Cannot get textbox Manual Asset Tag.";

                #endregion
                #region Populate Manual Serial Number

                textbox = wo.Textbox_SerialNumber();
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("ManualSN") + "01_" + wotaskId);
                    if (!flag)
                        error += "Cannot populate Manual Serial Number value.";
                }
                else
                    error += "Cannot get textbox Manual Serial Number.";

                #endregion
                #region Populate Manual Model

                //textbox = wo.tex;
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("ManualModel") + "01_" + wotaskId);
                    if (!flag)
                        error += "Cannot populate Manual Model value.";
                }
                else
                    error += "Cannot get textbox Manual Model.";

                #endregion
                #region Populate Manual Type

                //combobox = wo.combob;
                if (combobox.Existed)
                {
                    flag = combobox.SelectItem(Base.GData("ManualType"));
                    if (!flag)
                    {
                        error += "Cannot populate Manual Type value.";
                    }
                }
                else
                {
                    error += "Cannot get combobox Manual Type.";
                }

                #endregion
                #region Populate Status

                //combobox = wo.com;
                if (combobox.Existed)
                {
                    flag = combobox.SelectItem(Base.GData("AU_Status"));
                    if (!flag)
                    {
                        error += "Cannot populate Asset Usages Status value.";
                    }
                }
                else
                {
                    error += "Cannot get combobox Asset Usages Status.";
                }

                #endregion

                #region Populate Asset

                //lookup = wo;
                if (lookup.Existed)
                {
                    flag = lookup.Select(Base.GData("AU_Asset01"));
                    if (!flag)
                    {
                        error += "Cannot populate Asset Ussages Asset value.";
                    }
                    else
                         Thread.Sleep(2000);
                }
                else
                {
                    error += "Cannot get lookup Asset Ussages Asset.";
                }

                #endregion
                //#region Verify auto Populate Model according to selected Asset
               
                //lookup = wo.Lookup_AssetUssages_Model;
                //if (lookup.Existed)
                //{
                //    flag = lookup.VerifyCurrentText(Base.GData("AU_Model01"));
                //    if (!flag)
                //    {
                //        error += "Asset Ussages Model value is not auto populated.";
                //    }
                //}
                //else
                //{
                //    error += "Cannot get lookup Asset Ussages Model.";
                //}

                //#endregion
                #region check on swivel box
                //checkbox = wo.Checkbox_Swivel;
                flag = checkbox.Existed;

                if (flag)
                {
                    checkbox.Click(true);
                    flag = checkbox.Checked;
                    if (!flag)
                        error = error + "Cannot check Swivel box.";
                }
                else
                    error = error + "Cannot get checkbox swivel.";
                #endregion
                #region Submit Asset Usages

                //button = wo.Button_Submit;
                flag = button.Existed;
                if (!flag)
                {
                    error += "Error when submit Asset Usages.";
                }
                else
                {
                    button.Click(true);
                    wo.WaitLoading();
                }

                #endregion

                if (error != "") { flag = false; }

            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Select_AssetUsages_ClickNew()
        {
            try
            {
                // Open Asset Usages tab
                flag = wo.Select_Tab("Closure Information");
                if (flag)
                {
                    // Click on New button
                    //flag = tab.ClickNew(true);
                    //if (flag)
                    //    wo.WaitLoading();
                    //else
                    //    error = "Cannot click on New button";
                }
                else
                    error = "Cannot open Asset Usages tab";
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_035_Fillin_AssetUsages02()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && wotaskId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input WorkOrderTask Id.");
        //            addPara.ShowDialog();
        //            wotaskId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        //----------------------------------------------------------------------------------------------

        //        #region Populate Manual Asset Tag

        //        textbox = wo.Textbox_ManualAssetTag;
        //        if (textbox.Existed)
        //        {
        //            flag = textbox.SetText(Base.GData("ManualAssetTag") + "02_" + wotaskId);
        //            if (!flag)
        //                error += "Cannot populate Manual Asset Tag value.";
        //        }
        //        else
        //            error += "Cannot get textbox Manual Asset Tag.";

        //        #endregion
        //        #region Populate Manual Serial Number

        //        textbox = wo.Textbox_ManualSerialNumber;
        //        if (textbox.Existed)
        //        {
        //            flag = textbox.SetText(Base.GData("ManualSN") + "02_" + wotaskId);
        //            if (!flag)
        //                error += "Cannot populate Manual Serial Number value.";
        //        }
        //        else
        //            error += "Cannot get textbox Manual Serial Number.";

        //        #endregion
        //        #region Populate Manual Model

        //        textbox = wo.Textbox_ManualModel;
        //        if (textbox.Existed)
        //        {
        //            flag = textbox.SetText(Base.GData("ManualModel") + "02_" + wotaskId);
        //            if (!flag)
        //                error += "Cannot populate Manual Model value.";
        //        }
        //        else
        //            error += "Cannot get textbox Manual Model.";

        //        #endregion
        //        #region Populate Manual Type

        //        combobox = wo.Combobox_ManualType;
        //        if (combobox.Existed)
        //        {
        //            flag = combobox.SelectItem(Base.GData("ManualType"));
        //            if (!flag)
        //            {
        //                error += "Cannot populate Manual Type value.";
        //            }
        //        }
        //        else
        //        {
        //            error += "Cannot get combobox Manual Type.";
        //        }

        //        #endregion
        //        #region Populate Status

        //        combobox = wo.Combobox_AssetUsages_Status;
        //        if (combobox.Existed)
        //        {
        //            flag = combobox.SelectItem(Base.GData("AU_Status"));
        //            if (!flag)
        //            {
        //                error += "Cannot populate Asset Usages Status value.";
        //            }
        //        }
        //        else
        //        {
        //            error += "Cannot get combobox Asset Usages Status.";
        //        }

        //        #endregion

        //        #region Populate Asset

        //        lookup = wo.Lookup_AssetUssages_Asset;
        //        if (lookup.Existed)
        //        {
        //            flag = lookup.Select(Base.GData("AU_Asset02"));
        //            if (!flag)
        //            {
        //                error += "Cannot populate Asset Ussages Asset value.";
        //            }
        //            else
        //                 Thread.Sleep(2000);
        //        }
        //        else
        //        {
        //            error += "Cannot get lookup Asset Ussages Asset.";
        //        }

        //        #endregion
        //        //#region Verify auto Populate Model according to selected Asset
        //        //Thread.Sleep(2000);
        //        //lookup = wo.Lookup_AssetUssages_Model;
        //        //if (lookup.Existed)
        //        //{
        //        //    flag = lookup.VerifyCurrentText(Base.GData("AU_Model02"));
        //        //    if (!flag)
        //        //    {
        //        //        error += "Asset Ussages Model value is not auto populated.";
        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    error += "Cannot get lookup Asset Ussages Model.";
        //        //}

        //        //#endregion
        //        #region check on swivel box
        //        checkbox = wo.Checkbox_Swivel;
        //        flag = checkbox.Existed;

        //        if (flag)
        //        {
        //            checkbox.Click(true);
        //            flag = checkbox.Checked;
        //            if (!flag)
        //                error = error + "Cannot check Swivel box.";
        //        }
        //        else
        //            error =error+ "Cannot get checkbox swivel.";
        //        #endregion


        //        #region Submit Asset Usages

        //        button = wo.Button_Submit;
        //        flag = button.Existed;
        //        if (!flag)
        //        {
        //            error += "Error when submit Asset Usages.";
        //        }
        //        else
        //        {
        //            button.Click(true);
        //            wo.WaitLoading();
        //        }

        //        #endregion

        //        if (error != "") { flag = false; }

        //    }
        //    catch (Exception wex)
        //    {
        //        flag = false;
        //        error = wex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate logistic coordinator user.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_037_NavigateTo_QualifierDispatcher()
        //{
        //    try
        //    {
        //        flag = home.LeftMenuItemSelect("Field Service", "Qualifier - Dispatcher");
        //        if (flag)
        //        {
        //            woQualifierList.WaitLoading();
        //            label = woQualifierList.Label_QualifierDispatcher_Title();

        //            flag = label.Existed;
        //            if (flag)
        //            {
        //                if (label.Text != "Work Management Qualifier - Dispatcher")
        //                    error = "The name of page is incorrect.";

        //            }

        //        }
        //        else
        //            error = "Cannot navigate to Qualifier - Dispatcher.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_038_ClickLink_AssetsRequiresSwivel()
        //{
        //    try
        //    {
        //        table = woQualifierList.GTable("Assets - Requires Swivel",true);
        //        woQualifierList.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_039_SearchAssetUsageRelatedWOT()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && wotaskId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input WorkOrderTask Id.");
        //            addPara.ShowDialog();
        //            wotaskId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        //----------------------------------------------------------------------------------------------

        //        temp = wmAssetussageslist.Label_Title.Text;
        //        flag = temp.Equals("Asset Usages");
        //        if (flag)
        //        {
        //                flag = wmAssetussageslist.AddColumnHeader("Service order task", 6);
        //                if (flag)
        //                {
        //                    flag = wmAssetussageslist.SearchAndVerify("Service order task","="+ wotaskId, "Service order task=" + wotaskId + "|Manual asset tag=" + Base.GData("ManualAssetTag") + "01_" + wotaskId);
        //                    if (flag)
        //                    {
        //                        flag = wmAssetussageslist.SearchAndVerify("Service order task", "=" + wotaskId, "Service order task=" + wotaskId + "|Manual asset tag=" + Base.GData("ManualAssetTag") + "02_" + wotaskId);
        //                        if (!flag)
        //                            error = "Cannot find the asset usages 02 record generated for " + wotaskId;
        //                        else
        //                        {
        //                            flag = wmAssetussageslist.SearchAndOpen("Service order task", wotaskId, "Service order task=" + wotaskId, "Service order task");
        //                            if (!flag) error = "Error when search and open work order task {id:" + wotaskId + ")";
        //                            else wo.WaitLoading();
        //                        }
        //                    }
        //                    else
        //                        error = "Cannot find the asset usages 01 record generated for " + wotaskId;
        //                }
        //                else
        //                    error = "Cannot add Service order task in list.";
        //            }    
        //        else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Asset Usages)"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_040_Uncheck_Swivel_AssetUsages01()
        //{
        //    try
        //    {
        //        // Open Asset Usages tab
        //        tab = wo.GTab("Asset Usages", true);
        //        int i = 0;
        //        while (tab == null && i < 5)
        //        {
        //            Thread.Sleep(1000);
        //            tab = wo.GTab("Asset Usages", true);
        //            i++;
        //        }
        //        flag = tab.Header.Click(true);
        //        //open asset usage record
        //        if (flag)
        //        {
        //            flag = tab.RelatedTableSearchAndOpenViewButton("Manual asset tag","="+ Base.GData("ManualAssetTag") + "01_" + wotaskId, "Manual asset tag="+Base.GData("ManualAssetTag") + "01_" + wotaskId);
        //            if (flag)
        //            {
        //                checkbox = wo.Checkbox_Swivel;
        //                flag = checkbox.Existed;

        //                if (flag)
        //                {
        //                    flag = checkbox.Checked;
        //                    if (!flag)
        //                        error = "the swivel checkbox is not checked.";
        //                    else
        //                    {
        //                        checkbox.Click();
        //                        button = wo.Button_Update;
        //                        flag = button.Existed;
        //                        if (flag)
        //                        {
        //                            button.Click(true);
        //                            wo.WaitLoading();
        //                        }
        //                        else
        //                            error = "Cannot get update button.";
        //                    }
        //                }
        //                else
        //                    error = "Cannot get swivel checkbox.";
        //            }
        //            else
        //                error = "Cannot open asset usages record 1";
        //        }
        //        else
        //            error = "Cannot open Asset Usages tab";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_041_Uncheck_Swivel_AssetUsages02()
        //{
        //    try
        //    {
        //        // Open Asset Usages tab
        //        tab = wo.GTab("Asset Usages", true);
        //        int i = 0;
        //        while (tab == null && i < 5)
        //        {
        //            Thread.Sleep(1000);
        //            tab = wo.GTab("Asset Usages", true);
        //            i++;
        //        }
        //        flag = tab.Header.Click(true);
        //        //open asset usage record
        //        if (flag)
        //        {
        //            flag = tab.RelatedTableSearchAndOpenViewButton("Manual asset tag", "="+Base.GData("ManualAssetTag") + "02_" + wotaskId,"Manual asset tag="+ Base.GData("ManualAssetTag") + "02_" + wotaskId);
        //            if (flag)
        //            {
        //                checkbox = wo.Checkbox_Swivel;
        //                flag = checkbox.Existed;

        //                if (flag)
        //                {
        //                    flag = checkbox.Checked;
        //                    if (!flag)
        //                        error = "the swivel checkbox is not checked.";
        //                    else
        //                    {
        //                        checkbox.Click();
        //                        button = wo.Button_Update;
        //                        flag = button.Existed;
        //                        if (flag)
        //                        {
        //                            button.Click(true);
        //                            wo.WaitLoading();
        //                        }
        //                        else
        //                            error = "Cannot get update button.";
        //                    }
        //                }
        //                else
        //                    error = "Cannot get swivel checkbox.";
        //            }
        //            else
        //                error = "Cannot open asset usages 2.";
        //        }
        //        else
        //            error = "Cannot open Asset Usages tab";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_042_01_BackTo_AssetUsageList()
        //{
        //    try
        //    {
        //        button = wo.Button_Back;
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            button.Click(true);
        //            wmAssetussageslist.WaitLoading();
        //        }   
        //        else
        //            error = "Cannot get back button.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_042_02_VerifyNoSwivelRecordRelatedWOT()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && wotaskId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input WorkOrderTask Id.");
        //            addPara.ShowDialog();
        //            wotaskId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        //----------------------------------------------------------------------------------------------

        //        temp = wmAssetussageslist.Label_Title.Text;
        //        flag = temp.Equals("Asset Usages");
        //        if (flag)
        //        {

        //            bool cflag = wmAssetussageslist.SearchAndVerify("Service order task", "=" + wotaskId, "Service order task=" + wotaskId);
        //                if (cflag)
        //                {
        //                    error = "There is still have asset usage which swivel chekeck, related to WOT.";
        //                    flag = false;
        //                }
        //                else
        //                    System.Console.WriteLine("-*-Passed: There is no asset ussage related to WOT -  "+wotaskId+ " that required swivel.");
                            
        //        }
        //        else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Asset Usages)"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_043_Logout()
        //{
        //    try
        //    {
        //        flag = home.Logout();
        //        if (!flag)
        //        {
        //            error = "Error when logout system.";
        //        }
        //        else
        //            login.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
