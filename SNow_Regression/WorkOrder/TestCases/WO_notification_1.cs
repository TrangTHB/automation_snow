﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
namespace WorkOrder
{
    [TestFixture]
    public class WO_notification_1
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workorderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + wotaskId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Auto.otextbox textbox;
        Auto.olookup lookup;
        Auto.ocombobox combobox;
        Auto.obutton button;
        
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Incident inc = null;
        SNow.WorkOrder wo = null;
        WorkOrderList wotList = null;
        WorkOrderTask woTask = null;
        IncidentList inclist = null;
        //TaskList tasklist = null;
        EmailList emaillist = null;
        //------------------------------------------------------------------
        string incidentId, workorderId, wotaskId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                wotList = new WorkOrderList(Base, "Work Order List");
                woTask = new WorkOrderTask(Base, "Work Order Task");
                inclist = new IncidentList(Base, "Incident list");
                //tasklist = new Auto.TaskList(Base, "Task list");
                emaillist = new EmailList(Base,"Email List");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Stored]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_Populate_Sub_Category()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Populate_Location()
        {
            try
            {
                string temp = Base.GData("IncLocation");
                lookup = inc.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    if(lookup.Text != temp.Trim() || lookup.Text == string.Empty || lookup.Text == null)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate location value."; }
                    }
                    
                }
                else
                {
                    error = "Cannot get lookup location.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Populate_Short_Description()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_Populate_Assignment_Group()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //Thanh: Add this step per Rel 11.4
        [Test]
        public void Step_010_02_Populate_Assignee()
        {
            try
            {
                lookup = inc.Lookup_AssignedTo();
                flag = lookup.Existed;

                if(flag)
                {
                    flag = lookup.Select(Base.GData("IncAssignee1"));
                    if (!flag)
                    {
                        error = "Cannot select Assignee";
                    }
                }
                else
                {
                    error = "Assigned To field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Attach_A_File()
        {
            try
            {
                string attachmentFile = "wordAttachment.docx";
                flag = inc.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_Populate_More_Fields_If_Need()
        {
            try
            {
                string temp = Base.GData("Populate_More_Fields");
                if (temp.Trim().ToLower() != "no")
                {
                    flag = inc.Input_Value_For_Controls(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot populate more fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Impersonate_User_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user Logistic Coordinator.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Search_And_Open_INC()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    flag = inclist.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                    if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Create_WO()
        {
            try
            {
                flag = inc.CreateWorkOrder(true);
                if (!flag)
                {
                    error = "Can't create Work Order from context menu";
                }
                else
                {
                    wo.WaitLoading();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Validate_WO_Created()
        {
            try
            {
                string temp = wo.Textbox_Number().Text;

                if (Regex.IsMatch(temp, "WO*") == false)
                {
                    flag = false;
                    error = "Invalid work order ID OR cannot open work order form.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void Step_018_Verify_Information()
        {
            try
            {
                error = "";
                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncCaller"));
                    if (!flag) { error = "Incorrect caller value."; }
                }
                else
                    error = "Cannot get lookup caller.";

                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;

                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(Base.GData("IncShortDescription"));
                    if (!flag) { error = "Incorrect short description value."; }
                }
                else
                    error = "Cannot get textbox short description.";

                lookup = wo.Lookup_Location();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncLocation"));
                    if (!flag) { error = "Incorrect location value."; }
                }
                else
                    error = "Cannot get lookup location.";

                if (!error.Equals(""))
                {
                    flag = false;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Populate_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                flag = wo.Lookup_Company().SetText(temp, true);

                if (flag == false)
                {
                    error = "Cannot input company.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_Select_Category()
        {
            try
            {
                string temp = Base.GData("WOCat");
                flag = wo.Combobox_Category().SelectItem(temp);

                if (flag == false)
                {
                    error = "Invalid category selected.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_VerifyInitiatedFrom()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid initiated from value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "]";
                    }
                }
                else { error = "Cannot get Initiated From lookup"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_Attachment()
        {
            try
            {
                string IncAttachmentFile = "wordAttachment.docx";
                flag = wo.Add_AttachmentFile(IncAttachmentFile);

                if(!flag)
                {
                    flagExit = false;
                    error = "Attachment is not matched.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Save_WO()
        {
            try
            {
                flag = wo.Save();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_Click_ReadyForQualification_Button()
        {
            try
            {
                //Get Work Oder Id 
                textbox = wo.Textbox_Number();
                if (textbox != null)
                {
                    workorderId = textbox.Text;
                }

                 button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification."; }
                }
                else { error = "Cannot get ready for qualification button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_023_02_Populate_DispatchGroup_If_Need()
        //{
        //    try
        //    {
        //        string temp = Base.GData("DispatchGroup");
        //        lookup = wo.Lookup_DispatchGroup();
        //        if (lookup != null)
        //        {

        //           if (lookup.Text == "")
        //           {
        //                flag = lookup.SetText(temp,true);
        //                if (flag == false)
        //                {
        //                    error = "Cannot input dispatch group.";
        //                }
        //            }
        //            else
        //            {
        //                flag = false;
        //                error = "Cannot get dispatch group field";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_023_03_Select_WO_Task()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Env");
        //        if (temp.ToLower() == "devtest")
        //        {
        //            string conditions = "Number=@@WOT";

        //            flag = wo.RelatedTableOpenRecord("Work Order Tasks", conditions, "Number");
        //            if (flag)
        //            {
        //                /* Click on the Work Order Task ID of 1st result */
        //                wo.WaitLoading();
        //            }
        //            else
        //            {
        //                error = "Cannot open Work Order Tasks tab";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Qualify_WOT()
        {
            try
            {
                button = woTask.Button_Qualified();
                flag = button.Existed;

                if (flag)
                {
                    button.Click(true);
                    wo.WaitLoading();
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;

                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Pending Dispatch");
                        if (!flag) { error = "Invalid state value."; }
                    }
                    else
                    {
                        error = "Cannot get combobox state.";
                    }
                }
                else
                {
                    error = "Cannot get button qualified.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Verify_Assignment_Group()
        {
            try
            {
                lookup = wo.Lookup_AssignmentGroup();
                flag = (lookup != null);
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");
                    flag = lookup.Text.Equals(temp);
                    if (!flag)
                    {
                        error = "Assignment group does not auto populated.";
                    }
                }
                else
                {
                    error = "Assignment group does not exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Assign_To_WOTechnician()
        {
            try
            {
                lookup = wo.Lookup_AssignedTo();
                flag = (lookup != null);
                if (flag)
                {
                    string temp = Base.GData("WOTechnician");
                    flag = lookup.SetText(temp, true);
                    wo.WaitLoading();
                    if (!flag)
                    {
                        error = "Cannot input Assigned To";
                    }
                }
                else
                {
                    error = "Assigned To field does not exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Save_WOT()
        {
            try
            {
                textbox = wo.Textbox_Number();
                wotaskId = textbox.Text;
                flag = wo.Save(true);
                if (!flag)
                {
                    flag = false;
                    error = "Cannot save Work Order Task.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                if (!combobox.VerifyCurrentValue("Assigned"))
                {
                    flag = false;
                    error = "Invalid state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Verify_SLAs()
        {
            try
            {
                string temp = "SLA=@@" + Base.GData("SLA") + "|Stage=" + Base.GData("SLA_Status_21");
                flag = wo.Verify_RelatedTable_Row("Task SLAs", temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Incorrected SLAs.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Populate_Billing()
        {
            try
            {
                string temp = Base.GData("Billing");
                flag = wo.Select_Tab("Closure Information");

                lookup = woTask.Lookup_Billing();
                if (lookup != null)
                {
                    lookup.SetText(temp, true);
                    wo.WaitLoading();

                    temp = Base.GData("Contract_Number");
                    textbox = woTask.Textbox_Contractnumber();
                    if (textbox != null)
                    {
                        Thread.Sleep(2000);

                        if (textbox.Text.Equals(string.Empty))
                        {
                            flagExit = false;
                            error = "There is no associate Contract or fail to display";
                        }
                    }
                    else
                    {
                        flagExit = false;
                        error = "Cannot get Contract Number field on WOT form";
                    }
                }
                else
                {
                    flagExit = false;
                    error = "There is no 'Billing' field on WPT form so cannot input";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Save_WOT()
        {
            try
            {
                flag = wo.Save();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Verify Logistic Coordinator can view WOT in Incident
        [Test]
        public void Step_032_01_Click_Open_INC_on_Left_Nav()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                    inclist.WaitLoading();
                else
                    error = "Error when Open incident List.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Search_and_Open_INC()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    flag = inclist.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                    if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Verify_Assignee()
        {
            try
            {
                lookup = inc.Lookup_AssignedTo();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("IncAssignee1"));
                    if (!flag)
                    {
                        error = "Assignee is NOT correct. Expect ["+ Base.GData("IncAssignee1") + "]";
                    }
                }
                else
                {
                    error = "Assigned To field is NOT existed";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_04_Click_WOT_tab()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (!flag)
                    error = "Cannot select 'Notes' Tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_05_Verify_WOT_with_Assignee()
        {
            try
            {
                string conditions = "Number=@@WOT";

                flag = inc.Search_Verify_RelatedTable_Row("Work Order Task","Assigned to", "=" + Base.GData("WOTechnician"), conditions);
                if (!flag) error = "Invalid Assignee in list. Expected:(" + Base.GData("WOTechnician") + ")";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_07_Update_Assignee_on_List()
        {
            try
            {
                flag = wo.RelatedTable_Edit_Cell("Work Order Task","Assigned to = " + Base.GData("WOTechnician"), "Assigned to", Base.GData("WOT_Assignee1"));

                if (!flag)
                {
                    error = "Can't update Assignee on List";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_032_08_Remove_Assignee_on_List()
        //{
        //    try
        //    {
        //        flag = wo.RelatedTable_Edit_Cell("Work Order Task", "Assigned to = " + Base.GData("WOT_Assignee1"), "Assigned to", "");

        //        if (!flag)
        //        {
        //            error = "Can't remove Assignee on List";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //----------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_09_Add_Assignee_on_List()
        {
            try
            {
                flag = wo.RelatedTable_Edit_Cell("Work Order Task", "Assigned to = " + Base.GData("WOT_Assignee1"), "Assigned to", Base.GData("WOT_Assignee2"));

                if (!flag)
                {
                    error = "Can't add Assignee on List";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_10_Impersonate_Support_User()
        {
            try
            {
                string temp = Base.GData("SupportUser");
                flag = home.ImpersonateUser(temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Change_Domain_If_Need()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Open_Email_Log()
        {
            try
            {
               flag = home.LeftMenuItemSelect("CSC Run","Email Log");
                if (flag)
                {
                    emaillist.WaitLoading();

                    if(!emaillist.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_01_Filter_Email_Sent_to_Assignee()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------

                temp = "Subject;contains;" + wotaskId + "|and|Subject;contains;has been assigned" + "|and|Recipients;contains;" + Base.GData("AssignedEmail");
                flag = emaillist.EmailFilter(temp);

                if (flag)
                {
                    emaillist.WaitLoading();
                }
                else
                {
                    error = "Error when filter email.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_02_Verify_Notification_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && wotaskId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id.");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //----------------------------------------------------------------------------

                temp = "Subject=@@" + wotaskId;
                flag = emaillist.VerifyRow(temp);

                if (!flag)
                {
                    error = "Not found email sent to caller (opened).";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
