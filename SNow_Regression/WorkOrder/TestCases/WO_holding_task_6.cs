﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using Auto;
using System.Reflection;
using System.Threading;
using System.Collections.Generic;
using SNow;
namespace WorkOrder
{
    [TestFixture]
    public class WO_holding_task_6
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workOrderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + workOrderTaskId);
            System.Console.WriteLine("Finished - Expected data of assignee: New Today: {0} | Assigned: {1} | Accepted: {2} | Work In Progress: {3} | Awaiting Customer: {4}", newToday, assigned, accepted, wip, awaitingcustomer);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;        
        snobutton button;
        snotextarea textarea;
        snoelements elelist;
        snoelement ele;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Incident inc = null;
        SNow.WorkOrder wo = null;
        WorkOrderList wolist = null;
        WorkOrderTask wotask = null;
        

        //------------------------------------------------------------------
        string incidentId, workOrderId, workOrderTaskId;
        int newToday, assigned, accepted, wip, awaitingcustomer;
        string retrieveData;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                wo = new SNow.WorkOrder(Base, "Work Order");
                wolist = new WorkOrderList(Base, "Work Order List");
                wotask = new WorkOrderTask(Base, "Work Order Task");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                workOrderId = string.Empty;
                workOrderTaskId = string.Empty;
                //------------------------------------------------------------------
                newToday = -1;
                assigned = -1;
                accepted = -1;
                wip = -1;
                awaitingcustomer = -1;
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void PreStep_003_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_004_OpenAllWorkOrderTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Field Service", "All Work Order Tasks");
                if (flag)
                {
                    wolist.WaitLoading();
                }
                else
                    error = "Cannot select Field Service > All Work Order Tasks";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_005_RetrieveData()
        {
            try
            {
                wolist.WaitLoading();
                string temp = Base.GData("WOTechnician");
                //Count NewToday
                string condition = string.Format("Assigned to;is;{0}|and|Created;on;Today", temp);
                flag = wolist.Filter(condition);
                if (flag)
                {
                    wolist.WaitLoading();
                    newToday = wolist.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                    return;
                }
                //Count Assigned
                condition = string.Format("Assigned to;is;{0}|and|State;is;Assigned", temp);
                flag = wolist.Filter(condition);
                if (flag)
                {
                    wolist.WaitLoading();
                    assigned = wolist.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                    return;
                }
                //Count Accepted
                condition = string.Format("Assigned to;is;{0}|and|State;is;Accepted|and|Substate(u_substate);is;-- None --", temp);
                flag = wolist.Filter(condition);
                if (flag)
                {
                    wolist.WaitLoading();
                    accepted = wolist.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                    return;
                }
                //Count Work In Progress
                condition = string.Format("Assigned to;is;{0}|and|State;is;Work In Progress|and|Substate(u_substate);is;-- None --", temp);
                flag = wolist.Filter(condition);
                if (flag)
                {
                    wolist.WaitLoading();
                    wip = wolist.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                    return;
                }
                //Count Awaiting Customer
                condition = string.Format("Assigned to;is;{0}|and|State;is;Work In Progress|and|Substate(u_substate);is;Awaiting customer", temp);
                flag = wolist.Filter(condition);
                if (flag)
                {
                    wolist.WaitLoading();
                    awaitingcustomer = wolist.Table_List().RowCount;
                }
                else
                {
                    error = "Cannot search with condition " + condition;
                    return;
                }
                retrieveData = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", temp, newToday, assigned, accepted, wip, awaitingcustomer);
                System.Console.WriteLine("Data Retrieved: " + retrieveData);
               
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_006_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ServiceDesk()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_PopulateLocation()
        {
            try
            {
                string temp = Base.GData("IncLocation");
                lookup = inc.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.Text != temp.Trim() || lookup.Text == string.Empty || lookup.Text == null)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate location value."; }
                    }

                }
                else
                {
                    error = "Cannot get lookup location.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_PopulateAssignmentGroup_LBS()
        {
            try
            {
                string temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_SearchIncidentGlobalSearch()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(incidentId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_CreateWorkOrder()
        {
            try
            {
                flag = inc.CreateWorkOrder();

                if (!flag)
                {
                    error = "Invalid work order title or cannot open work order form.";
                }
                else {
                    wo.WaitLoading();
                    workOrderId = wo.Textbox_Number().Text;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_VerifyInformation()
        {
            try
            {
                string temp = Base.GData("IncCaller");
                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.MyElement.GetAttribute("value") == temp)
                    {
                        temp = Base.GData("WOLocation");
                        lookup = wo.Lookup_Location();
                        if (lookup.MyElement.GetAttribute("value") == temp)
                        {
                            temp = Base.GData("IncShortDescription");
                            textbox = wo.Textbox_ShortDescription();
                            if (textbox.MyElement.GetAttribute("value") != temp)
                            {
                                flag = false;
                                flagExit = false;
                                error = "Invalid short description information.";
                            }
                        }
                        else
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid location information.";
                        }
                    }
                    else
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid caller information.";
                    }
                }
                else
                {
                    error = "Cannot get Work Order Caller lookup.";

                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_PopulateCompany()
        {
            try
            {
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_SelectCategory()
        {
            try
            {
                string temp = Base.GData("WOCat");
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);

                    if (flag == false)
                    {
                        error = "Invalid category selected.";
                    }
                }
                else { error = "Cannot get Category combobox"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_VerifyInitiatedFrom()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && incidentId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid initiated from value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "]";
                    }
                }
                else { error = "Cannot get Initiated From lookup"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_01_ClickReadyForQualificationButton()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Ready for Qualification."; }
                }
                else { error = "Cannot get ready for qualification button."; }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_VerifyInitiatedFrom()
        {
            string temp = Base.GData("Debug");
            if (temp == "yes" && incidentId == string.Empty)
            {
                AddParameter addPara = new AddParameter("Please input incident Id.");
                addPara.ShowDialog();
                incidentId = addPara.value;
                addPara.Close();
                addPara = null;
            }
            lookup = wo.Lookup_Initiatedfrom();
            flag = lookup.Existed;
            if (flag)
            {
                flag = lookup.VerifyCurrentValue(incidentId);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid initiated from value. Expected: [" + incidentId + "]. Runtime: [" + lookup.Text + "]";
                }
            }
            else { error = "Cannot get Initiated From lookup"; }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_VerifyBlueAssignedToImageNotAvailable()
        {
            try
            {

                try { ele = wotask.GImage_AssignedToPickupFromGroup(); }
                catch { ele = null; }
                if (ele.Existed)
                {
                    flag = false;
                    flagExit = false;
                    error = "The the blue magnifying glass is available.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_ClickQualifiedButton()
        {
            try
            {
                /*Get Work Order Task number*/
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store work order task id
                    workOrderTaskId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Work Order Task Id:(" + workOrderTaskId + ")");
                }
                else { error = "Cannot get textbox number."; }
                //---------------------------------------------------------
                button = wotask.Button_Qualified();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    Thread.Sleep(5000);
                    wo.WaitLoading();
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;

                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Pending Dispatch");
                        if (!flag) { error = "The value of state is not correct."; }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else { error = "Cannot get button qualified."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_VerifyAssignmentGroup()
        {
            try
            {
                /* Get the Assignment Group field */
                lookup = wo.Lookup_AssignmentGroup();

                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");

                    /* Verify Assignment Group is populated */
                    flag = lookup.MyElement.GetAttribute("value").Equals(temp);
                    if (!flag)
                    {
                        error = "Assignment group does not auto populated.";
                    }
                }
                else
                {
                    error = "Assignment group does not exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_VerifyBlueAssignedToImageAvailable()
        {
            try
            {
                try { ele = wotask.GImage_AssignedToPickupFromGroup(); }
                catch { ele = null; }
                if (!ele.Existed)
                {
                    flag = false;
                    error = "The the blue magnifying glass is not available.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------    
        [Test]
        public void Step_024_ImpersonateUser_WOTechnician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_SearchForWOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == null || workOrderTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO Task ID");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(workOrderTaskId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_ClickOn_AssignToMe()
        {
            try
            {
                button = wotask.Button_AssignToMe();

                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    Thread.Sleep(5000);
                    wo.WaitLoading();
                }


                lookup = wo.Lookup_AssignmentGroup();

                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");
                    flag = lookup.MyElement.GetAttribute("value").Equals(temp);
                    if (!flag)
                    {
                        error = "Cannot select WOT Assignment Group";
                    }

                }

                wo.WaitLoading();
                
                if (wo.CheckCurrentState("Accepted") == false)
                {
                    flag = false;
                    error = "Progress is not Accepted";
                }
                else
                {
                    combobox = wo.Combobox_State();

                    if (!combobox.VerifyCurrentValue("Accepted"))
                    {
                        flag = false;
                        error = "State is not Accepted";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_ClickOn_StartWork()
        {
            try
            {
                button = wo.Button_StartWork();

                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    Thread.Sleep(5000);
                    wo.WaitLoading();
                }
                

                lookup = wo.Lookup_AssignmentGroup();

                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("WOTAssignmentGroup");
                    flag = lookup.MyElement.GetAttribute("value").Equals(temp);
                    if (flag)
                    {
                        button = wotask.Button_Dialog_OK();
                        button.Click();
                    }
                    else
                    {
                        error = "Cannot select WOT Assignment Group";
                        return;
                    }
                }

                wo.WaitLoading();

                if (!wo.CheckCurrentState("Work In Progress"))
                {
                    flag = false;
                    error = "Progress is not Work In Progress";
                }
                else
                {
                    combobox = wo.Combobox_State();

                    if (!combobox.VerifyCurrentValue("Work In Progress"))
                    {
                        flag = false;
                        error = "State is not Work In Progress";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_UpdateSubStateAsAwaitingCustomer()
        {
            try
            {
                combobox = wo.Combobox_Substate();

                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting customer");
                    if (!flag)
                    {
                        error = "Cannot select Substate = Awaiting Customer";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_AddCommentAndSave()
        {
            try
            {
                flag = wo.Select_Tab("Notes");
                if (flag)
                {
                    string temp = "Work Order Task Add Comment";
                    textarea = wo.Textarea_AdditionalCommentsCustomerVisible();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) 
                        { 
                            error = "Cannot populate additional comments value."; 
                        }
                        else 
                        {
                            string date = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                            snodatetime dt = wo.Datetime_Followupdate();
                            flag = dt.Existed;
                            if (flag)
                            {
                                flag = dt.SetText(date);
                                if (flag)
                                {
                                    flag = wo.Save();
                                    if (!flag)
                                        error = "Cannot save work order task";
                                }
                                else error = "Cannot populate followup date.";
                            }
                            else error = "Cannot get datetime followup date.";
                            
                        }
                    }
                }
                else { error = "Cannot click on tab (Notes)"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_Verify_Button_AssigneeInfo_NotExisted()
        {
            try
            {
                ele = wotask.GImage_AssignedToPickupFromGroup();

                flag = ele.Existed;
                if (flag)
                {
                    flag = false;
                    System.Console.WriteLine("Button assigned is existed. Expected NOT existed.");
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------    
        [Test]
        public void Step_030_ImpersonateUser_LogisticCoor()
        {
            try
            {
                string temp = Base.GData("LogisticCoor");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_SearchForWOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == null || workOrderTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO Task ID");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(workOrderTaskId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_VerifyAssigneeInfo()
        {
            try
            {
                string temp = Base.GData("Debug");
                if (temp == "yes" && newToday == -1)
                {
                    AddParameter addPara = new AddParameter("Please input New Today value.");
                    addPara.ShowDialog();
                    newToday = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                if (temp == "yes" && assigned == -1)
                {
                    AddParameter addPara = new AddParameter("Please input Assigned value.");
                    addPara.ShowDialog();
                    assigned = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                if (temp == "yes" && accepted == -1)
                {
                    AddParameter addPara = new AddParameter("Please input Accepted value.");
                    addPara.ShowDialog();
                    accepted = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                if (temp == "yes" && wip == -1)
                {
                    AddParameter addPara = new AddParameter("Please input Work in Progress value.");
                    addPara.ShowDialog();
                    wip = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                if (temp == "yes" && awaitingcustomer == -1)
                {
                    AddParameter addPara = new AddParameter("Please input Awaiting Customer value.");
                    addPara.ShowDialog();
                    awaitingcustomer = int.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                temp = Base.GData("WOTechnician");
                retrieveData = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", temp, newToday + 1, assigned, accepted, wip, awaitingcustomer + 1);
                string runtime = "";
                ele = wotask.GImage_AssignedToPickupFromGroup();

                flag = ele.Existed;
                if (flag)
                {
                    ele.Click();
                    Thread.Sleep(2000);
                    flag = Base.SwitchToPage(1);
                    if (flag)
                    {
                        Thread.Sleep(8000);
                        if (flag)
                        {
                            string eleDefine = string.Format(".//*[@id='datatable']//u[text()='{0}']/../../../../td", temp);
                            elelist = null;
                            elelist = Base.SNGObjects("elementlist", By.XPath(eleDefine), null, snobase.MainFrame);
                            if (elelist.Count != 0)
                            {
                                for (int i = 0; i < elelist.Count; i++)
                                {
                                    runtime += elelist.MyList[i].MyText.Trim();
                                    if (i != (elelist.Count - 1))
                                    {
                                        runtime += "|";
                                    }
                                }
                                System.Console.WriteLine("Run time value data for assignee: " + runtime);
                                if (runtime != retrieveData)
                                {
                                    flag = false;
                                    flagExit = false;
                                    error = string.Format("Data of assignee technican is incorrect. Expected: {0} | Run time: {1}", retrieveData, runtime);
                                }
                            }
                            else
                            {
                                flag = false;
                                error = "Cannot find data element for " + temp + " on form.";
                            }
                        }
                    }
                    else
                        error = "Cannot find Assignee Frame";
                }
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_ImpersonateUser_WOTechnician()
        {
            try
            {
                Base.SwitchToPage(0);
                string temp = Base.GData("WOTechnician");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_SearchForWOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == null || workOrderTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO Task ID");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(workOrderTaskId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_UpdateSubstateNone()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (flag)
                {
                    combobox = wo.Combobox_Substate();

                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem("-- None --");
                        if (flag)
                        {
                            flag = wo.Save();
                            if (!flag)
                            {
                                error = "Cannot save work order task";
                            }
                        }
                        else
                            error = "Cannot select option -- None --";
                    }

                }
                else
                    error = "Cannot find Work Order Task frame.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_Populate_Closure_Information()
        {
            try
            {
                flag = wotask.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = wotask.Combobox_CI_Update_Status();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem("No update required");
                        if (flag)
                        {
                            textarea = wotask.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                flag = textarea.SetText("WOT close notes");
                            }
                        }
                    }
                }
                else error = "Cannot select tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_ClickOn_CloseComplete()
        {
            try
            {
                button = wotask.Button_CloseComplete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    wotask.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_SearchForWOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderTaskId == null || workOrderTaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO Task ID");
                    addPara.ShowDialog();
                    workOrderTaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(workOrderTaskId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Verify_State_ClosedComplete()
        {
            try
            {
                combobox = wotask.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Closed Complete");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SearchForWO()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workOrderId == null || workOrderId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input WO ID");
                    addPara.ShowDialog();
                    workOrderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(workOrderId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Verify_State_ClosedComplete()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Closed Complete");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
   
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
