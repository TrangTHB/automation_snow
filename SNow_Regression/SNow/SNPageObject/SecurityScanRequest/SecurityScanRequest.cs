﻿using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System;
using System.Threading;


namespace Auto
{
    public class SecurityScanRequest : Create
    {
        obase Base = null;
        string pageName = string.Empty;

        public SecurityScanRequest(obase _base, string name):base(_base, name)
        {
            Base = _base;
            this.pageName = name;
        }

		#region combobox
		//-----------------------------------------------------------------------------------------------------------------------------------
		public ocombobox Combobox_ScanType
		{
			get { return GCombobox("scan_type", "Scan Type"); }
		}
		#endregion

		#region textbox
		//-----------------------------------------------------------------------------------------------------------------------------------      
		public otextbox Textbox_IpAddresses
		{
			get { return GTextbox("ip", "Ip Addresses"); }
		}
		//-----------------------------------------------------------------------------------------------------------------------------------      
		public otextbox Textbox_QualysAppliance
		{
			get { return GTextbox("u_qualys_appliance_label", "Qualys Appliance"); }
		}
		#endregion

		#region checkbox
		public ocheckbox Checkbox_DefaultScanner
		{
			get { return GCheckbox("u_default_scanner", "Default Scanner"); }
		}
		#endregion
	}
}
