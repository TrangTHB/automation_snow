﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    //This class is the pop-up has a table which contains all the appropriate assignee.
    //You can see this pop-up only when clicking button 'Select assignee' next to the 'Assign to' field
    public class WorkOrder_AssignedToSearch : ItilList
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;
        public string colDefine = "thead>tr>th:not([class])";
        public string rowDefine = "tbody>tr";
        public string cellDefine = "tbody>tr>td";

        #endregion End - Variables

        #region Constructor

        public WorkOrder_AssignedToSearch(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        
    }

}
