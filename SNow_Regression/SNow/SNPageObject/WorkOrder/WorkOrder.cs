﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;

namespace SNow
{
    public class WorkOrder : Itil
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public WorkOrder(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Public methods


        public snolookup Lookup_Initiatedfrom()
        {
            snolookup lookup = null;
            string define = "Initiated from";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_QualificationGroup()
        {
            snolookup lookup = null;
            string define = "Qualification group";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolist List_Skills()
        {
            snolist list = null;
            string define = "Skills";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                list = (snolist)obj;
            else
                list = new snolist(define, Base.Driver, null);
            return list;
        }

        private snoelement GetParent(string title, [Optional] bool linkclick)
        {
            snoelement parent = null;
            string define = ".//a[text()='" + title + "']";
            snoelement link = (snoelement)Base.SNGObject("link", "element", By.XPath(define), null, snobase.MainFrame);

            if (link.Existed)
            {
                parent = new snoelement("parent", Base.Driver, link.MyElement.FindElement(By.XPath("..")));
                while (parent.MyElement.TagName != "div" && !parent.MyElement.GetAttribute("id").Contains("window.com.glide.ui"))
                {
                    parent = new snoelement("parent", Base.Driver, parent.MyElement.FindElement(By.XPath("..")));
                }

                //click on link
                if (linkclick)
                {
                    link.Click(true);
                }
            }
            else
            {
                System.Console.WriteLine("***ERROR: Not found table with title [" + title + "]");
            }

            return parent;
        }

        public bool TableSelectRow(string title, string condition, string columnClick)
        {

            bool flag = true;
            snoelement parent = GetParent(title);
          
            if (flag)
            {
                //-- Sort-----
                string define = ".//thead//th[*]/span/a[text()='Number']";
                snoelement link = (snoelement)Base.SNGObject("link", "element", By.XPath(define), parent, snobase.MainFrame);
                flag = link.Existed;
                if (flag)
                {
                    snoelement pr = (snoelement)Base.SNGObject("Parent", "element", By.XPath(".."), link, snobase.MainFrame);
                    while (pr.MyElement.TagName != "th")
                    {
                        pr = (snoelement)Base.SNGObject("Parent", "element", By.XPath(".."), pr, snobase.MainFrame);
                    }
                    if (pr.MyElement.GetAttribute("sort_dir") != "DESC")
                    {
                        link.Click();
                        Thread.Sleep(5000);
                    }
                }
                //--------

                ItilList list = new ItilList(Base, "list", parent);
                flag = list.Open(condition, columnClick, false, "table table[id$='_table']", "thead>tr[id^='hdr'] a[class^='column_head']");               
            }
            return flag;
        }

        public bool WO_Verify_Table_Title(string title)
        {
            bool flag = false;
            string define = ".//a[text()='" + title + "']";
            snoelement link = (snoelement)Base.SNGObject("", "link", By.XPath(define), null, snobase.MainFrame);
            if (link.Existed)
            {
                System.Console.WriteLine("***Passed: Found table with title [" + title + "]");
                flag = true;
            }
            else
            {
                System.Console.WriteLine("***ERROR: Not found table with title [" + title + "]");
            }

            return flag;
        }


        #region Button

        public snobutton Button_ReadyForQualification()
        {
            snobutton bt = null;
            string define = "Ready For Qualification";
            bt = Base.GButtonByText(define);
            return bt;
        }

       

        #endregion End_Button
        #endregion End - Public methods

        #region Image control
        

        

        


        


        


        public snoelement Button_WO_Parent()
        {
            snoelement bf = null;

            string define = "viewr.wm_task.parent";
            By by = By.Id(define);
            bf = (snoelement)Base.SNGObject("WO Parent", "element", by, null, snobase.MainFrame);
            return bf;
        }

        public snoelement Button_WO_Initiatedfrom()
        {
            snoelement bf = null;

            string define = "viewr.wm_task.initiated_from";
            By by = By.Id(define);
            bf = (snoelement)Base.SNGObject("WO Parent", "element", by, null, snobase.MainFrame);
            return bf;
        }

        public bool CheckCurrentState(string currState)
        {
            bool flag = true;
            snoelement ele = null;
            string eleDefine = ".//*[@class='process-breadcrumb process-breadcrumb-border']/li[contains(@data-state,'current')]/a";
            By by = By.XPath(eleDefine);
            ele = (snoelement)Base.SNGObject("CurrentState", "elememt", by, null, snobase.MainFrame);
            Console.WriteLine("Runtime current state progress:[" + ele.MyText + "]");
            if (ele.MyText.Trim().ToLower() != currState.Trim().ToLower())
                flag = false;
            return flag;
        }




        #endregion End Image control

        #region Combobox control
        
        public snocombobox Combobox_CIUpdate_Status()
        {
            snocombobox combobox = null;
            string define = "CI Update Status";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }
        #endregion Combobox control

        #region Button

        public snobutton Button_StartWork()
        {
            snobutton button = null;
            string define = "Start Work";
            object obj = Base.GButtonByText(define);
            if (obj != null)
                button = (snobutton)obj;
            else
                button = new snobutton(define, Base.Driver, null);
            return button;
        }

        
        #endregion

        #region Textarea

        public snotextarea Textarea_WO_Task_Worknotes()
        {
            snotextarea area = null;

            string define = "#activity-stream-textarea";
            By by = By.CssSelector(define);
            area = (snotextarea)Base.SNGObject("WO task worknotes", "textarea", by, null, snobase.MainFrame);
            return area;
        }

        #endregion End - Textarea

        #region Textbox
        public snotextbox Textbox_CorrelationId()
        {
            snotextbox textbox = null;
            string define = "Correlation Id";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_CorrelationId2()
        {
            snotextbox textbox = null;
            string define = "Correlation Id 2";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_InitiatedFrom()
        {
            snotextbox textbox = null;
            string define = "Initiated from";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        #endregion

       

    }
}
