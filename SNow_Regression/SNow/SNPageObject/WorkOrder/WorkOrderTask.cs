﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class WorkOrderTask : Itil
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public WorkOrderTask(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Public methods

        public snolookup Lookup_Billing()
        {
            snolookup lookup = null;
            string define = "Billing";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snotextbox Textbox_Contractnumber()
        {
            snotextbox textbox = null;
            string define = "Contract number";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        #endregion End - Public methods

        #region Image control

        public snoelement Button_StartTravel()
        {
            snoelement bf = null;

            string define = "start_travel";
            By by = By.Id(define);
            bf = (snoelement)Base.SNGObject("Start Travel", "element", by, null, snobase.MainFrame);
            return bf;
        }

        public bool CheckCurrentState(string currState)
        {
            bool flag = true;
            snoelement ele = null;
            string eleDefine = ".//*[@class='process-breadcrumb process-breadcrumb-border']/li[contains(@data-state,'current')]/a";
            By by = By.XPath(eleDefine);
            ele = (snoelement)Base.SNGObject("CurrentState", "elememt", by, null, snobase.MainFrame);
            Console.WriteLine("Runtime current state progress:[" + ele.MyText + "]");
            if (ele.MyText.Trim().ToLower() != currState.Trim().ToLower())
                flag = false;
            return flag;
        }

        public snoelement GImage_AssignedToPickupFromGroup()
        {
            snoelement bf = null;

            string define = "#pick_assignee_from_group";
            By by = By.CssSelector(define);
            bf = (snoelement)Base.SNGObject("Image Assigned To Pickup From Group", "element", by, null, snobase.MainFrame);
            return bf;
        }

        public snoelement Button_Skill()
        {
            snoelement bf = null;

            string define = "wm_task.skills_unlock";
            By by = By.Id(define);
            bf = (snoelement)Base.SNGObject("Unlock Skills", "element", by, null, snobase.MainFrame);
            return bf;
        }

        public snoelement OpenQuickComment()
        {
            snoelement bf = null;

            string define = "lookup.wm_task.u_work_note_comments";
            By by = By.Id(define);
            bf = (snoelement)Base.SNGObject("Suggestion", "element", by, null, snobase.MainFrame);
            return bf;
        }
        #endregion End Image control

        #region Combobox control
        public snocombobox Combobox_CI_Update_Status()
        {
            snocombobox combobox = null;
            string define = "CI Update Status";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }
        #endregion Combobox control

        #region Checkbox
        public snocheckbox Checkbox_FollowUp()
        {
            snocheckbox checkbox = null;
            string define = "Follow up";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                checkbox = (snocheckbox)obj;
            else
                checkbox = new snocheckbox(define, Base.Driver, null);
            return checkbox;
        }

        public snocheckbox Checkbox_Swivel()
        {
            snocheckbox checkbox = null;
            string define = "Swivel";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                checkbox = (snocheckbox)obj;
            else
                checkbox = new snocheckbox(define, Base.Driver, null);
            return checkbox;
        }
        #endregion

        #region Button
        public snobutton Button_AssignToMe()
        {
            snobutton bt = null;
            string define = "Assign to Me";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_AutoAssign()
        {
            snobutton button = null;
            string define = "Auto Assign";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                button = (snobutton)obj;
            else
                button = new snobutton(define, Base.Driver, null);
            return button;
        }

        public snobutton Button_Qualified()
        {
            snobutton bt = null;
            string define = "Qualified";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion

        #region Other
        public snoelement Dialog_SelectAssignmentGroup(bool noWait)
        {
            snoelement dl = null;
            string define = "table[id='window.dialog_select_assignment']";
            By by = By.CssSelector(define);
            dl = (snoelement)Base.SNGObject("Dialog manage attachments", "element", by, null, snobase.MainFrame, noWait);
            return dl;
        }

        public snolookup Lookup_Dialog_SelectAssignmentGroup()
        {

                snolookup lk = null;
                string define = "input[id^='sys_display']";
                By by = By.CssSelector(define);
                lk = (snolookup)Base.SNGObject("Dialog manage attachments", "lookup", by, Dialog_SelectAssignmentGroup(true), snobase.MainFrame);

                return lk;
        }

        public snobutton Button_Dialog_OK()
        {
                snobutton bt = null;
                string define = "button[id^='ok']";
                By by = By.CssSelector(define);
                bt = (snobutton)Base.SNGObject("Button Dialog Ok","button",by,null,snobase.MainFrame);

                return bt;
        }
        #endregion

        #region Datetime
        public snodatetime Datetime_Scheduledstart()
        {
            snodatetime datetime = null;
            string define = "Scheduled start";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_ActualTravelStart()
        {
            snodatetime datetime = null;
            string define = "Actual travel start";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_ActualWorkStart()
        {
            snodatetime datetime = null;
            string define = "Actual work start";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        #endregion End - Datetime

        #region Lookup

        public snolookup Lookup_WO_Task_Skill()
        {
            snolookup lookup = null;
            string define = "input[id$='task.skills']:not([type])";
            By by = By.CssSelector(define);
            lookup = (snolookup)Base.SNGObject("WO task worknotes", "lookup", by, null, snobase.MainFrame);
            return lookup;
        }

        #endregion End - Lookup
    }

}
