﻿using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System.Threading;


namespace SNow
{
    public class ReportList : ItilList
    {
        snobase _Base = null;
        
        public ReportList(snobase _base, string name)
            : base(_base, name)
        {
            _Base = _base;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Element_AllReport()
        {
            snoelement e = null;

            //Thanh Tran: before London css
            //string define = "all_reports";

            //Thanh Tran: After London css
            string define = "li_all_reports_tab";

            e = (snoelement)_Base.SNGObject("All", "element", By.Id(define), null, snobase.MainFrame);

            return e;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_ReportSearch()
        {
            snotextbox tb = null;

            string define = "indicatortag_complete";

            tb = (snotextbox)_Base.SNGObject("Report search", "textbox", By.Id(define),null, snobase.MainFrame);

            return tb;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Element_ReportLink(string value)
        {
            snoelement e = null;

            string define = ".linked.tabs2_section.tabs2_list.ng-binding";

            snoelements list = _Base.SNGObjects("list", By.CssSelector(define), null, snobase.MainFrame);
            if (list.Count > 0)
            {
                bool flag = false;
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.Trim().ToLower().Equals(value.ToLower()))
                    {
                        e = new snoelement("link", Base.Driver, ele.MyElement);
                        flag = true;
                        break;
                    }
                }
                if (flag)
                {
                    System.Console.WriteLine("***[Passed]: Found report (" + value + ")");
                }
                else
                {
                    System.Console.WriteLine("***[FAILED]: NOT found report (" + value + ")");
                }
            }

            return e;
        }
        #region Methods

        public bool VerifyReportTitle(string value)
        {
            bool flag = true;

            snoelement ele = (snoelement)_Base.SNGObject("link", "element", By.CssSelector("a[id^='switch']"), null, snobase.MainFrame);
            flag = ele.Existed;
            if (flag)
            {
                if (ele.MyText != "Switch to Classic UI")
                {
                    flag = ele.Click();
                }

                if (flag)
                {
                    MyWaitLoading();
                    snoelements eles = _Base.SNGObjects("list", By.CssSelector("#input-main-report-title"), null, snobase.MainFrame);

                    int count = 0;
                    while (eles.Count <= 0 && count < 2)
                    {
                        Thread.Sleep(2000);

                        eles = _Base.SNGObjects("list", By.CssSelector("#input-main-report-title"), null, snobase.MainFrame);
                        count++;
                    }

                    if (eles.Count <= 0)
                    {
                        flag = false;
                    }
                    else
                    {
                        flag = eles.MyList[0].MyElement.GetAttribute("value").Trim().ToLower().Contains(value.ToLower());
                        System.Console.WriteLine("Run time: [" + eles.MyList[0].MyElement.GetAttribute("value") + "]");
                    }
                }

            }
                return flag;
        }

        public void MyWaitLoading()
        {
           

            snoelement e = (snoelement)_Base.SNGObject("edit report", "element", By.CssSelector("h1>span[class='ng-binding']"), null, snobase.MainFrame);
            int count = 0;
            while (count < 10 && (!e.Existed || e.MyElement.Text != "Edit report"))
            {
                Thread.Sleep(2000);
                e = (snoelement)_Base.SNGObject("edit report","element", By.CssSelector("h1>span[class='ng-binding']"), null, snobase.MainFrame);
                count++;
            }
        }

        public bool Verify_D_Report(string value)
        {
            bool flag = true;
            

            string define = ".linked.tabs2_section.tabs2_list.ng-binding";

            snoelements list = _Base.SNGObjects("list", By.CssSelector(define), null, snobase.MainFrame);
            if (list.Count > 1)
            {
                int count = 0;
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.Trim().ToLower().Equals(value.ToLower()))
                    {
                        count = count + 1;
                    }
                }

                if (count > 1)
                {
                    System.Console.WriteLine("***[FAILED]: FOUND duplicated report (" + value + ")");
                    flag = false;
                }
                else
                {
                    System.Console.WriteLine("***[Passed]: Not found duplicated report (" + value + ")");
                }
            }

            return flag;
        }

        #endregion End - Methods
    }
}