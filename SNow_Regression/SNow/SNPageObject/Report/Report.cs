﻿using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System;
using System.Threading;


namespace SNow
{
    public class Report
    {
        snobase Base = null;
        string pageName = string.Empty;

        
        
        public Report(snobase _base)
        {
            Base = _base;
        }
        

        #region Methods
        public void WaitLoading([Optional] bool noCheckGui, [Optional] bool noMainFrame)
        {
            System.Console.WriteLine("Report page wait loading...");
            Base.PageWaitLoading(noCheckGui, noMainFrame);
        }
        #endregion End - Methods
    }
}
