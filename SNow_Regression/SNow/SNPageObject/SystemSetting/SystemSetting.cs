﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;

namespace SNow
{
    public class SystemSetting
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public SystemSetting(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        #region Private methods

        private snoelement GMain([Optional] bool noWait) 
        {
            snoelement ele = null;
            
            try
            {
                string define = "div[id='settings_modal'] div[class^='modal-content']";
                By by = By.CssSelector(define);
                ele = (snoelement)Base.SNGObject("Setting dialog", "element", by, null, null, noWait);
            }
            catch 
            { 
                ele = null;
                Console.WriteLine("***[ERROR]: Cannot get control <Setting dialog>");
            }

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool LeftMenuSelect(string item)
        {
            bool flag = true;

            snoelement parent = GMain();
            if (parent.Existed)
            {
                string define = "div>div[class='settings-tabs col-sm-3']>ul>li>a, div[class='sn-widget-list_v2']>span>div>span[class^='sn-widget-list']";
                By by = By.CssSelector(define);
                snoelements items = Base.SNGObjects("Left menu items", by, parent);
                flag = items.ClickOnItem(item);
            }
            else { flag = false; }

            return flag;
        }
        #endregion End - Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine("System setting page wait loading...");
            Base.PageWaitLoading(true);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Setting(string domain, [Optional] bool turnOff)
        {
            bool flag = true;

            flag = LeftMenuSelect("Forms");
            if (flag)
            {
                snobutton tabbedForms = Button_TabbedForms("input");
                if (tabbedForms.Existed)
                {
                    if (turnOff)
                    {
                        if (tabbedForms.MyElement.GetAttribute("checked") == "true")
                        {
                            tabbedForms = Button_TabbedForms("label");
                            flag = tabbedForms.Click();
                        }
                    }
                    else
                    {
                        if (tabbedForms.MyElement.GetAttribute("checked") != "true")
                        {
                            tabbedForms = Button_TabbedForms("label");
                            flag = tabbedForms.Click();
                        }
                    }

                    if (flag)
                    {
                        flag = LeftMenuSelect("General");
                        if (flag)
                        {
                            //-- Domain
                            if (domain != null && domain != string.Empty)
                            {
                                snocombobox cbDomain = Combobox_Domain();
                                if (cbDomain.Existed)
                                {
                                    if (cbDomain.MyText.ToLower() != domain.ToLower())
                                    {
                                        flag = cbDomain.SelectItem(domain);
                                    }
                                }
                                else
                                {
                                    flag = false;
                                }
                            }
                            //-- Accessibility
                            if (flag)
                            {
                                snobutton acess = Button_Accessibility_Check();
                                if (acess.Existed)
                                {
                                    if (acess.MyElement.GetAttribute("checked") == "true")
                                    {
                                        acess = Button_Accessibility_Execute();
                                        flag = acess.Click();
                                        try
                                        {
                                            IAlert alert = Base.Driver.SwitchTo().Alert();
                                            alert.Accept();
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        snobutton close = Button_Close();
                                        if (close.Existed)
                                        {
                                            flag = close.Click();
                                        }
                                    }
                                }
                                else { flag = false; }
                            }
                        }
                    }
                }
                else { flag = false; }
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Existed([Optional] bool noWait)
        {
            bool flag = true;
            snoelement title = GMain(noWait);
            flag = title.Existed;
            return flag;
        }

        #endregion End - Public methods

        #region Control methods

        #region Button

        private snobutton Button_TabbedForms(string type)
        {
            snobutton button = null;
            snoelement main = GMain();
            if (main.Existed)
            {
                string define = ".settings-tab-panels.tab-content.col-sm-9.form-horizontal>div[id$='form'] div[class='input-switch']>" + type + ", #tabpanel_form>div:nth-child(1)>div>div.col-sm-7>div>div>" + type;
                By by = By.CssSelector(define);
                button = (snobutton)Base.SNGObject("Tabbed forms", "button", by, main);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get button tabbed form.");
            }

            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Accessibility_Check()
        {
            snobutton button = null;
            snoelement main = GMain();
            if (main.Existed)
            {
                string define = ".settings-tab-panels.tab-content.col-sm-9.form-horizontal>div[id$='general'] div[class='input-switch']>input[id$='accessibility'], #tabpanel_general>div:nth-child(1)>div>div.col-sm-7>div>div>input";
                By by = By.CssSelector(define);
                button = (snobutton)Base.SNGObject("Accessibility check", "button", by, main);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get button accessibility check.");
            }

            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Accessibility_Execute()
        {
            snobutton button = null;
            snoelement main = GMain();
            if (main.Existed)
            {
                string define = ".settings-tab-panels.tab-content.col-sm-9.form-horizontal>div[id$='general'] div[class='input-switch']>label[for$='accessibility'], #tabpanel_general>div:nth-child(1)>div>div.col-sm-7>div>div>label";
                By by = By.CssSelector(define);
                button = (snobutton)Base.SNGObject("Accessibility excute", "button", by, main);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get button accessibility execute.");
            }

            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Close()
        {
            snobutton button = null;
            snoelement main = GMain();
            if (main.Existed)
            {
                string define = ".btn.close.icon-cross";
                By by = By.CssSelector(define);
                button = (snobutton)Base.SNGObject("Close", "button", by, main);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get button close.");
            }

            return button;
        }

        #endregion End - Button

        #region Combobox

        private snocombobox Combobox_Domain()
        {
            snocombobox combobox = null;
            snoelement main = GMain();
            if (main.Existed)
            {
                string define = ".col-sm-7.selector.one-control>select";
                By by = By.CssSelector(define);
                combobox = (snocombobox)Base.SNGObject("Domain", "combobox", by, main);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get button tabbed form.");
            }

            return combobox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion End - Combobox

        #endregion End - Control methods
    }
}
