﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SNow
{
    public class Member
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public Member(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        #region Private methods

        public bool AddMember(string value)
        {
            bool flag = true;
            snotextbox tb = Textbox_Search();
            flag = tb.Existed;
            if (flag)
            {
                flag = tb.SetText(value, true, true);
                Thread.Sleep(2000);
                WaitLoading();
                if (flag)
                {
                    int count = 0;
                    snocombobox left = null;

                    left = List_Left();
                    flag = left.VerifyExpectedItemsExisted(value);

                    while (count < 10 && !flag)
                    {
                        Thread.Sleep(2000);
                        left = List_Left();
                        flag = left.VerifyExpectedItemsExisted(value);
                        count++;
                    }

                    System.Console.WriteLine("-*-[Loop]: " + count + " time(s).");

                    if (flag)
                    {
                        left = List_Left();
                        flag = left.SelectItem(value);

                        if (flag)
                        {
                            snobutton bt = Button_Add();
                            flag = bt.Existed;
                            if (flag)
                            {
                                flag = bt.Click();
                                if (flag)
                                {
                                    snocombobox right = List_Right();
                                    count = 0;
                                    flag = right.VerifyExpectedItemsExisted(value);
                                    
                                    while (count < 5 && !flag)
                                    {
                                        Thread.Sleep(2000);
                                        right = List_Right();
                                        flag = right.VerifyExpectedItemsExisted(value);
                                        count++;
                                    }

                                    Console.WriteLine("-*-[Loop]: " + count + " time(s).");
                                    if (!flag) Console.WriteLine("-*-[Info]: Cannot add member to right list.");
                                }
                                else Console.WriteLine("-*-[Info]: Error when click on button add.");
                            }
                            else Console.WriteLine("-*-[Info]: Cannot get button add.");
                        }
                        else Console.WriteLine("-*-[Info]: Cannot select item (" + value + ")");
                    }
                    else Console.WriteLine("-*-[Info]: Not found item in left list (" + value + ")");
                }
                else Console.WriteLine("-*-[Info]: Cannot populate value for textbox search (" + value + ")");
            }
            else Console.WriteLine("-*-[Info]: Cannot get textbox search.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool DeleteMember(string value)
        {
            bool flag = true;
            snocombobox right = List_Right();
            int count = 0;
            flag = right.VerifyExpectedItemsExisted(value);
            while (count < 5 && !flag)
            {
                Thread.Sleep(2000);
                right = List_Right();
                flag = right.VerifyExpectedItemsExisted(value);
                count++;
                System.Console.WriteLine("-*-[Loop]: " + count + " time(s).");
            }
            if (flag)
            {
                flag = right.SelectItem(value);
                if (flag)
                {
                    snobutton bt = Button_Remove();
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click(true);
                        if (!flag)
                        { Console.WriteLine("-*-[Info]: Cannot click on button Remove."); }
                    }
                    else { Console.WriteLine("-*-[Info]: Cannot get button Remove."); }
                }
                else Console.WriteLine("-*-[Info]: Cannot select item (" + value + ")");
            }
            else Console.WriteLine("-*-[Info]: Not found item in Selected list (" + value + ")");
            return flag;
        }

        #endregion End - Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine("Member page wait loading...");
            Base.PageWaitLoading();
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_Members(string members)
        {
            bool flag = true;
            string[] arr = null;

            if (members.Contains(";"))
            {
                arr = members.Split(';');
            }
            else
            {
                arr = new string[] { members };
            }

            foreach (string v in arr)
            {
                flag = AddMember(v);
                if (!flag)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(4000);
                }
            }

            if (flag)
            {
                snobutton save = Button_Save();
                flag = save.Existed;
                if (flag)
                {
                    flag = save.Click();
                    if (!flag)
                        Console.WriteLine("-*-[Info]: Error when click on button save.");
                }
                else
                    Console.WriteLine("-*-[Info]: Cannot get button save.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Delete_Members(string members)
        {
            bool flag = true;
            string[] arr = null;

            if (members.Contains(";"))
            {
                arr = members.Split(';');
            }
            else
            {
                arr = new string[] { members };
            }

            foreach (string v in arr)
            {
                flag = DeleteMember(v);
                if (!flag)
                {
                    break;
                }
            }

            if (flag)
            {
                snobutton save = Button_Save();
                flag = save.Existed;
                if (flag)
                {
                    flag = save.Click();
                    if(!flag)
                        Console.WriteLine("-*-[Info]: Error when click on button save.");
                }
                else Console.WriteLine("-*-[Info]: Cannot get button save.");
            }

            return flag;
        }
        #endregion End - Public methods

        #region Controls

        private snotextbox Textbox_Search()
        {
            snotextbox textbox = null;
            string define = "input[id][type='search']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Search member", "textbox", by, null, snobase.MainFrame);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snocombobox List_Left()
        {
            snocombobox list = null;
            string define = "select[id$='_0']";
            By by = By.CssSelector(define);
            list = (snocombobox)Base.SNGObject("Left list", "combobox", by, null, snobase.MainFrame);
            return list;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snocombobox List_Right()
        {
            snocombobox list = null;
            string define = "select[id$='_1']";
            By by = By.CssSelector(define);
            list = (snocombobox)Base.SNGObject("Right list", "combobox", by, null, snobase.MainFrame);
            return list;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Add()
        {
            snobutton button = null;
            string define = "a[class*='icon-chevron-right']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Add", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Remove()
        {
            snobutton button = null;
            string define = "a[class*='icon-chevron-left']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Remove", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Save()
        {
            snobutton button = null;
            string define = "div[class^='row'] button[id$='save'], div[class^='row'] button[id$='ok_button']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Save", "button", by, null, snobase.MainFrame);
            return button;
        }
        public snobutton Button_Cancel()
        {
            snobutton button = null;
            string define = "div[class^='row'] button[id$='save'], div[class^='row'] button[id$='ok_button']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Cancel", "button", by, null, snobase.MainFrame);
            return button;
        }
        #endregion End - Controls
    }
}
