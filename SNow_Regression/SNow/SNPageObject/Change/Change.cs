﻿using System;
using OpenQA.Selenium;
using System.Threading;
using System.Runtime.InteropServices;

namespace SNow
{
    public class Change : Itil
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public Change(snobase obase, string name): base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Controls

        #region Textarea

        public snotextarea Textarea_Justification()
        {
            snotextarea textarea = null;
            string define = "Justification";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
       

        public snotextarea Textarea_ChangePlan()
        {
            snotextarea textarea = null;
            string define = "Change plan";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_ImplementationPlan()
        {
            snotextarea textarea = null;
            string define = "Implementation plan";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_BackoutPlan()
        {
            snotextarea textarea = null;
            string define = "Backout plan";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_TestPlan()
        {
            snotextarea textarea = null;
            string define = "Test plan";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_Approval_Comments()
        {
            snotextarea textarea = null;
            string define = "textarea[id='activity-stream-textarea'], textarea[id='activity-stream-comments-textarea']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by, null, snobase.MainFrame, true);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.MyElement.Displayed) 
                {
                    textarea = new snotextarea("Comments", Base.Driver, ele.MyElement);
                }    
            }
            return textarea;
        }

        public snotextarea Textarea_Task_Worknotes()
        {
            snotextarea textarea = null;
            string define = "textarea[id='activity-stream-textarea'],textarea[id='activity-stream-work_notes-textarea']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by, null, snobase.MainFrame, true);
            foreach (Auto.oelement ele in list.MyList)
            {
                if (ele.MyElement.Displayed)
                {
                    textarea = new snotextarea("Worknotes", Base.Driver, ele.MyElement);
                }
            }
            return textarea;
        }

        public snotextarea Textarea_Justification_NoMainFrame()
        {
            snotextarea textarea = null;
            string define = "Justification";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        #endregion End - Textarea

        #region Datetime

        public snodatetime Datetime_Planned_Start_Date()
        {
            snodatetime datetime = null;
            string define = "Planned start date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Planned_End_Date()
        {
            snodatetime datetime = null;
            string define = "Planned end date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Risk_Assessment_Last_Run()
        {
            snodatetime datetime = null;
            string define = "Risk assessment last run";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snodatetime Datetime_PlannedStartDate_NoMainFrame()
        {
            snodatetime datetime = null;
            string define = "Planned start date";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_PlannedEndDate_NoMainFrame()
        {
            snodatetime datetime = null;
            string define = "Planned end date";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Actual_Start_Date()
        {
            snodatetime datetime = null;
            string define = "Actual start date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Actual_End_Date()
        {
            snodatetime datetime = null;
            string define = "Actual end date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Planned_Outage_Start_Date()
        {
            snodatetime datetime = null;
            string define = "Planned Outage start date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Planned_Outage_End_Date()
        {
            snodatetime datetime = null;
            string define = "Planned Outage end date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Actual_Outage_Start_Date()
        {
            snodatetime datetime = null;
            string define = "Actual Outage start date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snodatetime Datetime_Actual_Outage_End_Date()
        {
            snodatetime datetime = null;
            string define = "Actual Outage end date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        #endregion End - Datetime

        #region Button

        
        public snobutton Button_FillOutRiskAssessment()
        {
            snobutton bt = null;
            string define = "Fill Out Risk Assessment";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_ExecuteRiskCalculation()
        {
            snobutton bt = null;
            string define = "Execute Risk Calculation";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_RequestApproval_RequestReview()
        {
            snobutton bt = null;
            string define = "@@Request";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Approval_Reject()
        {
            snobutton bt = null;
            string define = "Reject";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Approval_Approve()
        {
            snobutton bt = null;
            string define = "Request Approval";
            bt = Base.GButtonByText(define);
            return bt;
        }
        public snobutton Button_SubmitNoMainFrame()
        {
            snobutton bt = null;
            string define = "Submit";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Reschedule()
        {
            snobutton bt = null;
            string define = "Reschedule";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        

        public snobutton Button_CheckConflict()
        {
            snobutton bt = null;

            string define = "conflict_action_run";

            bt = (snobutton)Base.SNGObject("CheckConflict", "button", By.Id(define), null, snobase.MainFrame);

            return bt;
        }

        public snobutton Button_VerifyProposedChanges()
        {
            snobutton bt = null;
            string define = "Verify Proposed Changes";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_ApplyProposedChange()
        {
            snobutton bt = null;
            string define = "Apply Proposed Changes";
            bt = Base.GButtonByText(define);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_CloseTask()
        {
            snobutton bt = null;
            string define = "Close Task";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion End - Button

        #region combobox
        
        public snocombobox Combobox_Type()
        {
            snocombobox combobox = null;
            string define = "Type";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Category_NoMainFrame()
        {
            snocombobox combobox = null;
            string define = "Category";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_State_NoMainFrame()
        {
            snocombobox combobox = null;
            string define = "State";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Priority_NoMainFrame()
        {
            snocombobox combobox = null;
            string define = "Priority";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Urgency_NoMainFrame()
        {
            snocombobox combobox = null;
            string define = "Urgency";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Risk()
        {
            snocombobox combobox = null;
            string define = "Risk";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snobutton Button_PlannedStartDate_Calendar()
        {
            snobutton bt = null;

            string define = "button[id='change_request.start_date.ui_policy_sensitive']";
            snoelements eles = Base.SNGObjects("list", By.CssSelector(define), null, snobase.MainFrame);
            foreach(Auto.oelement ele in eles.MyList)
            {
                if (ele.Existed)
                {
                    bt = new snobutton("Planned start date calendar", Base.Driver, ele.MyElement);
                    break;
                }
            }
            
            return bt;
        }

        public snobutton Button_ActualEndDate_Calendar()
        {
            snobutton bt = null;

            string define = "button[id='change_request.work_end.ui_policy_sensitive']";
            snoelements eles = Base.SNGObjects("list", By.CssSelector(define), null, snobase.MainFrame);
            foreach (Auto.oelement ele in eles.MyList)
            {
                if (ele.Existed)
                {
                    bt = new snobutton("Actual end date calendar", Base.Driver, ele.MyElement);
                    break;
                }
            }

            return bt;
        }

        
        #endregion combobox

        #region Lookup

        public snolookup Lookup_Company_NoMainFrame()
        {
            snolookup lookup = null;
            string define = "Company";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_AssignmentGroup_NoMainFrame()
        {
            snolookup lookup = null;
            string define = "Assignment group";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        #endregion End - Lookup

        #region Textbox
        public snotextbox Textbox_ShortDescription_NoMainFrame()
        {
            snotextbox textbox = null;
            string define = "Short description";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_Number_NoMainFrame()
        {
            snotextbox textbox = null;
            string define = "Number";
            object obj = Base.GControlByLabel(define, null, false, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_Rejected_Change()
        {
            snotextbox textbox = null;
            string define = "Rejected change";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_Rescheduled_Change()
        {
            snotextbox textbox = null;
            string define = "Rescheduled change";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        #endregion End - Textbox

        #region Other

        private snoelement CHG_Dialog_Confirmation(bool noWait)
        {
            snoelement dl = null;

            string define = "div[class^='modal-dialog modal-md'] div[class^='modal-body container']";

            dl = (snoelement)Base.SNGObject("Dialog confirmation", "element", By.CssSelector(define), null, snobase.MainFrame, noWait);

            return dl;
        }

        public snoelement Filter_Proposed_Change()
        {
            snoelement ele = null;

            string define = "change_request.proposed_changegcond_filters";

            ele = (snoelement)Base.SNGObject("Proposed_Change", "element", By.Id(define), null, snobase.MainFrame);

            return ele;
        }

        #endregion End - Other

        #region Method

        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool CHG_VerifyConfirmDialog(string expectedValue, [Optional] string agree)
        {
            bool flag = true;
            snoelement dialog = CHG_Dialog_Confirmation(false);
            flag = dialog.Existed;
            if (flag)
            {
                string define = "rendered_body p";
                snoelements eles = Base.SNGObjects("Confirm text", By.CssSelector(define), dialog, snobase.MainFrame);
                string[] arr = null;
                if (expectedValue.Contains("|"))
                    arr = expectedValue.Split('|');
                else
                    arr = new string[] { expectedValue };


                foreach (string exp in arr)
                {
                    bool flagF = false;
                    foreach (Auto.oelement ele in eles.MyList)
                    {
                        System.Console.WriteLine("-*-Runtime:(" + ele.MyText + ")");
                        if (ele.MyText.Trim().ToLower().Equals(exp.Trim().ToLower()))
                        {
                            System.Console.WriteLine("-*-[Passed]: Found (" + exp + ")");
                            flagF = true;
                            break;
                        }
                    }

                    if (!flagF && flag)
                        flag = false;
                }


                if (flag)
                {
                    if (agree != null && agree != string.Empty)
                    {
                        string str = string.Empty;
                        if (agree.ToLower() == "yes")
                        {
                            str = "button[id='ok_button']";
                        }
                        else
                        {
                            str = "button[id='cancel_button']";
                        }
                        snobutton bt = (snobutton)Base.SNGObject("Button", "button", By.CssSelector(str), dialog, snobase.MainFrame);
                        flag = bt.Existed;
                        if (flag)
                        {
                            flag = bt.Click();
                            if (!flag)
                                System.Console.WriteLine("-*-[ERROR]: Error when click on button.");
                        }
                        else System.Console.WriteLine("-*-[ERROR]: Cannot get button.");
                    }
                }
                else System.Console.WriteLine("-*-[ERROR]: Invalid message in dialog.");
            }
            else
                System.Console.WriteLine("-*-[ERROR]: Dialog confirmation is not existed.");

            return flag;
        }

        public bool WaitCheckConflicts_Completed_And_CloseDialog()
        {
            bool flag = true;
            string define = "#container>div>div>div>div:not([class])>span";
            snoelement status = null;
            try
            {
                status = (snoelement)Base.SNGObject("Conflicts status", "element", By.CssSelector(define), null, snobase.MainFrame, true);
                if (status.Existed && status.MyText == "The conflict check is complete")
                {
                    flag = true;
                }
                else { flag = false; }
            }
            catch { flag = false; }
            int count = 0;
            while (count < 20 && !flag)
            {
                Thread.Sleep(2000);
                try
                {
                    switch (count)
                    {
                        case 5:
                        case 10:
                        case 15:
                            snobutton bt = Button_CheckConflict();
                            flag = bt.Existed;
                            if (flag)
                                bt.Click();
                            break;
                    }
                    status = (snoelement)Base.SNGObject("Conflicts status", "element", By.CssSelector(define), null, snobase.MainFrame, true);
                    if (status.Existed && status.MyText == "The conflict check is complete")
                    {
                        flag = true;
                    }
                    else { flag = false; }
                }
                catch { flag = false; }
                count++;
            }

            if (flag)
            {
                snobutton bt = (snobutton)Base.SNGObject("Close", "button", By.Id("sysparm_button_close"), null, snobase.MainFrame);
                flag = bt.Existed;
                if (flag)
                {
                    flag = bt.Click();
                    if (!flag) System.Console.WriteLine("***ERROR: Error when click on button close.");
                }
                else System.Console.WriteLine("***ERROR: Cannot get button close.");
            }
            else System.Console.WriteLine("***ERROR: The status is not completed.");

            return flag;
        }


        public bool SelectField_ProposedChange(string proposedfield)
        {
            bool flag = false;
            int count = 0;
            snoelement parent = Filter_Proposed_Change();
            flag = parent.Existed;
            if (flag)
            {
                flag = proposedfield.Contains(";");
                if (flag)
                {
                    string[] arr = proposedfield.Split(';');
                    snoelements list = Base.SNGObjects("Proposed fields for change", By.CssSelector("tr[class='filter_row_condition']"), parent, snobase.MainFrame);
                    int i = 0;
                    while (list.MyList.Count == 0 && i < 2)
                    {
                        Thread.Sleep(2000);
                        list = Base.SNGObjects("Proposed fields for change", By.CssSelector("tr[class='filter_row_condition']"), parent, snobase.MainFrame);
                        i++;
                    }
                    if (list.MyList.Count > 0)
                    {
                        snoelement ele = null;
                        snoelement t = new snoelement("parent", Base.Driver, list.MyList[list.MyList.Count - 1].MyElement);
                        ele = (snoelement)Base.SNGObject("Proposed field name", "element", By.CssSelector("td[id='field']>select"), t, snobase.MainFrame);
                        snocombobox fcbox = new snocombobox("combo", Base.Driver, ele.MyElement);
                        flag = fcbox.Existed;
                        if (flag)
                        {
                            flag = fcbox.SelectItem(arr[0]);
                            if (flag)
                            {
                                WaitLoading();
                                t = new snoelement("parent", Base.Driver, list.MyList[list.MyList.Count - 1].MyElement);
                                ele = (snoelement)Base.SNGObject("Proposed value", "element", By.CssSelector("[id='value']>" + arr[2].ToLower() + ":not([type='hidden'])"), t, snobase.MainFrame);
                                flag = ele.Existed;
                                if (flag)
                                {
                                    switch (arr[2].ToLower())
                                    {
                                        case "select":
                                            snocombobox vcbox = new snocombobox("combo", Base.Driver, ele.MyElement);
                                            flag = vcbox.SelectItem(arr[1]);
                                            break;
                                        case "text":
                                            snotextbox vtext = new snotextbox("textbox", Base.Driver, ele.MyElement);
                                            flag = vtext.SetText(arr[1]);
                                            break;
                                        case "reference":
                                            snolookup vlookup = new snolookup("lookup", Base.Driver, ele.MyElement);
                                            flag = vlookup.SetText(arr[1]);
                                            break;
                                        case "textarea":
                                            snotextarea vtextarea = new snotextarea("textarea", Base.Driver, ele.MyElement);
                                            flag = vtextarea.SetText(arr[1]);
                                            break;
                                    }
                                    if (!flag)
                                    { System.Console.WriteLine("***ERROR: Cannot input/select proposed information"); count += 1; }
                                }
                                else
                                { System.Console.WriteLine("***ERROR: Cannot get field to input value."); count += 1; }
                            }
                            else
                            { System.Console.WriteLine("***ERROR: Cannot select field [" + arr[0] + "]"); count += 1; }
                        }
                        else
                        { System.Console.WriteLine("***ERROR: Cannot get filter combobox."); count += 1; }
                    }
                    else
                    { System.Console.WriteLine("***ERROR: The filter is not available for working on."); count += 1; }
                }
                else
                { System.Console.WriteLine("***ERROR: The format of input data is incorrect. Please validate again."); count += 1; }
            }
            else
            { System.Console.WriteLine("***ERROR: Cannot get proposed change filter."); count += 1; }

            if (count > 0)
                flag = false;
            return flag;
        }
        #endregion End - Method

        #endregion End - Controls

       
    }
}
