﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class Incident : Itil
    {
        #region Variables

        private snobase _snobase = null;
        
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public Incident(snobase obase, string name): base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Public methods

        public bool VerifyKnowledgeWasAttached(string article) 
        {
            bool flag = false;
            snoelement e = null;
            string define = ".obvious";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Artical list", by, null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.Existed) 
                {
                    e = new snoelement("artical", Base.Driver, ele.MyElement);
                    break;
                }
            }
            if (e != null) 
            {
                string runtime = e.MyText.Trim();
                if (runtime.ToLower().Contains(article.Trim().ToLower()))
                {
                    flag = true;
                    Console.WriteLine("-*-[Info]: Found articale:" + article);
                }
                else Console.WriteLine("-*-[Info]: Rutime:" + runtime + "| Expected:" + article);
            }
            
            return flag;
        }


        public bool IsVIPCaller()
        {
            bool flag = true;
            string define = "div[id='label.incident.caller_id']>label";
            snoelement ele = (snoelement)Base.SNGObject("Label of caller", "element", By.CssSelector(define), null, snobase.MainFrame);
            flag = ele.Existed;
            if (flag)
            {
                string temp = ele.MyElement.GetAttribute("style");
                if (!temp.Contains("images/icons/vip.gif"))
                    flag = false;
            }
            else { System.Console.WriteLine("-*-ERROR: Cannot get label of caller."); }

            return flag;
        }

        #endregion End - Public methods

        #region Control

        public snobutton Button_Location_Show_Locker()
        {
            snobutton bt = null;
            string define = "a[id^='location_show_locker']";
            bt = (snobutton)Base.SNGObject("Location show locker", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_Locker_Close()
        {
            snobutton bt = null;
            string define = "popup_close_image";
            bt = (snobutton)Base.SNGObject("Location show locker", "button", By.Id(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_ChangeRequestLookup()
        {
            snobutton bt = null;
            string define = "lookup.incident.rfc";
            bt = (snobutton)Base.SNGObject("Lookup using list", "button", By.Id(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_ResolveIncident()
        {
            snobutton bt = null;
            string define = "Resolve Incident";
            bt = (snobutton)Base.GButtonByText(define);
            return bt;
        }

        public snolookup Lookup_Source_Incident()
        {
            snolookup lookup = null;
            string define = "Source incident";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snocombobox Combobox_TEMneeded()
        {
            snocombobox combo = null;
            string define = "TEM needed";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_TEMjoined()
        {
            snocombobox combo = null;
            string define = "TEM joined";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_EventType()
        {
            snocombobox combo = null;
            string define = "Event type";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Business_Service_Impact()
        {
            snocombobox combo = null;
            string define = "Business/Service impact";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Cancellation_Code()
        {
            snocombobox combo = null;
            string define = "Cancellation code";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snotextbox Textbox_Business_Service_Impacted()
        {
            snotextbox textbox = null;
            string define = "Business/Service impacted";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextarea Textarea_Actions_Taken()
        {
            snotextarea textarea = null;
            string define = "Actions taken";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snocombobox Combobox_Resolution_Code()
        {
            snocombobox combo = null;
            string define = "Resolution code";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snotextarea Textarea_Resolution_Notes()
        {
            snotextarea textarea = null;
            string define = "Resolution notes";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_Summary()
        {
            snotextarea textarea = null;
            string define = "Summary";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_Lessons_Learned()
        {
            snotextarea textarea = null;
            string define = "Lessons learned";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snobutton Button_FollowUpDate_Calendar()
        {
            snobutton bt = null;

            string define = "button[id='incident.follow_up.ui_policy_sensitive']";
            snoelements eles = Base.SNGObjects("list", By.CssSelector(define), null, snobase.MainFrame);
            foreach (Auto.oelement ele in eles.MyList)
            {
                if (ele.Existed)
                {
                    bt = new snobutton("Planned start date calendar", Base.Driver, ele.MyElement);
                    break;
                }
            }

            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_CloseIncident()
        {
            snobutton bt = null;
            string define = "Close Incident";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion End - Control
    }
}
