﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class IncidentList : ItilList
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public IncidentList(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor
    }
}
