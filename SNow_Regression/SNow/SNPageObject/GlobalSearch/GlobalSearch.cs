﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace SNow
{
    public class GlobalSearch
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;
        //private snobase @base;

        #endregion End - Variables
        #region Properties

        public snobase Base
        {
            get { return _snobase; }
            set { _snobase = value; }
        }

        #endregion End - Properties
        #region Constructor

        public GlobalSearch(snobase obase, string name)
        {
            _snobase = obase;
            _name = name;
        }

        //public GlobalSearch(snobase @base)
        //{
        //    this.@base = @base;
        //}

        #endregion End - Constructor
        #region Controls
        //***********************************************************************************************************************************
        public snotextbox Textbox_Search()
        {
            snotextbox textbox = null;
            string define = "sysparm_search";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Search", "textbox", by, null, snobase.MainFrame);
            return textbox;
        }
        //------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_SearchKnowledge()
        {
            snotextbox textbox = null;
            string define = "input[id='kb_keywords']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Search Knowledge", "textbox", by, null, snobase.MainFrame);
            return textbox;
        }
        //------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Search()
        {
            snobutton button = null;
            string define = ".input-group-addon-transparent.icon-search.sysparm-search-icon";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Search", "button", by, null, snobase.MainFrame);
            return button;
        }
        //***********************************************************************************************************************************
        #endregion End - Controls
        #region Public methods
        public void WaitLoading()
        {
            Console.WriteLine("Global Search wait loading...");
            Base.PageWaitLoading();
        }
        //--------------------------------------------------------------------------------------------
        public snoelement GSearchResultGroup(string groupName, [Optional] int index)
        {
            snoelement e = null;
            string define = "[class='searchgroupheader'], div[class='result-list'] section header";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("List group result", by, null, snobase.MainFrame);
            if (list.Count > 0)
                    {
                        int count = 0;
                        foreach (Auto.oelement ele in list.MyList)
                        {
                            if (ele.MyText.ToLower().Contains(groupName.ToLower()))
                            {
                                if (count == index)
                                {
                                    e = new snoelement("group", Base.Driver, ele.MyElement);
                                    break;
                                }
                                else
                                {
                                    count = count + 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        System.Console.WriteLine("Not found any element.");
                    }
            
            if (e == null) { System.Console.WriteLine("Not found group result element [" + groupName + "]"); }
            return e;
        }
        //--------------------------------------------------------------------------------------------
        public snoelement GSearchResultArticle(string itemName, [Optional] int index)
        {
            snoelement e = null;
            string define = "article>a[class='article'], .record-container>h1>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("List item result", by, null, snobase.MainFrame);
            if (list.Count > 0)
                    {
                        int count = 0;
                        foreach (Auto.oelement ele in list.MyList)
                        {
                            if (ele.MyText.ToLower().Contains(itemName.ToLower()))
                            {
                                if (count == index)
                                {
                                    e = new snoelement("item", Base.Driver, ele.MyElement);
                                    break;
                                }
                                else
                                {
                                    count = count + 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        System.Console.WriteLine("Not found any element.");
                    }
                
            if (e == null) { System.Console.WriteLine("Not found item result element [" + itemName + "]"); }
            return e;
        }
        //--------------------------------------------------------------------------------------------
        public bool OpenArticleFromSelfServiceSearch(string article, [Optional] int index)
        {
            bool flagT = false;
            bool flag = true;
            string define = "a[class*='ng-binding'][target='_self']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("List item result", by, null, snobase.MainFrame);
            if (list.Count > 0)
                    {
                        int count = 0;
                        foreach (Auto.oelement ele in list.MyList)
                        {
                            if (ele.MyText.ToLower().Contains(article.ToLower()))
                            {
                                if (count == index)
                                {
                                    ele.Click();
                                    flagT = true;
                                    break;
                                }
                                else
                                {
                                    count = count + 1;
                                }
                            }
                        }
                        if (flagT)
                        {
                            flag = true;
                        }
                        else { flag = false; }
                    }
                    else
                    {
                        System.Console.WriteLine("Not found any element.");
                    }

            return flag;
        }
        //--------------------------------------------------------------------------------------------
        public bool FindItemFromGlobalSearchResult(string group, string item, bool click)
        {
            bool flag = true;
            snoelement eG = null;
            try
            {
                eG = GSearchResultGroup(group);
                flag = eG.Existed;
            }
            catch { flag = false; }

            int count = 0;
            while (!flag && count < 5)
            {
                Thread.Sleep(2000);
                
                try
                {
                    eG = GSearchResultGroup(group);
                    flag = eG.Existed;
                }
                catch { flag = false; }

                count++;
            }

            if (flag)
            {
                snoelement eI = GSearchResultArticle(item);
                flag = eI.Existed;
                if (flag)
                {
                    if (click)
                    {
                        eI.MoveToElement();
                        flag = eI.Click();
                        if (!flag) System.Console.WriteLine("-*-[ERROR]: Error when click on item.");
                    }
                    else { System.Console.WriteLine("--*-OK: Found article in the search result"); }
                }
                else System.Console.WriteLine("-*-[ERROR]: Cannot get (" + item + ") item from result.");

            }
            else System.Console.WriteLine("-*-[ERROR]: Cannot get (" + group + ") group from result.");
            return flag;
        }
        //--------------------------------------------------------------------------------------------
        public bool GlobalSearchItem(string search, bool enter)
        {
            bool flag = true;
            snotextbox texbox = Textbox_Search();
            if (texbox.Existed)
            {
                flag = texbox.SetText(search, enter);
            }
            else
            {
                snobutton button = Button_Search();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        flag = texbox.SetText(search, enter);
                    }
                }
            }

            return flag;
        }
        #endregion End - Public methods
    }
}
