﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace SNow
{
    public class RiskAssessment
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public RiskAssessment(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        public snoelement Title()
        {
            snoelement ele = null;
            string define = "div[class='gb_wrapper'] td[class$='gb_toolbar_text']";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Title", "element", by, null, snobase.MainFrame);
            return ele;
        }

        #region Button control
        
        public snobutton Button_Submit()
        {
            snobutton bt = null;
            string define = "post_survey";
            snoelements frameList = Base.SNGObjects("Frame list", By.TagName("iframe"), null, snobase.MainFrame, true, true);
            int index = frameList.MyList.Count - 1;
            Base.Driver.SwitchTo().Frame(index);
            IWebElement iwe = Base.Driver.FindElement(By.Id(define));
            if (iwe != null)
                bt = new snobutton("Submit", Base.Driver, iwe);
            return bt;
        }

        public snoelement Form_Submit()
        {
            snoelement bt = null;

            string define = "form[action='post_survey.do']";

            snoelements frameList = Base.SNGObjects("Frame list", By.TagName("iframe"), null, snobase.MainFrame, true, true);
            int index = frameList.MyList.Count - 1;
            Base.Driver.SwitchTo().Frame(index);

            IWebElement iwe = Base.Driver.FindElement(By.CssSelector(define));
            if (iwe != null)
                bt = new snoelement("Form submit", Base.Driver, iwe);

            return bt;
        }

        public snobutton Button_Close()
        {
            snobutton bt = null;

            string define = "span[class^='gb_close']";

            bt = (snobutton)Base.SNGObject("Close", "button", By.CssSelector(define), null, snobase.MainFrame );

            return bt;
        }
        
        #endregion End - Button control

        #region Methods
        
        public void WaitLoading()
        {
            System.Console.WriteLine("Risk assessment page wait loading...");
            Base.PageWaitLoading();
        }
        
        public List<snoelement> GetRiskQuestions()
        {
            List<snoelement> questionList = new List<snoelement>();
            string define = "tr[id^='element.QUESTION'] div[class='row sc-row']";
            snoelements frameList = Base.SNGObjects("Frame list", By.TagName("iframe"), null, snobase.MainFrame, true, true);
            int index = frameList.MyList.Count - 1;
            Base.Driver.SwitchTo().Frame(index);

            IReadOnlyCollection<IWebElement> iwes = Base.Driver.FindElements(By.CssSelector(define));
            foreach (IWebElement iwe in iwes) 
            {
                snoelement ele = new snoelement("question", Base.Driver, iwe);
                questionList.Add(ele);
            }

            return questionList;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string GetQuestionText(snoelement parent)
        {
            string qText = string.Empty;
            
            string define = "div[id^='label_QUESTION'] span[class^='sn-tooltip-basic']";
            snoelements frameList = Base.SNGObjects("Frame list", By.TagName("iframe"), null, snobase.MainFrame, true, true);
            int index = frameList.MyList.Count - 1;
            Base.Driver.SwitchTo().Frame(index);
            IWebElement iwe = parent.MyElement.FindElement(By.CssSelector(define));
            if (iwe != null) 
            {
                qText = iwe.Text.Trim();   
            }

            return qText;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public List<snoelement> GetAnswers(snoelement parent)
        {
            List<snoelement> answerList = new List<snoelement>();
            string define = "div[class*='input_controls'] input[id^='QUESTION']";
            snoelements frameList = Base.SNGObjects("Frame list", By.TagName("iframe"), null, snobase.MainFrame, true, true);
            int index = frameList.MyList.Count - 1;
            Base.Driver.SwitchTo().Frame(index);
            IReadOnlyCollection<IWebElement> iwes = parent.MyElement.FindElements(By.CssSelector(define));
            foreach (IWebElement iwe in iwes)
            {
                snoelement ele = new snoelement("answer", Base.Driver, iwe);
                answerList.Add(ele);
            }
            
            return answerList;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, List<snoelement>> GetRiskQuestionsAndAnswers()
        {
            Dictionary<string, List<snoelement>> list = new Dictionary<string, List<snoelement>>();

            // Get all Risk Questions
            List<snoelement> questions = GetRiskQuestions();
            if (questions.Count > 0)
            {
                string questionText = string.Empty;
                List<snoelement> answers = new List<snoelement>();

                int i = 1;
                // Add question text and answers into dictionary
                foreach (snoelement ele in questions)
                {
                    // Get the Question Text
                    questionText = GetQuestionText(ele);

                    // Get the list of radio buttons
                    answers = GetAnswers(ele);

                    // Add question elements into dictionary if OK
                    if (questionText != null && questionText != string.Empty && answers.Count > 0)
                    {
                        System.Console.WriteLine("***[OK]: Can get Risk questions number [" + i + "] with question text is [" + questionText + "]");
                        list.Add(questionText.ToLower(), answers);
                    }
                    else
                    {
                        System.Console.WriteLine("***[ERROR]: Cannot get Risk questions number [" + i + "]");
                    }

                    i++;
                }
            }
            else
            {
                System.Console.WriteLine("***[ERROR]: Cannot get Risk questions");
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SelectRiskQuestionsAndAnswers(string questionAndAnswer)
        {
            bool flag = true;

            if (questionAndAnswer == null || questionAndAnswer.Trim() == "" || !questionAndAnswer.Contains("::"))
            {
                System.Console.WriteLine("***ERROR: the parameter [questionAndAnswer] is incorrect. it should be a string with format: q1::ans1;q2::ans2...");
                return false;
            }

            Dictionary<string, List<snoelement>> list = GetRiskQuestionsAndAnswers();
            if (list.Count > 0)
            {
                string[] answerList = questionAndAnswer.Split(';');
                string question = string.Empty;
                string answer = string.Empty;

                foreach (string ans in answerList)
                {
                    string[] sTemp = ans.Split(new string[] { "::" }, System.StringSplitOptions.RemoveEmptyEntries);
                    question = sTemp[0];
                    answer = sTemp[1];

                    // Search the question
                    if (list.ContainsKey(question.ToLower()))
                    {
                        System.Console.WriteLine("***OK: Found question [" + question + "]");

                        List<snoelement> listTemp = list[question.ToLower()];
                        bool flagTemp = false;

                        // Search the answer
                        foreach (snoelement ele in listTemp)
                        {
                            IWebElement label = (ele.MyElement.FindElement(By.XPath(".."))).FindElement(By.TagName("label"));
                            if (label.Text.Trim().ToLower().Equals(answer.ToLower().Trim()))
                            {
                                // Click on answer
                                flag = ele.Click(true);
                                if (flag)
                                {
                                    System.Console.WriteLine("***PASSED: Can click on answer [" + answer + "]");
                                }
                                else
                                {
                                    System.Console.WriteLine("***ERROR: Cannot click on answer [" + answer + "]");
                                }

                                flagTemp = true;
                                break;
                            }
                        }

                        if (!flagTemp)
                        {
                            System.Console.WriteLine("***ERROR: Cannot find answer [" + answer + "]");
                            flag = false;
                        }
                    }
                    else
                    {
                        System.Console.WriteLine("***ERROR: Cannot find question [" + question + "]");
                        flag = false;
                    }
                }
            }
            else
            {
                System.Console.WriteLine("***ERROR: Cannot get any questions and answers");
                flag = false;
            }
            return flag;
        }

        //***********************************************************************************************************************************
        #endregion End - Methods
    }
}
