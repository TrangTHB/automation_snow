﻿using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System;
using System.Threading;


namespace Auto
{
    public class Alert : Create
    {
        obase Base = null;
        string pageName = string.Empty;

        private string mainFrame = "gsft_main";
        public Alert(obase _base, string name):base(_base, name)
        {
            Base = _base;
            this.pageName = name;
        }

        #region Lookups
        //-----------------------------------------------------------------------------------------------------------------------------------
        public olookup Lookup_Task
        {
            get { return GLookup("incident", "Task"); }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public olookup Lookup_KnowledgeArticle
        {
            get { return GLookup("kb", "Knowledge Article"); }
        }
		//-----------------------------------------------------------------------------------------------------------------------------------
		public olookup Lookup_Type
		{
			get { return GLookup("type", "Type"); }
		}
		#endregion End - Lookups

		#region Elements
		//-----------------------------------------------------------------------------------------------------------------------------------
		private oelement Dialog_Confirmation(bool noWait)
        {
            oelement dl = null;

            string define = "div[id^='confirm'] span[id='body_confirm_assignment']";

            if (Base.SwitchToDefaultContent())
            {
                if (Base.SwitchToFrame(mainFrame))
                {
                    dl = (oelement)Base.GObject("", "Dialog confirmation", By.CssSelector(define), null, 0, noWait);
                }
                else
                {
                    System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
                }
            }
            else
            {
                System.Console.WriteLine("***ERROR: Cannot switch to default content.");
            }

            return dl;
        }

		#endregion End - Elements

		#region Textbox control
		//***********************************************************************************************************************************
		public otextbox Textbox_Resource
		{
			get { return GTextbox("resource", "Resource"); }
		}
		//***********************************************************************************************************************************
		public otextbox Textbox_Source
		{
			get { return GTextbox("source", "Source"); }
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		public otextbox Textbox_Node
		{
			get { return GTextbox("node", "Node"); }
		}
		
		#endregion End - Textbox control

		#region Button
		//-----------------------------------------------------------------------------------------------------------------------------------
		public obutton Button_Close
		{
			get
			{
				obutton bt = null;
				string define = "close_alert";

				if (Base.SwitchToDefaultContent())
				{
					if (Base.SwitchToFrame(mainFrame))
					{
						bt = (obutton)Base.GObject("button", "Close", By.Id(define));
					}
					else
					{
						System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
					}
				}
				else
				{
					System.Console.WriteLine("***ERROR: Cannot switch to default content.");
				}

				return bt;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		public obutton Button_Maintenance
		{
			get
			{
				obutton bt = null;
				string define = "Maintenance";

				if (Base.SwitchToDefaultContent())
				{
					if (Base.SwitchToFrame(mainFrame))
					{
						bt = (obutton)Base.GObject("button", "Maintenance", By.Id(define));
					}
					else
					{
						System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
					}
				}
				else
				{
					System.Console.WriteLine("***ERROR: Cannot switch to default content.");
				}

				return bt;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		public obutton Button_CreateSecurityIncident
		{
			get
			{
				obutton bt = null;
				string define = "create_security_incident_alert";

				if (Base.SwitchToDefaultContent())
				{
					if (Base.SwitchToFrame(mainFrame))
					{
						bt = (obutton)Base.GObject("button", "Create Security Incident", By.Id(define));
					}
					else
					{
						System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
					}
				}
				else
				{
					System.Console.WriteLine("***ERROR: Cannot switch to default content.");
				}

				return bt;
			}
		}
		//-----------------------------------------------------------------------------------------------------------------------------------
		public obutton Button_CreateIncident
		{
			get
			{
				obutton bt = null;
				string define = "create_incident_alert";

				if (Base.SwitchToDefaultContent())
				{
					if (Base.SwitchToFrame(mainFrame))
					{
						bt = (obutton)Base.GObject("button", "Create Incident", By.Id(define));
					}
					else
					{
						System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
					}
				}
				else
				{
					System.Console.WriteLine("***ERROR: Cannot switch to default content.");
				}

				return bt;
			}
		}
		#endregion Button

		#region Checkbox
		//-----------------------------------------------------------------------------------------------------------------------------------
		public ocheckbox Checkbox_Acknowledged
        {
            get { return GCheckbox("acknowledged", "Acknowledged"); }
        }
		//-----------------------------------------------------------------------------------------------------------------------------------
		public ocheckbox Checkbox_Maintenance
		{
			get { return GCheckbox("maintenance", "Maintenance"); }
		}
		#endregion Checkbox

		#region Combobox
		//-----------------------------------------------------------------------------------------------------------------------------------
		public ocombobox Combobox_Severity
		{
			get { return GCombobox("severity", "Acknowledged"); }
		}
		#endregion Checkbox


		#region Methods
		//***********************************************************************************************************************************
		//-----------------------------------------------------------------------------------------------------------------------------------
		public bool VerifyConfirmationDialog_Incident(string expectedValue, [Optional] string agree, [Optional] bool notDisplay)
        {
            bool flag = true;
            oelement dialog = null;
            try {dialog = Dialog_Confirmation(false);}
            catch (Exception){ dialog = null; }
            flag = dialog.Existed; 
            if (notDisplay == false && flag)
            {
                string define = "tr:nth-child(2)";
                oelement ele = (oelement)Base.GObject("", "Confirm text", By.CssSelector(define), dialog);
                flag = ele.Existed;
                if (flag)
                {
                    System.Console.WriteLine("-*-Runtime:(" + ele.Text + ")");
                    if (!ele.Text.Trim().ToLower().Equals(expectedValue.ToLower()))
                    {
                        flag = false;
                    }
                    else
                    {
                        if (agree != null && agree != string.Empty)
                        {
                            string str = string.Empty;
                            if (agree.ToLower() == "yes")
                            {
                                str = "button[id='yes_btn']";
                            }
                            else
                            {
                                str = "button[id='no_btn']";
                            }
                            obutton bt = (obutton)Base.GObject("button", "Button", By.CssSelector(str), dialog);
                            flag = bt.Existed;
                            if (flag)
                            {
                                flag = bt.Click();
                                if (!flag)
                                    System.Console.WriteLine("-*-[ERROR]: Error when click on button.");
                            }
                            else System.Console.WriteLine("-*-[ERROR]: Cannot get button.");
                        }
                    }
                }
            }
            else 
            { 
                if (notDisplay)
                {
                    flag = true;
                    System.Console.WriteLine("-*-OK---: Dialog confirmation is not existed."); 
                }
                //System.Console.WriteLine("-*ERROR-: Dialog confirmation is not existed."); 
            }
                
            return flag;
        }

        public bool IsVIPCaller() 
        {
            bool flag = true;
            string define = "div[id='label.incident.caller_id']>label";
            if (Base.SwitchToDefaultContent())
            {
                if (Base.SwitchToFrame(mainFrame))
                {
                    oelement ele = (oelement)Base.GObject("", "Label of caller", By.CssSelector(define));
                    flag = ele.Existed;
                    if (flag)
                    {
                        string temp = ele.MyEle.GetAttribute("style");
                        if (!temp.Contains("images/icons/vip.gif"))
                            flag = false;
                    }
                    else { System.Console.WriteLine("-*-ERROR: Cannot get label of caller."); }
                }
                else
                {
                    System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
                }
            }
            else
            {
                System.Console.WriteLine("***ERROR: Cannot switch to default content.");
            }
            
            return flag;
        }

        public bool AutoCreateIncident(string Cpny, string Caller, string Cat, string SubCat, string ShortDescript, string impact, string urgcy)
        {
            bool flag = true;
            flag = SelectCompany(Cpny);
            if (flag)
            {
                flag = SelectCaller(Caller);
                if (flag)
                {
                    flag = SelectCategory(Cat);
                    if (flag)
                    {
                        WaitLoading();
                        flag = SelectSubCategory(SubCat);
                        if (flag)
                        {
                            flag = SetTextDescription(ShortDescript);
                            if (flag)
                            {
                                flag = SelectImpact(impact);
                                if (flag)
                                {
                                    flag = SelectUrgency(urgcy);
                                }
                            }
                        }
                    }
                }
            }
            return flag;
        }
        //----------------------------------------------------------------------------------------------
        private bool SelectCompany(string company) 
        {
            bool flag = true;
            olookup comp = Lookup_Company;
            flag = comp.Existed;
            if (flag)
            {
                flag = comp.Select(company);
                if(!flag)
                    Console.WriteLine("-*-[ERROR]: Error when select Company.");
            }else Console.WriteLine("-*-[ERROR]: Cannot get lookup company.");
            return flag;
        }
        //-----------------------------
        private bool SelectCaller(string caller)
        {
            bool flag = true;
            olookup cl = Lookup_Caller;
            flag = cl.Existed;
            if (flag)
            {
                flag = cl.Select(caller);
                if (!flag)
                    Console.WriteLine("-*-[ERROR]: Error when select Caller.");
            }
            else Console.WriteLine("-*-[ERROR]: Cannot get lookup caller.");
            return flag;
        }
        //-----------------------------
        private bool SetTextDescription(string ShortDescript)
        {
            bool flag = true;
            otextbox sd = Textbox_ShortDescription;
            flag = sd.Existed;
            if (flag)
            {
                flag = sd.SetText(ShortDescript, true);
                if (!flag)
                    Console.WriteLine("-*-[ERROR]: Error when populate value for short description.");
            }
            else Console.WriteLine("-*-[ERROR]: Cannot get textbox short description.");
            return flag;
        }
        //-----------------------------
        private bool SelectCategory(string cat)
        {
            bool flag = true;
            ocombobox ct = Combobox_Category;
            flag = ct.Existed;
            if (flag)
            {
                flag = ct.SelectItem(cat);
                if (!flag)
                    Console.WriteLine("-*-[ERROR]: Cannot select item Category.");
            }
            else Console.WriteLine("-*-[ERROR]: Lookup Category does not exist.");
            return flag;
        }
        //-----------------------------
        private bool SelectSubCategory(string subcat)
        {
            bool flag = true;
            ocombobox sub = Combobox_SubCategory;
            flag = sub.Existed;
            if (flag)
            {
                flag = VerifyHaveItemInComboboxList("subcategory", subcat);
                if (flag)
                {
                    flag = sub.SelectItem(subcat);
                    if (!flag)
                    {
                        Console.WriteLine("-*-[ERROR]: Cannot select item SubCategory.");
                    }
                }
                else
                {
                    Console.WriteLine("-*-[ERROR]: Not found item (" + subcat + ") in sub category list..");
                }                
            }
            else Console.WriteLine("-*-[ERROR]: Combobox SubCategory does not exist.");
            return flag;
        }
        //-----------------------------
        private bool SelectImpact(string impact)
        {
            bool flag = true;
            ocombobox imp = Combobox_Impact;
            flag = imp.Existed;
            if (flag)
            {
                flag = imp.SelectItem(impact);
                if (!flag)
                    Console.WriteLine("-*-[ERROR]: Cannot select item Impact.");
            }
            else Console.WriteLine("-*-[ERROR]: Combobox Impact does not exist.");
            return flag;
        }
        //-----------------------------
        private bool SelectUrgency(string urgency)
        {
            bool flag = true;
            ocombobox urg = Combobox_Urgency;
            flag = urg.Existed;
            if (flag)
            {
                flag = urg.SelectItem(urgency);
                if (!flag)
                    Console.WriteLine("-*-[ERROR]: Cannot select item Urgency.");
            }
            else Console.WriteLine("-*-[ERROR]: Combobox Urgency does not exist.");
            return flag;
        }
        //-----------------------------
        public bool CheckCurrentState(string currState)
        {
            oelement ele = null;
            bool flag = true;
            string eleDefine = ".//*[@class='process-breadcrumb process-breadcrumb-border']/li[contains(@data-state,'current')]/a";
            flag = Base.SwitchToDefaultContent();
            if (flag)
            {
                flag = Base.SwitchToFrame(mainFrame);
                if (flag)
                {
                    ele = (oelement)Base.GObject("element", "CurrentState", By.XPath(eleDefine));
                }
            }
            if (ele != null)
                if (ele.Text != currState)
                    return false;
                else return true;
            else return false;

        }


        //***********************************************************************************************************************************
        #endregion End - Methods
    }
}
