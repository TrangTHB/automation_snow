﻿using OpenQA.Selenium;

namespace Auto
{
    public class AlertList : List
    {
        obase Base = null;
        string pageName = string.Empty;

        public AlertList(obase _base, string name): base(_base, name)
        {
            Base = _base;
            pageName = name;
        }
    }
}
