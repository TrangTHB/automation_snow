﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class Submission : Itil
    {
         #region Variables

        private snobase _snobase = null;
        
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public Submission(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Control
        public snotextarea Textarea_Text()
        {
            snotextarea textarea = null;

            string define = "tinymce";
            snoelements list = Base.SNGObjects("list frame", By.TagName("iframe"), null, snobase.MainFrame);
            string frame = list.MyList[0].MyElement.GetAttribute("id").Trim();
            Console.WriteLine(frame);
            Base.Driver.SwitchTo().Frame(frame);
            IWebElement ele = Base.Driver.FindElement(By.Id(define));
            if (ele != null)
                textarea = new snotextarea("textarea", Base.Driver, ele);

            return textarea;
        }

        public snoelement GEle_Source
        {
            get
            {
                snoelement ele = null;
                string define = ".//*[@id='kb_submission.kb_knowledge.source_breadcrumb']//b";
                ele = (snoelement)Base.SNGObject("Source", "Element", By.XPath(define), null, snobase.MainFrame);

                return ele;
            }
        }
        #endregion End - Control

        #region Buttons
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_CreateArticle()
        {
            snobutton bt = null;
            string define = "Create Article";
            bt = Base.GButtonByText(define);
            return bt;
        }

        #endregion End - Buttons

        #region Public methods
     
        #endregion End - public methods
    }
}
