﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SNow
{
    public class KnowledgeSearch
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public KnowledgeSearch(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine("Knowledge search page wait loading...");
            Base.PageWaitLoading(false, true);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndOpen(string artical) 
        {
            bool flag = false;

            snotextbox textbox = Textbox_Search();
            flag = textbox.Existed;
            if (flag) 
            {
                flag = textbox.SetText(artical);
                if (flag) 
                {
                    snobutton button = Button_Search();
                    flag = button.Existed;
                    if (flag) 
                    {
                        button.Click();
                        WaitLoading();
                        string define = "div[class='result'] a";
                        By by = By.CssSelector(define);
                        snoelements list = Base.SNGObjects("Artical list", by);
                        foreach (Auto.oelement ele in list.MyList) 
                        {
                            if (ele.MyText.Trim().ToLower().Equals(artical.Trim().ToLower())) 
                            {
                                flag = ele.Click();
                                if (!flag)
                                    Console.WriteLine("-*-[Info]: Error when set click on artical.");
                                else
                                    Base.PageWaitLoading(true, true);
                                break;
                            }
                        }
                    }
                    else Console.WriteLine("-*-[Info]: Cannot get button search.");
                }
                else Console.WriteLine("-*-[Info]: Error when set text.");
            }
            else Console.WriteLine("-*-[Info]: Cannot get textbox search.");

            return flag;
        }
        public bool FindKnowledge(string name, [Optional] bool noOpen)
        {
            bool flag = true;

            string define;
            if (noOpen)
                define = ".article";
            else
                define = ".article>a";

            snoelements list = Base.SNGObjects("Knowledge list", By.CssSelector(define));
            flag = list.ClickOnItem("@@"+name);

            return flag;
        }
        public bool VerifyKnowledgeInfo_KnowledgeSearch(string knowledge_name, string knowledgeinfos)
        {
            bool flag = false;
            string[] infos = null;
            //string runtimestr = string.Empty;

            snoelements ka_list = null;
            snoelements ka_infos = null;
            snoelement parent = null;

            string p_define = ".result";
            string ka_define = "div>span[class='article-info']";

            ka_list = Base.SNGObjects("Knowledge list", By.CssSelector(p_define));
            if (ka_list.Count > 0)
            {
                foreach (Auto.oelement e in ka_list.MyList)
                {
                    snoelement p = new snoelement("Parent", Base.Driver, e.MyElement);
                    snoelement ele = (snoelement)Base.SNGObject("knowledge name", "element", By.CssSelector("h4>a"), p);
                    if (ele.Existed)
                    {
                        if (ele.MyText.Trim() == knowledge_name.Trim())
                        {
                            parent = p;
                            flag = true;
                            break;
                        }
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("Cannot get knowledge short description.");
                    }
                }

                if (flag == true)
                {
                    ka_infos = Base.SNGObjects("Knowledge infos", By.CssSelector(ka_define), parent);
                    if (ka_infos.Count > 0)
                    {
                        if (knowledgeinfos.Contains(";"))
                            infos = knowledgeinfos.Split(';');
                        else
                            infos = new string[] { knowledgeinfos };
                        foreach (string info in infos)
                        {
                            flag = false;
                            foreach (Auto.oelement ka_ele in ka_infos.MyList)
                            {
                                //runtimestr = runtimestr + "." + ka_ele.Text;
                                if (ka_ele.MyText.Trim() == info.Trim())
                                {
                                    flag = true;
                                    System.Console.WriteLine("Could find information [" + info + "] in knowledge record.");
                                    break;
                                }
                            }
                            //System.Console.WriteLine("Runtime knowledge info element - [" + runtimestr + "]");
                            if (!flag)
                            {
                                System.Console.WriteLine("Cannot find information [" + info + "] in knowledge record.");
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("Cannot find any knowledge information.");
                    }
                }
                else
                {
                    flag = false;
                    System.Console.WriteLine("Cannot get any knowledge record with expected short description.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("Cannot get result list.");
            }

            return flag;
        }
        #endregion End - Public methods

        #region Controls

        public snotextbox Textbox_Search()
        {
            snotextbox textbox = null;
            string define = "sysparm_search";
            By by = By.Id(define);
            textbox = (snotextbox)Base.SNGObject("Search", "textbox", by);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Search()
        {
            snobutton button = null;
            string define = ".btn.btn-primary.kb-btn-primary";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Search", "button", by);
            return button;
        }

        public snobutton Button_Attach()
        {
            snobutton button = null;
            string define = "attachToTask";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Attach", "button", by);
            return button;
        }
        #endregion End - Controls
    }
}
