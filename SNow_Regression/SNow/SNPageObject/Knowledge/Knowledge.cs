﻿using OpenQA.Selenium;
using System;
using System.Runtime.InteropServices;

namespace SNow
{
    public class Knowledge : Itil
    {

        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;
        #endregion End - Variables

        #region Properties

        public SNow.snobase _Base
        {
            get { return _snobase; }
            set { _snobase = value; }
        }

        #endregion End - Properties

        #region Constructor

        public Knowledge(snobase obase, string name): base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Private methods
        public snolookup Lookup_SourceTask()
        {
            snolookup lookup = null;
            string define = "Source Task";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snodate Datetime_ValidTo()
        {
            snodate datetime = null;
            string define = "Valid to";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodate)obj;
            else
                datetime = new snodate(define, Base.Driver, null);
            return datetime;
        }
        #endregion End - Private methods

        #region Public methods

       
        public bool Publish_KA(string knowledgeId)
        {
            bool flag = true;
            string error = string.Empty;

            Console.WriteLine("[Publishing] - <" + knowledgeId + ">");

            snobutton button = Button_KA_Publish();

            // If the Publish button exists
            if (button.Existed)
            {
                flag = button.Click(true);
                if (flag)
                {
                    WaitLoading();

                    // Search the Knowledge in Knowledge List
                    ItilList ka_list = new ItilList(Base, "Knowledge List");
                    flag = ka_list.SearchAndOpen("Number", knowledgeId, "Number=" + knowledgeId, "Number");
                    if (flag)
                    {
                        WaitLoading();
                    }
                    else
                    {
                        error = "Cannot search Knowledge with Number = [" + knowledgeId + "]";
                    }
                }
                else
                {
                    error = "Cannot click [Publish] button";
                }
            }
            else error = "*****Button [Publish] is NOT found";

            //---------------------------------------------------------------------------------
            if (error == string.Empty)
            {
                flag = true;
                Console.WriteLine("[Publish] - OK");
            }
            else
            {
                flag = false;
                Console.WriteLine("[Publish] - ERRORS: < " + error + " >");
            }


            return flag;
        }
        //---------------------------------------------------------------------------------------------
        public bool Review_KA(string knowledgeId)
        {
            bool flag = true;
            SNow.ItilList ka_list = new SNow.ItilList(Base, "Knowledge List");
            string error = string.Empty;

            Console.WriteLine("[Reviewing] - <" + knowledgeId + ">");

            snobutton button = Button_KA_Review();

            // If the Review button exists
            if (button.Existed)
            {
                flag = button.Click(true);
                if (flag)
                {
                    // Search the Knowledge in Knowledge List
                    ka_list.WaitLoading();

                    flag = ka_list.SearchAndOpen("Number", knowledgeId, "Number=" + knowledgeId, "Number");
                    if (!flag)
                    {
                        error = "Cannot search Knowledge with Number = [" + knowledgeId + "]";
                    }
                }
                else
                {
                    error = "Cannot click on Review button";
                }
            }
            else error = "*****Button [Review] is NOT found";
            //---------------------------------------------------------------------------------

            if (error == string.Empty)
            {
                flag = true;
                Console.WriteLine("[Review] - OK");
            }
            else
            {
                flag = false;
                Console.WriteLine("[Review] - ERRORS: < " + error + " >");
            }

            return flag;
        }

        public string GetMessageInfo_TicketNumber()
        {
            string message = string.Empty;
            bool flag = true;
            flag = Base.SwitchToPage(0);
            if (flag)
            {
                flag = Base.SwitchToPage(0);
                if (flag)
                {
                    string define = ".outputmsg.outputmsg_info.notification.notification-info";
                    snoelements list = Base.SNGObjects("Message info", By.CssSelector(define), null, snobase.MainFrame);
                    if (list.Count > 0)
                    {
                        //bool flagF = true;
                        foreach (Auto.oelement e in list.MyList)
                        {
                            string value = e.MyText;
                            if (value.Contains("SUB"))
                            {
                                message = value.Remove(0, value.IndexOf("SUB"));
                                //message = message.Remove(9, message.Length-9);
                                System.Console.WriteLine("***[Passed]: Found message info (" + value + ")");
                                System.Console.WriteLine("***[Passed]: Found ID : (" + message + ")");
                                break;
                            }
                            else if (value.Contains("KB"))
                            {
                                message = value.Remove(0, value.IndexOf("KB"));
                                message = message.Remove(9, message.Length - 9);
                                System.Console.WriteLine("***[Passed]: Found message info (" + value + ")");
                                System.Console.WriteLine("***[Passed]: Found ID : (" + message + ")");
                                break;
                            }
                            else
                            {
                                System.Console.WriteLine("***[Failed]: Not found message contain (" + value + ")");
                                flag = false;
                            }

                        }
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("***[ERROR]: Not found any message info.");
                    }
                }
                else
                {
                    System.Console.WriteLine("***ERROR: Cannot switch to main frame.");
                }
            }
            else
            {
                System.Console.WriteLine("***ERROR: Cannot switch to default content.");
            }

            return message;
        }
        //----------------------------------------------------------------------------------------------
        public bool AddRole(string role)
        {
            bool flag = true;

            snocombobox right = null;

            right = List_Right();
            flag = right.VerifyExpectedItemsExisted(role);

            if (!flag)
            {
                snocombobox left = List_Left();
                flag = left.SelectItem(role);
                if (flag)
                {
                    snobutton bt = Button_Add();
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click();
                        if (flag)
                        {
                            flag = right.VerifyExpectedItemsExisted(role);
                        }
                    }
                    else
                        System.Console.WriteLine("*****Element [" + bt.MyText + "] is NOT found.");
                }
            }
            return flag;
        }
        public snotextarea Textarea_Text()
        {
            snotextarea textarea = null;

            string define = "tinymce";
            snoelements list = Base.SNGObjects("list frame", By.TagName("iframe"), null, snobase.MainFrame);
            string frame = list.MyList[0].MyElement.GetAttribute("id").Trim();
            Base.Driver.SwitchTo().Frame(frame);
            IWebElement ele = Base.Driver.FindElement(By.Id(define));
            if (ele != null)
                textarea = new snotextarea("textarea", Base.Driver, ele);
            
            return textarea;
        }
        private snocombobox List_Left()
        {
            snocombobox list = null;
            string define = "[id$= 'rolesselect_0']";
            list = (snocombobox)Base.SNGObject("Left list", "combobox", By.CssSelector(define), null, snobase.MainFrame);
            return list;
        }
        //----------------------------------------------------------------------------------------------
        private snocombobox List_Right()
        {
            snocombobox list = null;
            string define = "[id$='rolesselect_1']";
            list = (snocombobox)Base.SNGObject("Right list", "combobox", By.CssSelector(define), null, snobase.MainFrame);
            return list;
        }
        public snoelement Button_EditRole()
        {
            snoelement er = null;
            string define = "[id$='.roles_unlock']";
            er = (snoelement)Base.SNGObject("EditRole", "element", By.CssSelector(define), null, snobase.MainFrame);
            return er;
        }
        public snobutton Button_Add()
        {
            snobutton bt_add = null;
            string define = "a[data-original-title='Add']";
            bt_add = (snobutton)Base.SNGObject("Add", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt_add;
        }
        public snobutton Button_Done()
        {
            snobutton bt_add = null;
            string define = "[id$='roles_lock']";
            bt_add = (snobutton)Base.SNGObject("Done", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt_add;
        }


        public bool AutoCreateKnowledgeArticles(string KB, string ShortDescript, string Domain, string AssignmentGroup, [Optional] string Language, [Optional] string TextContent)
        {
            bool flag = true;
            snolookup kn = Lookup_Knowledge_base();
            flag = kn.Existed;
            if (flag)
            {
                flag = kn.Select(KB);
                if (flag)
                {
                    snotextbox sd = Textbox_ShortDescription();
                    flag = sd.Existed;
                    if (flag)
                    {
                        flag = sd.SetText(ShortDescript, true);
                        if (flag)
                        {
                            snolookup ag = Lookup_AssignmentGroup();
                            flag = ag.Existed;
                            if (flag)
                            {
                                flag = ag.Select(AssignmentGroup);
                                if (flag)
                                {
                                    snolookup dm = Lookup_Domain();
                                    flag = dm.Existed;
                                    if (flag)
                                    {
                                        flag = dm.Select(Domain);
                                        if (flag)
                                        {
                                            if (Language != null && Language != string.Empty)
                                            {
                                                snocombobox lan = Combobox_Language();
                                                flag = lan.Existed;
                                                if (flag)
                                                {
                                                    flag = lan.SelectItem(Language);
                                                    if (flag)
                                                    {
                                                        if (TextContent != null)
                                                        {
                                                            snotextarea text = Textarea_Text();
                                                            flag = text.Existed;
                                                            if (flag)
                                                            {
                                                                flag = text.SetText(TextContent, false, false, true);
                                                                if (!flag)
                                                                    Console.WriteLine("-*-[ERROR]: Error when populate value for text.");
                                                            }
                                                            else Console.WriteLine("-*-[ERROR]: Cannot get textbox text.");
                                                        }
                                                    }
                                                    else Console.WriteLine("-*-[ERROR]: Error when select item for language.");
                                                }
                                                else Console.WriteLine("-*-[ERROR]: Cannot get combobox language.");
                                            }


                                            if (flag)
                                            {
                                                flag = Button_Submit().Click(true);
                                                if (!flag) Console.WriteLine("-*-[ERROR]: Error when submit knowledge.");
                                            }
                                        }
                                        else Console.WriteLine("-*-[ERROR]: Error when select item for domain.");
                                    }
                                    else Console.WriteLine("-*-[ERROR]: Cannot get lookup domain.");
                                }
                                else Console.WriteLine("-*-[ERROR]: Error when select item for assignment group.");
                            }
                            else Console.WriteLine("-*-[ERROR]: Cannot get lookup assignment group.");
                        }
                        else Console.WriteLine("-*-[ERROR]: Error when populate value for short description.");
                    }
                    else Console.WriteLine("-*-[ERROR]: Cannot get textbox short description.");
                }
                else Console.WriteLine("-*-[ERROR]: Error when select item for knowledge base.");
            }
            else Console.WriteLine("-*-[ERROR]: Cannot get lookup knowledge base.");

            return flag;
        }

        
        #endregion End - Public methods

        #region Control

        public snodatetime Datetime_Published()
        {
            snodatetime datetime = null;
            string define = "Published";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snocombobox Combobox_Language()
        {
            snocombobox combo = null;
            string define = "Language";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }


        public snocombobox Combobox_AdvancedSearch()
        {
            snocombobox combo = null;

            string define = "selectKnowledgeDropdown";
            combo = (snocombobox)Base.SNGObject("Add", "combobox", By.Id(define), null, snobase.MainFrame);
            return combo;
        }
        #endregion End - Control
    }
}
