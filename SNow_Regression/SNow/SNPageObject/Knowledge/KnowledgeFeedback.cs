﻿using System;
using OpenQA.Selenium;
using System.Collections.ObjectModel;


namespace SNow
{
    public class KnowledgeFeedback : Itil
    {

        #region Variables
        //***********************************************************************************************************************************

        private snobase _snobase = null;
        private string _name = string.Empty;
        #endregion End - Variables

         #region Constructor
        //***********************************************************************************************************************************
        public KnowledgeFeedback(snobase obase, string name): base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Controls

        public snobutton Button_Helpful_Yes()
        {
            snobutton button = null;
            string define = ".btn.btn-success-subdued.kb-article-yes-btn";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Yes", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Helpful_No()
        {
            snobutton button = null;
            string define = ".btn.btn-destructive-subdued.kb-article-no-btn";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("No", "button", by, null, snobase.MainFrame);
            return button;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_Comment()
        {
            snotextarea textarea = null;
            string define = "textarea[id*='comments'][class*='form-control']";
            By by = By.CssSelector(define);
            textarea = (snotextarea)Base.SNGObject("Comment", "textarea", by, null, snobase.MainFrame);
            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_Comment_NoMainFrame()
        {
            snotextarea textarea = null;
            string define = "textarea[id*='comments'][class*='form-control']";
            By by = By.CssSelector(define);
            textarea = (snotextarea)Base.SNGObject("Comment", "textarea", by);
            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Comment()
        {
            snobutton button = null;
            string define = "feedback_button";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Comment", "button", by, null, snobase.MainFrame);
            return button;
        }

        #endregion End - Controls

        #region Methods
        //***********************************************************************************************************************************
        public bool SelectRating(int index)
        {
            bool flag = false;
            ReadOnlyCollection<IWebElement> eles;
            string define = "label[data-index][class*='icon-star-empty']";
            int count = 1;
            eles = Base.Driver.FindElements(By.CssSelector(define));
            if (eles.Count > 0)
            {
                foreach (IWebElement e in eles)
                {
                    if (count == index && index <= 5)
                    {
                        e.Click();
                        flag = true;
                        break;
                    }
                    else
                    {
                        count = count + 1;
                    }
                }
            }
            return flag;
        }

        public bool SelectHelpful(string choice)
        {
            bool flag = true;
            switch (choice.ToLower())
            {
                case "no":
                    snobutton button = Button_Helpful_No();
                    if (button.Existed)
                    {
                        button.Click(true);
                        WaitLoading();
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("No button does NOT exist");
                    }
                    break;
                
                case "yes":
                    button = Button_Helpful_Yes();
                    if (button.Existed)
                    {
                        button.Click(true);
                        WaitLoading();
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("Yes button does NOT exist");
                    }
                    break;
            }
            return flag;
        }

        #endregion End - Methods
    }
}
