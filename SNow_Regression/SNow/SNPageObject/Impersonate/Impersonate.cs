﻿using System;
using OpenQA.Selenium;
using System.Threading;

namespace SNow
{
    public class Impersonate
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public Impersonate(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        #region Private methods

        private snoelement GMain() 
        {
            snoelement ele = null;
            
            try
            {
                string define = "div[class*='impersonate-dialog']";
                By by = By.CssSelector(define);
                ele = (snoelement)Base.SNGObject("Impersonate main", "element", by);
            }
            catch 
            { 
                ele = null;
                Console.WriteLine("***[ERROR]: Cannot get control <Impersonate main>");
            }

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private void WaitSearching()
        {
            string define = ".select2-searching";
            By by = By.CssSelector(define);
            snoelement searching = (snoelement)Base.SNGObject("Searching", "element", by, null, null, true);
            while (searching.Existed)
            {
                Thread.Sleep(1000);
            }
        }
        
        #endregion End - Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine("Impersonate page wait loading...");
            Base.PageWaitLoading(true);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SelectUserOnRecentList(string user)
        {
            bool flag = true;
            snoelement parent = RecentUserContent();
            snoelement main = GMain();
            if (parent.Existed)
            {
                string define = "a";
                By by = By.CssSelector(define);
                snoelements userlist = Base.SNGObjects("Recent user list", by, parent);
                
                flag = userlist.ClickOnItem(user, true);
                
                if (flag)
                {
                    try
                    {
                        while (main.Existed == true)
                        {
                            Thread.Sleep(1000);
                        }
                    }
                    catch { }
                }
                else Console.WriteLine("-*-ERROR: Error when click on recent user.");
            }
            else
            {
                flag = false;
                Console.WriteLine("-*-ERROR: Cannot get recent user content.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndSelectUser(string user)
        {
            bool flag = true;
            string sU = null;
            string sE = null;

            snoelement main = GMain();
            snoelement searchforuser = SearchForUser();
            if (searchforuser.Existed)
            {
                if (user.Contains(";"))
                {
                    string[] arr = user.Split(';');
                    sU = arr[0].Trim();
                    sE = arr[1].Trim();
                }
                else
                {
                    sU = user;
                    sE = null;
                }
                flag = searchforuser.Click();
                if (flag)
                {
                    snotextbox searchuser = Textbox_SearchUser();
                    if (searchuser.Existed)
                    {
                        flag = searchuser.SetText(sU);
                        if (flag)
                        {
                            try
                            {
                                WaitSearching();
                            }
                            catch { }
                            snoelement usercontent = UserContent();
                            flag = usercontent.Existed;
                            if (flag)
                            {
                                string define = "li>div>div";
                                By by = By.CssSelector(define);
                                snoelements userlist = Base.SNGObjects("User list", by, usercontent);
                                flag = userlist.MyList.Count > 0;
                                if (flag)
                                {
                                    string u = string.Empty;
                                    if (sE != null && sE != string.Empty)
                                    {
                                        u = sE;
                                    }
                                    else { u = sU; }
                                    flag = userlist.ClickOnItem(u);
                                    if (flag)
                                    {
                                        try
                                        {
                                            while (main.Existed == true)
                                            {
                                                Thread.Sleep(1000);
                                            }
                                        }
                                        catch { }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            else
            {
                flag = false;
            }

            return flag;
        }

        #endregion End - Public methods

        #region Control methods

        #region Textbox

        private snotextbox Textbox_SearchUser() 
        {
            snotextbox textbox = null;
            string define = "#s2id_autogen2_search";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Search user", "textbox", by);
            
            return textbox;
        }
        
        #endregion End - Textbox

        #region Element

        private snoelement RecentUserContent()
        {
            snoelement ele = null;
            
            string define = ".panel.panel-default.ng-scope";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Recent user content", "element", by);

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement SearchForUser()
        {
            snoelement ele = null;
            
            string define = ".//*[starts-with(@id,'select') and contains(@id, '-chosen-')]";
            By by = By.XPath(define);
            ele = (snoelement)Base.SNGObject("Search for user", "element", by);

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement UserContent()
        {
            snoelement ele = null;
            string define = "#select2-results-2";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("User content", "element", by);

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement Title()
        {
            snoelement ele = null;
            string define = "#impersonate_title";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Title", "element", by);

            return ele;
        }

        #endregion End - Element

        #endregion End - Control methods
    }
}
