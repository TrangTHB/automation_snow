﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class Login
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public Login(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        #region Private methods

        private snoelement GLoginMain() 
        {
            snoelement ele = null;
            
            try
            {
                string define = ".login";
                By by = By.CssSelector(define);
                ele = (snoelement)Base.SNGObject("Login main", "element", by, null, snobase.MainFrame);
            }
            catch 
            { 
                ele = null;
                Console.WriteLine("***[ERROR]: Cannot get control <Login main>");
            }

            return ele;
        }

        #endregion End - Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine("Login page wait loading...");
            Base.PageWaitLoading();
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool LoginToSystem(string user, string pwd)
        {
            bool flag = true;
            string isGlobalPass = string.Empty;
            snotextbox uname = null;
            snotextbox upwd = null;
            snobutton blogin = null;

            isGlobalPass = Base.UseGlobalPass;

            if (isGlobalPass.Trim().ToLower() == "yes")
            {
                uname = Textbox_UserName_Global();
                flag = uname.Existed;
                if (flag)
                {
                    flag = uname.SetText(user);
                    if (flag)
                    {
                        upwd = Textbox_Password_Global();
                        flag = upwd.Existed;
                        if (flag)
                        {
                            upwd.MyElement.SendKeys(Base.DecryptString(pwd));
                            if (flag)
                            {
                                blogin = Button_Login_Global();
                                flag = blogin.Existed;
                                if (flag)
                                {
                                    flag = blogin.Click();
                                    if (!flag) { Console.WriteLine("-*-ERROR: Cannot click on login button."); }
                                }
                                else
                                {
                                    Console.WriteLine("-*-ERROR: Cannot get login button.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("-*-ERROR: Cannot populate password.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("-*-ERROR: Cannot get password text box.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("-*-ERROR: Cannot populate user name.");
                    }
                }
                else
                {
                    Console.WriteLine("-*-ERROR: Cannot get user name text box.");
                }
            }
            else
            {
                uname = Textbox_UserName();
                flag = uname.Existed;
                if (flag)
                {
                    flag = uname.SetText(user);
                    if (flag)
                    {
                        upwd = Textbox_Password();
                        flag = upwd.Existed;
                        if (flag)
                        {
                            flag = upwd.SetText(pwd);
                            if (flag)
                            {
                                blogin = Button_Login();
                                flag = blogin.Existed;
                                if (flag)
                                {
                                    flag = blogin.Click();
                                    if (!flag) { Console.WriteLine("-*-ERROR: Cannot click on login button."); }
                                }
                                else
                                {
                                    Console.WriteLine("-*-ERROR: Cannot get login button.");
                                }
                            }
                            else
                            {
                                Console.WriteLine("-*-ERROR: Cannot populate password.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("-*-ERROR: Cannot get password text box.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("-*-ERROR: Cannot populate user name.");
                    }
                }
                else
                {
                    Console.WriteLine("-*-ERROR: Cannot get user name text box.");
                }
            }

            return flag;
        }

        #endregion End - Public methods

        #region Control methods

        #region Textbox

        private snotextbox Textbox_UserName() 
        {
            snotextbox textbox = null;
            snoelement main = GLoginMain();
            if (main.Existed)
            {
                string define = "user_name";
                By by = By.Id(define);
                textbox = (snotextbox)Base.SNGObject("User name", "textbox", by, main, snobase.MainFrame);
            }
            else 
            {
                Console.WriteLine("***[ERROR]: Cannot get login main control.");
            }
            
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snotextbox Textbox_Password()
        {
            snotextbox textbox = null;
            snoelement main = GLoginMain();
            if (main.Existed)
            {
                string define = "user_password";
                By by = By.Id(define);
                textbox = (snotextbox)Base.SNGObject("Password", "textbox", by, main, snobase.MainFrame);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get login main control.");
            }

            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snotextbox Textbox_UserName_Global()
        {
            snotextbox textbox = null;

            string define = "USER";
            By by = By.Id(define);
            textbox = (snotextbox)Base.SNGObject("User name", "textbox", by);

            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snotextbox Textbox_Password_Global()
        {
            snotextbox textbox = null;

            string define = "input[name='PASSWORD']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Password", "textbox", by);

            return textbox;
        }

        #endregion End - Textbox

        #region Button

        private snobutton Button_Login()
        {
            snobutton button = null;
            snoelement main = GLoginMain();
            if (main.Existed)
            {
                string define = "sysverb_login";
                By by = By.Id(define);
                button = (snobutton)Base.SNGObject("Login", "button", by, main, snobase.MainFrame);
            }
            else
            {
                Console.WriteLine("***[ERROR]: Cannot get login main control.");
            }

            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Login_Global()
        {
            snobutton button = null;

            string define = "loginbtn";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Login", "button", by);

            return button;
        }

        #endregion End - Button

        #endregion End - Control methods
    }
}
