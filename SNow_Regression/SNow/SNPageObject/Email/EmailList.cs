﻿using System;
using OpenQA.Selenium;
using System.Threading;

namespace SNow
{
    public class EmailList : ItilList
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public EmailList(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Public methods

        public bool EmailFilter(string condition)
        {
            bool flag = true;
            string temp = "Created;on;Today|or|Created;on;Yesterday|and|" + condition;
            flag = Filter(temp);
            if (flag)
            {
                string define = ".//a[text()='Created']";
                snoelement ele = null;
                snoelements eles = Base.SNGObjects("list", By.XPath(define), null, snobase.MainFrame);
                foreach (Auto.oelement e in eles.MyList) 
                {
                    if (e.MyElement.Displayed) 
                    {
                        ele = new snoelement("Create", Base.Driver, e.MyElement);
                        break;
                    }
                }
                flag = ele.Existed;
                if (flag)
                {
                    snoelement parent = (snoelement)Base.SNGObject("parent", "element", By.XPath(".."), ele, snobase.MainFrame);
                    while (parent.MyElement.TagName != "th")
                    {
                        parent = (snoelement)Base.SNGObject("parent", "element", By.XPath(".."), parent, snobase.MainFrame);
                    }
                    if (parent.MyElement.GetAttribute("sort_dir") != "DESC")
                    {
                        ele.Click(true);
                        Thread.Sleep(2000);
                        WaitLoading();
                    }
                }
                else Console.WriteLine("-*-[Info]: Cannot get created control.");
            }
            return flag;
        }
        
        #endregion End - Public methods
    }
}
