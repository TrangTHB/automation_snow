﻿using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace SNow
{
    public class Email : Itil
    {
        private snobase _snobase = null;
        private string _name = string.Empty;

        public Email(snobase obase, string name) : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #region Text box control

        //***********************************************************************************************************************************

        private snotextbox Textbox_Subject()
        {
            snotextbox textbox = null;
            string define = "Subject";
            object obj = Base.GControlByLabel(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        //***********************************************************************************************************************************

        #endregion Text box control

        #region Textarea control

        private snotextarea Textarea_Recipients()
        {
            snotextarea textarea = null;
            string define = "Recipients";
            object obj = Base.GControlByLabel(define, null);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        #endregion Textarea control

        #region Other

        private snoelement Link_PreviewHtmlBody()
        {
            snoelement ele = null;
            string define = ".related_links_container a";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Action Menu", "element", by, null, snobase.MainFrame);
            return ele;
        }

        private snoelement Dialog_PreviewEmail()
        {
            snoelement ele = null;

            string define = "#preview_email div[class='modal-content']";

            By by = By.CssSelector(define);

            Base.Driver.SwitchTo().DefaultContent();
            Base.Driver.SwitchTo().Frame(snobase.MainFrame);
            Thread.Sleep(2000);
            Base.Driver.SwitchTo().Frame("email_preview_iframe");
  

            ele = new snoelement("Dialog Preview", Base.Driver, by, null, snobase.MainFrame);

            return ele;
        }

        //public oelement GLink_ByText(string text)
        //{
        //    oelement ele = null;
        //    bool flag = true;
        //    //string define = "div[id='email_preview_content']>a";
        //    string define = "div>a";
        //    oelementlist eles = null;

        //    flag = Base.SwitchToDefaultContent();
        //    if (flag == true)
        //    {
        //        flag = Base.SwitchToFrame(mainFrame);
        //        if (flag == true)
        //        {
        //            Base.SwitchToFrame("email_preview_iframe");
        //            eles = Base.GObjects("GLink_ByText", By.CssSelector(define));
        //            foreach (oelement e in eles.MyList)
        //            {
        //                System.Console.WriteLine("link ===" + e.MyEle.GetAttribute("href"));
        //                if (e.Text == text)
        //                {
        //                    ele = e;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return ele;
        //}

        #endregion Other

        #region Button control

        private snobutton Button_CloseBody()
        {
            snobutton bt = null;
            string define = "#preview_email div[class='modal-content'] button[class='btn btn-icon close icon-cross']";
            By by = By.CssSelector(define);

            Base.Driver.SwitchTo().DefaultContent();
            Base.Driver.SwitchTo().Frame(snobase.MainFrame);
            Base.Driver.SwitchTo().Frame("email_preview_iframe");

            bt = new snobutton("Close Body", Base.Driver, by, null, snobase.MainFrame);
            return bt;
        }

        #endregion Button control

        #region Methods

        //***********************************************************************************************************************************

        //public void WaitLoading()
        //{
        //    System.Console.WriteLine("Email page wait loading...");
        //    Base.PageWaitLoading();
        //    Thread.Sleep(4000);
        //}

        public bool VerifySubject(string expected)
        {
            bool flag = true;
            snotextbox textbox = Textbox_Subject();
            flag = textbox.Existed;
            if (flag)
            {
                flag = textbox.VerifyCurrentValue(expected);
            }
            return flag;
        }

        public bool VerifyRecipient(string recipient)
        {
            bool flag = true;
            snotextarea textarea = Textarea_Recipients();
            flag = textarea.Existed;
            if (flag)
            {
                flag = textarea.VerifyCurrentValue("@@" + recipient);
            }
            return flag;
        }

        public bool ClickOnPreviewHtmlBody()
        {
            bool flag = true;
            snoelement ele = Link_PreviewHtmlBody();
            ele.MoveToElement();
            flag = ele.Existed;
            if (flag)
            {               
                System.Console.WriteLine("The 'Preview html body' link is FOUND");
                flag = ele.Click();
            }
            else System.Console.WriteLine("The 'Preview html body' link is NOT found");
            return flag;
        }

        public bool ClickOnCloseButton()
        {
            bool flag = true;
            snobutton bt = Button_CloseBody();
            flag = bt.Existed;
            if (flag)
            {
                flag = bt.Click();
            }
            return flag;
        }

        public bool VerifyEmailBody(string expected)
        {
            bool flag = true;
            snoelement dialog = Dialog_PreviewEmail();
            flag = dialog.Existed;
            if (flag)
            {
                string[] arr = null;
                if (expected.Contains("|"))
                {
                    arr = expected.Split('|');
                }
                else
                {
                    arr = new string[] { expected };
                }

                bool flagF = true;

                foreach (string value in arr)
                {
                    flagF = VerifyBody(value);
                    if (!flagF)
                    {
                        System.Console.WriteLine("***[ERROR]: Not found (" + value + ") in email body.");

                        if (flag) flag = false;
                    }
                    else
                    {
                        System.Console.WriteLine("***[Found]: (" + value + ") in email body.");
                    }
                }
            } else System.Console.WriteLine("***The Dialog Preview is NOT found");
            return flag;
        }

        private bool VerifyBody(string value)
        {
            bool flag = false;
            string[] arr = null;
            string define = string.Empty;

            snoelement dl = Dialog_PreviewEmail();

            //if (value.Contains("="))
            //{
            //    arr = value.Split('=');
            //    define = "body>div:not([class]):not([id]) b, body>p b";
            //}
            //else
            //{
            //    arr = new string[] { value };
            //    define = "body>div:not([class]):not([id]), body>p";
            //}
            arr = new string[] { value };
            define = "body>div:not([class]):not([id]), body>p";
            Base.Driver.SwitchTo().Frame("email_preview_iframe");
            
            string frame = snobase.MainFrame + ";" + "email_preview_iframe";
            
            snoelements list = Base.SNGObjects("Label list", By.CssSelector(define), null, frame);

            foreach (Auto.oelement e in list.MyList)
            {
                System.Console.WriteLine("Runtime: " + e.MyText.Trim());
                if (e.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(arr[0].Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                {
                    flag = true;
                    break;
                    //if (value.Contains("="))
                    //{
                    //    IWebElement pr = e.MyElement.FindElement(By.XPath(".."));
                    //    snoelement parent = new snoelement("parent", Base.Driver, pr);
                    //    string temp = parent.MyText.Trim().Replace(arr[0], "");
                    //    temp = temp.Replace("\r\n", " ");
                    //    System.Console.WriteLine("Runtime: " + temp);
                    //    if (temp.Trim().ToLower().Contains(arr[1].Trim().ToLower()))
                    //        flag = true;
                    //    break;
                    //}
                    //else
                    //{
                    //    flag = true;
                    //    break;
                    //}
                }
            }
            return flag;
        }

        //public bool VerifyURL(string requestedItemId, string containText)
        //{
        //    bool flag = true;
        //    oelement link = GLink_ByText(requestedItemId);
        //    flag = link.Existed;
        //    if (flag)
        //    {
        //        string url = link.MyEle.GetAttribute("href");
        //        System.Console.WriteLine(url);
        //        if (!url.Contains(containText))
        //        {
        //            flag = false;
        //        }
        //    }
        //    else
        //    {
        //        System.Console.WriteLine("***[ERROR]: Not found URL in email body.");
        //    }
        //    return flag;
        //}

        //// Enter 1 of 3 Email URL Link Type: approve, reject, detail
        //public string GetEmailURL(string requestedItemId, string linkType)
        //{
        //    bool flag = true;
        //    oelement link = null;
        //    string uRL = "";

        //    switch (linkType.ToLower())
        //    {
        //        case "approve":
        //            link = GLink_ByText("Click here to approve " + requestedItemId);
        //            break;
        //        case "reject":
        //            link = GLink_ByText("Click here to reject " + requestedItemId);
        //            break;
        //        case "detail":
        //            link = GLink_ByText(requestedItemId);
        //            break;
        //    }
        //    flag = link.Existed;
        //    if (flag)
        //    {
        //        uRL = link.MyEle.GetAttribute("href");
        //    }
        //    return uRL;
        //}

        //***********************************************************************************************************************************

        #endregion Methods
    }
}