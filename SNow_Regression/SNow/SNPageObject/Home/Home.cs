﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;

namespace SNow
{
    public class Home
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public Home(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor

        #region Private methods

        //private snoelement GHeader() 
        //{
        //    snoelement ele = null;
            
        //    try
        //    {
        //        string define = "body header";
        //        By by = By.CssSelector(define);
        //        ele = (snoelement)Base.SNGObject("Header", "element", by);
        //    }
        //    catch 
        //    { 
        //        ele = null;
        //        Console.WriteLine("***[ERROR]: Cannot get control <Header>");
        //    }

        //    return ele;
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement GNav()
        {
            snoelement ele = null;

            try
            {
                string define = "body nav";
                By by = By.CssSelector(define);
                ele = (snoelement)Base.SNGObject("Nav", "element", by);
            }
            catch
            {
                ele = null;
                Console.WriteLine("***[ERROR]: Cannot get control <Nav>");
            }

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement GMain()
        {
            snoelement ele = null;

            try
            {
                string define = "body main";
                By by = By.CssSelector(define);
                ele = (snoelement)Base.SNGObject("Main", "element", by);
            }
            catch
            {
                ele = null;
                Console.WriteLine("***[ERROR]: Cannot get control <Main>");
            }

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool ReturnLoginUser(string loginUser)
        {
            bool flag = true;

            flag = MenuUserInfo_ClickOnItem("Impersonate User");

            if (flag)
            {
                Impersonate impdialog = new Impersonate(Base);
                impdialog.WaitLoading();
                flag = impdialog.SelectUserOnRecentList(loginUser);
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool MenuUserInfo_ClickOnItem(string item)
        {
            bool flag = true;
            snobutton bt = Button_UserInfo();
            if (bt.Existed)
            {
                flag = bt.Click(true);
                if (flag)
                {
                    snoelement menu = Menu_UserInfo();
                    if (menu.Existed)
                    {
                        By by = By.CssSelector("li>a");
                        snoelements items = Base.SNGObjects("Menu user info items", by, menu);
                        flag = items.ClickOnItem(item, true);
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("-*-ERROR: Cannot get menu user info.");
                    }
                }
                else 
                {
                    Console.WriteLine("***[ERROR]: Error when click on user button.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-ERROR: Cannot get button user info.");
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool SearchAndSelectUser(string user)
        {
            bool flag = true;

            flag = MenuUserInfo_ClickOnItem("Impersonate User");

            if (flag)
            {
                Impersonate impdialog = new Impersonate(Base);
                impdialog.WaitLoading();

                flag = impdialog.SearchAndSelectUser(user);
            }

            return flag;
        }

        private bool LeftMenuValidate(string value)
        {
            bool flag = false;
            string define = ".module-node";
            snoelement main = GNav();

            if (Base.SwitchToPage(0))
            {
                snoelements items = Base.SNGObjects("Menu items", By.CssSelector(define), main);
                foreach (Auto.oelement e in items.MyList)
                {
                    if (e.MyText.Trim().ToLower().Equals(value.ToLower()))
                    {
                        flag = true;
                        System.Console.WriteLine("-*-[Pass]: Found (" + value + ") item.");
                        break;
                    }
                }
            }
            else flag = false;
            if (!flag) System.Console.WriteLine("-*-[ERROR]: Not found (" + value + ") item.");
            return flag;
        }

        #endregion End - Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine("Home page wait loading...");
            Base.PageWaitLoading();
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool ImpersonateUser(string user, [Optional] bool returnLoginUser, [Optional] string loginUser, [Optional] bool goPortal)
        {
            System.Console.WriteLine("***[Call Funtion]: ImpersonateUser.");
            bool flag = true;

            if (returnLoginUser)
            {
                flag = ReturnLoginUser(loginUser);
                if (flag)
                   Thread.Sleep(5000);
            }

            if (flag)
            {
                flag = SearchAndSelectUser(user);
                if (flag && !goPortal)
                {
                    Thread.Sleep(5000);

                    string[] arr = null;
                    if (user.Contains(";"))
                    {
                        arr = user.Split(';');
                    }
                    else
                        arr = new string[] { user };

                    string temp = UserFullName().MyText;
                    if (temp.ToLower() != arr[0].ToLower())
                    {
                        flag = false;
                    }
                }
                else
                    Thread.Sleep(10000);
            }

            if (!flag)
                System.Console.WriteLine("***ERROR: Cannot impersonate user.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool LeftMenuItemSelect(string module, string item, [Optional] int expindex)
        {
            bool flag = true;
            snotextbox filter = Textbox_Filter();
            flag = filter.Existed; 
            
            if (flag)
            {
                flag = filter.SetText(module);
                
                if (flag)
                {
                    Thread.Sleep(2000);
                    WaitLoading();
                    snoelement main = GNav();
                    flag = main.Existed;
                    
                    if(flag)
                    {
                        string define = "[class='sn-aside-filler navigator-view sn-widget-list_indentation ng-isolate-scope'] li:not([style='display: none;']) ul>li:not([style='display: none;']) a div div[class^='sn-widget-list-title']";
                        By by = By.CssSelector(define);
                        
                        snoelements items = Base.SNGObjects("Menu items", by, main);

                        flag = items.HaveItemsInlist(item, ";");

                        if (flag)
                        {
                            flag = items.ClickOnItem(item, false, false, expindex);
                            if (!flag)
                            {
                                Thread.Sleep(2000);
                                flag = items.ClickOnItem(item, false, false, expindex);
                            }

                        }
                    }
                    else Console.WriteLine("***[ERROR]: Nav main not existed.");

                }
            }
            else
            {
                Console.WriteLine("***[ERROR]: Textbox filter not existed.");
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SystemSetting([Optional] string domain, [Optional] bool turnOff)
        {
            System.Console.WriteLine("***[Call Funtion]: System setting.");
            bool flag = true;
            Thread.Sleep(2000);
            snobutton bt = Button_SystemSetting();
            if (bt.Existed)
            {
                flag = bt.Click(true);
                if (flag)
                {
                    SystemSetting system = new SystemSetting(Base);
                    if (system.Existed())
                    {
                        flag = system.Setting(domain, turnOff);
                        if (flag) { WaitLoading(); }
                    }
                    else { flag = false; }
                }
            }
            else { flag = false; }
            if (!flag)
                System.Console.WriteLine("***ERROR: Cannot setting system.");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_User_FullName(string name)
        {
            
            bool flag = true;
            snoelement userFullName = UserFullName();

            string temp = UserFullName().MyText.ToLower().Trim();
            flag = name.ToLower().Trim().Equals(temp);
            if (!flag)
            {
                System.Console.WriteLine("***User full name is NOT correct. Expect ["+ name +"]. Run time ["+ temp +"]");
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public void Logout()
        {
            string temp = Base.GData("Url");
            Base.ClearCache();
            Thread.Sleep(2000);
            Base.Driver.Navigate().GoToUrl(temp);
            Thread.Sleep(2000);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SwitchToPortal()
        {
            System.Console.WriteLine("***[Call Funtion]: SwitchToPortal.");
            bool flag = true;
            flag = MenuUserInfo_ClickOnItem("Portal");
            if (!flag)
                System.Console.WriteLine("***ERROR: Cannot switch to portal.");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool LeftMenuItemValidate(string module, string itemList)
        {
            bool flag = true;
            if (itemList.Equals(null))
            {
                flag = false;
            }
            else
            {
                snotextbox filter = Textbox_Filter();
                if (filter.Existed)
                {
                    flag = filter.SetText(module);
                    if (flag)
                    {
                        WaitLoading();

                        string[] arr = null;
                        if (itemList.Contains(";"))
                        {
                            arr = itemList.Split(';');
                        }
                        else
                        {
                            arr = new string[] { itemList };
                        }
                        bool flagF = true;

                        foreach (string value in arr)
                        {
                            flagF = LeftMenuValidate(value);
                            if (!flagF && flag) flag = false;
                        }
                    }
                }
                else
                {
                    flag = false;
                }
            }
            return flag;
        }

        #endregion End - Public methods

        #region Control methods

        #region Textbox

        public snotextbox Textbox_Filter()
        {
            snotextbox textbox = null;
            snoelement main = GNav();

            if (main.Existed)
            {
                string define = "filter";
                By by = By.Id(define);
                textbox = (snotextbox)Base.SNGObject("Filter", "textbox", by, main);
            }
            else 
            {
                Console.WriteLine("***[ERROR]: Cannot get nav main control.");
            }
            
            return textbox;
        }

        #endregion End - Textbox

        #region Button

        private snobutton Button_UserInfo()
        {
            snobutton button = null;
            //snoelement main = GHeader();
            //if (main.Existed)
            //{
            //    string define = "user_info_dropdown";
            //    By by = By.Id(define);
            //    button = (snobutton)Base.SNGObject("User info", "button", by, main);
            //}
            //else
            //{
            //    Console.WriteLine("***[ERROR]: Cannot get header main control.");
            //}
            string define = "user_info_dropdown";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("User info", "button", by);
            return button;
        }

        private snobutton Button_SystemSetting()
        {
            snobutton button = null;
            //snoelement main = GHeader();
            //if (main.Existed)
            //{
            //    string define = "nav-settings-button";
            //    By by = By.Id(define);
            //    button = (snobutton)Base.SNGObject("System setting", "button", by, main);
            //}
            //else
            //{
            //    Console.WriteLine("***[ERROR]: Cannot get header main control.");
            //}
            string define = "nav-settings-button";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("System setting", "button", by);
            return button;
        }

        #endregion End - Button

        #region Element

        private snoelement Menu_UserInfo()
        {
            snoelement ele = null;
            string define = ".dropdown.pull-left.open ul[class='dropdown-menu']";
            By by = By.CssSelector(define);
            
            ele = (snoelement)Base.SNGObject("User info", "element", by);

            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement UserFullName()
        {
            snoelement ele = null;
            string define = ".user-name.hidden-xs.hidden-sm";
            By by = By.CssSelector(define);

            ele = (snoelement)Base.SNGObject("User full name", "element", by);

            return ele;
        }
        #endregion End - Element

        #endregion End - Control methods
    }
}
