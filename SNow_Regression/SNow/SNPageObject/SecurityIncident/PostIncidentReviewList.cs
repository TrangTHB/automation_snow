﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class PostIncidentReviewList : ItilList
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public PostIncidentReviewList(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Button
        public snobutton Button_TakeAssessment(string IncidentId)
        {
            snobutton bt = null;

            string define = "button[id^='take_survey']";
            snoelements list = Base.SNGObjects("list of Take Assessment buttons", By.CssSelector(define), null, snobase.MainFrame);
            if (list.Count > 0)
            {
                foreach (Auto.oelement e in list.MyList)
                {
                    IWebElement parent1 = e.MyElement.FindElement(By.XPath(".."));
                    IWebElement parent2 = parent1.FindElement(By.XPath(".."));
                    IWebElement parent3 = parent2.FindElement(By.XPath(".."));
                    if (parent3 != null)
                    {
                        string assessmentFor = parent3.Text;
                        if (assessmentFor.Contains(IncidentId))
                        {
                            bt = new snobutton("Take Assessment button", Base.Driver, e.MyElement);
                        }
                    }
                }
            }
            return bt;
        }
        #endregion Button
    }
}
