﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class PostIncidentReview : Itil
    {
        #region Variables

        private snobase _snobase = null;
        
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public PostIncidentReview(snobase obase, string name): base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Public methods

        //----------------------------------------------------------------------------------------------------
        public bool InputAllAssessmentFields(string InputText, string SelectAnswer)
        {
            bool flag = true;
            string AnswerValue = string.Empty;

            switch (SelectAnswer)
            {
                case "Yes":
                    AnswerValue = "1";
                    break;
                case "No":
                    AnswerValue = "0";
                    break;
                case "Not Applicable":
                    AnswerValue = "-1";
                    break;
            }

            string define = "input[id^='ASMTQUESTION'][value='" + AnswerValue + "']";

            snoelements RadioList = Base.SNGObjects("Radio list", By.CssSelector(define), null, snobase.MainFrame, true);

            if (RadioList.Count > 0)
            {
                foreach (Auto.oelement Radio in RadioList.MyList)
                {
                    flag = Radio.Click(true);
                    if (!flag) break;
                }
            }

            define = "textarea[id^='ASMTQUESTION']";

            snoelements TextareaList = Base.SNGObjects("Textarea list", By.CssSelector(define), null, snobase.MainFrame, true);

            if (TextareaList.Count > 0)
            {
                foreach (Auto.oelement Textarea in TextareaList.MyList)
                {
                    Textarea.MyElement.SendKeys(InputText);
                    flag = (Textarea.MyText == InputText);
                    if (!flag)
                    {
                        flag = true;
                        break;
                    }
                }
            }
            return flag;

        }

        #endregion End - Public methods

        #region Button
        public snobutton Button_SubmitAssessment()
        {
            snobutton bt = null;
            string define = "submit";
            bt = (snobutton)Base.SNGObject("Submit Assessment", "button", By.Id(define), null, snobase.MainFrame);
            return bt;
        }

        #endregion
    }
}
