﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class SecurityIncident : Itil
    {
        #region Variables

        private snobase _snobase = null;
        
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public SecurityIncident(snobase obase, string name): base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Lookups
        //-----------------------------------------------------------------------------------------------------------------------------------
        //public snolookup Lookup_AddRequestAssessmentName()
        //{
        //    snolookup lk = null;
        //    string define = "input[id^='sys_display'][id$='pir_respondents']";
        //    lk = (snolookup) Base.SNGObject("Lock request assessments list", "lookup", By.CssSelector(define), null, snobase.MainFrame);
        //    return lk;
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion End - Lookups

        #region Button
        //-----------------------------------------------------------------------------------------------------------------------------------
        //public snobutton Button_LockRequestAssessmentList()
        //{
        //    snobutton bt = null;
        //    string define = "button[id$='pir_respondents_lock']";
        //    bt = (snobutton) Base.SNGObject("Lock request assessments list", "button", By.CssSelector(define), null, snobase.MainFrame);
        //    return bt;
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //public snobutton Button_UnlockRequestAssessmentList()
        //{
        //    snobutton bt = null;
        //    string define = "button[id$='pir_respondents_unlock']";
        //    bt = (snobutton)Base.SNGObject("Unlock request assessments list", "button", By.CssSelector(define), null, snobase.MainFrame);
        //    return bt;
        //}
        #endregion Button

        #region Public Methods
        //-----------------------------
        public bool CheckCurrentState(string currState)
        {
            snoelement ele = null;
            string eleDefine = ".//*[@class='process-breadcrumb process-breadcrumb-border']/li[contains(@data-state,'current')]/a";
            
            ele = (snoelement)Base.SNGObject("CurrentState", "element", By.XPath(eleDefine), null, snobase.MainFrame);
            if (ele != null)
                if (ele.MyText != currState)
                    return false;
                else return true;
            else return false;
        }
        #endregion

    }
}
