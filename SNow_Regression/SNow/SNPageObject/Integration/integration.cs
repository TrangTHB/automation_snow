﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;

namespace SNow
{
    public class Integration
    {
        #region Variables

        private snobase _snobase = null;
        
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public Integration(snobase obase) 
        {
            Base = obase;
        }

        #endregion End - Constructor



        #region Public methods

        public bool Click_Edit_Or_Disable_Integration(string name, string type)
        {
            bool flag = false;
            string rowDefine = "div[class^='integration-row']:not([class*='head']):not([class*='footer'])";
            snoelements rows = Base.SNGObjects("rows", By.CssSelector(rowDefine), null, snobase.MainFrame);

            foreach (Auto.oelement r in rows.MyList)
            {
                string cellDefine = "div[class^='section']";
                snoelement ro = new snoelement("ro", Base.Driver, r.MyElement);
                snoelements cells = Base.SNGObjects("cells", By.CssSelector(cellDefine), ro, snobase.MainFrame);
                Auto.oelement ce = cells.MyList[0];
                snoelement cell = new snoelement("cell", Base.Driver, ce.MyElement);
                if (cell.MyText.Trim().ToLower() == name.Trim().ToLower())
                {
                    Auto.oelement cE = cells.MyList[2];
                    snoelement cEdit = new snoelement("cell edit", Base.Driver, cE.MyElement);
                    snoelement button = null;
                    if(type.Trim().ToLower() == "edit")
                        button = (snoelement)Base.SNGObject("button edit", "element", By.TagName("a"), cEdit, snobase.MainFrame);
                    else
                        button = (snoelement)Base.SNGObject("button edit", "element", By.TagName("button"), cEdit, snobase.MainFrame);

                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                    }
                    break;
                }
            }   

            return flag;
        }


        public bool Verify_Information_Integration(string name, string condition)
        {
            bool flag = false;
            string rowDefine = "div[class^='integration-row']:not([class*='head']):not([class*='footer'])";
            snoelements rows = Base.SNGObjects("rows", By.CssSelector(rowDefine), null, snobase.MainFrame);

            foreach (Auto.oelement r in rows.MyList)
            {
                string cellDefine = "div[class^='section']";
                snoelement ro = new snoelement("ro", Base.Driver, r.MyElement);
                snoelements cells = Base.SNGObjects("cells", By.CssSelector(cellDefine), ro, snobase.MainFrame);
                Auto.oelement ce = cells.MyList[0];
                snoelement cell = new snoelement("cell", Base.Driver, ce.MyElement);
                if (cell.MyText.Trim().ToLower() == name.Trim().ToLower())
                {
                    Auto.oelement cE = cells.MyList[1];
                    snoelement cEdit = new snoelement("cell edit", Base.Driver, cE.MyElement);
                    snoelement button = (snoelement)Base.SNGObject("button edit", "element", By.CssSelector("span button"), cEdit, snobase.MainFrame);
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        string peDefine = "div[class='popover-content'] div[class='job active']";
                        snoelements puItems = Base.SNGObjects("item", By.CssSelector(peDefine), null, snobase.MainFrame);
                        string activeItems = string.Empty;
                        foreach (Auto.oelement item in puItems.MyList)
                        {
                            activeItems = activeItems + item.MyText.Trim() + "|";
                        }

                        activeItems = activeItems.Substring(0, activeItems.Length - 1);
                        Console.WriteLine("//-- Active item string: [" + activeItems + "]");
                        if (activeItems.ToLower() == condition.ToLower())
                        {
                            flag = true;
                        }
                        else flag = false;
                    }
                    break;
                }
            }

            return flag;
        }

        #endregion End - Public methods

        #region Control methods



        #endregion End - Control methods
    }
}
