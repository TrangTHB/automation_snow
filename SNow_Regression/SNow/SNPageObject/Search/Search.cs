﻿using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;

namespace SNow
{
    public class Search
    {
        snobase Base = null;
        string myName = string.Empty;

        public Search(snobase _base, string name)
        {
            Base = _base;
            this.myName = name;
        }

        #region Label
       
        #endregion End - Label

        #region Button
        public snobutton Button_New(bool noWait)
        {
            snobutton bt = null;
            string define = "sysverb_new";
            bt = (snobutton)Base.SNGObject("New", "button", By.Id(define), null, null, noWait);
            return bt;
        }
        #endregion Button 

        #region Methods
        public void WaitLoading()
        {
            System.Console.WriteLine(this.myName + " page wait loading...");
            Base.PageWaitLoading(false, true);
        }
        #endregion End - Methods
    }
}
