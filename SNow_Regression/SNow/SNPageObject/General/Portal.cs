﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SNow
{
    public class Portal
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;
        private string tableD = "table[id]";
        private string rowD = "tbody tr[role='row'][class]";
        private string colD = "thead th[class]";
        private string cellD = "td";
        private string textReplace = ": activate to sort column ascending;: activate to sort column descending";
        private string columnAttribute = "aria-label";
        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public Portal(snobase obase, string name) 
        {
            Base = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Private methods

        private snoelement GSearchResultGroupByName(string groupName)
        {
            snoelement ele = null;
            string define = "#" + groupName.ToLower() + "Panel";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Search result of " + groupName + " group", "element", by);
            return ele;
        }

        private bool SelectCatalog(string cat, snoelements list) 
        {
            bool flag = false;

            foreach (Auto.oelement ele in list.MyList) 
            {
                if (Base.StringCompare(ele.MyText, cat)) 
                {
                    if (ele.Existed) 
                    {
                        flag = ele.Click();
                        break;
                    }
                }
            }

            return flag;
        }

        private bool VItemStage(string stage, snoelements list, [Optional] bool noVerifyColor)
        {
            bool flag = false;
            foreach (Auto.oelement ele in list.MyList)
            {
                if (ele.MyText.Trim().ToLower().Equals(stage.Trim().ToLower()))
                {
                    flag = true;
                    ele.Highlight();

                    if (flag && noVerifyColor)
                    {
                        flag = false;
                        if (ele.MyElement.GetCssValue("color").Trim() == "rgba(32, 80, 208, 1)")
                        {
                            Console.WriteLine("Runtime item stage is: [" + ele.MyText.Trim() + "]");
                            flag = true;
                        }
                    }
                    break;
                }
            }

            return flag;
        }

        private bool Search(string searchValue)
        {
            bool flag = true;
            snotextbox search = Textbox_Search();
            flag = search.Existed;
            if (flag)
            {
                flag = search.SetText(searchValue, true);

                if (flag)
                {
                    WaitLoading(true);
                }
                else System.Console.WriteLine("-*-[ERROR]: Cannot populate search value.");
            }
            else System.Console.WriteLine("-*-[ERROR]: Cannot get textbox search.");
            return flag;
        }
        #endregion End - Private methods

        #region Public methods

        public void WaitLoading([Optional] bool noMainFrame)
        {
            Console.WriteLine(_name + " page wait loading...");
            Base.PageWaitLoading(false, noMainFrame);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool OpenItemFromSearchGroupResult(string groupName, string findCondition)
        {
            bool flag = false;
            snoelement groupResult = GSearchResultGroupByName(groupName);
            if (groupResult.Existed)
            {
                string state = groupResult.MyElement.GetAttribute("class");
                if (state.Trim().ToLower().Contains("state-closed")) 
                {
                    groupResult.Click();
                }
                snoelement ele = null;
                string define = "a>div[class='self_help_description']";
                By by = By.CssSelector(define);
                snoelements list = Base.SNGObjects("Link list", by, groupResult);
                foreach (Auto.oelement e in list.MyList)
                {
                    if (e.MyText.Trim().ToLower().Contains(findCondition.Trim().ToLower())) 
                    {
                        ele = new snoelement("link", Base.Driver, e.MyElement);
                        Console.WriteLine("-*-[Info]: Found link contain [" + findCondition + "]");
                        flag = true;
                        break;
                    }
                }
                if (!flag) Console.WriteLine("-*-[Info]: NOT FOUND link contain [" + findCondition + "]");
                else 
                {
                    flag = ele.Existed;
                    if (flag) 
                    {
                        flag = ele.Click();
                        if(!flag)
                            Console.WriteLine("-*-[Info]: Error when click on link.");
                    }
                    else Console.WriteLine("-*-[Info]: The found link not existed.");
                }
            }
            else Console.WriteLine("-*-[Info]: Not found group [" + groupName + "] in search result.");
            return flag;
        }
        //--------------------------------------------------------------------------------------------
        public bool GlobalSearchItem(string search, bool enter)
        {
            bool flag = true;
            snotextbox texbox = Textbox_Global_Search();
            if (texbox.Existed)
            {
                flag = texbox.SetText(search, enter);
            }
            else
            {
                snobutton button = Button_Search();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        flag = texbox.SetText(search, enter);
                    }
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement GMenu(string menuName)
        {
            snoelement e = null;
            
            string define = ".nav.navbar-nav>li>a[id^='menu']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("List item menu", by);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.MyText.Trim().ToLower() == menuName.Trim().ToLower()) 
                {
                    e = new snoelement(menuName, Base.Driver, ele.MyElement);
                    break;
                }
            }

            if (e == null) { System.Console.WriteLine("Not found menu element [" + menuName + "]"); }
            return e;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement GLeftMenu(string menuName)
        {
            snoelement e = null;

            string define = "li>h3,.category-title.title>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("List item menu", by, null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList)
            {
                if (ele.MyText.Trim().ToLower() == menuName.Trim().ToLower())
                {
                    e = new snoelement(menuName, Base.Driver, ele.MyElement);
                    break;
                }
            }

            if (e == null) { System.Console.WriteLine("Not found menu element [" + menuName + "]"); }
            return e;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SelectCatalogItem(string path, string item) 
        {
            bool flag = true;
            string cat = "[id*='category-']";
            string it = "h3>a[href^='ticket_detail']";
            By by = By.CssSelector(cat);

            snoelements list = Base.SNGObjects("List cat", by, null, null, true);
            string[] arr = null;
            if (path.Contains(";"))
                arr = path.Split(';');
            else
                arr = new string[] { path };

            foreach (string str in arr) 
            {
                bool flagOK = SelectCatalog(str, list);
                Thread.Sleep(2000);
                if (flag && !flagOK)
                    flag = false;
            }

            if (flag) 
            {
                Thread.Sleep(5000);
                by = By.CssSelector(it);
                snoelements eits = Base.SNGObjects("items", by);
                snoelement eit = null;
                foreach (Auto.oelement e in eits.MyList) 
                {
                    if (e.MyText.Trim().ToLower() == item.Trim().ToLower()) 
                    {
                        eit = new snoelement("item", Base.Driver, e.MyElement);
                    }
                }

                if (eit != null && eit.Existed)
                    flag = eit.Click();
                else flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement GDashBoardMenu(string menuName, [Optional] int index)
        {
            snoelement e = null;
            
            string define = ".count_dash_label";
            snoelements list = Base.SNGObjects("List item menu", By.CssSelector(define));
            if (list.MyList.Count > 0)
            {
                int count = 0;
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.ToLower().Contains(menuName.ToLower()))
                    {
                        if (count == index)
                        {
                            e = new snoelement(menuName, Base.Driver, ele.MyElement);
                            break;
                        }
                        else
                        {
                            count = count + 1;
                        }
                    }
                }
            }
            else
            {
                System.Console.WriteLine("Not found any element.");
            }

            if (e == null) { System.Console.WriteLine("Not found dash board menu element [" + menuName + "]"); }
            return e;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool TKP_ValidateItemColumns(string column)
        {
            string temp = string.Empty;
            bool flag = true;
            string[] arr = null;
            string define = "[class^='checkout_title']";
            if (column.Contains(";"))
            {
                arr = column.Split(';');
            }
            else arr = new string[] { column};
            snoelement ele = (snoelement)Base.SNGObject("Column header", "element", By.CssSelector(define), null, snobase.MainFrame);
            if (ele.Existed)
            {
                
                string colDefine = "[class*='sc_os_']";
                snoelements colList = Base.SNGObjects("Column Define", By.CssSelector(colDefine), ele, snobase.MainFrame);
                if (colList != null && colList.MyList.Count > 0)
                {
                    foreach (string str in arr) 
                    {
                        bool flagF = true;
                        foreach (Auto.oelement e in colList.MyList) 
                        {
                            flagF = false;
                            if (Base.StringCompare(e.MyText, str)) 
                            {
                                flagF = true;
                                break;
                            }
                        }

                        if (!flagF) 
                        {
                            if(flag)
                                flag = false;
                            Console.WriteLine("***[NOT FOUND]: Column [" + str + "]");
                        }
                        else Console.WriteLine("***[Found]: Column [" + str + "]");    
                    }
                }
                else flag = false;
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool TKP_Validate_ItemStage(string stage, [Optional] bool noVerifyColor)
        {
            bool flag = true;

            snoelements eles = null;
            string define = "[id*='.text']";

            //Get & click Expand button
            snoelement expand = (snoelement)Base.SNGObject("Expand", "element", By.CssSelector("img[id^='filterimg']"), null, snobase.MainFrame);
            if (expand != null && expand.MyElement.GetAttribute("title") == "Expand")
            {
                expand.Click();
                WaitLoading();
            }
            eles = Base.SNGObjects("Stage", By.CssSelector(define), null, snobase.MainFrame);
            if (eles != null && eles.MyList.Count > 0)
            {
                string[] array = null;

                if (stage.Contains(";"))
                {
                    array = stage.Split(';');
                }
                else array = new string[] { stage };

                foreach (string sta in array)
                {
                    bool flagCheck;

                    flagCheck = VItemStage(sta, eles, noVerifyColor);

                    if (!flagCheck)
                    {
                        System.Console.WriteLine("***Failed: Not found stage:" + sta);
                        if (flag)
                            flag = false;
                    }
                    else System.Console.WriteLine("***Passed: Found stage:" + sta);
                }

            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndVerifyRow(string searchValue, string findConditions, [Optional] bool expectedNotFound)
        {
            bool flag = true;
            flag = Search(searchValue);
            if (flag)
            {
                Thread.Sleep(2000);
                snotable table = Table_List();
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;
                    

                    if (columns.Count > 0)
                    {
                        Auto.oelements rows = table.RowsList; 
                        if (rows != null && rows.MyList.Count > 0)
                        {
                            int rowIndex = table.RowIndex(findConditions, columns, rows);
                            if (rowIndex >= 0)
                            {
                                if (!expectedNotFound)
                                    Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ")");
                                else
                                {
                                    flag = false;
                                    Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ") - Expected: NOT FOUND.");
                                }
                            }
                            else
                            {
                                if (!expectedNotFound)
                                {
                                    flag = false;
                                    Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ")");
                                }
                                else
                                    Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ") as Expected.");
                            }
                        }
                        else
                        {
                            if (!expectedNotFound)
                            {
                                flag = false;
                            }
                            System.Console.WriteLine("-*-[Info]: Not have any row(s).");
                        }
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                    }
                }
                else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            }
            else System.Console.WriteLine("-*-[Info]: Error when search item.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndOpen(string searchValue, string findConditions, string columnClick)
        {
            bool flag = true;
            flag = Search(searchValue);
            if (flag)
            {
                Thread.Sleep(2000);
                snotable table = Table_List();
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;


                    if (columns.Count > 0)
                    {
                        Auto.oelements rows = table.RowsList;
                        if (rows != null && rows.MyList.Count > 0)
                        {
                            int rowIndex = table.RowIndex(findConditions, columns, rows);
                            if (rowIndex >= 0)
                            {
                                int columnIndex = table.ColumnIndex(columnClick, columns);
                                if (columnIndex >= 0)
                                {
                                    flag = table.CellClick(rowIndex, columnIndex, "a", columns, rows);
                                    if (!flag)
                                    {
                                        Console.WriteLine("***[Info]:Error when click on cell.");
                                    }

                                }
                                else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                            }
                            else
                            {
                                Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ")");
                            }
                        }
                        else
                        {
                            flag = false;
                            System.Console.WriteLine("-*-[Info]: Not have any row(s).");
                        }
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                    }
                }
                else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            }
            else System.Console.WriteLine("-*-[Info]: Error when search item.");

            return flag;
        }

        public bool SearchAndOpenCatalogItem(string item)
        {
            bool flag = true;
            snotextbox search = Textbox_Catalog_Search();
            flag = search.Existed;
            if (flag)
            {
                flag = search.SetText(item, true);
                if (flag)
                {
                    string it = "h3>a[href^='ticket_detail']";
                    snoelements items_list = Base.SNGObjects("Get all items", By.CssSelector(it));

                    if (items_list != null && items_list.Count > 0)
                    {
                        bool flagCheck = false;
                        foreach (Auto.oelement i in items_list.MyList)
                        {
                            if (item.Contains("@@"))
                            {
                                string t = item.Replace("@@", "");
                                if (i.MyText.ToLower().Contains(t.ToLower()))
                                {
                                    i.Click(true);
                                    flagCheck = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (i.MyText.ToLower().Equals(item.ToLower()))
                                {
                                    i.Click(true);
                                    flagCheck = true;
                                    break;
                                }
                            }
                        }
                        flag = flagCheck;
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("Not found any category.");
                    }
                }
            }
            return flag;
        }
        #endregion End - Public methods

        #region Controls

        #region Textbox

        public snotextbox Textbox_Global_Search()
        {
            snotextbox textbox = null;
            string define = ".header_search";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Global search", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_FirstName()
        {
            snotextbox textbox = null;
            string define = "First Name:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_LastName()
        {
            snotextbox textbox = null;
            string define = "Last Name:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_EmailAddress()
        {
            snotextbox textbox = null;
            string define = "Email Address:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_UserID()
        {
            snotextbox textbox = null;
            string define = "User ID:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_UserTelephoneNumber()
        {
            snotextbox textbox = null;
            string define = "User Telephone Number:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_BriefDescription()
        {
            snotextbox textbox = null;
            string define = "Brief Description";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_UserName()
        {
            snotextbox textbox = null;
            string define = "User Name:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_Search()
        {
            snotextbox textbox = null;
            string define = "div[id$='filter']>label>input";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Search", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_Catalog_Search()
        {
            snotextbox tb = null;
            string define = "sysparm_search";
            By by = By.Id(define);
            tb = (snotextbox)Base.SNGObject("Search", "textbox", by);
            return tb;
        }

        public snotextbox Textbox_Number()
        {
            snotextbox textbox = null;
            string define = "Number";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        #endregion End - Textbox

        #region Lookup

        public snolookup Lookup_UserName()
        {
            snolookup lookup = null;
            string define = "User name:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_UserLocation()
        {
            snolookup lookup = null;
            string define = "User Location:";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }



        #endregion End - Lookup

        #region Textarea

        public snotextarea Textarea_AdditionalComments()
        {
            snotextarea textarea = null;
            string define = "Additional comments";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_Comments()
        {
            snotextarea textarea = null;
            string define = "Comments";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_DetailedInformation()
        {
            snotextarea textarea = null;
            string define = "Detailed Information";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

       
        #endregion End - Textarea

        #region Button

        public snobutton Button_ResolveIncident()
        {
            snobutton bt = null;
            string define = "Resolve Incident";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Search()
        {
            snobutton bt = null;
            string define = "Search";
            bt = Base.GButtonByText(define);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Order()
        {
            snobutton bt = null;
            string define = "button[id*='oi_order_item'],button[id*='order_now']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Order", "button", by, null, snobase.MainFrame);
            return bt;
        }

        public snoelement Button_Submit()
        {
            snoelement bf = null;

            string define = "submit_button";
            By by = By.Id(define);
            bf = (snoelement)Base.SNGObject("Submit", "element", by, null, snobase.MainFrame);
            return bf;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Back()
        {
            snobutton bt = null;
            string define = "button[id*='back_to_catalog']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Back", "button", by, null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_Reject()
        {
            snobutton bt = null;
            string define = "Reject";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Approve()
        {
            snobutton bt = null;
            string define = "Approve";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion End - Button

        #region Combobox

        public snocombobox Combobox_Type_1()
        {
            snocombobox combobox = null;
            string define = "What kind of issue are you having?";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }


        public snocombobox Combobox_Type_2()
        {
            snocombobox combobox = null;
            string define = "What is the nature of the issue?";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_PriorityAssessment()
        {
            snocombobox combobox = null;
            string define = "Priority Assessment";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Approval()
        {
            snocombobox combobox = null;
            string define = "Approval";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Stage()
        {
            snocombobox combobox = null;
            string define = "Stage";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }

        public snocombobox Combobox_Quantity()
        {
            snocombobox cb = null;
            string define = "#quantity";
            By by = By.CssSelector(define);
            cb = (snocombobox)Base.SNGObject("Quantity", "combobox", by, null, snobase.MainFrame);
            return cb;
        }

        #endregion End - Combobox

        #region Others

        public snoelement UserFullName()
        {
            snoelement ele = null;
            string define = ".//li[contains(@class,'head')][1]";
            By by = By.XPath(define);
            ele = (snoelement)Base.SNGObject("User full name", "element", by);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Price()
        {
            snoelement ele = null;
            string define = "price_span";
            By by = By.Id(define);
            ele = (snoelement)Base.SNGObject("Price", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Recurring_Price()
        {
            snoelement ele = null;
            string define = "recurring_price_span";
            By by = By.Id(define);
            ele = (snoelement)Base.SNGObject("Recurring price", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Recurring_Frequency()
        {
            snoelement ele = null;
            string define = "recurring_frequency_span";
            By by = By.Id(define);
            ele = (snoelement)Base.SNGObject("Recurring frequency", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Subtotal()
        {
            snoelement ele = null;
            string define = "price_subtotal_span";
            By by = By.Id(define);
            ele = (snoelement)Base.SNGObject("Subtotal", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Recurring_Subtotal()
        {
            snoelement ele = null;
            string define = "recurring_price_subtotal_span";
            By by = By.Id(define);
            ele = (snoelement)Base.SNGObject("Recurring subtotal", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement ThankYouMessage()
        {
            snoelement ele = null;
            string define = ".outputmsg.outputmsg_success, .notification.notification-success>span";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Thank you message", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement TKP_Request_Number()
        {
            snoelement ele = null;
            string define = "a[id='requesturl']>b";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Request number", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement TKP_Request_Item_Number()
        {
            snoelement ele = null;
            string define = ".sc_os_req_item>a";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Request item number", "element", by, null, snobase.MainFrame);
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotable Table_List([Optional] bool expectedNotFound)
        {
            snotable table = null;
            By by = By.CssSelector(tableD);
            if (!expectedNotFound)
                table = new snotable("Portal table", Base.Driver, by, null, null, false, colD, rowD, cellD, columnAttribute, textReplace, true);
            else
                table = new snotable("Portal table", Base.Driver, by, null, null, true, colD, rowD, cellD, columnAttribute, textReplace, true);

            return table;
        }
        #endregion End - Others

        #endregion End - Controls
    }
}
