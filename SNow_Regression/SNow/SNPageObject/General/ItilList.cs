﻿using System;
using OpenQA.Selenium;
using System.Threading;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace SNow
{
    public class ItilList
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;
        private snoelement _parent = null;

        #endregion End - Variables

        #region Properties

        public snobase Base 
        {
            get { return _snobase; }
            set { _snobase = value; }
        }
        
        #endregion End - Properties

        #region Constructor

        public ItilList(snobase obase, string name, [Optional] snoelement parent) 
        {
            Base = obase;
            _name = name;
            _parent = parent;
        }

        #endregion End - Constructor

        #region Private methods

        private bool VerifyExpectedColumnExisted(string item, List<string> columns)
        {
            bool flag = false;
            bool flagM = true;

            if (item.Contains("@@"))
            {
                item = item.Substring(2, item.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ <" + item + ">");
            }
            else
            {
                Console.WriteLine("---finding..M <" + item + ">");
            }

            foreach (string col in columns)
            {
                if (flagM)
                {
                    if (col.Trim().ToLower().Equals(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
                else
                {
                    if (col.Trim().ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found] <" + item + "> in actual column list.");
            else
                Console.WriteLine("-*-[NOT FOUND] <" + item + "> in actual column list.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyActualColumnExisted(string col, string[] arr)
        {
            bool flag = false;
            
            Console.WriteLine("---finding... <" + col + "> in expected column list.");

            foreach (string exp in arr)
            {
                string item = exp;
                if (item.Contains("@@"))
                {
                    item = exp.Substring(2, item.Length - 2);
                    if (col.Trim().ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        Console.WriteLine("-*-[Info]: Runtime <" + col + "> - Expected: <" + exp + ">");
                        break;
                    }
                }
                else
                {
                    if (item.Trim().ToLower().Equals(col.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found] <" + col + "> in expected column list.");
            else
                Console.WriteLine("-*-[NOT FOUND] <" + col + "> in expected column list.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Search(string searchBy, string searchValue, [Optional] bool noMainFrame)
        {
            bool flag = true;
            snocombobox goTo = Combobox_Goto(noMainFrame);
            flag = goTo.Existed;
            if (flag)
            {
                flag = goTo.SelectItem(searchBy);

                if (flag)
                {
                    snotextbox search = Textbox_Search(noMainFrame);
                    
                    flag = search.Existed;
                    if (flag)
                    {
                        string val = string.Empty;
                        if (searchValue.Contains("@@"))
                            val = searchValue.Replace("@@", "");
                        else val = searchValue;

                        flag = search.SetText(val, true);
                        if (flag)
                        {
                            string frame = snobase.MainFrame;
                            if (noMainFrame)
                                frame = null;
                            snoelement ele = (snoelement)Base.SNGObject("breadcrumb", "element", By.CssSelector("span[id$='breadcrumb']"), _parent, frame);

                            int count = 0;
                            val = val.Replace("=", "");
                            val = val.Replace("!", "").Trim();
                            bool flagT = ele.MyText.ToLower().Contains(val.ToLower());
                            while (!flagT && count < 10)
                            {
                                Thread.Sleep(2000);
                                ele = (snoelement)Base.SNGObject("breadcrumb", "element", By.CssSelector("span[id$='breadcrumb']"), _parent, frame);
                                flagT = ele.MyText.ToLower().Contains(val.ToLower());
                                count++;
                            }

                            WaitLoading(false, noMainFrame);
                        }
                        else System.Console.WriteLine("-*-[Info]: Cannot populate search value.");
                    }
                    else System.Console.WriteLine("-*-[Info]: Cannot get textbox search.");
                }
                else System.Console.WriteLine("-*-[Info]: Cannot populate combobox goto.");
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get combobox goto.");
            return flag;
        }        
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool InputCondition(snoelement block, string condition)
        {
            bool flag = true;
            string field = string.Empty;
            string operation = string.Empty;
            string value = string.Empty;
            string[] arr = condition.Split(';');

            field = arr[0];
            operation = arr[1];
            value = arr[2];
            //-- populate field
            snocombobox cField = (snocombobox)Base.SNGObject("Field", "combobox", By.CssSelector("#field>select"), block, snobase.MainFrame);
            if (cField.Existed)
            {
                flag = cField.SelectItem(field);
                Thread.Sleep(2000);
                snocombobox cOperation = (snocombobox)Base.SNGObject("Operation", "combobox", By.CssSelector("#oper>select"), block, snobase.MainFrame);
                if (cOperation.Existed)
                {
                    flag = cOperation.SelectItem(operation);
                    Thread.Sleep(2000);
                    if (flag)
                    {
                        snoelement eValue = (snoelement)Base.SNGObject("Value", "element", By.CssSelector("#value"), block, snobase.MainFrame);
                        //snoelements list = Base.SNGObjects("Value list", By.CssSelector("[title='Choose Input']:not([type='hidden']),[title='Choose option']:not([type='hidden']), #value [title='input value']:not([type='hidden']), #value [title='Input value']:not([type='hidden'])"), eValue, snobase.MainFrame);
                        snoelements list = Base.SNGObjects("Value list", By.CssSelector("input:not([type='hidden']), select"), eValue, snobase.MainFrame);
                        if (list.MyList.Count > 0 && value != string.Empty)
                        {
                            snoelement item = new snoelement("item", Base.Driver, list.MyList[0].MyElement);
                            string tagname = item.MyElement.TagName;
                            switch (tagname.ToLower())
                            {
                                case "input":
                                    string classname = item.MyElement.GetAttribute("class");
                                    if (classname.Contains("filter-reference-input"))
                                    {
                                        snolookup lk = new snolookup("lookup", Base.Driver, item.MyElement);
                                        flag = lk.Select(value, false, true);
                                    }
                                    else
                                    {
                                        snotextbox tb = new snotextbox("textbox", Base.Driver, item.MyElement);
                                        flag = tb.SetText(value, false, true);
                                    }
                                    Thread.Sleep(2000);
                                    break;
                                case "select":
                                    snocombobox cb = new snocombobox("combo", Base.Driver, item.MyElement);
                                    flag = cb.SelectItem(value);
                                    Thread.Sleep(2000);
                                    break;
                            }
                        }
                    }
                }
                else { flag = false; }
            }
            else
            { flag = false; }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool AddMoreColumn(string value)
        {
            bool flag = true;

            snocombobox right = null;

            right = List_Right();
            flag = right.VerifyExpectedItemsExisted(value);

            if (!flag)
            {
                snocombobox left = List_Left();
                flag = left.SelectItem(value);
                if (flag)
                {
                    snobutton bt = Button_Add();
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click();
                        if (flag)
                        {
                            flag = right.VerifyExpectedItemsExisted(value);
                        }
                    }
                    else
                        System.Console.WriteLine("*****Element [" + bt.MyText +"] is NOT found.");
                }
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Verify_ContextMenu_Existed()
        {
            bool flag = true;
            string define = "context_menu";
            By by = By.ClassName(define);
            snoelements menus = Base.SNGObjects("Menu list", by, null, snobase.MainFrame);
            if (menus.MyList.Count <= 0) { flag = false; }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Select_ContextMenu_Item(string item, bool javaClick)
        {
            bool flag = true;
            string menudefine = "context_menu";
            string itemdefine = "div[class*='context_item']";
            By menuby = By.ClassName(menudefine);
            By itemby = By.CssSelector(itemdefine);
            snoelements menus = Base.SNGObjects("Menu list", menuby, null, snobase.MainFrame);

            if (menus.MyList.Count > 0)
            {
                snoelement parent = new snoelement("parent", Base.Driver, menus.MyList[menus.MyList.Count - 1].MyElement);
                snoelements items = Base.SNGObjects("Item list", itemby, parent, snobase.MainFrame);
                if (items.MyList.Count > 0)
                {
                    bool flagF = false;
                    foreach (Auto.oelement ele in items.MyList)
                    {
                        if (ele.MyText.Trim().ToLower().Equals(item.Trim().ToLower()))
                        {
                            //-- Move to element
                            Actions ac = new Actions(Base.Driver);
                            ac.MoveToElement(ele.MyElement);
                            ac.Perform();
                            //-- End move to element
                            flagF = ele.Click(javaClick);
                            if (flagF)
                                break;
                        }
                    }
                    if (!flagF)
                    {
                        flag = false;
                        System.Console.WriteLine("***[INFO]: Not found item (" + item + ")");
                    }
                }
                else
                {
                    flag = false;
                    System.Console.WriteLine("***[INFO]: Not found any item.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("***[INFO]: Not found any context menu.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Select_ContextMenu_Items(string items, [Optional] bool javaClick)
        {
            string[] itemArray;
            bool flag = true;

            System.Console.WriteLine("***[Call function]: ContextMenuSelectItems");
            System.Console.WriteLine("-*- Select items:(" + items + ")");

            if (items.Contains(";"))
                itemArray = items.Split(';');
            else
                itemArray = new string[] { items };

            foreach (string item in itemArray)
            {
                flag = Select_ContextMenu_Item(item, javaClick);
                Thread.Sleep(1000);
                if (flag == false)
                {
                    break;
                }
            }

            return flag;
        }

        #endregion End - Private methods

        #region Public methods

        public void WaitLoading([Optional] bool noCheckGui, [Optional] bool noMainFrame)
        {
            Console.WriteLine(_name + " page wait loading...");
            Base.PageWaitLoading(noCheckGui, noMainFrame);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool ResetColumnToDefaultValue()
        {
            bool flag = true;
            snobutton button = Button_Personalize();
            flag = button.Click(true);
            if (flag)
            {
                button = Button_OK();
                flag = button.Existed;
                WaitLoading();
                int count = 0;
                while (!flag && count < 5)
                {
                    Thread.Sleep(2000);
                    button = Button_Personalize();
                    button.Click(true);
                    button = Button_OK();
                    flag = button.Existed;
                }
                WaitLoading();
                button = Button_ResetColumnDefault();
                if (button.Existed)
                {
                    flag = button.Click(true);
                }
                else
                {
                    button = Button_X();
                    flag = button.Existed;
                    if (flag)
                        flag = button.Click();
                    
                }
                WaitLoading();
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyColumnHeader(string expected, ref string error, [Optional] bool noMainFrame, [Optional] string tableDefine, [Optional] string colDefine, [Optional] string rowDefine, [Optional] string cellDefine) 
        {
            bool flag = true;
            string[] arr = null;
            if (expected.Contains(";"))
                arr = expected.Split(';');
            else
                arr = new string[] { expected };

            snotable table = Table_List(tableDefine, noMainFrame);
            flag = table.Existed;
            if (flag) 
            {

                if (colDefine != null && colDefine != string.Empty)
                    table.ColumnDefine = colDefine;
                if (rowDefine != null && rowDefine != string.Empty)
                    table.RowDefine = rowDefine;
                if (cellDefine != null && cellDefine != string.Empty)
                    table.CellDefine = cellDefine;

                List<string> columns = table.ColumnHeader;
                if (columns.Count > 0)
                {
                    Console.WriteLine("*** Expected column(s) need to verify: [" + arr.Length + "]");
                    Console.WriteLine("*** Runtime column(s) have: [" + columns.Count + "]");
                    Console.WriteLine("*** Verify expected column existed -*-");
                    Console.WriteLine("................................................");
                    foreach (string exp in arr)
                    {
                        bool flagF = VerifyExpectedColumnExisted(exp, columns);
                        if (!flagF) 
                        {
                            if(flag)
                                flag = false;
                            error = error + "***Not found:[" + exp + "] in actual column list." + "\n";
                        }
                    }
                    Console.WriteLine("--------------------------------------------------");
                    Console.WriteLine("*** Verify actual column have in expected list -*-");
                    Console.WriteLine("................................................");
                    foreach (string act in columns)
                    {
                        bool flagF = VerifyActualColumnExisted(act, arr);
                        if (!flagF && error == string.Empty && flag)
                            error = "WARNING";
                    }
                }
                else
                {
                    flag = false;
                    Console.WriteLine("***[FAILED]: Not found any columns.");
                }
            }
            else Console.WriteLine("***[FAILED]:Table NOT existed.");
            
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyComboboxGoto(string itemList, ref string error)
        {
            bool flag = true;

            snocombobox combobox = Combobox_Goto();

            flag = (itemList != null || itemList != string.Empty || itemList.Trim() != "");
            if (flag)
            {
                flag = combobox.Existed;
                if (flag)
                {
                    bool flagTemp = false;
                    // Verify if the input items are in combobox
                    System.Console.WriteLine("*** Verify expected items existed ***");
                    Console.WriteLine("................................................");
                    flagTemp = combobox.VerifyExpectedItemsExisted(itemList);
                    if (!flagTemp)
                    {
                        flag = false;
                        System.Console.WriteLine("***[ERROR]: Some input items are NOT in combobox [Go to]");
                    }
                    System.Console.WriteLine("----------------------------------------------------");
                    Console.WriteLine("*** Verify actual items have in expected list -*-");
                    Console.WriteLine("................................................");
                    flagTemp = combobox.VerifyActualItemsExisted(itemList);
                    if (!flagTemp && error == string.Empty && flag)
                        error = "WARNING";
                }
                else
                {
                    System.Console.WriteLine("***[ERROR]: Cannot find combobox [Go to]");
                }
            }
            else
            {
                System.Console.WriteLine("***[ERROR]: The parameter [itemList] is incorrect");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndOpen(string searchBy, string searchValue, string findConditions, string columnClick,[Optional] string tableDefine, [Optional] bool noMainFrame)
        {
            bool flag = true;
            flag = Search(searchBy, searchValue, noMainFrame);
            if (flag)
            {
                snotable table = Table_List(tableDefine, noMainFrame);
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;
                    Auto.oelements rows = table.RowsList;
                    if (columns.Count > 0 && rows.MyList.Count > 0) 
                    {
                        int rowIndex = table.RowIndex(findConditions, columns, rows);
                        if (rowIndex >= 0)
                        {
                            int columnIndex = table.ColumnIndex(columnClick, columns);
                            if (columnIndex >= 0)
                            {
                                try
                                {
                                    flag = table.CellClick(rowIndex, columnIndex, "a", columns, rows);
                                }
                                catch { flag = false; }
                                if (!flag) 
                                {
                                    flag = table.CellClick(rowIndex, columnIndex, "a", columns, rows, true);
                                    if(!flag)
                                    Console.WriteLine("***[Info]:Error when click on cell.");
                                }
                                    
                            }
                            else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                        }
                        else
                        {
                            flag = false;
                            Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
                }
                else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            }
            else System.Console.WriteLine("-*-[Info]: Error when search row.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyRow(string findConditions, [Optional] bool expectedNotFound)
        {
            bool flag = true;
            snotable table = Table_List();
            flag = table.Existed;
            if (flag)
            {
                List<string> columns = table.ColumnHeader;
                Auto.oelements rows = table.RowsList;

                if (columns.Count > 0)
                {
                    if (rows.MyList.Count > 0)
                    {
                        int rowIndex = table.RowIndex(findConditions, columns, rows);
                        if (rowIndex >= 0)
                        {
                            if (!expectedNotFound)
                                Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ")");
                            else 
                            {
                                flag = false;
                                Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ") - Expected: NOT FOUND.");
                            }
                        }
                        else
                        {
                            if (!expectedNotFound) 
                            {
                                flag = false;
                                Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ")");
                            }
                            else
                                Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ") as Expected.");
                        }
                    }
                    else
                    {
                        if (!expectedNotFound)
                        {
                            flag = false;
                            System.Console.WriteLine("-*-[Info]: Not have any row(s).");
                        }
                        else
                        {
                            System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                        }
                    }
                }
                else 
                {
                    flag = false;
                    System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                }
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get table list.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int GetRowCount()
        {
            int result = -1;
            snotable table = Table_List();
            bool flag = table.Existed;
            if (flag)
            {
                Auto.oelements rows = table.RowsList;
                result = rows.MyList.Count;
            }
            return result;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool RelatedTableOpen(string findConditions, string columnClick)
        {
            bool flag = true;
            snotable table = Table_List();
            flag = table.Existed;
            if (flag)
            {
                List<string> columns = table.ColumnHeader;
                Auto.oelements rows = table.RowsList;
                if (columns.Count > 0 && rows.MyList.Count > 0)
                {
                    int rowIndex = table.RowIndex(findConditions, columns, rows);
                    if (rowIndex >= 0)
                    {
                        int columnIndex = table.ColumnIndex(columnClick, columns);
                        if (columnIndex >= 0)
                        {
                            flag = table.CellClick(rowIndex, columnIndex, "a", columns, rows);
                            if (!flag)
                                Console.WriteLine("***[Info]: Error when click on cell.");
                        }
                        else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                    }
                    else
                    {
                        flag = false;
                        Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                    }
                }
                else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            return flag;

        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string RelatedTableGetCell(string findConditions, string columnName, [Optional] bool noMainFrame, [Optional] string tableDefine, [Optional] string colDefine, [Optional] string rowDefine, [Optional] string cellDefine, [Optional] string childCell)
        {
            
            string result = string.Empty;
            bool flag = true;
            snotable table = Table_List(tableDefine, noMainFrame);
            flag = table.Existed;
            if (flag)
            {
                if (colDefine != null && colDefine != string.Empty)
                    table.ColumnDefine = colDefine;
                if (rowDefine != null && rowDefine != string.Empty)
                    table.RowDefine = rowDefine;
                if (cellDefine != null && cellDefine != string.Empty)
                    table.CellDefine = cellDefine;

                List<string> columns = table.ColumnHeader;
                Auto.oelements rows = table.RowsList;
                if (columns.Count > 0 && rows.MyList.Count > 0)
                {
                    int rowIndex = table.RowIndex(findConditions, columns, rows);
                    if (rowIndex >= 0)
                    {
                        int columnIndex = table.ColumnIndex(columnName, columns);
                        if (columnIndex >= 0)
                        {
                            result = table.CellText(rowIndex, columnIndex, childCell);
                        }
                        else Console.WriteLine("***[Info]: NOT FOUND column (" + columnName + ")");
                    }
                    else
                    {
                        flag = false;
                        Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                    }
                }
                else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            return result;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndEdit(string searchBy, string searchValue, string findConditions, string columnClick, string value)
        {
            bool flag = true;
            flag = Search(searchBy, searchValue);
            if (flag)
            {
                Thread.Sleep(5000);
                snotable table = Table_List();
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;
                    Auto.oelements rows = table.RowsList;
                    if (columns.Count > 0 && rows.MyList.Count > 0)
                    {
                        int rowIndex = table.RowIndex(findConditions, columns, rows);
                        if (rowIndex >= 0)
                        {
                            int columnIndex = table.ColumnIndex(columnClick, columns);
                            if (columnIndex >= 0)
                            {
                                flag = table.CellClick(rowIndex, columnIndex, null, columns, rows, false, true);
                                if (!flag)
                                    Console.WriteLine("***[Info]: Error when click on cell.");
                                else 
                                {
                                    WaitLoading();
                                    string define = "table[id='window.cell_edit_window'] [class^='form-control']";
                                    By by = By.CssSelector(define);
                                    snoelement cellEdit = (snoelement)Base.SNGObject("Cell edit", "element", by, null, snobase.MainFrame);
                                    flag = cellEdit.Existed;
                                    if (flag)
                                    {
                                        switch (cellEdit.MyElement.TagName.ToLower())
                                        {
                                            case "select":
                                                snocombobox cb = new snocombobox(columnClick, Base.Driver, cellEdit.MyElement);
                                                flag = cb.Existed;
                                                if (flag)
                                                {
                                                    flag = cb.SelectItem(value);
                                                    if (flag)
                                                    {
                                                        define = "table[id='window.cell_edit_window'] a[id='cell_edit_ok']";
                                                        by = By.CssSelector(define);
                                                        snoelement ok = (snoelement)Base.SNGObject("OK", "element", by, null, snobase.MainFrame);
                                                        flag = ok.Existed;
                                                        if (flag)
                                                        {
                                                            flag = ok.Click();
                                                            if(!flag)
                                                                Console.WriteLine("-*-[Info]: Error when click on ok button.");
                                                        }
                                                        else Console.WriteLine("-*-[Info]: Cannot get button ok.");
                                                    }
                                                    else Console.WriteLine("-*-[Info]: Cannot change cell value.");
                                                }
                                                break;
                                             case "input":
                                                snolookup lookup = new snolookup(columnClick, Base.Driver, cellEdit.MyElement);
                                                flag = lookup.Existed;
                                                if (flag)
                                                {
                                                    flag = lookup.Select(value, false, true);
                                                    if (flag)
                                                    {
                                                        define = "table[id='window.cell_edit_window'] a[id='cell_edit_ok']";
                                                        by = By.CssSelector(define);
                                                        snoelement ok = (snoelement)Base.SNGObject("OK", "element", by, null, snobase.MainFrame);
                                                        flag = ok.Existed;
                                                        if (flag)
                                                        {
                                                            flag = ok.Click();
                                                            if (!flag)
                                                                Console.WriteLine("-*-[Info]: Error when click on ok button.");
                                                        }
                                                        else Console.WriteLine("-*-[Info]: Cannot get button ok.");
                                                    }
                                                    else Console.WriteLine("-*-[Info]: Cannot change cell value.");
                                                }
                                                break;
                                        }
                                    }
                                    else Console.WriteLine("-*-[Info]: The edit control is not existed.");
                                }
                            }
                            else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                        }
                        else
                        {
                            flag = false;
                            Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
                }
                else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            }
            else System.Console.WriteLine("-*-[Info]: Error when search row.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchByColumnAndChoose(string coloumnFind, string findConditions)
        {
            bool flag = true;
            int index = 0;
            snotable table = Table_List("div[class='panel-body editor-body'] table[role='grid']");
            table.ColumnDefine = "thead a>span";
            table.RowDefine = "tbody tr[role='row']";
            table.CellDefine = "td[class^='vt']";
            table.ColumnAttribute = null;
            flag = table.Existed;
            if (flag)
            {
                snobutton button = Button_SearchByColumn();
                flag = button.Click(true);
                if (flag)
                {
                    index = table.ColumnIndex(coloumnFind);
                    snoelement parent = new snoelement("parent", Base.Driver, table.MyElement);
                    snoelements listTextbox = Base.SNGObjects("eles", By.CssSelector("thead tr[class='list_header'] th[class^='test'] input"), parent, snobase.MainFrame);
                    Auto.oelement ele = listTextbox.MyList[index];
                    snotextbox tx = new snotextbox("textbox", Base.Driver, ele.MyElement);
                    flag = tx.Existed;
                    if (flag)
                    {
                        tx.SetText(findConditions, true);
                        WaitLoading();
                        flag = table.FindRowAndClickCheckbox(coloumnFind + "=" + findConditions);
                        if (!flag)
                            Console.WriteLine("Error when click on checkbox of record.");
                        else
                        {
                            snobutton bt = new snobutton("add", Base.Driver, By.CssSelector("button[class='btn btn-icon icon icon-add']"), null, snobase.MainFrame);
                            flag = bt.Existed;
                            if (flag)
                                bt.Click();
                            else Console.WriteLine("Error: Cannot add relationship CI.");
                        }
                    }
                    else Console.WriteLine("Error: Cannot get table list.");
                }
                else Console.WriteLine("Error: Cannot click on Search button.");
            }  

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool CellEdit(string findConditions, string columnClick, string value, [Optional] string rIndex)
        {
            bool flag = true;
            snotable table = null;
            if (rIndex == null)
                table = Table_List();
            else
                table = Table_List("table[id$='_table'][class]");
            flag = table.Existed;
            if (flag)
            {
                List<string> columns = table.ColumnHeader;
                if (rIndex != null) 
                {
                    table.RowDefine = "tbody>tr[id]";
                    table.CellDefine = "td[class^='vt']";
                }
                    
                Auto.oelements rows = table.RowsList;
                if (columns.Count > 0 && rows.MyList.Count > 0)
                {
                    int rowIndex = -1;
                    if (rIndex == null)
                        rowIndex = table.RowIndex(findConditions, columns, rows);
                    else
                        rowIndex = int.Parse(rIndex);
                    if (rowIndex >= 0)
                    {
                        int columnIndex = table.ColumnIndex(columnClick, columns);
                        if (columnIndex >= 0)
                        {
                            flag = table.CellClick(rowIndex, columnIndex, null, columns, rows, false, true);
                            if (!flag)
                                Console.WriteLine("***[Info]: Error when click on cell.");
                            else
                            {
                                WaitLoading();
                                string define = "table[id='window.cell_edit_window'] [class^='form-control']";
                                By by = By.CssSelector(define);
                                snoelement cellEdit = (snoelement)Base.SNGObject("Cell edit", "element", by, null, snobase.MainFrame);
                                flag = cellEdit.Existed;
                                if (flag)
                                {
                                    switch (cellEdit.MyElement.TagName.ToLower())
                                    {
                                        case "select":
                                            snocombobox cb = new snocombobox(columnClick, Base.Driver, cellEdit.MyElement);
                                            flag = cb.Existed;
                                            if (flag)
                                            {
                                                flag = cb.SelectItem(value);
                                                if (flag)
                                                {
                                                    define = "table[id='window.cell_edit_window'] a[id='cell_edit_ok']";
                                                    by = By.CssSelector(define);
                                                    snoelement ok = (snoelement)Base.SNGObject("OK", "element", by, null, snobase.MainFrame);
                                                    flag = ok.Existed;
                                                    if (flag)
                                                    {
                                                        flag = ok.Click();
                                                        if (!flag)
                                                            Console.WriteLine("-*-[Info]: Error when click on ok button.");
                                                    }
                                                    else Console.WriteLine("-*-[Info]: Cannot get button ok.");
                                                }
                                                else Console.WriteLine("-*-[Info]: Cannot change cell value.");
                                            }
                                            break;
                                        case "input":
                                            string id = cellEdit.MyElement.GetAttribute("id");
                                            if (id.ToLower() != "cell_edit_value")
                                            {
                                                snolookup lookup = new snolookup(columnClick, Base.Driver, cellEdit.MyElement);
                                                flag = lookup.Existed;
                                                if (flag)
                                                {
                                                    flag = lookup.Select(value, false, true);
                                                    if (flag)
                                                    {
                                                        define = "table[id='window.cell_edit_window'] a[id='cell_edit_ok']";
                                                        by = By.CssSelector(define);
                                                        snoelement ok = (snoelement)Base.SNGObject("OK", "element", by, null, snobase.MainFrame);
                                                        flag = ok.Existed;
                                                        if (flag)
                                                        {
                                                            flag = ok.Click();
                                                            if (!flag)
                                                                Console.WriteLine("-*-[Info]: Error when click on ok button.");
                                                        }
                                                        else Console.WriteLine("-*-[Info]: Cannot get button ok.");
                                                    }
                                                    else Console.WriteLine("-*-[Info]: Cannot change cell value.");
                                                }
                                            }
                                            else 
                                            {
                                                snotextbox textbox = new snotextbox(columnClick, Base.Driver, cellEdit.MyElement);
                                                flag = textbox.Existed;
                                                if (flag)
                                                {
                                                    flag = textbox.SetText(value, false, true);
                                                    if (flag)
                                                    {
                                                        define = "table[id='window.cell_edit_window'] a[id='cell_edit_ok']";
                                                        by = By.CssSelector(define);
                                                        snoelement ok = (snoelement)Base.SNGObject("OK", "element", by, null, snobase.MainFrame);
                                                        flag = ok.Existed;
                                                        if (flag)
                                                        {
                                                            flag = ok.Click();
                                                            if (!flag)
                                                                Console.WriteLine("-*-[Info]: Error when click on ok button.");
                                                        }
                                                        else Console.WriteLine("-*-[Info]: Cannot get button ok.");
                                                    }
                                                    else Console.WriteLine("-*-[Info]: Cannot change cell value.");
                                                }
                                            }
                                            break;
                                    }
                                }
                                else Console.WriteLine("-*-[Info]: The edit control is not existed.");
                            }
                        }
                        else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                    }
                    else
                    {
                        flag = false;
                        Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                    }
                }
                else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get table list.");

            return flag;
        }
        //-------------------------------------------------------------------------------------------------
        public bool SearchAndSelectContextMenu(string searchBy, string searchValue, string searchConditions, string columnClick, string selectValue)
        {
            bool flag = true;
            flag = Search(searchBy, searchValue);
            if (flag)
            {
                Thread.Sleep(5000);
                snotable table = Table_List();
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;
                    Auto.oelements rows = table.RowsList;
                    if (columns.Count > 0 && rows.MyList.Count > 0)
                    {
                        int rowIndex = table.RowIndex(searchConditions, columns, rows);
                        if (rowIndex >= 0)
                        {
                            int columnIndex = table.ColumnIndex(columnClick, columns);
                            if (columnIndex >= 0)
                            {
                                flag = table.CellClick(rowIndex, columnIndex, null, columns, rows, false, false, true);
                                if (!flag)
                                    Console.WriteLine("***[Info]: Error when right click on cell.");
                                else
                                {
                                    int count = 0;
                                    while (!Verify_ContextMenu_Existed() && count < 5)
                                    {
                                        Thread.Sleep(1000);
                                        table.CellClick(rowIndex, columnIndex, null, columns, rows, false, false, true);
                                        count++;
                                    }
                                    flag = Verify_ContextMenu_Existed();
                                    if (flag)
                                    {
                                        flag = Select_ContextMenu_Items(selectValue, true);
                                        if (!flag) System.Console.WriteLine("-*-[ERROR]: Error when select context menu item.");
                                    }
                                    else System.Console.WriteLine("-*-[ERROR]: Context menu not show.");
                                }
                            }
                        }
                    }
                }
                else System.Console.WriteLine("-*-[ERROR]: Cannot get table list.");   
            }
            else System.Console.WriteLine("-*-[ERROR]: Error when search row.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Open(string findConditions, string columnClick, [Optional] bool noMainFrame, [Optional] string tableDefine, [Optional] string colDefine, [Optional] string rowDefine, [Optional] string cellDefine)
        {
            bool flag = true;
            snotable table = Table_List(tableDefine, noMainFrame);
            flag = table.Existed;
            if (flag)
            {
                if (colDefine != null && colDefine != string.Empty)
                    table.ColumnDefine = colDefine;
                if (rowDefine != null && rowDefine != string.Empty)
                    table.RowDefine = rowDefine;
                if (cellDefine != null && cellDefine != string.Empty)
                    table.CellDefine = cellDefine;
                
                List<string> columns = table.ColumnHeader;
                Auto.oelements rows = table.RowsList;
                if (columns.Count > 0 && rows.MyList.Count > 0)
                {
                    int rowIndex = table.RowIndex(findConditions, columns, rows);

                    if (rowIndex >= 0)
                    {
                        int columnIndex = table.ColumnIndex(columnClick, columns);
                        if (columnIndex >= 0)
                        {
                            flag = table.CellClick(rowIndex, columnIndex, "a", columns, rows);
                            if (!flag)
                                Console.WriteLine("***[Info]: Error when click on cell.");
                        }
                        else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                    }
                    else
                    {
                        flag = false;
                        Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                    }
                }
                else { System.Console.WriteLine("-*-[Info]: Not have any columns or rows."); flag = false; }
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndVerify(string searchBy, string searchValue, string findConditions, [Optional] bool expectedNotFound)
        {
            bool flag = true;
            flag = Search(searchBy, searchValue);
            if (flag)
            {
                Thread.Sleep(5000);
                flag = VerifyRow(findConditions, expectedNotFound);
            }
            else System.Console.WriteLine("-*-[Info]: Error when search row.");

            return flag;
        }     
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Filter(string condition)
        {
            bool flag = true;
            bool flagE = false;

            string[] arrCon = null;

            if (condition.Contains("|"))
            {
                arrCon = condition.Split('|');
            }
            else
            {
                arrCon = new string[] { condition };
            }
            Thread.Sleep(2000);
            snoelement container = Toolbar_Filter(true);
            flag = container.Existed;
            int count = 0;

            while (!flag && count < 1)
            {
                container = Toolbar_Filter(true);
                flag = container.Existed;
                count++;
                Thread.Sleep(1000);
            }

            if (flag) flagE = true;

            if (!flag)
            {
                snobutton bt = Button_Filter();
                flag = bt.Existed;
                if (flag)
                {
                    bt.Click(true);
                    WaitLoading();
                    container = Toolbar_Filter(true);
                    flag = container.Existed;
                    //-- waiting
                    count = 0;
                    while (!flag && count < 10)
                    {
                        container = Toolbar_Filter(true);
                        flag = container.Existed;
                        count++;
                        Thread.Sleep(2000);
                    }
                }
                else Console.WriteLine("-*-[Info]: Cannot get button filter.");
            }

            if (flag)
            {
                snoelements deleteList = Base.SNGObjects("Deletes", By.CssSelector("button[title^='Delete']"), null, snobase.MainFrame);
                while (deleteList.MyList.Count > 0)
                {
                    Auto.oelement bD = deleteList.MyList[0];
                    if (bD.Existed)
                        bD.Click(true);
                    Thread.Sleep(2000);
                    deleteList = Base.SNGObjects("Deteles", By.CssSelector("button[title^='Delete']"), null, snobase.MainFrame);
                }
                snoelements blocks = Base.SNGObjects("Blocks", By.CssSelector(".filter_row_condition"), null, snobase.MainFrame);
                int index = 0;
                snoelement pr = null;
                foreach (string con in arrCon)
                {
                    switch (con.ToLower())
                    {
                        case "and":
                            pr = new snoelement("parent", Base.Driver, blocks.MyList[0].MyElement);
                            snobutton bAND = (snobutton)Base.SNGObject("AND", "button", By.CssSelector("td button[alt^='Add AND']"), pr, snobase.MainFrame);
                            if (bAND.Existed)
                            {
                                bAND.Click(true);
                                Thread.Sleep(2000);
                                index++;
                                blocks = Base.SNGObjects("Blocks", By.CssSelector(".filter_row_condition"), null, snobase.MainFrame);
                            }
                            else { flag = false; }
                            break;
                        case "or":
                            snoelements bORL = Base.SNGObjects("OR list", By.CssSelector("td button[alt^='Add OR']"), null, snobase.MainFrame);
                            snobutton bOR = new snobutton("OR", Base.Driver, bORL.MyList[bORL.MyList.Count - 1].MyElement);
                            if (bOR.Existed)
                            {
                                bOR.Click(true);
                                Thread.Sleep(2000);
                                index++;
                                blocks = Base.SNGObjects("Blocks", By.CssSelector(".filter_row_condition"), null, snobase.MainFrame);
                            }
                            else { flag = false; }
                            break;
                        case "aor":
                            snobutton bAOR = (snobutton)Base.SNGObject("AOR", "button", By.CssSelector("div[class='filterToolbar'] button[alt^='Add OR']"), null, snobase.MainFrame);
                            if (bAOR.Existed)
                            {
                                bAOR.Click(true);
                                Thread.Sleep(2000);
                                index++;
                                blocks = Base.SNGObjects("Blocks", By.CssSelector(".filter_row_condition"), null, snobase.MainFrame);
                            }
                            else { flag = false; }
                            break;
                        default:
                            pr = new snoelement("parent", Base.Driver, blocks.MyList[index].MyElement);
                            flag = InputCondition(pr, con);
                            break;
                    }
                    if (!flag) break;
                }

                if (flag)
                {
                    snoelement parent = (snoelement)Base.SNGObject("parent", "element", By.CssSelector(".filterToolbar"), null, snobase.MainFrame);
                    snobutton bRun = (snobutton)Base.SNGObject("Run", "button", By.CssSelector("button[title='Run filter']"), parent, snobase.MainFrame);
                    if (bRun.Existed)
                    {
                        bRun.Click(true);
                        Thread.Sleep(2000);
                        WaitLoading();

                        if (!flagE)
                        {
                            container = Toolbar_Filter(true);
                            bool flag1 = container.Existed;
                            count = 0;
                            while (flag1 && count < 20)
                            {
                                Thread.Sleep(1000);
                                container = Toolbar_Filter(true);
                                flag1 = container.Existed;
                                count++;
                            }
                        }
                    }
                    else { flag = false; }
                }
            }
            else
            {
                System.Console.WriteLine("***[ERROR]: Cannot get container.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_More_Columns(string columns)
        {
            bool flag = true;
            string[] arr = null;
            if (columns.Contains("|"))
            {
                arr = columns.Split('|');
            }
            else
            {
                arr = new string[] { columns };
            }

            snobutton button = Button_Personalize();
            flag = button.Existed;
            if (flag) 
            {
                flag = button.Click(true);
                if (flag) 
                {
                    Thread.Sleep(5000);
                    foreach (string v in arr)
                    {
                        flag = AddMoreColumn(v);
                        if (!flag)
                        {
                            break;
                        }
                        else
                        {
                            Thread.Sleep(2000);
                        }
                    }

                    button = Button_OK();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        WaitLoading();
                    }
                        
                }
            }
            
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Action_Items_Visible(string items, [Optional] bool noNeedClick)
        {
            bool flag = true;

            snocombobox combo = Combobox_Actions();
            flag = combo.Existed;
            if (flag)
            {
                if (!noNeedClick)
                    flag = combo.Click();

                if (flag)
                {
                    string css = "select[id$='labelAction'] option:not([id='ignore']):not([style*='display: none;'])";
                    snoelement parent = new snoelement("parent", Base.Driver, combo.MyElement);
                    snoelements list = Base.SNGObjects("list", By.CssSelector(css), parent, snobase.MainFrame);
                    flag = list.HaveItemsInlist(items, "|", true);
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Action_Items_Enable(string items, [Optional] bool noNeedClick)
        {
            bool flag = true;

            snocombobox combo = Combobox_Actions();
            flag = combo.Existed;
            if (flag)
            {
                if (!noNeedClick)
                    flag = combo.Click();

                if (flag)
                {
                    string css = "select[id$='labelAction'] option:not([id='ignore']):not([style*='display: none;']):not([style*='color: rgb'])";
                    snoelement parent = new snoelement("parent", Base.Driver, combo.MyElement);
                    snoelements list = Base.SNGObjects("list", By.CssSelector(css), parent, snobase.MainFrame);
                    flag = list.HaveItemsInlist(items, "|", true);
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Sort_Items_By_Column(string column) 
        {
            bool flag = true;

            string define = "(.//a[text()='" + column + "'])[2]";
            snoelement ele = (snoelement)Base.SNGObject("Create", "element", By.XPath(define), null, snobase.MainFrame);
            flag = ele.Existed;
            if (flag)
            {
                snoelement parent = (snoelement)Base.SNGObject("parent", "element", By.XPath(".."), ele, snobase.MainFrame);
                while (parent.MyElement.TagName != "th")
                {
                    parent = (snoelement)Base.SNGObject("parent", "element", By.XPath(".."), parent, snobase.MainFrame);
                }
                if (parent.MyElement.GetAttribute("sort_dir") != "DESC")
                {
                    ele.Click(true);
                    Thread.Sleep(2000);
                    WaitLoading();
                }
            }
            else Console.WriteLine("-*-[Info]: Cannot get created control.");

            return flag;
        }
        
        #endregion End - Public methods

        #region Control methods

        #region Table

        public snotable Table_List([Optional] string tableDefine, [Optional] bool noMainFrame) 
        {
            snotable table_list = null;
            string define = "table[id$='_table'][class], table[class='wide']";
            if (tableDefine != null && tableDefine != string.Empty)
                define = tableDefine;
            By by = By.CssSelector(define);
            if(!noMainFrame)
                table_list = (snotable)Base.SNGObject(_name, "table", by, _parent, snobase.MainFrame);
            else
                table_list = (snotable)Base.SNGObject(_name, "table", by, _parent);
            return table_list;
        }

        #endregion End - Table

        #region Textbox

        private snotextbox Textbox_Search([Optional] bool noMainFrame)
        {
            snotextbox tb = null;
            string define = "input[id$='_text']";
            By by = By.CssSelector(define);
            if(!noMainFrame)
                tb = (snotextbox)Base.SNGObject("Search", "textbox", by, _parent, snobase.MainFrame);
            else
                tb = (snotextbox)Base.SNGObject("Search", "textbox", by, _parent);
            return tb;
        }

        #endregion End - Textbox

        #region Combobox

        public snocombobox Combobox_Goto([Optional] bool noMainFrame)
        {
            snocombobox cb = null;
            string define = "select[id$='_select']";
            By by = By.CssSelector(define);
            if (!noMainFrame)
                cb = (snocombobox)Base.SNGObject(_name, "combobox", by, _parent, snobase.MainFrame);
            else
                cb = (snocombobox)Base.SNGObject(_name, "combobox", by, _parent);
            return cb;
        }      
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snocombobox List_Left()
        {
            snocombobox list = null;
            string define = "select[id$='_left']";
            By by = By.CssSelector(define);
            list = (snocombobox)Base.SNGObject("Left list", "combobox", by, null, snobase.MainFrame);
            return list;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snocombobox List_Right()
        {
            snocombobox list = null;
            string define = "select[id$='_right']";
            By by = By.CssSelector(define);
            list = (snocombobox)Base.SNGObject("Right list", "combobox", by, null, snobase.MainFrame);
            return list;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snocombobox Combobox_Actions()
        {
            snocombobox cb = null;
            string define = "select[id$='labelAction']";
            By by = By.CssSelector(define);
            cb = (snocombobox)Base.SNGObject("Actions", "combobox", by, null, snobase.MainFrame);
            return cb;
        }
        #endregion Combobox

        #region Label

        public snoelement List_Title()
        {
            snoelement lb = null;
            string define = ".navbar-title.list_title";
            lb = (snoelement)Base.SNGObject("List tilte", "element", By.CssSelector(define), _parent, snobase.MainFrame);
            return lb;
        }

        #endregion End - Label

        #region Button

        public snobutton Button_Personalize()
        {
            snobutton button = null;
            string define = "table[id$='table'] i[class^='icon-cog']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Button personalize", "button", by, _parent, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_SearchByColumn()
        {
            snobutton button = null;
            string define = "div[class='panel-body editor-body'] table[role='grid'] button[class^='icon-search']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Button search by column", "button", by, _parent, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_ResetColumnDefault()
        {
            snobutton button = null;
            string define = "reset_column_defaults";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Button reset column default", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_OK()
        {
            snobutton button = null;
            string define = "ok_button";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Button OK", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_X()
        {
            snobutton button = null;
            string define = "list_mechanic_closemodal";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Button X", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Cancel()
        {
            snobutton button = null;
            string define = "cancel_button";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Button cancel", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Integrations()
        {
            snobutton button = null;
            string define = "//*[@id='app']//div[@class='body']//div[@class='category'][2]//div[@class='task completed']//span[@class='task-link']";
            By by = By.XPath(define);
            button = (snobutton)Base.SNGObject("Button cancel", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_AddIntegration()
        {
            snobutton button = null;
            string define = "//div[@id='app']//a[text()='Add Integration']";
            By by = By.XPath(define);
            button = (snobutton)Base.SNGObject("Button cancel", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_New()
        {
            snobutton button = null;
            string define = "sysverb_new";
            By by = By.Id(define);
            button = (snobutton)Base.SNGObject("Button New", "button", by, _parent, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Edit()
        {
            snobutton button = null;
            string define = "button[id^='sysverb_edit']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Button Edit", "button", by, _parent, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Filter()
        {
            snobutton button = null;
            string define = "a[id$='_filter_toggle_image']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Button filter", "button", by, _parent, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Add()
        {
            snobutton button = null;
            string define = "a[class$='add']";
            //string define = "a[data-original-title='Add']"; 
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Add", "button", by, null, snobase.MainFrame);
            return button;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_Remove()
        {
            snobutton button = null;
            string define = "a[class$='remove']";
            By by = By.CssSelector(define);
            button = (snobutton)Base.SNGObject("Remove", "button", by, null, snobase.MainFrame);
            return button;
        }
        #endregion End - Button

        #region Others

        public snoelement Toolbar_Filter([Optional] bool noWait)
        {
            snoelement tbar = null;
            string define = ".filterToolbar";
            tbar = (snoelement)Base.SNGObject("Tool bar", "element", By.CssSelector(define), _parent, snobase.MainFrame, noWait);
            return tbar;
        }

        #endregion End - Others

        #endregion End - Control methods
    }
}
