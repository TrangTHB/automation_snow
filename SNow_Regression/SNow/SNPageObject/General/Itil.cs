﻿using System;
using OpenQA.Selenium;
using System.Threading;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;


namespace SNow
{
    public class Itil
    {

        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;
        string allTabDefine = "span[class^='tab_header']>span[class^='tabs2_tab']:not([style*='display: none']) span[class='tab_caption_text'], div[class$='related']:not([style*='display: none']) h2";
        string allIntegrationTabDefine = "li[role^='presentation']>a";
        #endregion End - Variables
        
        #region Properties

        public snobase Base
        {
            get { return _snobase; }
            set { _snobase = value; }
        }

        #endregion End - Properties

        #region Constructor

        public Itil(snobase obase, string name)
        {
            Base = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Private methods

        private void PleaseWait()
        {
            ReadOnlyCollection<IWebElement> eles;
            string define = "#attachment>div[class='modal-dialog modal-md']>div[class='modal-content'] img[id='please_wait']";
            Base.Driver.SwitchTo().DefaultContent();
            Base.Driver.SwitchTo().Frame(snobase.MainFrame);
            eles = Base.Driver.FindElements(By.CssSelector(define));
            if (eles.Count > 0)
            {
                int count = 0;
                while (count < 10 && eles[0].Displayed)
                {
                    Thread.Sleep(1000);
                    count++;
                }
            }
            else
            {
                Thread.Sleep(5000);
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Verify_Attachment(string fileName, [Optional] bool noWait)
        {
            bool flag = false;
            string define = "a[class='content_editable']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Attachment files", by, null, snobase.MainFrame, false, noWait);
            flag = list.HaveItemsInlist(fileName, ";", noWait);
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Verify_ContextMenu_Existed()
        {
            bool flag = true;
            string define = "context_menu";
            By by = By.ClassName(define);
            snoelements menus = Base.SNGObjects("Menu list", by, null, snobase.MainFrame);
            if (menus.MyList.Count <= 0) { flag = false; }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Select_ContextMenu_Item(string item, bool javaClick)
        {
            bool flag = true;
            string menudefine = "context_menu";
            string itemdefine = "div[class*='context_item']";
            By menuby = By.ClassName(menudefine);
            By itemby = By.CssSelector(itemdefine);
            snoelements menus = Base.SNGObjects("Menu list", menuby, null, snobase.MainFrame);

            if (menus.MyList.Count > 0)
            {
                snoelement parent = new snoelement("parent", Base.Driver, menus.MyList[menus.MyList.Count - 1].MyElement);
                snoelements items = Base.SNGObjects("Item list", itemby, parent, snobase.MainFrame);
                if (items.MyList.Count > 0)
                {
                    bool flagF = false;
                    foreach (Auto.oelement ele in items.MyList)
                    {
                        if (ele.MyText.Trim().ToLower().Equals(item.Trim().ToLower()))
                        {
                            //-- Move to element
                            Actions ac = new Actions(Base.Driver);
                            ac.MoveToElement(ele.MyElement);
                            ac.Perform();
                            //-- End move to element
                            flagF = ele.Click(javaClick);
                            if (flagF)
                                break;
                        }
                    }
                    if (!flagF)
                    {
                        flag = false;
                        System.Console.WriteLine("***[INFO]: Not found item (" + item + ")");
                    }
                }
                else
                {
                    flag = false;
                    System.Console.WriteLine("***[INFO]: Not found any item.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("***[INFO]: Not found any context menu.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement GRelatedTabByName(string tabName, [Optional] string tabDefine)
        {
            snoelement tab = null;
            string define = "div[class$='tab_section']>span[class^='list_span_related'], div[class$='related']:not([style*='display: none'])";
            
            if (tabDefine != null && tabDefine != string.Empty)
                define = tabDefine;
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("List tab", by, null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList)
            {
                IWebElement ie = null;
                if (ele.MyElement.GetAttribute("tab_caption") == null)
                    ie = ele.MyElement.FindElement(By.XPath(".."));
                else
                    ie = ele.MyElement;
                if (!tabName.Contains("@@"))
                {
                    if (ele.MyText.Trim().Contains("(") && ele.MyText.Trim().Contains(")"))
                    {
                        if (ie.GetAttribute("tab_caption").Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(tabName.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                        {
                            tab = new snoelement("Tab " + tabName, Base.Driver, ele.MyElement);
                            break;
                        }
                    }
                    else
                    {
                        if (ie.GetAttribute("tab_caption").Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Equals(tabName.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                        {
                            tab = new snoelement("Tab " + tabName, Base.Driver, ele.MyElement);
                            break;
                        }
                    }
                }
                else
                {
                    tabName = tabName.Replace("@@", "");
                    if (ie.GetAttribute("tab_caption").Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(tabName.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                    {
                        tab = new snoelement("Tab " + tabName, Base.Driver, ele.MyElement);
                        break;
                    }
                }
            }
            return tab;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedItem(string item, snoelements list)
        {
            bool flag = false;
            bool flagM = true;

            if (item.Contains("@@"))
            {
                item = item.Substring(2, item.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ [" + item + "]");
            }
            else
            {
                Console.WriteLine("---finding..M [" + item + "]");
            }

            foreach (Auto.oelement ele in list.MyList)
            {
                string mess = ele.MyText.Trim().Replace("Error Message", "");
                Console.WriteLine("---[Runtime]: [" + mess + "]");
                if (flagM)
                {
                    if (Base.StringCompare( mess.ToLower(),item))
                    {
                        flag = true;
                        break;
                    }
                }
                else
                {
                    if (mess.ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found]: [" + item + "] in actual items.");
            else
            {
                Console.WriteLine("-*-[NOT FOUND]: [" + item + "] in actual items.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyVisibleControls(string expType, string expControls, ref string error, Dictionary<string, snoelement> dic)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            if (dic.Count > 0)
            {
                Console.WriteLine("***[Info]: Verify expected " + expType + " visible on form " + "***");
                Console.WriteLine("***[Info]: Expected control(s) need to verify: [" + expArray.Length + "]");
                Console.WriteLine("***[Info]: Actual control(s) on form: [" + dic.Count + "]");
                Console.WriteLine("...........................................................................");

                foreach (string control in expArray)
                {
                    bool flagF = VerifyExpectedVisibleControl(expType, control, dic);
                    if (!flagF && flag)
                        flag = false;
                }

                Console.WriteLine("---------------------------------------------------------------------------");
                Console.WriteLine("***[Info]: Verify actual controls have in expected controls list ***");
                Console.WriteLine("...........................................................................");

                foreach (var d in dic)
                {
                    bool flagF = VerifyActualVisibleControl(expType, d.Key, expArray);
                    if (!flagF && flag && error == string.Empty)
                        error = "WARNING";
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("***[Info]: Not found any " + expType + " on form.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedVisibleControl(string expType, string control, Dictionary<string, snoelement> dic)
        {
            bool flag = false;
            bool flagM = true;

            if (control.Contains("@@"))
            {
                control = control.Substring(2, control.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ [" + control + "]");
            }
            else
            {
                Console.WriteLine("---finding..M [" + control + "]");
            }

            foreach (var d in dic)
            {
                if (flagM)
                {
                    if (d.Key.ToLower().Equals(control.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
                else
                {
                    if (d.Key.Trim().ToLower().Contains(control.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found]: Expected " + expType + " [" + control + "] on actual form controls.");
            else
                Console.WriteLine("-*-[NOT FOUND]: Expected " + expType + " [" + control + "] on actual form controls.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyActualVisibleControl(string expType, string control, string[] expArray)
        {
            bool flag = false;

            Console.WriteLine("---finding... " + expType + " [" + control + "] in expected " + expType + " list.");

            foreach (string exp in expArray)
            {
                string item = exp;
                if (item.Contains("@@"))
                {
                    item = exp.Substring(2, item.Length - 2);
                    if (control.Trim().ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        Console.WriteLine("-*-[Info]: Runtime [" + control + "] - Expected: [" + exp + "]");
                        break;
                    }
                }
                else
                {
                    if (item.Trim().ToLower().Equals(control.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found]: " + expType + " [" + control + "] in expected " + expType + " list.");
            else
                Console.WriteLine("-*-[NOT FOUND]: " + expType + " [" + control + "] in expected " + expType + " list.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyReadonlyControls(string expType, string expControls, ref string error, Dictionary<string, snoelement> dic)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            Dictionary<string, snoelement> readonlyDic = new Dictionary<string, snoelement>();

            foreach (var d in dic)
            {
                bool isReadOnly = true;
                string section = string.Empty;
                switch (expType.Trim().ToLower())
                {
                    case "list":
                        snolist list = new snolist("list", Base.Driver, d.Value.MyElement);
                        try
                        {
                            section = d.Value.MySection;
                            if (section != null && section != string.Empty)
                                Select_Tab(section, false, true);
                        }
                        catch { }
                        isReadOnly = list.MyReadOnly;
                        break;
                    case "radiogroup":
                        snoradiogroup radiogroup = new snoradiogroup("radiogroup", Base.Driver, d.Value.MyElement);
                        try
                        {
                            section = d.Value.MySection;
                            if (section != null && section != string.Empty)
                                Select_Tab(section, false, true);
                        }
                        catch { }
                        isReadOnly = radiogroup.MyReadOnly;
                        break;
                    case "checkboxgroup":
                        snocheckboxgroup checkboxgroup = new snocheckboxgroup("checkboxgroup", Base.Driver, d.Value.MyElement);
                        try
                        {
                            section = d.Value.MySection;
                            if (section != null && section != string.Empty)
                                Select_Tab(section, false, true);
                        }
                        catch { }
                        isReadOnly = checkboxgroup.MyReadOnly;
                        break;
                    case "currency":
                        snocurrency currency = new snocurrency("currency", Base.Driver, d.Value.MyElement);
                        try
                        {
                            section = d.Value.MySection;
                            if (section != null && section != string.Empty)
                                Select_Tab(section, false, true);
                        }
                        catch { }
                        isReadOnly = currency.MyReadOnly;
                        break;
                    case "checkbox":
                        snocheckbox checkbox = new snocheckbox("checkbox", Base.Driver, d.Value.MyElement);
                        try 
                        {
                            section = checkbox.MySection;
                            if (section != null && section != string.Empty)
                                Select_Tab(section, false, true);
                        }
                        catch { }
                        checkbox.MoveToElement();
                        string result = checkbox.MyReadOnly;
                        if (result != null && (result.Trim().ToLower() == "readonly" || result.Trim().ToLower() == "true"))
                            isReadOnly = true;
                        else
                            isReadOnly = false;
                        break;
                    default:
                        try
                        {
                            section = d.Value.MySection;
                            if (section != null && section != string.Empty)
                                Select_Tab(section, false, true);
                        }
                        catch { }

                        d.Value.MoveToElement();
                        isReadOnly = d.Value.ReadOnly;
                        break;
                }

                if (isReadOnly)
                {
                    readonlyDic.Add(d.Key, d.Value);
                }
            }
            Console.WriteLine("*** Found total: [" + readonlyDic.Count + "] readonly " + expType + " on form.");
            foreach (var rd in readonlyDic)
            {
                Console.WriteLine("*** Found readonly " + expType + " [" + rd.Key + "] on form.");
            }
            Console.WriteLine("...........................................................................");
            if (readonlyDic.Count > 0)
            {
                Console.WriteLine("***[Info]: Verify expected readonly " + expType + " on form " + "***");
                Console.WriteLine("***[Info]: Expected readonly control(s) need to verify: [" + expArray.Length + "]");
                Console.WriteLine("***[Info]: Actual readonly control(s) on form: [" + readonlyDic.Count + "]");
                Console.WriteLine("...........................................................................");

                foreach (string control in expArray)
                {
                    bool flagF = VerifyExpectedReadonlyControl(expType, control, readonlyDic);
                    if (!flagF && flag)
                        flag = false;
                }

                Console.WriteLine("---------------------------------------------------------------------------");
                Console.WriteLine("***[Info]: Verify actual readonly controls have in expected controls list ***");
                Console.WriteLine("...........................................................................");

                foreach (var rd in readonlyDic)
                {
                    bool flagF = VerifyActualReadOnlyControl(expType, rd.Key, expArray);
                    if (!flagF && flag && error == string.Empty)
                        error = "WARNING";
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("***[Info]: Not found any readonly " + expType + " on form.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedReadonlyControl(string expType, string control, Dictionary<string, snoelement> dic)
        {
            bool flag = false;
            bool flagM = true;

            if (control.Contains("@@"))
            {
                control = control.Substring(2, control.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ [" + control + "]");
            }
            else
            {
                Console.WriteLine("---finding..M [" + control + "]");
            }
            snoelement ele = null;
            foreach (var d in dic)
            {
                if (flagM)
                {
                    if (d.Key.ToLower().Equals(control.Trim().ToLower()))
                    {
                        ele = d.Value;
                        flag = true;
                        break;
                    }
                }
                else
                {
                    if (d.Key.Trim().ToLower().Contains(control.Trim().ToLower()))
                    {
                        ele = d.Value;
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
            {
                Console.WriteLine("-*-[Found]: Expected readonly " + expType + " [" + control + "] on actual form controls.");
            }
            else
                Console.WriteLine("-*-[NOT FOUND]: Expected readonly " + expType + " [" + control + "] on actual form controls.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyActualReadOnlyControl(string expType, string control, string[] expArray)
        {
            bool flag = false;

            Console.WriteLine("---finding... Readonly " + expType + " [" + control + "] in expected " + expType + " list.");

            foreach (string exp in expArray)
            {
                string item = exp;
                if (item.Contains("@@"))
                {
                    item = exp.Substring(2, item.Length - 2);
                    if (control.Trim().ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        Console.WriteLine("-*-[Info]: Runtime [" + control + "] - Expected: <" + exp + ">");
                        break;
                    }
                }
                else
                {
                    if (item.Trim().ToLower().Equals(control.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found]: Readonly " + expType + " [" + control + "] in expected " + expType + " list.");
            else
                Console.WriteLine("-*-[NOT FOUND]: Readonly " + expType + " [" + control + "] in expected " + expType + " list.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyDefaultValueControls(string expType, string expControls, ref string error, Dictionary<string, snoelement> dic)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            if (dic.Count > 0)
            {
                Console.WriteLine("***[Info]: Verify " + expType + " default value on form " + "***");
                Console.WriteLine("...........................................................................");

                foreach (string control in expArray)
                {
                    bool flagF = VerifyDefaultValueControl(expType, control, dic);
                    if (!flagF && flag)
                        flag = false;
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("***[Info]: Not found any " + expType + " on form.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyDefaultValueControl(string expType, string control, Dictionary<string, snoelement> dic)
        {
            bool flag = false;
            bool flagM = true;

            string[] array = control.Split('=');
            string ctr = array[0];
            string val = array[1];

            if (val.Contains("@@"))
            {
                val = val.Substring(2, val.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ [" + val + "]");
            }
            else
            {
                Console.WriteLine("---finding..M [" + val + "]");
            }

            foreach (var d in dic)
            {
                if (d.Key.ToLower().Equals(ctr.Trim().ToLower()))
                {
                    string runtimeValue = null;
                    string section = null;
                    switch (expType.ToLower())
                    {
                        case "textbox":
                            snotextbox textbox = new snotextbox(ctr, Base.Driver, d.Value.MyElement);
                            runtimeValue = textbox.Text;
                            break;
                        case "textarea":
                            snotextarea textarea = new snotextarea(ctr, Base.Driver, d.Value.MyElement);
                            runtimeValue = textarea.Text;
                            break;
                        case "lookup":
                            snolookup lookup = new snolookup(ctr, Base.Driver, d.Value.MyElement);
                            runtimeValue = lookup.Text;
                            break;
                        case "combobox":
                            snocombobox combobox = new snocombobox(ctr, Base.Driver, d.Value.MyElement);
                            try 
                            {
                                section = combobox.MySection;
                                if (section != null && section != string.Empty)
                                    Select_Tab(section, false, true);
                            }
                            catch { }
                            
                            combobox.MoveToElement();
                            runtimeValue = combobox.Text;
                            break;
                        case "date":
                            snodate date = new snodate(ctr, Base.Driver, d.Value.MyElement);
                            runtimeValue = date.Text;
                            break;
                        case "datetime":
                            snodatetime datetime = new snodatetime(ctr, Base.Driver, d.Value.MyElement);
                            runtimeValue = datetime.Text;
                            break;
                        case "checkbox":
                            snocheckbox checkbox = new snocheckbox(ctr, Base.Driver, d.Value.MyElement);
                            try 
                            {
                                section = checkbox.MySection;
                                if (section != null && section != string.Empty)
                                    Select_Tab(section, false, true);
                            }
                            catch { }
                            
                            checkbox.MoveToElement();
                            runtimeValue = checkbox.Checked.ToString();
                            break;
                    }
                    Console.WriteLine("-*-[Runtime value]: [" + runtimeValue + "]");

                    if (flagM)
                    {
                        if (runtimeValue != null && runtimeValue.Trim().ToLower().Equals(val.Trim().ToLower()))
                            flag = true;
                        else if (runtimeValue == null && val.Trim() == string.Empty)
                            flag = true;
                        break;
                    }
                    else
                    {
                        if (runtimeValue != null && runtimeValue.Trim().ToLower().Contains(val.Trim().ToLower()))
                            flag = true;
                        else if (runtimeValue == null && val.Trim() == string.Empty)
                            flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found]: " + expType + " with default value [" + control + "] on form and default value = [" + val + "] as expected.");
            else
                Console.WriteLine("-*-[NOT FOUND]: " + expType + " with default value [" + control + "] on form Or default value = [" + val + "] NOT as expected.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyButtonControls(bool expNotFound, string expControls, ref string error, [Optional] Dictionary<string, snoelement> dic, [Optional] string split)
        {
            bool flag = true;

            string[] expArray = null;
            string spl = "|";

            if (split != null && split != string.Empty)
                spl = split;

            if (expControls.Contains(spl))
                expArray = expControls.Split(new string[] { spl }, StringSplitOptions.None);
            else
                expArray = new string[] { expControls };

            if (dic.Count > 0)
            {
                if (!expNotFound)
                    Console.WriteLine("***[Info]: Verify expected button visible on form " + "***");
                else
                    Console.WriteLine("***[Info]: Verify expected button NOT visible on form " + "***");
                Console.WriteLine("***[Info]: Expected button need to verify: [" + expArray.Length + "]");
                Console.WriteLine("***[Info]: Actual button on form: [" + dic.Count + "]");
                Console.WriteLine("...........................................................................");

                foreach (string control in expArray)
                {
                    bool flagF = VerifyExpectedButtonControl(expNotFound, control, dic);
                    if (!flagF && flag)
                        flag = false;
                }

                Console.WriteLine("---------------------------------------------------------------------------");
                Console.WriteLine("***[Info]: Verify actual button have in expected controls list ***");
                Console.WriteLine("...........................................................................");

                foreach (var d in dic)
                {
                    bool flagF = VerifyActualButtonControl(expNotFound, d.Key, expArray);
                    if (!flagF && flag && error == string.Empty)
                        error = "WARNING";
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("***[Info]: Not found any button on form.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedButtonControl(bool expNotFound, string control, Dictionary<string, snoelement> dic)
        {
            bool flag = false;
            bool flagM = true;

            if (control.Contains("@@"))
            {
                control = control.Substring(2, control.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ [" + control + "]");
            }
            else
            {
                Console.WriteLine("---finding..M [" + control + "]");
            }

            foreach (var d in dic)
            {
                if (flagM)
                {
                    if (d.Key.ToLower().Normalize(System.Text.NormalizationForm.FormKD).Equals(control.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                    {
                        flag = true;
                        break;
                    }
                }
                else
                {
                    if (d.Key.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(control.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
            {
                if (!expNotFound)
                    Console.WriteLine("-*-[Found]: Expected button [" + control + "] on actual form controls.");
                else
                {
                    flag = false;
                    Console.WriteLine("-*-[FAILED - FOUND]: Expected button [" + control + "] on actual form controls. Expected NOT found.");
                }
            }
            else
            {
                if (!expNotFound)
                    Console.WriteLine("-*-[FAILED - NOT FOUND]: Expected button [" + control + "] on actual form controls.");
                else
                {
                    flag = true;
                    Console.WriteLine("-*-[Passed - Not found]: Expected button [" + control + "] on actual form controls as expectation.");
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyActualButtonControl(bool expNotFound, string control, string[] expArray)
        {
            bool flag = false;

            Console.WriteLine("---finding... Actual button [" + control + "] in expected button list.");

            foreach (string exp in expArray)
            {
                string item = exp;
                if (item.Contains("@@"))
                {
                    item = exp.Substring(2, item.Length - 2);
                    if (control.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(item.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                    {
                        flag = true;
                        Console.WriteLine("-*-[Info]: Runtime [" + control + "] - Expected: [" + exp + "]");
                        break;
                    }
                }
                else
                {
                    if (item.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Equals(control.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
            {
                if (!expNotFound)
                    Console.WriteLine("-*-[Found]: actual button [" + control + "] in expected button list.");
                else
                {
                    flag = false;
                    Console.WriteLine("-*-[FOUND]: actual button [" + control + "] in expected button list. Expected NOT found.");
                }
            }

            else
            {
                if (!expNotFound)
                    Console.WriteLine("-*-[NOT FOUND]: actual button [" + control + "] in expected button list.");
                else
                {
                    flag = true;
                    Console.WriteLine("-*-[Not found]: actual button [" + control + "] in expected button list as expectation.");
                }
            }


            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyComboboxItems(string control, string items, ref string error, [Optional] snocombobox combo)
        {
            bool flag = true;
            snocombobox combobox = null;
            if (combo == null)
                combobox = (snocombobox)Base.GControlByLabel(control);
            else
                combobox = combo;

            try
            {
                string section = combobox.MySection;
                if (section != null && section != string.Empty)
                    Select_Tab(section);
            }
            catch { }
            
            Thread.Sleep(1000);
            flag = combobox.Existed;
            if (flag)
            {
                Console.WriteLine("-*-[Info]: Verify expected items of combobox [" + control + "] existed.");
                Console.WriteLine("...........................................................................");
                flag = combobox.VerifyExpectedItemsExisted(items);
                Console.WriteLine("-*-[Info]: Verify actual items of combobox [" + control + "] have in expected items list.");
                Console.WriteLine("...........................................................................");
                bool flagT = combobox.VerifyActualItemsExisted(items);
                if (!flagT && flag && error == string.Empty)
                    error = "WARNING";
            }
            else
            {
                Console.WriteLine("-*-[Info]: Not found combobox [" + control + "] on form.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyComboboxParentChildItem(string expControl, ref string error)
        {
            bool flag = true;
            bool flagT = true;

            string[] expArray = expControl.Split(new string[] { "::::" }, StringSplitOptions.None);
            string parent = expArray[0];
            string child = expArray[1];
            string[] arrParent = parent.Split('=');
            string[] arrChild = child.Split('=');
            string spControl = arrParent[0];
            string spValue = arrParent[1];
            string scControl = arrChild[0];
            string scValue = arrChild[1];

            snocombobox pControl = (snocombobox)Base.GControlByLabel(spControl);
            snocombobox cControl = (snocombobox)Base.GControlByLabel(scControl);

            if (pControl.MyElement != null)
            {
                try 
                {
                    string pSection = pControl.MySection;
                    if (pSection != null && pSection != string.Empty)
                    {
                        Select_Tab(pSection);
                        
                    }
                }
                catch 
                { }

                pControl.MoveToElement();

                string[] arrPValue = null;
                if (spValue.Contains("::"))
                    arrPValue = spValue.Split(new string[] { "::" }, StringSplitOptions.None);
                else
                    arrPValue = new string[] { spValue };

                string[] arrCValue = null;
                if (scValue.Contains("::"))
                    arrCValue = scValue.Split(new string[] { "::" }, StringSplitOptions.None);
                else
                    arrCValue = new string[] { scValue };

                if (arrPValue.Length == arrCValue.Length)
                {
                    int index = 0;
                    foreach (string vP in arrPValue)
                    {
                        try
                        {
                            flagT = pControl.SelectItem(vP);
                        }
                        catch { flagT = false; }

                        int count = 0;

                        while (count < 5 && !flagT)
                        {
                            flagT = pControl.SelectItem(vP);
                            count++;
                        }

                        if (flagT)
                        {
                            Thread.Sleep(2000);
                            WaitLoading();
                            Console.WriteLine("-*-[Info]: Parent combobox:[" + spControl + "] - Value:[" + vP + "]");
                            Console.WriteLine("-*-[Info]: Verify items of child combobox:[" + scControl + "]");
                            Console.WriteLine("...........................................................................");
                            string vC = arrCValue[index];
                            bool flagV = VerifyComboboxItems(scControl, vC, ref error, cControl);
                            if (!flagV && flag)
                                flag = false;

                            index++;
                        }
                        else { flag = false; Console.WriteLine("-*-[FAILED]: Cannot input value for parent [" + vP + "]"); break; }
                    }
                }
                else
                {
                    flag = false;
                    Console.WriteLine("-*-[Info]: The number of items between parent and child are not matched. Please review excel data input.");
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("-*-[Info]: Not found parent control.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool InputValueForControl(string exp)
        {
            bool flag = true;
            string[] array = exp.Split('=');
            string label = array[0].Trim();
            string value = array[1].Trim();
            object obj = Base.GControlByLabel(label);
            flag = InputValue(obj, value, label);
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool InputValue(object obj, string value, string label)
        {
            bool flag = true;

            string type = obj.GetType().FullName.Trim().ToLower().Replace("snow.sno", "");
            string section = string.Empty;
            switch (type.Trim().ToLower())
            {
                case "combobox":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for combobox [" + label + "].");
                    snocombobox combobox = (snocombobox)obj;
                    try 
                    {
                        section = combobox.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    
                    Thread.Sleep(1000);
                    combobox.MoveToElement();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                            flag = combobox.SelectItem(1);
                        else
                            flag = combobox.SelectItem(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found combobox [" + label + "].");

                    if (!flag)
                        Console.WriteLine("-*-[Info]: Cannot input value for combobox [" + label + "].");
                    break;
                case "lookup":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for lookup [" + label + "].");
                    snolookup lookup = (snolookup)obj;
                    try 
                    {
                        section = lookup.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    
                    Thread.Sleep(1000);
                    lookup.MoveToElement();
                    flag = lookup.Existed;
                    if (flag)
                        if (value.ToLower() == "auto")
                        {
                            flag = lookup.ClickOnFindButton();
                            if (flag)
                            {
                                Thread.Sleep(5000);
                                ItilList list = new ItilList(Base, "Search");
                                Base.SwitchToPage(1);
                                list.WaitLoading(false, true);
                                snotable table = list.Table_List(null, true);
                                table.CellDefine = "td[style]:not([class]) a, td[title][class='tree_item_text'] a";
                                Base.Driver.SwitchTo().DefaultContent();

                                ReadOnlyCollection<IWebElement> rows = table.MyElement.FindElements(By.CssSelector(table.RowDefine));
                                for (int i = 0; i < 20; i++)
                                {
                                    IWebElement row = rows[i];
                                    Auto.oelement r = new Auto.oelement("row", Base.Driver, row);
                                    string temp = table.CellText(r, 0);
                                    if (temp != null && temp.Trim() != string.Empty && temp.Trim().ToLower() != "(empty)")
                                    {
                                        flag = table.CellClick(0, r);
                                        break;
                                    }
                                }
                                Base.SwitchToPage(0);
                                if (!flag)
                                    Console.WriteLine("-*-[Info]: Cannot select value for lookup.");
                            }
                            else Console.WriteLine("-*-[Info]: Error when click on lookup find button.");
                        }
                        else
                        {
                            flag = lookup.Select(value);
                        }
                    else
                        Console.WriteLine("-*-[Info]: Not found lookup [" + label + "].");

                    if (!flag)
                        Console.WriteLine("-*-[Info]: Cannot input value for textbox [" + label + "].");
                    break;
                case "textbox":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for textbox [" + label + "].");
                    snotextbox textbox = (snotextbox)obj;
                    try 
                    {
                        section = textbox.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    
                    Thread.Sleep(1000);
                    textbox.MoveToElement();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                            flag = textbox.SetText("Automation value");
                        else
                            flag = textbox.SetText(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found textbox [" + label + "].");

                    if (flag)
                        textbox.MyElement.SendKeys(Keys.Tab);
                    else
                        Console.WriteLine("-*-[Info]: Cannot input value for textbox [" + label + "].");
                    break;
                case "textarea":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for textarea [" + label + "].");
                    snotextarea textarea = (snotextarea)obj;
                    try
                    {
                        section = textarea.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    Thread.Sleep(1000);
                    textarea.MoveToElement();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                            flag = textarea.SetText("Automation value");
                        else
                            flag = textarea.SetText(value);

                        if (flag)
                            textarea.MyElement.SendKeys(Keys.Tab);
                        else
                            Console.WriteLine("-*-[Info]: Cannot input value for textarea [" + label + "].");
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found textarea [" + label + "].");
                    break;
                case "date":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for date [" + label + "].");
                    snodate date = (snodate)obj;
                    try
                    {
                        section = date.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    
                    Thread.Sleep(1000);
                    date.MoveToElement();
                    flag = date.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            string startDate = DateTime.Now.ToString("yyyy-MM-dd");
                            string endDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            if (label.ToLower().Contains("start") || label.ToLower().Contains("begin"))
                            {
                                flag = date.SetText(startDate);
                            }
                            else if (label.ToLower().Contains("end") || label.ToLower().Contains("complete") || label.ToLower().Contains("extension"))
                            {
                                flag = date.SetText(endDate);
                            }
                            else
                            {
                                flag = date.SetText(startDate);
                            }
                        }
                        else
                            flag = date.SetText(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found date [" + label + "].");

                    if (flag)
                        date.MyElement.SendKeys(Keys.Tab);
                    else
                        Console.WriteLine("-*-[Info]: Cannot input value for date [" + label + "].");
                    break;
                case "datetime":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for datetime [" + label + "].");
                    snodatetime datetime = (snodatetime)obj;
                    try
                    {
                        section = datetime.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    Thread.Sleep(1000);
                    datetime.MoveToElement();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            string startDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            string endDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                            if (label.ToLower().Contains("start") || label.ToLower().Contains("begin"))
                            {
                                flag = datetime.SetText(startDate);
                            }
                            else if (label.ToLower().Contains("end") || label.ToLower().Contains("complete") || label.ToLower().Contains("extension"))
                            {
                                flag = datetime.SetText(endDate);
                            }
                            else
                            {
                                flag = datetime.SetText(startDate);
                            }
                        }
                        else
                            flag = datetime.SetText(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found datetime [" + label + "].");

                    if (flag)
                        datetime.MyElement.SendKeys(Keys.Tab);
                    else
                        Console.WriteLine("-*-[Info]: Cannot input value for datetime [" + label + "].");
                    break;
                case "checkboxgroup":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for checkboxgroup [" + label + "].");
                    snocheckboxgroup cbgroup = (snocheckboxgroup)obj;
                    try
                    {
                        section = cbgroup.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    Thread.Sleep(1000);
                    cbgroup.MoveToElement();
                    flag = cbgroup.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            snocheckbox fistItem = cbgroup.Items[0];
                            flag = fistItem.Click();
                        }
                        else
                        {
                            foreach (snocheckbox cb in cbgroup.Items) 
                            {
                                if (cb.MyText.Trim().ToLower() == value.Trim().ToLower()) 
                                {
                                    flag = cb.Click();
                                }
                            }
                        }
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found checkboxgroup [" + label + "].");
                    break;
                case "radiogroup":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for radiogroup [" + label + "].");
                    snoradiogroup rdgroup = (snoradiogroup)obj;
                    try
                    {
                        section = rdgroup.MySection;
                        if (section != null && section != string.Empty)
                            Select_Tab(section, false, true);
                    }
                    catch { }
                    Thread.Sleep(1000);
                    rdgroup.MoveToElement();
                    flag = rdgroup.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            foreach (snoradio rd in rdgroup.Items)
                            {
                                if (rd.MyText.Trim().ToLower() != "-- none --")
                                {
                                    flag = rd.Click();
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (snoradio rd in rdgroup.Items)
                            {
                                if (rd.MyText.Trim().ToLower() == value.Trim().ToLower())
                                {
                                    flag = rd.Click();
                                    break;
                                }
                            }
                        }
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found radiogroup [" + label + "].");
                    break;
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyControlsDependentAdditional(string expControl, ref string error)
        {
            bool flag = true;

            string[] expArray = expControl.Split(new string[] { "::::" }, StringSplitOptions.None);
            string parent = expArray[0];
            string child = expArray[1];
            string[] arrParent = parent.Split('=');
            string spControl = arrParent[0];
            string spValue = arrParent[1];

            string[] arrPValue = null;
            if (spValue.Contains("::"))
                arrPValue = spValue.Split(new string[] { "::" }, StringSplitOptions.None);
            else
                arrPValue = new string[] { spValue };

            string[] arrCValue = null;
            if (child.Contains("::"))
                arrCValue = child.Split(new string[] { "::" }, StringSplitOptions.None);
            else
                arrCValue = new string[] { child };

            if (arrPValue.Length == arrCValue.Length)
            {
                object pObj = Base.GControlByLabel(spControl);
                int index = 0;
                foreach (string vP in arrPValue)
                {
                    bool flagT = InputValue(pObj, vP, spControl);
                    Thread.Sleep(2000);
                    if (flagT)
                    {
                        flagT = VerifyControlsAdditional(arrCValue[index]);
                    }
                    else
                    {
                        Console.WriteLine("-*-[Info]: Error when input value [" + vP + "] for control [" + spControl + "].");
                        break;
                    }

                    if (!flagT && flag)
                        flag = false;

                    index++;
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("-*-[Info]: The number of items between parent and child are not matched. Please review excel data input.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyControlsAdditional(string expInfo)
        {
            bool flag = true;

            string[] arrInfo = null;
            if (expInfo.Contains(";"))
                arrInfo = expInfo.Split(new string[] { ";" }, StringSplitOptions.None);
            else
                arrInfo = new string[] { expInfo };

            foreach (string info in arrInfo)
            {
                bool flagV = VerifyControlAdditional(info);
                if (!flagV && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyControlAdditional(string expInfo)
        {
            bool flag = true;

            string[] arrInfo = expInfo.Split('=');
            string control = arrInfo[0];
            string[] arrExp = arrInfo[1].Split(':');
            string type = arrExp[0];
            string ronly = arrExp[1];
            string mandatory = arrExp[2];

            object obj = Base.GControlByLabel(control);
            if (obj == null)
            {
                flag = false;
                Console.WriteLine("-*-[FAILED]: NOT FOUND control [" + control + "] on form as expected.");
            }
            else
            {
                Console.WriteLine("-*-[Passed]: Found control [" + control + "] on form as expected.");
                string rtype = obj.GetType().FullName.Trim().ToLower().Replace("snow.sno", "");
                flag = rtype.Trim().ToLower().Equals(type.Trim().ToLower());
                if (flag)
                {
                    Console.WriteLine("-*-[Passed]: Type of control [" + control + "] is [" + rtype + "] as expected.");
                    bool flagR = true;
                    switch (rtype.Trim().ToLower())
                    {
                        case "list":
                            snolist list = (snolist)obj;
                            flagR = list.MyReadOnly;
                            break;
                        case "currency":
                            snocurrency currency = (snocurrency)obj;
                            flagR = currency.MyReadOnly;
                            break;
                        default:
                            string temp = obj.GetType().GetProperty("ReadOnly").GetValue(obj).ToString();
                            if (temp.Trim().ToLower() == "true")
                                flagR = true;
                            else
                                flagR = false;
                            break;
                    }
                    string str1 = string.Empty;
                    if (flagR)
                        str1 = "yes";
                    else
                        str1 = "no";
                    if ((flagR && ronly.Trim().ToLower() == "yes") || (!flagR && ronly.Trim().ToLower() == "no"))
                    {
                        Console.WriteLine("-*-[Passed]: Control [" + control + "] have property [ReadOnly = " + str1 + "] as expected.");
                        bool flagM = true;
                        string rmandatory = obj.GetType().GetProperty("isMandatory").GetValue(obj).ToString();
                        if (rmandatory.Trim().ToLower() == "true")
                            flagM = true;
                        else
                            flagM = false;
                        string str2 = string.Empty;
                        if (flagM)
                            str2 = "yes";
                        else
                            str2 = "no";
                        if ((flagM && mandatory.Trim().ToLower() == "yes") || (!flagM && mandatory.Trim().ToLower() == "no"))
                        {
                            Console.WriteLine("-*-[Passed]: Control [" + control + "] have property [Mandatory = " + str2 + "] as expected.");
                        }
                        else
                        {
                            flag = false;
                            Console.WriteLine("-*-[FAILED]: Control [" + control + "] have property [Mandatory = " + str2 + "] NOT as expected - Expected [" + mandatory + "]");
                        }
                    }
                    else
                    {
                        flag = false;
                        Console.WriteLine("-*-[FAILED]: Control [" + control + "] have property [ReadOnly = " + str1 + "] NOT as expected - Expected [" + ronly + "]");
                    }
                }
                else
                {
                    Console.WriteLine("-*-[FAILED]: Invalid type of control [" + control + "] - Runtime type: [" + rtype + "] - Expected type: [" + type + "].");
                }
            }
            Console.WriteLine("...........................................................................");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyCheckboxGroupItems(string control, string items, ref string error, [Optional] snocheckboxgroup cbg)
        {
            bool flag = true;
            snocheckboxgroup checkboxgroup = null;
            if (cbg == null)
                checkboxgroup = (snocheckboxgroup)Base.GControlByLabel(control);
            else
                checkboxgroup = cbg;
            try
            {
                string section = checkboxgroup.MySection;

                if (section != null && section != string.Empty)
                    Select_Tab(section);
            }
            catch { }
            Thread.Sleep(1000);
            flag = checkboxgroup.Existed;
            if (flag)
            {
                Console.WriteLine("-*-[Info]: Verify expected items of checkboxgroup [" + control + "] existed.");
                Console.WriteLine("...........................................................................");
                flag = checkboxgroup.VerifyExpectedItemsExisted(items);
                Console.WriteLine("-*-[Info]: Verify actual items of checkboxgroup [" + control + "] have in expected items list.");
                Console.WriteLine("...........................................................................");
                bool flagT = checkboxgroup.VerifyActualItemsExisted(items);
                if (!flagT && flag && error == string.Empty)
                    error = "WARNING";
            }
            else
            {
                Console.WriteLine("-*-[Info]: Not found checkboxgroup [" + control + "] on form.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyRadioGroupItems(string control, string items, ref string error, [Optional] snoradiogroup rdg)
        {
            bool flag = true;
            snoradiogroup radiogroup = null;
            if (rdg == null)
                radiogroup = (snoradiogroup)Base.GControlByLabel(control);
            else
                radiogroup = rdg;
            try
            {
                string section = radiogroup.MySection;

                if (section != null && section != string.Empty)
                    Select_Tab(section);
            }
            catch { }
            Thread.Sleep(1000);
            flag = radiogroup.Existed;
            if (flag)
            {
                Console.WriteLine("-*-[Info]: Verify expected items of radiogroup [" + control + "] existed.");
                Console.WriteLine("...........................................................................");
                flag = radiogroup.VerifyExpectedItemsExisted(items);
                Console.WriteLine("-*-[Info]: Verify actual items of radiogroup [" + control + "] have in expected items list.");
                Console.WriteLine("...........................................................................");
                bool flagT = radiogroup.VerifyActualItemsExisted(items);
                if (!flagT && flag && error == string.Empty)
                    error = "WARNING";
            }
            else
            {
                Console.WriteLine("-*-[Info]: Not found radiogroup [" + control + "] on form.");
            }

            return flag;
        }
        #endregion End - Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine(_name + " page wait loading...");
            Base.PageWaitLoading();
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #region Select methods

        public bool Select_Change_Type(string type)
        {
            bool flag = true;

            string define = ".container-fluid.wizard-container>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Change types", by, null, snobase.MainFrame);
            flag = false;
            foreach (Auto.oelement e in list.MyList)
            {
                string temp = e.MyText;
                string[] arr = temp.Split(':');
                if (arr[0].Trim().ToLower().Contains(type.ToLower()))
                {
                    flag = e.Click();
                    break;
                }
            }
            if (!flag)
            {
                System.Console.WriteLine("-*-[Info]: Not found change type link (" + type + ")");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Select_Tab(string tabName, [Optional] bool javaClick, [Optional] bool noWait, [Optional] bool getNoDisplay)
        {
            bool flag = true;
            By by = By.CssSelector(allTabDefine);
            snoelements list = Base.SNGObjects("Tab list", by, null, snobase.MainFrame, getNoDisplay, noWait);
            bool flagF = false;

            string str = tabName;

            if (!tabName.Contains("@@"))
                str = "@@" + str;

            flagF = list.HaveItemsInlist(str, ";", noWait);
            Thread.Sleep(2000);
            if (flagF)
            {
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (!tabName.Contains("@@"))
                    {
                        if (ele.MyText.Trim().Contains("(") && ele.MyText.Trim().Contains(")"))
                        {
                            if (ele.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(tabName.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                            {
                                if (ele.MyElement.TagName.ToLower() != "h2")
                                    flag = ele.Click(javaClick);
                                break;
                            }
                        }
                        else
                        {
                            if (Base.StringCompare(ele.MyText, tabName))
                            {
                                if (ele.MyElement.TagName.ToLower() != "h2")
                                    flag = ele.Click(javaClick);
                                break;
                            }
                        }
                    }
                    else
                    {
                        string temp = tabName.Replace("@@", "");
                        if (ele.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(temp.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                        {
                            if (ele.MyElement.TagName.ToLower() != "h2")
                                flag = ele.Click(javaClick);
                            break;
                        }
                    }
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("***[Info]: Not found tab [" + tabName + "]");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Select_IntegrationTab(string tabName, [Optional] bool javaClick, [Optional] bool noWait)
        {
            bool flag = true;
            By by = By.CssSelector(allIntegrationTabDefine);
            snoelements list = Base.SNGObjects("Tab list", by, null, snobase.MainFrame);
            bool flagF = false;

            string str = tabName;

            if (!tabName.Contains("@@"))
                str = "@@" + str;

            flagF = list.HaveItemsInlist(str, ";", noWait);
            Thread.Sleep(2000);
            if (flagF)
            {
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (!tabName.Contains("@@"))
                    {
                        if (ele.MyText.Trim().Contains("(") && ele.MyText.Trim().Contains(")"))
                        {
                            if (ele.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(tabName.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                            {
                                if (ele.MyElement.TagName.ToLower() != "h2")
                                    flag = ele.Click(javaClick);
                                break;
                            }
                        }
                        else
                        {
                            if (Base.StringCompare(ele.MyText, tabName))
                            {
                                if (ele.MyElement.TagName.ToLower() != "h2")
                                    flag = ele.Click(javaClick);
                                break;
                            }
                        }
                    }
                    else
                    {
                        string temp = tabName.Replace("@@", "");
                        if (ele.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(temp.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
                        {
                            if (ele.MyElement.TagName.ToLower() != "h2")
                                flag = ele.Click(javaClick);
                            break;
                        }
                    }
                }
            }
            else
            {
                flag = false;
                Console.WriteLine("***[Info]: Not found tab [" + tabName + "]");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Select_ContextMenu_Items(string items, [Optional] bool javaClick)
        {
            string[] itemArray;
            bool flag = true;

            System.Console.WriteLine("***[Call function]: ContextMenuSelectItems");
            System.Console.WriteLine("-*- Select items:(" + items + ")");

            if (items.Contains(";"))
                itemArray = items.Split(';');
            else
                itemArray = new string[] { items };

            foreach (string item in itemArray)
            {
                flag = Select_ContextMenu_Item(item, javaClick);
                Thread.Sleep(1000);
                if (flag == false)
                {
                    break;
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Select_Ideas_Type(string type)
        {
            bool flag = true;

            string define = "div[class='container-fluid wizard-container']>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Ideas types", by, null, snobase.MainFrame);
            flag = false;
            foreach (Auto.oelement e in list.MyList)
            {
                string temp = e.MyText;
                if (temp.Trim().ToLower().Contains(type.ToLower()))
                {
                    flag = e.Click();
                    break;
                }
            }
            if (!flag)
            {
                System.Console.WriteLine("-*-[Info]: Not found ideas type link (" + type + ")");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Select_OtherAssets_Type(string type)
        {
            bool flag = true;

            string define = "div[class='container-fluid wizard-container']>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Ideas types", by, null, snobase.MainFrame);
            flag = false;
            foreach (Auto.oelement e in list.MyList)
            {
                string temp = e.MyText;
                if (temp.Trim().ToLower().Contains(type.ToLower()))
                {
                    flag = e.Click();
                    break;
                }
            }
            if (!flag)
            {
                System.Console.WriteLine("-*-[Info]: Not found ideas type link (" + type + ")");
            }

            return flag;
        }
        #endregion End - Select methods

        #region Add methods

        public bool Add_AttachmentFile(string filename)
        {
            bool flag = true;
            flag = Base.IsDllRegistered();

            if (flag)
            {
                snobutton bt = Button_ManageAttachments();
                if (bt.Existed)
                {
                    flag = bt.Click(true);
                    
                    snoelement dl = Dialog_ManageAttachment(false);
                    if (dl.Existed)
                    {
                        snoelement bf = BrowseFile_Attachment();
                        if (bf.Existed)
                        {
                            flag = bf.Click();
                            if (flag)
                            {
                                Thread.Sleep(5000);
                                flag = Base.WFileUploadSelect(filename);
                                if (flag)
                                {
                                    PleaseWait();
                                    bt = Button_CloseAttachmentDialog();
                                    if (bt.Existed)
                                    {
                                        flag = bt.Click(true);
                                        if (flag)
                                        {
                                            int count = 0;
                                            dl = Dialog_ManageAttachment(true);
                                            while (count < 5 && dl.Existed)
                                            {
                                                Thread.Sleep(1000);
                                            }
                                            if (dl.Existed)
                                            {
                                                flag = false;
                                                System.Console.WriteLine("-*-[Info]: Cannot close manage attachment dialog.");
                                            }
                                        }
                                    }
                                    else { flag = false; }
                                }
                            }
                        }
                        else { flag = false; }
                    }
                    else { System.Console.WriteLine("******** [Dialog_ManageAttachment] is NOT found"); flag = false; }
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_Worknotes(string value, [Optional] bool noClickButtonShowField, [Optional] snotextarea control)
        {
            bool flag = true;
            if (noClickButtonShowField == false)
            {
                snobutton bt = Button_Showfields();
                if (bt.Existed)
                {
                    if ((bt.MyElement.GetAttribute("title") != null && bt.MyElement.GetAttribute("title").ToLower() == "show all journal fields") || (bt.MyElement.GetAttribute("data-original-title") != null && bt.MyElement.GetAttribute("data-original-title").ToLower() == "show all journal fields"))
                    {
                        bt.Click();
                    }
                }
                Thread.Sleep(2000);
            }

            snotextarea textarea = null;
            if (control == null)
                textarea = Textarea_Worknotes();
            else
                textarea = control;

            flag = textarea.Existed;
            if (flag)
            {
                flag = textarea.SetText(value);
                if (!flag)
                    Console.WriteLine("-*-[Info]: Error when set text.");
            }
            else Console.WriteLine("-*-[Info]: Cannot get textarea work notes.");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_Comments(string value, [Optional] bool noClickButtonShowField, [Optional] snotextarea control)
        {
            bool flag = true;
            if (noClickButtonShowField == false)
            {
                snobutton bt = Button_Showfields();
                if (bt.Existed)
                {
                    if ((bt.MyElement.GetAttribute("title") != null && bt.MyElement.GetAttribute("title").ToLower() == "show all journal fields") || (bt.MyElement.GetAttribute("data-original-title") != null && bt.MyElement.GetAttribute("data-original-title").ToLower() == "show all journal fields"))
                    {
                        bt.Click();
                    }
                }
                Thread.Sleep(2000);
            }

            snotextarea textarea = null;
            if (control == null)
                textarea = Textarea_Comments();
            else
                textarea = control;

            flag = textarea.Existed;
            if (flag)
            {
                flag = textarea.SetText(value);
                if (!flag)
                    Console.WriteLine("-*-[Info]: Error when set text.");
            }
            else Console.WriteLine("-*-[Info]: Cannot get textarea work notes.");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_AdditionComments(string value)
        {
            bool flag = true;
            snobutton bt = Button_Showfields();
            if (bt.Existed)
            {
                if ((bt.MyElement.GetAttribute("title") != null && bt.MyElement.GetAttribute("title").ToLower() == "show all journal fields") || (bt.MyElement.GetAttribute("data-original-title") != null && bt.MyElement.GetAttribute("data-original-title").ToLower() == "show all journal fields"))
                {
                    bt.Click();
                }
            }
            Thread.Sleep(2000);
            snotextarea textarea = Textarea_AdditionalCommentsCustomerVisible();
            flag = textarea.Existed;
            if (flag)
            {
                flag = textarea.SetText(value);
                if (!flag)
                    Console.WriteLine("-*-[Info]: Error when set text.");
            }
            else Console.WriteLine("-*-[Info]: Cannot get textarea additional comments.");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_AdditionComments_SIR(string value)
        {
            bool flag = true;
            snobutton bt = Button_Post();
            snotextarea textarea = Textarea_AdditionalComments();
            flag = textarea.Existed;
            if (flag)
            {
                flag = textarea.SetText(value);
                if (!flag)
                    Console.WriteLine("-*-[Info]: Error when set text.");
            }
            else Console.WriteLine("-*-[Info]: Cannot get textarea additional comments.");
            flag = bt.Click();
            if (flag)
            {

            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_List_Value(snolist list, string users)
        {
            bool flag = true;
            flag = list.Existed;
            if (flag)
            {
                snobutton button = list.Button_Unlock();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        snotextbox textbox = list.Textbox_Fill_Data();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            string[] arr = null;
                            string verify = users;
                            if (users.Contains(";"))
                            {
                                arr = users.Split(';');
                                verify = users.Replace(";", ", ");
                            }
                            else
                                arr = new string[] { users };

                            foreach (string user in arr)
                            {
                                bool flagTemp = true;
                                flagTemp = textbox.SetText(user, true);
                                if (!flagTemp && flag)
                                    flag = false;
                                Thread.Sleep(1000);
                            }

                            if (!flag)
                            {
                                Console.WriteLine("-*-[Info]: Error when add user to watch list.");
                            }
                            else
                            {
                                Thread.Sleep(2000);
                                button = list.Button_Lock();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (flag)
                                    {
                                        WaitLoading();
                                        snoelement showlist = list.Showlist_Added();
                                        flag = showlist.Existed;
                                        if (flag)
                                        {
                                            string runtime = showlist.MyText;
                                            if (!runtime.Trim().ToLower().Equals(verify.Trim().ToLower()))
                                            {
                                                flag = false;
                                                Console.WriteLine("-*-[Info]: Runtime:" + runtime + " - Expected:" + verify);
                                            }
                                            else Console.WriteLine("-*-[Info]: Added watch list as expected:" + runtime);
                                        }
                                        else Console.WriteLine("-*-[Info]: Cannot get show list added.");
                                    }
                                    else Console.WriteLine("-*-[Info]: Error when click on lock button.");
                                }
                                else Console.WriteLine("-*-[Info]: Cannot get button lock.");
                            }
                        }
                        else Console.WriteLine("-*-[Info]: Cannot get textbox email address.");
                    }
                    else Console.WriteLine("-*-[Info]: Error when click on button unlock.");
                }
                else Console.WriteLine("-*-[Info]: Cannot get button unlock.");
            }
            else Console.WriteLine("-*-[Info]: Cannot get control watch list.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Add_Related_Members(string tabName, string members)
        {
            bool flag = true;
            flag = Select_Tab(tabName);
            if (flag)
            {
                Thread.Sleep(2000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    snobutton bt = tabL.Button_Edit();
                    bt.MoveToElement();
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click();
                        if (flag)
                        {
                            SNow.Member member = new Member(Base);
                            member.WaitLoading();
                            flag = member.Add_Members(members);
                            if (!flag)
                                Console.WriteLine("-*-[Info]: Error when add members.");
                            else
                                WaitLoading();
                        }
                        else Console.WriteLine("-*-[Info]: Error when click on button Edit.");
                    }
                    else Console.WriteLine("-*-[Info]: Cannot get button Edit.");
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab:" + tabName);
            return flag;
        }

        #endregion End - Add methods

        #region Delete and Rename methods

        public bool Rename_AttachmentFile(string fileName)
        {
            bool flag = true;
            Base.ActiveWindow();
            snobutton bt = Button_ManageAttachments();
            if (bt.Existed)
            {
                bt.Click(true);

                snoelement dl = Dialog_ManageAttachment(false);
                if (dl.Existed)
                {
                    string define = "div[id='attachment_dialog_list'] tr:not([id])";
                    By by = By.CssSelector(define);
                    snoelements list = Base.SNGObjects("Attachment list", by, dl, snobase.MainFrame);
                    if (list.MyList.Count > 0)
                    {
                        foreach (Auto.oelement ele in list.MyList)
                        {
                            snoelement parent = new snoelement("parent", Base.Driver, ele.MyElement);
                            snoelement file = (snoelement)Base.SNGObject("File item", "element", By.CssSelector("a[data-id]"), parent, snobase.MainFrame);
                            flag = file.Existed;
                            if (flag)
                            {

                                if (file.MyText.Trim().ToLower().Equals(fileName.Trim().ToLower()))
                                {
                                    snoelement rename = (snoelement)Base.SNGObject("rename", "element", By.XPath(".//td/a[contains(text(), 'rename')]"), parent, snobase.MainFrame);
                                    flag = rename.Existed;
                                    if (flag)
                                    {
                                        rename.Click(true);
                                        Thread.Sleep(1000);
                                        string[] arr = fileName.Split('.');
                                        Actions a = new Actions(Base.Driver);
                                        a.MoveToElement(file.MyElement);
                                        Thread.Sleep(1000);
                                        a.SendKeys(Keys.Control + "a" + Keys.Control);
                                        Thread.Sleep(1000);
                                        a.SendKeys(Keys.Delete);
                                        Thread.Sleep(1000);
                                        a.SendKeys(arr[0] + "_rename." + arr[1]);
                                        Thread.Sleep(1000);
                                        a.SendKeys(Keys.Enter);
                                        Thread.Sleep(1000);
                                        a.Build().Perform();
                                        Thread.Sleep(2000);
                                        bt = Button_CloseAttachmentDialog();
                                        if (bt.Existed)
                                        {
                                            flag = bt.Click(true);
                                            if (flag)
                                            {
                                                int count = 0;
                                                dl = Dialog_ManageAttachment(true);
                                                while (count < 5 && dl.Existed)
                                                {
                                                    Thread.Sleep(1000);
                                                }
                                                if (dl.Existed)
                                                {
                                                    flag = false;
                                                    System.Console.WriteLine("-*-[Info]: Cannot close manage attachment dialog.");
                                                }
                                            }
                                        }
                                        else { flag = false; }
                                    }
                                    break;
                                }
                            }
                            else Console.WriteLine("-*-[Info]: Cannot get file name.");
                        }
                    }
                }
                else { flag = false; }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Delete_AttachmentFile(string filename)
        {
            bool flag = true;
            snobutton bt = Button_ManageAttachments();
            if (bt.Existed)
            {
                bt.Click(true);

                snoelement dl = Dialog_ManageAttachment(false);
                if (dl.Existed)
                {
                    string define = "div[id='attachment_dialog_list']";
                    By by = By.CssSelector(define);
                    snoelements list = Base.SNGObjects("Attachment list", by, dl, snobase.MainFrame);
                    if (list.MyList.Count > 0)
                    {
                        foreach (Auto.oelement ele in list.MyList)
                        {
                            snoelement parent = new snoelement("parent", Base.Driver, ele.MyElement);
                            snoelement file = (snoelement)Base.SNGObject("File item", "element", By.CssSelector("div[id='attachment_dialog_list'] a[data-id]"), parent, snobase.MainFrame);
                            flag = file.Existed;
                            if (flag)
                            {
                                if (file.MyText.Trim().ToLower().Equals(filename.Trim().ToLower()))
                                {
                                    snocheckbox checkbox = (snocheckbox)Base.SNGObject("Check box", "checkbox", By.CssSelector("div[id='attachment_dialog_list'] input[type='checkbox']"), parent, snobase.MainFrame);
                                    flag = checkbox.Existed;
                                    if (flag)
                                    {
                                        flag = checkbox.Click(true);
                                        if (flag)
                                        {
                                            snobutton remove = Button_RemoveAttachmentFile();
                                            flag = remove.Existed;
                                            if (flag)
                                            {
                                                flag = remove.Click(true);
                                                if (flag)
                                                {
                                                    PleaseWait();

                                                    bt = Button_CloseAttachmentDialog();
                                                    if (bt.Existed)
                                                    {
                                                        flag = bt.Click(true);
                                                        if (flag)
                                                        {
                                                            int count = 0;
                                                            dl = Dialog_ManageAttachment(true);
                                                            while (count < 5 && dl.Existed)
                                                            {
                                                                Thread.Sleep(1000);
                                                            }
                                                            if (dl.Existed)
                                                            {
                                                                flag = false;
                                                                System.Console.WriteLine("-*-[Info]: Cannot close manage attachment dialog.");
                                                            }
                                                        }
                                                    }
                                                    else { flag = false; }
                                                }
                                                else Console.WriteLine("-*-[Info]: Error when click on remove button.");
                                            }
                                            else Console.WriteLine("-*-[Info]: Cannot get button remove.");
                                        }
                                        else Console.WriteLine("-*-[Info]: Error when click on checkbox.");
                                    }
                                    else Console.WriteLine("-*-[Info]: Cannot get check box.");
                                    break;
                                }
                            }
                            else Console.WriteLine("-*-[Info]: Cannot get file name.");
                        }
                    }
                }
                else { flag = false; }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Delete_Related_Members(string tabName, string members)
        {
            bool flag = true;
            flag = Select_Tab(tabName);
            if (flag)
            {
                Thread.Sleep(2000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    snobutton bt = tabL.Button_Edit();
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click();
                        if (flag)
                        {
                            SNow.Member member = new Member(Base);
                            member.WaitLoading();
                            flag = member.Delete_Members(members);
                            if (!flag)
                                Console.WriteLine("-*-[Info]: Error when delete members.");
                            else
                                WaitLoading();
                        }
                        else Console.WriteLine("-*-[Info]: Error when click on button Edit.");
                    }
                    else Console.WriteLine("-*-[Info]: Cannot get button Edit.");
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab:" + tabName);
            return flag;
        }

        #endregion End - Delete and Rename methods

        #region Save and Update methods

        public bool Save([Optional] bool javaClick, [Optional] bool noVerify)
        {
            System.Console.WriteLine("***[Call function]: Save " + _name);
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Item("Save", javaClick);
                        if (flag)
                        {
                            Thread.Sleep(2000);
                            //-- Verify if need

                            if (!noVerify)
                            {
                                try
                                {
                                    IAlert alert = Base.Driver.SwitchTo().Alert();
                                    flag = false;
                                }
                                catch
                                { }

                                if (flag)
                                {
                                    snoelements errorlist = Error_Message_List();
                                    if (errorlist.MyList.Count > 0)
                                    {
                                        flag = false;
                                        System.Console.WriteLine("-*-[Info]: Existed error message.");
                                    }
                                }
                                else System.Console.WriteLine("-*-[Info]: Existed error alert.");
                            }
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot save " + _name); }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Update([Optional] bool javaClick)
        {
            System.Console.WriteLine("***[Call function]: Update " + _name);
            bool flag = true;
            snobutton bt = Button_Update();
            if (bt.Existed)
            {
                flag = bt.Click(javaClick);
                if (flag)
                {
                    try
                    {
                        IAlert alert = Base.Driver.SwitchTo().Alert();
                        flag = false;
                    }
                    catch
                    { }

                    if (flag)
                    {
                        snoelements errorlist = Error_Message_List();
                        if (errorlist.MyList.Count > 0)
                        {
                            flag = false;
                            System.Console.WriteLine("-*-[Info]: Existed error message.");
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Existed error alert.");
                }
            }
            else { flag = false; }
            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot update " + _name); }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SaveAndExit([Optional] bool javaClick)
        {
            System.Console.WriteLine("***[Call function]: Save and Exit " + _name);
            bool flag = true;
            snobutton bt = Button_SaveAndExit();
            if (bt.Existed)
            {
                flag = bt.Click(javaClick);
                if (flag)
                {
                    try
                    {
                        //IAlert alert = Base.Driver.SwitchTo().Alert();
                        //flag = false;
                        bt = new snobutton("accept", Base.Driver, By.CssSelector("button[class='btn btn-primary editor-dialog-button']"), null, snobase.MainFrame);
                        flag = bt.Existed;
                        if (flag)
                        {
                            bt.Click();
                        }
                        else Console.WriteLine("Error: Cannot click Ok button");
                    }
                    catch
                    { }

                    if (flag)
                    {
                        snoelements errorlist = Error_Message_List();
                        if (errorlist.MyList.Count > 0)
                        {
                            flag = false;
                            System.Console.WriteLine("-*-[Info]: Existed error message.");
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Existed error alert.");
                }
            }
            else { flag = false; }
            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot save and exit " + _name); }
            return flag;
        }


        #endregion End - Save and Update methods

        #region Related table

        public bool Verify_RelatedTable_Row(string tabName, string findConditions, [Optional] bool expectedNotFound, [Optional] string tabDefine)
        {
            bool flag = false;
            flag = Select_Tab(tabName);

            if (flag)
            {
                flag = false;
                Thread.Sleep(2000);
                snoelement tab = GRelatedTabByName(tabName, tabDefine);

                ItilList tabL = new ItilList(Base, tabName, tab);

                flag = tabL.VerifyRow(findConditions, expectedNotFound);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Search_Verify_RelatedTable_Row(string tabName, string searchBy, string searchValue, string findConditions, [Optional] bool expectedNotFound)
        {
            bool flag = false;

            flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(2000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    flag = tabL.SearchAndVerify(searchBy, searchValue, findConditions, expectedNotFound);
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Search_Open_RelatedTable_Row(string tabName, string searchBy, string searchValue, string findConditions, string columnClick)
        {
            bool flag = false;

            flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(5000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    flag = tabL.SearchAndOpen(searchBy, searchValue, findConditions, columnClick);
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;
        }
        //------------------------------------------------------------------------------------------------------
        public bool RelatedTableOpenRecord(string tabName, string findConditions, string columnClick)
        {
            bool flag = false;

            flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(5000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    flag = tabL.RelatedTableOpen(findConditions, columnClick);
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;

        }
        //------------------------------------------------------------------------------------------------------
        public bool RelatedTable_Open_Information_Record(string tabName, string findConditions)
        {
            bool flag = false;

            flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(5000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    snotable tb = tabL.Table_List();
                    flag = tb.Existed;
                    if (flag)
                    {
                        flag = tb.FindRowAndClickInformation(findConditions);
                        if (flag)
                        {
                            snobutton button = Button_OpenRecord();
                            flag = button.Existed;
                            if (flag)
                                flag = button.Click();
                        }
                        else
                        {
                            Console.WriteLine("-*-[Info]: Error when click on icon information.");
                        }
                    }
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;

        }
        //------------------------------------------------------------------------------------------------------
        public string RelatedTable_Get_Cell_Text(string tabName, string findConditions, string columnName, [Optional] string childCell)
        {
            string result = string.Empty;

            bool flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(5000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    result = tabL.RelatedTableGetCell(findConditions, columnName, false, null, null, null, null, childCell);
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return result;
        }
        //------------------------------------------------------------------------------------------------------
        public bool RelatedTable_Edit_Cell(string tabName, string findConditions, string columnClick, string value, [Optional] string rIndex)
        {
            
            bool flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(5000);
                string tabDefine = null;
                if (rIndex != null)
                    tabDefine = "span[class$='tab_section']:not([aria-hidden='true'])";
                snoelement tab = GRelatedTabByName(tabName, tabDefine);
                if (tab != null)
                {
                    SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                    flag = tabL.CellEdit(findConditions, columnClick, value, rIndex);
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;
        }
        //------------------------------------------------------------------------------------------------------
        public bool RelatedTab_Click_Button(string tabName, string buttonName)
        {
            bool flag = true;

            flag = Select_Tab(tabName);
            if (flag)
            {
                flag = false;
                Thread.Sleep(5000);
                snoelement tab = GRelatedTabByName(tabName);
                if (tab != null)
                {
                    snobutton button = Base.GButtonByText(buttonName, tab);
                    flag = button.Existed;
                    if (flag)
                    {
                        button.Click();
                    }
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;
        }

        public bool RelatedTab_Verify_NoRecordsToDisplay(string tabName, [Optional] string tabDefine)
        {
            bool flag = false;

            flag = Select_Tab(tabName);
            if (flag)
            {
                Thread.Sleep(2000);
                snoelement tab = GRelatedTabByName(tabName, tabDefine);
                if (tab != null)
                {
                    snoelement ele = null;

                    string define = "tbody[class='list2_body']>tr[class='list2_no_records']";

                    ele = (snoelement)Base.SNGObject("Loading", "element", By.CssSelector(define), tab, snobase.MainFrame, true);
                    if (ele.Existed)
                        flag = true;
                    else
                        flag = false;
                }
                else Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName);
            }
            else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);

            return flag;
        }

        #endregion End - Related table

        #region Verify methods

        public bool Verify_CurrentProcess(string process)
        {
            bool flag = false;
            string define = "ol[class^='process']>li>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Process list", by, null, snobase.MainFrame);

            if (list.MyList.Count > 0)
            {
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.Trim().ToLower() == process.ToLower())
                    {
                        IWebElement e = ele.MyElement.FindElement(By.XPath(".."));
                        if (e.GetAttribute("data-state").Trim().ToLower() == "current")
                        {
                            flag = true;
                            Console.WriteLine("-*-[Info]: " + process + " is current process.");
                            break;
                        }
                    }
                }
            }
            else { Console.WriteLine("-*-[Info]: Not found any process item."); }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Activity(string value)
        {
            bool flag = false;
            string define = "li[class^='h-card h-card_md h-card']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Activity list", by, null, snobase.MainFrame);
            int count = 0;
            foreach (Auto.oelement e in list.MyList)
            {
                snoelement parent = new snoelement("parent", Base.Driver, e.MyElement);
                by = By.CssSelector("span[class*='createdby']");
                snoelement ele = (snoelement)Base.SNGObject("created by", "element", by, parent, snobase.MainFrame);
                string createdby = ele.MyElement.GetAttribute("textContent").ToLower();

                string df = "div[class$='records'], div[class$='summary_spacing'], div[class*='attachment']";
                by = By.CssSelector(df);

                ele = (snoelement)Base.SNGObject("records", "element", by, parent, snobase.MainFrame);
                string records = ele.MyElement.Text.Trim().Replace("\r\n", "|").ToLower();
                string temp = createdby + "|" + records;

                System.Console.WriteLine("-*-Run time activity value[" + count + "]:(" + temp + ")");

                if (value.Contains("@@"))
                {
                    string str = value;
                    str = str.Replace("@@", "");
                    if (temp.Replace(" ", "").Contains(str.Replace(" ", "").ToLower()))
                    {
                        flag = true;
                        System.Console.WriteLine("-*-[Passed]: Found activity value (" + value + ")");
                        break;
                    }
                    else
                        count++;
                }
                else
                {
                    if (temp.Replace(" ", "").Equals(value.Replace(" ","").ToLower()))
                    {
                        flag = true;
                        System.Console.WriteLine("-*-[Passed]: Found activity value (" + value + ")");
                        break;
                    }
                    else
                        count++;
                }

            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Attachment_File(string fileNames, [Optional] bool noWait)
        {
            bool flag = true;
            if (fileNames.Contains(";"))
            {
                string[] arr = fileNames.Split(';');
                foreach (string item in arr)
                {
                    bool flagF = Verify_Attachment(item, noWait);
                    if (!flagF && flag)
                        flag = false;
                }
            }
            else
            {
                flag = Verify_Attachment(fileNames, noWait);
            }
            return flag;
        }

        //-------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_ExpectedErrorMessages_Existed(string items)
        {
            bool flag = true;
            string[] arr = null;
            if (items.Contains(";"))
            {
                arr = items.Split(';');
            }
            else { arr = new string[] { items }; }

            snoelements errorList = Error_Message_List();

            if (arr.Length != errorList.MyList.Count)
            {
                System.Console.WriteLine("***[WARNING]: NOT match items count. Expected:(" + arr.Length + "). Runtime:(" + errorList.MyList.Count + ")");
            }
            else System.Console.WriteLine("-*-[Infor]: Match items count. Expected:(" + arr.Length + "). Runtime:(" + errorList.MyList.Count + ")");

            bool flagF = true;
            foreach (string item in arr)
            {
                flagF = VerifyExpectedItem(item, errorList);
                if (!flagF && flag) { flag = false; }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Visible_Controls(string expType, string expControls, ref string error, [Optional] Dictionary<string, snoelement> onFormControls)
        {
            bool flag = true;

            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            if (onFormControls == null)
            {
                switch (expType.Trim().ToLower())
                {
                    case "mandatory":
                        dic = GetManatoryControlOnForm();
                        break;
                    case "mandatorynotvalue":
                        dic = GetManatoryControlOnForm(true);
                        break;
                    default:
                        dic = GetControlOnForm(expType);
                        break;
                }
            }
            else
                dic = onFormControls;

            flag = VerifyVisibleControls(expType, expControls, ref error, dic);

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Readonly_Controls(string expType, string expControls, ref string error, [Optional] Dictionary<string, snoelement> onFormControls)
        {
            bool flag = true;

            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            if (onFormControls == null)
            {
                switch (expType.Trim().ToLower())
                {
                    case "mandatory":
                        dic = GetManatoryControlOnForm();
                        break;
                    case "mandatorynotvalue":
                        dic = GetManatoryControlOnForm(true);
                        break;
                    default:
                        dic = GetControlOnForm(expType);
                        break;
                }
            }
            else
                dic = onFormControls;

            flag = VerifyReadonlyControls(expType, expControls, ref error, dic);

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Default_Value_Controls(string expType, string expControls, ref string error, [Optional] Dictionary<string, snoelement> onFormControls)
        {
            bool flag = true;

            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            if (onFormControls == null)
            {
                switch (expType.Trim().ToLower())
                {
                    case "mandatory":
                        dic = GetManatoryControlOnForm();
                        break;
                    case "mandatorynotvalue":
                        dic = GetManatoryControlOnForm(true);
                        break;
                    default:
                        dic = GetControlOnForm(expType);
                        break;
                }
            }
            else
                dic = onFormControls;

            flag = VerifyDefaultValueControls(expType, expControls, ref error, dic);

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Button_Controls(bool expNotFound, string expControls, ref string error, [Optional] Dictionary<string, snoelement> onFormControls)
        {
            bool flag = true;

            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            if (onFormControls == null)
            {
                dic = GetButtonControlOnForm();
            }
            else
                dic = onFormControls;

            flag = VerifyButtonControls(expNotFound, expControls, ref error, dic);

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Combobox_Items(string expControls, ref string error)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            foreach (string control in expArray)
            {
                string[] array = control.Split('=');
                string ctr = array[0].Trim();
                string items = array[1].Trim();
                Console.WriteLine("***[Info]: Verify items of combobox " + ctr + "***");
                Console.WriteLine("...........................................................................");
                bool flagF = VerifyComboboxItems(ctr, items, ref error);
                if (!flagF && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Lookup_Columns_Search_Table(string expected, ref string error)
        {
            bool flag = true;

            Base.SwitchToPage(0);
            string[] array = null;
            if (expected.Contains("|"))
                array = expected.Split('|');
            else
                array = new string[] { expected };

            foreach (string a in array)
            {
                bool flagP = true;
                string[] arr = a.Split('=');
                string name = arr[0];
                string columns = arr[1];

                snolookup lookup = (snolookup)Base.GControlByLabel(name);
                
                try 
                {
                    string section = lookup.MySection;
                    if (section != null && section != string.Empty)
                    {
                        Select_Tab(section);
                        Thread.Sleep(2000);
                    }
                }
                catch { }
                

                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.ClickOnFindButton();
                    if (flag)
                    {
                        Thread.Sleep(4000);
                        ItilList list = new ItilList(Base, "Search");
                        Base.SwitchToPage(1);
                        list.WaitLoading(false, true);
                        flagP = list.VerifyColumnHeader(columns, ref error, true);
                        if (!flagP && flag)
                            flag = false;
                        Base.Driver.Close();
                        Thread.Sleep(2000);
                        Base.SwitchToPage(0);
                    }
                    
                }
            }
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Combobox_Parent_Child_Items(string expControls, ref string error)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            foreach (string expControl in expArray)
            {
                bool flagF = VerifyComboboxParentChildItem(expControl, ref error);
                if (!flagF && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Controls_Dependent_Additional(string expControls, ref string error)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            foreach (string expControl in expArray)
            {
                bool flagF = VerifyControlsDependentAdditional(expControl, ref error);
                if (!flagF && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_Tabs_Visilbe(string expControls)
        {
            bool flag = true;
            By by = By.CssSelector(allTabDefine);
            snoelements list = Base.SNGObjects("Tab list", by, null, snobase.MainFrame);
            flag = list.HaveItemsInlist(expControls, "|");
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_RelatedTables_Default_Columns(string expected, ref string error, [Optional] bool expectedNotFound)
        {
            bool flag = true;

            string[] array = null;
            if (expected.Contains("|"))
                array = expected.Split('|');
            else
                array = new string[] { expected };

            foreach (string exp in array)
            {
                string[] arr = exp.Split('=');
                string tabName = arr[0].Trim();
                string cols = arr[1].Trim();
                bool flagV = true;
                if (cols == null || cols == string.Empty)
                {
                    Console.WriteLine("-*-[Info]: No need to verify columns of related table [" + tabName + "].");
                }
                else
                {
                    Console.WriteLine("-*-[Info]: Verify columns of related table [" + tabName + "].");
                    Console.WriteLine("........................................................................");
                    flagV = Select_Tab(tabName);
                    if (flagV)
                    {
                        Thread.Sleep(2000);
                        snoelement tab = GRelatedTabByName(tabName);
                        if (tab != null)
                        {
                            SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                            flagV = tabL.ResetColumnToDefaultValue();
                            Thread.Sleep(2000);
                            if (flagV)
                            {
                                Select_Tab(tabName);
                                Thread.Sleep(2000);
                                tab = GRelatedTabByName(tabName);
                                if (tab != null)
                                {
                                    tabL = new ItilList(Base, tabName, tab);
                                    flagV = tabL.VerifyColumnHeader(cols, ref error);
                                }
                                else flagV = false;
                            }
                        }
                        else { flagV = false; Console.WriteLine("-*-[Info]: Cannot get related tab: " + tabName); }
                    }
                    else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);
                }

                if (!flagV && flag)
                    flag = false;

            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_RelatedTables_Combobox_Goto(string expected, ref string error, [Optional] bool expectedNotFound)
        {
            bool flag = true;

            string[] array = null;
            if (expected.Contains("|"))
                array = expected.Split('|');
            else
                array = new string[] { expected };

            foreach (string exp in array)
            {
                string[] arr = exp.Split('=');
                string tabName = arr[0].Trim();
                string cols = arr[1].Trim();
                bool flagV = true;
                if (cols == null || cols == string.Empty)
                {
                    Console.WriteLine("-*-[Info]: No need to verify combobox goto item of related table [" + tabName + "].");
                }
                else
                {
                    Console.WriteLine("-*-[Info]: Verify combobox goto item of related table [" + tabName + "].");
                    Console.WriteLine("........................................................................");
                    flagV = Select_Tab(tabName);
                    if (flagV)
                    {
                        Thread.Sleep(2000);
                        snoelement tab = GRelatedTabByName(tabName);
                        SNow.ItilList tabL = new ItilList(Base, tabName, tab);
                        flagV = tabL.VerifyComboboxGoto(cols, ref error);
                    }
                    else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);
                }
                if (!flagV && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_RelatedTables_Button_Controls(bool expNotFound, string expControls, ref string error, [Optional] Dictionary<string, snoelement> onFormControls)
        {
            bool flag = true;

            string[] array = null;
            if (expControls.Contains("|"))
                array = expControls.Split('|');
            else
                array = new string[] { expControls };

            foreach (string exp in array)
            {
                string[] arr = exp.Split('=');
                string tabName = arr[0].Trim();
                string buttons = arr[1].Trim();
                bool flagV = true;
                if (buttons == null || buttons == string.Empty || buttons.Trim().ToLower() == "no")
                {
                    Console.WriteLine("-*-[Info]: No need to verify buttons of related table [" + tabName + "].");
                }
                else
                {
                    Console.WriteLine("-*-[Info]: Verify buttons of related table [" + tabName + "].");
                    Console.WriteLine("........................................................................");
                    flagV = Select_Tab(tabName);
                    if (flagV)
                    {
                        Thread.Sleep(2000);
                        snoelement tab = GRelatedTabByName(tabName);
                        Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
                        if (onFormControls == null)
                        {
                            dic = GetRelatedTableButtonControlOnForm(tab);
                        }
                        else
                            dic = onFormControls;

                        flagV = VerifyButtonControls(expNotFound, buttons, ref error, dic, ";");
                    }
                    else Console.WriteLine("-*-[Info]: Error when select tab: " + tabName);
                }
                if (!flagV && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_CheckboxGroup_Items(string expControls, ref string error)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            foreach (string control in expArray)
            {
                string[] array = control.Split('=');
                string ctr = array[0].Trim();
                string items = array[1].Trim();
                Console.WriteLine("***[Info]: Verify items of checkbox group " + ctr + "***");
                Console.WriteLine("...........................................................................");
                bool flagF = VerifyCheckboxGroupItems(ctr, items, ref error);
                if (!flagF && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Verify_RadioGroup_Items(string expControls, ref string error)
        {
            bool flag = true;

            string[] expArray = null;
            if (expControls.Contains("|"))
                expArray = expControls.Split('|');
            else
                expArray = new string[] { expControls };

            foreach (string control in expArray)
            {
                string[] array = control.Split('=');
                string ctr = array[0].Trim();
                string items = array[1].Trim();
                Console.WriteLine("***[Info]: Verify items of radio group " + ctr + "***");
                Console.WriteLine("...........................................................................");
                bool flagF = VerifyRadioGroupItems(ctr, items, ref error);
                if (!flagF && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyConfirmDialog(string expectedValue, [Optional] string agree)
        {
            bool flag = true;
            snoelement dialog = Dialog_Confirmation(false);
            flag = dialog.Existed;
            if (flag)
            {
                string define = "rendered_body div:not([style*='left']):not([style*='center']):not([align='right'])";
                snoelements eles = Base.SNGObjects("Confirm text", By.CssSelector(define), dialog, snobase.MainFrame);
                string[] arr = null;
                if (expectedValue.Contains(";"))
                    arr = expectedValue.Split(';');
                else
                    arr = new string[] { expectedValue };


                foreach (string exp in arr)
                {
                    bool flagF = false;
                    foreach (Auto.oelement ele in eles.MyList)
                    {
                        System.Console.WriteLine("-*-Runtime:(" + ele.MyText + ")");
                        if (ele.MyText.Trim().ToLower().Equals(exp.Trim().ToLower()))
                        {
                            System.Console.WriteLine("-*-[Passed]: Found (" + exp + ")");
                            flagF = true;
                            break;
                        }
                    }

                    if (!flagF && flag)
                        flag = false;
                }


                if (flag)
                {
                    if (agree != null && agree != string.Empty)
                    {
                        string str = string.Empty;
                        if (agree.ToLower() == "yes")
                        {
                            str = "button[id='ok_button']";
                        }
                        else
                        {
                            str = "button[id='cancel_button']";
                        }
                        snobutton bt = (snobutton)Base.SNGObject("Button", "button", By.CssSelector(str), dialog, snobase.MainFrame);
                        flag = bt.Existed;
                        if (flag)
                        {
                            flag = bt.Click();
                            if (!flag)
                                System.Console.WriteLine("-*-[ERROR]: Error when click on button.");
                        }
                        else System.Console.WriteLine("-*-[ERROR]: Cannot get button.");
                    }
                }
                else System.Console.WriteLine("-*-[ERROR]: Invalid message in dialog.");
            }
            else
                System.Console.WriteLine("-*-[ERROR]: Dialog confirmation is not existed.");

            return flag;
        }
        #endregion End - Verify methods

        #region Get controls

        public Dictionary<string, snoelement> GetControlOnForm([Optional] string type, [Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dic = Base.GAllControlWithLabelOnForm(type, parent, onPortal);
            return dic;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GetManatoryControlOnForm([Optional] bool notValue, [Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dic = Base.GAllMandatoryControlWithLabelOnForm(notValue, parent, onPortal);
            return dic;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GetButtonControlOnForm([Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dic = Base.GAllButtonControlOnForm(parent, onPortal);
            return dic;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GetRelatedTableButtonControlOnForm([Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dic = Base.GAllRelatedTableButtonControlOnForm(parent, onPortal);
            return dic;
        }
        #endregion End - Get controls

        #region Input value for control

        public bool Input_Value_For_Controls(string expected)
        {
            bool flag = true;

            string[] expArray = null;
            if (expected.Contains("|"))
                expArray = expected.Split('|');
            else
                expArray = new string[] { expected };

            foreach (string exp in expArray)
            {
                bool flagV = InputValueForControl(exp);
                if (!flagV && flag)
                    flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Input_Mandatory_Fields(string value)
        {
            bool flag = true;

            Dictionary<string, snoelement> dic = Base.GAllMandatoryControlWithLabelOnForm(true);

            while (dic.Count > 0)
            {
                foreach (var control in dic)
                {
                    string label = control.Key;
                    snoelement e = control.Value;
                    string temp = string.Empty;
                    string type = e.MyType;
                    switch (type.Trim().ToLower())
                    {
                        case "textarea":
                        case "textbox":
                            temp = label + "=" + value;
                            break;
                        default:
                            temp = label + "=auto";
                            break;
                    }
                    bool flagV = InputValueForControl(temp);
                    if (!flagV && flag)
                        flag = false;
                }

                dic = Base.GAllMandatoryControlWithLabelOnForm(true);
            }

            return flag;
        }

        #endregion End - Input value for control

        #region Other

        public bool GlobalSearchItem(string search, bool enter)
        {
            bool flag = true;

            snotextbox texbox = Textbox_Search();
            if (texbox.Existed)
            {
                flag = texbox.SetText(search, enter, true);
                if (!flag)
                {
                    Console.WriteLine("-*-[FAILED]: Cannot input and search item record.");
                }
            }
            else
            {
                snobutton button = Button_Search();
                flag = button.Existed;
                if (flag)
                {
                    button.MoveToElement();
                    flag = button.Click(true);
                    
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        flag = texbox.SetText(search, enter, true);
                        if (!flag) { Console.WriteLine("-*-[FAILED]: Cannot input and search item record."); }
                    }
                    else
                    {
                        Console.WriteLine("-*-[FAILED]: Cannot click on Search button.");
                    }
                }
                else
                {
                    Console.WriteLine("-*-[FAILED]: Cannot get Search button.");
                }
            }

            return flag;
        }

        public bool CreateWorkOrder([Optional] bool javaClick, [Optional] bool noVerify)
        {
            System.Console.WriteLine("***[Call function]: Create Work Order " + _name);
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Item("Create Work Order", javaClick);
                        if (flag)
                        {
                            WaitLoading();

                            //-- Verify if need

                            if (!noVerify)
                            {
                                try
                                {
                                    IAlert alert = Base.Driver.SwitchTo().Alert();
                                    flag = false;
                                }
                                catch
                                { }

                                if (flag)
                                {
                                    snoelements errorlist = Error_Message_List();
                                    if (errorlist.MyList.Count > 0)
                                    {
                                        flag = false;
                                        System.Console.WriteLine("-*-[Info]: Existed error message.");
                                    }
                                }
                                else System.Console.WriteLine("-*-[Info]: Existed error alert.");
                            }
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot save " + _name); }

            return flag;
        }

        public bool VerifySLAs(string slaList)
        {
            bool flag = true;
            if (slaList.ToLower() != "no")
            {
                string[] taskSLA = null;
                if (slaList.Contains(";") == true)
                {
                    taskSLA = slaList.Split(';');
                }
                else
                {
                    taskSLA = new string[] { slaList};
                }

                foreach (string SLA in taskSLA)
                {
                    bool flagF = Verify_RelatedTable_Row("Task SLAs", SLA );
                    if (flagF)
                    {
                        System.Console.WriteLine("Found SLA:[" + SLA + "]");
                        
                    }
                    else { System.Console.WriteLine("NOT FOUND SLA:[" + SLA + "]"); }

                    if (!flagF && flag)
                        flag = false;
                }
            }
            
            return flag;

        }

        public snoelement GView(string key, string name)
        {
            snoelement ele = null;

            string define = "a[id*='" + key.ToLower() + "'][name^='view']:not([type='hidden'])";

            ele = (snoelement)Base.SNGObject(name, "element", By.CssSelector(define), null, snobase.MainFrame);

            return ele;
        }

        public string Get_Activity_Date(int index)
        {
            string result = string.Empty;
            string define = "li[class^='h-card h-card_md h-card']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Activity list", by, null, snobase.MainFrame);

            Auto.oelement pr = list.MyList[index];
            snoelement parent = new snoelement("parent", Base.Driver, pr.MyElement);
            define = "div[class='date-calendar']";
            snoelement d = (snoelement)Base.SNGObject("date", "element", By.CssSelector(define), parent, snobase.MainFrame);
            if (d.Existed)
                result = d.MyText;
            return result;
        }
        #endregion Other

        #endregion End - Public methods

        #region Control methods

        #region Textbox

        public snotextbox Textbox_Number()
        {
            snotextbox textbox = null;
            string define = "Number";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Name()
        {
            snotextbox textbox = null;
            string define = "name";
            By by = By.Id(define);
            textbox = (snotextbox)Base.SNGObject("Name", "textbox", by, null, snobase.MainFrame, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Name_CI()
        {
            snotextbox textbox = null;
            string define = "input[id$='.name']:not([type='hidden'])";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Name", "textbox", by, null, snobase.MainFrame, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_ServerURL()
        {
            snotextbox textbox = null;
            string define = "url";
            By by = By.Id(define);
            textbox = (snotextbox)Base.SNGObject("Server URL", "textbox", by, null, snobase.MainFrame, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Email()
        {
            snotextbox textbox = null;
            string define = "Email";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_BusinessPhone()
        {
            snotextbox textbox = null;
            string define = "Business phone";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_ShortDescription()
        {
            snotextbox textbox = null;
            string define = "Short description";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_AssetTag()
        {
            snotextbox textbox = null;
            string define = "Asset tag";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Search()
        {
            snotextbox textbox = null;
            string define = "#sysparm_search";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Search", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_RepeatIntervalDays()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_integration.run_perioddur_day']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_QualysRepeatIntervalDays()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_qualys_integration.run_perioddur_day']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_RepeatIntervalHours()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_integration.run_perioddur_hour']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_QualysRepeatIntervalHours()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_qualys_integration.run_perioddur_hour']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_RepeatIntervalMin()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_integration.run_perioddur_min']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_QualysRepeatIntervalMin()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_qualys_integration.run_perioddur_min']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_RepeatIntervalSec()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_integration.run_perioddur_sec']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_QualysRepeatIntervalSec()
        {
            snotextbox textbox = null;
            string define = "input[id='ni.sn_vul_qualys_integration.run_perioddur_sec']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Repeat Interval", "textbox", by, null, null, true);
            return textbox;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_SerialNumber()
        {
            snotextbox textbox = null;
            string define = "Serial number";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_AlternateAssetTag()
        {
            snotextbox textbox = null;
            string define = "Alternate Asset Tag";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Floor()
        {
            snotextbox textbox = null;
            string define = "Floor";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Seat()
        {
            snotextbox textbox = null;
            string define = "Seat";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Room()
        {
            snotextbox textbox = null;
            string define = "Room";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_UserID()
        {
            snotextbox textbox = null;
            string define = "User ID";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Username()
        {
            snotextbox textbox = null;
            string define = "username";
            By by = By.Id(define);
            textbox = (snotextbox)Base.SNGObject("Username", "textbox", by, null, snobase.MainFrame, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Password()
        {
            snotextbox textbox = null;
            string define = "password";
            By by = By.Id(define);
            textbox = (snotextbox)Base.SNGObject("Password", "textbox", by, null, snobase.MainFrame, true);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Firstname()
        {
            snotextbox textbox = null;
            string define = "First name";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Lastname()
        {
            snotextbox textbox = null;
            string define = "Last name";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        public snotextbox Textbox_Rating()
        {
            snotextbox textbox = null;
            string define = "Rating";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_Riskscore()
        {
            snotextbox textbox = null;
            string define = "Risk score";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snocurrency Textbox_PlannedCost()
        {
            snocurrency textbox = null;
            string define = "Planned Cost";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snocurrency)obj;
            else
                textbox = new snocurrency(define, Base.Driver, null);
            return textbox;
        }

        public snocurrency Textbox_AllocatedCost()
        {
            snocurrency textbox = null;
            string define = "Allocated Cost";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snocurrency)obj;
            else
                textbox = new snocurrency(define, Base.Driver, null);
            return textbox;
        }
        #endregion End - Textbox

        #region Date

        public snodate Date_RetiredDate()
        {
            snodate datetime = null;
            string define = "Retired date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodate)obj;
            else
                datetime = new snodate(define, Base.Driver, null);
            return datetime;
        }
        public snodate Date_ValidTo()
        {
            snodate datetime = null;
            string define = "Valid to";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodate)obj;
            else
                datetime = new snodate(define, Base.Driver, null);
            return datetime;
        }

        #endregion End - Date

        #region Datetime

        public snodatetime Datetime_Followupdate()
        {
            snodatetime datetime = null;
            string define = "Follow up date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_DueDate()
        {
            snodatetime datetime = null;
            string define = "Due date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Assigned()
        {
            snodatetime datetime = null;
            string define = "Assigned";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Assigned_Date()
        {
            snodatetime datetime = null;
            string define = "Assigned Date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Installed()
        {
            snodatetime datetime = null;
            string define = "Installed";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Installed_Date()
        {
            snodatetime datetime = null;
            string define = "Installed Date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_RetiredDate()
        {
            snodatetime datetime = null;
            string define = "Retired date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Delegate_Starts()
        {
            snodatetime datetime = null;
            string define = "Starts";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Delegate_Ends()
        {
            snodatetime datetime = null;
            string define = "Ends";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        public snodatetime Datetime_Created()
        {
            snodatetime datetime = null;
            string define = "Created";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_StartTime()
        {
            snodatetime datetime = null;
            string define = "Start time";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_StopTime()
        {
            snodatetime datetime = null;
            string define = "Stop time";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        #endregion End - Datetime

        #region Lookup

        public snolookup Lookup_Caller()
        {
            snolookup lookup = null;
            string define = "Caller";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Company()
        {
            snolookup lookup = null;
            string define = "Company";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Location()
        {
            snolookup lookup = null;
            string define = "Location";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_BusinessService()
        {
            snolookup lookup = null;
            string define = "Business Service";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Parent()
        {
            snolookup lookup = null;
            string define = "Parent";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_ParentIncident()
        {
            snolookup lookup = null;
            string define = "Parent Incident";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Problem()
        {
            snolookup lookup = null;
            string define = "Problem";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_DispatchGroup()
        {
            snolookup lookup = null;
            string define = "Dispatch group";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_AssignmentGroup()
        {
            snolookup lookup = null;
            string define = "Assignment group";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_AssignedTo()
        {
            snolookup lookup = null;
            string define = "Assigned to";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_ConfigurationItem()
        {
            snolookup lookup = null;
            string define = "Configuration item";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Vendor()
        {
            snolookup lookup = null;
            string define = "Vendor";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Model()
        {
            snolookup lookup = null;
            string define = "Model";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_ModelCategory()
        {
            snolookup lookup = null;
            string define = "Model category";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Knowledge_base()
        {
            snolookup lookup = null;
            string define = "Knowledge base";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Kb_Category()
        {
            snolookup lookup = null;
            string define = "Category";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snolookup Lookup_Kb_Domain()
        {
            snolookup lookup = null;
            string define = "Domain";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_ChangeRequest()
        {
            snolookup lookup = null;
            string define = "Change Request";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_ClosedBy()
        {
            snolookup lookup = null;
            string define = "Closed by";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_RequestItem()
        {
            snolookup lookup = null;
            string define = "Request item";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_RequestedFor()
        {
            snolookup lookup = null;
            string define = "Requested for";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_Schedule()
        {
            snolookup lookup = null;
            string define = "Schedule";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------


        public snolookup Lookup_RequestedBy()
        {
            snolookup lookup = null;
            string define = "Requested by";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_Template()
        {
            snolookup lookup = null;
            string define = "Template";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snolookup Lookup_Modelcategory()
        {
            snolookup lookup = null;
            string define = "Model category";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        public snolookup Lookup_Manufacturer()
        {
            snolookup lookup = null;
            string define = "Manufacturer";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        
       public snolookup Lookup_ReservedFor()
        {
            snolookup lookup = null;
            string define = "Reserved For";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        public snolookup Lookup_ModelID()
        {
            snolookup lookup = null;
            string define = "Model ID";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Asset()
        {
            snolookup lookup = null;
            string define = "Asset";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_SLADefinition()
        {
            snolookup lookup = null;
            string define = "SLA";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_AddRequestAssessmentName()
        {
            snolookup lookup = null;
            string define = "input[id^='sys_display'][id$='pir_respondents']";
            lookup = (snolookup)Base.SNGObject("Unlock Request assessnents", "lookup", By.CssSelector(define), null, snobase.MainFrame);
            return lookup;
        }
        public snolookup Lookup_Domain()
        {
            snolookup lookup = null;
            string define = "Domain";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_RunAs()
        {
            snolookup lookup = null;
            string define = "Run as";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_QualysApiCredentials()
        {
            snolookup lookup = null;
            string define = "Qualys API credentials";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_HostDetectionIntegration()
        {
            snolookup lookup = null;
            string define = "Host detection integration";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        public snolookup Lookup_Integration()
        {
            snolookup lookup = null;
            string define = "Integration";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        public snolookup Lookup_Scanner()
        {
            snolookup lookup = null;
            string define = "Scanner";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Delegate_User()
        {
            snolookup lookup = null;
            string define = "User";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Delegate()
        {
            snolookup lookup = null;
            string define = "Delegate";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Approver()
        {
            snolookup lookup = null;
            string define = "Approver";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Approving()
        {
            snolookup lookup = null;
            string define = "document_id";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        #region Hardware Asset


        //-----------------------------------------------------------------------------------------------------------------------------------


        #region Hardware Asset

        public snolookup Lookup_Department()
        {
            snolookup lookup = null;
            string define = "Department";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_ManagedByCompany()
        {
            snolookup lookup = null;
            string define = "Managed By Company";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_ManagedBy()
        {
            snolookup lookup = null;
            string define = "Managed By";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_OwnedByCompany()
        {
            snolookup lookup = null;
            string define = "Owned By Company";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_OwnedBy()
        {
            snolookup lookup = null;
            string define = "Owned By";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_SupportGroup()
        {
            snolookup lookup = null;
            string define = "Support group";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Stockroom()
        {
            snolookup lookup = null;
            string define = "Stockroom";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        #endregion End - Hardware Asset

        public snolookup Lookup_Feedback()
        {
            snolookup lookup = null;
            string define = "Feedback";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        public snolookup Lookup_Article()
        {
            snolookup lookup = null;
            string define = "Article";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        public snolookup Lookup_User()
        {
            snolookup lookup = null;
            string define = "User";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        #endregion End - Lookup

        #region Combobox

        public snocombobox Combobox_Category()
        {
            snocombobox combo = null;
            string define = "Category";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Subcategory()
        {
            snocombobox combo = null;
            string define = "Subcategory";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_ContactType()
        {
            snocombobox combo = null;
            string define = "Contact type";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_ProblemSource()
        {
            snocombobox combo = null;
            string define = "Problem source";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Source()
        {
            snocombobox combo = null;
            string define = "Source";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Impact()
        {
            snocombobox combo = null;
            string define = "Impact";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        public snocombobox Combobox_ReassignmentReason()
        {
            snocombobox combo = null;
            string define = "Reassignment reason";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Urgency()
        {
            snocombobox combo = null;
            string define = "Urgency";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Priority()
        {
            snocombobox combo = null;
            string define = "Priority";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_State()
        {
            snocombobox combo = null;
            string define = "State";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_CloseCode()
        {
            snocombobox combo = null;
            string define = "Close code";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_KB_Article_type()
        {
            snocombobox combo = null;
            string define = "Article type";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_KB_Workflow()
        {
            snocombobox combo = null;
            string define = "Workflow";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_CIClass()
        {
            snocombobox combo = null;
            string define = "CI class";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Class()
        {
            snocombobox combo = null;
            string define = "Class";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Approval()
        {
            snocombobox combo = null;
            string define = "approval";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Usedfor()
        {
            snocombobox combo = null;
            string define = "Used For";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Run()
        {
            snocombobox combo = null;
            string define = "Run";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_Schedule()
        {
            snocombobox cb = null;
            string define = "div.kb-schedule-selectx.row > div.col-md-4 > select";
            cb = (snocombobox)Base.SNGObject("Schedule", "combobox", By.CssSelector(define), null, snobase.MainFrame);
            return cb;
        }

        #region Hardware Asset
        public snocombobox Combobox_Substate()
        {
            snocombobox combo = null;
            string define = "SubState";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Ownership()
        {
            snocombobox combo = null;
            string define = "Ownership";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Status()
        {
            snocombobox combo = null;
            string define = "Status";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Stage()
        {
            snocombobox combo = null;
            string define = "Stage";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Timezone()
        {
            snocombobox combo = null;
            string define = "Timezone";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        public snocombobox Combobox_Useful()
        {
            snocombobox combo = null;
            string define = "Useful";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        #endregion End - Hardware Asset

        #endregion End - Combobox

        #region Button

        public snobutton Button_SearchKnowledge()
        {
            snobutton bt = null;
            string define = ".btn.btn-default.icon-book.knowledge";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Search knowledge", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_EditthisURL()
        {
            snobutton bt = null;
            string define = "div[class='col-xs-2 col-sm-3 col-lg-2 form-field-addons'] button [id='kb_knowledge.u_automated_script_url_title_text']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Edit this URL", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snobutton Button_ManageAttachments()
        {
            snobutton bt = null;
            string define = "#header_add_attachment, #sc_attachment_button";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Manage attachments", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_CloseAttachmentDialog()
        {
            snobutton bt = null;
            string define = "#attachment>div[class='modal-dialog modal-md']>div[class='modal-content'] button[class='btn btn-icon close icon-cross']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Close attachment dialog", "button", by, null, snobase.MainFrame);

            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snobutton Button_RemoveAttachmentFile()
        {
            snobutton bt = null;
            string define = "removeButton";
            By by = By.Id(define);
            bt = (snobutton)Base.SNGObject("Remove attachment file", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_AdditionalActions()
        {
            snobutton bt = null;
            string define = ".btn.btn-icon.icon-menu.navbar-btn";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Additional actions", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Update()
        {
            snobutton bt = null;
            string define = "Update";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_SaveAndExit()
        {
            snobutton bt = null;
            string define = "Save and Exit";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Submit()
        {
            snobutton bt = null;
            string define = "@@Submit";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Review()
        {
            snobutton bt = null;
            string define = "Review";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Accept()
        {
            snobutton bt = null;
            string define = "Accept";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Next()
        {
            snobutton bt = null;
            string define = "Next";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Previous()
        {
            snobutton bt = null;
            string define = "Previous";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_ExecuteNow()
        {
            snobutton bt = null;
            string define = "Execute Now";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_InitiateScan()
        {
            snobutton bt = null;
            string define = "Initiate Scan";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Delete()
        {
            snobutton bt = null;
            string define = "Delete";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_CloseComplete()
        {
            snobutton bt = null;
            string define = "Close Complete";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Showfields()
        {
            snobutton bt = null;
            string define = "div[class*='form-group'][aria-hidden='false'] button[class^='icon-stream-']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Button list", by, null, snobase.MainFrame);
            if (list.MyList.Count > 0)
            {
                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.Existed)
                    {
                        bt = new snobutton("Show fields", Base.Driver, ele.MyElement);
                        break;
                    }
                }
            }
            else bt = new snobutton("Show fields", Base.Driver, null);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Close_Error()
        {
            snobutton bt = null;
            string define = "#output_messages > button";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Close error", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Finish()
        {
            snobutton bt = null;
            string define = "div.next-button > a > button";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Finish", "button", by, null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Search()
        {
            snobutton bt = null;
            string define = ".input-group-addon-transparent.icon-search.sysparm-search-icon";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Search", "button", by, null);
            return bt;
        }

        public snobutton Button_KA_Review()
        {
            snobutton bt = null;
            string define = "Review";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_KA_Publish()
        {
            snobutton bt = null;
            string define = "Publish";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Cancel()
        {
            snobutton bt = null;
            string define = "Cancel";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_CancelTask()
        {
            snobutton bt = null;
            string define = "Cancel Task";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Approve()
        {
            snobutton bt = null;
            string define = "Approve";
            bt = Base.GButtonByText(define);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_OpenRecord()
        {
            snobutton bt = null;
            string define = "div.popover-header.popover-header-bar.popover-header-buttons > a";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Open Record", "button", by, null, snobase.MainFrame);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Back()
        {
            snobutton bt = null;
                string define = ".btn.btn-default.icon-chevron-left.navbar-btn";
                bt = (snobutton)Base.SNGObject("Back", "button", By.CssSelector(define), null, snobase.MainFrame);

                return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Schedule()
        {
            snobutton bt = null;
            string define = "Schedule";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Allocate()
        {
            snobutton bt = null;
            string define = "Allocate";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public snobutton Button_Yes()
        {
            snobutton bt = null;
            string define = "Yes";
            bt = Base.GButtonByText(define);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_ItegrationYes()
        {
            snobutton bt = null;
            string define = "div.fade.in.modal > div > div >div.modal-footer > button.btn.btn-primary";
            bt = (snobutton)Base.SNGObject("Unlock Request assessnents", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_ItegrationNo()
        {
            snobutton bt = null;
            string define = "div.fade.in.modal > div > div > div.modal-footer > button.btn.btn-default";
            bt = (snobutton)Base.SNGObject("Unlock Request assessnents", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_UnlockRequestAssessmentList()
        {
            snobutton bt = null;
            string define = "button[id$='pir_respondents_unlock']";
            bt = (snobutton)Base.SNGObject("Unlock Request assessnents", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_LockRequestAssessmentList()
        {
            snobutton bt = null;
            string define = "button[id$='pir_respondents_lock']";
            bt = (snobutton)Base.SNGObject("Request assessnents", "button", By.CssSelector(define), null, snobase.MainFrame);

            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Post()
        {
            snobutton bt = null;
            string define = "Post";
            bt = Base.GButtonByText(define);
            return bt;
        }
        public snobutton Button_Retire()
        {
            snobutton bt = null;
            string define = "Retire";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Publish()
        {
            snobutton bt = null;
            string define = "Publish";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Reject()
        {
            snobutton bt = null;
            string define = "Reject";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion End - Button

        #region Textarea

        public snotextarea Textarea_Worknotes()
        {
            snotextarea textarea = null;
            string define = "Work notes";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        

        public snotextarea Textarea_Worknotes_REL()
        {
            string define = String.Empty;

            define = "activity-stream-textarea";

            snotextarea textarea = null;

            textarea = (snotextarea)Base.SNGObject("Work notes", "textarea", By.Id(define), null, snobase.MainFrame);

            return textarea;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_AdditionalCommentsCustomerVisible()
        {
            string define = String.Empty;
            
            define = "Additional comments (Customer visible)";

            snotextarea textarea = null;

            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_AdditionalComments()
        {
            string define = String.Empty;

            define = "activity-stream-textarea";

            snotextarea textarea = null;

            textarea = (snotextarea)Base.SNGObject("Additional comments", "textarea", By.Id(define), null, snobase.MainFrame);

            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_CloseNotes()
        {
            snotextarea textarea = null;
            string define = "Close notes";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_IpAddresses()
        {
            snotextarea textarea = null;
            string define = "IP addresses";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_Description()
        {
            snotextarea textarea = null;
            string define = "Description";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextarea Textarea_Comments()
        {
            snotextarea textarea = null;
            string define = "Comments";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
        public snotextarea Textarea_KnowledgeArticleNotes()
        {
            snotextarea textarea = null;
            string define = "Knowledge article notes";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }
        #endregion End - Textarea

        #region List

        public snolist List_Watchlist()
        {
            snolist list = null;
            string define = "Watch list";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                list = (snolist)obj;
            else
                list = new snolist(define, Base.Driver, null);
            return list;
        }
        public snolist List_CanRead()
        {
            snolist list = null;
            string define = "Can Read";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                list = (snolist)obj;
            else
                list = new snolist(define, Base.Driver, null);
            return list;
        }
        public snolist List_CannotRead()
        {
            snolist list = null;
            string define = "Cannot Read";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                list = (snolist)obj;
            else
                list = new snolist(define, Base.Driver, null);
            return list;
        }
        #endregion End - List

        #region Checkbox

        public snocheckbox Checkbox_KB_Display_attachments()
        {
            snocheckbox cbox = null;
            string define = "Display attachments";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_KB_Attachment_Link()
        {
            snocheckbox cbox = null;
            string define = "Attachment Link";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_KB_Automation()
        {
            snocheckbox cbox = null;
            string define = "Automation";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_KB_Active()
        {
            snocheckbox cbox = null;
            string define = "Active";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Default()
        {
            snocheckbox cbox = null;
            string define = "Default";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Urgent()
        {
            snocheckbox cbox = null;
            string define = "//div[@id='is_active_severity_filter']//div[@class='filter-specific-content']//div[@class='form-group severities']//span[1]//label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }
        public snocheckbox Checkbox_Critical()
        {
            snocheckbox cbox = null;
            string define = "//div[@id='is_active_severity_filter']//div[@class='filter-specific-content']//div[@class='form-group severities']//span[2]//label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }

        public snocheckbox Checkbox_Serious()
        {
            snocheckbox cbox = null;
            string define = "//div[@id='is_active_severity_filter']//div[@class='filter-specific-content']//div[@class='form-group severities']//span[3]//label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }

        public snocheckbox Checkbox_Medium()
        {
            snocheckbox cbox = null;
            string define = "//div[@id='is_active_severity_filter']//div[@class='filter-specific-content']//div[@class='form-group severities']//span[4]//label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }

        public snocheckbox Checkbox_Minimal()
        {
            snocheckbox cbox = null;
            string define = "//div[@id='is_active_severity_filter']//div[@class='filter-specific-content']//div[@class='form-group severities']//span[5]//label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }

        public snocheckbox Checkbox_KnowledgeImport()
        {
            snocheckbox cbox = null;
            string define = "#active > div.input-switch > label";
            By by = By.CssSelector(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }
        public snocheckbox Checkbox_ScheduleImport()
        {
            snocheckbox cbox = null;
            string define = "#active > div.input-switch > label";
            By by = By.CssSelector(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }

        public snocheckbox Checkbox_FixedVulnerabilities()
        {
            snocheckbox cbox = null;
            string define = "//*[@id='host-tabs-pane-1']/div/form/section[2]/span/label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }
        public snocheckbox Checkbox_AssetTags()
        {
            snocheckbox cbox = null;
            string define = "//*[@id='host-tabs-pane-1']/div/form/section[3]/span/label";
            By by = By.XPath(define);
            cbox = new snocheckbox(define, Base.Driver, by, null, snobase.MainFrame);
            return cbox;
        }

        public snocheckbox Checkbox_Manageddomain()
        {
            snocheckbox cbox = null;
            string define = "Managed domain";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_UseDefaultAppliance()
        {
            snocheckbox cbox = null;
            string define = "Use default appliance";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Approvals()
        {
            snocheckbox cbox = null;
            string define = "Approvals";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Assignments()
        {
            snocheckbox cbox = null;
            string define = "Assignments";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_CCNotifications()
        {
            snocheckbox cbox = null;
            string define = "CC notifications";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_MeetingInvitations()
        {
            snocheckbox cbox = null;
            string define = "Meeting invitations";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }
        public snocheckbox Checkbox_CreateKnowledge()
        {
            snocheckbox cbox = null;
            string define = "Create Knowledge";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Responded()
        {
            snocheckbox cbox = null;
            string define = "Responded";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }
        #endregion End of Checkbox

        #region Other

        private snoelement Dialog_ManageAttachment(bool noWait)
        {
            snoelement dl = null;
            string define = "#attachment>div[class='modal-dialog modal-md']>div[class='modal-content']";
            By by = By.CssSelector(define);
            dl = (snoelement)Base.SNGObject("Dialog manage attachments", "element", by, null, snobase.MainFrame, noWait);
            return dl;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement BrowseFile_Attachment()
        {
            snoelement bf = null;

            string define = "input[id='attachFile'], input[id='loadFileXml']";
            By by = By.CssSelector(define);
            snoelements sneles = Base.SNGObjects("List", by, null, snobase.MainFrame);
            foreach (Auto.oelement e in sneles.MyList) 
            {
                if (e.MyElement.Displayed) 
                {
                    bf = new snoelement("Choose file", Base.Driver, e.MyElement);
                    break;
                }
            }
            return bf;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelements Error_Message_List()
        {
            snoelements errs = null;
            string define = "div[class^='outputmsg outputmsg_error']";
            By by = By.CssSelector(define);
            errs = Base.SNGObjects("Error list", by, null, snobase.MainFrame, false, true);
            return errs;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelements AttachedKnowledge()
        {
            snoelements errs = null;
            string define = "div[class^='outputmsg outputmsg_error']";
            By by = By.CssSelector(define);
            errs = Base.SNGObjects("Error list", by, null, snobase.MainFrame, false, true);
            return errs;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement View_RequestItem()
        {
            return GView("request_item", "Requested Item");
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement Dialog_Confirmation(bool noWait)
        {
            snoelement dl = null;

            string define = "div[class^='modal-dialog modal-md'] div[class^='modal-body container']";

            dl = (snoelement)Base.SNGObject("Dialog confirmation", "element", By.CssSelector(define), null, snobase.MainFrame, noWait);

            return dl;
        }
        #endregion End - Other

        #region Demand
        public snolookup Lookup_DemandManagerAssignmentGroup()
        {
            snolookup lookup = null;
            string define = "Demand manager assignment group";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_DemandManager()
        {
            snolookup lookup = null;
            string define = "Demand manager";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snocombobox Combobox_ResourceType()
        {
            snocombobox combo = null;
            string define = "Resource type";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snolookup Lookup_Group()
        {
            snolookup lookup = null;
            string define = "Group";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Task()
        {
            snolookup lookup = null;
            string define = "Task";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snotextbox Textbox_PlannedHours()
        {
            snotextbox textbox = null;
            string define = "Planned hours";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snodate Date_StartDate()
        {
            snodate date = null;
            string define = "Start date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                date = (snodate)obj;
            else
                date = new snodate(define, Base.Driver, null);
            return date;
        }

        public snodate Date_EndDate()
        {
            snodate date = null;
            string define = "End date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                date = (snodate)obj;
            else
                date = new snodate(define, Base.Driver, null);
            return date;
        }
        #endregion

        #endregion End - Control methods

        #region Button

        public snobutton Button_AddCIRelationship()
        {
            snobutton bt = null;

            string define = "a[id='addrelation'][href*='ci_relationship_manage'], a[id='add_relationship']";
            bt = (snobutton)Base.SNGObject("Add", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_CI_Split()
        {
            snobutton bt = null;

            string define = "span[id='split_button'][class$='ctiveButton']";
            bt = (snobutton)Base.SNGObject("Split", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_CI_BSMMapView()
        {
            snobutton bt = null;

            string define = "a[id='super_bsm_map'][href*='ngbsm.do']";
            bt = (snobutton)Base.SNGObject("Split", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_Calendar_Select()
        {
            snobutton bt = null;

            string define = "button[id='GwtDateTimePicker_ok']";
            bt = (snobutton)Base.SNGObject("Select", "button", By.CssSelector(define), null, snobase.MainFrame);
            return bt;
        }

        public snobutton Button_Request()
        {
            snobutton bt = null;
            string define = "Request";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Confirm()
        {
            snobutton bt = null;
            string define = "Confirm";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Complete()
        {
            snobutton bt = null;
            string define = "Complete";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_SubmitDemand()
        {
            snobutton bt = null;
            string define = "Submit demand";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Screen()
        {
            snobutton bt = null;
            string define = "Screen";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Qualify()
        {
            snobutton bt = null;
            string define = "Qualify";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion End - Button

        private snoelement Ele_Loading()
        {
            snoelement ele = null;

            string define = "div[id='loading_span'] h1";

            ele = (snoelement)Base.SNGObject("Loading", "element", By.CssSelector(define), null, snobase.MainFrame, true);

            return ele;
        }


        
        #region Combobox

        public snocombobox List_CI_AvailableRelationships()
        {
            snocombobox list = null;
            string define = "available_select";
            By by = By.Id(define);
            list = (snocombobox)Base.SNGObject("available list", "combobox", by, null, snobase.MainFrame);
            return list;
        }

        public snocombobox Combobox_RequestType()
        {
            snocombobox combo = null;
            string define = "Request Type:";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        #endregion End - Combobox

        #endregion End - Control methods

        #region Other methods

        private string pageName = string.Empty;

        public bool CreateNormalChange([Optional] bool javaClick, [Optional] bool noFrame)
        {

            System.Console.WriteLine("***[Call function]: Create Normal Change " + this.pageName);
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Item("Create Normal Change", javaClick);
                        if (flag)
                        {
                            WaitLoading();
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot click Create Normal Change  " + _name); }
            return flag;
        }
        //-------------------------------------------------------------------------------------------------------
        public bool CreateProblem([Optional] bool javaClick, [Optional] bool noFrame)
        {

            System.Console.WriteLine("***[Call function]: Create Problem " + this.pageName);
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Item("Create Problem", javaClick);
                        if (flag)
                        {
                            WaitLoading();
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot click Create Problem  " + _name); }
            return flag;
        }
        //--------------------------------------------------------------------------------------------------
        public bool ReloadForm([Optional] bool javaClick, [Optional] bool noFrame)
        {

            System.Console.WriteLine("***[Call function]: Reload form " + this.pageName);
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Item("Reload form", javaClick);
                        if (flag)
                        {
                            WaitLoading();
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot click Reload form " + _name); }
            return flag;
        }
        //--------------------------------------------------------------------------------------------------
        public snoelement GRelatedLink(string name)
        {
            snoelement link = null;

            string define = ".related_links_container a";
            snoelements list = Base.SNGObjects("Link list", By.CssSelector(define), null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList)
            {
                if (ele.MyText.ToLower().Equals(name.ToLower()))
                {
                    link = new snoelement("link", Base.Driver, ele.MyElement);
                    break;
                }
            }

            return link;
        }
        //--------------------------------------------------------------------------------------------------
        public bool SelectTemplate(string template, [Optional] bool javaClick, [Optional] bool noFrame)
        {

            System.Console.WriteLine("***[Call function]: Select template (" + template + ")");
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Items(template, javaClick);

                        if (flag)
                        {
                            WaitLoading();
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot click Templates " + _name); }
            return flag;
        }


        public bool SelectTemplate_1(string template, [Optional] bool javaClick)
        {

            System.Console.WriteLine("***[Call function]: Select template (" + template + ")");
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Items("Templates;Apply Template", javaClick);

                        if (flag)
                        {
                            Thread.Sleep(4000);
                            ItilList list = new ItilList(Base, "Search");
                            Base.SwitchToPage(1);
                            list.WaitLoading(false, true);
                            string condition = "Name=" + template;
                            flag = list.SearchAndOpen("Name", template, condition, "Name", null, true);
                            Thread.Sleep(2000);
                            
                            Base.SwitchToPage(0);
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot click Templates " + _name); }
            return flag;
        }

        public bool VerifyMessageInfo(string values)
        {
            bool flag = true;
            string[] arr = null;
            if (values.Contains("|"))
            {
                arr = values.Split('|');
            }
            else
            {
                arr = new string[] { values };
            }
            string define = ".outputmsg.outputmsg_info.notification.notification-info";
            snoelements list = Base.SNGObjects("Message info list", By.CssSelector(define), null, snobase.MainFrame);
            if (list.MyList.Count > 0)
            {
                bool flagF = true;
                foreach (string value in arr)
                {
                    flagF = VerifyInfo(list, value);
                    if (flagF)
                    {
                        System.Console.WriteLine("***[Passed]: Found message info (" + values + ")");
                    }
                    else
                    {
                        System.Console.WriteLine("***[FAILED]: NOT found message info (" + values + ")");
                        if (flag) flag = false;
                    }
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("***[ERROR]: Not found any message info.");
            }
            return flag;
        }

        public bool Verify_Field_MessageInfo(string values)
        {
            bool flag = true;
            string[] arr = null;
            if (values.Contains("|"))
            {
                arr = values.Split('|');
            }
            else
            {
                arr = new string[] { values };
            }
            string define = "div[class='fieldmsg notification notification-error']";
            snoelements list = Base.SNGObjects("Message field info list", By.CssSelector(define), null, snobase.MainFrame);
            if (list.MyList.Count > 0)
            {
                bool flagF = true;
                foreach (string value in arr)
                {
                    flagF = VerifyInfo(list, value);
                    if (flagF)
                    {
                        System.Console.WriteLine("***[Passed]: Found message info (" + values + ")");
                    }
                    else
                    {
                        System.Console.WriteLine("***[FAILED]: NOT found message info (" + values + ")");
                        if (flag) flag = false;
                    }
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("***[ERROR]: Not found any message info.");
            }
            return flag;
        }


        private bool VerifyInfo(snoelements list, string value)
        {
            bool flag = false;
            foreach (Auto.oelement ele in list.MyList)
            {
                if (ele.MyText.Trim().ToLower().Contains(value.Trim().ToLower()))
                {
                    flag = true;
                    ele.Highlight();
                    break;
                }
            }
            return flag;

        }
        //-------------------------------------------------------------------------------------------------------
        public bool HandleAlert(string choice, [Optional] string AlerMess)
        {
            bool flag = true;

            if (AlerMess == null)
            {
                AlerMess = "No Check Alert";
            }

            IAlert alert = Base.Driver.SwitchTo().Alert();
            System.Console.WriteLine("///***The system got an alert with content: " + alert.Text);

            if (AlerMess != "No Check Alert")
            {
                if (AlerMess.Contains(alert.Text.ToString()))
                {
                    System.Console.WriteLine("///***[PASS]: Found alert message:[" + alert.Text + "]");
                }
                else
                {
                    flag = false;
                    System.Console.WriteLine("///***[ERROR]: Not found alert message:[" + alert.Text + "]");
                }
            }
            else
            {
                System.Console.WriteLine("///***No need verify alert message.");

            }

            if (choice.ToLower() == "accept")
            {
                alert.Accept();
                System.Console.WriteLine("///***Accept this alert");
            }
            else if (choice.ToLower() == "dismiss")
            {
                alert.Dismiss();
                System.Console.WriteLine("///***Dismiss this alert!");
            }
            else
            {
                System.Console.WriteLine("Wrong action to handle this alert. It must be Accept or Dismiss!");
                flag = false;
            }

            return flag;
        }

        //--------------------------------------------------------------------------------------------------
        public bool SelectView(string view, [Optional] bool javaClick, [Optional] bool noFrame)
        {

            System.Console.WriteLine("***[Call function]: Select view (" + view + ")");
            bool flag = true;
            snobutton bt = Button_AdditionalActions();
            if (bt.Existed)
            {
                flag = bt.Click();
                if (flag)
                {
                    flag = Verify_ContextMenu_Existed();
                    if (!flag)
                    {
                        bt.Click(true);
                        flag = Verify_ContextMenu_Existed();
                    }

                    if (flag)
                    {
                        flag = Select_ContextMenu_Items(view, javaClick);

                        if (flag)
                        {
                            WaitLoading();
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Context menu not existed.");
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("-*-[Info]: Cannot get button additional actions.");
            }

            if (!flag) { System.Console.WriteLine("-*-[Info]: Cannot click view " + _name); }
            return flag;
        }

        //--------------------------------------------------------------------------------------------------------------------
        public bool VerifyTabHeader(string listTabName)
        {
            bool flag = true;

            flag = VerifyDupplicateRelatedTable();

            if (flag)
            {
                string[] arrTab = listTabName.Split(';');
                foreach (string tabName in arrTab)
                {
                    flag = Select_Tab(tabName, true);
                    
                    if (flag)
                    {
                            System.Console.WriteLine("***Tab " + tabName + " exists");  
                    }
                    else
                    {
                        System.Console.WriteLine("***[ERROR]: Not found tab [" + tabName + "]");
                        flag = false;
                    }
                }
            }

            return flag;
        }
        //--------------------------------------------------------------------------------------------------------------------
        public bool VerifyDupplicateRelatedTable()
        {
            bool flag = true;

            string define = ".tab_caption_text";
            snoelements list = Base.SNGObjects("Header list", By.CssSelector(define), null, snobase.MainFrame);
            Console.WriteLine("***Info: Found " + list.MyList.Count + " related table (tab).");

            List<string> noDup = new List<string>();
            List<string> dup = new List<string>();


            foreach (Auto.oelement e in list.MyList)
            {
                if (!noDup.Contains(e.MyText))
                    noDup.Add(e.MyText);
                else
                    dup.Add(e.MyText);
            }

            if (dup.Count > 0)
            {
                flag = false;
                foreach (string str in dup)
                {
                    Console.WriteLine("***ERROR: Found dupplicate tab [" + str + "]");
                }
            }

            return flag;
        }

        public bool VerifyScheduleChangesSection(string ExpContent)
        {
            bool flag = true;
            string comp_str = string.Empty;
            string define = "td>table[class='wide'][cellspacing]>tbody>tr>td";
            snoelement expandicon = null;
            snoelements elist = Base.SNGObjects("elements", By.CssSelector(define), null, snobase.MainFrame);
            if (elist.MyList.Count > 0)
            {
                foreach (Auto.oelement e in elist.MyList)
                {
                    if (e.MyElement.GetAttribute("colspan") == "99")
                    {
                        IWebElement ele = e.MyElement.FindElement(By.CssSelector("span[id='toggle_sched']"));
                        if (ele.GetAttribute("style").Contains("display: none;"))
                        {
                            expandicon = (snoelement)Base.SNGObject("Expand icon", "element", By.CssSelector("td[class='label']>span>a>img"), null, snobase.MainFrame);
                            if (expandicon.Existed)
                            {
                                expandicon.Click(true);
                            }
                            else
                            {
                                flag = false;
                                System.Console.WriteLine("Cannot find Expand icon");
                                break;
                            }
                        }
                    }

                    if (e.MyText != string.Empty)
                    {
                        if (comp_str == string.Empty)
                        {
                            comp_str = comp_str + e.MyText.Replace("\r\n", "|");
                        }
                        else
                        {
                            comp_str = comp_str + ";" + e.MyText.Replace("\r\n", "|");
                        }
                    }
                }
                System.Console.WriteLine("System runs - " + comp_str);
                if (!Base.StringCompare(comp_str, ExpContent))
                {
                    flag = false;
                    System.Console.WriteLine("Cannot find the expected result regarding Schedule changes.");
                    System.Console.WriteLine("Expected result: " + ExpContent);
                }
            }
            else
            {
                System.Console.WriteLine("-*-ERROR: Not element is found.");
                flag = false;
            }

            return flag;
        }

        public void WaitFilterLoading()
        {
            System.Console.WriteLine("Filter loading...");
            snoelement ele = Ele_Loading();
            int count = 0;
            bool flag = ele.Existed;
            while (count < 10 && flag)
            {
                Thread.Sleep(1000);
                flag = ele.Existed;
                count++;
            }
        }

        public bool CI_Split_Button_Select(bool deSelect = false)
        {
            bool flag = true;
            snobutton button = Button_CI_Split();

            // Verify the Split button
            flag = (button != null);
            if (flag)
            {
                // If users want to select the Split View button
                if (!deSelect)
                {
                    // Only click on the button when it's not active
                    if (button.MyElement.GetAttribute("Class").Contains("notActive"))
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            WaitLoading();
                            button = Button_CI_Split();
                            flag = (!button.MyElement.GetAttribute("Class").Contains("notActive"));
                            if (!flag)
                            {
                                System.Console.WriteLine("***[ERROR]: Cannot click on [SplitView] button");
                            }
                        }
                    }
                }
                // If users want to deselect the Split View button
                else
                {
                    // Only click on the button when it's active
                    if (!button.MyElement.GetAttribute("Class").Contains("notActive"))
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            WaitLoading();
                            button = Button_CI_Split();
                            flag = (button.MyElement.GetAttribute("Class").Contains("notActive"));
                            if (!flag)
                            {
                                System.Console.WriteLine("***[ERROR]: Cannot click on [SplitView] button");
                            }
                        }
                    }
                }
            }
            else
            {
                System.Console.WriteLine("***[ERROR]: Cannot click on [SplitView] button");
            }

            return flag;
        }

        public bool CI_VerifyRelationshipTable(string CIName, string tableName)
        {
            bool flag = Select_Tab("System");
            if (flag)
            {
                snoelement table = GRelationshipTable(tableName);
                flag = (table != null);
                if (flag)
                {
                    snoelement e = (snoelement)Base.SNGObject("Relationship", "element", By.LinkText(CIName), table, snobase.MainFrame);
                    flag = (e != null);
                }
                else
                {
                    System.Console.WriteLine("***[ERROR]: Cannot get [CI Relationship] table");
                }
            }
            else
            {
                System.Console.WriteLine("***[ERROR]: Not found [System] tab");
            }
            return flag;
        }

        private snoelement GRelationshipTable(string tableName)
        {
            bool flag = false;
            snoelement table = null;
            snoelements list = null;

            string define = "[id='div_relation_flat'] > table[class='wide'] td[class='vsplit']";

            list = Base.SNGObjects("list table", By.CssSelector(define), null, snobase.MainFrame);
            flag = (list.MyList.Count == 2);
            if (!flag)
            {
                System.Console.WriteLine("***ERROR: Cannot get CI Relationship table.");
            }

            if (flag)
            {
                int index = GTableRelationshipIndex(tableName);
                if (index >= 0)
                {
                    table = new snoelement(tableName, Base.Driver, list.MyList[index].MyElement);
                }
            }

            return table;
        }

        private int GTableRelationshipIndex(string tableName)
        {
            bool flag = false;
            int index = -1;
            snoelements list = null;

            string define = "[id='div_relation_flat'] > table[class='wide'] > thead > tr > td";

            list = Base.SNGObjects("header", By.CssSelector(define), null, snobase.MainFrame);
            flag = (list.MyList.Count == 2);
            if (!flag)
            {
                System.Console.WriteLine("***ERROR: Cannot get CI Relationship table headers.");
            }

            if (flag)
            {
                index = list.MyList.FindIndex(x => x.MyText.Trim().ToLower() == tableName.Trim().ToLower());
            }
            return index;
        }

        public bool CI_CheckCIInBSM(string CIName)
        {
            string define = "g[class^='label-group'] text[class*='label'] + title";
            snoelements list = Base.SNGObjects("CI Title", By.CssSelector(define), null, snobase.MainFrame);
            foreach (Auto.oelement e in list.MyList)
            {
                if (CIName.ToLower().Trim() == e.MyText.ToLower().Trim())
                {
                    return true;
                }
            }
            return false;

        }

        #endregion Other methods
    
    }

    }

