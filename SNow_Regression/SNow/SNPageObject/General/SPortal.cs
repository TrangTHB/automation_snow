﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace SNow
{
    public class SPortal
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        private string tabledefine = "table[class*='table-responsive']";
        private string colDefine = "thead div[class^='th-title']";
        private string rowDefine = "tbody tr[ng-repeat]";
        private string cellDefine = "td[role]:not([role='link'])";

        #endregion Variables

        #region Properties

        public snobase Base
        {
            get { return _snobase; }
            set { _snobase = value; }
        }

        #endregion Properties

        #region Constructor

        public SPortal(snobase obase, string name)
        {
            Base = obase;
            _name = name;
        }

        #endregion Constructor

        #region Private methods

        //Use this method ONLY when working in search box of [Check Status] > [My Issues] or [My Requests] or [My Prior Approvals]
        private bool Search(string condition)
        {
            bool flag = true;
            snoelement clear = ClearSearch();
            flag = clear.Existed;
            if (flag)
            {
                flag = clear.Click();
                if (flag)
                {
                    WaitLoading();
                    snotextbox textbox = Textbox_Check_status_Search();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(condition, true);
                        if (flag)
                            WaitLoading();
                    }
                    else Console.WriteLine("***[Info]:Cannot get textbox item search.");
                }
            }
            else Console.WriteLine("***[Info]:Cannot get link clear search.");

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public string GBreadcrumbs()
        {
            string result = string.Empty;
            string define = "ul[aria-label*='breadcrumbs'] li a, ul[class*='nav-pills'] li a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by);
            foreach (Auto.oelement ele in list.MyList)
            {
                result = result + ele.MyText.Trim() + ";";
            }
            result = result.Substring(0, result.Length - 1);
            return result;
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        ///
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="page">input as 'incident' or 'catalog' or 'ritm'</param>
        /// <returns></returns>
        private bool VerifyAttachment(string fileName, string page)
        {
            bool flag = false;
            string define = String.Empty;

            switch (page)
            {
                case "incident":
                case "catalog":
                    define = "a[class^='get-attachment']";
                    break;

                case "ritm":
                    define = "li[class^='attached-file'] a";
                    break;
            }

            snoelements list = Base.SNGObjects("Attachment files", By.CssSelector(define));
            foreach (Auto.oelement e in list.MyList)
            {
                if (e.MyText.Trim().ToLower().Contains(fileName.Trim().ToLower()))
                {
                    flag = true;
                    e.Highlight();
                    break;
                }
            }
            if (flag) { System.Console.WriteLine("***[Passed]: Found attachment file (" + fileName + ")"); }
            else { System.Console.WriteLine("***[ERROR]: NOT found attachment file (" + fileName + ")"); }
            return flag;
        }

        //-----------------------------------------------------------------------------------------
        private bool InputValueForControl(string label, string value)
        {
            bool flag = true;

            object obj = Base.GControlByLabelServicePortal(label);
            string type = obj.GetType().FullName.Trim().ToLower().Replace("snow.sno_sp_", "");
            type = type.Replace("snow.sno", "");
            string temp = string.Empty;
            switch (type.Trim().ToLower())
            {
                case "textarea":
                case "textbox":
                    temp = value;
                    break;
                default:
                    temp = "auto";
                    break;
            }
            flag = InputValue(obj, type, temp, label);
            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool InputValue(object obj, string type, string value, string label)
        {
            bool flag = true;

            switch (type.Trim().ToLower())
            {
                case "combobox":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for combobox [" + label + "].");
                    snocombobox combobox = (snocombobox)obj;
                    combobox.MoveToElement();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                            flag = combobox.SP_SelectItem(2);
                        else
                            flag = combobox.SP_SelectItem(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found combobox [" + label + "].");

                    if (!flag)
                        Console.WriteLine("-*-[Info]: Cannot input value for combobox [" + label + "].");
                    break;

                case "lookup":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for lookup [" + label + "].");
                    snolookup lookup = (snolookup)obj;
                    lookup.MoveToElement();
                    flag = lookup.Existed;
                    if (flag)
                        if (value.ToLower() == "auto")
                        {
                            flag = lookup.SP_Select(2);
                        }
                        else
                        {
                            flag = lookup.SP_Select(value);
                        }
                    else
                        Console.WriteLine("-*-[Info]: Not found lookup [" + label + "].");

                    if (!flag)
                        Console.WriteLine("-*-[Info]: Cannot input value for textbox [" + label + "].");
                    break;

                case "textbox":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for textbox [" + label + "].");
                    snotextbox textbox = (snotextbox)obj;
                    textbox.MoveToElement();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                            flag = textbox.SetText("Automation value");
                        else
                            flag = textbox.SetText(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found textbox [" + label + "].");

                    if (flag)
                        textbox.MyElement.SendKeys(Keys.Tab);
                    else
                        Console.WriteLine("-*-[Info]: Cannot input value for textbox [" + label + "].");
                    break;

                case "textarea":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for textarea [" + label + "].");
                    snotextarea textarea = (snotextarea)obj;
                    textarea.MoveToElement();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                            flag = textarea.SetText("Automation value");
                        else
                            flag = textarea.SetText(value);

                        if (flag)
                            textarea.MyElement.SendKeys(Keys.Tab);
                        else
                            Console.WriteLine("-*-[Info]: Cannot input value for textarea [" + label + "].");
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found textarea [" + label + "].");
                    break;

                case "date":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for date [" + label + "].");
                    snodate date = (snodate)obj;
                    Thread.Sleep(1000);
                    date.MoveToElement();
                    flag = date.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            string startDate = DateTime.Now.ToString("yyyy-MM-dd");
                            string endDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                            if (label.ToLower().Contains("start") || label.ToLower().Contains("begin") || label.ToLower().Contains("activation"))
                            {
                                flag = date.SetText(startDate);
                            }
                            else if (label.ToLower().Contains("end") || label.ToLower().Contains("complete") || label.ToLower().Contains("extension") || label.ToLower().Contains("expiration"))
                            {
                                flag = date.SetText(endDate);
                            }
                            else
                            {
                                flag = date.SetText(startDate);
                            }
                        }
                        else
                            flag = date.SetText(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found date [" + label + "].");

                    if (flag)
                        date.MyElement.SendKeys(Keys.Tab);
                    else
                        Console.WriteLine("-*-[Info]: Cannot input value for date [" + label + "].");
                    break;

                case "datetime":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for datetime [" + label + "].");
                    snodatetime datetime = (snodatetime)obj;
                    datetime.MoveToElement();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            string startDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            string endDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                            if (label.ToLower().Contains("start") || label.ToLower().Contains("begin"))
                            {
                                flag = datetime.SetText(startDate);
                            }
                            else if (label.ToLower().Contains("end") || label.ToLower().Contains("complete") || label.ToLower().Contains("extension"))
                            {
                                flag = datetime.SetText(endDate);
                            }
                            else
                            {
                                flag = datetime.SetText(startDate);
                            }
                        }
                        else
                            flag = datetime.SetText(value);
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found datetime [" + label + "].");

                    if (flag)
                        datetime.MyElement.SendKeys(Keys.Tab);
                    else
                        Console.WriteLine("-*-[Info]: Cannot input value for datetime [" + label + "].");
                    break;
                case "radiogroup":
                    Console.WriteLine("-*-[Info]: Input value [" + value + "] for radiogroup [" + label + "].");
                    sno_sp_radiogroup rdgroup = (sno_sp_radiogroup)obj;
                    
                    rdgroup.MoveToElement();
                    flag = rdgroup.Existed;
                    if (flag)
                    {
                        if (value.ToLower() == "auto")
                        {
                            foreach (snoradio rd in rdgroup.Items)
                            {
                                if (rd.SP_Text.Trim().ToLower() != "-- none --")
                                {
                                    flag = rd.Click();
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (snoradio rd in rdgroup.Items)
                            {
                                if (rd.SP_Text.Trim().ToLower() == value.Trim().ToLower())
                                {
                                    flag = rd.Click();
                                    break;
                                }
                            }
                        }
                    }
                    else
                        Console.WriteLine("-*-[Info]: Not found radiogroup [" + label + "].");
                    break;
            }
            return flag;
        }

        //-------------------------------------------------------------------------------------------------
        /// <summary>
        /// This method to retrieve data using a single input string. Give it the label and it return a text. E.g. Number, State...
        /// </summary>
        /// <param name="field">The field can be found in Record Details section in an INCIDENT detail form</param>
        /// <param name="section">Can be either 'Record Details' or 'Options'</param>
        /// <returns></returns>
        public string INC_Record_Details(string field)
        {
            string value = String.Empty;

            //In Record Detail section
            string define = "//label[text()='" + field + "']/following-sibling::span/div[contains(@class,'ng-binding')]";
            snoelement ele = null;
            By by = By.XPath(define);

            ele = (snoelement)Base.SNGObject(field, "element", by);

            if (ele.Existed)
            {
                value = ele.MyText.Trim();
            }

            return value;
        }

        //-------------------------------------------------------------------------------------------------
        /// <summary>
        /// This method to retrieve data using a single input string. Give it the label and it return a text. E.g. Number, State, User name:...
        /// </summary>
        /// <param name="field">The field can be found in Options section which under Resolve button</param>
        /// <returns></returns>
        public string INC_Options(string field)
        {
            string value = String.Empty;
            //In Options section
            string define = "//label[text()='" + field + "']/following-sibling::div[contains(@class,'ng-binding')]";
            snoelement ele = null;
            By by = By.XPath(define);

            ele = (snoelement)Base.SNGObject(field, "element", by);

            if (ele.Existed)
            {
                value = ele.MyText.Trim();
            }

            return value;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement GSearchResultGroup(string groupName, [Optional] int index)
        {
            snoelement e = null;
            bool flag = true;
            string define = "a[class^='list-group-item ng-scope']";
            if (flag)
            {
                snoelements list = Base.SNGObjects("List group result", By.CssSelector(define));
                if (list.MyList.Count > 0)
                {
                    int count = 0;
                    foreach (Auto.oelement ele in list.MyList)
                    {
                        if (ele.MyText.ToLower().Contains(groupName.ToLower()))
                        {
                            if (count == index)
                            {
                                e = new snoelement("Group", Base.Driver, ele.MyElement);
                                break;
                            }
                            else
                            {
                                count = count + 1;
                            }
                        }
                    }
                }
                else
                {
                    System.Console.WriteLine("Not found any element.");
                }
            }

            if (e == null) { System.Console.WriteLine("Not found group result element [" + groupName + "]"); }
            return e;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="index"></param>
        /// <param name="kaSearch">Set true if seaching on Knowledge Search</param>
        /// <returns></returns>
        private snoelement GSearchResultItem(string itemName, [Optional] int index, [Optional] string type,[Optional] bool flagFC)
        {
            snoelement e = null;
            bool flag = true;
            string define = "span[class='ng-binding']";
            string define_kaSearch = "a[class='kb-title ng-binding ng-scope']";
            string define_catalogItem = "div[ng-if^='item.type']>a>span[class]";
            By by = null;

            flag = true;
            if (flag)
            {
                if (type == null || type == string.Empty)
                {
                    //Search on Global Search
                    by = By.CssSelector(define);
                }
                else 
                {
                    switch (type.ToLower()) 
                    {
                        case "knowledge":
                            //Search on Knowledge Search
                            by = By.CssSelector(define_kaSearch);
                            break;
                        case "catalog":
                            //Search on Knowledge Search
                            by = By.CssSelector(define_catalogItem);
                            break;
                    }
                }
                
                
                snoelements list = Base.SNGObjects("List item result", by);

                if (list.MyList.Count > 0)
                {
                    int count = 0;
                    
                    foreach (Auto.oelement ele in list.MyList)
                    {
                        if (flagFC)
                        {
                            if (ele.MyText.ToLower().Contains(itemName.ToLower()))
                            {
                                if (count == index)
                                {
                                    e = new snoelement("Item", Base.Driver, ele.MyElement);
                                    break;
                                }
                                else
                                {
                                    count = count + 1;
                                }
                            }
                        }
                        else
                        {
                            if (ele.MyText.ToLower().Equals(itemName.ToLower()))
                            {
                                if (count == index)
                                {
                                    e = new snoelement("Item", Base.Driver, ele.MyElement);
                                    break;
                                }
                                else
                                {
                                    count = count + 1;
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.Console.WriteLine("Not found any element.");
                }
            }

            if (e == null) { System.Console.WriteLine("Not found item result element [" + itemName + "]"); }
            return e;
        }

        private bool VItemStage(string stage, snoelements list, [Optional] bool noVerifyColor)
        {
            bool flag = false;
            foreach (Auto.oelement ele in list.MyList)
            {
                if (ele.MyText.Trim().ToLower().Equals(stage.Trim().ToLower()))
                {
                    flag = true;
                    ele.Highlight();

                    if (flag && noVerifyColor)
                    {
                        flag = false;
                        IWebElement pr = ele.MyElement.FindElement(By.XPath(".."));
                        IWebElement img = pr.FindElement(By.TagName("img"));
                        if (img.GetAttribute("src").Contains("check-circle"))
                        {
                            Console.WriteLine("Runtime item stage is: [" + ele.MyText.Trim() + "]");
                            flag = true;
                        }
                    }
                    break;
                }
            }

            return flag;
        }
        #endregion Private methods

        #region Public methods

        public void WaitLoading()
        {
            Console.WriteLine(_name + " page wait loading...");
            Base.PageWaitLoadingServicePortal();
        }

        //-------------------------------------------------------------------------------------------------

        public bool Input_Mandatory_Fields(string value)
        {
            bool flag = true;

            List<string> list = Base.SP_GAllMandatoryControlWithLabelOnForm();

            while (list.Count > 0)
            {
                foreach (string label in list)
                {
                    bool flagV = InputValueForControl(label, value);
                    if (!flagV && flag)
                        flag = false;
                    Thread.Sleep(2000);
                }

                list = Base.SP_GAllMandatoryControlWithLabelOnForm();
            }

            return flag;
        }

        //-------------------------------------------------------------------------------------------------
        public bool AttachmentFile(string fileName)
        {
            bool flag = true;

            flag = Base.IsDllRegistered();

            if (flag)
            {
                snobutton bt = Button_Add_Attachments();
                if (bt.Existed)
                {
                    bt.Click();
                    flag = Base.WFileUploadSelect(fileName);
                    if (flag)
                    {
                        WaitLoading();
                    }
                }
            }
            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///
        /// </summary>
        /// <param name="fileNames"></param>
        /// <param name="page">input as 'incident' or 'catalog' or 'ritm'</param>
        /// <returns></returns>
        public bool VerifyAttachmentFile(string fileNames, string page)
        {
            bool flag = true;
            if (fileNames.Contains(";"))
            {
                string[] arr = fileNames.Split(';');
                foreach (string item in arr)
                {
                    bool flagF = VerifyAttachment(item, page);
                    if (!flagF && flag)
                        flag = false;
                }
            }
            else
            {
                flag = VerifyAttachment(fileNames, page);
            }
            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// This method will do 2 actions: click the remove button and scan the attachment list. Require which page are working on
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="page">Page to Delete Attachment, Example: "incident","catalog","ritm"</param>
        /// <returns></returns>
        public bool Remove_Attachment(string filename, string page)
        {
            bool flag = true;
            snobutton bt = null;

            switch (page)
            {
                case "incident":
                case "catalog":
                    bt = Button_Remove_Attachment(filename, page);
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click();
                        if (flag)
                        {
                            //Click Ok button
                            bt = Button_OK_Delete_Attachment();
                            flag = bt.Existed;
                            if (flag)
                            {
                                flag = bt.Click();
                                if (!flag) System.Console.WriteLine("CANNOT click button [OK] on Delete Attachment pop-up");
                            }
                            else System.Console.WriteLine("Button [OK] on Delete Attachment pop-up is NOT found");
                        }
                        else System.Console.WriteLine("Can't click button/icon [X] to remove attachment");
                    }
                    else System.Console.WriteLine("Button/icon [X] is NOT found");
                    break;

                case "ritm":
                    //Click Edit button
                    bt = Button_Edit_Delete_Attachment();
                    flag = bt.Existed;
                    if (flag)
                    {
                        flag = bt.Click();
                        if (flag)
                        {
                            //Click X icon
                            bt = Button_Remove_Attachment(filename, page);
                            flag = bt.Existed;
                            if (flag)
                            {
                                flag = bt.Click();
                                if (flag)
                                {
                                    //Wait
                                    WaitLoading();
                                }
                                else System.Console.WriteLine("Can't click button/icon [X] to remove attachment");
                            }
                            else System.Console.WriteLine("Button/icon [X] is NOT found");
                        }
                        else System.Console.WriteLine("Can't click button [Edit]");
                    }
                    else System.Console.WriteLine("Button [Edit] is NOT found");
                    break;
            }

            //After remove the attachment, recheck the attachment list with attachment name
            //Reuse /VerifyAttachmentFile/ and DON'T expect to see the attachment again
            if (VerifyAttachmentFile(filename, page))
                return flag = false;

            return flag;
        }

        //-------------------------------------------------------------------------------------------------

        public bool Verify_Page_Open(string breadcrumbs)
        {
            bool flag = true;

            string runtime = GBreadcrumbs();
            Console.WriteLine("***[Info]: Runtime [" + runtime + "]");
            if (!runtime.Trim().ToLower().Equals(breadcrumbs.Trim().ToLower()))
                flag = false;
            return flag;
        }

        //-------------------------------------------------------------------------------------------------

        public snoelement GHeaderMenu(string item)
        {
            snoelement ele = null;
            string define = "//span[text()='" + item + "']";
            By by = By.XPath(define);
            ele = (snoelement)Base.SNGObject(item, "element", by);
            return ele;
        }

        //-------------------------------------------------------------------------------------------------

        public snoelement GHeaderItemMenu(string item)
        {
            snoelement ele = null;
            string define = "ul[class^='nav nav-pills'] li a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("item menu list", by);
            foreach (Auto.oelement e in list.MyList)
            {
                if (e.MyText.Trim().ToLower().Equals(item.Trim().ToLower()))
                {
                    ele = new snoelement("item", Base.Driver, e.MyElement);
                    break;
                }
            }
            return ele;
        }

        //-------------------------------------------------------------------------------------------------

        public snoelement GLeftMenu(string item)
        {
            snoelement ele = null;
            string define = "div[class='list-group'] a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("left menu list", by);
            foreach (Auto.oelement e in list.MyList)
            {
                if (e.MyText.Trim().ToLower().Equals(item.Trim().ToLower()))
                {
                    ele = new snoelement("item", Base.Driver, e.MyElement);
                    break;
                }
            }
            return ele;
        }

        //--------------------------------------------------------------------------------------------------
        public bool Action_Click(string item)
        {
            bool flag = false;
            snoelement bt = Button_ActionMenu();
            flag = bt.Existed;
            if (flag)
            {
                flag = bt.Click();
                Thread.Sleep(1000);
                if (flag)
                {
                    string define = "li[class='ng-scope']>a[role='link']>span[ng-bind-html]";
                    By by = By.CssSelector(define);
                    snoelements list = Base.SNGObjects("action list", by);
                    foreach (Auto.oelement e in list.MyList)
                    {
                        if (e.MyText.Trim().ToLower().Equals(item.Trim().ToLower()))
                        {
                            flag = e.Existed;
                            if (flag)
                            {
                                flag = e.Click();
                            }
                            break;
                        }
                    }
                }
            }

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndVerifyRow(string searchValue, string findConditions, [Optional] bool expectedNotFound)
        {
            bool flag = true;
            flag = Search(searchValue);
            if (flag)
            {
                snotable table = Table_List();
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;
                    Auto.oelements rows = table.RowsList;

                    if (columns.Count > 0)
                    {
                        if (rows.MyList.Count > 0)
                        {
                            int rowIndex = table.RowIndex(findConditions, columns, rows);
                            if (rowIndex >= 0)
                            {
                                if (!expectedNotFound)
                                    Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ")");
                                else
                                {
                                    flag = false;
                                    Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ") - Expected: NOT FOUND.");
                                }
                            }
                            else
                            {
                                if (!expectedNotFound)
                                {
                                    flag = false;
                                    Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ")");
                                }
                                else
                                    Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ") as Expected.");
                            }
                        }
                        else
                        {
                            if (!expectedNotFound)
                            {
                                flag = false;
                                System.Console.WriteLine("-*-[Info]: Not have any row(s).");
                            }
                            else
                            {
                                System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                            }
                        }
                    }
                    else
                    {
                        flag = false;
                        System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                    }
                }
                else 
                {
                    if (!expectedNotFound)
                        System.Console.WriteLine("-*-[Info]: Cannot get table list.");
                    else 
                    {
                        flag = true;
                    }
                } 
            }
            else System.Console.WriteLine("-*-[Info]: Error when search item.");

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SearchAndOpen(string searchValue, string findConditions, string columnClick)
        {
            bool flag = true;

            flag = Search(searchValue);
            if (flag)
            {
                snotable table = Table_List();
                flag = table.Existed;
                if (flag)
                {
                    List<string> columns = table.ColumnHeader;
                    Auto.oelements rows = table.RowsList;
                    if (columns.Count > 0 && rows.MyList.Count > 0)
                    {
                        int rowIndex = table.RowIndex(findConditions, columns, rows);
                        if (rowIndex >= 0)
                        {
                            int columnIndex = table.ColumnIndex(columnClick, columns);
                            if (columnIndex >= 0)
                            {
                                flag = table.CellClick(rowIndex, columnIndex, null, columns, rows);
                                if (!flag)
                                    Console.WriteLine("***[Info]: Error when click on cell.");
                            }
                            else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                        }
                        else
                        {
                            flag = false;
                            Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                        }
                    }
                    else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
                }
                else System.Console.WriteLine("-*-[Info]: Cannot get table list.");
            }
            else System.Console.WriteLine("-*-[Info]: Error when search item.");

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Open(string findConditions, string columnClick)
        {
            bool flag = true;
            snotable table = Table_List();
            flag = table.Existed;
            if (flag)
            {
                List<string> columns = table.ColumnHeader;
                Auto.oelements rows = table.RowsList;
                if (columns.Count > 0 && rows.MyList.Count > 0)
                {
                    int rowIndex = table.RowIndex(findConditions, columns, rows);
                    if (rowIndex >= 0)
                    {
                        int columnIndex = table.ColumnIndex(columnClick, columns);
                        if (columnIndex >= 0)
                        {
                            flag = table.CellClick(rowIndex, columnIndex, null, columns, rows);
                            if (!flag)
                                Console.WriteLine("***[Info]: Error when click on cell.");
                        }
                        else Console.WriteLine("***[Info]: NOT FOUND column (" + columnClick + ")");
                    }
                    else
                    {
                        flag = false;
                        Console.WriteLine("***[Info]: NOT FOUND with conditions:(" + findConditions + ")");
                    }
                }
                else System.Console.WriteLine("-*-[Info]: Not have any columns or rows.");
            }
            else System.Console.WriteLine("-*-[Info]: Cannot get table list.");

            return flag;
        }
        //----------------------------------------------------------------------------------------------------------
        /// <summary>
        ///
        /// </summary>
        /// <param name="group">Left navigation bars in 'Search' page. Input should be: 'knowledge', 'catalog', 'issue', request </param>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool OpenItemFromGlobalSearchResult(string group, string item)
        {
            bool flag = true;
            snoelement eG = GSearchResultGroup(group);
            flag = eG.Existed;
            if (flag)
            {
                string temp = eG.MyElement.GetAttribute("class");
                if (temp.Trim().ToLower().Contains("ng-scope"))
                {
                    flag = eG.Click();
                    if (!flag) System.Console.WriteLine("-*-[ERROR]: Error when click on group.");
                    else
                        WaitLoading();
                }
                snoelement eI = GSearchResultItem(item);
                flag = eI.Existed;
                if (flag)
                {
                    flag = eI.Click();
                    if (!flag) System.Console.WriteLine("-*-[ERROR]: Error when click on item.");
                }
                else System.Console.WriteLine("-*-[ERROR]: Cannot get (" + item + ") item from result.");

            }
            else System.Console.WriteLine("-*-[ERROR]: Cannot get (" + group + ") group from result.");
            return flag;
        }

        //----------------------------------------------------------------------------------------------------------
        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Open_Item_From_Knowledge_Search_Result(string item, [Optional] bool flagFC)
        {
            bool flag = true;

            snoelement eI = GSearchResultItem(item, 0, "knowledge", flagFC);
            flag = eI.Existed;
            if (flag)
            {
                flag = eI.Click();
                if (!flag) System.Console.WriteLine("-*-[ERROR]: Error when click on item.");
            }
            else System.Console.WriteLine("-*-[ERROR]: Cannot get (" + item + ") item from result.");

            return flag;
        }

        //----------------------------------------------------------------------------------------------------------
        //Use this method ONLY when working in Knowledge landing page
        public bool Search_KA(string condition)
        {
            bool flag = true;

            snotextbox textbox = Textbox_Knowledge_Search();
            flag = textbox.Existed;
            if (flag)
            {
                flag = textbox.SetText(condition, true);
                if (flag) WaitLoading();
            }
            else Console.WriteLine("***[Info]:Cannot get textbox [Knowledge Search].");
            return flag;
        }

        //----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Use this method to click one of 4 links (Profile, Logout, ITIL Portal, End Impersonation) within the User Profile icon on topleft
        /// This method will do 3 actions: 1. Click User Profile icon 2. Verify existing list 3. Click desired item
        /// </summary>
        /// <param name="link">Input as: 'Profile', 'Logout', 'ITIL Portal' or 'End Impersonation'</param>
        /// <returns></returns>
        public bool User_Profile_link_Click(string link)
        {
            bool flag = true;

            snoelement ele = UserFullName();

            flag = ele.Existed;

            if (flag)
            {
                // 1.Click User Profile icon
                flag = ele.Click();

                if (flag)
                {
                    //WaitLoading();
                    ele = Profile_Dropdown_Items(link);
                    flag = ele.Existed;

                    //2. Verify existing item
                    if (flag)
                    {
                        //3. Click desired item
                        flag = ele.Click();

                        if (flag)
                        {
                            //Thread.Sleep(2000);
                            WaitLoading();
                        }
                        else Console.WriteLine("The [" + link + "] element is NOT clickable");
                    }
                    else Console.WriteLine("The [" + link + "] element is NOT found");
                }
                else Console.WriteLine("The [User Full Name] element is NOT clickable");
            }
            else Console.WriteLine("The [User Full Name] element is NOT found");

            return flag;
        }

        //----------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Use this method to go back to the original User and quit impersonating the current user.
        /// This method will do 3 actions: 1. Click the End Impersonation link 2. Verify original User is reloaded [Optional]3. Go back to Itil view
        /// </summary>
        /// <param name="impersonator">The original User that we want to go back</param>
        /// <param name="goToITIL">put 'true' if wanting to do one more step of going to the ITIL view</param>
        /// <returns></returns>
        public bool End_Impersonating(string impersonator, [Optional] bool goToITIL)
        {
            bool flag = true;

            //1. Click the End Impersonation link
            flag = User_Profile_link_Click("End Impersonation");

            if (flag)
            {
                //2.Verify original User is reloaded
                snoelement ele = UserFullName();
                flag = ele.Existed;
                if (flag)
                {
                    Console.WriteLine("Run time:(" + ele.MyText.Trim() + ")");
                    if (ele.MyText.Trim().ToLower() != impersonator.Trim().ToLower())
                    {
                        flag = false; Console.WriteLine("Invalid Impesonator name");
                    }
                }
                else Console.WriteLine("Cannot get control user Impesonator full name.");
            }
            //[Optional]3. Go back to Itil view
            if (goToITIL)
            {
                flag = User_Profile_link_Click("ITIL Portal");
                if (flag)
                {
                    Thread.Sleep(2000);
                }
                else Console.WriteLine("The [ITIL Portal] link is NOT clickable.");
            }

            return flag;
        }

        public bool Search_Open_CatalogItem(string item)
        {
            bool flag = true;
            bool flagFC = false;

            string titem = string.Empty;

            if (item.Contains("@@"))
            {
                titem = item.Replace("@@", "");
                flagFC = true;
            }    
            else
                titem = item;

            snotextbox textbox = Textbox_Catalog_Item_Search();
            flag = textbox.Existed;
            if (flag)
            {
                flag = textbox.SetText(titem, true);
                if (flag)
                {
                    WaitLoading();
                    snoelement eI = GSearchResultItem(titem, 0, "catalog", flagFC);
                    flag = eI.Existed;
                    if (flag)
                    {
                        flag = eI.Click();
                        if (!flag) System.Console.WriteLine("-*-[ERROR]: Error when click on item.");
                    }
                    else System.Console.WriteLine("-*-[ERROR]: Cannot get (" + titem + ") item from result.");
                }
            }
            else Console.WriteLine("***[Info]:Cannot get textbox [Catalog Search].");
            return flag;
        }

        public bool TKP_Validate_ItemStage(string stage, [Optional] bool noVerifyColor)
        {
            bool flag = true;

            snoelements eles = null;
            string define = "div[class='stage-on']";

            //Get & click Expand button
            snoelement expand = (snoelement)Base.SNGObject("Expand", "element", By.CssSelector("img[ng-if]"));
            if (expand != null && expand.MyElement.GetAttribute("ng-if") == "!data.expand")
            {
                expand.Click();
                WaitLoading();
            }
            eles = Base.SNGObjects("Stage", By.CssSelector(define));
            if (eles != null && eles.MyList.Count > 0)
            {
                string[] array = null;

                if (stage.Contains(";"))
                {
                    array = stage.Split(';');
                }
                else array = new string[] { stage };

                foreach (string sta in array)
                {
                    bool flagCheck;

                    flagCheck = VItemStage(sta, eles, noVerifyColor);

                    if (!flagCheck)
                    {
                        System.Console.WriteLine("***Failed: Not found stage:" + sta);
                        if (flag)
                            flag = false;
                    }
                    else System.Console.WriteLine("***Passed: Found stage:" + sta);
                }

            }

            return flag;
        }

        public snoelement GLinkByText(string item)
        {
            snoelement ele = null;
            string temp = item;
            string define = string.Empty;
            if (item.Contains("@@")) 
            {
                temp = temp.Replace("@@", "");
                define = "//a[contains(text(),'" + temp + "')]";
            }
            else define = "//a[text()='" + temp + "']";
            
            By by = By.XPath(define);
            ele = (snoelement)Base.SNGObject("Link", "element", by);
            return ele;
        }

        public bool OpenMyApprovalItem(string item) 
        {
            bool flag = false;
            string define = "ul[class][role='menu'][aria-label] a>span[class^='block']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.MyText.Trim().ToLower().Contains(item.ToLower())) 
                {
                    Console.WriteLine("***[Info]: Found item [" + ele.MyText + "]");
                    flag = ele.Click();
                    break;
                }
            }

            if(!flag)
                Console.WriteLine("***[Info]: NOT found item [" + item + "]");
            return flag;
        }

        public snoelements Get_Most_Popular_Catalog_Items()
        {
            string define = "div[class='panel panel-default item-card'] h2";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by);
            return list;
        }

        public bool Verify_Catalog_Left_Menu_Items(string exp) 
        {
            bool flag = true;

            string temp = Get_Catalog_Left_Menu_Items();
            Console.WriteLine("Runtime value");
            Console.WriteLine(temp);
            Console.WriteLine("-------------------------------------------------------------");
            exp = exp.Replace("\n", "|");
            string[] arrE = exp.Split('|');

            foreach (string item in arrE)
            {
                if (!temp.ToLower().Contains(item.ToLower().Trim()))
                {
                    if (flag)
                        flag = false;
                    System.Console.WriteLine("***FAILED: Not found item [" + item + "].");
                }
                else System.Console.WriteLine("***Passed: Found item [" + item + "].");
            }

            return flag;
        }

        private string Get_Catalog_Left_Menu_Items() 
        {
            string result = string.Empty;

            string define = "div[role='listitem'][level='0'], div[role='listitem']:not([level])";
           
            snoelements topItems = Base.SNGObjects("list", By.CssSelector(define));
            foreach (Auto.oelement topItem in topItems.MyList)
            {
                string temp = topItem.MyText.Replace(@"\r\n?|\n", "").Trim();
                temp = Regex.Replace(temp, @"\d", "");
                result = result + "*" + temp + "\n";

                IWebElement expand = null;
                try
                {
                    expand = topItem.MyElement.FindElement(By.CssSelector("a i[aria-label^='Expand']"));
                }
                catch { expand = null; }

                if (expand != null)
                {
                    expand.Click();
                    Thread.Sleep(5000);
                    string childItem = GetChildItems(topItem, 0);
                    result = result + childItem;
                }
            }

            while (result.EndsWith("*"))
            {
                result = result.Substring(0, result.Length - 1);
            }

            if (result.EndsWith("\n"))
            {
                result = result.Substring(0, result.Length - 1);
            }
            
            return result;
        }

        private string GetChildItems(Auto.oelement parent, int curIndex)
        {
            string result = string.Empty;
            string define = "div[role='listitem'][level^='level']";
            snoelement pr = new snoelement("parent", Base.Driver, parent.MyElement);
            snoelements cItems = Base.SNGObjects("clist", By.CssSelector(define), pr);
            if (cItems.Count > 0) 
            {
                string str = "*";
                for (int i = 0; i <= curIndex; i++)
                {
                    str = " " + str + "*";
                }
                foreach (Auto.oelement item in cItems.MyList)
                {
                    string temp = item.MyText.Replace(@"\r\n?|\n", "").Trim();
                    temp = Regex.Replace(temp, @"\d", "");
                    result = result + str + temp + "\n";

                    IWebElement expand = null;
                    try
                    {
                        expand = item.MyElement.FindElement(By.CssSelector("a i[aria-label^='Expand']"));
                    }
                    catch { expand = null; }

                    if (expand != null)
                    {
                        expand.Click();
                        Thread.Sleep(3000);

                        string childItem = GetChildItems(item, curIndex + 1);
                        result = result + childItem;
                    }
                }
            }       
            return result;
        }

        public bool Verify_Related_Link(string exp) 
        {
            bool flag = true;

            string define = "a[ng-if*='rl.type']";
            snoelements list = Base.SNGObjects("list", By.CssSelector(define));

            string[] arr = null;
            if (exp.Contains("|"))
                arr = exp.Split('|');
            else
                arr = new string[] { exp };

            foreach (string link in arr) 
            {
                bool flagF = false;

                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.Trim().ToLower().Contains(link.Trim().ToLower()))
                    {
                        flagF = true;
                        break;
                    }
                }
                if (flagF)
                    Console.WriteLine("Found link: [" + link + "]");
                else 
                {
                    if (flag)
                        flag = false;
                    Console.WriteLine("NOT FOUND link: [" + link + "]");
                }
            }

            return flag;
        }

        public bool VerifyRow(string findConditions, [Optional] bool expectedNotFound)
        {
            bool flag = true;
            snotable table = Table_List();
            flag = table.Existed;
            if (flag)
            {
                List<string> columns = table.ColumnHeader;
                Auto.oelements rows = table.RowsList;

                if (columns.Count > 0)
                {
                    if (rows.MyList.Count > 0)
                    {
                        int rowIndex = table.RowIndex(findConditions, columns, rows);
                        if (rowIndex >= 0)
                        {
                            if (!expectedNotFound)
                                Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ")");
                            else
                            {
                                flag = false;
                                Console.WriteLine("***[Info]: Found row with conditions:(" + findConditions + ") - Expected: NOT FOUND.");
                            }
                        }
                        else
                        {
                            if (!expectedNotFound)
                            {
                                flag = false;
                                Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ")");
                            }
                            else
                                Console.WriteLine("***[Info]: NOT FOUND row with conditions:(" + findConditions + ") as Expected.");
                        }
                    }
                    else
                    {
                        if (!expectedNotFound)
                        {
                            flag = false;
                            System.Console.WriteLine("-*-[Info]: Not have any row(s).");
                        }
                        else
                        {
                            System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                        }
                    }
                }
                else
                {
                    flag = false;
                    System.Console.WriteLine("-*-[Info]: Not have any columns of table.");
                }
            }
            else
            {
                if (!expectedNotFound)
                    System.Console.WriteLine("-*-[Info]: Cannot get table list.");
                else
                {
                    flag = true;
                }
            }

            return flag;
        }


        public bool Verify_Check_Status_Left_Menu(string exp)
        {
            bool flag = true;

            string define = "a[class^='list-group-item']";
            snoelements list = Base.SNGObjects("list", By.CssSelector(define));

            string[] arr = null;
            if (exp.Contains("|"))
                arr = exp.Split('|');
            else
                arr = new string[] { exp };

            foreach (string link in arr)
            {
                bool flagF = false;

                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.Trim().ToLower().Contains(link.Trim().ToLower()))
                    {
                        flagF = true;
                        break;
                    }
                }
                if (flagF)
                    Console.WriteLine("Found: [" + link + "]");
                else
                {
                    if (flag)
                        flag = false;
                    Console.WriteLine("NOT FOUND: [" + link + "]");
                }
            }

            return flag;
        }

        public bool Verify_Check_Status_Tab_Menu(string exp)
        {
            bool flag = true;

            string define = "a[href^='javascript'][class='ng-binding']";
            snoelements list = Base.SNGObjects("list", By.CssSelector(define));

            string[] arr = null;
            if (exp.Contains("|"))
                arr = exp.Split('|');
            else
                arr = new string[] { exp };

            foreach (string link in arr)
            {
                bool flagF = false;

                foreach (Auto.oelement ele in list.MyList)
                {
                    if (ele.MyText.Trim().ToLower().Contains(link.Trim().ToLower()))
                    {
                        flagF = true;
                        break;
                    }
                }
                if (flagF)
                    Console.WriteLine("Found: [" + link + "]");
                else
                {
                    if (flag)
                        flag = false;
                    Console.WriteLine("NOT FOUND: [" + link + "]");
                }
            }

            return flag;
        }

        public bool Verify_Approval_Form_Field(string field, string value) 
        {
            bool flag = false;
            string define = "div[class='m-b break-word ng-scope'], div[class='m-b-xs ng-scope']";
            snoelements list = Base.SNGObjects(field, By.CssSelector(define));
            foreach (Auto.oelement ele in list.MyList) 
            {
                string label = ele.MyElement.FindElement(By.CssSelector("label")).Text;
                string data = ele.MyElement.FindElement(By.CssSelector("div")).Text;
                if (label.Trim().ToLower() == field.Trim().ToLower()) 
                {
                    if (data.Trim().ToLower() == value.Trim().ToLower()) 
                    {
                        flag = true;
                        break;
                    }
                }
            }         
            return flag;
        }

        public bool Select_Approvals_Item(string ritm) 
        {
            bool flag = false;

            string define = "a[ng-if*='approval']";
            snoelements list = Base.SNGObjects("list", By.CssSelector(define));
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.MyText.Trim().ToLower().Contains(ritm.Trim().ToLower())) 
                {
                    flag = ele.Click();
                    break;
                }
            }
            return flag;
        }

        public bool Verify_Activity(string value)
        {
            bool flag = false;
            string define = "div[class^='timeline-panel']:not([class='timeline-panel'])";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Activity list", by);
            int count = 0;
            foreach (Auto.oelement e in list.MyList)
            {
                snoelement parent = new snoelement("parent", Base.Driver, e.MyElement);
                by = By.CssSelector("div[class*='heading']>div");
                snoelement ele = (snoelement)Base.SNGObject("created by", "element", by, parent);
                string createdby = ele.MyText.Trim().ToLower();

                string df = "div[class*='body']";
                by = By.CssSelector(df);

                ele = (snoelement)Base.SNGObject("records", "element", by, parent);
                string records = ele.MyElement.Text.Trim().Replace("\r\n", "|").ToLower();
                string temp = createdby + "|" + records;

                System.Console.WriteLine("-*-Run time activity value[" + count + "]:(" + temp + ")");

                if (value.Contains("@@"))
                {
                    string str = value;
                    str = str.Replace("@@", "");
                    if (temp.Contains(str.ToLower()))
                    {
                        flag = true;
                        System.Console.WriteLine("-*-[Passed]: Found activity value (" + value + ")");
                        break;
                    }
                    else
                        count++;
                }
                else
                {
                    if (temp.Equals(value.ToLower()))
                    {
                        flag = true;
                        System.Console.WriteLine("-*-[Passed]: Found activity value (" + value + ")");
                        break;
                    }
                    else
                        count++;
                }

            }
            return flag;
        }

        public bool KN_Rate_Star(string ratestar)
        {
            bool flag = false;
            string define = "div[ng-if='c.showRatings'] i[value='" + ratestar + "']";
            By by = By.CssSelector(define);
            snoelement star =(snoelement)Base.SNGObject("star", "element", by);
            flag = star.Click(true);
            return flag;
        }

        public bool Verify_Header_Menu(string items)
        {
            bool flag = false;

            string define = "ul[aria-label='Header menu']>li[class$='scope header-menu-item']>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("item menu list", by);

            flag = list.HaveItemsInlist(items, "|");

            return flag;
        }

        public bool Verify_Footer(string items)
        {
            bool flag = false;

            string define = "ul[class$='footer_item']>a";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("item menu list", by);

            flag = list.HaveItemsInlist(items, "|");

            return flag;
        }
        #endregion Public methods

        #region Controls

        #region Textbox

        public snotextbox Textbox_Global_Search()
        {
            snotextbox textbox = null;
            string define = ".//input[@placeholder='Search the self service portal...']";
            By by = By.XPath(define);
            textbox = (snotextbox)Base.SNGObject("Global search", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_Catalog_Item_Search()
        {
            snotextbox textbox = null;
            string define = "input[placeholder='Search'], input[placeholder='Search (minimum 3 characters)']";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Catalog Item search", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_Knowledge_Search()
        {
            snotextbox textbox = null;
            string define = ".//input[@placeholder='Search (minimum 3 characters)']";
            By by = By.XPath(define);
            textbox = (snotextbox)Base.SNGObject("Item search", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_Check_status_Search()
        {
            snotextbox textbox = null;
            string define = ".//input[@placeholder='Keyword Search']";
            By by = By.XPath(define);
            textbox = (snotextbox)Base.SNGObject("Item search", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_FirstName()
        {
            snotextbox textbox = null;
            string define = "First Name:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_LastName()
        {
            snotextbox textbox = null;
            string define = "Last Name:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_EmailAddress()
        {
            snotextbox textbox = null;
            string define = "Email Address:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_UserID()
        {
            snotextbox textbox = null;
            string define = "User ID:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_TelephoneNumber()
        {
            snotextbox textbox = null;
            string define = "Telephone Number:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_BriefDescription()
        {
            snotextbox textbox = null;
            string define = "Brief Description";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox Textbox_Number()
        {
            snotextbox textbox = null;
            string define = "Number";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }

        public snotextbox ApprovalComment()
        {
            snotextbox textbox = null;
            string define = "#xpInput";
            By by = By.CssSelector(define);
            textbox = (snotextbox)Base.SNGObject("Comment", "textbox", by);
            return textbox;
        }

        public snotextbox Textbox_REQIT_Number()
        {
            snotextbox textbox = null;
            string define = "Number";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        #endregion Textbox

        #region Textarea

        public snotextarea Textarea_DetailedInformation()
        {
            snotextarea textarea = null;
            string define = "Detailed Information";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_Approval_Active_Stream_Comment()
        {
            snotextarea textarea = null;
            string define = "textarea[id='post-input']";
            By by = By.CssSelector(define);
            textarea = (snotextarea)Base.SNGObject("Comment", "textarea", by);
            return textarea;
        }

        public snotextarea Textarea_Knowledge_Add_Comment()
        {
            snotextarea textarea = null;
            string define = "textarea[id='comment']";
            By by = By.CssSelector(define);
            textarea = (snotextarea)Base.SNGObject("Comment", "textarea", by);
            return textarea;
        }

        public snotextarea Textarea_Comments()
        {
            snotextarea textarea = null;
            string define = "textarea[aria-label='Comments']";
            By by = By.CssSelector(define);
            textarea = (snotextarea)Base.SNGObject("Comment", "textarea", by);
            return textarea;
        }
        #endregion Textarea

        #region Lookup

        public snolookup Lookup_Location()
        {
            snolookup lookup = null;
            string define = "Location:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_UserName()
        {
            snolookup lookup = null;
            string define = "User name:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_RequestedFor()
        {
            snolookup lookup = null;
            string define = "Requested for:";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Delegate_User()
        {
            snolookup lookup = null;
            string define = "User";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }

        public snolookup Lookup_Delegate()
        {
            snolookup lookup = null;
            string define = "Delegate";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                lookup = (snolookup)obj;
            else
                lookup = new snolookup(define, Base.Driver, null);
            return lookup;
        }
        #endregion Lookup

        #region Combobox

        public snocombobox Combobox_PriorityAssessment()
        {
            snocombobox combo = null;
            string define = "Priority Assessment";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        //---------------------------------------------------------------------------------------------
        //This is combobox 'What kind of issue are you having?' on GUI
        public snocombobox Combobox_Type_1()
        {
            System.Console.WriteLine("Validating combobox 'What kind of issue are you having?' on GUI");

            snocombobox combo = null;
            string define = "What kind of issue are you having?";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        //---------------------------------------------------------------------------------------------
        //This is combobox 'What is the nature of the issue?' on GUI
        public snocombobox Combobox_Type_2()
        {
            System.Console.WriteLine("Validating combobox 'What is the nature of the issue?' on GUI");

            snocombobox combo = null;
            string define = "What is the nature of the issue?";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Approval()
        {
            snocombobox combo = null;
            string define = "Approval";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Stage()
        {
            snocombobox combo = null;
            string define = "Stage";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }

        public snocombobox Combobox_Rating()
        {
            snocombobox combo = null;
            
            string define = "span[id='select2-chosen-6']";
            By by = By.CssSelector(define);
            combo = (snocombobox)Base.SNGObject("Rating", "combobox", by);
           
            return combo;
        }
        #endregion Combobox

        #region Button

        /// <summary>
        /// This is the toggle button that can be found in pages: INC details, RITM approval detail, Change approval detail
        /// </summary>
        /// <param name="name">Text of the toggle. It could be 'Options' or 'Change Request'</param>
        /// <returns></returns>
        public snobutton Button_Toggle(string name)
        {
            snobutton bt = null;
            string define = "button[class*='" + name.ToLower() + "']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject(name, "button", by);
            return bt;
        }

        //-------------------------------------------------------------------------------
        public snoelement Button_ActionMenu()
        {
            snoelement bt = null;
            string define = "a[role='button'][aria-controls]";
            By by = By.CssSelector(define);
            bt = (snoelement)Base.SNGObject("Action Menu", "element", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <param name="index">This parameter is identified from top to bottom</param>
        public snobutton Button_Add_Attachments([Optional] int index)
        {
            //snoelements bts = null;
            snobutton bt = null;

            string define = "button[class*='attachment']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Add attachment", "button", by);

            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snobutton Button_Submit()
        {
            snobutton bt = null;
            //string define = "//button[text()='Submit']";
            string define = "button[name='submit'], button[id='submit']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Submit", "button", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snobutton Button_AddToCart()
        {
            snobutton bt = null;
            string define = "button[name='add_to_cart']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Add to cart", "button", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        //This button is only seen in INC and Catalog form
        public snobutton Button_OK_Delete_Attachment()
        {
            snobutton bt = null;
            string define = "//button[text()='OK']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("OK", "button", by);
            return bt;
        }

        public snobutton Button_OK()
        {
            snobutton bt = null;
            string define = "//button[text()='OK']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("OK", "button", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snobutton Button_Cancel_Delete_Attachment()
        {
            snobutton bt = null;
            string define = "//button[text()='Cancel']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Cancel", "button", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        //This button is only seen in RITM form
        public snobutton Button_Edit_Delete_Attachment()
        {
            snobutton bt = null;
            string define = "//div[starts-with(@class,'sp-attachment-manager')]//button[text()='Edit']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Edit", "button", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snobutton Button_Resolve()
        {
            snobutton bt = null;
            string define = "button[name='resolve']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Resolve", "button", by);
            return bt;
        }
        
        //-----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// The button to remove attachment is appearing on pages: INC,Catalog and RITM
        /// INC and Catalog share the same element while RITM does not
        /// </summary>
        /// <param name="page">Page has remove-attachment button, input as: "incident","catalog" or "ritm"</param>
        /// <returns></returns>
        public snobutton Button_Remove_Attachment(string filename, string page)
        {
            snobutton bt = null;
            string define = String.Empty;

            switch (page)
            {
                case "incident":
                case "catalog":
                    define = "//a[contains(text(),'" + filename + "')]//parent::div//following-sibling::div//button[@title='Delete']";
                    break;

                case "ritm":
                    define = "//a[text()='" + filename + "']//following-sibling::button[contains(@class,'delete-attachment')]";
                    break;
            }

            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Remove attachment", "button", by);
            return bt;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Approve()
        {
            snobutton bt = null;
            string define = "//button[text()='Approve']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Approve", "button", by);
            return bt;
        }

        public snobutton Button_Reject()
        {
            snobutton bt = null;
            string define = "//button[text()='Reject']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Reject", "button", by);
            return bt;
        }

        public snobutton Button_Browse_Catalog()
        {
            snobutton bt = null;
            string define = "//a[text()='Browse Catalog']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Browse Catalog", "button", by);
            return bt;
        }

        public snobutton Button_CancelRequest()
        {
            snobutton bt = null;
            string define = "//button[text()='Cancel Request']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Cancel Request", "button", by);
            return bt;
        }

        public snobutton Button_Save()
        {
            snobutton bt = null;
            string define = "//button[contains(text(),'Save')]";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Save", "button", by);
            return bt;
        }

        public snobutton Button_Send()
        {
            snobutton bt = null;
            string define = "input[type='submit']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Send", "button", by);
            return bt;
        }

        public snobutton Button_New()
        {
            snobutton bt = null;
            string define = "button[name='new']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("New", "button", by);
            return bt;
        }

        public snobutton Button_Yes()
        {
            snobutton bt = null;
            string define = "//button[text()='Yes']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("Yes", "button", by);
            return bt;
        }

        public snobutton Button_GetStarted()
        {
            snobutton bt = null;
            string define = "//button[text()='Get Started']";
            By by = By.XPath(define);
            bt = (snobutton)Base.SNGObject("OK", "button", by);
            return bt;
        }
        #endregion Button

        #region Checkbox

        public snocheckbox Checkbox_Approvals()
        {
            snocheckbox cbox = null;
            string define = "Approvals";
            object obj = Base.GControlByLabelServicePortal(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Assignments()
        {
            snocheckbox cbox = null;
            string define = "Assignments";
            object obj = Base.GControlByLabelServicePortal(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_CCNotifications()
        {
            snocheckbox cbox = null;
            string define = "CC notifications";
            object obj = Base.GControlByLabelServicePortal(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_MeetingInvitations()
        {
            snocheckbox cbox = null;
            string define = "Meeting invitations";
            object obj = Base.GControlByLabelServicePortal(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }
        #endregion End - Checkbox

        #region Table

        public snotable Table_List([Optional] bool expectedNotFound)
        {
            snotable table = null;
            By by = By.CssSelector(tabledefine);
            if (!expectedNotFound)
                table = new snotable("Portal table", Base.Driver, by, null, null, false, colDefine, rowDefine, cellDefine, null, "Sort by");
            else
                table = new snotable("Portal table", Base.Driver, by, null, null, true, colDefine, rowDefine, cellDefine, null, "Sort by");

            return table;
        }

        #endregion Table

        #region Datetime
        public snodatetime Datetime_Delegate_Starts()
        {
            snodatetime datetime = null;
            string define = "Starts";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_Delegate_Ends()
        {
            snodatetime datetime = null;
            string define = "Ends";
            object obj = Base.GControlByLabelServicePortal(define, null);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        #endregion End - Datetime

        #region Others

        public snoelement UserFullName()
        {
            snoelement ele = null;
            string define = "#profile-dropdown > span.visible-lg-inline.ng-binding";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("User full name", "element", by);
            return ele;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement ClearSearch()
        {
            snoelement ele = null;
            string define = "a[class='pull-right']";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Clear search", "element", by);
            return ele;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        //These elements will appear ONLY after clicking Profile name on top right
        public snoelement Profile_Dropdown_Items(string item)
        {
            snoelements eles = null;
            snoelement ele = null;

            string define = "li[class='dropdown hidden-xs open'] a";
            By by = By.CssSelector(define);
            eles = Base.SNGObjects("Profile Dropdown items", by);

            foreach (Auto.oelement e in eles.MyList)
            {
                if (e.MyText.Trim().ToLower().Contains(item.Trim().ToLower()))
                {
                    ele = new snoelement("Item", Base.Driver, e.MyElement);
                    ele.Highlight();
                    break;
                }
            }
            return ele;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //Use this element ONLY on Knowledge search page
        public snoelement No_Article()
        {
            snoelement ele = null;

            string define = "div[class='no-article']";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("No Article found", "element", by);

            return ele;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snoelement TKP_Request_Number()
        {
            snoelement ele = null;
            string define = "div > div.panel-body > div > div > div:nth-child(1) > span > div";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Request number", "element", by);
            return ele;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snoelement TKP_Request_Item_Number()
        {
            snoelement ele = null;
            string define = "a > div > small > span > span, a > div > div";
            By by = By.CssSelector(define);
            snoelements eles = Base.SNGObjects("Request item number", by);
            foreach(Auto.oelement e in eles.MyList)
            {
                if (e.MyText.Trim().ToLower().Contains("ritm"))
                {
                    ele = new snoelement("item id", Base.Driver, e.MyElement);
                    break;
                }
            }
            return ele;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        public snoelement TKP_Request_Item_Name()
        {
            snoelement ele = null;
            string define = "div > a > div > small > span > span";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Request item name", "element", by);
            return ele;
        }

        public snoelement ThankYouMessage()
        {
            snoelement ele = null;
            string define = "div.panel-heading > div > h2";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Thank you message", "element", by);
            return ele;
        }

        public snoelement Price()
        {
            snoelement ele = null;
            string define = "div.form-group.m-b-xs.ng-binding.ng-scope, div.form-group.ng-binding.ng-scope";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("Price", "element", by);
            return ele;
        }

        public snoelement KB_Number()
        {
            snoelement ele = null;
            string define = "div[class='kb-number-info']>span";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("KB number", "element", by);
            return ele;
        }

        public snoelement FeedbackPageTitle()
        {
            snoelement ele = null;
            string define = "div>h3";
            By by = By.CssSelector(define);
            ele = (snoelement)Base.SNGObject("title", "element", by);
            return ele;
        }
        #endregion Others

        #endregion Controls

        public bool SP_Add_AttachmentFile(string filename)
        {
            bool flag = true;
            flag = Base.IsDllRegistered();
            Thread.Sleep(5000);
            if (flag)
            {
                flag = Base.WFileUploadSelect(filename);
            }
            
            return flag;
        }

        public bool SP_Verify_Attachment(string fileName, [Optional] bool noWait)
        {
            bool flag = false;
            string define = "div [ng-if*='attachment']> a:not([ng-if]), li[class^='attached-file']> a:not([ng-if]), .file-name.v-middle";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Attachment files", by, null, snobase.MainFrame, false, noWait);
            flag = list.HaveItemsInlist(fileName, ";", noWait);
            return flag;
        }
    }
}