﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class Problem : Itil
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public Problem(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Lookup

        

        #endregion End - Lookup

        #region Button

        public snobutton Button_ChangeRequestLookup()
        {
            snobutton bt = null;
            string define = "a[id^='lookup'][id$='rfc']";
            By by = By.CssSelector(define);
            bt = (snobutton)Base.SNGObject("Change Request Lookup Button", "button", by, null, snobase.MainFrame);
            return bt;
        }


        public snobutton Button_NewNoMainFrame()
        {
            snobutton bt = null;
            string define = "New";
            bt = Base.GButtonByText(define);
            return bt;
        }
         #endregion End- Button

        #region Datetime
        public snodatetime Datetime_RootCauseIdentified()
        {
            snodatetime datetime = null;
            string define = "Root cause identified";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }

        public snodatetime Datetime_WorkaroundIdentified()
        {
            snodatetime datetime = null;
            string define = "Workaround identified";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                datetime = (snodatetime)obj;
            else
                datetime = new snodatetime(define, Base.Driver, null);
            return datetime;
        }
        #endregion End - Datetime

        #region Textarea

        

        public snotextarea Textarea_Problem_Worknotes()
        {
            snotextarea ta = null;
            string define = "#activity-stream-textarea, #activity-stream-work_notes-textarea";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by, null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.MyElement.Displayed) 
                {
                    ta = new snotextarea("Problem work notes", Base.Driver, ele.MyElement);
                    break;
                }
            }
            
            return ta;
        }

        public snotextarea Textarea_Workaround()
        {
            snotextarea textarea = null;
            string define = "Workaround";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        public snotextarea Textarea_RootCauseDescription()
        {
            snotextarea textarea = null;
            string define = "Root cause description";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textarea = (snotextarea)obj;
            else
                textarea = new snotextarea(define, Base.Driver, null);
            return textarea;
        }

        #endregion End - Textarea

        #region Checkbox
        public snocheckbox Checkbox_RCADelivered()
        {
            snocheckbox cbox = null;
            string define = "RCA delivered";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_MissingConfigurationItem()
        {
            snocheckbox cbox = null;
            string define = "Missing configuration item";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_TaskPassedDueDate()
        {
            snocheckbox cbox = null;
            string define = "Task passed due date";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        #endregion Checkbox

        #region Textbox
        public snotextbox Textbox_RootCauseStatement()
        {
            snotextbox textbox = null;
            string define = "Root cause statement";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        #endregion End - Textbox

        #region Combobox
        public snocombobox Combobox_ClosureCode()
        {
            snocombobox combo = null;
            string define = "Closure code";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                combo = (snocombobox)obj;
            else
                combo = new snocombobox(define, Base.Driver, null);
            return combo;
        }
        #endregion End - Combobox

        #region Public methods

        public bool VerifyKnowledgeWasAttached(string article) 
        {
            bool flag = false;
            snoelement e = null;
            string define = ".obvious";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("Artical list", by, null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.Existed) 
                {
                    e = new snoelement("artical", Base.Driver, ele.MyElement);
                    break;
                }
            }
            if (e != null) 
            {
                string runtime = e.MyText.Trim();
                if (runtime.ToLower().Contains(article.Trim().ToLower()))
                {
                    flag = true;
                    Console.WriteLine("-*-[Info]: Found articale:" + article);
                }
                else Console.WriteLine("-*-[Info]: Rutime:" + runtime + "| Expected:" + article);
            }
            
            return flag;
        }


        #region Textbox
        public snotextbox Textbox_ProblemStatement()
        {
            snotextbox textbox = null;
            string define = "Problem statement";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                textbox = (snotextbox)obj;
            else
                textbox = new snotextbox(define, Base.Driver, null);
            return textbox;
        }
        #endregion End - Textbox

        #region Combobox
       
        public snocombobox Combobox_CauseCode()
        {
            snocombobox combobox = null;
            string define = "Cause code";
            object obj = Base.GControlByLabel(define, null, true);
            if (obj != null)
                combobox = (snocombobox)obj;
            else
                combobox = new snocombobox(define, Base.Driver, null);
            return combobox;
        }
        #endregion End - Combobox

        #region Button
        public snobutton Button_Edit()
        {
            snobutton bt = null;
            string define = "Edit...";
            bt = Base.GButtonByText(define);
            return bt;
        }

        #endregion End - Button

        #region Checkbox

        public snocheckbox Checkbox_RelatedP1P2_Incident()
        {
            snocheckbox cbox = null;
            string define = "Related P1/P2 incident ";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Knowledge()
        {
            snocheckbox cbox = null;
            string define = "Known error";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }

        public snocheckbox Checkbox_Create_Knowledge()
        {
            snocheckbox cbox = null;
            string define = "Create Knowledge";
            object obj = Base.GControlByLabel(define);
            if (obj != null)
                cbox = (snocheckbox)obj;
            else
                cbox = new snocheckbox(define, Base.Driver, null);
            return cbox;
        }
        #endregion End - Checkbox

        #endregion End - Public methods

        #region Methods
        public string GetMessageInfo_TicketNumber()
        {
            string message = string.Empty;
           
                
                    string define = ".outputmsg.outputmsg_info.notification.notification-info";
                    snoelements list = Base.SNGObjects("Message info", By.CssSelector(define), null, snobase.MainFrame);
                    if (list.MyList.Count > 0)
                    {
                        //bool flagF = true;
                        foreach (Auto.oelement e in list.MyList)
                        {
                            string value = e.MyText.Trim();
                            if (value.Contains(":"))
                            {
                                string[] msg = value.Split(':');
                                string temp = msg[1];
                                message = temp.Remove(0, temp.IndexOf("SUB"));

                                System.Console.WriteLine("***[Passed]: Found message info (" + value + ")");
                                System.Console.WriteLine("***[Passed]: Found ID : (" + message + ")");
                            }
                            else
                            {
                                System.Console.WriteLine("***[Failed]: Not found message contain (" + value + ")");
                            }

                        }
                    }

            return message;
        }
        #endregion End - Methods


    }
}
