﻿using System;
using OpenQA.Selenium;

namespace SNow
{
    public class ProblemTask : Itil
    {
        #region Variables

        private snobase _snobase = null;
        private string _name = string.Empty;

        #endregion End - Variables

        #region Constructor

        public ProblemTask(snobase obase, string name)
            : base(obase, name)
        {
            _snobase = obase;
            _name = name;
        }

        #endregion End - Constructor

        #region Button
        public snobutton Button_New()
        {
            snobutton bt = null;
            string define = "New";
            bt = Base.GButtonByText(define);
            return bt;
        }

        public snobutton Button_Edit()
        {
            snobutton bt = null;
            string define = "Edit...";
            bt = Base.GButtonByText(define);
            return bt;
        }
        #endregion End - Button

        public snotextarea Textarea_Problem_Task_Worknotes()
        {
            snotextarea ta = null;
            string define = "div[class*='form-group'][aria-hidden='false'] textarea[id^='activity-stream']";
            By by = By.CssSelector(define);
            snoelements list = Base.SNGObjects("list", by, null, snobase.MainFrame);
            foreach (Auto.oelement ele in list.MyList) 
            {
                if (ele.Existed)
                {
                    ta = new snotextarea("Problem task work notes", Base.Driver, ele.MyElement);
                    break;
                }
            }
            
            return ta;
        }

    }
}
