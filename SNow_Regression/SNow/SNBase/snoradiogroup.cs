﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SNow
{
    public class snoradiogroup : Auto.oelement
    {
        IWebDriver _driver = null;

        public snoradiogroup(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) 
        {
            _driver = driver;
        }
        public snoradiogroup(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) 
        {
            _driver = driver;
        }

        public bool MyReadOnly
        {
            get
            {
                snoradio radio = Items[0];
                radio.MoveToElement();
                string result = radio.IsReadOnly;
                if (result != null && (result.Trim().ToLower() == "readonly" || result.Trim().ToLower() == "true"))
                    return true;
                else
                    return false;
            }
        }

        public List<snoradio> Items
        {
            get
            {
                List<snoradio> list = new List<snoradio>();

                IWebElement pr = MyElement.FindElement(By.XPath(".."));
                string define = "label[class*='radio']:not([type='hidden'])";
                IReadOnlyCollection<IWebElement> eles = pr.FindElements(By.CssSelector(define));
                foreach (IWebElement e in eles)
                {
                    snoradio radio = new snoradio(e.Text.Trim(), _driver, e);
                    if (radio.MyElement != null)
                        list.Add(radio);
                }

                return list;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyExpectedItemsExisted(string items)
        {
            bool flag = true;
            string[] arr = null;
            if (items.Contains(";"))
            {
                arr = items.Split(';');
            }
            else { arr = new string[] { items }; }
            List<snoradio> list = Items;
            if (arr.Length != list.Count)
            {
                System.Console.WriteLine("[WARNING]: NOT match items count. Expected:(" + arr.Length + "). Runtime:(" + list.Count + ")");
            }
            else System.Console.WriteLine("[Passed]: Match items count. Expected:(" + arr.Length + "). Runtime:(" + list.Count + ")");

            bool flagF = true;
            foreach (string item in arr)
            {
                flagF = VerifyExpectedItem(item);
                if (!flagF && flag) { flag = false; }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyActualItemsExisted(string items)
        {
            bool flag = true;
            string[] arr = null;
            if (items.Contains(";"))
            {
                arr = items.Split(';');
            }
            else { arr = new string[] { items }; }
            List<snoradio> list = Items;
            if (arr.Length != list.Count)
            {
                System.Console.WriteLine("[WARNING]: NOT match items count. Expected:(" + arr.Length + "). Runtime:(" + list.Count + ")");
            }
            else System.Console.WriteLine("[Passed]: Match items count. Expected:(" + arr.Length + "). Runtime:(" + list.Count + ")");

            bool flagF = true;
            foreach (snoradio rd in list)
            {
                string temp = null;
                temp = rd.MyText.Trim();
                if (temp != null)
                    flagF = VerifyActualItem(temp, arr);
                else flagF = false;

                if (!flagF && flag) { flag = false; }
            }

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedItem(string item)
        {
            bool flag = false;
            bool flagM = true;

            if (item.Contains("@@"))
            {
                item = item.Substring(2, item.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ <" + item + ">");
            }
            else
            {
                Console.WriteLine("---finding..M <" + item + ">");
            }

            foreach (snoradio rd in Items)
            {
                string temp = null;
                temp = rd.MyText.Trim();
                if (flagM)
                {
                    if (temp != null && temp.ToLower().Equals(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
                else
                {
                    if (temp != null && temp.ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found] <" + item + "> in actual items.");
            else
                Console.WriteLine("-*-[NOT FOUND] <" + item + "> in actual items.");

            return flag;
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyActualItem(string item, string[] arr)
        {
            bool flag = false;

            Console.WriteLine("---finding... <" + item + "> in expected list.");
            foreach (string exp in arr)
            {
                string it = exp;
                if (it.Contains("@@"))
                {
                    it = exp.Substring(2, it.Length - 2);
                    if (item.Trim().ToLower().Contains(it.Trim().ToLower()))
                    {
                        flag = true;
                        Console.WriteLine("-*-[INFO]: Runtime <" + item + "> - Expected: <" + exp + ">");
                        break;
                    }
                }
                else
                {
                    if (it.Trim().ToLower().Equals(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found] <" + item + "> in expected items.");
            else
                Console.WriteLine("-*-[NOT FOUND] <" + item + "> in expected items.");

            return flag;
        }


        private string mySection = null;

        public string MySection
        {
            get
            {
                if (mySection == null)
                {
                    IWebElement pr = MyElement.FindElement(By.XPath(".."));
                    while (pr.GetAttribute("id") == null || !pr.GetAttribute("id").StartsWith("section") || pr.GetAttribute("class") == null || !pr.GetAttribute("class").StartsWith("section"))
                    {
                        pr = pr.FindElement(By.XPath(".."));
                    }
                    if (pr != null)
                    {
                        pr = pr.FindElement(By.XPath(".."));
                        mySection = pr.GetAttribute("tab_caption_raw");
                        return mySection;
                    }
                    else return null;
                }
                else return mySection;
            }
        }

        

        #region Sub control

        
        #endregion End - Sub control
    }
}
