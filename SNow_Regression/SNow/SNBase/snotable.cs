﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using System.Threading;

namespace SNow
{
    public class snotable : Auto.otable
    {
        private string columnDefine = "thead>tr[id^='hdr'] i[class^='icon-menu'], thead>tr[class='header']>td[id^='tree_view']>h1";
        private string rowDefine = "tbody>tr[id^='row'], tbody div[role='tree'] div:not([id]):not([role]):not([class])";
        private string cellDefine = "td[class='vt'], td[title][class='tree_item_text'] a, td:not([class])";

        private string columnAttribute = "aria-label";

        private string columnTextReplace = "column menu;column options;\r\n;Sorted in ascending order;Sort in descending order;Sorted in descending order;Sort in ascending order";

        private string checkboxDefine = "label[class='checkbox-label']";
        private string iconInformationDefine = "[class^='list_decoration_cell col-small col-center']";
        // private snoelementlist currentCelllist;

        public snotable(string name, IWebDriver driver, IWebElement iwe, [Optional] string frame) 
            : base(name, driver, iwe) 
        {
            base.Frame = frame;
        }
        public snotable(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait, [Optional] string colDef, [Optional] string rowDef, [Optional] string cellDef, [Optional] string colAttribute, [Optional] string colTextReplace, [Optional] bool getNoDisplay)
            : base(name, driver, by, parent, frame, noWait) 
        {
            if (colDef != null && colDef != string.Empty)
                columnDefine = colDef;
            if (rowDef != null && rowDef != string.Empty)
                rowDefine = rowDef;
            if (cellDef != null && cellDef != string.Empty)
                cellDefine = cellDef;
            if (colAttribute != null && colAttribute != string.Empty)
                columnAttribute = colAttribute;
            if (colTextReplace != null && colTextReplace != string.Empty)
                columnTextReplace = colTextReplace;

            base.ColumnDefine = columnDefine;
            base.RowDefine = rowDefine;
            base.CellDefine = cellDefine;
            base.ColumnAttribute = columnAttribute;
            base.ColumnTextReplace = columnTextReplace;
            base.GetNoDisplay = getNoDisplay;
            base.Frame = frame;
        }

        public bool FindRowAndClickCheckbox(string conditions)
        {
            bool flag = true;
            int index = base.RowIndex(conditions);
            if (index >= 0)
            {
                System.Console.WriteLine("***[OK]: Found row with conditions:(" + conditions + ")");
                snoelements list = GCheckbox();
                if (list.MyList.Count - 1 >= index)
                {
                    snocheckbox checkbox = new snocheckbox("Checkbox", MyDriver, list.MyList[index].MyElement);
                    flag = checkbox.Existed;
                    if (flag)
                    {
                        if (!checkbox.Checked)
                        {
                            flag = checkbox.Click(true);
                            if (!flag) System.Console.WriteLine("***[ERROR]: Error when click on checkbox.");
                        }
                    }
                    else System.Console.WriteLine("***[ERROR]: Cannot get checkbox.");
                }
                else
                {
                    flag = false;
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("***[ERROR]: Not found with conditions:(" + conditions + ")");
            }
            return flag;
        }

        public bool FindRowAndClickInformation(string conditions)
        {
            bool flag = true;
            int index = base.RowIndex(conditions);
            if (index >= 0)
            {
                System.Console.WriteLine("***[OK]: Found row with conditions:(" + conditions + ")");
                snoelements list = GIconInformation();
                if (list.MyList.Count - 1 >= index)
                {
                    snoelement ele = new snoelement("element", MyDriver, list.MyList[index].MyElement);
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                    }
                    else System.Console.WriteLine("***[ERROR]: Cannot get icon information.");
                }
                else
                {
                    flag = false;
                }
            }
            else
            {
                flag = false;
                System.Console.WriteLine("***[ERROR]: Not found with conditions:(" + conditions + ")");
            }
            return flag;
        }

        private snoelements GCheckbox()
        {
            By by = By.CssSelector(checkboxDefine);
            snoelement parent = new snoelement("parent", MyDriver, MyElement);
            return new snoelements("Checkbox list", MyDriver, by, parent, snobase.MainFrame);
        }


        private snoelements GIconInformation()
        {
            By by = By.CssSelector(iconInformationDefine);
            snoelement parent = new snoelement("parent", MyDriver, MyElement);
            return new snoelements("Icon information list", MyDriver, by, parent, snobase.MainFrame);
        }

        #region Properties
        //***********************************************************************************************************************************

        public string Name
        {
            get { return this.MyName; }
        }

       
   
  

       
        //***********************************************************************************************************************************
        #endregion End - Properties



    }
}
