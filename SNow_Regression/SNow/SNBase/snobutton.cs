﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;

namespace SNow
{
    public class snobutton : Auto.obutton
    {
        public snobutton(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }
        public snobutton(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) { }

        
    }
}
