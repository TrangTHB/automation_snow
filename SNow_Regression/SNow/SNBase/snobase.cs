﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Generic;

namespace SNow
{
    public class snobase : Auto.obase
    {
        
        #region Variables

        private string waitControlDefine = "#page_timing_div";
        private string waitControlDefineServicePortal = "div[class^='header-loader'][aria-hidden='false']>div[class*='loading']";
        public static Dictionary<string, snoelement> dicDupplicateControl = new Dictionary<string, snoelement>();

        #endregion End - Variables

        #region Properties

        #endregion End - Properties

        #region Init method

        public void SNBeforeRunTestCase(string caseName, ref snobase Base, ref bool flagExit, ref bool flagW, ref bool flag, ref bool flagC, [Optional] string sRow)
        {
            Auto.obase.MainFrame = "gsft_main";
            //-------------------------------------------------------------------------------------------
            Auto.obase tempBase = (Auto.obase)Base;
            BeforeRunTestCase(caseName, ref tempBase, ref flagExit, ref flagW, ref flag, ref flagC, sRow);
            Base = (snobase)tempBase;
        }

        #endregion End - Init method

        #region Public methods

        #region SN get object methods

        public object SNGObject(string name, string type, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait) 
        {
            object obj = null;
            
            switch (type.ToLower())
            {
                case "textbox":
                    obj = new snotextbox("Textbox|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "button":
                    obj = new snobutton("Button|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "combobox":
                    obj = new snocombobox("Combobox|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "lookup":
                    obj = new snolookup("Lookup|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "table":
                    obj = new snotable("Table|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "checkbox":
                    obj = new snocheckbox("Checkbox|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "date":
                    obj = new snodate("Date|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "datetime":
                    obj = new snodatetime("Datetime|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "textarea":
                    obj = new snotextarea("Textarea|" + name, Driver, by, parent, frame, noWait);
                    break;
                case "list":
                    obj = new snolist("List|" + name, Driver, by, parent, frame, noWait);
                    break;
                default:
                    obj = new snoelement("Element|" + name, Driver, by, parent, frame, noWait);
                    break;
            }

            return obj;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public object SNGObject(string name, string type, IWebDriver driver, IWebElement iwe, By by)
        {
            object obj = null;

            switch (type.ToLower())
            {
                case "textbox":
                    obj = new snotextbox("Textbox|" + name, Driver, iwe);
                    break;
                case "button":
                    obj = new snobutton("Button|" + name, Driver, iwe);
                    break;
                case "combobox":
                    obj = new snocombobox("Combobox|" + name, Driver, iwe);
                    break;
                case "lookup":
                    obj = new snocombobox("Lookup|" + name, Driver, iwe);
                    break;
                case "table":
                    obj = new snotable("Table|" + name, Driver, iwe);
                    break;
                case "checkbox":
                    obj = new snocheckbox("Checkbox|" + name, Driver, iwe);
                    break;
                case "date":
                    obj = new snodate("Date|" + name, Driver, iwe);
                    break;
                case "datetime":
                    obj = new snodatetime("Datetime|" + name, Driver, iwe);
                    break;
                case "textarea":
                    obj = new snotextarea("Textarea|" + name, Driver, iwe);
                    break;
                case "list":
                    obj = new snolist("List|" + name, Driver, iwe);
                    break;
                default:
                    obj = new snoelement("Element|" + name, Driver, iwe, type);
                    break;
            }

            return obj;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelements SNGObjects(string name, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool getNoDisplay , [Optional] bool noWait)
        {
            return new snoelements(name, Driver, by, parent, frame, getNoDisplay, noWait);
        }

        #endregion End - SN get object methods

        #region SN page wait loading

        public void PageWaitLoading([Optional] bool noCheckGui, [Optional] bool noMainFrame) 
        {
            //-- Wait script load completed
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            bool jsFlag = ((IJavaScriptExecutor)js).ExecuteScript("return document.readyState").Equals("complete");
            int iWait = 0;

            while (jsFlag == false && iWait < 10)
            {
                Thread.Sleep(2000);
                jsFlag = ((IJavaScriptExecutor)js).ExecuteScript("return document.readyState").Equals("complete");
                iWait = iWait + 1;
            }
            Console.WriteLine("-*-Java loop: <" + iWait + ">");
            //-- Wait ajax load completed
            //-----------------------------------------------------------------------------------------------------
            bool ajaxFlag = (bool)((IJavaScriptExecutor)js).ExecuteScript("return jQuery.active == 0");
            iWait = 0;
            while (ajaxFlag == false && iWait < 10) 
            {
                Thread.Sleep(2000);
                ajaxFlag = (bool)((IJavaScriptExecutor)js).ExecuteScript("return jQuery.active == 0");
                iWait = iWait + 1;
            }
            Console.WriteLine("-*-Ajax loop: <" + iWait + ">");
            //-----------------------------------------------------------------------------------------------------
            if (!noCheckGui)
            {
                Thread.Sleep(5000);
                //Driver.SwitchTo().DefaultContent();
                snoelement waitControl = null;
                if (!noMainFrame)
                {
                    waitControl = (snoelement)SNGObject("Wait control", "element", By.CssSelector(waitControlDefine), null, MainFrame);
                }
                else
                {
                    waitControl = (snoelement)SNGObject("Wait control", "element", By.CssSelector(waitControlDefine));
                }
            }
            else Thread.Sleep(10000);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public void PageWaitLoadingServicePortal()
        {
            //-- Wait script load completed
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            bool jsFlag = ((IJavaScriptExecutor)js).ExecuteScript("return document.readyState").Equals("complete");
            int iWait = 0;

            while (jsFlag == false && iWait < 10)
            {
                Thread.Sleep(2000);
                jsFlag = ((IJavaScriptExecutor)js).ExecuteScript("return document.readyState").Equals("complete");
                iWait = iWait + 1;
            }
            Console.WriteLine("-*-Java loop: <" + iWait + ">");
            //-- Wait ajax load completed
            //-----------------------------------------------------------------------------------------------------
            bool ajaxFlag = (bool)((IJavaScriptExecutor)js).ExecuteScript("return jQuery.active == 0");
            iWait = 0;
            while (ajaxFlag == false && iWait < 10)
            {
                Thread.Sleep(2000);
                ajaxFlag = (bool)((IJavaScriptExecutor)js).ExecuteScript("return jQuery.active == 0");
                iWait = iWait + 1;
            }
            Console.WriteLine("-*-Ajax loop: <" + iWait + ">");
            //-----------------------------------------------------------------------------------------------------
            snoelement waitControl = (snoelement)SNGObject("Wait control", "element", By.CssSelector(waitControlDefineServicePortal), null, null, true);
            int count = 0;
            while (waitControl.Existed && count < 10)
            {
                Thread.Sleep(2000);
                waitControl = (snoelement)SNGObject("Wait control", "element", By.CssSelector(waitControlDefineServicePortal), null, null, true);
                count++;
            }
        }

        #endregion End - SN page wait loading

        //#region SN get textbox by label

        //public snotextbox GTextboxByLabel(string label, [Optional] snoelement parent)
        //{
        //    snotextbox textbox = null;
        //    snoelement lb = null;

        //    string define = "div[id^='element'][class^='form-group']:not([style*='display: none'])>div[id^='label']:not([type='date']):not([type='date_time']):not([type='reference']):not([type='radio']):not([type='boolean']):not([type='choice']):not([type='glide_list']):not([type='journal_input']):not([type='table_name']) span[class='label-text']";
        //    By by = By.CssSelector(define);

        //    snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame);

        //    foreach (Auto.oelement e in labelList.MyList)
        //    {
        //        if (e.MyText.ToLower() == label.ToLower())
        //        {
        //            lb = new snoelement(e.MyText, Driver, e.MyElement);
        //            break;
        //        }
        //    }

        //    if (lb != null)
        //    {
        //        snoelement pr = GParent(lb);
        //        define = "input[class*='form-control']:not([type='hidden'])";
        //        by = By.CssSelector(define);
        //        textbox = (snotextbox)SNGObject(label, "textbox", by, pr, snobase.MainFrame);
        //    }
        //    else textbox = new snotextbox(label, Driver, null);

        //    return textbox;
        //}

        //#endregion End - SN get textbox by label

        //#region SN get lookup by label

        //public snolookup GLookupByLabel(string label, [Optional] snoelement parent)
        //{
        //    snolookup lookup = null;
        //    snoelement lb = null;

        //    string define = "[class^='form-group']:not([style*='display: none'])>div[id^='label'][type='reference']";
        //    By by = By.CssSelector(define);

        //    snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame);

        //    foreach (Auto.oelement e in labelList.MyList)
        //    {
        //        if (e.MyText.ToLower() == label.ToLower())
        //        {
        //            lb = new snoelement(e.MyText, Driver, e.MyElement);
        //            break;
        //        }
        //    }

        //    if (lb != null)
        //    {
        //        snoelement pr = GParent(lb);
        //        define = "input[class*='form-control']:not([type='hidden'])";
        //        by = By.CssSelector(define);
        //        lookup = (snolookup)SNGObject(label, "lookup", by, pr, snobase.MainFrame);
        //    }
        //    else lookup = new snolookup(label, Driver, null);

        //    return lookup;
        //}

        //#endregion End - SN get lookup by label

        //#region SN get combobox by label

        //public snocombobox GComboboxByLabel(string label, [Optional] snoelement parent)
        //{
        //    snocombobox combo = null;
        //    snoelement lb = null;

        //    string define = "[class^='form-group']:not([style*='display: none'])>div[id^='label'][type='choice']";
        //    By by = By.CssSelector(define);

        //    snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame);

        //    foreach (Auto.oelement e in labelList.MyList)
        //    {
        //        if (e.MyText.ToLower() == label.ToLower())
        //        {
        //            lb = new snoelement(e.MyText, Driver, e.MyElement);
        //            break;
        //        }
        //    }

        //    if (lb != null)
        //    {
        //        snoelement pr = GParent(lb);
        //        define = "select[class*='form-control']:not([type='hidden'])";
        //        by = By.CssSelector(define);
        //        combo = (snocombobox)SNGObject(label, "combobox", by, pr, snobase.MainFrame);
        //    }
        //    else combo = new snocombobox(label, Driver, null);

        //    return combo;
        //}

        //#endregion End - SN get combobox by label

        //#region SN get date by label

        //public snodate GDateByLabel(string label, [Optional] snoelement parent)
        //{
        //    snodate date = null;
        //    snoelement lb = null;

        //    string define = "[class^='form-group']:not([style*='display: none'])>div[id^='label'][type='date']";
        //    By by = By.CssSelector(define);

        //    snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame);

        //    foreach (Auto.oelement e in labelList.MyList)
        //    {
        //        if (e.MyText.ToLower() == label.ToLower())
        //        {
        //            lb = new snoelement(e.MyText, Driver, e.MyElement);
        //            break;
        //        }
        //    }

        //    if (lb != null)
        //    {
        //        snoelement pr = GParent(lb);
        //        define = "input[class*='form-control']:not([type='hidden'])";
        //        by = By.CssSelector(define);
        //        date = (snodate)SNGObject(label, "date", by, pr, snobase.MainFrame);
        //    }
        //    else date = new snodate(label, Driver, null);

        //    return date;
        //}

        //#endregion End - SN get date by label

        //#region SN get datetime by label

        //public snodatetime GDatetimeByLabel(string label, [Optional] snoelement parent)
        //{
        //    snodatetime datetime = null;
        //    snoelement lb = null;

        //    string define = "[class^='form-group']:not([style*='display: none'])>div[id^='label'][type='date_time']";
        //    By by = By.CssSelector(define);

        //    snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame);

        //    foreach (Auto.oelement e in labelList.MyList)
        //    {
        //        if (e.MyText.ToLower() == label.ToLower())
        //        {
        //            lb = new snoelement(e.MyText, Driver, e.MyElement);
        //            break;
        //        }
        //    }

        //    if (lb != null)
        //    {
        //        snoelement pr = GParent(lb);
        //        define = "input[class*='form-control']:not([type='hidden'])";
        //        by = By.CssSelector(define);
        //        datetime = (snodatetime)SNGObject(label, "datetime", by, pr, snobase.MainFrame);
        //    }
        //    else datetime = new snodatetime(label, Driver, null);
            
        //    return datetime;
        //}

        //#endregion End - SN get date by label

        #region SN get button by text

        public snobutton GButtonByText(string text, [Optional] snoelement parent)
        {
            snobutton button = null;
            string temp = string.Empty;
            string define = string.Empty;
            if (text.Contains("@@"))
            {
                temp = text.Replace("@@", "");
                define = "//button[contains(text(), '" + temp + "')]";
            }
            else 
            {
                temp = text;
                define = "//button[text()='" + temp + "']";
            } 

            By by = By.XPath(define);
            snoelements buttons = SNGObjects("button list", by, parent, snobase.MainFrame);
            if (buttons.MyList.Count > 0)
            {
                button = new snobutton(text, Driver, buttons.MyList[0].MyElement);
            }
            else button = new snobutton(text, Driver, null);
            return button;
        }

        #endregion End - SN get button by text

        //#region SN get list by label

        //public snolist GListByLabel(string label, [Optional] snoelement parent)
        //{
        //    snolist list = null;
        //    snoelement lb = null;

        //    string define = "div[id^='element'][class^='form-group']:not([style*='display: none'])>div[id^='label'][type='glide_list']";
        //    By by = By.CssSelector(define);

        //    snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame);

        //    foreach (Auto.oelement e in labelList.MyList)
        //    {
        //        if (e.MyText.ToLower() == label.ToLower())
        //        {
        //            lb = new snoelement(e.MyText, Driver, e.MyElement);
        //            break;
        //        }
        //    }

        //    if (lb != null)
        //    {
        //        snoelement pr = GParent(lb);
        //        list = new snolist(label, Driver, pr.MyElement);
        //    }
        //    else list = new snolist(label, Driver, null);

        //    return list;
        //}
        //#endregion End - SN get list by label

        #region SN get control by label

        public object GControlByLabel(string label, [Optional] snoelement parent, [Optional] bool isPortal, [Optional] bool noMainFrame)
        {
            object obj = null;
            snoelement lb = null;
            string define = string.Empty;
            string mainFrame = snobase.MainFrame;

            if (noMainFrame)
                mainFrame = null;

            if (isPortal)
            {
                define = "tr[id^='element']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class]:not([id]), [id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class='label-text']:not([id]), [id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label[ng-bind]:not([id])";
            }
            else 
            {
                //define = "[id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class='label-text'], [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class][ng-bind]";
                //define = "[id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class='label-text'], [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class][ng-bind], tr[id^='element.IO']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label:not([id]):not([class^='radio']) span[class]:not([id]), tr[id^='element.IO']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='none']):not([aria-hidden='true']) legend:not([id]):not([class^='radio']) span[class]:not([id])";
                define = "[id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label span[class='label-text']";
                define = define + "," + "[class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class][ng-bind]";
                define = define + "," + "tr[id^='element']:not([type='hidden']):not([style*='none']):not([id*='element.container']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]):not([class^='radio']) span[class]:not([id])";
                define = define + "," + "tr[id^='element']:not([type='hidden']):not([style*='none']):not([id*='element.container']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) legend:not([id]) span[class]:not([id])";
            }

            By by = By.CssSelector(define);
            snoelements labelList = SNGObjects("Label list", by, parent, mainFrame, true);

            foreach (Auto.oelement e in labelList.MyList)
            {
                if (StringCompare(e.MyText.Trim().ToLower(), label.Trim().ToLower()))
                {
                    lb = new snoelement(e.MyText, Driver, e.MyElement);
                    break;
                }
            }

            if (lb != null)
            {
                snoelement pr = GParent(lb, noMainFrame);
                string type = GType(pr);
                Console.WriteLine("-*//*-[Info]: Type of control is: [" + type + "]");
                switch (type)
                {
                    case "textbox":
                    case "lookup":
                    case "date":
                    case "datetime":
                        define = "input[class*='form-control']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, mainFrame);
                        break;
                    case "combobox":
                        define = "select[class*='form-control']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, mainFrame);
                        break;
                    case "list":
                        obj = new snolist(label, Driver, pr.MyElement);
                        break;
                    case "textarea":
                        define = "textarea[class*='form-control']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, mainFrame);
                        break;
                    case "currency":
                        obj = new snocurrency(label, Driver, pr.MyElement);
                        break;
                    case "checkbox":
                        define = "label[class*='checkbox']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, "checkbox", by, pr, mainFrame);
                        break;
                    case "radio":
                        define = "label[class*='radio']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, "radio", by, pr, snobase.MainFrame);
                        break;
                    case "checkboxgroup":
                        obj = new snocheckboxgroup(label, Driver, pr.MyElement);
                        break;
                    case "radiogroup":
                        obj = new snoradiogroup(label, Driver, pr.MyElement);
                        break;
                }
            }
            else obj = null;

            return obj;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public object GControlByLabelServicePortal(string label, [Optional] snoelement parent)
        {
            object obj = null;
            snoelement lb = null;
            string define = string.Empty;

            define = "[class^='form-group']:not([style*='none']):not([aria-hidden='true']) label[class^='field-label'], [class^='form-group']:not([style*='none']):not([aria-hidden='true']) label>span[style]";

            By by = By.CssSelector(define);
            snoelements labelList = SNGObjects("Label list", by, parent, null, true);

            foreach (Auto.oelement e in labelList.MyList)
            {
                if (e.MyText.Trim().ToLower() == label.Trim().ToLower())
                {
                    lb = new snoelement(e.MyText, Driver, e.MyElement);
                    break;
                }
            }

            if (lb != null)
            {
                snoelement pr = GParentServicePortal(lb);
                string type = GTypeServicePortal(pr);
                Console.WriteLine("-*//*-[Info]: Type of control is: [" + type + "]");
                switch (type)
                {
                    case "textbox":
                    case "date":
                    case "datetime":
                        define = "input[class*='form-control']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, null);
                        break;
                    case "combobox":
                        define = "span[id^='select2-chosen']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, null);
                        break;
                    case "list":
                        obj = new snolist(label, Driver, pr.MyElement);
                        break;
                    case "textarea":
                        define = "textarea[class*='form-control']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, null);
                        break;
                    case "lookup":
                        define = "span[class^='ref']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, type, by, pr, null);
                        break;
                    case "currency":
                        obj = new snocurrency(label, Driver, pr.MyElement);
                        break;
                    case "radiogroup":
                        obj = new sno_sp_radiogroup(label, Driver, pr.MyElement);
                        break;
                    case "checkbox":
                        define = "input[type='checkbox']:not([type='hidden'])";
                        by = By.CssSelector(define);
                        obj = SNGObject(label, "checkbox", by, pr, null);
                        break;
                }
            }
            else obj = null;

            return obj;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GAllControlWithLabelOnForm([Optional] string expType , [Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dicDupplicateControl.Clear();
            snoelement lb = null;
            string define = string.Empty;
            if (onPortal)
                define = "[class^='form-group']:not([style*='none']):not([style*='hidden']):not([aria-hidden='true']) label:not([id]) span[class='label-text'], [class^='form-group']:not([style*='none']):not([style*='hidden']):not([aria-hidden='true']) label[ng-bind]:not([id])";
            else 
            {
                define = "[id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class='label-text']";
                define = define + "," + "div:not([aria-hidden='true'])>[class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class][ng-bind]";
                define = define + "," + "tr[id^='element']:not([type='hidden']):not([style*='none']):not([id*='element.container']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]):not([class^='radio']) span[class]:not([id])";
                define = define + "," + "tr[id^='element']:not([type='hidden']):not([style*='none']):not([id*='element.container']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) legend:not([id]) span[class]:not([id])";
            }
                
            By by = By.CssSelector(define);
            snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame, true);

            snoelement control;
            snoelement ele;

            foreach (Auto.oelement e in labelList.MyList)
            {
                control = null;
                ele = null;
                lb = new snoelement(e.MyText, Driver, e.MyElement);
                snoelement pr = GParent(lb);
                string type = GType(pr);
                if (expType == null || expType.ToLower().Equals(type.Trim().ToLower())) 
                {
                    switch (type)
                    {
                        case "textbox":
                        case "lookup":
                        case "date":
                        case "datetime":
                            define = "input[class*='form-control']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "combobox":
                            define = "select[class*='form-control']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "list":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                        case "textarea":
                            define = "textarea[class*='form-control']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "currency":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                        case "checkbox":
                            define = "label[class*='checkbox']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "radio":
                            define = "label[class*='radio']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "checkboxgroup":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                        case "radiogroup":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                    }
                    if (control != null && control.MyElement != null)
                    {
                        string key = e.MyText.Trim().ToLower();
                        if (!dic.ContainsKey(key))
                            dic.Add(key, control);
                        else
                        {
                            dicDupplicateControl.Add(key, control);
                        }
                            
                    }
                }
            }
            Console.WriteLine("-*-[Found total]: " + dic.Keys.Count + " control(s) on form.");
            Console.WriteLine(".................................................................");
            foreach (var d in dic)
            {
                Console.WriteLine("---[Info]: Found " + d.Value.MyType + " [" + d.Key + "] on form.");
            }
            return dic;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GAllMandatoryControlWithLabelOnForm([Optional] bool notValue, [Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dicDupplicateControl.Clear();
            snoelement lb = null;
            string define = string.Empty;
            if (onPortal)
                define = "[class^='form-group']:not([style*='none']):not([style*='hidden']):not([aria-hidden='true']) label:not([id]) span[class='label-text'], [class^='form-group']:not([style*='none']):not([style*='hidden']):not([aria-hidden='true']) label[ng-bind]:not([id])";
            else 
            {
                define = "[id^='section'][class^='section']:not([type='hidden']):not([style*='none']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class='label-text']";
                define = define + "," + "[class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]) span[class][ng-bind]";
                define = define + "," + "tr[id^='element']:not([type='hidden']):not([style*='none']):not([id*='element.container']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) label:not([id]):not([class^='radio']) span[class]:not([id])";
                define = define + "," + "tr[id^='element']:not([type='hidden']):not([style*='none']):not([id*='element.container']) [class^='form-group']:not([style*='hidden']):not([style*='none']):not([aria-hidden='true']) legend:not([id]) span[class]:not([id])";
            }
            
            By by = By.CssSelector(define);
            snoelements labelList = SNGObjects("Label list", by, parent, snobase.MainFrame, true);

            snoelement control;
            snoelement ele;

            foreach (Auto.oelement e in labelList.MyList)
            {
                control = null;
                ele = null;
                lb = new snoelement(e.MyText, Driver, e.MyElement);
                snoelement pr = GParent(lb);
                string type = GType(pr);
                string temp = pr.MyElement.GetAttribute("class").Trim().ToLower();
                bool flagF = false;
                if (!notValue)
                {
                    if (temp.Contains("is-required") || temp.Contains("is-filled") || temp.Contains("is-prefilled"))
                    {
                        IWebElement ip = e.MyElement.FindElement(By.XPath(".."));
                        IWebElement ie = ip.FindElement(By.CssSelector("span[id]"));
                        if (ie.GetAttribute("mandatory") == "true")
                            flagF = true;
                    }
                }
                else 
                {
                    if (temp.Contains("is-required"))
                    {
                        IWebElement ip = e.MyElement.FindElement(By.XPath(".."));
                        IWebElement ie = ip.FindElement(By.CssSelector("span[id]"));
                        if (ie.GetAttribute("mandatory") == "true")
                            flagF = true;
                    }
                }
                
                if (flagF)
                {
                    switch (type)
                    {
                        case "textbox":
                        case "lookup":
                        case "date":
                        case "datetime":
                            define = "input[class*='form-control']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "combobox":
                            define = "select[class*='form-control']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "list":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                        case "textarea":
                            define = "textarea[class*='form-control']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "currency":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                        case "checkbox":
                            define = "label[class*='checkbox']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "radio":
                            define = "label[class*='radio']:not([type='hidden'])";
                            by = By.CssSelector(define);
                            ele = (snoelement)SNGObject(e.MyText, "element", by, pr, snobase.MainFrame);
                            control = new snoelement(e.MyText, Driver, ele.MyElement, type);
                            break;
                        case "checkboxgroup":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                        case "radiogroup":
                            control = new snoelement(e.MyText, Driver, pr.MyElement, type);
                            break;
                    }
                    if (control != null && control.MyElement != null)
                    {
                        string key = e.MyText.Trim().ToLower();
                        if (!dic.ContainsKey(key))
                            dic.Add(key, control);
                        else
                            dicDupplicateControl.Add(key, control);
                    }
                }
            }
            Console.WriteLine("-*-[Found total]: " + dic.Keys.Count + " control(s) on form, (Mandatory fields - Only fields not have value = " + notValue + ").");
            Console.WriteLine(".................................................................");
            foreach (var d in dic)
            {
                if(!notValue)
                    Console.WriteLine("---[Info]: Found mandatory: " + d.Value.MyType + " [" + d.Key + "] on form.");
                else
                    Console.WriteLine("---[Info]: Found mandatory not have value :" + d.Value.MyType + " [" + d.Key + "] on form.");
            }
            return dic;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public List<string> SP_GAllMandatoryControlWithLabelOnForm([Optional] snoelement parent)
        {
            List<string> list = new List<string>();
            string define = string.Empty;
            define = "div[ng-if^='hasMandatory'] label[ng-repeat*='mandatory']";
            By by = By.CssSelector(define);
            snoelements labelList = SNGObjects("Label list", by, parent, null, true, true);

            foreach (Auto.oelement e in labelList.MyList)
            {
                list.Add(e.MyText);
            }
            Console.WriteLine("-*-[Found total]: " + list.Count + " control(s) on form, (Mandatory fields).");
            Console.WriteLine(".................................................................");
            foreach (string l in list)
            {
                Console.WriteLine("---[Info]: Found mandatory: [" + l + "] on form.");       
            }
            return list;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GAllButtonControlOnForm([Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            dicDupplicateControl.Clear();
            string define = string.Empty;
            if (onPortal)
                define = "button[class^='form_action_button header']";
            else
                define = "button[class^='form_action_button header'], td[style^='padding-right']>button[type='button']";
            By by = By.CssSelector(define);
            snoelements buttonList = SNGObjects("Button list", by, parent, snobase.MainFrame, true);

            foreach (Auto.oelement e in buttonList.MyList)
            {
                snoelement ele = new snoelement("ele", Driver, e.MyElement, "button");
                string key = ele.MyText.Trim().ToLower();
                if (!dic.ContainsKey(key))
                    dic.Add(key, ele);
                else
                    dicDupplicateControl.Add(key, ele);
            }
            Console.WriteLine("-*-[Found total]: " + dic.Keys.Count + " button control(s) on form.");
            Console.WriteLine(".................................................................");
            foreach (var d in dic)
            {
                Console.WriteLine("---[Info]: Found " + d.Value.MyType + " [" + d.Key + "] on form.");
            }
            return dic;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public Dictionary<string, snoelement> GAllRelatedTableButtonControlOnForm([Optional] snoelement parent, [Optional] bool onPortal)
        {
            Dictionary<string, snoelement> dic = new Dictionary<string, snoelement>();
            string define = string.Empty;
            if (onPortal)
                define = "div[class='navbar-header']>button[id]";
            else
                define = "div[class='navbar-header']>button[id][type='submit']";
            By by = By.CssSelector(define);
            snoelements buttonList = SNGObjects("Button list", by, parent, snobase.MainFrame, true);

            foreach (Auto.oelement e in buttonList.MyList)
            {
                snoelement ele = new snoelement("ele", Driver, e.MyElement, "button");
                dic.Add(ele.MyText.Trim().ToLower(), ele);
            }
            Console.WriteLine("-*-[Found total]: " + dic.Keys.Count + " button control(s) on form.");
            Console.WriteLine(".................................................................");
            foreach (var d in dic)
            {
                Console.WriteLine("---[Info]: Found " + d.Value.MyType + " [" + d.Key + "] on form.");
            }
            return dic;
        }
        #endregion End - SN get control by label

        #endregion End - Public methods

        #region Private methods

        private snoelement GParent(snoelement e, [Optional] bool noMainFrame)
        {
            snoelement parent = null;
            string mainFrame = snobase.MainFrame;
            if (noMainFrame)
                mainFrame = null;
            parent = (snoelement)SNGObject("parent", "element", By.XPath(".."), e, mainFrame);
            bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
            while (!flag)
            {
                Thread.Sleep(200);
                parent = (snoelement)SNGObject("parent", "element", By.XPath(".."), parent, mainFrame);
                flag = parent.MyElement.GetAttribute("class").Contains("form-group");
            }

            return parent;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private snoelement GParentServicePortal(snoelement e)
        {
            snoelement parent = null;

            parent = (snoelement)SNGObject("parent", "element", By.XPath(".."), e, null);
            bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
            while (!flag)
            {
                Thread.Sleep(200);
                parent = (snoelement)SNGObject("parent", "element", By.XPath(".."), parent, null);
                flag = parent.MyElement.GetAttribute("class").Contains("form-group");
            }

            return parent;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private string GType(snoelement pr)
        {
            string type = string.Empty;
            string define = "div[id^='label']";
            By by = By.CssSelector(define);
            IWebElement etype = null;
            try
            {
                etype = pr.MyElement.FindElement(by);
            }
            catch { etype = null; }

            if (etype != null)
            {
                string temp = etype.GetAttribute("type");
                switch (temp.Trim().ToLower())
                {
                    case "reference":
                    case "8":
                       type = "lookup";
                        break;
                    case "choice":
                    case "table_name":
                    case "5":
                    case "1":
                    case "18":
                        type = "combobox";
                        break;
                    case "date":
                    case "9":
                        type = "date";
                        break;
                    case "date_time":
                    case "10":
                        type = "datetime";
                        break;
                    case "glide_list":
                        type = "list";
                        break;
                    case "radio":
                        type = "radio";
                        break;
                    case "boolean":
                        type = "checkbox";
                        break;
                    case "journal_input":
                    case "2":
                        type = "textarea";
                        break;
                    case "string":
                        if (etype.GetAttribute("oncontextmenu") != null && etype.GetAttribute("oncontextmenu") != string.Empty)
                            type = "textarea";
                        else
                            type = "textbox";
                        break;
                    case "currency":
                        type = "currency";
                        break;
                    case "sys_class_name":
                        if (etype.GetAttribute("choice") != null)
                            type = "combobox";
                        break;
                    case "text":
                        type = "textarea";
                        break;
                    case "4":
                    case "3":
                        type = "radiogroup";
                        break;
                    case "0":
                        type = "checkboxgroup";
                        break;
                    default:
                        type = "textbox";
                        break;
                }
            }
            else type = "textarea";

            return type;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private string GTypeServicePortal(snoelement pr)
        {
            string type = string.Empty;
            string define = "[class^='form-group']:not([style*='none']):not([aria-hidden='true']) span[ng-class]:not([class^='field']):not([ng-show]):not([class^='ref'])";
            By by = By.CssSelector(define);
            IWebElement etype = null;
            try
            {
                etype = pr.MyElement.FindElement(by);
            }
            catch { etype = null; }

            if (etype != null)
            {
                string temp = etype.GetAttribute("class");
                string[] arr = temp.Split(' ');
                temp = arr[0].Trim().ToLower();
                switch (temp.Trim().ToLower())
                {
                    case "type-textarea":
                        type = "textarea";
                        break;
                    case "type-reference":
                        type = "lookup";
                        break;
                    case "type-multiple_choice":
                        type = "radiogroup";
                        break;
                    case "type-choice":
                        type = "combobox";
                        break;
                    case "type-boolean":
                        type = "checkbox";
                        break;
                    case "type-glide_date":
                        type = "date";
                        break;
                    case "type-glide_date_time":
                        type = "datetime";
                        break;
                    default:
                        type = "textbox";
                        break;
                }
            }
            
            return type;
        }

        #endregion End - Private methods               
    }
}
