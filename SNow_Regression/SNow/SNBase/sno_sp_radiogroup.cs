﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SNow
{
    public class sno_sp_radiogroup : Auto.oelement
    {
        IWebDriver _driver = null;

        public sno_sp_radiogroup(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) 
        {
            _driver = driver;
        }
        public sno_sp_radiogroup(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) 
        {
            _driver = driver;
        }

        public bool MyReadOnly
        {
            get
            {
                snoradio radio = Items[0];
                radio.MoveToElement();
                string result = radio.IsReadOnly;
                if (result != null && (result.Trim().ToLower() == "readonly" || result.Trim().ToLower() == "true"))
                    return true;
                else
                    return false;
            }
        }

        public List<snoradio> Items
        {
            get
            {
                List<snoradio> list = new List<snoradio>();

                IWebElement pr = MyElement.FindElement(By.XPath(".."));
                string define = "input[type='radio']:not([type='hidden'])";
                IReadOnlyCollection<IWebElement> eles = pr.FindElements(By.CssSelector(define));
                foreach (IWebElement e in eles)
                {
                    snoradio radio = new snoradio(e.Text.Trim(), _driver, e);
                    if (radio.MyElement != null)
                        list.Add(radio);
                }

                return list;
            }
        }
        
    }
}
