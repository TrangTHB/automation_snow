﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace SNow
{
    public class snolookup : Auto.olookup
    {
        private string popupDefine = "div[id^='AC'][class='ac_dropdown']:not([style*='display: none'])";
        private string popupItemDefine = "div[id^='AC'] tr[id^='ac_option'] td[class='ac_cell'], div[id^='AC'] div[id^='ac_option'] span";
        private string expectedControlDefine = "[id^='view']:not([style*='display: none']), [id^='IO'][id$='info'][data-original-title='View']:not([style*='display: none'])";
        private string unexpectedControlDefine = "div[class='fieldmsg notification notification-error']";

        public snolookup(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) 
        {
            base.PopupDefine = popupDefine;
            base.PopupItemDefine = popupItemDefine;
            base.ExpectedControlDefine = expectedControlDefine;
            base.UnexpectedControlDefine = unexpectedControlDefine;
        }
        public snolookup(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) 
        {
            base.PopupDefine = popupDefine;
            base.PopupItemDefine = popupItemDefine;
            base.ExpectedControlDefine = expectedControlDefine;
            base.UnexpectedControlDefine = unexpectedControlDefine;
        }

        private string mySection = null;

        //-----------------------------------------------------------------------------------------------------------------------------------
        public string SP_Text
        {
            get
            {
                if (MyElement != null)
                {
                    string define = "span[id^='select2-chosen']";
                    By by = By.CssSelector(define);
                    snoelement pr = new snoelement("parent", MyDriver, this.MyElement);
                    snoelement ele = new snoelement("ele", MyDriver, by, pr);
                    return ele.MyText.Trim();
                }
                else
                    return null;
            }
        }

        public string MySection
        {
            get
            {
                if (mySection == null)
                {
                    IWebElement pr = MyElement.FindElement(By.XPath(".."));
                    while (pr.GetAttribute("id") == null || !pr.GetAttribute("id").StartsWith("section") || pr.GetAttribute("class") == null || !pr.GetAttribute("class").StartsWith("section"))
                    {
                        pr = pr.FindElement(By.XPath(".."));
                    }
                    if (pr != null)
                    {
                        pr = pr.FindElement(By.XPath(".."));
                        mySection = pr.GetAttribute("tab_caption_raw");
                        return mySection;
                    }
                    else return null;
                }
                else return mySection;
            }
        }

        public bool ClickOnFindButton([Optional] bool javaClick)
        {
            bool flag = true;
            IWebElement ele = MyElement.FindElement(By.XPath("..")).FindElement(By.CssSelector("span[class='input-group-btn']>*:nth-child(1)"));
            if (ele != null)
            {
                snoelement sne = new snoelement("Find", MyDriver, ele);
                try
                {
                    flag = sne.Click();
                }
                catch { flag = false; }

                if (!flag)
                {
                    Thread.Sleep(2000);
                    ele = MyElement.FindElement(By.XPath("..")).FindElement(By.CssSelector("span[class='input-group-btn']>*:nth-child(1)"));
                    sne = new snoelement("Find", MyDriver, ele);
                    flag = sne.Click(javaClick);
                }
                    
            }
            else flag = false;
            
            return flag;
        }

        public bool isMandatory
        {
            get
            {
                snoelement sne = new snoelement("sne", base.MyDriver, MyElement);
                snoelement parent = null;
                parent = new snoelement("parent", base.MyDriver, By.XPath(".."), sne, snobase.MainFrame);
                bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                while (!flag)
                {
                    Thread.Sleep(200);
                    parent = new snoelement("parent", base.MyDriver, By.XPath(".."), parent, snobase.MainFrame);
                    flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                }
                string temp = parent.MyElement.GetAttribute("class").Trim().ToLower();
                if (temp.Contains("is-required") || temp.Contains("is-filled") || temp.Contains("is-prefilled"))
                {
                    return true;
                }
                return false;
            }
        }

        #region Service Portal

        public bool SP_Select(string text, [Optional] bool noVerify)
        {
            Console.WriteLine("[Call function]: Select");
            bool flag = true;

            flag = this.Click();
            if (flag)
            {
                string define = "div[id='select2-drop']";
                By by = By.CssSelector(define);
                snotextbox textbox = new snotextbox("textbox", MyDriver, by);
                flag = textbox.Existed;
                if (flag)
                {
                    Actions ac = new Actions(MyDriver);
                    ac.MoveToElement(textbox.MyElement);
                    ac.Click();
                    ac.SendKeys(text);
                    ac.Build().Perform();
                    Thread.Sleep(1000);
                    //-----------------------------------
                    by = By.XPath("//ul[contains(@id, 'select2-results')]/li//div[contains(text(), " + '"' + text + '"' + ")]");
                    snoelement ele = new snoelement("item", MyDriver, by);
                    flag = ele.Existed;
                    if (flag)
                    {
                        ele.Click();
                    }
                }
            }

            //----- Verify -----
            if (!noVerify)
            {
                if (!SP_VerifyExpectedControl())
                {
                    flag = false;
                    Console.WriteLine("***[ERROR]: Invalid refernce.");
                }

                if (flag)
                {
                    Console.WriteLine("***[Select OK]");
                }
            }

            Console.WriteLine(".................................................................................................................");

            return flag;
        }

        public bool SP_Select(int index)
        {
            Console.WriteLine("[Call function]: Select");
            bool flag = true;

            flag = this.Click();
            if (flag)
            {
                By by = By.XPath("//ul[contains(@id, 'select2-results')]/li/div");
                snoelements list = new snoelements("items", MyDriver, by, null, null, true);
                snoelement ele = new snoelement("item", MyDriver, list.MyList[index].MyElement);
                
                Actions ac = new Actions(MyDriver);
                ac.MoveToElement(ele.MyElement);
                ac.Click();
                ac.Build().Perform();
            }

            Console.WriteLine(".................................................................................................................");

            return flag;
        }

        public bool SP_VerifyCurrentValue(string expValue, [Optional] bool wait)
        {
            bool flag = true;
            Console.WriteLine("***[Call function]: VerifyCurrentValue");

            if (MyElement != null)
            {
                if (expValue.Substring(0, 2) == "@@")
                {
                    string temp = expValue.Trim().Substring(2);

                    if (wait)
                    {
                        int count = 0;
                        while (!SP_Text.Trim().ToLower().Contains(temp.ToLower()) && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = SP_Text.Trim().ToLower().Contains(temp.ToLower());
                }
                else
                {
                    if (wait)
                    {
                        int count = 0;
                        while (SP_Text.Trim().ToLower() != expValue.Trim().ToLower() && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = (SP_Text.Trim().ToLower() == expValue.Trim().ToLower());
                }

                if (flag)
                    Console.WriteLine("***[Passed] - Runtime [" + SP_Text + "] - Expected [" + expValue + "]");
                else
                    Console.WriteLine("***[FAILED] - Runtime [" + SP_Text + "] - Expected [" + expValue + "]");
            }
            else
            {
                flag = false;
                Console.WriteLine("***[ERROR] - Element is NULL");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool SP_VerifyExpectedControl()
        {
            bool flag = true;
            By by = By.XPath("//*[contains(@id, 'select2-results')]/li[text()='No matches found']");
            snoelement ele = new snoelement("verify", MyDriver, by, null, null, true);
            flag = ele.Existed;
            return !flag;
        }

        #endregion End - Service Portal
    }
}
