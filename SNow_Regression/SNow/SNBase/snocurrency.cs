﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SNow
{
    public class snocurrency : Auto.oelement
    {
        IWebDriver _driver = null;

        public snocurrency(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) 
        {
            _driver = driver;
        }
        public snocurrency(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) 
        {
            _driver = driver;
        }

        public bool MyReadOnly 
        {
            get
            {
                return Textbox_Value().ReadOnly;
            }
        }

        private string mySection = null;
        public string MySection
        {
            get
            {
                if (mySection == null)
                {
                    IWebElement pr = MyElement.FindElement(By.XPath(".."));
                    while (pr.GetAttribute("id") == null || !pr.GetAttribute("id").StartsWith("section") || pr.GetAttribute("class") == null || !pr.GetAttribute("class").StartsWith("section"))
                    {
                        pr = pr.FindElement(By.XPath(".."));
                    }
                    if (pr != null)
                    {
                        pr = pr.FindElement(By.XPath(".."));
                        mySection = pr.GetAttribute("tab_caption_raw");
                        return mySection;
                    }
                    else return null;
                }
                else return mySection;
            }
        }

        public bool isMandatory
        {
            get
            {
                snoelement sne = new snoelement("sne", base.MyDriver, MyElement);
                snoelement parent = null;
                parent = new snoelement("parent", base.MyDriver, By.XPath(".."), sne, snobase.MainFrame);
                bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                while (!flag)
                {
                    Thread.Sleep(200);
                    parent = new snoelement("parent", base.MyDriver, By.XPath(".."), parent, snobase.MainFrame);
                    flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                }
                string temp = parent.MyElement.GetAttribute("class").Trim().ToLower();
                if (temp.Contains("is-required") || temp.Contains("is-filled") || temp.Contains("is-prefilled"))
                {
                    return true;
                }
                return false;
            }
        }

        #region Sub control

        public snotextbox Textbox_Value()
        {
            snotextbox textbox = null;
            string define = "input[class*='form-control']";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            textbox = new snotextbox("Email", _driver, by, pr, snobase.MainFrame);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snocombobox Combobox_CurrencyType()
        {
            snocombobox combo = null;
            string define = "select[class*='form-control']";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            combo = new snocombobox("Currency type", _driver, by, pr, snobase.MainFrame);
            return combo;
        }
        #endregion End - Sub control
    }
}
