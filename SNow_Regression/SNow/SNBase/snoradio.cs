﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;

namespace SNow
{
    public class snoradio : Auto.oradio
    {
        public snoradio(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }
        public snoradio(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) { }


        public string SP_Text
        {
            get
            {
                string value = null;
                value = MyElement.GetAttribute("aria-label");
                if (value == null || value == string.Empty)
                {
                    value = MyElement.GetAttribute("value");
                }
                return value;
            }
        }
    }
}
