﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SNow
{
    public class snolist : Auto.oelement
    {
        IWebDriver _driver = null;

        public snolist(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) 
        {
            _driver = driver;
        }
        public snolist(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) 
        {
            _driver = driver;
        }


        public bool MyReadOnly
        {
            get
            {
                return Button_Unlock().ReadOnly;
            }
        }

        private string mySection = null;

        public string MySection
        {
            get
            {
                if (mySection == null)
                {
                    IWebElement pr = MyElement.FindElement(By.XPath(".."));
                    while (pr.GetAttribute("id") == null || !pr.GetAttribute("id").StartsWith("section") || pr.GetAttribute("class") == null || !pr.GetAttribute("class").StartsWith("section"))
                    {
                        pr = pr.FindElement(By.XPath(".."));
                    }
                    if (pr != null)
                    {
                        pr = pr.FindElement(By.XPath(".."));
                        mySection = pr.GetAttribute("tab_caption_raw");
                        return mySection;
                    }
                    else return null;
                }
                else return mySection;
            }
        }

        public bool isMandatory
        {
            get
            {
                snoelement sne = new snoelement("sne", base.MyDriver, MyElement);
                snoelement parent = null;
                parent = new snoelement("parent", base.MyDriver, By.XPath(".."), sne, snobase.MainFrame);
                bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                while (!flag)
                {
                    Thread.Sleep(200);
                    parent = new snoelement("parent", base.MyDriver, By.XPath(".."), parent, snobase.MainFrame);
                    flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                }
                string temp = parent.MyElement.GetAttribute("class").Trim().ToLower();
                if (temp.Contains("is-required") || temp.Contains("is-filled") || temp.Contains("is-prefilled"))
                {
                    return true;
                }
                return false;
            }
        }

        #region Sub control

        public snobutton Button_Unlock() 
        {
            snobutton bt = null;
            string define = "button[id$='_unlock']";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            bt = new snobutton("Unlock", _driver, by, pr, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_Lock()
        {
            snobutton bt = null;
            string define = "button[id$='_lock']";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            bt = new snobutton("Lock", _driver, by, pr, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_AddMe_Locked()
        {
            snobutton bt = null;
            string define = "button[id^='add_me_locked']";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            bt = new snobutton("Add me", _driver, by, pr, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snobutton Button_AddMe()
        {
            snobutton bt = null;
            string define = "button[id^='add_me']:not([id*='locked'])";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            bt = new snobutton("Add me", _driver, by, pr, snobase.MainFrame);
            return bt;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snotextbox Textbox_Fill_Data()
        {
            snotextbox textbox = null;
            string define = "input[id]";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            textbox = new snotextbox("Email", _driver, by, pr, snobase.MainFrame);
            return textbox;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public snoelement Showlist_Added()
        {
            snoelement sl = null;
            string define = "p[id$='_nonedit']";
            By by = By.CssSelector(define);
            snoelement pr = new snoelement("parent", _driver, MyElement);
            sl = new snoelement("Show list", _driver, by, pr, snobase.MainFrame);
            return sl;
        }
        #endregion End - Sub control
    }
}
