﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;

namespace SNow
{
    public class snotextbox : Auto.otextbox
    {
        public snotextbox(string name, IWebDriver driver, IWebElement iwe)
            : base(name, driver, iwe) { }
        public snotextbox(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) {}
        
        private string mySection = null;

        public string MySection
        {
            get
            {
                if (mySection == null)
                {
                    IWebElement pr = MyElement.FindElement(By.XPath(".."));
                    while (pr.GetAttribute("id") == null || !pr.GetAttribute("id").StartsWith("section") || pr.GetAttribute("class") == null || !pr.GetAttribute("class").StartsWith("section"))
                    {
                        pr = pr.FindElement(By.XPath(".."));
                    }
                    if (pr != null)
                    {
                        pr = pr.FindElement(By.XPath(".."));
                        mySection = pr.GetAttribute("tab_caption_raw");
                        return mySection;
                    }
                    else return null;
                }
                else return mySection;               
            }
        }

        public bool isMandatory
        {
            get 
            {
                snoelement sne = new snoelement("sne", base.MyDriver, MyElement);
                snoelement parent = null;
                parent = new snoelement("parent", base.MyDriver, By.XPath(".."), sne, snobase.MainFrame);
                bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                while (!flag)
                {
                    Thread.Sleep(200);
                    parent = new snoelement("parent", base.MyDriver, By.XPath(".."), parent, snobase.MainFrame);
                    flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                }
                string temp = parent.MyElement.GetAttribute("class").Trim().ToLower();
                if (temp.Contains("is-required") || temp.Contains("is-filled") || temp.Contains("is-prefilled"))
                {
                    return true;
                }
                return false;
            }
        }
    }
}
