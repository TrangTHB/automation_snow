﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace SNow
{
    public class snocombobox : Auto.ocombobox
    {
        public snocombobox(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }
        public snocombobox(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, noWait) { }
        
        private string mySection = null;

        public string MySection
        {
            get
            {
                if (mySection == null)
                {
                    IWebElement pr = MyElement.FindElement(By.XPath(".."));
                    while (pr.GetAttribute("id") == null || !pr.GetAttribute("id").StartsWith("section") || pr.GetAttribute("class") == null || !pr.GetAttribute("class").StartsWith("section"))
                    {
                        pr = pr.FindElement(By.XPath(".."));
                    }
                    if (pr != null)
                    {
                        pr = pr.FindElement(By.XPath(".."));
                        mySection = pr.GetAttribute("tab_caption_raw");
                        return mySection;
                    }
                    else return null;
                }
                else return mySection;
            }
        }

        public bool isMandatory
        {
            get
            {
                snoelement sne = new snoelement("sne", base.MyDriver, MyElement);
                snoelement parent = null;
                parent = new snoelement("parent", base.MyDriver, By.XPath(".."), sne, snobase.MainFrame);
                bool flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                while (!flag)
                {
                    Thread.Sleep(200);
                    parent = new snoelement("parent", base.MyDriver, By.XPath(".."), parent, snobase.MainFrame);
                    flag = parent.MyElement.GetAttribute("class").Contains("form-group");
                }
                string temp = parent.MyElement.GetAttribute("class").Trim().ToLower();
                if (temp.Contains("is-required") || temp.Contains("is-filled") || temp.Contains("is-prefilled"))
                {
                    return true;
                }
                return false;
            }
        }

        public string SP_Text
        {
            get
            {
                if (MyElement != null)
                {
                    return MyElement.Text;
                }
                else
                    return null;
            }
        }

        public bool SP_SelectItem(string text, [Optional] bool noVerify)
        {
            Console.WriteLine("[Call function]: SelectItem");
            bool flag = true;

            flag = this.Click();
            if (flag)
            {
                string define = "div[id='select2-drop']";
                By by = By.CssSelector(define);
                snotextbox textbox = new snotextbox("textbox", MyDriver, by);
                flag = textbox.Existed;
                if (flag)
                {
                    Actions ac = new Actions(MyDriver);
                    ac.MoveToElement(textbox.MyElement);
                    ac.Click();
                    ac.SendKeys(text);
                    ac.Build().Perform();
                    Thread.Sleep(1000);
                    //-----------------------------------
                    by = By.XPath("//ul[contains(@id, 'select2-results')]/li/div/span[contains(text(), " + '"' + text + '"' + ")]");
                    snoelement ele = new snoelement("item", MyDriver, by);
                    flag = ele.Existed;
                    if (flag)
                    {
                        ele.Click();
                    }
                }
            }

            Console.WriteLine(".................................................................................................................");

            return flag;
        }

        public bool SP_SelectItem(int index, [Optional] bool noVerify)
        {
            Console.WriteLine("[Call function]: SelectItem");
            bool flag = true;

            flag = this.Click();
            Thread.Sleep(1000);
            if (flag)
            {
                By by = By.XPath("//ul[contains(@id, 'select2-results')]/li/div");
                snoelements list = new snoelements("items", MyDriver, by, null, null, true);
                snoelement it = null;
                foreach (Auto.oelement ele in list.MyList) 
                {
                    if (ele.MyText.Trim() != string.Empty && ele.MyText.Trim() != "-- None --" && ele.MyText.Trim() != string.Empty && ele.MyText.Trim() != "--None--") 
                    {
                        it = new snoelement("item", MyDriver, ele.MyElement);
                        break;
                    }
                }

                if (it == null) { flag = false; }
                else
                {
                    Actions ac = new Actions(MyDriver);
                    ac.MoveToElement(it.MyElement);
                    ac.Click();
                    ac.Build().Perform();
                } 
            }

            Console.WriteLine(".................................................................................................................");

            return flag;
        }

        public bool SP_VerifyCurrentValue(string expValue, [Optional] bool wait)
        {
            bool flag = true;
            Console.WriteLine("***[Call function]: VerifyCurrentValue");

            if (MyElement != null)
            {
                if (expValue.Substring(0, 2) == "@@")
                {
                    string temp = expValue.Trim().Substring(2);

                    if (wait)
                    {
                        int count = 0;
                        while (!SP_Text.Trim().ToLower().Contains(temp.ToLower()) && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = SP_Text.Trim().ToLower().Contains(temp.ToLower());
                }
                else
                {
                    if (wait)
                    {
                        int count = 0;
                        while (SP_Text.Trim().ToLower() != expValue.Trim().ToLower() && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = (SP_Text.Trim().ToLower() == expValue.Trim().ToLower());
                }

                if (flag)
                    Console.WriteLine("***[Passed] - Runtime [" + SP_Text + "] - Expected [" + expValue + "]");
                else
                    Console.WriteLine("***[FAILED] - Runtime [" + SP_Text + "] - Expected [" + expValue + "]");
            }
            else
            {
                flag = false;
                Console.WriteLine("***[ERROR] - Element is NULL");
            }

            return flag;
        }
    }
}
