﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;

namespace SNow
{
    public class snoelements : Auto.oelements
    {
        public snoelements(string name, IWebDriver driver, By by, [Optional] snoelement parent, [Optional] string frame, [Optional] bool getNoDisplay, [Optional] bool noWait)
            : base(name, driver, by, parent, frame, getNoDisplay, noWait) {}
    }
}
