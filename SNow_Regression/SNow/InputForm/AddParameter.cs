﻿using System.Windows.Forms;

namespace Auto
{
    public partial class AddParameter : Form
    {

        public string value = string.Empty;
       

        public AddParameter(string title)
        {
            InitializeComponent();
            
            this.lbParaName.Text = title;
        }

        private void AddParameter_FormClosing(object sender, FormClosingEventArgs e)
        {
            value = this.tbParaValue.Text.Trim();
        }
    }
}
