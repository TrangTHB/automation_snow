﻿namespace Auto
{
    partial class AddParameter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbParaName = new System.Windows.Forms.Label();
            this.tbParaValue = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbParaName
            // 
            this.lbParaName.AutoSize = true;
            this.lbParaName.Location = new System.Drawing.Point(12, 9);
            this.lbParaName.Name = "lbParaName";
            this.lbParaName.Size = new System.Drawing.Size(90, 18);
            this.lbParaName.TabIndex = 0;
            this.lbParaName.Text = "lbParaName";
            // 
            // tbParaValue
            // 
            this.tbParaValue.Location = new System.Drawing.Point(15, 30);
            this.tbParaValue.Name = "tbParaValue";
            this.tbParaValue.Size = new System.Drawing.Size(276, 24);
            this.tbParaValue.TabIndex = 1;
            // 
            // AddParameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 75);
            this.Controls.Add(this.tbParaValue);
            this.Controls.Add(this.lbParaName);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddParameter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddParameter";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddParameter_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbParaName;
        private System.Windows.Forms.TextBox tbParaValue;
    }
}