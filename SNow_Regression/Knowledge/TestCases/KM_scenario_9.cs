﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
namespace Knowledge
{
    class KM_scenario_9
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            for (int i = 0; i < 4; i++ )
            {
                int ord = i + 1;
                System.Console.WriteLine("Knowledge Id"+ord+":" + knowledge_arr[i]);
            }
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        //------------------------------------------------------------------
        Login login = null;
        Incident inc = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        KnowledgeSearch knls = null;
        KnowledgeList kmlist = null;
        IncidentList incList = null;

        //------------------------------------------------------------------
        string incidentId;
        string[] knowledge_arr;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new KnowledgeList(Base, "Knowledge List");
                incList = new IncidentList(Base, "Incident list");
                knls = new KnowledgeSearch(Base);
                //------------------------------------------------------------------
                knowledge_arr = new string[4];
                incidentId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_003_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KnowledgeManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_005_Create4KnowledgeArticles()
        {
            try
            {
                error = string.Empty;
                string temp = string.Empty;
                string[] knowledge_infos = null; ;
                for (int i = 1; i <= 4; i++)
                {
                    temp = Base.GData("KA_" + i);
                    if (temp.Contains("|"))
                    {
                        knowledge_infos = temp.Split('|');
                        if (knowledge_infos.Length != 4)
                        { error += "Incorrect data format. Correct format: KB|Short description|article's domain|Assignment group."; flag = false; }
                        else
                            flag = true;
                    }
                    else
                    {error += "Incorrect data format in Excel file. Please correct."; flag = false;}
                    if(flag)
                    { //search if knowledge is already existing and published
                        bool sflag = true;
                        sflag = home.LeftMenuItemSelect("Knowledge", "Published");
                        if (sflag)
                        {
                            kmlist.WaitLoading();
                            sflag = kmlist.Add_More_Columns("Domain");
                            sflag = kmlist.SearchAndOpen("Short description",knowledge_infos[1], "Domain=@@" + knowledge_infos[2], "Number");
                            if (sflag)
                            {
                                knowledge.WaitLoading();
                                textbox = knowledge.Textbox_Number();
                                if (textbox.Existed)
                                    knowledge_arr[i - 1] = textbox.Text;
                            }
                            else
                            {
                                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                                if (flag)
                                {// create new knowledge if not existing in system
                                    bool crflag = true;
                                    string domain_str = knowledge_infos[2];
                                    if (domain_str.Trim().ToLower() == "global")
                                        domain_str = Base.GData("Domain");
                                    crflag = knowledge.AutoCreateKnowledgeArticles(knowledge_infos[0], knowledge_infos[1], domain_str, knowledge_infos[3]);
                                    if (!crflag)
                                        error += "Cannot create knowledge article with information:[" + temp + "]";
                                    else
                                    {

                                        kmlist.WaitLoading();
                                        temp = "Number=@@KB";
                                        crflag = kmlist.SearchAndOpen("Short description", "*" + knowledge_infos[1], temp, "Number");
                                        if (crflag)
                                        {
                                            knowledge.WaitLoading();
                                            textbox = knowledge.Textbox_Number();
                                            knowledge_arr[i - 1] = textbox.Text;
                                            if (knowledge_infos[0].Trim().ToLower() == "self service")
                                            {
                                                button = knowledge.Button_KA_Review();
                                                if (button.Existed)
                                                {
                                                    button.Click(true);
                                                    kmlist.WaitLoading();
                                                    crflag = kmlist.SearchAndOpen("Short description", knowledge_infos[1], temp, "Number");
                                                    if (!crflag)
                                                        error += "Cannot search the Review knowlege article KA_" + i;
                                                    else
                                                        knowledge.WaitLoading();
                                                }
                                                else
                                                    error += "Cannot get Review button.";
                                            }
                                            if (knowledge_infos[2].Trim().ToLower() == "global")
                                            {
                                                lookup = knowledge.Lookup_Domain();
                                                if (lookup.Existed)
                                                {
                                                    crflag = lookup.SetText("");
                                                    System.Console.WriteLine("Text has been set to blank");
                                                    if (!crflag)
                                                        error += "Cannot set text the domain field.";
                                                }
                                                else
                                                    error += "Cannot get domain lookup.";
                                            }

                                            button = knowledge.Button_KA_Publish();
                                            if (button.Existed)
                                            {
                                                button.Click(true);
                                                kmlist.WaitLoading();
                                            }
                                            else
                                                error += "Cannot get publish button.";
                                        }
                                        else
                                            error += "Cannot search the Draft knowlege article KA_" + i;
                                    }
                                }
                                else
                                    error += "Cannot select left item.Knowledge> Create New.";
                            }
                        }
                        else
                            error += "Cannot select left item. Knowledge> Published.";
                    }
                }
                if (error != "")
                    flag = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_006_Logout()
        {
            try
            {
                Base.ClearCache();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                    
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_ImpersonateUser_Resolver()
        {
            try
            {
                string temp = Base.GData("Resolver");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
       
        [Test]
        public void Step_007_01_PopulateShortDescription_KeyWord()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_02_PopulateDescription()
        {
            try
            {
                
                string temp = "Auto Description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textbox description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    int i = 1;
                    while(!flag && i<4)
                    {
                        Thread.Sleep(1000);
                        flag = combobox.SelectItem(temp);
                        i++;
                    }
                        if (flag)
                        {
                            inc.WaitLoading();
                        }
                        else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else { inc.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Click_SearchKnowledge()
        {
            try
            {
                button = inc.Button_SearchKnowledge();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        //-- Switch to page 1
                        flag = Base.SwitchToPage(1);
                        if (flag)
                        {
                            knls.WaitLoading();
                        }
                        else { error = "Error when switch to page 1"; }
                    }
                    else { error = "Cannot click on button search knowledge."; }
                }
                else { error = "Cannot get button search knownledge."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_SearchAndVerifyVisibleKnowledges()
        {
            try
            {
                error = "";
               string temp = string.Empty;
                string[] knowledge_infos = null; ;
                for (int i = 1; i <= 4; i++)
                {
                    temp = Base.GData("KA_" + i);
                    if (temp.Contains("|"))
                    {
                        knowledge_infos = temp.Split('|');
                        if (knowledge_infos.Length != 4)
                        {error += "Incorrect data format. Correct format: KB|Short description|article's domain|Assignment group."; flag = false;}
                        else
                            flag = true;
                    }
                    else
                    {error += "Incorrect data format in Excel file. Please correct."; flag = false;}
                    if (flag)
                    {
                        flag = knls.FindKnowledge(knowledge_infos[1], true);
                        if(flag)
                        {
                            if (i == 2 || i == 4)
                                error += "Knowledge KA_" + i + "is in search result. Knowlege information: " + temp;
                            else
                            {
                                System.Console.WriteLine("**Passed - Visible knowledge article - " + knowledge_infos[1]);
                                if (Base.GData("UpdateEnv").Contains(Base.GData("Env")))
                                {
                                    flag = knls.VerifyKnowledgeInfo_KnowledgeSearch(knowledge_infos[1], knowledge_infos[0]);
                                    if (!flag)
                                        error += "Cannot find expected information in knowledge record.[" + knowledge_infos[0] + "]";
                                }
                            }
                                
                        }
                        else
                        {
                            if(i==1 || i==3)
                                error += "Knowledge KA_" + i + "is not in search result. Knowlege information: " + temp;
                            else
                                System.Console.WriteLine("**Passed - Not visible knowledge article - " + knowledge_infos[1]);
                        }
                    }
                }
                if (error == "")
                    flag = true;
                else
                    flag = false;
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_Close_AdditionalDialog()
        {
            try
            {
                Base.Driver.Close();
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch page.";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                if (flag == false)
                {
                    error = "Cannot logout system.";
                }
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        #endregion
    }
}
