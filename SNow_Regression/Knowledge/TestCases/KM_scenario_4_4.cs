﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;

namespace Knowledge
{
    class KM_scenario_4_4
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Problem Id: " + problemId);
            System.Console.WriteLine("Knowledge Submission Id: " + SubmissionId);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;        
        snocheckbox checkbox;
        snotextarea textarea;
        snobutton button;
        
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        KnowledgeList kmlist = null;        
        Problem prb = null;
        ProblemList prbList = null;
        Submission submission = null;
        ItilList sublist = null;
        //------------------------------------------------------------------
        string problemId;
        string SubmissionId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new KnowledgeList(Base, "Knowledge List");                
                prb = new Problem(Base, "Problem");
                prbList = new ProblemList(Base, "Problem list");
                submission = new Submission(Base, "Submission");
                
                sublist = new ItilList(Base, "Submission List");
                //------------------------------------------------------------------
                
                problemId = string.Empty;
                SubmissionId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
       
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_003_ImpersonateUser_ServiceDesk_1()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("ServiceDesk_1"));
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_005_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Cannot select create new problem"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_006_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        problemId = textbox.Text;
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }

                //-- Input company
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string company = Base.GData("Company");
                    flag = lookup.Select(company);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else
                { error = "Cannot get textbox company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_01_PopulateProblemImpact()
        {
            try
            {
                combobox = prb.Combobox_Impact();
                string temp = Base.GData("ProImpact");
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Problem impact."; }
                }
                else { error = "Cannot get combobox Problem impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_02_PopulateProblemStatement()
        {
            try
            {
                textbox = prb.Textbox_ProblemStatement();
                string temp = Base.GData("ProStatement");
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_03_PopulateProblemSource()
        {
            try
            {
                combobox = prb.Combobox_ProblemSource();
                string temp = Base.GData("ProSource");
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Problem source."; }
                }
                else { error = "Cannot get combobox Problem source."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------        
        [Test]
        public void Pre_008_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //*************************************************************************************************

        [Test]
        public void Step_001_CheckKnowledgeBox()
        {
            try
            {
                checkbox = prb.Checkbox_Create_Knowledge();

                flag = checkbox.Existed;
                if (flag)
                {
                    checkbox.Click();
                }
                else
                {
                    error = "Can not check knowledge";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_002_PopulateCloseCodeAndCloseNotes()
        {
            try
            {
                combobox = prb.Combobox_CauseCode();
                temp = Base.GData("CauseCode");

                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);

                    if (flag == false)
                    {
                        error = "Cannot input cause code.";
                    }
                    else
                    {
                        temp = Base.GData("ClosureCode");
                        combobox = prb.Combobox_ClosureCode();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                temp = Base.GData("CloseNote");
                                textarea = prb.Textarea_CloseNotes();

                                flag = textarea.SetText(temp);

                                if (flag == false)
                                {
                                    error = "Cannot input problem close notes.";
                                }
                            }
                            else
                            {
                                error = "Cannot input problem closure code.";
                            }
                        }
                        
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_CloseProblem()
        {
            try
            {
                button = prb.Button_AdditionalActions();

                flag = button.Existed;

                if (flag)
                {
                    button.Click();
                    flag = prb.Select_ContextMenu_Items("Close Problem");

                    if (flag == false)
                    {
                        error = "Cannot find close problem menu";

                    }
                    else
                    {
                        prbList.WaitLoading();
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_VerifyMessageAndGetSubmissionId()
        {
            try
            {
                
                SubmissionId = prb.GetMessageInfo_TicketNumber();
                if (SubmissionId != string.Empty)
                {
                    flag = prb.VerifyMessageInfo("Knowledge Submission created: " + SubmissionId);
                    if (!flag)
                        error = "Invalid info message.";
                }
                else
                { error = "No submission number in a info message."; flag = false; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_SearchAndOpenProblem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "All");
                if (flag)
                {
                    prbList.WaitLoading();
                    temp = prbList.List_Title().MyText;
                    flag = temp.Equals("Problems");
                    if (flag)
                    {
                        flag = prbList.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                        if (!flag) error = "Error when search and open problem (id:" + problemId + ")";
                        else prb.WaitLoading();
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)"; }
                }
                else error = "Error when select open all problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_SearchAndOpenSubmission()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input submission Id.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                
                flag = prb.Select_Tab("KB Submissions");
                if (flag == true)
                {
                    flag = prb.Search_Open_RelatedTable_Row("KB Submissions", "Number", SubmissionId, "Number=" + SubmissionId, "Number");
                    if (!flag)
                    {
                        error = "Can not find kb submission";
                    }
                    else
                    {
                        submission.WaitLoading();
                    }

                }
                else
                {
                    error = "Cannot open KB Submissions tab.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_AssignKBSubmissionToGroup()
        {
            try
            {
                temp = Base.GData("AssignGroup");
                lookup = submission.Lookup_AssignmentGroup();

                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);

                    submission.WaitLoading();

                    if (flag == false)
                    {
                        error = "Cannot populate assigned to.";
                    }
                }
                else
                {
                    error = "Cannot get assigned to control.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_AssignKBSubmissionToMe()
        {
            try
            {
                temp = Base.GData("ServiceDesk_1");
                lookup = submission.Lookup_AssignedTo();

                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);

                    submission.WaitLoading();

                    if (flag == false)
                    {
                        error = "Cannot populate assigned to.";
                    }
                }
                else
                {
                    error = "Cannot get assigned to control.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_SetStatusToAssigned()
        {
            try
            {
                temp = "Assigned";
                combobox = submission.Combobox_Status();

                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);

                    if (flag)
                    {
                        submission.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot select Status = " + temp;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_SaveSubmission()
        {
            try
            {
                flag = submission.Save();
                if (!flag)
                {
                    error = "Cannot save submission";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_SearchAndVerifySubmission_Active()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input submission Id.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = home.LeftMenuItemSelect("Knowledge", "Assigned to me");
                if(flag)
                {
                    sublist.WaitLoading();
                    flag = sublist.List_Title().MyText == "KB Submissions";
                    if (flag)
                    {
                        flag = sublist.Add_More_Columns("Active");
                        if (flag)
                        {
                            flag = sublist.SearchAndVerify("Number", SubmissionId, "Number=" + SubmissionId + "|Active=true");
                            if (!flag)
                                error = "Cannot find any record as provided conditions.";
                            else
                                submission.WaitLoading();
                        }
                        else
                            error = "Cannot add active column to submission list.";
                    }
                    else
                        error = "Incorrect list displays.";
                    
                }
                else
                    error = "Cannot select Assigned to me in the left menu.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_ImpersonateUser_TechnicalUser()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("TechnicalUser"), true, Base.GData("UserFullName"));
                if (!flag)
                    error = "Cannot impersonate technical user.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SearchAndAssignedToMe()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input submission Id.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Knowledge", "Open Submissions");
                if (flag)
                {
                    sublist.WaitLoading();
                    flag = sublist.Add_More_Columns("Assigned to");
                    if (flag)
                    {
                        flag = sublist.SearchAndSelectContextMenu("Number", SubmissionId, "Number=" + SubmissionId, "Number", "Assign to me");
                        if (!flag)
                            error = "Cannot find any record as provided conditions to do action on it.";
                        else
                        {
                            flag = sublist.SearchAndVerify("Number", SubmissionId, "Number=" + SubmissionId + "|Assigned to=" + Base.GData("TechnicalUser"));
                            if (!flag)
                                error = "Cannot assign to me kb sumissions.";
                        }
                    }
                    else
                        error = "Cannot add assigned to column into km submission list.";
                        
                }
                else
                    error = "Cannot select Assigned to me in the left menu.";


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_SearchAndOpenSubmission()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input submission Id.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Knowledge", "Open Submissions");
                if (flag)
                {
                    sublist.WaitLoading();
                    flag = sublist.SearchAndOpen("Number", SubmissionId, "Number=" + SubmissionId+"|Assigned to="+Base.GData("TechnicalUser"), "Number");
                    if (!flag)
                        error = "Cannot find any record as provided conditions.";
                }
                else
                    error = "Cannot select Assigned to me in the left menu.";


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_SetStatusToClosedInvalid()
        {
            try
            {
                temp = "Closed, Invalid";
                combobox = submission.Combobox_Status();

                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);

                    if (flag)
                    {
                        submission.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot select Status = " + temp;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        
        [Test]
        public void Step_017_SaveSubmission()
        {
            try
            {
                flag = submission.Save();
                if (!flag)
                {
                    error = "Cannot save submission";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
       //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_VerifyCloseNotes()
        {
            try
            {
                textarea = submission.Textarea_CloseNotes();
                flag = textarea.Existed;
                temp = Base.GData("KB_SubmissionNote");
                if(flag)
                {
                    flag = textarea.VerifyCurrentValue(temp);
                    if (!flag)
                        error = "Invalid info message displays.Runtime:" + textarea.Text;
                }
                else
                    error = "Cannot get text textarea.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SearchAndVerifySubmission_Inactive()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input submission Id.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = home.LeftMenuItemSelect("Knowledge", "Assigned to me");
                if (flag)
                {
                    sublist.WaitLoading();
                    flag = sublist.Add_More_Columns("Active");
                    if (flag)
                    {
                        flag = sublist.Filter("Number;is;" + SubmissionId + "|and|Active;is;false");
                        if (flag)
                        {
                            flag = sublist.SearchAndVerify("Number", SubmissionId, "Number=" + SubmissionId + "|Active=false");
                            if (!flag)
                                error = "Cannot find any record as provided conditions.";
                            else
                                submission.WaitLoading();
                        }
                        else
                            error = "Cannot filter as provided conditions.";

                    }
                    else
                        error = "Cannot add active column to submission list.";

                }
                else
                    error = "Cannot select Assigned to me in the left menu.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                if (flag == false)
                {
                    error = "Cannot logout system.";
                }
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }    
}
