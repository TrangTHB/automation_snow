﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
namespace Knowledge
{
    class KM_scenario_6_6
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Problem Id: " + problemId);
            System.Console.WriteLine("Finished - KB Submission Id1: " + SubmissionId);
            System.Console.WriteLine("Finished - Knowledge Id1: " + knowledgeId);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        
        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snotextarea textarea;
        snobutton button;
        snoelement ele;
        //------------------------------------------------------------------
        Login login = null;
        Incident inc = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        KnowledgeList kmlist = null;        
        Problem prb = null;
        ProblemList prbList = null;
        Submission submission = null;
        GlobalSearch globalSearch = null;
        //------------------------------------------------------------------
        string problemId;
        string SubmissionId;
        string knowledgeId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************
        //****NOTE: This script is no longer available by DFCT0020420

        //[Test]
        //public void ClassInit()
        //{
        //    try
        //    {
        //        ------------------------------------------------------------------
        //        login = new Login(Base);
        //        home = new Home(Base);
        //        inc = new Incident(Base, "Incident");
        //        knowledge = new SNow.Knowledge(Base, "Knowledge");
        //        kmlist = new KnowledgeList(Base, "Knowledge List");
        //        prb = new Problem(Base, "Problem");
        //        prbList = new ProblemList(Base, "Problem list");
        //        submission = new Submission(Base, "Submission");
        //        globalSearch = new GlobalSearch(Base, "Global search");
        //        ------------------------------------------------------------------

        //        problemId = string.Empty;
        //        SubmissionId = string.Empty;
        //        knowledgeId = string.Empty;
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_001_OpenSystem()
        //{
        //    try
        //    {
        //        Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
        //        login.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_002_Login()
        //{
        //    try
        //    {
        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");
        //        flag = login.LoginToSystem(user, pwd);
        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            flag = false;
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_003_ImpersonateUser_SupportUser()
        //{
        //    try
        //    {
        //        flag = home.ImpersonateUser(Base.GData("SupportUser"));
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_004_SystemSetting()
        //{
        //    try
        //    {
        //        flag = home.SystemSetting();
        //        if (!flag) { error = "Error when config system."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_005_OpenNewProblem()
        //{
        //    try
        //    {
        //        flag = home.LeftMenuItemSelect("Problem", "Create New");
        //        if (flag)
        //        {
        //            prb.WaitLoading();
        //        }
        //        else { error = "Cannot select create new problem"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_006_PopulateCompany()
        //{
        //    try
        //    {
        //        textbox = prb.Textbox_Number();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            flag = textbox.Click();
        //            if (flag)
        //            {
        //                problemId = textbox.Text;
        //            }
        //            else error = "Error when click on textbox number.";
        //        }
        //        else { error = "Cannot get textbox number."; }

        //        --Input company
        //       lookup = prb.Lookup_Company();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {
        //            string company = Base.GData("Company");
        //            flag = lookup.Select(company);
        //            if (!flag) { error = "Cannot populate company value."; }
        //        }
        //        else
        //        { error = "Cannot get textbox company."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_007_01_PopulateProblemImpact()
        //{
        //    try
        //    {
        //        combobox = prb.Combobox_Impact();
        //        string temp = Base.GData("PrbImpact");
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            flag = combobox.SelectItem(temp);
        //            if (!flag) { error = "Cannot populate Problem impact."; }
        //        }
        //        else { error = "Cannot get combobox Problem impact."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_007_02_PopulateProblemStatement()
        //{
        //    try
        //    {
        //        textbox = prb.Textbox_ProblemStatement();
        //        string temp = Base.GData("PrbStatement");
        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            flag = textbox.SetText(temp);
        //            if (!flag) { error = "Cannot populate Problem statement."; }
        //        }
        //        else { error = "Cannot get textbox Problem statement."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_008_PopulateCategoryAndSubcategory()
        //{
        //    try
        //    {
        //        string temp = Base.GData("PrbCat");
        //        combobox = prb.Combobox_Category();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            flag = combobox.SelectItem(temp);
        //            if (flag)
        //            {
        //                prb.WaitLoading();
        //                string sub_cat = Base.GData("PrbSubCat");
        //                combobox = prb.Combobox_Subcategory();
        //                flag = combobox.Existed;
        //                if (flag)
        //                {
        //                    flag = combobox.SelectItem(sub_cat);
        //                    if (!flag) error = "Cannot populate Subcategory value.";
        //                }
        //                else { error = "Cannot get combobox Subcategory."; }
        //            }
        //            else { error = "Cannot populate Category value."; }
        //        }
        //        else { error = "Cannot get combobox Category."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_009_PopulateAssignmentGroup()
        //{
        //    try
        //    {
        //        string temp = Base.GData("AssignmentGroup");
        //        lookup = prb.Lookup_AssignmentGroup();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {
        //            flag = lookup.Select(temp);
        //            if (!flag) { error = "Cannot populate Assignment Group value."; }
        //        }
        //        else { error = "Cannot get lookup Assignment Group."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_010_PopulateDescription()
        //{
        //    try
        //    {
        //        textarea = prb.Textarea_Description();
        //        flag = textarea.Existed;
        //        if (flag)
        //        {
        //            string temp = Base.GData("PrbDescription");
        //            flag = textarea.SetText(temp);
        //            if (!flag) { error = "Cannot populate description value."; }
        //        }
        //        else { error = "Cannot get textarea Description."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_011_01_PopulateWorkAround()
        //{
        //    try
        //    {
        //        textarea = prb.Textarea_Workaround();
        //        flag = textarea.Existed;
        //        if (flag)
        //        {
        //            string temp = Base.GData("PrbWorkAround");
        //            flag = textarea.SetText(temp);
        //            if (!flag) { error = "Cannot populate workaround value or Invalid value."; }
        //        }
        //        else { error = "Cannot get textarea Workaround."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_011_02_Populate_More_Fields_If_Need()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Populate_More_Fields");
        //        if (temp.Trim().ToLower() != "no")
        //        {
        //            flag = prb.Input_Value_For_Controls(temp);
        //            if (!flag)
        //            {
        //                flagExit = false;
        //                error = "Cannot populate more fields.";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Pre_012_SaveProblem()
        //{
        //    try
        //    {
        //        flag = prb.Save();
        //        if (flag)
        //        {
        //            prb.WaitLoading();
        //        }
        //        else { error = "Cannot save problem."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Pre_013_Logout()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //*************************************************************************************************
        //End of Pre-condition

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_001_OpenSystem()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.Driver.Navigate().GoToUrl(temp);
        //        if (flag == false)
        //        {
        //            error = "Cannot logout system.";
        //        }
        //        Thread.Sleep(5000);
        //        login.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_002_Login()
        //{
        //    try
        //    {
        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");
        //        flag = login.LoginToSystem(user, pwd);
        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            flag = false;
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_003_ImpersonateUser_KnowledgeManager()
        //{
        //    try
        //    {
        //        string temp = Base.GData("SupportUser");
        //        flag = home.ImpersonateUser(temp);
        //        if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_004_SystemSetting()
        //{
        //    try
        //    {
        //        flag = home.SystemSetting();
        //        if (!flag) { error = "Error when config system."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}


        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_005_006_SelectProblem()
        //{
        //    try
        //    {

        //        temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && problemId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
        //            addPara.ShowDialog();
        //            problemId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        -----------------------------------------------------------------------------

        //        flag = home.LeftMenuItemSelect("Problem", "All");
        //        if (flag)
        //        {
        //            prbList.WaitLoading();
        //            temp = prbList.List_Title().MyText;
        //            flag = temp.Equals("Problems");
        //            if (flag)
        //            {
        //                temp = "Number;is;" + problemId;
        //                flag = prbList.Filter(temp);
        //                if (flag)
        //                {
        //                    flag = prbList.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
        //                    if (flag)
        //                    {
        //                        prb.WaitLoading();
        //                        if (prb.Textbox_Number().VerifyCurrentValue(problemId))
        //                        {
        //                            temp = Base.GData("Company");
        //                            if (prb.Lookup_Company().VerifyCurrentValue(temp))
        //                            {
        //                                temp = Base.GData("AssignmentGroup");

        //                                if (prb.Lookup_AssignmentGroup().VerifyCurrentValue(temp))
        //                                {
        //                                    combobox = prb.Combobox_Category();
        //                                    temp = Base.GData("PrbCat");
        //                                    if (combobox.VerifyCurrentValue(temp))
        //                                    {
        //                                        combobox = prb.Combobox_Subcategory();
        //                                        temp = Base.GData("PrbSubCat");
        //                                        if (combobox.VerifyCurrentValue(temp))
        //                                        {
        //                                            temp = Base.GData("PrbStatement");
        //                                            if (prb.Textbox_ProblemStatement().VerifyCurrentValue(temp))
        //                                            {
        //                                                temp = Base.GData("PrbDescription");
        //                                                if (!prb.Textarea_Description().VerifyCurrentValue(temp))
        //                                                {
        //                                                    flag = false;
        //                                                    error = "Invalid problem description value.";
        //                                                }
        //                                            }
        //                                            else
        //                                            {
        //                                                flag = false;
        //                                                error = "Invalid problem statement value.";
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            flag = false;
        //                                            error = "Invalid Sub Category value.";
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        flag = false;
        //                                        error = "Invalid Category value.";
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    flag = false;
        //                                    error = "Invalid assignment group value.";
        //                                }
        //                            }
        //                            else
        //                            {
        //                                flag = false;
        //                                error = "Invalid company value.";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            flag = false;
        //                            error = "Incorrect problem number value.";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        error = "Error when search and open problem (id:" + problemId + ")";
        //                    }
        //                }
        //                else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)"; }
        //            }
        //            else error = "Error when select open all problem.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_007_ClickOnPostNews()
        //{
        //    try
        //    {
        //        ele = prb.GRelatedLink("Post News");
        //        flag = ele.Existed;
        //        if (flag)
        //        {
        //            flag = ele.Click(true);
        //            if (flag)
        //            {
        //                prb.WaitLoading();
        //                SubmissionId = prb.GetMessageInfo_TicketNumber();
        //            }
        //            else { error = "Cannot click Post Knowledge related link."; }
        //        }
        //        else { error = "Post News link is not existing"; }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_008_OpenSubmissions()
        //{
        //    try
        //    {
        //        flag = home.LeftMenuItemSelect("Open Submissions", "Open Submissions");
        //        if (flag == true)
        //        {
        //            kmlist.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot open submissions";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_009_SearchSubmission()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && SubmissionId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input KB Submission Id.");
        //            addPara.ShowDialog();
        //            SubmissionId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        ---------------------------------------------------------------------------
        //        string condition = "Number;is;" + SubmissionId;
        //        flag = kmlist.Filter(condition);
        //        if (flag)
        //        {
        //            kmlist.WaitLoading();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_010_SelectSubmission()
        //{
        //    try
        //    {

        //        temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && SubmissionId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input KB Submission Id.");
        //            addPara.ShowDialog();
        //            SubmissionId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        -----------------------------------------------------------------------------
        //        flag = kmlist.SearchAndOpen("Number", SubmissionId, "Number=" + SubmissionId, "Number");
        //        if (flag)
        //        {
        //            submission.WaitLoading();
        //            if (!submission.Textbox_Number().VerifyCurrentValue(SubmissionId))
        //            {
        //                flag = false;
        //                error = "Invalid KB Submission number value.";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_013_VerifyStatusField()
        //{
        //    try
        //    {
        //        if (!submission.Combobox_Status().VerifyCurrentValue("Submitted"))
        //        {
        //            flag = false;
        //            error = "Status is not Submitted.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_014_VerifyParentField()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug");
        //        if (temp == "yes" && problemId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
        //            addPara.ShowDialog();
        //            problemId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        if (!submission.Lookup_Parent().VerifyCurrentValue(problemId))
        //        {
        //            flag = false;
        //            error = "Invalid Parent Number value.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_015_VerifySourceInFilter()
        //{
        //    try
        //    {
        //        if (SubmissionId == string.Empty)
        //        {
        //            SubmissionId = submission.Textbox_Number().Text.Trim();
        //        }
        //        ele = submission.GEle_Source;
        //        temp = ele.MyText;
        //        temp = temp.Substring(temp.Length - 10, 10);
        //        if (temp != SubmissionId)
        //        {
        //            flag = false;
        //            error = string.Format("Invalid Source Number value. Expected: Source = {0} --- Run time: Source = {1}", SubmissionId, temp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_016_VerifyTextContent()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug");
        //        if (temp == "yes" && problemId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Problem Id.");
        //            addPara.ShowDialog();
        //            problemId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        textarea = submission.Textarea_Text();
        //        temp = Base.GData("PrbDescription");

        //        flag = textarea.Existed;
        //        if (flag)
        //        {
        //            if (textarea.MyText.Contains(temp))
        //            {
        //                temp = Base.GData("PrbWorkAround");
        //                if (!textarea.MyText.Contains(temp))
        //                {
        //                    flag = false;
        //                    error = "Text is missing Work Around from Problem.";
        //                }
        //            }
        //            else
        //            {
        //                flag = false;
        //                error = "Text is missing Description from Problem";
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_017_ClickCreateArticleButton()
        //{
        //    try
        //    {
        //        button = submission.Button_CreateArticle();
        //        button.Click();
        //        knowledge.WaitLoading();
        //        Check Knowledge ID
        //        knowledgeId = knowledge.GetMessageInfo_TicketNumber();

        //        flag = knowledge.VerifyMessageInfo("Article " + knowledgeId + " created");
        //        if (!flag)
        //        {
        //            flag = false;
        //            error = "No message for KB Article created";

        //        }

        //        if (knowledge.Textbox_Number().VerifyCurrentValue(knowledgeId))
        //        {
        //            combobox = knowledge.Combobox_KB_Article_type();
        //            if (combobox.VerifyCurrentValue("HTML"))
        //            {
        //                temp = Base.GData("FullPathDomain");
        //                if (knowledge.Lookup_Domain().VerifyCurrentValue(temp))
        //                {

        //                    textarea = submission.Textarea_Text();
        //                    temp = Base.GData("PrbDescription");

        //                    flag = textarea.Existed;
        //                    if (flag)
        //                    {
        //                        if (textarea.MyText.Contains(temp))
        //                        {
        //                            temp = Base.GData("PrbWorkAround");
        //                            if (!textarea.MyText.Contains(temp))
        //                            {
        //                                flag = false;
        //                                error = "Text is missing Work Around from Problem.";
        //                            }
        //                        }
        //                        else
        //                        {
        //                            flag = false;
        //                            error = "Text is missing Description from Problem";
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    flag = false;
        //                    error = "Invalid domain value.";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            flag = false;
        //            error = "Invalid knowledge Id value.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_018_VerifyArticleShortDescriptionandSourceField()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && problemId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
        //            addPara.ShowDialog();
        //            problemId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        Verify Short Description
        //        temp = problemId + " " + Base.GData("PrbStatement");

        //        if (knowledge.Textbox_ShortDescription().Text.Contains(temp))
        //        {
        //            Check Source
        //            temp = Base.GData("Debug").ToLower();
        //            if (temp == "yes" && SubmissionId == string.Empty)
        //            {
        //                Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submission Id.");
        //                addPara.ShowDialog();
        //                SubmissionId = addPara.value;
        //                addPara.Close();
        //                addPara = null;
        //            }
        //            temp = knowledge.Lookup_SourceTask().Text;
        //            if (temp != SubmissionId)
        //            {
        //                flag = false;
        //                error = "Invalid source value.";
        //            }
        //        }
        //        else
        //        {
        //            flag = false;
        //            error = "Invalid short description value.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_019_1_PopulateAssignmentGroup()
        //{
        //    try
        //    {
        //        temp = Base.GData("AssignmentGroup");
        //        if (temp.Equals(string.Empty))
        //        {
        //            flag = false;
        //            error = "There is no any Assignment Group in data file. Please provide";
        //        }
        //        else
        //        {
        //            lookup = knowledge.Lookup_AssignmentGroup();

        //            flag = lookup.Existed;
        //            if (flag)
        //            {
        //                flag = lookup.Select(temp);
        //                knowledge.WaitLoading();
        //                if (!flag)
        //                {
        //                    error = "Cannot select workflow value.";

        //                }
        //            }

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_019_2_PublishKnowledge()
        //{
        //    try
        //    {
        //        Thread.Sleep(3000);
        //        button = knowledge.Button_KA_Publish();

        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            button.Click();
        //            submission.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot find the Publish button.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_019_3_OpenKnowledgePublished()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug").ToLower();

        //        if (temp == "yes" && knowledgeId == string.Empty)
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id.");
        //            addPara.ShowDialog();
        //            knowledgeId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        --------------------------------------------------------------
        //        flag = home.LeftMenuItemSelect("Publish", "Published");
        //        if (flag == true)
        //        {
        //            kmlist.WaitLoading();

        //            flag = kmlist.Filter("Number;is;" + knowledgeId);
        //            if (flag == true)
        //            {
        //                kmlist.WaitLoading();
        //                flag = kmlist.Open("Number=" + knowledgeId, "Number");
        //                if (flag)
        //                {
        //                    if (!knowledge.Combobox_KB_Workflow().VerifyCurrentValue("Published"))
        //                    {
        //                        flag = false;
        //                        error = "Invalid workflow value.";
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            error = "Cannot open knowledge published.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_020_Logout()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(2000);
        //        Base.Driver.Navigate().GoToUrl(temp);
        //        if (flag == false)
        //        {
        //            error = "Cannot logout system.";
        //        }
        //        Thread.Sleep(5000);
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        #endregion
    }
}
