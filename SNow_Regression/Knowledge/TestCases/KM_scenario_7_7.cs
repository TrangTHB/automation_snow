﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;

namespace Knowledge
{
    class KM_scenario_7_7
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Finished - Knowledge Article 01 Id: " + knowledgeId01);
            System.Console.WriteLine("Finished - Knowledge Article 02 Id: " + knowledgeId02);
            System.Console.WriteLine("Finished - Knowledge Article 03 Id: " + knowledgeId03);
            System.Console.WriteLine("Finished - Incident Id: " + incidentId); 
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;        
        snocheckbox checkbox;
        snotextarea textarea;
        snobutton button;
        
        //------------------------------------------------------------------
        Login login = null;
        Incident inc = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        KnowledgeSearch knls = null;
        IncidentList incList = null;
        KnowledgeFeedback knowledgefb = null;
        //------------------------------------------------------------------
        string knowledgeId01, knowledgeId02, knowledgeId03, incidentId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                incList = new IncidentList(Base, "Incident list");
                knls = new KnowledgeSearch(Base);
                knowledgefb = new KnowledgeFeedback(Base, "Knowledge Feedback");
                //------------------------------------------------------------------
                knowledgeId01 = string.Empty;
                knowledgeId02 = string.Empty;
                knowledgeId03 = string.Empty;
                incidentId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_ImpersonateUser_ServiceDesk()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                //temp = Base.GData("FullPathDomain");
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
       
        #region Create a new Incdient

        [Test]
        public void Pre_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_007_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_008_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_009_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }

                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_010_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else { inc.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion Create a new Incdient

        #region Create a new Knowledge Article with Attachment and Attachment link checkbox is checked, then publish it
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_011_Open_NewKnowledge()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when create new Knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_012_Populate_KnowledgeBase()
        {
            try
            {
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    knowledgeId01 = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

                string temp = Base.GData("KnowledgeBase");
                lookup = knowledge.Lookup_Knowledge_base();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Knowledge Base value.";
                    }
                    else { knowledge.WaitLoading(); }
                }
                else { error = "Cannot get Knowledge Base lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_013_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category");
                lookup = knowledge.Lookup_Kb_Category();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Category value."; }
                }
                else { error = "Cannot get Category lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_014_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId01 == null || knowledgeId01 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 1.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = Base.GData("IncShortDescription") + " - " + knowledgeId01;
                textbox = knowledge.Textbox_ShortDescription();
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Short Description value.";
                    }
                }
                else { error = "Cannot get Short Description textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }        
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_015_Add_AttachmentFile()
        {
            try
            {
                string attachmentFile = Base.GData("KA01_Attachment");
                flag = knowledge.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_016_Verify_AttachmentFile_Attached()
        {
            try
            {
                string attachmentFile = Base.GData("KA01_Attachment");
                flag = knowledge.Verify_Attachment_File(attachmentFile);
                if (flag == false)
                {
                    error = "Error whe verify attachmentent file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_017_Populate_Assignmentgroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group value."; }
                }
                else { error = "Cannot get Assignment Group lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_018_Check_AttachmentLink_Checkbox()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Attachment_Link();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click();
                        if (!flag) { error = "Cannot check Attachment Link checkbox."; }
                    }
                    else 
                    { 
                        flag = false; 
                        error = "Checkbox is already checked. Expected: NOT check."; 
                    }
                }
                else { error = "Cannot get Attachment Link checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_019_Save_Knowledge()
        {
            try
            {
                flag = knowledge.Save();
                if (!flag) { error = "Cannot save Knowledge"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_020_Publish_Knowledge()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) { error = "Cannot click Publish button."; }
                    else { knowledge.WaitLoading(); }
                }
                else { error = "Cannot get Publish button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion Create a new Knowledge Article with Attachment and Attachment link checkbox is checked, then publish it

        #region Create a new Knowledge Article WITHOUT Attachment, then publish it
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_021_Open_NewKnowledge()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when create new Knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_022_Populate_KnowledgeBase()
        {
            try
            {
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    knowledgeId02 = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

                string temp = Base.GData("KnowledgeBase");
                lookup = knowledge.Lookup_Knowledge_base();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Knowledge Base value.";
                    }
                    else { knowledge.WaitLoading(); }
                }
                else { error = "Cannot get Knowledge Base lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_023_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category");
                lookup = knowledge.Lookup_Kb_Category();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Category value."; }
                }
                else { error = "Cannot get Category lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_024_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = Base.GData("IncShortDescription") + " - " + knowledgeId02;
                textbox = knowledge.Textbox_ShortDescription();
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Short Description value.";
                    }
                }
                else { error = "Cannot get Short Description textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_025_Populate_Assignmentgroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group value."; }
                }
                else { error = "Cannot get Assignment Group lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_026_Save_Knowledge()
        {
            try
            {
                flag = knowledge.Save();
                if (!flag) { error = "Cannot save Knowledge"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_027_Publish_Knowledge()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) { error = "Cannot click Publish button."; }
                    else { knowledge.WaitLoading(); }
                }
                else { error = "Cannot get Publish button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion Create a new Knowledge Article WITHOUT Attachment, then publish it

        #region Create a new Knowledge Article with Attachment and Display Attachment checkbox is checked, then publish it
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_028_Open_NewKnowledge()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when create new Knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_029_Populate_KnowledgeBase()
        {
            try
            {
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    knowledgeId03 = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

                string temp = Base.GData("KnowledgeBase");
                lookup = knowledge.Lookup_Knowledge_base();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Knowledge Base value.";
                    }
                    else { knowledge.WaitLoading(); }
                }
                else { error = "Cannot get Knowledge Base lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_030_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category");
                lookup = knowledge.Lookup_Kb_Category();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Category value."; }
                }
                else { error = "Cannot get Category lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_031_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId03 == null || knowledgeId03 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 3.");
                    addPara.ShowDialog();
                    knowledgeId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = Base.GData("IncShortDescription") + " - " + knowledgeId03;
                textbox = knowledge.Textbox_ShortDescription();
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Short Description value.";
                    }
                }
                else { error = "Cannot get Short Description textbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_032_Add_AttachmentFile()
        {
            try
            {
                string attachmentFile = Base.GData("KA03_Attachment");
                flag = knowledge.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_033_Verify_AttachmentFile_Attached()
        {
            try
            {
                string attachmentFile = Base.GData("KA03_Attachment");
                flag = knowledge.Verify_Attachment_File(attachmentFile);
                if (flag == false)
                {
                    error = "Error whe verify attachmentent file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_034_Populate_Assignmentgroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group value."; }
                }
                else { error = "Cannot get Assignment Group lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_035_Check_DisplayAttachment_Checkbox()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Display_attachments();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = checkbox.Click();
                        if (!flag) { error = "Cannot check Display Attachment checkbox."; }
                    }
                    else
                    {
                        flag = false;
                        error = "Checkbox is already checked. Expected: NOT check.";
                    }
                }
                else { error = "Cannot get Display Attachment checkbox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_036_Save_Knowledge()
        {
            try
            {
                flag = knowledge.Save();
                if (!flag) { error = "Cannot save Knowledge"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_037_Publish_Knowledge()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) { error = "Cannot click Publish button."; }
                    else { knowledge.WaitLoading(); }
                }
                else { error = "Cannot get Publish button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion Create a new Knowledge Article with Attachment and Display Attachment checkbox is checked, then publish it

        #region Search for Article - Where Attachment link is checked

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_007_SearchAndOpen_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    Thread.Sleep(3000);
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        /* Populate Short description to search Knowledge 01 */
        [Test]
        public void Step_008_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId01 == null || knowledgeId01 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 1.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = Base.GData("IncShortDescription") + " - " + knowledgeId01;
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_Click_SearchKnowledge()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId01 == null || knowledgeId01 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 1.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                button = inc.Button_SearchKnowledge();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        //-- Switch to page 1
                        flag = Base.SwitchToPage(1);
                        if (flag)
                        {
                            temp = Base.GData("IncShortDescription") + " - " + knowledgeId01;
                            textbox = knls.Textbox_Search();
                            flag = textbox.Existed;
                            if (flag)
                            {
                                flag = textbox.VerifyCurrentValue(temp);
                                if (!flag)
                                {
                                    error = "Default value of texbox search knowledge is NOT THE SAME with short description.";
                                    flagExit = false;
                                }
                            }
                            else
                                error = "Cannot get textbox search knowledge.";
                        }
                        else { error = "Error when switch to page 1"; }
                    }
                    else { error = "Cannot click on button search knowledge."; }
                }
                else { error = "Cannot get button search knownledge."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_SearchAndSelect_Article()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId01 == null || knowledgeId01 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 1.");
                    addPara.ShowDialog();
                    knowledgeId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("IncShortDescription") + " - " + knowledgeId01;
                textbox = knls.Textbox_Search();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (flag)
                    {
                        button = knls.Button_Search();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                knls.WaitLoading();
                                flag = knls.SearchAndOpen(temp);
                                if (!flag) { error = "Not found knowledge article."; }                               
                            }
                            else error = "Cannot click on button search.";
                        }
                        else error = "Cannot get button search.";
                    }
                    else { error = "Cannot set value for textbox search knowledge."; }
                }
                else error = "Cannot get textbox search knowledge.";
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_012_Close_AdditionalDialog()
        {
            try
            {
                Base.Driver.Close();
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch page 0.";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        #endregion Search for Article - Where Attachment link is checked

        #region Search for Article - Where Display Attachment is checked

        //-------------------------------------------------------------------------------------------------

        /* Populate Short description to search Knowledge 03 */
        [Test]
        public void Step_013_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId03 == null || knowledgeId03 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 3.");
                    addPara.ShowDialog();
                    knowledgeId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = Base.GData("IncShortDescription") + " - " + knowledgeId03;
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Click_SearchKnowledge()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId03 == null || knowledgeId03 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 3.");
                    addPara.ShowDialog();
                    knowledgeId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                button = inc.Button_SearchKnowledge();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        //-- Switch to page 1
                        flag = Base.SwitchToPage(1);
                        if (flag)
                            {
                                temp = Base.GData("IncShortDescription") + " - " + knowledgeId03;
                                textbox = knls.Textbox_Search();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    flag = textbox.VerifyCurrentValue(temp);
                                    if (!flag)
                                    {
                                        error = "Default value of texbox search knowledge is NOT THE SAME with short description.";
                                        flagExit = false;
                                    }
                                }
                                else
                                    error = "Cannot get textbox search knowledge.";
                        }
                        else { error = "Error when switch to page 1"; }
                    }
                    else { error = "Cannot click on button search knowledge."; }
                }
                else { error = "Cannot get button search knownledge."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_SearchAndSelect_Article()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId03 == null || knowledgeId03 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 3.");
                    addPara.ShowDialog();
                    knowledgeId03 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("IncShortDescription") + " - " + knowledgeId03;
                textbox = knls.Textbox_Search();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (flag)
                    {
                        button = knls.Button_Search();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                knls.WaitLoading();
                                flag = knls.SearchAndOpen(temp);
                                if (!flag) { error = "Not found knowledge article."; }
                            }
                            else error = "Cannot click on button search.";
                        }
                        else error = "Cannot get button search.";
                    }
                    else { error = "Cannot set value for textbox search knowledge."; }
                }
                else error = "Cannot get textbox search knowledge.";
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_Close_AdditionalDialog()
        {
            try
            {
                Base.Driver.Close();
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch page.";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        #endregion

        #region Rate an Article

        //-------------------------------------------------------------------------------------------------

        /* Populate Short description to search Knowledge 02 */
        [Test]
        public void Step_017_Populate_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                temp = Base.GData("IncShortDescription") + " - " + knowledgeId02;
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_Click_SearchKnowledge()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                button = inc.Button_SearchKnowledge();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        //-- Switch to page 1
                        flag = Base.SwitchToPage(1);
                        if (flag)
                            {
                                temp = Base.GData("IncShortDescription") + " - " + knowledgeId02;
                                textbox = knls.Textbox_Search();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    flag = textbox.VerifyCurrentValue(temp);
                                    if (!flag)
                                    {
                                        error = "Default value of texbox search knowledge is NOT THE SAME with short description.";
                                        flagExit = false;
                                    }
                                }
                                else
                                    error = "Cannot get textbox search knowledge.";
                            }
                        else { error = "Error when switch to page 1"; }
                    }
                    else { error = "Cannot click on button search knowledge."; }
                }
                else { error = "Cannot get button search knownledge."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_020_SearchAndSelect_Article()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("IncShortDescription") + " - " + knowledgeId02;
                textbox = knls.Textbox_Search();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (flag)
                    {
                        button = knls.Button_Search();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                knls.WaitLoading();
                                flag = knls.SearchAndOpen(temp);
                                if (!flag) { error = "Not found knowledge article."; }
                            }
                            else error = "Cannot click on button search.";
                        }
                        else error = "Cannot get button search.";
                    }
                    else { error = "Cannot set value for textbox search knowledge."; }
                }
                else error = "Cannot get textbox search knowledge.";
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_022_Rate_Star()
        {
            try
            {

            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_Populate_Comment()
        {
            try
            {
                string temp = Base.GData("Comment");
                textarea = knowledgefb.Textarea_Comment_NoMainFrame();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate Comment value."; }
                }
                else { error = "Cannot get Leave a Comment textare."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_01_Click_AttachToIncident()
        {
            try
            {
                button = knls.Button_Attach();
                flag = button.Existed;
                if (flag) 
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot click Attach to Incident button.";
                    }
                }
                else { error = "Cannot get Attach to Incident button."; }

            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_02_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page 0"; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        /* Verify Work Notes */
        [Test]
        public void Step_025_Verify_WorkNote()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                Thread.Sleep(2000);
                textarea = inc.Textarea_Worknotes();
                flag = textarea.Existed;
                if (flag)
                {
                    temp = textarea.Text;
                    string note = "Knowledge article " + knowledgeId02;
                    if (temp.Contains(note)) 
                    { 
                        error = "Cannot verify Work note value.";
                        flag = false;
                        flagExit = false; 
                    }
                    else
                    {
                        flag = true;
                    }
                }
                else { error = "Cannot get Work Note textarea."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        /* Save Incident*/
        [Test]
        public void Step_026_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Cannot save Incident."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        /* Check the Attached Knowledge link */
        [Test]
        public void Step_027_Check_AttachedKnowledge()
        {
            //try
            //{
            //    //-- Input information
            //    string temp = Base.GData("Debug").ToLower();
            //    if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
            //    {
            //        Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
            //        addPara.ShowDialog();
            //        knowledgeId02 = addPara.value;
            //        addPara.Close();
            //        addPara = null;
            //    }
            //    //-----------------------------------------------------------------------
            //    tab = inc.GTab("Notes");
            //    //---------------------------------------
            //    int i = 0;
            //    while (tab == null && i < 5)
            //    {
            //        Thread.Sleep(2000);
            //        tab = inc.GTab("Notes", true);
            //        i++;
            //    }
            //    flag = tab.Header.Click(true);
            //    if (flag)
            //    {
            //        ele = inc.AttachedKnowledge(null);
            //        flag = ele.Existed;
            //        if (flag)
            //        {
            //            string value1 = ele.Text;                        
            //            temp = knowledgeId02 + " - " + Base.GData("IncShortDescription") + " - " + knowledgeId02;
            //            if (!value1.Equals(temp))
            //            {
            //                flag = false;
            //                flagExit = false;
            //                error = "Attached Knowledge value is not correct. Expected: " + temp;
            //            }
            //        }
            //        else { error = "Cannot get Attached Knowledge element."; }
            //    }
            //    else { error = "Cannot select tab Notes."; }
            //}
            //catch (WebDriverException wex)
            //{
            //    flag = false;
            //    error = wex.Message;
            //}
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_Open_KnowledgeArticle()
        {
            try
            {
            //    ele = inc.AttachedKnowledge(null);
            //    flag = ele.Existed;
            //    if (flag)
            //    {
            //        flag = ele.Click(true);
            //        if (!flag) { error = "Cannot open Knowledge Article."; }
            //    }
            //    else { error = "Cannot get Attached Knowledge element."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_Verify_MostRecentTasks()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId02 == null || knowledgeId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id 2.");
                    addPara.ShowDialog();
                    knowledgeId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //ele = inc.AttachedKnowledge(null);
                //flag = ele.Existed;
                //if (flag)
                //{
                //    string value1 = ele.Text;
                //    temp = Base.GData("IncShortDescription") + " - " + knowledgeId02;
                //    if (!value1.Contains(temp))
                //    {
                //        flag = false;
                //        flagExit = false;
                //        error = "Attached Knowledge value is not correct. Expected: " + temp;
                //    }
                //}
                else { error = "Cannot get Attached Knowledge element."; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
       
        #endregion

        #endregion
    }    
}
