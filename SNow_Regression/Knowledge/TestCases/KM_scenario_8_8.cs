﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;

namespace Knowledge
{
    class KM_scenario_8_8
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Knowledge Id: " + knowledgeId);            
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;        
        snocheckbox checkbox;
        snotextarea textarea;
        snobutton button;
        snodatetime datetime;       
        //------------------------------------------------------------------
        Login login = null;
        Incident inc = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        GlobalSearch globalSearch = null;
        KnowledgeSearch knls = null;
        Submission submission = null;
        KnowledgeList kmlist = null;
        IncidentList incList = null;
        KnowledgeFeedback knowledgefb = null;
        //------------------------------------------------------------------
        string KFTId, knowledgeId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new KnowledgeList(Base, "Knowledge List");
                incList = new IncidentList(Base, "Incident list");
                knls = new KnowledgeSearch(Base);
                submission = new Submission(Base,"Submission");
                globalSearch = new GlobalSearch(Base, "Global search");
                knowledgefb = new KnowledgeFeedback(Base, "Knowledge Feedback");
                //------------------------------------------------------------------
                KFTId = string.Empty;
                knowledgeId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_Resolver()
        {
            try
            {
                string temp = Base.GData("Resolver");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewKnowledge()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when create new knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /// <summary>
        /// Loc Truong Update Rel 12.3
        /// </summary>
        [Test]
        public void Step_006_00_Verify_InformationMessage()
        {
            try
            {
                string temp = Base.GData("InformationMessage");
                flag = knowledge.VerifyMessageInfo(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid message. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_01_GetKnowledgeId()
        {
            try
            {
                //-Get knowledge ID
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag) 
                    {
                        knowledgeId = textbox.Text;     
                    }
                    else { error = "Cannot click textbox number."; }
                }
                else { error = "Cannot get textbox number."; }    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_VerifyDefaultValue_Category()
        {
            try
            {
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    if (lookup.Text != "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid category default value.";
                    }
                }
                else { error = "Not found lookup Category"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_03_VerifyDefaultValue_Published()
        {
            try
            {
                datetime = knowledge.Datetime_Published();
                if (datetime.Existed)
                {                   
                    if (datetime.Text == "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid published default value.";
                    }
                }
                else { error = "Not found datetime field Published"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_04_VerifyDefaultValue_ValidTo()
        {
            try
            {
                snodate date = knowledge.Datetime_ValidTo();
                if (date.Existed)
                {
                    if (date.Text == "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid valid to default value.";
                    }
                }
                else { error = "Not found date valid to."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_05_VerifyDefaultValue_ArticleType()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Article_type();
                if (combobox.Existed)
                {
                    string temp = "HTML";
                    flag = combobox.VerifyCurrentValue(temp);
                    if(!flag)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid article type default value;";
                    }
                }
                else { error = "Not found combobox Article Type"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_06_VerifyDefaultValue_WorkFlow()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Workflow();
                if (combobox.Existed)
                {

                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid workflow default value;";
                    }                    
                }
                else { error = "Not found combobox Workflow"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_07_VerifyDefaultValue_Domain()
        {
            try
            {
                temp = Base.GData("FullPathDomain");
                lookup = knowledge.Lookup_Domain();
                if (lookup.Existed)
                {
                    if(lookup.Text != temp)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid domain default value;";
                    }
                }
                else { error = "Not found lookup Domain"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_08_VerifyDefaultValue_Active()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Active();
                if (checkbox.Existed)
                {
                    if (!checkbox.Checked)
                    {
                        flag = false;
                        error = "Invalid active default value.";
                    }                    
                }
                else { error = "Not found checkbox Active"; }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_09_VerifyDefaultValue_Text()
        {
            try
            {
                bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                temp = textarea.MyText;
                string expected = "SymptomsDescribe symptoms here CauseDescribe cause here ResolutionDescribe resolution here";

                if (!temp.Contains(expected))
                {
                    try 
                    { 
                        expected = "SymptomsDescribe symptoms here CauseDescribe cause here ResolutionDescribe resolution here";
                        if (!temp.Contains(expected))
                        {
                            flagCheck = false;
                            flagExit = false;
                            error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                        }
                        else { flagCheck = true; }
                    }
                    catch { }
                    if (!flagCheck)
                    {
                        flag = false;
                        flagExit = false;
                        error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_InputKnowledgeBase()
        {
            try
            {
                temp = Base.GData("KnowledgeBase");
                lookup = knowledge.Lookup_Knowledge_base();
                if (lookup.Existed)
                {
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        knowledge.WaitLoading();
                    }
                    else { error = "Cannot input knowledge base."; }
                }
                else { error = "Not found lookup Knowledge Base"; }                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_VerifyTextField_Changed()
        {
            try
            {
                bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                temp = textarea.MyText;                
                string expected = "Region or Business Unit: Description of the Issue/Question/Error Message: Workaround/Solution/Answer: Call Capture Information: Routing Information:";

                if (!temp.Contains(expected))
                {
                    try
                    {
                        expected = "Region or Business Unit: Description of the Issue/Question/Error Message: Workaround/Solution/Answer: Call Capture Information: Routing Information:";
                        if (!temp.Contains(expected))
                        {
                            flagCheck = false;
                            flagExit = false;
                            error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                        }
                        else { flagCheck = true; }
                    }
                    catch { }
                    if (!flagCheck)
                    {
                        flag = false;
                        flagExit = false;
                        error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_PopulateCategory()
        {
            try
            {
                temp = Base.GData("Category");
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        knowledge.WaitLoading();
                    }
                    else { error = "Cannot populate combobox Category."; }                    
                }
                else { error = "Cannot get combobox Category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_PopulateShortDescription()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------
                temp = Base.GData("ShortDescription") + " _ " + knowledgeId;
                textbox = knowledge.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {error = "Cannot input short description.";}
                }
                else
                { error = "Not found textbox Short Description"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_01_InputTextField()
        {
            try
            {
                temp = Base.GData("Text");
                textarea = knowledge.Textarea_Text();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp, false, false, true);
                    if (!flag)
                    { error = "Cannot input text field.";}
                }
                else { error = "Cannot get textarea Text"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_02_Populate_AssignmentGroup()
        {
            try
            {
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assignment group value.";
                    }
                }
                else { error = "Cannot get lookup Assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------               


        //-------------------------------------------------------------------------------------------------               

        [Test]
        public void Step_012__SaveKnowledge()
        {
            try
            {
                flag = knowledge.Save();
                if (!flag) { error = "Cannot save knowledge"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_PublishKnowledge()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        kmlist.WaitLoading();
                    }
                    else { error = "Cannot click button Publish"; }
                }
                else { error = "Cannot get button Publish."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = Base.GData("ShortDescription") + " _ " + knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);

                if (flag)
                {
                    Thread.Sleep(5000);
                    globalSearch.WaitLoading();
                    flag = globalSearch.FindItemFromGlobalSearchResult("Knowledge", temp, false);
                    if (!flag)
                    {
                        error = "Not found knowledge in the list";
                    }
                   
                }
                else { error = "Cannot search Knowledge via Global Search field "; }
                              
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_OpenKnowledgeUnderSelfServiceApplication()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                string shortDes = Base.GData("ShortDescription") + " _ " + knowledgeId;
                flag = home.LeftMenuItemSelect("Self-Service", "Knowledge");
                if (flag)
                {
                    globalSearch.WaitLoading();
                    combobox = knowledge.Combobox_AdvancedSearch();
                    if (flag)
                    {
                        temp = Base.GData("KnowledgeBase");
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            globalSearch.WaitLoading();
                            textbox = globalSearch.Textbox_SearchKnowledge();
                            if (textbox.Existed)
                            {
                                flag = textbox.SetText(shortDes, true);
                                Thread.Sleep(5000);
                                if (flag)
                                {
                                    globalSearch.WaitLoading();
                                    flag = globalSearch.OpenArticleFromSelfServiceSearch(shortDes);
                                    if (flag)
                                    {
                                        knowledge.WaitLoading();
                                    }
                                    else { error = "Not found the article in search result list"; }
                                }
                                else { error = "Cannot set text"; }
                            }
                            else { error = "Cannot get textbox Search"; }
                        }
                        else { error = "Cannot populate Advanced Search value."; }
                    }
                    else { error = "Cannot get comboxbox Advanced Search."; }
                }
                else { error = "Cannot select knowledge module from Self-Service application"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                if (flag == false)
                {
                    error = "Cannot logout system.";
                }
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        #endregion
    }    
}
