﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
using SNow;
using OpenQA.Selenium.Interactions;

namespace Knowledge
{
    class KM_e2e_create_article_in_KM_1
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("Knowledge Id: " + knowledgeId);
            System.Console.WriteLine("Knowledge Feedback Task Id: " + KFTId);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snocheckbox checkbox;
        SNow.snotextarea textarea;
        SNow.snobutton button;
        SNow.snodate date;
        SNow.snodatetime datetime;
        SNow.snoelement ele;
        SNow.snolist list;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Incident inc = null;
        SNow.Home home = null;
        SNow.Knowledge knowledge = null;
        SNow.GlobalSearch globalSearch = null;
        SNow.KnowledgeSearch knls = null;
        SNow.Submission submission = null;
        SNow.KnowledgeList kmlist = null;
        SNow.IncidentList incList = null;
        SNow.KnowledgeFeedback knowledgefb = null;
        SNow.Member member = null;
        //------------------------------------------------------------------
        string KFTId, knowledgeId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new SNow.KnowledgeList(Base, "Knowledge List");
                incList = new SNow.IncidentList(Base, "Incident list");
                knls = new SNow.KnowledgeSearch(Base);
                submission = new SNow.Submission(Base,"Submission");
                globalSearch = new SNow.GlobalSearch(Base, "Global Search");
                knowledgefb = new SNow.KnowledgeFeedback(Base, "Knowledge Feedback");
                member = new SNow.Member(Base);
                //------------------------------------------------------------------
                KFTId = string.Empty;
                knowledgeId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_004_ImpersonateUser_KM_User()
        {
            try
            {
                string temp = Base.GData("KM_User");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_SystemSetting()
        {
            try
            {
                string temp = Base.GData("FullPathDomain");
                flag = home.SystemSetting(temp);
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_OpenNewKnowledge()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when create new knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_Verify_InformationMessage()
        {
            try
            {
                string temp = Base.GData("InformationMessage");
                flag = knowledge.VerifyMessageInfo(temp);
                if (!flag)
                {
                    flagExit = false;
                    
                    error = "Invalid message. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_VerifyDefaultValue_KnowledgeBase()
        {
            try
            {
                //-Get knowledge ID
                textbox = knowledge.Textbox_Number();
                if(textbox.Existed)
                {
                    textbox.Click();
                    knowledgeId = textbox.Text;
                }
                
                System.Console.WriteLine("Knowledge Id: " + knowledgeId);
                //-------------------------------
                lookup = knowledge.Lookup_Knowledge_base();
                if (lookup.Existed)
                {
                    if (lookup.Text != "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid knowledge base default value.";
                    }
                }
                else { error = "Not found lookup Knowledge Base"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_03_VerifyDefaultValue_Category()
        {
            try
            {
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    if (lookup.Text != "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid category default value.";
                    }
                }
                else { error = "Not found lookup Category"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_04_VerifyDefaultValue_ValidTo()
        {
            try
            {
                string temp = Base.GData("ValidTo");
                date = knowledge.Date_ValidTo();
                if (date.Existed)
                {
                    if (date.Text == temp)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid valid to default value.";
                    }
                }
                else { error = "Not found datetime field Published"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_05_VerifyDefaultValue_ArticleType()
        {
            try
            {
                string temp = "HTML";
                combobox = knowledge.Combobox_KB_Article_type();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid article type default value;";
                    }
                }else { error = "Not found combobox Article Type"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_06_VerifyDefaultValue_WorkFlow()
        //{
        //    try
        //    {
        //        combobox = knowledge.Combobox_Workflow;
        //        if (combobox.Existed)
        //        {
        //            if(combobox.CurrentValue != "Draft")
        //            flag = false;
        //            flagExit = false;
        //            error = "Invalid workflow default value;";
        //        }
        //        else { error = "Not found combobox Workflow"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_07_VerifyDefaultValue_Domain()
        {
            try
            {
                string temp = Base.GData("FullPathDomain");
                lookup = knowledge.Lookup_Kb_Domain();
                if (lookup.Existed)
                {
                    if(lookup.Text != temp)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid domain default value;";
                    }
                }
                else { error = "Not found lookup Domain"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_08_VerifyDefaultValue_Active()
        //{
        //    try
        //    {
        //        checkbox = knowledge.Checkbox_Active();
        //        if (checkbox.Existed)
        //        {
        //            if (!checkbox.Checked)
        //            flag = false;
        //            error = "Invalid active default value.";
        //        }
        //        else
        //        { error = "Not found checkbox Active"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_09_VerifyDefaultValue_Text()
        {
            try
            {
                // bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                //temp = textarea.Text.Replace("\r\n", "|");
                string temp = textarea.MyText;
                string expected = "SymptomsDescribe symptoms hereCauseDescribe cause hereResolutionDescribe resolution here";
                
                flag = temp.Contains(expected);
                if (flag == false)
                {
                    flagExit = false;
                    error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_10_Verify_CanRead_CannotRead_Existed()
        {
            try
            {
                list = knowledge.List_CanRead();
                flag = list.Existed;
                if (flag)
                {
                    list = knowledge.List_CannotRead();
                    flag = list.Existed;
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found Cannot Read";
                    }
                }
                else { flagExit = false; error = "Not found Can Read"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_InputKnowledgeBase()
        {
            try
            {
                string temp = Base.GData("KnowledgeBase");
                lookup = knowledge.Lookup_Knowledge_base();
                if (lookup.Existed)
                {
                    flag = lookup.Select(temp);
                    knowledge.WaitLoading();
                    if (!flag)
                    {error = "Cannot input knowledge base.";}
                    else { Thread.Sleep(4000); }
                }
                else
                { error = "Not found lookup Knowledge Base"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_InputCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    flag = lookup.Select(temp);
                    knowledge.WaitLoading();
                    if (!flag)
                    {error = "Invalid category.";}
                    else
                    {
                        error = "Cannot input category.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_InputShortDescription()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------
                temp = Base.GData("ShortDescription") + " _ " + knowledgeId;
                textbox = knowledge.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {error = "Cannot input short description.";}
                }
                else
                { error = "Not found textbox Short Description"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_Verify_AssignmentGroup()
        {
            try
            {
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.isMandatory;
                    if (flag == false)
                    {
                        flagExit = false;
                        error = "Assignment Group is not a required field";
                    }
                }
                else { flagExit = false; error = "Cannot get lookup Assignment Group"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_03_Populate_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("Assignment_Group");
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assignment Group";
                    }
                }
                else {  error = "Cannot get lookup Assignment Group"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_04_InputTextField()
        {
            try
            {
                string temp = Base.GData("Text");
                textarea = knowledge.Textarea_Text();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp, false, false, true);
                    if (!flag)
                    { flagExit = false; error = "Cannot input text field."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_05_Populate_KnowledgeArticleNotes()
        {
            try
            {
                string temp = "Knowledge article notes (1)";
                textarea = knowledge.Textarea_KnowledgeArticleNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                    { error = "Cannot input Knowledge article notes field."; }
                } else { error = "Cannot get textarea Knowledge article notes. "; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_AttachWordFile()
        {
            try
            {
                string wordAttachment = "wordAttachment.docx";

                flag = knowledge.Add_AttachmentFile(wordAttachment);
                if (flag == false)
                {
                    error = "Cannot attach word file";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Verify_AttachmentFile()
        {
            try
            {
                string wordAttachment = "wordAttachment.docx";
                flag = knowledge.Verify_Attachment_File(wordAttachment);
                if (!flag)
                {
                    error = "Not found attachment file (" + wordAttachment + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_CheckOnDisplayAttachmentCheckBox()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Display_attachments();
                if (checkbox.Existed && checkbox.Checked == false)
                {
                    flag = checkbox.Click(true);
                    if (!flag)
                    {error = "Cannot check Attachment Link Checkbox";}
                }
                else
                {
                    flag = false;
                    error = "Attachment Link Checkbox is null or checked";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SubmitKnowledge()
        {
            try
            {
                flag = knowledge.Save();
                if (!flag)
                { error = "Cannot submit knowledge"; }
                else
                { kmlist.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_ImpersonateUser_ServiceDeskAgent()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("Service_Desk"), true,temp,false);
                if(!flag)
                { error = "Cannot impersonate SDA"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_OpenKnowledgeEdit()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Edit");
                if (flag)
                {
                    kmlist.WaitLoading();
                }
                else
                {
                    error = "Cannot select knowledge edit.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_SearchAndOpenKnowledge()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------
                temp = "Number=" + knowledgeId;
                flag = kmlist.SearchAndOpen("Number", knowledgeId, temp, "Number");
                if (flag)
                {
                    knowledge.WaitLoading();
                }
                else { error = "Not found Knowledge in the list"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_ClickOn_Edit_Of_AffectedProducts_Tab()
        {
            try
            {
                flag = knowledge.RelatedTab_Click_Button("Affected Products", "@@Edit");
                    if (flag)
                    {
                        member.WaitLoading();
                    }
                    else { error = "Error when click on Edit button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_Add_CI_1()
        {
            try
            {
                string ci1 = Base.GData("CI1");
                flag = member.Add_Members(ci1);
                if (!flag)
                    error = "Error when add ci.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_03_Verify_CI_1_Added()
        {
            try
            {
                string ci1 = Base.GData("CI1");
                string condition = "Configuration item=" + ci1;
                flag = knowledge.Verify_RelatedTable_Row("Affected Products", condition);
                if (!flag)
                { error = "Not found row with condition " + condition; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_ClickOn_Edit_Of_AffectedProducts_Tab()
        {
            try
            {
                flag = knowledge.RelatedTab_Click_Button("Affected Products", "@@Edit");
                if (flag)
                {
                    member.WaitLoading();
                }
                else { error = "Error when click on Edit button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Add_CI_2()
        {
            try
            {
                string ci2 = Base.GData("CI2");
                flag = member.Add_Members(ci2);
                if (!flag)
                    error = "Error when add ci.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_Verify_CI_2_Added()
        {
            try
            {
                string ci2 = Base.GData("CI2");
                string condition = "Configuration item=" + ci2;
                flag = knowledge.Verify_RelatedTable_Row("Affected Products", condition);
                if (!flag)
                { error = "Not found row with condition " + condition; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_ClickOn_Edit_Of_AffectedProducts_Tab()
        {
            try
            {
                flag = knowledge.RelatedTab_Click_Button("Affected Products", "@@Edit");
                if (flag)
                {
                    member.WaitLoading();
                }
                else { error = "Error when click on Edit button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_Add_CI_3()
        {
            try
            {
                string ci3 = Base.GData("CI3");
                flag = member.Add_Members(ci3);
                if (!flag)
                    error = "Error when add ci.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_03_Verify_CI_3_Added()
        {
            try
            {
                string ci3 = Base.GData("CI3");
                string condition = "Configuration item=" + ci3;
                flag = knowledge.Verify_RelatedTable_Row("Affected Products", condition);
                if (!flag)
                { error = "Not found row with condition " + condition; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_01_ClickOn_Edit_Of_AffectedProducts_Tab()
        {
            try
            {
                flag = knowledge.RelatedTab_Click_Button("Affected Products", "@@Edit");
                if (flag)
                {
                    member.WaitLoading();
                }
                else { error = "Error when click on Edit button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_02_Add_CI_1_Again_Expected_Cannot_Add()
        {
            try
            {
                string ci1 = Base.GData("CI1");
                flag = member.Add_Members(ci1);
                if (flag)
                {
                    flag = false;
                    error = "Can add CI 1. Expected Cannot add CI 1.";
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_03_Click_Cancel()
        {
            try
            {
                button = member.Button_Cancel();
                flag = button.Existed;
                if (flag)
                    flag = button.Click();

                if (!flag) error = "Error when click on cancel button.";
                else knowledge.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_01_ClickOn_Edit_Of_AffectedProducts_Tab()
        {
            try
            {
                flag = knowledge.RelatedTab_Click_Button("Affected Products", "@@Edit");
                if (flag)
                {
                    member.WaitLoading();
                }
                else { error = "Error when click on Edit button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_02_Remove_CI_2()
        {
            try
            {
                string ci2 = Base.GData("CI2");
                flag = member.Delete_Members(ci2);
                if (!flag)
                    error = "Error when remove ci.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_03_Verify_CI_2_Removed()
        {
            try
            {
                string ci2 = Base.GData("CI2");
                string condition = "Configuration item=" + ci2;
                flag = knowledge.Verify_RelatedTable_Row("Affected Products", condition, true);
                if (!flag)
                { error = "Found row with condition " + condition; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_ClickOnRoleEditButton()
        {
            try
            {
                ele = knowledge.Button_EditRole();
                if (ele.Existed)
                {
                    ele.Click(true);
                    knowledge.WaitLoading();
                }
                else { error = "Cannot found Edit Role icon"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_AddRole_Itil()
        {
            try
            {
                string temp = "itil";
                flag = knowledge.AddRole(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Cannot add role itil";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_AddRole_Knowlege()
        {
            try
            {
                string temp = "knowledge";
                flag = knowledge.AddRole(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Cannot add role " + temp;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_AddRole_Knowledge_Admin()
        {
            try
            {
                string temp = "knowledge_admin";
                flag = knowledge.AddRole(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Cannot add role " + temp;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_ClickDoneButton()
        {
            try
            {
                button = knowledge.Button_Done();
                flag = button.Existed;
                if (flag)
                {
                    button.Click(true);
                    knowledge.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_ClickPublishButton()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    knowledge.WaitLoading();
                }
                //string temp = "This knowledge item has been published";
                //flag = kmlist.VerifyMessageInfo(temp);
                //System.Console.WriteLine("Message Info: " + temp);
                //if (!flag)
                //{
                //    flagExit = false;
                //    error = "Cannot publish knowledge.";
                //}
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_ImpersonateUser_Resolver_User()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("Resolver"));
                if (!flag)
                { error = "Cannot impersonate Resolver"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = Base.GData("ShortDescription") + " _ " + knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    globalSearch.WaitLoading();
                    flag = globalSearch.FindItemFromGlobalSearchResult("knowledge", temp, false);
                    if (!flag)
                    { error = "Not found knowledge in the list"; }
                }
                else
                { error = "Cannot search Incident via Global Search field "; }

            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_OpenKnowledgeUnderSelfServiceApplication()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = Base.GData("ShortDescription") + " _ " + knowledgeId;
                flag = home.LeftMenuItemSelect("Self-Service", "Knowledge");
                if (flag)
                {
                    globalSearch.WaitLoading();
                    textbox = globalSearch.Textbox_SearchKnowledge();
                    if (textbox.Existed)
                    {
                        flag = textbox.SetText(temp, true);
                        if (flag)
                        {
                            globalSearch.WaitLoading();
                            flag = globalSearch.OpenArticleFromSelfServiceSearch(temp);
                            if (flag)
                            {
                                knowledgefb.WaitLoading();
                            }
                            else { error = "Not found the article in search result list"; }
                        }
                        else { error = "Cannot set text"; }
                    }
                    else
                    { error = "Not found textbox Search"; }
                }
                else
                {
                    error = "Cannot select knowledge module from Self-Service application";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_ClickYesOnFeedback()
        {
            try
            {
                string temp = Base.GData("Useful").Trim().ToLower();
                flag = knowledgefb.SelectHelpful(temp);
                if (!flag)
                {
                    error = "Cannot select helpful";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_SelectRating()
        {
            try
            {
                string temp = Base.GData("Rating");
                int index = int.Parse(temp);
                if (index > 5)
                {
                    error = "Please check rating in Data file. Rating must be on period (1 to 5).";
                    flag = false;
                }
                else
                {
                    flag = knowledgefb.SelectRating(index);
                    if (!flag)
                    {
                        error = "Cannot select rating";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_InputFeedbackComment()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------

                textarea = knowledgefb.Textarea_Comment();
                temp = "Feedback comment - " + knowledgeId;
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (flag)
                    {
                        button = knowledgefb.Button_Comment();
                        if (button.Existed)
                        {
                            button.Click(true);
                            knowledgefb.WaitLoading();
                        }
                        else { error = "Submit comment button not found"; }
                    }
                    else { error = "Cannot input into Comment field"; }
                }
                else { error = "Not found textarea Comment"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_OpenknowledgefbTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //-----------------------------------------------------------

                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                {
                    kmlist.WaitLoading();
                    flag = kmlist.Add_More_Columns("Feedback;Description");
                    if (!flag)
                    {
                        error = "Cannot add one or more columns. Kindly check log";
                    }
                    else
                    {
                        string des = "Feedback comment - " + knowledgeId;
                        temp = "Feedback=" + knowledgeId + "|Description=" + des;
                        flag = kmlist.SearchAndOpen("Feedback", "*" + knowledgeId, temp, "Number");
                        if (flag)
                        {
                            knowledge.WaitLoading();
                        }
                        else { error = "Not found knowledge feedback task."; }
                    }
                }
                else { error = "Cannot select Knowledge Feedback Tasks module"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_VerifyFeedbackTask_Number()
        {
            try
            {
                textbox = knowledge.Textbox_Number();
                if (textbox.Existed)
                {
                    if (textbox.Text.Contains("KFT"))
                    {
                        KFTId = textbox.Text;
                    }
                    else { error = "Invalid knowledge feedback task number."; }
                }
                else { error = "Not found textbox Number"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_VerifyknowledgefbTask_Feedback()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //-----------------------------------------------------------

                lookup = knowledge.Lookup_Feedback();
                if (lookup.Existed)
                {
                    temp = lookup.Text;
                    if (temp != knowledgeId)
                    {
                        flag = false;
                        error = "Invalid feedback value.";
                    }
                }
                else { error = "Not found lookup Feedback"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_VerifyknowledgefbTask_Comment()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------

                textarea = knowledgefb.Textarea_Comment();
                if (textarea.Existed)
                {
                    temp = textarea.Text;
                    if (temp != "Feedback comment - " + knowledgeId)
                    {
                        flag = false;
                        error = "Invalid comment.";
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_ImpersonateUser_KM_User()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(Base.GData("KM_User"), true, temp, false);
                if (!flag)
                { error = "Cannot impersonate Knowledge User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Openknowledge_fb()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Feedback");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when open Feedback list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_SearchandOpenFeedbackUseful()
        {
            try
            {
                //Check knowledge ID is null or not
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = "Article;is;" + knowledgeId + "|and|Useful;is;" + Base.GData("Useful");
                flag = kmlist.Filter(temp);
                if (flag)
                {
                    kmlist.WaitLoading();
                    flag = kmlist.Open("Article=" + knowledgeId, "Created");
                    if (flag)
                    {
                        knowledgefb.WaitLoading();
                    }
                    else { error = "Not found Knowledge Feedback"; }
                }
                else { error = "Cannot add/run filter"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_VerifyFeedbackUseful()
        {
            try
            {
                error = "";

                //Check knowledge ID is null or not
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //Verify Created
                System.Console.WriteLine("----- Verify Created Field");
                datetime = knowledgefb.Datetime_Created();
                if (datetime.Existed)
                {
                    if (datetime.Text == "")
                    {
                        flag = false;
                        flagExit = false;
                        error += "Created is empty";
                    }
                }
                else
                {
                    flag = false;
                    error += "Cannot get Created element";
                }

                //Verify Article
                System.Console.WriteLine("---- Verify Article Field");
                lookup = knowledgefb.Lookup_Article();
                if (lookup.Existed)
                {
                    temp = lookup.Text;
                    if (temp != knowledgeId)
                    {
                        flag = false;
                        error += string.Format("Article value is incorrect. Expected {0} | Actual: {1}", knowledgeId, temp);
                    }
                    System.Console.WriteLine(string.Format("Expected: {0} | Actual: {1}", knowledgeId, temp));
                }
                else
                {
                    flag = false;
                    error += "Cannot get lookup Article";
                }

                //Verify User
                System.Console.WriteLine("---- Verify User Field");
                lookup = knowledgefb.Lookup_User();
                if (lookup.Existed)
                {
                    temp = lookup.Text;
                    string user = Base.GData("Resolver");
                    if (temp != user)
                    {
                        flag = false;
                        error += string.Format("User is incorrect. Expected {0} | Actual: {1}", temp, user);
                    }
                    System.Console.WriteLine(string.Format("Expected: {0} | Actual: {1}", temp, user));
                }
                else
                {
                    flag = false;
                    error += "Cannot get lookup User";
                }

                //Verify Comment
                System.Console.WriteLine("--------Verify Comment Field");
                textarea = knowledgefb.Textarea_Comment();
                if (textarea.Existed)
                {
                    temp = textarea.Text;
                    if (temp != "")
                    {
                        flag = false;
                        error += "Comment is NOT blank";
                    }
                    System.Console.WriteLine(string.Format("Expected: BLANK | Actual: {0}", temp == "" ? "BLANK" : temp));
                }
                else
                {
                    flag = false;
                    error += "Cannot get Comment";
                }

                //Verify Useful
                System.Console.WriteLine("------Verify Useful Field");
                combobox = knowledgefb.Combobox_Useful();
                string useful = Base.GData("Useful");
                if (combobox.Existed)
                {
                    temp = combobox.Text;
                    if (temp != useful)
                    {
                        error += string.Format("Useful field is incorrect. Expected: {0} | Actual: {1}", temp, useful);
                    }
                    System.Console.WriteLine(string.Format("Expected: {0} | Actual: {1}", temp, useful));
                }
                else
                {
                    flag = false;
                    error += "Cannot get combobox Useful";
                }

                //Verify Rating
                System.Console.WriteLine("------Verify Rating Field");
                textbox = knowledgefb.Textbox_Rating();
                if (textbox.Existed)
                {
                    temp = textbox.Text;
                    if (temp != "")
                    {
                        flag = false;
                        error += "Rating is not blank";
                    }
                    System.Console.WriteLine(string.Format("Expected: BLANK | Actual: {0}", temp == "" ? "BLANK" : temp));
                }
                else
                {
                    flag = false;
                    error += "Cannot get textbox Rating";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Openknowledgefb()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Feedback");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when open Feedback list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_SearchandOpenFeedbackRating()
        {
            try
            {
                //Check knowledge ID is null or not
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = "Article;is;" + knowledgeId + "|and|Rating;is;" + Base.GData("Rating");
                flag = kmlist.Filter(temp);
                if (flag)
                {
                    kmlist.WaitLoading();
                    flag = kmlist.Open("Article=" + knowledgeId, "Created");
                    if (flag)
                    {
                        knowledgefb.WaitLoading();
                    }
                    else { error = "Not found Knowledge Feedback"; }
                }
                else { error = "Cannot add/run filter"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_VerifyFeedbackRating()
        {
            try
            {
                //Check knowledge ID is null or not
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //Verify Created
                System.Console.WriteLine("----- Verify Created Field");
                datetime = knowledgefb.Datetime_Created();
                if (datetime.Existed)
                {
                    if (datetime.Text == "")
                    {
                        flag = false;
                        flagExit = false;
                        error += "Created is empty";
                    }
                }
                else
                {
                    flag = false;
                    error += "Cannot get Created element";
                }

                //Verify Article
                System.Console.WriteLine("---- Verify Article Field");
                lookup = knowledgefb.Lookup_Article();
                if (lookup.Existed)
                {
                    temp = lookup.Text;
                    if (temp != knowledgeId)
                    {
                        flag = false;
                        error += string.Format("Article value is incorrect. Expected {0} | Actual: {1}", knowledgeId, temp);
                    }
                    System.Console.WriteLine(string.Format("Expected: {0} | Actual: {1}", knowledgeId, temp));
                }
                else
                {
                    flag = false;
                    error += "Cannot get lookup Article";
                }

                //Verify User
                System.Console.WriteLine("---- Verify User Field");
                lookup = knowledgefb.Lookup_User();
                if (lookup.Existed)
                {
                    temp = lookup.Text;
                    string user = Base.GData("Resolver");
                    if (temp != user)
                    {
                        flag = false;
                        error += string.Format("User is incorrect. Expected {0} | Actual: {1}", temp, user);
                    }
                    System.Console.WriteLine(string.Format("Expected: {0} | Actual: {1}", temp, user));
                }
                else
                {
                    flag = false;
                    error += "Cannot get lookup User";
                }

                //Verify Comment
                System.Console.WriteLine("--------Verify Comment Field");
                textarea = knowledgefb.Textarea_Comment();
                if (textarea.Existed)
                {
                    temp = textarea.Text;
                    if (temp != "")
                    {
                        flag = false;
                        error += "Comment is NOT blank";
                    }
                    System.Console.WriteLine(string.Format("Expected: BLANK | Actual: {0}", temp == "" ? "BLANK" : temp));
                }
                else
                {
                    flag = false;
                    error += "Cannot get Comment";
                }

                //Verify Useful
                System.Console.WriteLine("------Verify Useful Field");
                combobox = knowledgefb.Combobox_Useful();
                //string useful = "";
                if (combobox.Existed)
                {
                    temp = combobox.Text;
                    if (temp != "-- None --")
                    {
                        flag = false;
                        error += "Invalid Useful";
                    }
                    System.Console.WriteLine("Expected: [-- None --]; Runtime:[" + temp + "]");
                }
                else
                {
                    flag = false;
                    error += "Cannot get combobox Useful";
                }

                //Verify Rating
                System.Console.WriteLine("------Verify Rating Field");
                textbox = knowledgefb.Textbox_Rating();
                if (textbox.Existed)
                {
                    temp = textbox.Text;
                    string rating = Base.GData("Rating");
                    if (temp != rating)
                    {
                        flag = false;
                        error += "Rating is incorrect";
                    }
                    System.Console.WriteLine(string.Format("Expected: {0} | Actual: {1}", rating, temp));
                }
                else
                {
                    flag = false;
                    error += "Cannot get textbox Rating";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_OpenKnowledgePublised()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Published");
                if (flag == true)
                {
                    kmlist.WaitLoading();
                }
                else
                {
                    error = "Cannot open publised knowledge list .";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_SearchAndOpenKnowledge_Published()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //-----------------------------------------------------------
                temp = "Number=" + knowledgeId;
                flag = kmlist.SearchAndOpen("Number", knowledgeId, temp, "Number");
                if (flag)
                {
                    knowledge.WaitLoading();
                }
                else { error = "Not found knowledge in the published list"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_ClickOnRetireButton()
        {
            try
            {
                button = knowledge.Button_Retire();
                if (button.Existed)
                {
                    button.Click(true);
                    IAlert alert = Base.Driver.SwitchTo().Alert();
                    if (alert.Text.Trim() != "You are retiring this knowledge article and it cannot be republished. Do you wish to continue?")
                    {
                        flag = false;
                        error = "Invalid message info.";
                    }
                    else
                    {
                        alert.Accept();
                        knowledge.WaitLoading();
                    }
                    knowledge.WaitLoading();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_CheckMessageInfo()
        {
            try
            {
                string temp = "This knowledge item has been retired";
                flag = knowledge.VerifyMessageInfo(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid message info.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_OpenKnowledgeRetired()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Retired");
                if (flag == true)
                { kmlist.WaitLoading(); }
                else
                { error = "Cannot knowledge retired."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_SearchAndOpenKnowledge_Retired()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //-----------------------------------------------------------

                temp = "Number=" + knowledgeId;
                flag = kmlist.SearchAndOpen("Number", knowledgeId, temp, "Number");
                if (flag)
                {
                    knowledge.WaitLoading();
                    //combobox = knowledge.Combobox_Workflow;
                    //if (combobox.CurrentValue != "Retired")
                    //{
                    //   flag = false;
                    //    error = "Invalid work flow value.";
                    //}

                }
                else { error = "Not found knowledge in the Retired list"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }    
}
