﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;

namespace Knowledge
{
    class KM_assignment_11
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Knowledge Id: " + knowledgeId);

            // should have output for KTF here
            int i = 0;
            foreach(string id in KFTid_arr)
            {
                    i += 1;
                    System.Console.WriteLine("Knowledge Feedback Task "+ i +": "+id);
            }
            //----------------------------------------------------------------
            KFTid_arr = null;


            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;

        snocheckbox checkbox;
        snotextarea textarea;
        snobutton button;
        snodate date;

        //------------------------------------------------------------------
        Login login = null;
        Incident inc = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        KnowledgeSearch knls = null;
        Submission submission = null;
        KnowledgeList kmlist = null;
        IncidentList incList = null;
        KnowledgeFeedback knowledgefb = null;
        EmailList emailList = null;
        Email email = null;
        ItilList taskslaList = null;
        Itil tasksla = null;
        //------------------------------------------------------------------
        string knowledgeId;
        string[] KFTid_arr;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new KnowledgeList(Base, "Knowledge List");
                incList = new IncidentList(Base, "Incident list");
                knls = new KnowledgeSearch(Base);
                submission = new Submission(Base, "Submission");
                knowledgefb = new KnowledgeFeedback(Base, "Knowledge Feedback");
                emailList = new EmailList(Base,"Email List");
                email = new Email(Base, "Email");
                taskslaList = new ItilList(Base,"Task SLA List");
                tasksla = new Itil(Base,"Task SLA Defination");
                //------------------------------------------------------------------
                knowledgeId = string.Empty;
                KFTid_arr = new string[5];
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_004_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KM_User");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_SystemSetting()
        {
            try
            {
                string temp = Base.GData("FullPathDomain");
                flag = home.SystemSetting(temp);
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_OpenNewKnowledge()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Create New");
                if (flag)
                    knowledge.WaitLoading();
                else
                    error = "Error when create new knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_01_VerifyDefaultValue_KnowledgeBase()
        {
            try
            {
                //Get knowledge ID
                knowledgeId = knowledge.Textbox_Number().Text;
                System.Console.WriteLine("Knowledge Id: " + knowledgeId);

                lookup = knowledge.Lookup_Knowledge_base();
                if (lookup.Existed)
                {
                    if (lookup.Text != "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid knowledge base default value.";
                    }
                }
                else { flag = false; error = "Not found lookup Knowledge Base"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_02_VerifyDefaultValue_Category()
        {
            try
            {
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    if (lookup.Text != "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid category default value.";
                    }
                }
                else { flag = false; error = "Not found lookup Category"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_007_03_VerifyDefaultValue_Published()
        //{
        //    try
        //    {
        //        datetime = knowledge.Datetime_Published;
        //        if (datetime.Existed)
        //        {
        //            if (datetime.Text == "")
        //            {
        //                flag = false;
        //                flagExit = false;
        //                error = "Invalid published default value.";
        //            }
        //        }
        //        else { flag = false; error = "Not found datetime field Published"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_03_VerifyDefaultValue_ValidTo()
        {
            try
            {
                date = knowledge.Datetime_ValidTo();
                if (date.Existed)
                {
                    if (date.Text == "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid valid to default value.";
                    }
                }
                else { flag = false; error = "Not found datetime field Published"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_05_VerifyDefaultValue_ArticleType()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Article_type();
                if (combobox.Existed)
                {
                    if (!combobox.VerifyCurrentValue("HTML"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid article type default value;";
                    }
                }
                else { flag = false; error = "Not found combobox Article Type"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_007_06_VerifyDefaultValue_WorkFlow()
        //{
        //    try
        //    {
        //        combobox = knowledge.Combobox_Workflow;
        //        if (combobox.Existed)
        //        {
        //            if (combobox.CurrentValue != "Draft")
        //                flag = false;
        //            flagExit = false;
        //            error = "Invalid workflow default value;";
        //        }
        //        else { flag = false; error = "Not found combobox Workflow"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_06_VerifyDefaultValue_Domain()
        {
            try
            {
                string temp = Base.GData("FullPathDomain");
                lookup = knowledge.Lookup_Kb_Domain();
                if (lookup.Existed)
                {
                    if (lookup.Text != temp)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid domain default value;";
                    }
                }
                else { flag = false; error = "Not found lookup Domain"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_08_VerifyDefaultValue_Active()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Active();
                if (checkbox.Existed)
                {
                    if (!checkbox.Checked)
                        flag = false;
                    error = "Invalid active default value.";
                }
                else
                { flag = false; error = "Not found checkbox Active"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_09_VerifyDefaultValue_Text()
        {
            try
            {
                bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                string temp = textarea.MyElement.Text.Replace("\r\n", "|");
                string expected = "Symptoms|Describe symptoms here| |Cause|Describe cause here| |Resolution|Describe resolution here";

                if (!temp.Contains(expected))
                {
                    try
                    {
                        expected = "Symptoms|Describe symptoms here| Cause|Describe cause here| Resolution|Describe resolution here";
                        if (!temp.Contains(expected))
                        {
                            flagCheck = false;
                            flagExit = false;
                            error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                        }
                        else { flagCheck = true; }
                    }
                    catch { }
                    if (!flagCheck)
                    {
                        flag = false;
                        flagExit = false;
                        error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_VerifyAssignmentGroup_Mandatory()
        {
            try
            {
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.isMandatory;
                    if (!flag)
                        error = "-*- ERROR: The Assignment Group is not Mandatory field.";
                }
                else
                    error = "Cannot get lookup assignment group.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_Input_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("Assignment_Group");
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                        error = "Cannot input Assignment Group data.";
                }
                else
                    error = "Cannot get lookup assignment group.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------


        [Test]
        public void Step_010_01_InputKnowledgeBase()
        {
            try
            {
                string temp = Base.GData("KnowledgeBase");
                lookup = knowledge.Lookup_Knowledge_base();
                if (lookup.Existed)
                {
                    flag = lookup.Select(temp);
                    knowledge.WaitLoading();
                    if (!flag)
                    { error = "Cannot input knowledge base."; }
                }
                else
                { flag = false; error = "Not found lookup Knowledge Base"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_02_InputCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    bool Tflag = lookup.ReadOnly;
                    int i = 0;
                    while (i < 5 && Tflag)
                    {
                        Thread.Sleep(500);
                        Tflag = lookup.ReadOnly;
                        i++;
                    }
                    if (!Tflag)
                    {
                        flag = lookup.Select(temp);
                        knowledge.WaitLoading();
                        if (!flag)
                        { error = "Cannot input category."; }
                    }
                    else
                    {
                        flag = false;
                        error = "Category is Readonly field.";
                    }
                }
                else
                { flag = false; error = "Cannot get lookup category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_03_InputShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription") + " _ " + knowledgeId;
                textbox = knowledge.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    { error = "Cannot input short description."; }
                }
                else
                { error = "Not found textbox Short Description"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_010_04_InputTextField()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Text");
        //        textarea = knowledge.Textarea_Text();
        //        flag = textarea.Existed;
        //        if (flag)
        //        {
        //            flag = textarea.SetText(temp);
        //            if (!flag)
        //            { error = "Cannot input text field."; }
        //        }
        //        else
        //            error = "Cannot get text field.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_SaveKnowledge()
        {
            try
            {
                flag = knowledge.Save(true);
                if (!flag)
                { error = "Cannot save knowledge"; }
                else
                { knowledge.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
       
        [Test]
        public void Step_012_ClickPublishButton()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        kmlist.WaitLoading();
                    }
                    else { error = "Cannot click button Publish"; }
                }
                else { error = "Cannot get button Publish."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Global_Search_Knowledge()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;
                Thread.Sleep(3000);
                //flag = knls.GSearchItem(temp, true);
                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Knowledge via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SelectRating_1()
        {
            try
            {
                flag = knowledgefb.SelectRating(1);
                if (!flag)
                {
                    error = "Cannot select rating";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_Navigate_toKnowledgeFBTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                    kmlist.WaitLoading();
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_SearchANDOpenKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------------------------
                flag = kmlist.Add_More_Columns("Feedback");
                if (flag)
                {
                    flag = kmlist.SearchAndOpen("Feedback", "*" + knowledgeId, "Feedback=" + knowledgeId, "Number");
                    if (!flag)
                        error = "Cannot open feedback task knowledge article - " + knowledgeId;
                    else
                        knowledge.WaitLoading();
                }
                else
                {
                    error = "Cannot add Feedback column in to the list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_VerifyAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("Assignment_Group");
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                        error = "Incorrect assignment group value.Expected - " + temp;
                }
                else
                    error = "Cannot get lookup assignment group";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_OpenPrivateBrowser()
        {
            try
            {
                Base.ActiveWindow();
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    KFTid_arr[0] = textbox.Text;
                    string temp = Base.GData("Type").ToLower();
                    if (temp == "ff")
                    {
                        textbox.MyElement.SendKeys(Keys.LeftControl + Keys.Shift + "P");
                        flag = Base.SwitchToPage(1);
                    }
                    else if (temp == "chr")
                    {

                        System.Windows.Forms.SendKeys.SendWait("^+{N}");
                        flag = Base.SwitchToPage(1);
                    }
                    if (!flag)
                        error = "Cannot switch to page (1).";

                }
                else { error = "Cannot get attached knowledge control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Login()
        {
            try
            {
                //Open the URL
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));

                login.WaitLoading();
                //--------------------------------------------------
                //Log IN
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_FilterEmail_AssignedKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback Task Id01.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + KFTid_arr[0] + "|and|Subject;contains;assigned to group " + Base.GData("Assignment_Group");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_OpenEmail_AssignedKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id01.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + KFTid_arr[0];
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to member of assignment group.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_VerifySubject_AssignedKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id01.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Knowledge Feedback Task " + KFTid_arr[0] + " has been assigned to group " + Base.GData("Assignment_Group");
                flag = email.VerifySubject(temp);
                if (!flag)
                    error = "Incorrect subject. Expected [" + temp + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId == string.Empty || knowledgeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFT01 Id.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string exp_knowledgeinfo = "Knowledge Article: " + knowledgeId + "|Short description: " + Base.GData("ShortDescription")+" _ "+knowledgeId + "|Click here to view Knowledge Feedback Task: " + KFTid_arr[0];
                flag = email.VerifyEmailBody(exp_knowledgeinfo);
                if (flag)
                {
                    string exp_fbinfo = "Rating: 1|Helpful:|Assignment group: " + Base.GData("Assignment_Group") + "|Comments:";
                    flag = email.VerifyEmailBody(exp_fbinfo);
                    if (!flag)
                        error = "Incorret feedback's information.";
                }
                else
                    error = "Incorrect knowledge information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_03_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_SwitchToFirstPage()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_SelectRating_2()
        {
            try
            {
                flag = knowledgefb.SelectRating(2);
                if (!flag)
                {
                    error = "Cannot select rating";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Navigate_toKnowledgeFBTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                    kmlist.WaitLoading();
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_SearchANDOpenKFT02()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                flag = kmlist.Add_More_Columns("Feedback");
                if (flag)
                {
                    flag = kmlist.Filter("Assignment group;is;" + Base.GData("Assignment_Group") + "|and|Number;is not;" + KFTid_arr[0]);
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Feedback", "*" + knowledgeId, "Feedback=" + knowledgeId, "Number");
                        if (!flag)
                            error = "Cannot open feedback task knowledge article - " + knowledgeId;
                        else
                            knowledge.WaitLoading();
                    }
                    else
                        error = "Cannot filter the condition.";
                }
                else
                {
                    error = "Cannot add Feedback column in to the list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_VerifyAssignmentGroup()
        {
            try
            {
                //get KFTid02
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    KFTid_arr[1] = textbox.Text;
                    string temp = Base.GData("Assignment_Group");
                    lookup = knowledge.Lookup_AssignmentGroup();
                    flag = lookup.Existed;

                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp);
                        if (!flag)
                            error = "Incorrect assignment group value.Expected - " + temp;
                    }
                    else
                        error = "Cannot get lookup assignment group";
                }
                else
                    error = "Cannot get textbox number.";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_SwitchToSecondPage()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_FilterEmail_AssignedKFT02()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[1] == string.Empty || KFTid_arr[1] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId02.");
                    addPara.ShowDialog();
                    KFTid_arr[1] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + KFTid_arr[1] + "|and|Subject;contains;assigned to group " + Base.GData("Assignment_Group");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_OpenEmail_AssignedKFT02()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[1] == string.Empty || KFTid_arr[1] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTid02.");
                    addPara.ShowDialog();
                    KFTid_arr[1] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + KFTid_arr[1];
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to member of assignment group.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_VerifySubject_AssignedKFT02()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[1] == string.Empty || KFTid_arr[1] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id02.");
                    addPara.ShowDialog();
                    KFTid_arr[1] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Knowledge Feedback Task " + KFTid_arr[1] + " has been assigned to group " + Base.GData("Assignment_Group");
                flag = email.VerifySubject(temp);
                if (!flag)
                    error = "Incorrect subject. Expected [" + temp + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_01_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_02_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId == string.Empty || knowledgeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                if (temp == "yes" && (KFTid_arr[1] == string.Empty || KFTid_arr[1] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFT02 Id.");
                    addPara.ShowDialog();
                    KFTid_arr[1] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string exp_knowledgeinfo = "Knowledge Article: " + knowledgeId + "|Short description: " + Base.GData("ShortDescription") + " _ " + knowledgeId + "|Click here to view Knowledge Feedback Task: " + KFTid_arr[1];
                System.Console.WriteLine(exp_knowledgeinfo);
                flag = email.VerifyEmailBody(exp_knowledgeinfo);
                if (flag)
                {
                    string exp_fbinfo = "Rating: 2|Helpful:|Assignment group: " + Base.GData("Assignment_Group") + "|Comments:";
                    flag = email.VerifyEmailBody(exp_fbinfo);
                    if (!flag)
                        error = "Incorret feedback's information.";
                }
                else
                    error = "Incorrect knowledge information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_03_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_SwitchToFirstPage()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_SelectRating_3()
        {
            try
            {
                flag = knowledgefb.SelectRating(3);
                if (!flag)
                {
                    error = "Cannot select rating";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Navigate_toKnowledgeFBTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                    kmlist.WaitLoading();
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SearchANDOpenKFT03()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                string conditions = string.Empty;
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                for (int i = 0; i < 2; i++)
                {
                    if (temp == "yes" && (KFTid_arr[i] == string.Empty || KFTid_arr[i] == null))
                    {

                        Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId0" + (i + 1));
                        addPara.ShowDialog();
                        KFTid_arr[i] = addPara.value;
                        addPara.Close();
                        addPara = null;
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                    else
                    {
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                }
                //--------------------------------------------
                flag = kmlist.Add_More_Columns("Feedback");
                if (flag)
                {
                    flag = kmlist.Filter("Assignment group;is;" + Base.GData("Assignment_Group") + conditions);
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Feedback", "*" + knowledgeId, "Feedback=" + knowledgeId, "Number");
                        if (!flag)
                            error = "Cannot open feedback task knowledge article - " + knowledgeId;
                        else
                            knowledge.WaitLoading();
                    }
                    else
                        error = "Cannot filter the condition.";
                }
                else
                {
                    error = "Cannot add Feedback column in to the list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_VerifyAssignmentGroup()
        {
            try
            {
                //get KFTid03
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    KFTid_arr[2] = textbox.Text;
                    string temp = Base.GData("Assignment_Group");
                    lookup = knowledge.Lookup_AssignmentGroup();
                    flag = lookup.Existed;

                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp);
                        if (!flag)
                            error = "Incorrect assignment group value.Expected - " + temp;
                    }
                    else
                        error = "Cannot get lookup assignment group";
                }
                else
                    error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_SwitchToSecondPage()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_FilterEmail_AssignedKFT03()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[2] == string.Empty || KFTid_arr[2] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId03.");
                    addPara.ShowDialog();
                    KFTid_arr[2] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + KFTid_arr[2] + "|and|Subject;contains;assigned to group " + Base.GData("Assignment_Group");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_OpenEmail_AssignedKFT03()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[2] == string.Empty || KFTid_arr[2] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id03.");
                    addPara.ShowDialog();
                    KFTid_arr[2] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + KFTid_arr[2];
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to member of assignment group.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_VerifySubject_AssignedKFT03()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[2] == string.Empty || KFTid_arr[2] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTid02.");
                    addPara.ShowDialog();
                    KFTid_arr[2] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Knowledge Feedback Task " + KFTid_arr[2] + " has been assigned to group " + Base.GData("Assignment_Group");
                flag = email.VerifySubject(temp);
                if (!flag)
                    error = "Incorrect subject. Expected [" + temp + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId == string.Empty || knowledgeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                if (temp == "yes" && (KFTid_arr[2] == string.Empty || KFTid_arr[2] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFT03 Id.");
                    addPara.ShowDialog();
                    KFTid_arr[2] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string exp_knowledgeinfo = "Knowledge Article: " + knowledgeId + "|Short description: " + Base.GData("ShortDescription") + " _ " + knowledgeId + "|Click here to view Knowledge Feedback Task: " + KFTid_arr[2];
                System.Console.WriteLine(exp_knowledgeinfo);
                flag = email.VerifyEmailBody(exp_knowledgeinfo);
                if (flag)
                {
                    string exp_fbinfo = "Rating: 3|Helpful:|Assignment group: " + Base.GData("Assignment_Group") + "|Comments:";
                    flag = email.VerifyEmailBody(exp_fbinfo);
                    if (!flag)
                        error = "Incorret feedback's information.";
                }
                else
                    error = "Incorrect knowledge information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_03_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_SwitchToFirstPage()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Select_Helpful_NO()
        {
            try
            {
                flag = knowledgefb.SelectHelpful("no");
                if (!flag)
                {
                    error = "Cannot select helpful.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Navigateto_KnowledgeFBTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                    kmlist.WaitLoading();
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_SearchANDOpenKFT04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                string conditions = string.Empty;
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                for (int i = 0; i < 3; i++)
                {
                    if (temp == "yes" && (KFTid_arr[i] == string.Empty || KFTid_arr[i] == null))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId0" + (i + 1));
                        addPara.ShowDialog();
                        KFTid_arr[i] = addPara.value;
                        addPara.Close();
                        addPara = null;
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                    else
                    {
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                }
                //--------------------------------------------
                flag = kmlist.Add_More_Columns("Feedback");
                if (flag)
                {
                    flag = kmlist.Filter("Assignment group;is;" + Base.GData("Assignment_Group") + conditions);
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Feedback", "*" + knowledgeId, "Feedback=" + knowledgeId, "Number");
                        if (!flag)
                            error = "Cannot open feedback task knowledge article - " + knowledgeId;
                        else
                            knowledge.WaitLoading();
                    }
                    else
                        error = "Cannot filter the condition.";
                }
                else
                {
                    error = "Cannot add Feedback column in to the list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_VerifyAssignmentGroup()
        {
            try
            {
                //get KFTid04
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    KFTid_arr[3] = textbox.Text;
                    string temp = Base.GData("Assignment_Group");
                    lookup = knowledge.Lookup_AssignmentGroup();
                    flag = lookup.Existed;

                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp);
                        if (!flag)
                            error = "Incorrect assignment group value.Expected - " + temp;
                    }
                    else
                        error = "Cannot get lookup assignment group";
                }
                else
                    error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_SwitchToSecondPage()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_FilterEmail_AssignedKFT04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[3] == string.Empty || KFTid_arr[3] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId04.");
                    addPara.ShowDialog();
                    KFTid_arr[3] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + KFTid_arr[3] + "|and|Subject;contains;assigned to group " + Base.GData("Assignment_Group");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_OpenEmail_AssignedKFT04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[3] == string.Empty || KFTid_arr[3] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTid03.");
                    addPara.ShowDialog();
                    KFTid_arr[3] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + KFTid_arr[3];
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to member of assignment group.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_VerifySubject_AssignedKFT04()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[3] == string.Empty || KFTid_arr[3] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id04.");
                    addPara.ShowDialog();
                    KFTid_arr[3] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Knowledge Feedback Task " + KFTid_arr[3] + " has been assigned to group " + Base.GData("Assignment_Group");
                flag = email.VerifySubject(temp);
                if (!flag)
                    error = "Incorrect subject. Expected [" + temp + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_01_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_02_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId == string.Empty || knowledgeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                if (temp == "yes" && (KFTid_arr[3] == string.Empty || KFTid_arr[3] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFT03 Id.");
                    addPara.ShowDialog();
                    KFTid_arr[3] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string exp_knowledgeinfo = "Knowledge Article: " + knowledgeId + "|Short description: " + Base.GData("ShortDescription") + " _ " + knowledgeId + "|Click here to view Knowledge Feedback Task: " + KFTid_arr[3];
                System.Console.WriteLine(exp_knowledgeinfo);
                flag = email.VerifyEmailBody(exp_knowledgeinfo);
                if (flag)
                {
                    string exp_fbinfo = "Rating:|Helpful: No|Assignment group: " + Base.GData("Assignment_Group") + "|Comments:";
                    flag = email.VerifyEmailBody(exp_fbinfo);
                    if (!flag)
                        error = "Incorret feedback's information.";
                }
                else
                    error = "Incorrect knowledge information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_03_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_SwitchToFirstPage()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Provide_Comment()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------

                textarea = knowledgefb.Textarea_Comment();
                temp = "Feedback comment - " + knowledgeId;
                flag = textarea.Existed; 
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (flag)
                    {
                        button = knowledgefb.Button_Comment();
                        if (button.Existed)
                        {
                            button.Click(true);
                            knowledgefb.WaitLoading();
                        }
                        else { error = "Submit comment button not found"; }
                    }
                    else { error = "Cannot input into Comment field"; }
                }
                else { error = "Not found textarea Comment"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_Navigate_toKnowledgeFBTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                    kmlist.WaitLoading();
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_SearchANDOpenKFT05()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                string conditions = string.Empty;
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                for (int i = 0; i < 4; i++)
                {
                    if (temp == "yes" && (KFTid_arr[i] == string.Empty || KFTid_arr[i] == null))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId0" + (i + 1));
                        addPara.ShowDialog();
                        KFTid_arr[i] = addPara.value;
                        addPara.Close();
                        addPara = null;
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                    else
                    {
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                }
                //---------------------------------------------------------------------------------------------------------
                flag = kmlist.Add_More_Columns("Feedback");
                if (flag)
                {
                    flag = kmlist.Filter("Assignment group;is;" + Base.GData("Assignment_Group") + conditions);
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Feedback", "*" + knowledgeId, "Feedback=" + knowledgeId, "Number");
                        if (!flag)
                            error = "Cannot open feedback task knowledge article - " + knowledgeId;
                        else
                            knowledge.WaitLoading();
                    }
                    else
                        error = "Cannot filter the condition.";
                }
                else
                {
                    error = "Cannot add Feedback column in to the list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_VerifyAssignmentGroup()
        {
            try
            {
                //get KFTid05
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    KFTid_arr[4] = textbox.Text;
                    string temp = Base.GData("Assignment_Group");
                    lookup = knowledge.Lookup_AssignmentGroup();
                    flag = lookup.Existed;

                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp);
                        if (!flag)
                            error = "Incorrect assignment group value.Expected - " + temp;
                    }
                    else
                        error = "Cannot get lookup assignment group";
                }
                else
                    error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_SwitchToSecondPage()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_FilterEmail_AssignedKFT05()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[4] == string.Empty || KFTid_arr[4] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId05.");
                    addPara.ShowDialog();
                    KFTid_arr[4] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + KFTid_arr[4] + "|and|Subject;contains;assigned to group " + Base.GData("Assignment_Group");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_OpenEmail_AssignedKFT05()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[4] == string.Empty || KFTid_arr[4] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTid04.");
                    addPara.ShowDialog();
                    KFTid_arr[4] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + KFTid_arr[4];
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to member of assignment group.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_VerifySubject_AssignedKFT05()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[4] == string.Empty || KFTid_arr[4] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id05.");
                    addPara.ShowDialog();
                    KFTid_arr[4] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Knowledge Feedback Task " + KFTid_arr[4] + " has been assigned to group " + Base.GData("Assignment_Group");
                flag = email.VerifySubject(temp);
                if (!flag)
                    error = "Incorrect subject. Expected [" + temp + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_01_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_02_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId == string.Empty || knowledgeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                if (temp == "yes" && (KFTid_arr[4] == string.Empty || KFTid_arr[4] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFT05 Id.");
                    addPara.ShowDialog();
                    KFTid_arr[4] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string exp_knowledgeinfo = "Knowledge Article: " + knowledgeId + "|Short description: " + Base.GData("ShortDescription") + " _ " + knowledgeId + "|Click here to view Knowledge Feedback Task: " + KFTid_arr[4];
                System.Console.WriteLine(exp_knowledgeinfo);
                flag = email.VerifyEmailBody(exp_knowledgeinfo);
                if (flag)
                {
                    string comment = "Feedback comment - " + knowledgeId;
                    string exp_fbinfo = "Rating:|Helpful:|Assignment group: " + Base.GData("Assignment_Group") + "|Comments: "+comment;
                    flag = email.VerifyEmailBody(exp_fbinfo);
                    if (!flag)
                        error = "Incorret feedback's information.";
                }
                else
                    error = "Incorrect knowledge information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_03_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_SwitchToFirstPage()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);

                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_SelectRating_4()
        {
            try
            {
                flag = knowledgefb.SelectRating(4);
                if (!flag)
                    error = "Cannot select rating 4";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_SelectRating_5()
        {
            try
            {
                flag = knowledgefb.SelectRating(5);
                if (!flag)
                    error = "Cannot select rating 5";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_SearchKnowledgeUseGlobalSearch()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------
                temp = knowledgeId;

                flag = knowledge.GlobalSearchItem(temp, true);
                if (flag)
                {
                    knowledgefb.WaitLoading();
                }
                else
                { error = "Cannot search Incident via Global Search field "; }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_Select_Helpful_Yes()
        {
            try
            {
                flag = knowledgefb.SelectHelpful("Yes");
                if (!flag)
                {
                    error = "Cannot select helpful.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_Navigate_toKnowledgeFBTasks()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                    kmlist.WaitLoading();
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_VerifyNOTaskGenerated()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                string conditions = string.Empty;
                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                for (int i = 0; i < 5; i++)
                {
                    if (temp == "yes" && (KFTid_arr[i] == string.Empty || KFTid_arr[i] == null))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTId0" + (i + 1));
                        addPara.ShowDialog();
                        KFTid_arr[i] = addPara.value;
                        addPara.Close();
                        addPara = null;
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                    else
                    {
                        conditions = conditions + "|and|Number;is not;" + KFTid_arr[i];
                    }
                }

                //--------------------------------------------
                flag = kmlist.Add_More_Columns("Feedback");
                if (flag)
                {
                    flag = kmlist.Filter("Feedback;is;" + knowledgeId + conditions);
                    if (flag)
                    {
                        int count = kmlist.Table_List().RowCount;
                        if (count > 0)
                        { error = "Unexpected row displayd."; flag = false; }
                    }
                    else
                        error = "Could not filter the condition.";
                }
                else
                {
                    error = "Cannot add Feedback column in to the list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_01_SearchANDOpenKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge feedback Id01.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Knowledge", "Knowledge Feedback Tasks");
                if (flag)
                {
                    kmlist.WaitLoading();
                    flag = kmlist.SearchAndOpen("Number", KFTid_arr[0], "Number=" + KFTid_arr[0], "Number");
                    if (!flag)
                        error = "Cannot open feedback task knowledge article - " + KFTid_arr[0];
                    else
                        knowledge.WaitLoading();
                }
                else
                    error = "Cannot select knowlege feedback tasks module";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_02_SwitchToSecondPage()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KM_User");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_085_AccessSLATaskList()
        {
            try
            {
                textbox = home.Textbox_Filter();
                flag = textbox.Existed;
                if(flag)
                {
                    flag = textbox.SetText("task_sla.list",true);
                    if (!flag)
                        error = "Cannot input task to text filter.";
                    else
                        taskslaList.WaitLoading();
                    
                }
                else
                error  = "Cannot get textbox filter on left menu.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_SearchAndOpenRecord_RelatedKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id01.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                flag = taskslaList.SearchAndOpen("Task", KFTid_arr[0], "Task=" + KFTid_arr[0], "Stage");
                if (!flag)
                    error = "Cannot open record with condition.";
                else
                    tasksla.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_01_VerifySLADefinition()
        {
            try
            {
                string temp = Base.GData("SLADefinition");
                lookup = tasksla.Lookup_SLADefinition();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                        error = "Incorrect SLA Definition";
                }
                else
                    error = "Cannot get lookup sla definition.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_02_VerifyStage()
        {
            try
            {
                combobox = tasksla.Combobox_Stage();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In progress");
                    if (!flag)
                    {
                        error = "Incorrect stage value.";
                        flagExit = false;
                    }
                        
                }
                else
                    error = "Cannot get combobox stage.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_03_VerifySchedule()
        {
            try
            {
                lookup = tasksla.Lookup_Schedule();
                flag = lookup.Existed;

                if (flag)
                {
                    flag = lookup.VerifyCurrentValue("24x5 Weekdays");
                    if (!flag)
                    {
                        error = "Incorrect schedule value.";
                        flag = false;
                    }
                }
                else
                    error = "Cannot get lookup schedule.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_04_VerifyTimezone()
        {
            try
            {
                string temp = Base.GData("Timezone");
                combobox = tasksla.Combobox_Timezone();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Incorrect timezone value.";
                        flagExit = false;
                    }
                }
                else
                    error = "Cannot get combobox timezone.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_ChangeKFT01ToCompleteState()
        {
            try
            {
                string temp = Base.GData("KFTCloseState");
                combobox = knowledge.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Cannot select closed state.";
                    else
                    {
                        flag = knowledge.Save();
                        if (!flag)
                            error = "Cannot save knowledge feedback task.";
                    }
                }
                else
                    error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_AccessSLATaskList()
        {
            try
            {
                textbox = home.Textbox_Filter();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText("task_sla.list", true);
                    if (!flag)
                        error = "Cannot input task to text filter.";
                    else
                        taskslaList.WaitLoading();

                }
                else
                    error = "Cannot get textbox filter on left menu.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_SearchAndVerify_RelatedKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id01.");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                flag = taskslaList.SearchAndVerify("Task", KFTid_arr[0], "Task=" + KFTid_arr[0] + "|Stage=Completed|SLA=" + Base.GData("SLADefinition"));
                if (!flag)
                    error = "Cannot find record with completed state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_093_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------------------------------------
        [Test]
        public void Step_094_ChangeKFT01ToInprogress()
        {
            try
            {
                string temp = Base.GData("OpenState");
                combobox = knowledge.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Cannot select work in progress state.";
                    else
                    {
                        flag = knowledge.Save();
                        if (!flag)
                            error = "Cannot save knowledge feedback task.";
                    }
                }
                else
                    error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------------------------------------
        [Test]
        public void Step_095_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------------------------------------
        [Test]
        public void Step_096_SearchAndVerify_RelatedKFT01()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (KFTid_arr[0] == string.Empty || KFTid_arr[0] == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Feedback task Id01");
                    addPara.ShowDialog();
                    KFTid_arr[0] = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                flag = taskslaList.SearchAndVerify("Task", KFTid_arr[0], "Task=" + KFTid_arr[0] + "|Stage=Completed|SLA=" + Base.GData("SLADefinition"));
                if (!flag)
                    error = "Cannot find record with completed state.";
                else
                {
                    flag = taskslaList.SearchAndVerify("Task", KFTid_arr[0], "Task=" + KFTid_arr[0] + "|Stage=In progress|SLA=" + Base.GData("SLADefinition"));
                    if (!flag)
                        error = "Cannot find record with in progress state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_097_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }
}
