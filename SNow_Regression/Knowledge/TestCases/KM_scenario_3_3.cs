﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;

namespace Knowledge
{
    class KM_scenario_3_3
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Problem Id: " + problemId);
            System.Console.WriteLine("Knowledge Id 1: " + knowledgeId1);
            System.Console.WriteLine("Knowledge Id 2: " + knowledgeId2);
            System.Console.WriteLine("Knowledge Submission Id 1: " + SubmissionId);
            System.Console.WriteLine("Knowledge Submission Id 2: " + kbSubmissionId2);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;        
        snocheckbox checkbox;
        snotextarea textarea;
        snobutton button;
        snodate date;
        snoelement ele;
        //------------------------------------------------------------------
        Login login = null;
        Incident inc = null;
        Home home = null;
        SNow.Knowledge knowledge = null;
        KnowledgeList kmlist = null;
        Problem prb = null;
        ProblemList prbList = null;

        SNow.Submission submission = null;
        ItilList taskList = null;
        Itil taskSLA = null;
        //------------------------------------------------------------------
        string knowledgeId1, knowledgeId2, problemId;
        string SubmissionId, kbSubmissionId2;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new KnowledgeList(Base, "Knowledge List");   
                prb = new Problem(Base, "Problem");
                prbList = new ProblemList(Base, "Problem list");
                submission = new Submission(Base, "Submission");
                taskList = new ItilList(Base, "Task list");
                taskSLA = new Itil(Base, "Task SLA");
                //------------------------------------------------------------------
                knowledgeId1 = string.Empty;
                knowledgeId2 = string.Empty;
                problemId = string.Empty;
                SubmissionId = string.Empty;
                kbSubmissionId2 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
       
        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_003_ImpersonateUser_ProblemManager()
        {
            try
            {
                string temp = Base.GData("ProblemManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_005_OpenNewProblem()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Cannot select create new problem"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_006_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    problemId = textbox.Text;
                }
                else error = "Textbox number is not existed.";

                //-- Input company
                string temp = Base.GData("Company");
                lookup = prb.Lookup_Company();
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get lookup Company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_007_PopulateProblemStatement()
        {
            try
            {
                textbox = prb.Textbox_ProblemStatement();
                string temp = Base.GData("ProStatement");
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate Problem statement."; }
                }
                else { error = "Cannot get textbox Problem statement."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_008_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("ProCat");
                combobox = prb.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_008_01_PopulateSubcategory()
        {
            try
            {
                string temp = Base.GData("ProSubCat");
                combobox = prb.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate subcategory value."; }
                }
                else
                {
                    error = "Cannot get combobox subcategory.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_009_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("ProAssignmentGroup");
                lookup = prb.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment Group value."; }
                }
                else { error = "Cannot get lookup Assignment Group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_010_PopulateDescription()
        {
            try
            {
                textarea = prb.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProDescription");
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea Description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_011_PopulateWorkAround()
        {
            try
            {
                textarea = prb.Textarea_Workaround();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ProWorkAround");
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate workaround value or Invalid value."; }
                }
                else { error = "Cannot get textarea Workaround."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_012_PopulateImpact()
        {
            try
            {
                string temp = Base.GData("ProImpact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate impact value."; }
                }
                else
                {
                    error = "Cannot get combobox impact.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_013_PopulateProblemSource()
        {
            try
            {
                string temp = Base.GData("ProSource");
                combobox = prb.Combobox_ProblemSource();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate problem source value."; }
                }
                else
                {
                    error = "Cannot get combobox problem source.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_014_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KMUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_002_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_004_SearchAndOpenProblem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "All");
                if (flag)
                {
                    prbList.WaitLoading();
                    temp = prbList.List_Title().MyText.ToLower();
                    flag = temp.Equals("problems");
                    if (flag)
                    {
                        flag = prbList.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + problemId + ")";
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)"; }
                }
                else error = "Error when select open all problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_01_ClickOnPostKnowledge()
        {
            try
            {
                ele = knowledge.GRelatedLink("Post Knowledge");
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click(true);
                    if (flag)
                    {
                        kmlist.WaitLoading();
                    }
                    else { error = "Cannot click Post Knowledge related link."; }
                }
                else { error = "Cannot get Post Knowledge Related Link"; }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_02_GetSubmissionNumber()
        {
            try
            {
                SubmissionId = knowledge.GetMessageInfo_TicketNumber();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_007_SearchAndOpen_KnowledgeSubmission()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KB Submissions 01.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Submissions", "Open Submissions");
                if (flag)
                {
                    kmlist.WaitLoading();
                    temp = kmlist.List_Title().MyText.ToLower();
                    flag = temp.Equals("kb submissions");
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Number", SubmissionId, "Number=" + SubmissionId, "Number");
                        if (!flag) error = "Error when search and open knowledge submission (id:" + SubmissionId + ")";
                        else kmlist.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(KB Submissions)";
                    }
                }
                else { error = "Cannot select open submissions."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_VerifyParentField()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = submission.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(problemId);
                    if (!flag)
                    {
                        error = "Cannot verify parent value or Invalid value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get lookup parent."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_Verify_KnowledgeSubmissionText()
        {
            try
            {
                bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                temp = textarea.MyElement.Text.Replace("\r\n", "|");

                string description = Base.GData("ProDescription");
                string workA = Base.GData("ProWorkAround");

                //Verify Description
                string expected = "Description:|" + description;
                if (temp.Contains(expected))
                {
                    try
                    {
                        //Verify Workaround
                        expected =  workA;
                        if (!temp.Contains(expected))
                        {
                            flagCheck = false;
                            flagExit = false;
                            error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                        }
                        else { flagCheck = true; }
                    }
                    catch { }
                    if (!flagCheck)
                    {
                        flag = false;
                        flagExit = false;
                        error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_VerifyStatusCombobox()
        {
            try
            {
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Submitted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify status value or Invalid value.";
                    }
                }
                else { error = "Cannot get combobox Status."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_001_Click_CreateArticaleButton()
        {
            try
            {
                button = submission.Button_CreateArticle();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        knowledge.WaitLoading();
                    }
                    else { error = "Cannot click button Article."; }
                }
                else { error = "Cannot get button Article."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_002_GetKnowledgeId()
        {
            try
            {
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    //Get Knowledge ticket number
                    knowledgeId1 = textbox.Text;
                }
                else { error = "Cannot get textbox Number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_003_VerifyDefaultValue_Category()
        {
            try
            {
                lookup = knowledge.Lookup_Kb_Category();
                if (lookup.Existed)
                {
                    if (lookup.Text != "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid category default value.";
                    }
                }
                else { error = "Not found lookup Category"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        //[Test]
        //public void Step_011_004_VerifyDefaultValue_Published()
        //{
        //    try
        //    {
        //        datetime = knowledge.Datetime_Published();
        //        if (datetime.Existed)
        //        {
        //            if (datetime.Text == "")
        //            {
        //                flag = false;
        //                flagExit = false;
        //                error = "Invalid published default value.";
        //            }
        //        }
        //        else { error = "Not found datetime field Published"; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_005_VerifyDefaultValue_SourceTask()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submission Id.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = knowledge.Lookup_SourceTask();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(SubmissionId);
                    if (!flag) { error = "Cannot verify source value or Invalid source."; flagExit = false; }
                }
                else { error = "Cannot get lookup Source."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_006_VerifyDefaultValue_ValidTo()
        {
            try
            {
                date = knowledge.Datetime_ValidTo();
                if (date.Existed)
                {
                    if (date.Text == "")
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid valid to default value.";
                    }
                }
                else { error = "Not found datetime field Published"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_007_VerifyDefaultValue_ArticleType()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Article_type();
                if (combobox.Existed)
                {
                    string temp = "HTML";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid article type default value;";
                    }
                }
                else { error = "Not found combobox Article Type"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_008_VerifyDefaultValue_WorkFlow()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Workflow();
                if (combobox.Existed)
                {

                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid workflow default value;";
                    }
                }
                else { error = "Not found combobox Workflow"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_009_VerifyDefaultValue_Domain()
        {
            try
            {
                temp = Base.GData("FullPathDomain");
                lookup = knowledge.Lookup_Kb_Domain();
                if (lookup.Existed)
                {
                    if (lookup.Text != temp)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid domain default value;";
                    }
                }
                else { error = "Not found lookup Domain"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_010_Verify_KnowledgeText()
        {
            try
            {
                bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                temp = textarea.MyElement.Text.Replace("\r\n", "|");

                string description = Base.GData("ProDescription");
                string workA = Base.GData("ProWorkAround");

                //Verify Description
                string expected = "Description:|" + description;
                if (temp.Contains(expected))
                {
                    try
                    {
                        //Verify Workaround
                        expected = workA;
                        if (!temp.Contains(expected))
                        {
                            flagCheck = false;
                            flagExit = false;
                            error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                        }
                        else { flagCheck = true; }
                    }
                    catch { }
                    if (!flagCheck)
                    {
                        flag = false;
                        flagExit = false;
                        error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_011_Verify_ShortDescription()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string shortDes = Base.GData("ProStatement");
                temp = problemId + " " + shortDes;

                textbox = knowledge.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify short description or Invalid value."; flagExit = false; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_00_PopulateAssignmentGroup()
        {
            try
            {

                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("Assignment_Group"));
                    if (!flag)
                    {
                        error = "Cannot select Assignment Group value.";
                    }
                }
                else
                    error = "Cannot get lookup assignment group.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_01_PublishKnowledge()
        {
            try
            {
                button = knowledge.Button_KA_Publish();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        kmlist.WaitLoading();
                    }
                    else { error = "Cannot click button Publish"; }
                }
                else { error = "Cannot get button Publish."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_02_SearchAndOpen_Knowledge()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (knowledgeId1 == null || knowledgeId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge 1.");
                    addPara.ShowDialog();
                    knowledgeId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Knowledge", "Published");
                if (flag)
                {
                    kmlist.WaitLoading();
                    temp = kmlist.List_Title().MyText.ToLower();
                    flag = temp.Equals("knowledge");
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Number", knowledgeId1, "Number=" + knowledgeId1, "Number");
                        if (!flag) error = "Error when search and open Knowledge (id:" + knowledgeId1 + ")";
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Knowledge)"; }
                }
                else error = "Error when select open all Knowledge.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_03_Verify_KnowledgeWorkflow()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Workflow();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Published";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify workflow or Invalid workflow."; flagExit = false; }
                }
                else { error = "Cannot get combobox Workflow."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_SearchandOpen_Problem()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "All");
                if (flag)
                {
                    prbList.WaitLoading();
                    temp = prbList.List_Title().MyText.ToLower();
                    flag = temp.Equals("problems");
                    if (flag)
                    {
                        flag = prbList.SearchAndOpen("Number", problemId, "Number=" + problemId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + problemId + ")";
                    }
                    else { error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)"; }
                }
                else error = "Error when select open all problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Check_Knowledge_Checkbox()
        {
            try
            {
                checkbox = prb.Checkbox_Create_Knowledge();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Checked;
                    if (!flag)
                    {
                        flag = true;
                        flag = checkbox.Click();
                        if (!flag)
                        {
                            error = "Cannot check Knowledge checkbox.";
                        }
                    }
                }
                else { error = "Cannot get checkbox Knowledge."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_PopulateState()
        {
            try
            {
                combobox = prb.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed/Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_017_Populate_CloseCode_CloseNotes()
        {
            try
            {
                combobox = prb.Combobox_CauseCode();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PrbCauseCode");
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        combobox = prb.Combobox_ClosureCode();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = Base.GData("PrbClosureCode");
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                textarea = prb.Textarea_CloseNotes();
                                flag = textarea.Existed;
                                if (flag)
                                {
                                    temp = Base.GData("PrbCloseNote");
                                    flag = textarea.SetText(temp);
                                    if (!flag) { error = "Cannot populate Close Notes value."; }
                                }
                                else { error = "Cannot get textarea Close Notes."; }
                            }
                        }
                    }
                    else { error = "Cannot populate Close Code value."; }
                }
                else { error = "Cannot get combobox Close Code."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (flag)
                {
                    prb.WaitLoading();
                }
                else { error = "Cannot save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_GetSubmissionNumber()
        {
            try
            {
                kbSubmissionId2 = prb.GetMessageInfo_TicketNumber();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_023_Open_TaskSLAList()
        {
            try
            {
                textbox = home.Textbox_Filter();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = "task_sla.list";
                    flag = textbox.SetText(temp, true);
                    if (flag)
                    {
                        taskList.WaitLoading();
                        temp = taskList.List_Title().MyText.ToLower(); ;
                        flag = temp.Equals("task slas");
                        if (!flag)
                        {
                            error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Task SLAs)";
                        }
                    }
                    else { error = "Cannot open Task SLA list."; }
                }
                else
                {
                    error = "Cannot get Filter textbox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_025_SearchAndOpen_KnowledgeSubmission_1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 1.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Task=" + SubmissionId + "|SLA=SUB-CLOSE-Global|Stage=In progress";
                flag = kmlist.SearchAndOpen("Task", SubmissionId, condition, "Stage");
                if (!flag) error = "Error when search and open knowledge submission (id:" + SubmissionId + ")";
                else submission.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_01_Verify_SLADefinition()
        {
            try
            {
                lookup = taskSLA.Lookup_SLADefinition();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("SLA_Definition");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify SLA Definition value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get SLA Definition lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_02_Verify_Stage()
        {
            try
            {
                combobox = taskSLA.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Stage value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Stage combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

//        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_01_Verify_Schedule()
        {
            try
            {
                lookup = taskSLA.Lookup_Schedule();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Schedule");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Schedule value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Schedule lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_02_Verify_Timezone()
        {
            try
            {
                combobox = taskSLA.Combobox_Timezone();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Timezone");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Timezone value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Timezone combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_03_OpenNewPrivateBrowser()
        {
            try
            {
                lookup = taskSLA.Lookup_SLADefinition();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Type").ToLower();
                    if (temp == "ff")
                    {
                        lookup.MyElement.SendKeys(Keys.LeftControl + Keys.Shift + "P");
                        flag = Base.SwitchToPage(1);
                    }
                    else if (temp == "chr")
                    {

                        Base.SwitchToPage(0);
                        Base.ActiveWindow();

                        System.Windows.Forms.SendKeys.SendWait("^+{N}");
                        Thread.Sleep(2000);
                        flag = Base.SwitchToPage(1);
                    }

                    if (!flag)
                        error = "Cannot swicth to page (1).";
                }
                else { error = "Cannot get SLA Definition lookup."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_04_Login()
        {
            try
            {
                //Open the URL
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));

                login.WaitLoading();

                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_05_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KMUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_06_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_07_Open_TaskSLAList()
        {
            try
            {
                textbox = home.Textbox_Filter();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = "task_sla.list";
                    flag = textbox.SetText(temp, true);
                    if (flag)
                    {
                        taskList.WaitLoading();
                        temp = taskList.List_Title().MyText.ToLower(); ;
                        flag = temp.Equals("task slas");
                        if (!flag)
                        {
                            error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Task SLAs)";
                        }
                    }
                    else { error = "Cannot open Task SLA list."; }
                }
                else
                {
                    error = "Cannot get Filter textbox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_08_SearchAndOpen_KnowledgeSubmission_1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 1.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Task=" + SubmissionId + "|SLA=SUB-CLOSE-Global|Stage=In progress";
                flag = kmlist.SearchAndOpen("Task", SubmissionId, condition, "Task");
                if (!flag) error = "Error when search and open knowledge submission (id:" + SubmissionId + ")";
                else submission.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_00_Populate_Status_Close_Invalid()
        {
            try
            {
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Closed, Invalid";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot select item [" + temp + "]"; }
                }
                else { error = "Cannot get Status combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_01_Save_Submission()
        {
            try
            {

                flag = submission.Save();
                if (!flag) { error = "Cannot save KB submission."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_02_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page 0."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_03_ReloadForm()
        {
            try
            {
                flag = taskSLA.ReloadForm();
                if (!flag) { error = "Cannot reload form."; }
                else { taskSLA.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_04_Verify_SLADefinition()
        {
            try
            {
                lookup = taskSLA.Lookup_SLADefinition();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("SLA_Definition");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify SLA Definition value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get SLA Definition lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_05_Verify_Stage()
        {
            try
            {
                combobox = taskSLA.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Completed";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Stage value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Stage combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page 1."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_02_Populate_Status()
        {
            try
            {
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Assigned";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot select item [" + temp + "]"; }
                }
                else { error = "Cannot get Status combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_03_Save_Submission()
        {
            try
            {
                flag = submission.Save();
                if (!flag) { error = "Cannot save KB submission."; }
                else { submission.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_04_Close_Page()
        {
            try
            {
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_05_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page 0."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_06_Open_TaskSLAList()
        {
            try
            {
                textbox = home.Textbox_Filter();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = "task_sla.list";
                    flag = textbox.SetText(temp, true);
                    if (flag)
                    {
                        taskList.WaitLoading();
                        temp = taskList.List_Title().MyText.ToLower();
                        flag = temp.Equals("task slas");
                        if (!flag)
                        {
                            error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Task SLAs)";
                        }
                    }
                    else { error = "Cannot open Task SLA list."; }
                }
                else
                {
                    error = "Cannot get Filter textbox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------


        [Test]
        public void Step_030_SearchAndVerify_SubmissionGenerated()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (SubmissionId == null || SubmissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 1.");
                    addPara.ShowDialog();
                    SubmissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Task=" + SubmissionId + "|SLA=SUB-CLOSE-Global|Stage=Completed";
                flag = kmlist.SearchAndVerify("Task", SubmissionId, condition);
                if (flag)
                {
                    condition = "Task=" + SubmissionId + "|SLA=SUB-CLOSE-Global|Stage=In progress";
                    flag = kmlist.SearchAndVerify("Task", SubmissionId, condition);
                    if (!flag)
                    {
                        error = "Error when search and verify KB submission with condition: " + condition;
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Error when search and verify KB submission with condition: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_to_033_SearchAndOpen_KnowledgeSubmission_2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (kbSubmissionId2 == null || kbSubmissionId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 2.");
                    addPara.ShowDialog();
                    kbSubmissionId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Submissions", "Open Submissions");
                if (flag)
                {
                    kmlist.WaitLoading();
                    temp = kmlist.List_Title().MyText.ToLower();
                    flag = temp.Equals("kb submissions");
                    if (flag)
                    {
                        flag = kmlist.SearchAndOpen("Number", kbSubmissionId2, "Number=" + kbSubmissionId2, "Number");
                        if (!flag) error = "Error when search and open knowledge submission (id:" + kbSubmissionId2 + ")";
                        else submission.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(KB Submissions)";
                    }
                }
                else { error = "Cannot select open submissions."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_VerifyParentField()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (problemId == null || problemId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem Id.");
                    addPara.ShowDialog();
                    problemId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                lookup = submission.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(problemId);
                    if (!flag)
                    {
                        error = "Cannot verify parent value or Invalid value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get lookup parent."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_Verify_KnowledgeSubmissionText()
        {
            try
            {
                bool flagCheck = false;
                textarea = knowledge.Textarea_Text();
                temp = textarea.MyElement.Text.Replace("\r\n", "|");

                string description = Base.GData("ProDescription");
                string workA = Base.GData("ProWorkAround");

                //Verify Description
                string expected = "Description:|" + description;
                if (temp.Contains(expected))
                {
                    try
                    {
                        //Verify Workaround
                        expected = workA;
                        if (!temp.Contains(expected))
                        {
                            flagCheck = false;
                            flagExit = false;
                            error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                        }
                        else { flagCheck = true; }
                    }
                    catch { }
                    if (!flagCheck)
                    {
                        flag = false;
                        flagExit = false;
                        error = string.Format("Invalid text default value. Expected: {0} ||| Actual: {1}", expected, temp);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_VerifyStatusCombobox()
        {
            try
            {
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Submitted";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify status value or Invalid value.";
                    }
                }
                else { error = "Cannot get combobox Status."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_Click_CreateArticaleButton()
        {
            try
            {
                button = submission.Button_CreateArticle();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        knowledge.WaitLoading();
                    }
                    else { error = "Cannot click button Article."; }
                }
                else { error = "Cannot get button Article."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_GetKnowledgeId()
        {
            try
            {
                textbox = knowledge.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    //Get Knowledge ticket number
                    knowledgeId2 = textbox.Text;
                }
                else { error = "Cannot get textbox Number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_Verify_ShortDescription()
        {
            try
            {
                string temp = Base.GData("ProStatement");
                textbox = knowledge.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify short description or Invalid value."; flagExit = false; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_VerifyDefaultValue_WorkFlow()
        {
            try
            {
                combobox = knowledge.Combobox_KB_Workflow();
                if (combobox.Existed)
                {

                    string temp = "Draft";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid workflow default value;";
                    }
                }
                else { error = "Not found combobox Workflow"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }    
}
