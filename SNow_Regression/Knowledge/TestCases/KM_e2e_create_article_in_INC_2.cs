﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
using SNow;
using OpenQA.Selenium.Interactions;

namespace Knowledge
{
    public class KM_e2e_create_article_in_INC_2
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error, temp;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - KB Submission Id: " + submissionId);
            System.Console.WriteLine("Finished - Knowledge Id: " + knowledgeId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snocheckbox checkbox;
        SNow.snotextarea textarea;
        SNow.snobutton button;
        SNow.snodatetime datetime;
       
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Incident inc = null;
        SNow.Home home = null;
        SNow.Knowledge knowledge = null;
        SNow.KnowledgeSearch knls = null;
        SNow.Submission submission = null;
        SNow.KnowledgeList kmlist = null;
        SNow.IncidentList incList = null;
        SNow.ItilList taskSLAlist = null;
        SNow.Itil taskSLA = null;
        //------------------------------------------------------------------
        string incidentId, submissionId, knowledgeId;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                knowledge = new SNow.Knowledge(Base, "Knowledge");
                kmlist = new SNow.KnowledgeList(Base, "Knowledge List");
                incList = new SNow.IncidentList(Base, "Incident list");
                knls = new SNow.KnowledgeSearch(Base);
                submission = new SNow.Submission(Base,"Submission");
                taskSLAlist = new SNow.ItilList(Base, "Task SLA List");
                taskSLA = new SNow.Itil(Base, "Task SLA");
                //------------------------------------------------------------------
                incidentId = string.Empty;
                submissionId = string.Empty;
                knowledgeId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_004_ImpersonateUser_ServiceDesk()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("IncCaller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        lookup.Click();
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("IncCallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
    
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            inc.WaitLoading();
                        }
                        else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_PopulateAssignmentGroup()
        {
            try
            {
                temp = Base.GData("IncAssignmentGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Select(temp);
                if (!flag)
                {error = "Cannot input Assignment Group.";}
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_ClickOnKnowledgeCheckbox()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    checkbox = inc.Checkbox_CreateKnowledge();
                    flag = checkbox.Existed;
                    if (flag)
                    {
                        flag = checkbox.Click(true);
                        if (!flag && !checkbox.Checked)
                        {
                            error = "Cannot select Create Knowledge checkbox.";
                        }
                    } else { error = "Not found Create Knowledge checkbox."; }
                } else { error = "Cannot click Closure Information tab."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_01_VerifyNoSubmissionCreated()
        {
            try
            {
                temp = "Knowledge Submission created";
                flag = inc.VerifyMessageInfo(temp);
                if (flag)
                {
                    flag = false;
                    error = "Unexpected message";
                }
                else
                { 
                    flag = true;
                    System.Console.WriteLine("----PASSED----- No info message displayed");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_02_Verify_KnowledgeCheckbox_Checked()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    checkbox = inc.Checkbox_CreateKnowledge();
                    flag = checkbox.Existed;
                    if (flag)
                    {
                        flag = checkbox.Checked;
                        if (!flag)
                        {
                            error = "Knowledge checkbox is not checked. Expected: CHECKED.";
                            flagExit = false;
                        }
                    }
                    else { flagExit = false; error = "Cannot get Create Knowledge checkbox."; }
                }
                else { error = "Cannot click Closure Information tab."; } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_ChangeIncStateToActive()
        {
            try
            {
                temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    { 
                        flag = inc.Save();
                        if (!flag)
                        {error = "Cannot save incident.";}
                        else {inc.WaitLoading();}
                    }
                    else
                    {error = "Cannot change state.";}

                }
                  
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_VerifyNoSubmissionCreated()
        {
            try
            {
                temp = "Knowledge Submission created";
                flag = inc.VerifyMessageInfo(temp);
                if (flag)
                {
                    flag = false;
                    error = "Unexpected message";
                }
                else
                {
                    flag = true;
                    System.Console.WriteLine("----PASSED----- No info message displayed");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_ChangeIncStateToAwaitingCustomer()
        {
            try
            {
                temp = "Awaiting Customer";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        string d = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                        datetime = inc.Datetime_Followupdate();
                        flag = datetime.Existed;
                        if (flag) 
                        {
                            flag = datetime.SetText(d);
                            if (flag) 
                            {
                                flag = inc.Save();
                                if (!flag)
                                { error = "Cannot save incident."; }
                                else { inc.WaitLoading(); }
                            }
                        }           
                    }
                    else
                    { error = "Cannot change state."; }

                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_VerifyNoSubmissionCreated()
        {
            try
            {
                temp = "Knowledge Submission created";
                flag = inc.VerifyMessageInfo(temp);
                if (flag)
                {
                    flag = false;
                    error = "Unexpected message";
                }
                else
                {
                    flag = true;
                    System.Console.WriteLine("----PASSED----- No info message displayed");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_ChangeIncStateToActive()
        {
            try
            {
                temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        flag = inc.Save();
                        if (!flag)
                        { error = "Cannot save incident."; }
                        else { inc.WaitLoading(); }
                    }
                    else
                    { error = "Cannot change state."; }

                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_VerifyNoSubmissionCreated()
        {
            try
            {
                temp = "Knowledge Submission created";
                flag = inc.VerifyMessageInfo(temp);
                if (flag)
                {
                    flag = false;
                    error = "Unexpected message";
                }
                else
                {
                    flag = true;
                    System.Console.WriteLine("----PASSED----- No info message displayed");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_ChangeIncStateToResolved()
        {
            try
            {
                temp = "Resolved";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    { inc.WaitLoading(); }
                    else
                    { error = "Cannot change incident state."; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_InputCloseCodeAndCloseNote()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    temp = Base.GData("IncCloseCode");
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            temp = Base.GData("IncCloseNote");
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                flag = textarea.SetText(temp);
                                if (!flag)
                                {
                                    error = "Cannot input close notes.";
                                }
                            }
                        }
                        else
                        { error = "Cannot select close code."; }
                    }
                    else { error = "Close code does not exist"; }
                }
                else
                {
                    error = "Cannot select Closure Information tab.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag)
                {
                    error = "Cannot save incident.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_VerifySubmissionCreated()
        {
            try
            {

                temp = "Knowledge Submission created";
                flag = inc.VerifyMessageInfo(temp);
                if (!flag)
                {
                    error = "Unexpected message";
                }
                else
                {
                    System.Console.WriteLine("----PASSED----- Submission message displayed");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_OpenSubmissions()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Open Submissions", "Open Submissions");
                if (flag)
                {
                    kmlist.WaitLoading();
                }
                else
                {
                    error = "Cannot open submissions";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_SearchAOpenSubmission()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //---------------------------------------------------------------------------
                flag = incList.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + incidentId;
                    flag = incList.SearchAndOpen("Parent", incidentId, temp, "Number");
                    if (flag)
                    {
                        submission.WaitLoading();
                    }
                    else { error = "Cannot find Knowledge Submission"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_ValidateSubmission()
        {
            try
            {
                //-- Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                submissionId = submission.Textbox_Number().Text;
                System.Console.WriteLine("KB Submission Id: " + submissionId);
                if (submissionId != string.Empty)
                {
                    temp = Base.GData("IncAssignmentGroup");
                    if (submission.Lookup_AssignmentGroup().Text == temp)
                    {
                        temp = Base.GData("IncCloseNote");
                        if (submission.Textarea_Text().MyText.Contains(temp))
                        {
                            combobox = submission.Combobox_Status();
                            if (combobox.Text == "Submitted")
                            {
                                if (submission.Lookup_Parent().Text != incidentId)
                                {
                                    flag = false;
                                    error = "Invalid KB Submission parent value.";
                                }
                            }
                            else
                            {
                                flag = false;
                                error = "Invalid KB Submission status value.";
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "Invalid KB Submission close note value.";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Invalid KB Submission assignment group value.";
                    }
                }
                else
                {
                    flag = false;
                    error = "Invalid KB Submission number value.";
                }
            }

            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_ClickAttachmentLink()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Attachment_Link();
                if (checkbox.Existed && checkbox.Checked == false)
                {
                    flag = checkbox.Click(true);
                    if (!flag)
                    { error = "Cannot check Attachment Link Checkbox"; }
                }
                else
                {
                    flag = false;
                    error = "Attachment Link Checkbox is null or checked";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_SaveSubmission()
        {
            try
            {
                flag = submission.Save();
                if (!flag)
                {
                    error = "Cannot save submission";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        ///-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_AttachFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = submission.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_Attach2ndFile()
        {
            try
            {
                string attachmentFile = "wordAttachment.docx";
                flag = submission.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_Open_TaskSLAList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("slas", "My Groups Work");
                if (flag)
                { kmlist.WaitLoading(); }
                else
                {
                    error = "Cannot open submissions";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_SearchAndOpen_KnowledgeSubmission_1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (submissionId == null || submissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 01.");
                    addPara.ShowDialog();
                    submissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //----------------------------------------------------------------------
                flag = kmlist.SearchAndOpen("Task", submissionId, "Task=" + submissionId, "Stage");
                if (!flag) error = "Error when search and open knowledge submission id:" + submissionId;
                else submission.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_Verify_SLADefinition()
        {
            try
            {
                lookup = taskSLA.Lookup_SLADefinition();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("SLA_Definition");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify SLA Definition value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get SLA Definition lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_Verify_Stage()
        {
            try
            {
                combobox = taskSLA.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Stage value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Stage combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_01_Verify_Schedule()
        {
            try
            {
                lookup = taskSLA.Lookup_Schedule();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Schedule");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Schedule value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Schedule lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_02_Verify_Timezone()
        {
            try
            {
                combobox = taskSLA.Combobox_Timezone();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Timezone");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Timezone value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Timezone combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_01_OpenNewPrivateBrowser()
        {
            try
            {
                Base.SwitchToPage(0);
                string temp = Base.GData("Type").ToLower();
                if (temp == "ff")
                {
                    textbox.MyElement.SendKeys(Keys.LeftControl + Keys.Shift + "P");
                    flag = Base.SwitchToPage(1);
                }
                else if (temp == "chr")
                {
                    System.Windows.Forms.SendKeys.SendWait("^(+N)");
                    flag = Base.SwitchToPage(1);
                }
                if (!flag) { error = "Cannot open private browser."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_02_Login()
        {
            try
            {
                //Open the URL
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
                
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_03_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KMUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_04_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_05_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page 1."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_06_SearchAndOpen_KnowledgeSubmission_1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (submissionId == null || submissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 01.");
                    addPara.ShowDialog();
                    submissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(submissionId, true);
                if (!flag)
                    error = "Cannot open submission.";
                else
                    submission.WaitLoading();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_Populate_Status()
        {
            try
            {
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Closed, Invalid";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot select item [" + temp + "]"; }
                }
                else { error = "Cannot get Status combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_01_Save_Submission()
        {
            try
            {

                flag = submission.Save();
                if (!flag) { error = "Cannot save KB submission."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_02_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page 0."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_03_ReloadForm()
        {
            try
            {
                flag = taskSLA.ReloadForm();
                if (!flag) { error = "Cannot reload form."; }
                else { taskSLA.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_04_Verify_SLADefinition()
        {
            try
            {
                lookup = taskSLA.Lookup_SLADefinition();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("SLA_Definition");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify SLA Definition value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get SLA Definition lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_05_Verify_Stage()
        {
            try
            {
                combobox = taskSLA.Combobox_Stage();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Completed";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Stage value. Expected: " + temp;
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Stage combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page 1."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_02_Populate_Status()
        {
            try
            {
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Assigned";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot select item [" + temp + "]"; }
                }
                else { error = "Cannot get Status combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_03_Save_Submission()
        {
            try
            {
                flag = submission.Save();
                if (!flag) { error = "Cannot save KB submission."; }
                else { submission.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_04_Close_Page()
        {
            try
            {
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_05_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page 0."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_06_Open_TaskSLAList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("slas", "My Groups Work");
                if (flag)
                { 
                    kmlist.WaitLoading(); 
                }
                else
                {
                    error = "Cannot open submissions";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------


        [Test]
        public void Step_040_00_SearchAndVerify_SubmissionGenerated()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (submissionId == null || submissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 1.");
                    addPara.ShowDialog();
                    submissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                string condition = "Task=" + submissionId + "|SLA=SUB-CLOSE-Global|Stage=In progress";
                flag = kmlist.SearchAndVerify("Task", submissionId, condition);
                if (!flag)
                {
                    error = "Error when search and verify KB submission with condition: " + condition;
                    flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_01_OpenSubmissions()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Open Submissions", "Open Submissions");
                if (flag)
                { kmlist.WaitLoading(); }
                else
                {
                    error = "Cannot open submissions";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_02_SearchAOpenSubmission()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //---------------------------------------------------------------------------
                flag = incList.Add_More_Columns("Parent");
                if (flag)
                {
                    temp = "Parent=" + incidentId;
                    flag = incList.SearchAndOpen("Parent", incidentId, temp, "Number");
                    if (flag)
                    {
                        submission.WaitLoading();
                    }
                    else { error = "Cannot find Knowledge Submission"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_ChangeSubmissionStateToClosed()
        {
            try
            {
                temp = "Closed, Article(s) Created";
                combobox = submission.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    { error = "Cannot select Status = " + temp; }
                }
                else
                {
                    error = "Combobox Status does not exist";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_InputCloseNotes()
        {
            try
            {
                textarea = submission.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    temp = "Auto Close Notes";
                    flag = textarea.SetText(temp);
                    if (!flag)
                    { error = "Cannot input Close Notes"; }
                }
                else { error = "Not found textarea Close notes"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_SaveSubmission()
        {
            try
            {
                flag = submission.Save();
                if (!flag)
                { error = "Cannot save submission"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_ImpersonateUser_KnowledgeManager()
        {
            try
            {
                string temp = Base.GData("KMUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
       
        [Test]
        public void Step_045_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_SearchAOpenSubmission()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (submissionId == null || submissionId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input knowledge submissions Id 01.");
                    addPara.ShowDialog();
                    submissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(submissionId, true);
                if (!flag)
                    error = "Cannot open submission.";
                else
                    submission.WaitLoading();

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_ClickCreateArticleButton()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && submissionId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KB Submission Id.");
                    addPara.ShowDialog();
                    submissionId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------
                button = submission.Button_CreateArticle();
                if (button.Existed)
                {
                    button.Click();
                    knowledge.WaitLoading();
                    knowledgeId = knowledge.Textbox_Number().Text;
                    if (knowledgeId != string.Empty)
                    {
                        lookup = knowledge.Lookup_Kb_Category();
                        if (lookup.Text == string.Empty)
                        {
                            combobox = knowledge.Combobox_KB_Article_type();
                            if (combobox.Text == "HTML")
                            {
                                temp = knowledge.Lookup_SourceTask().Text;
                                if (temp == submissionId)
                                {
                                    temp = Base.GData("Debug").ToLower();

                                    if (temp == "yes" && incidentId == string.Empty)
                                    {
                                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                                        addPara.ShowDialog();
                                        incidentId = addPara.value;
                                        addPara.Close();
                                        addPara = null;
                                    }

                                    temp = Base.GData("FullPathDomain");
                                    if (knowledge.Lookup_Domain().Text == temp)
                                    {
                                        temp = Base.GData("IncShortDescription");
                                        textbox = knowledge.Textbox_ShortDescription();
                                        if (textbox.Text == temp)
                                        {
                                            textbox.SetText(temp + " - " + incidentId);
                                            temp = Base.GData("IncCloseNote");
                                            if (knowledge.Textarea_Text().MyText.Contains(temp) == false)
                                            {
                                                flag = false;
                                                error = "Invalid text close note value.";
                                            }
                                        }
                                        else
                                        {
                                            flag = false;
                                            error = "Invalid short description value.";
                                        }
                                    }
                                    else
                                    {
                                        flag = false;
                                        error = "Invalid domain value.";
                                    }
                                }
                                else
                                {
                                    flag = false;
                                    error = "Invalid source value.";
                                }
                            }
                            else
                            {
                                flag = false;
                                error = "Invalid article type value.";
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "Invalid category value.";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Invalid knowledge Id value.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_01_VerifyAutomationcheckbox()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Automation();
                flag = checkbox.Existed;
                if (flag)
                {
                    flag = checkbox.Click(true);
                    if (!flag && !checkbox.Checked)
                    {
                        error = "Cannot check the Automation checkbox.";
                    }
                    else
                    {
                        //knowledge.WaitLoading(); Thread.Sleep(12000); 
                        flag = checkbox.Click(false);//Skip to populating the URL.
                    }
                }
                else
                {
                    error = "Not existing Automation checkbox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_048_02_Click_EditURL_Button()
        //{
        //    try
        //    {
        //        button = knowledge.Button_EditthisURL();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            flag = button.Click();
        //            if (!flag)
        //            {
        //                error = "Cannot click on Edit this URL button.";
        //            }
        //            else { knowledge.WaitLoading(); Thread.Sleep(1000); }
        //        }
        //        else
        //        {
        //            error = "Cannot get Edit this URL button.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_to_051_ChangeWorkflowToPublishedAndSave()
        {
            try
            {
                lookup = knowledge.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("IncAssignmentGroup"));
                    if (!flag)
                    {
                        error = "Cannot select Assignment Group value.";
                    }
                    else
                    {
                        button = knowledge.Button_KA_Publish();
                        flag = button.Existed;
                        if (flag)
                        {
                            button.Click();
                            knowledge.WaitLoading();
                        }
                        else
                        { error = "Cannot select workflow value."; }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_OpenKnowledgePublished()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && knowledgeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Knowledge Id.");
                    addPara.ShowDialog();
                    knowledgeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Publish", "Published");
                if (flag)
                {
                    kmlist.WaitLoading();
                    temp = "Number=" + knowledgeId;
                    flag = kmlist.SearchAndOpen("Number", knowledgeId, temp, "Number");
                    if (!flag)
                    { error = "Not found knowledge in published list."; }
                    else
                    {
                        knowledge.WaitLoading();  
                    }
                }
                else
                { error = "Cannot open published knowledge list."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_VerifyAutomationcheckbox()
        {
            try
            {
                checkbox = knowledge.Checkbox_KB_Automation();
                flag = checkbox.Existed;
                if (!flag)
                {
                    error = "Not existing Automation checkbox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
                try
                {
                    IAlert alert = Base.Driver.SwitchTo().Alert();
                    alert.Accept();
                }
                catch
                { }
                if (flag == false)
                {
                    error = "Cannot logout system.";
                }
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion
    }

}
