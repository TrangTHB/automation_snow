﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace General
{
    [TestFixture]
    public class General_Change_Workflow
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Change task Id 1: " + changeTaskId_1);
            System.Console.WriteLine("Finished - Change task Id 2: " + changeTaskId_2);
            System.Console.WriteLine("Finished - Change task Id 3: " + changeTaskId_3);
            System.Console.WriteLine("Finished - Rejected change: " + (rejected_before + rejected_runtime).ToString());
            System.Console.WriteLine("Finished - Test Case Name: " + Base.GData("Comment"));
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        
        Login login;
        Home home;
        Change chg;
        ItilList chglist, approvalList;
        RiskAssessment riskAss;
        EmailList emailList = null;
        Email email = null;
        //-----------------------------------------------------------------------------------------------------------------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snodatetime datetime = null;
        snobutton button = null;

        bool isRecall = false;
        
        string changeId, changeTaskId_1, changeTaskId_2, changeTaskId_3;

        string rootuser, changeType;

        string haveRFCApproval, haveImpactedCIApproval, haveAdhocApprovalTech, haveChangeManagerReview, haveAffectedCIApproval, haveAdhocApprovalBus;
        string rfcManagerGroup, rfcAccountGroup, rfcCscGroup, impactedCIGroup, managerReviewGroup, affectedCIGroup;
        string rfcManagerUser, rfcAccountUser, rfcCscUser, impactedCIUser, adhocApprover_tech, adhocApprover_bus, technicalApprover, managerReview, affectedCIUser, businessApprover, changeManager;
        string rejectUserList_rfc, rejectUserList_tech, rejectUserList_business, rejectUserList_cab;
        
        int rejected_before, rejected_runtime;
        bool change_rejected = false;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                chg = new Change(Base, "Change");
                chglist = new ItilList(Base, "Change list");
                riskAss = new RiskAssessment(Base);
                approvalList = new ItilList(Base, "Approval list");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                //---------------------------------------------------------------------------------------------------------
                changeId = string.Empty; 
                changeTaskId_1 = string.Empty;
                changeTaskId_2 = string.Empty;
                changeTaskId_3 = string.Empty;
                //---------------------------------------------------------------------------------------------------------
                rootuser = Base.GData("UserFullName");
                changeType = Base.GData("ChangeType").ToLower();
                //---------------------------------------------------------------------------------------------------------
                rejectUserList_rfc = Base.GData("Reject_User_List_RFC").Trim();
                rejectUserList_tech = Base.GData("Reject_User_List_Tech").Trim();
                rejectUserList_business = Base.GData("Reject_User_List_Business").Trim();
                rejectUserList_cab = Base.GData("Reject_User_List_CAB").Trim();
                //---------------------------------------------------------------------------------------------------------
                haveRFCApproval = Base.GData("HaveRFCApproval").Trim().ToLower();
                haveImpactedCIApproval = Base.GData("HaveImpactedCIApproval").Trim().ToLower();
                haveAdhocApprovalTech = Base.GData("HaveAdhocApprovalTech").Trim().ToLower();
                haveChangeManagerReview = Base.GData("HaveChangeManagerReview").Trim().ToLower();
                haveAffectedCIApproval = Base.GData("HaveAffectedCIApproval").Trim().ToLower();
                haveAdhocApprovalBus = Base.GData("HaveAdhocApprovalBus").Trim().ToLower();
                //---------------------------------------------------------------------------------------------------------
                rfcManagerGroup = Base.GData("RFCManagerGroup").Trim();
                rfcManagerUser = Base.GData("RFCManagerUser").Trim();

                rfcAccountGroup = Base.GData("RFCAccountGroup");
                rfcAccountUser = Base.GData("RFCAccountUser");
                
                rfcCscGroup = Base.GData("RFCCscGroup");
                rfcCscUser = Base.GData("RFCCscUser");

                impactedCIGroup = Base.GData("ImpactedCIGroup");
                impactedCIUser = Base.GData("ImpactedCIUser");
                
                adhocApprover_tech = Base.GData("AdhocApprover_Tech");
                adhocApprover_bus = Base.GData("AdhocApprover_Bus");
                technicalApprover = Base.GData("TechnicalApprover");

                managerReviewGroup = Base.GData("ManagerReviewGroup");
                managerReview = Base.GData("ChangeManagerReview");
                
                affectedCIGroup = Base.GData("AffectedCIGroup");
                affectedCIUser = Base.GData("AffectedCIUser");
                
                businessApprover = Base.GData("BusinessApprover");
                changeManager = Base.GData("ChangeManager");

                rejected_before = 0;
                rejected_runtime = 0;
                if (rejectUserList_rfc.ToLower() != "no" || rejectUserList_tech.ToLower() != "no" || rejectUserList_business.ToLower() != "no" || rejectUserList_cab.ToLower() != "no")
                    change_rejected = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Create new change

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else 
                {
                    login.WaitLoading();
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                
                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Open_New_Change()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Populate_Company()
        {
            try
            {
                string company = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(company);
                }
                else error = "Cannot get lookup company.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Populate_Short_Description()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Populate_Justification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Error when select Planning tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Populate_Planned_Date()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    button = chg.Button_PlannedStartDate_Calendar();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            button = chg.Button_Calendar_Select();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                if (flag)
                                {
                                    datetime = chg.Datetime_Planned_Start_Date();
                                    flag = datetime.Existed;
                                    if (flag)
                                    {
                                        string start = datetime.Text.Trim();
                                        start = Convert.ToDateTime(start).AddDays(5).ToString("yyyy-MM-dd HH:mm:ss");
                                        Console.WriteLine("Planned start date:" + start);
                                        datetime.SetText(start, true);
                                        string end = Convert.ToDateTime(start).AddDays(3).ToString("yyyy-MM-dd HH:mm:ss");
                                        datetime = chg.Datetime_Planned_End_Date();
                                        flag = datetime.Existed;
                                        if (flag)
                                        {
                                            flag = datetime.SetText(end, true);
                                            if (!flag)
                                                error = "Error when input planned end date.";
                                        }
                                        else error = "Cannot get datetime planned end date.";
                                    }
                                    else error = "Cannot get datetime planned start date.";
                                }
                            }
                            else error = "Cannot get button select.";
                        }
                        else error = "Error when click on button planned start date calendar.";
                    }
                    else error = "Cannot get button planned start date calendar.";
                    //    string startDate = "";
                    //    string endDate = "";
                    //    string temp = Base.GData("Planned_Start_Date");
                    //    string[] totalday = temp.Split(':');
                    //    startDate = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");
                    //    endDate = DateTime.Today.AddDays(3).ToString("yyyy-MM-dd HH:mm:ss");
                    //    switch (totalday[0].Trim().ToLower())
                    //    {
                    //        case "future":
                    //            startDate = Convert.ToDateTime(startDate).AddDays(Int32.Parse(totalday[1].Trim())).ToString("yyyy-MM-dd HH:mm:ss");
                    //            endDate = Convert.ToDateTime(endDate).AddDays(Int32.Parse(totalday[1].Trim())).ToString("yyyy-MM-dd HH:mm:ss");
                    //            break;
                    //        case "past":
                    //            startDate = Convert.ToDateTime(startDate).AddDays(-Int32.Parse(totalday[1].Trim())).ToString("yyyy-MM-dd HH:mm:ss");
                    //            break;
                    //    }
                    //    datetime = chg.Datetime_Planned_Start_Date();
                    //    flag = datetime.Existed;
                    //    if (flag)
                    //    {
                    //        flag = datetime.SetText(startDate, true);
                    //        if (flag)
                    //        {
                    //            datetime = chg.Datetime_Planned_End_Date();
                    //            flag = datetime.Existed;
                    //            if (flag)
                    //            {
                    //                flag = datetime.SetText(endDate, true);
                    //                if (!flag) error = "Cannot populate planned end date.";
                    //            }
                    //            else error = "Cannot get datetime planned end date.";
                    //        }
                    //        else error = "Cannot populate planned start date.";
                    //    }
                    //    else error = "Cannot get datetime planned start date.";
                }
                else error = "Error when select Schedule tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_0_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_1_Populate_Priority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("4 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate priority value."; }
                }
                else
                {
                    error = "Cannot get combobox priority.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_2_Save_Change()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeId = textbox.Text;
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change.";
                    else chg.WaitLoading();
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_1_Verify_State()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    if (changeType != "emergency change")
                    {
                        flag = combobox.VerifyCurrentValue("Draft", true);
                    }
                    else 
                    {
                        flag = combobox.VerifyCurrentValue("Review", true);
                    }

                    if (!flag)
                    {
                        error = "Invalid state";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox state."; }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_2_Verify_State_ReadOnly()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    if (changeType != "emergency change")
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                        {
                            error = "Combobox state is NOT readonly.";
                            flagExit = false;
                        }
                    }    
                }
                else { error = "Cannot get combobox state."; }    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Add_CI()
        {
            try
            {
                string temp = Base.GData("CI");
                if (temp.ToLower() != "no")
                {
                    lookup = chg.Lookup_ConfigurationItem();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot add CI."; }
                    }
                    else
                        error = "Cannot get lookup CI.";
                }
                else
                    Console.WriteLine("*** This case has NO CI ***");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Populate_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Populate_AssignedTo_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Save_Change()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Error when save change."; }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Add_BusinessService()
        {
            try
            {
                string temp = Base.GData("BS");
                if (temp.ToLower() != "no")
                {
                    flag = chg.Add_Related_Members("Impacted Services", temp);
                    if (!flag)
                        error = "Error when add business service.";
                    else
                        chg.WaitLoading();
                }
                else
                    Console.WriteLine("*** This case has NO BS ***");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Save_Change()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Error when save change."; }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - Create new change

        #region Request for approval

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Search_And_Open_Change()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else { Thread.Sleep(5000); chg.WaitLoading(); }
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Change Requests)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_AddWorkNotes()
        {
            try
            {
                Thread.Sleep(2000);
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    string temp = Base.GData("WorkNotes");
                    flag = chg.Add_Worknotes(temp);
                    if (!flag)
                        error = "Error when add work notes.";
                }
                else error = "Error when select tab Notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_PopulatePlanningSection()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    textarea = chg.Textarea_ChangePlan();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("ChangePlan");
                        flag = textarea.SetText(temp);
                        if (flag)
                        {
                            textarea = chg.Textarea_ImplementationPlan();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("ImplementationPlan");
                                flag = textarea.SetText(temp);
                                if (flag)
                                {
                                    textarea = chg.Textarea_BackoutPlan();
                                    flag = textarea.Existed;
                                    if (flag)
                                    {
                                        temp = Base.GData("BackoutPlan");
                                        flag = textarea.SetText(temp);
                                        if (flag)
                                        {
                                            textarea = chg.Textarea_TestPlan();
                                            flag = textarea.Existed;
                                            if (flag)
                                            {
                                                temp = Base.GData("TestPlan");
                                                flag = textarea.SetText(temp);
                                                if (!flag) error = "Cannot populate test plan.";
                                            }
                                            else error = "Cannot get textarea test plan.";
                                        }
                                        else error = "Cannot populate backout plan.";
                                    }
                                    else error = "Cannot get textarea backout plan.";
                                }
                                else error = "Cannot populate implementation plan.";
                            }
                            else error = "Cannot get textarea implementation plan.";
                        }
                        else error = "Cannot populate change plan.";
                    }
                    else error = "Cannot get textarea change plan.";
                }
                else error = "Error when select tab Planning.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Save()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Error when save change."; }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_RequestForApproval()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    //-- Request
                    if (flag)
                    {
                        button = chg.Button_RequestApproval_RequestReview();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag)
                                error = "Error when click button request approval";
                            else
                                chg.WaitLoading();
                        }
                        else error = "Cannot get button request approval";
                    }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
                //datetime = chg.Datetime_Risk_Assessment_Last_Run();
                
                //if (datetime.Text.Trim() == "") 
                //{
                //    flag = chg.Select_Tab("Risk Assessment");
                //    if (flag)
                //    {
                //        button = chg.Button_FillOutRiskAssessment();
                //        flag = button.Existed;
                //        if (flag)
                //        {
                //            flag = button.Click();
                //            if (flag)
                //            {
                //                riskAss.WaitLoading();

                //                Thread.Sleep(5000);

                //                // Verify Dialog title
                //                snoelement ele = riskAss.Title();
                //                flag = ele.Existed;
                //                if (flag)
                //                {
                //                    flag = ele.MyText.Equals("Risk Assessment");
                //                    if (!flag)
                //                    {
                //                        error = "Invalid title.";
                //                    }
                //                }
                //                else
                //                {
                //                    error = "Cannot get label.";
                //                }
                //            }
                //            else error = "Error when click on button Fill Out Risk Assessment.";
                //        }
                //        else error = "Cannot get button fill out risk assessment.";
                //    }
                //    else error = "Error when select Risk Assessment tab.";

                //    //-- fill data
                //    if (flag)
                //    {
                //        flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                //        if (!flag)
                //        {
                //            error = "Cannot answer Risk Assessment questions";
                //        }
                //    }

                //    //-- Submit

                //    if (flag)
                //    {
                //        snoelement ele = riskAss.Form_Submit();
                //        ele.MyElement.Submit();
                //        Thread.Sleep(2000);
                //        button = riskAss.Button_Close();
                //        flag = button.Existed;
                //        if (flag)
                //            button.Click();
                //        Thread.Sleep(2000);                
                //    }

                //    //-- Reload
                //    if (flag)
                //    {
                //        flag = chg.Save();
                //        if (flag)
                //            chg.WaitLoading();
                //    }
                //    //-- Verify
                //    if (flag)
                //    {
                //        datetime = chg.Datetime_Risk_Assessment_Last_Run();
                //        flag = datetime.Existed;
                //        if (flag)
                //        {
                //            flag = datetime.Text.Trim() != "";
                //            if (!flag)
                //                error = "Cannot fill and execute risk assessment.";
                //        }
                //        else { error = "Cannot get datetime last run."; }
                //    }
                //}

                ////-- Request
                //if (flag)
                //{
                //    button = chg.Button_RequestApproval_RequestReview();
                //    flag = button.Existed;
                //    if (flag)
                //    {
                //        flag = button.Click();
                //        if (!flag)
                //            error = "Error when click button request approval";
                //        else
                //            chg.WaitLoading();
                //    }
                //    else error = "Cannot get button request approval";
                //}
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_1_Verify_State_Draft()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    if (changeType != "emergency change")
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                        {
                            error = "Combobox state is NOT readonly.";
                            flagExit = false;
                        }
                    }
                }
                else { error = "Cannot get combobox state."; }    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_2_Verify_State_ReadOnly()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    if (changeType != "emergency change")
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                        {
                            error = "Combobox state is NOT readonly.";
                            flagExit = false;
                        }
                    }
                }
                else { error = "Cannot get combobox state."; }   
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion End - Request for approval

        #region Case have RFC approval

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_VerifyApprovers_RFCManagerUser_Requested()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcManagerUser.ToLower() != "no" && rfcManagerGroup.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + rfcManagerUser + "|Assignment group=" + rfcManagerGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + rfcManagerUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not RFCManagerUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Impersonate_RFCManagerUser()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcManagerUser.ToLower() != "no")
                    {
                        flag = home.ImpersonateUser(rfcManagerUser, true, rootuser);
                        if (!flag) error = "Error when impersonate user rfc manager user.";
                        else home.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not RFCManagerUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SystemSetting()
        {
            if (haveRFCApproval == "yes")
            {
                if (rfcManagerUser.ToLower() != "no")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                }
                else Console.WriteLine("*** This case has not RFCManagerUser approval ***");
            }
            else
            {
                Console.WriteLine("*** This case has not RFC approval state ***");
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_SearchAndOpenChange()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcManagerUser.ToLower() != "no")
                    {
                        Step_021_Search_And_Open_Change();
                        if (!flag)
                            error = "Error when recall step: Step_021_Search_And_Open_Change";
                    }
                    else
                        Console.WriteLine("*** This case has not RFCManagerUser approval ***");
                }
                else
                    Console.WriteLine("*** This case has not RFC approval ***");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_SelectRFCManagerUserApproverSignatureLine()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcManagerUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + rfcManagerUser + "|Assignment group=" + rfcManagerGroup;
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + rfcManagerUser, conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else { Thread.Sleep(5000); chg.WaitLoading(); }
                    }
                    else Console.WriteLine("*** This case has not RFCManagerUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region RFCManagerUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_RFCManagerUser_Click_Reject()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rejectUserList_rfc.Contains(rfcManagerUser))
                    {
                        Thread.Sleep(2000);
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(rfcManagerUser + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_RERUN_AFTER_RFCManagerUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveRFCApproval == "yes" && rejectUserList_rfc.Contains(rfcManagerUser))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_019_ImpersonateUser_ChangeCoor ***");
                        Step_019_ImpersonateUser_ChangeCoor();
                    }
                    else
                        error = "Error when recall step: Step_019_ImpersonateUser_ChangeCoor";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_021_Search_And_Open_Change ***");
                        Step_021_Search_And_Open_Change();
                    }
                    else
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_024_RequestForApproval ***");
                        Step_024_RequestForApproval();
                    }
                    else
                        error = "Error when recall step: Step_024_RequestForApproval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_027_Impersonate_RFCManagerUser ***");
                        Step_027_Impersonate_RFCManagerUser();
                    }
                    else
                        error = "Error when recall step: Step_027_Impersonate_RFCManagerUser";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_029_SearchAndOpenChange ***");
                        Step_029_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_029_SearchAndOpenChange()";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_030_SelectRFCManagerUserApproverSignatureLine ***");
                        Step_030_SelectRFCManagerUserApproverSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_030_SelectRFCManagerUserApproverSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion RFCManagerUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_RFCManagerUser_Click_Approve()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcManagerUser.ToLower() != "no")
                    {
                        button = chg.Button_Approve();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag) error = "Error when click on button approve.";
                            else
                            {
                                Thread.Sleep(5000);
                                chg.WaitLoading();
                            }
                        }
                        else error = "Cannot get button approve.";
                    }
                    else Console.WriteLine("*** This case has not RFCManagerUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_1_Verify_State_Draft()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Draft");
                        if (!flag)
                        {
                            error = "Invalid state value.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_2_Verify_State_ReadOnly()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                        {
                            error = "Combobox State is NOT readonly.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_VerifyApprovers_RFCManagerUser_Approved()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcManagerUser.ToLower() != "no")
                    {
                        string conditions = "State=Approved|Approver=" + rfcManagerUser + "|Assignment group=" + rfcManagerGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + rfcManagerUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not RFCManagerUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_VerifyApprovers_RFCAccountUser_Requested()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + rfcAccountUser + "|Assignment group=" + rfcAccountGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + rfcAccountUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_VerifyApprovers_RFCCscUser_Requested()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + rfcCscUser + "|Assignment group=" + rfcCscGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + rfcCscUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_ImpersonateUser_RFCAccountUser()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        flag = home.ImpersonateUser(rfcAccountUser, true, rootuser);
                        if (!flag) error = "Error when impersonate user rfc approver.";
                        else home.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_SystemSetting()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        Step_004_SystemSetting();
                        if (!flag)
                            error = "Error when recall step: Step_004_SystemSetting";
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_SearchAndOpenChange()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        Step_021_Search_And_Open_Change();
                        if (!flag)
                            error = "Error when recall step: Step_021_Search_And_Open_Change";
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_SelectRFCAccountUserSignatureLine()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + rfcAccountUser + "|Assignment group=" + rfcAccountGroup;
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + rfcAccountUser, conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else chg.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region RFCAccountUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_RFCAccountUser_Click_Reject()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rejectUserList_rfc.Contains(rfcAccountUser))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(rfcAccountUser + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_RERUN_AFTER_RFCAccountUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveRFCApproval == "yes" && rejectUserList_rfc.Contains(rfcAccountUser))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_032_RERUN_AFTER_RFCManagerUser_Reject_Approval ***");
                        isRecall = true;
                        Step_032_RERUN_AFTER_RFCManagerUser_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_032_RERUN_AFTER_RFCManagerUser_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_033_RFCManagerUser_Click_Approve ***");
                        Step_033_RFCManagerUser_Click_Approve();
                    }

                    else
                        error = "Error when recall step: Step_033_RFCManagerUser_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_038_ImpersonateUser_RFCAccountUser ***");
                        Step_038_ImpersonateUser_RFCAccountUser();
                    }
                    else
                        error = "Error when recall step: Step_038_ImpersonateUser_RFCAccountUser";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_021_Search_And_Open_Change ***");
                        Step_021_Search_And_Open_Change();
                    }
                    else
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_041_SelectRFCAccountUserSignatureLine ***");
                        Step_041_SelectRFCAccountUserSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_041_SelectRFCAccountUserSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion RFCAccountUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_RFCAccountUser_Click_Approve()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        button = chg.Button_Approve();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag) error = "Error when click on button approve.";
                            else
                            {
                                Thread.Sleep(5000);
                                chg.WaitLoading();
                            }
                        } error = "Cannot get button approve.";
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_1_Verify_State_Draft()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Draft");
                        if (!flag)
                        {
                            error = "Invalid state value.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_2_Verify_State_ReadOnly()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                        {
                            error = "Combobox State is NOT readonly.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_VerifyApprovers_RFCManagerUser_Approved()
        {
            try
            {
                Step_035_VerifyApprovers_RFCManagerUser_Approved();
                if (!flag)
                    error = "Error when recall step: Step_035_VerifyApprovers_RFCManagerUser_Approved";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_VerifyApprovers_RFCAccountUser_Approved()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcAccountUser.ToLower() != "no")
                    {
                        string conditions = "State=Approved|Approver=" + rfcAccountUser + "|Assignment group=" + rfcAccountGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + rfcAccountUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not RFCAccountUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_VerifyApprovers_RFCCscUser_Requested()
        {
            try
            {
                Step_037_VerifyApprovers_RFCCscUser_Requested();
                if (!flag)
                    error = "Error when recall step: Step_037_VerifyApprovers_RFCCscUser_Requested";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_ImpersonateUser_RFCCscUser()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        flag = home.ImpersonateUser(rfcCscUser, true, rootuser);
                        if (!flag) error = "Error when impersonate user rfc csc approver.";
                        else home.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_SystemSetting()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        Step_004_SystemSetting();
                        if (!flag)
                            error = "Error when recall step: Step_004_SystemSetting";
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_SearchAndOpenChange()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        Step_021_Search_And_Open_Change();
                        if (!flag)
                            error = "Error when recall step: Step_021_Search_And_Open_Change";
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_SelectRFCCscUserSignatureLine()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + rfcCscUser + "|Assignment group=" + rfcCscGroup;
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + rfcCscUser, conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else chg.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region RFCCscUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_RFCCscUser_Click_Reject()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rejectUserList_rfc.Contains(rfcCscUser))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(rfcCscUser + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000); chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_RERUN_AFTER_RFCCscUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveRFCApproval == "yes" && rejectUserList_rfc.Contains(rfcCscUser))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_043_RERUN_AFTER_RFCAccountUser_Reject_Approval ***");
                        isRecall = true;
                        Step_043_RERUN_AFTER_RFCAccountUser_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_043_RERUN_AFTER_RFCAccountUser_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_044_RFCAccountUser_Click_Approve ***");
                        Step_044_RFCAccountUser_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_044_RFCAccountUser_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_049_ImpersonateUser_RFCCscUser ***");
                        Step_049_ImpersonateUser_RFCCscUser();
                    }
                    else
                        error = "Error when recall step: Step_049_ImpersonateUser_RFCCscUser";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_051_SearchAndOpenChange ***");
                        Step_051_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_051_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_052_SelectRFCCscUserSignatureLine ***");
                        Step_052_SelectRFCCscUserSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_052_SelectRFCCscUserSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion RFCCscUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_RFCCscUser_Click_Approve()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        button = chg.Button_Approve();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag) error = "Error when click on button approve.";
                            else
                            {
                                Thread.Sleep(5000);
                                chg.WaitLoading();
                            }
                        } error = "Cannot get button approve.";
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_1_Verify_State_Draft()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Draft");
                        if (!flag)
                        { 
                            error = "Invalid state";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_2_Verify_State_ReadOnly()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                        {
                            error = "Combobx State is NOT readonly.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get combobox state."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_1_VerifyApprovers_RFCManagerUser_Approved()
        {
            try
            {
                Step_046_VerifyApprovers_RFCManagerUser_Approved();
                if (!flag)
                    error = "Error when recall step: Step_046_VerifyApprovers_RFCManagerUser_Approved";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_2_VerifyApprovers_RFCAccountUser_Approved()
        {
            try
            {
                Step_047_VerifyApprovers_RFCAccountUser_Approved();
                if (!flag)
                    error = "Error when recall step: Step_047_VerifyApprovers_RFCAccountUser_Approved()";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_3_VerifyApprovers_RFCCscUser_Approved()
        {
            try
            {
                if (haveRFCApproval == "yes")
                {
                    if (rfcCscUser.ToLower() != "no")
                    {
                        string conditions = "State=Approved|Approver=" + rfcCscUser + "|Assignment group=" + rfcCscGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + rfcCscUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not RFCCscUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_4_VerifyApprovers_ImpactedCIUser_Requested()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    if (impactedCIUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + impactedCIUser + "|Assignment group=" + impactedCIGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + impactedCIUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not ImpactedCIUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval or have not impacted ci approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_ImpersonateUser_ImpactedCIUser()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    if (impactedCIUser.ToLower() != "no")
                    {
                        string rootuser = Base.GData("UserFullName");
                        flag = home.ImpersonateUser(impactedCIUser, true, rootuser);
                        if (!flag) error = "Error when impersonate user impacted ci approver.";
                        else home.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not ImpactedCIUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval or have not impacted ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_SystemSetting()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    if (impactedCIUser.ToLower() != "no")
                    {
                        Step_004_SystemSetting();
                        if (!flag)
                            error = "Error when recall step: Step_004_SystemSetting";
                    }
                    else Console.WriteLine("*** This case has not ImpactedCIUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval or have not impacted ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_SearchAndOpenChange()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    if (impactedCIUser.ToLower() != "no")
                    {
                        Step_021_Search_And_Open_Change();
                        if (!flag)
                            error = "Error when recall step: Step_021_Search_And_Open_Change";
                    }
                    else Console.WriteLine("*** This case has not ImpactedCIUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval or have not impacted ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_SelectImpactedCIUserSignatureLine()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    if (impactedCIUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + impactedCIUser + "|Assignment group=" + impactedCIGroup;
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + impactedCIUser, conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else chg.WaitLoading();
                    }
                    else Console.WriteLine("*** This case has not ImpactedCIUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval or have not impacted ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region ImpactedCIUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_ImpactedCIUser_Click_Reject()
        {
            try
            {
                if (haveRFCApproval == "yes" && haveImpactedCIApproval == "yes")
                {
                    if (rejectUserList_rfc.Contains(impactedCIUser))
                    {
                        Thread.Sleep(2000);
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(impactedCIUser + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000); chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval state ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_RERUN_AFTER_ImpactedCIUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveRFCApproval == "yes" && haveImpactedCIApproval == "yes" && rejectUserList_rfc.Contains(impactedCIUser))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_054_RERUN_AFTER_RFCCscUser_Reject_Approval ***");
                        isRecall = true;
                        Step_054_RERUN_AFTER_RFCCscUser_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_054_RERUN_AFTER_RFCCscUser_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_055_RFCCscUser_Click_Approve ***");
                        Step_055_RFCCscUser_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_055_RFCCscUser_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_058_ImpersonateUser_ImpactedCIUser ***");
                        Step_058_ImpersonateUser_ImpactedCIUser();
                    }
                    else
                        error = "Error when recall step: Step_058_ImpersonateUser_ImpactedCIUser";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_060_SearchAndOpenChange ***");
                        Step_060_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_060_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_061_SelectImpactedCIUserSignatureLine ***");
                        Step_061_SelectImpactedCIUserSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_061_SelectImpactedCIUserSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - ImpactedCIUser Reject if need.

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_ImpactedCIUser_Click_Approve()
        {
            try
            {
                if (haveRFCApproval.ToLower() == "yes" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    if (impactedCIUser.ToLower() != "no")
                    {
                        button = chg.Button_Approve();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (!flag) error = "Error when click on button approve.";
                            else
                            {
                                Thread.Sleep(5000);
                                chg.WaitLoading();
                            }
                        } error = "Cannot get button approve.";
                    }
                    else Console.WriteLine("*** This case has not ImpactedCIUser approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not RFC approval or have not impacted ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - Case have RFC approval

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_1_ImpersonateUser_ChangeCoor()
        {
            try
            {
                Step_019_ImpersonateUser_ChangeCoor();
                if (!flag)
                    error = "Error when recall step: Step_019_ImpersonateUser_ChangeCoor()";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_2_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_3_SearchAndOpenChange()
        {
            try
            {
                Step_021_Search_And_Open_Change();
                if (!flag)
                    error = "Error when recall step: Step_021_Search_And_Open_Change";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_1_RequestForReview()
        {
            try
            {
                if (changeType != "emergency change") 
                {
                    button = chg.Button_RequestApproval_RequestReview();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        Thread.Sleep(5000);
                        if (!flag)
                            error = "Error when click button request approval";
                        else
                            chg.WaitLoading();
                    }
                    else error = "Cannot get button request approval";
                }             
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_2_Verify_State_Review()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Review");
                    if (!flag)
                    {
                        error = "Invalid state";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox state."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_3_FillRiskAssessment_RequestForApproval()
        {
            try
            {
                datetime = chg.Datetime_Risk_Assessment_Last_Run();
                //--------------------------------------------------------------------
                if (datetime.Text.Trim() == "")
                {
                    flag = chg.Select_Tab("Risk Assessment");
                    if (flag)
                    {
                        button = chg.Button_FillOutRiskAssessment();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                riskAss.WaitLoading();

                                Thread.Sleep(5000);

                                // Verify Dialog title
                                snoelement ele = riskAss.Title();
                                flag = ele.Existed;
                                if (flag)
                                {
                                    flag = ele.MyText.Equals("Risk Assessment");
                                    if (!flag)
                                    {
                                        error = "Invalid title.";
                                    }
                                }
                                else
                                {
                                    error = "Cannot get label.";
                                }
                            }
                            else error = "Error when click on button Fill Out Risk Assessment.";
                        }
                        else error = "Cannot get button fill out risk assessment.";
                    }
                    else error = "Error when select Risk Assessment tab.";

                    //-- fill data
                    if (flag)
                    {
                        flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                        if (!flag)
                        {
                            error = "Cannot answer Risk Assessment questions";
                        }
                    }

                    //-- Submit

                    if (flag)
                    {
                        snoelement ele = riskAss.Form_Submit();
                        ele.MyElement.Submit();
                        Thread.Sleep(5000);
                        button = riskAss.Button_Close();
                        flag = button.Existed;
                        if (flag)
                            button.Click();
                        Thread.Sleep(5000);
                    }

                    //-- Reload
                    if (flag)
                    {
                        flag = chg.Save();
                        Thread.Sleep(5000);
                        if (flag)
                            chg.WaitLoading();
                    }
                    //-- Verify
                    if (flag)
                    {
                        datetime = chg.Datetime_Risk_Assessment_Last_Run();
                        flag = datetime.Existed;
                        if (flag)
                        {
                            flag = datetime.Text.Trim() != "";
                            if (!flag)
                                error = "Cannot fill and execute risk assessment.";
                        }
                        else { error = "Cannot get datetime last run."; }
                    }
                }

                //-- Request
                if (flag)
                {
                    button = chg.Button_RequestApproval_RequestReview();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        
                        if (!flag)
                            error = "Error when click button request approval";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                        }
                            
                    }
                    else error = "Cannot get button request approval";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_1_Change_Verify_Activity_67_1()
        {
            try
            {
                flag = chg.Select_Tab("Notes");
                if (flag) 
                {
                    string temp = Base.GData("Activity_67_1");
                    flag = chg.Verify_Activity(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_2_Change_Verify_Activity_67_2()
        {
            try
            {
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    string temp = Base.GData("Activity_67_2");
                    flag = chg.Verify_Activity(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_Verify_State_Approval()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Approval");
                    if (!flag) error = "Invalid state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_VerifyApprovers_ChangeCoor_Requested()
        {
            try
            {
                string approver = Base.GData("ChangeCoor");
                string conditions = "State=Requested|Approver=" + approver;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + approver, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_VerifyApprovers_Technical_Requested()
        {
            try
            {
                string approver = Base.GData("TechnicalApprover");
                string conditions = "State=Requested|Approver=" + approver;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + approver, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_VerifySelfAuthorizationIsNotApplicable_NotValid()
        {
            try
            {
                string approver = Base.GData("ChangeRequester");
                string conditions = "State=Not Valid|Approver=" + approver;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + approver, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_Add_Adhoc_Approver_TechPhase()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    if (adhocApprover_tech.ToLower() != "no")
                        flag = chg.Add_Related_Members("Approvers", adhocApprover_tech);

                    if (!flag)
                        error = "Error when add adhoc approver.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_VerifyAdhocApprover_NotYetRequested()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    if (adhocApprover_tech.ToLower() != "no")
                    {
                        string conditions = "State=Not Yet Requested|Approver=" + adhocApprover_tech;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_tech, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_ImpersonateUser_TechnicalApprover()
        {
            try
            {
                flag = home.ImpersonateUser(technicalApprover, true, rootuser);
                if (!flag) error = "Error when impersonate user technical approver.";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_SearchAndOpenChange()
        {
            try
            {
                Step_021_Search_And_Open_Change();
                if (!flag)
                    error = "Error when recall step: Step_021_Search_And_Open_Change";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_SelectTechnicalApproverSignatureLine()
        {
            try
            {
                string conditions = "State=Requested|Approver=" + technicalApprover;
                flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + technicalApprover, conditions, "State");
                if (!flag) error = "Error when open record.";
                else { Thread.Sleep(5000); chg.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region TechnicalApprover Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_TechnicalApprover_Click_Reject()
        {
            try
            {
                if (rejectUserList_tech.Contains(technicalApprover))
                {
                    textarea = chg.Textarea_Approval_Comments();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(technicalApprover + " Reject Approval");
                        if (flag)
                        {
                            button = chg.Button_Approval_Reject();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                if (!flag) error = "Error when click on button reject.";
                                else
                                {
                                    Thread.Sleep(5000);
                                    chg.WaitLoading();
                                    rejected_runtime++;
                                }
                            }
                            else error = "Cannot get button reject.";
                        }
                        else error = "Error when populate comment.";
                    }
                    else error = "Cannot get textarea comment.";
                }
                else Console.WriteLine("*** No need to run this step ***");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_RERUN_AFTER_TechnicalApprover_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (rejectUserList_tech.Contains(technicalApprover))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_065_1_ImpersonateUser_ChangeCoor ***");
                        Step_065_1_ImpersonateUser_ChangeCoor();
                    }
                    else
                        error = "Error when recall step: Step_065_1_ImpersonateUser_ChangeCoor";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_065_3_SearchAndOpenChange ***");
                        Step_065_3_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_065_3_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_066_3_FillRiskAssessment_RequestForApproval ***");
                        Step_066_3_FillRiskAssessment_RequestForApproval();
                    }
                    else
                        error = "Error when recall step: Step_066_3_FillRiskAssessment_RequestForApproval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_074_ImpersonateUser_TechnicalApprover ***");
                        Step_074_ImpersonateUser_TechnicalApprover();
                    }
                    else
                        error = "Error when recall step: Step_074_ImpersonateUser_TechnicalApprover";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_076_SearchAndOpenChange ***");
                        Step_076_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_076_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_077_SelectTechnicalApproverSignatureLine ***");
                        Step_077_SelectTechnicalApproverSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_077_SelectTechnicalApproverSignatureLine";
                }
                else Console.WriteLine("*** No need to run this step ***");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - TechnicalApprover Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_TechnicalApprover_Click_Approve()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) error = "Error when click on button approve.";
                    else
                    {
                        Thread.Sleep(5000);
                        chg.WaitLoading();
                    }
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_Verify_State_Approval()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Approval", true);
                    if (!flag) error = "Invalid state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_082_ImpersonateUser_ManagerReviewApprover_IfNeed()
        {
            try
            {
                if (haveChangeManagerReview == "yes" && managerReview.ToLower() != "no")
                {
                    flag = home.ImpersonateUser(managerReview, true, rootuser);
                    if (!flag) error = "Error when impersonate change manager review.";
                    else home.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case no need manager review approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_083_SystemSetting()
        {
            try
            {
                if (haveChangeManagerReview == "yes" && managerReview.ToLower() != "no")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                }
                else
                {
                    Console.WriteLine("*** This case no need manager review approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_SearchAndOpenChange()
        {
            try
            {
                if (haveChangeManagerReview == "yes" && managerReview.ToLower() != "no")
                {
                    Step_021_Search_And_Open_Change();
                    if (!flag)
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                }
                else
                {
                    Console.WriteLine("*** This case no need manager review approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_085_SelectChangeManagerReviewApproverSignatureLine()
        {
            try
            {
                if (haveChangeManagerReview == "yes" && managerReview.ToLower() != "no")
                {
                    string conditions = "State=Requested|Approver=" + managerReview + "|Assignment group=" + managerReviewGroup;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + managerReview, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else { Thread.Sleep(5000); chg.WaitLoading(); }
                }
                else
                {
                    Console.WriteLine("*** This case no need manager review approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region ChangeManagerReview Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_ChangeManagerReview_Click_Reject()
        {
            try
            {
                if (haveChangeManagerReview == "yes")
                {
                    if (rejectUserList_tech.Contains(managerReview))
                    {
                        Thread.Sleep(2000);
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(managerReview + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case no need manager review approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_RERUN_AFTER_ChangeManagerReview_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveChangeManagerReview == "yes" && rejectUserList_tech.Contains(managerReview))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_079_RERUN_AFTER_TechnicalApprover_Reject_Approval ***");
                        isRecall = true;
                        Step_079_RERUN_AFTER_TechnicalApprover_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_079_RERUN_AFTER_TechnicalApprover_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_080_TechnicalApprover_Click_Approve ***");
                        Step_080_TechnicalApprover_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_080_TechnicalApprover_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_082_ImpersonateUser_ManagerReviewApprover_IfNeed ***");
                        Step_082_ImpersonateUser_ManagerReviewApprover_IfNeed();
                    }
                    else
                        error = "Error when recall step: Step_082_ImpersonateUser_ManagerReviewApprover_IfNeed";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_083_SystemSetting ***");
                        Step_083_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_083_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_084_SearchAndOpenChange ***");
                        Step_084_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_084_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_085_SelectChangeManagerReviewApproverSignatureLine ***");
                        Step_085_SelectChangeManagerReviewApproverSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_085_SelectChangeManagerReviewApproverSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion ChangeManagerReview Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_ChangeManagerReview_Click_Approve()
        {
            try
            {
                if (haveChangeManagerReview == "yes" && managerReview.ToLower() != "no")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                       
                        if (!flag) error = "Error when click on button approve.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                        }
                    } error = "Cannot get button approve.";
                }
                else
                {
                    Console.WriteLine("*** This case no need manager review approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_VerifyStateApproval_Approval()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Approval");
                    if (!flag) error = "Invalid state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_VerifyApprovers_Technical_Approved()
        {
            try
            {
                string conditions = "State=Approved|Approver=" + technicalApprover;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + technicalApprover, conditions);
                if (!flag) error = "Invalid state of technical approver. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_VerifyApprovers_Adhoc()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes")
                {
                    if (haveAdhocApprovalTech.ToLower() == "yes")
                    {
                        string conditions = "State=Not Yet Requested|Approver=" + adhocApprover_tech;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_tech, conditions);
                        if (!flag) error = "Invalid state of adhoc approver. Expected:(" + conditions + ")";
                    }
                    else
                    {
                        Console.WriteLine("*** This case has not Adhoc Approver for Technical phase ***");
                    }
                }
                else
                {
                    if (haveAdhocApprovalTech == "yes")
                    {
                        string conditions = "State=Requested|Approver=" + adhocApprover_tech;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_tech, conditions);
                        if (!flag) error = "Invalid state of adhoc approver. Expected:(" + conditions + ")";
                    }
                    else
                    {
                        Console.WriteLine("*** This case has not Adhoc Approver for Technical phase ***");
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_VerifyApprovers_AffectedCIUser_Requested()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes")
                {
                    if (affectedCIUser.ToLower() != "no")
                    {
                        string conditions = "State=Requested|Approver=" + affectedCIUser + "|Assignment group=" + affectedCIGroup;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + affectedCIUser, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                    else Console.WriteLine("*** This case has not affected ci approval ***");
                }
                else
                {
                    Console.WriteLine("*** This case has not AffectedCIUser approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_093_ImpersonateUser_AffectedCIUser_IfNeed()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes" && affectedCIUser.ToLower() != "no")
                {
                    flag = home.ImpersonateUser(affectedCIUser, true, rootuser);
                    if (!flag) error = "Error when impersonate affected ci user.";
                    else home.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case has not affected ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_094_SystemSetting()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes" && affectedCIUser.ToLower() != "no")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                }
                else
                {
                    Console.WriteLine("*** This case has not affected ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_095_SearchAndOpenChange()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes" && affectedCIUser.ToLower() != "no")
                {
                    Step_021_Search_And_Open_Change();
                    if (!flag)
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                }
                else
                {
                    Console.WriteLine("*** This case has not affected ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_096_SelectAffectedCIUserSignatureLine()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes" && affectedCIUser.ToLower() != "no")
                {
                    string conditions = "State=Requested|Approver=" + affectedCIUser + "|Assignment group=" + affectedCIGroup;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + affectedCIUser, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else chg.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case has not affected ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region AffectCIUser Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_097_AffectedCIUser_Click_Reject()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes")
                {
                    if (rejectUserList_tech.Contains(affectedCIUser))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(affectedCIUser + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case no need affected ci approve. ***");
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_098_RERUN_AFTER_AffectedCIUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveAffectedCIApproval.ToLower() == "yes" && rejectUserList_tech.Contains(affectedCIUser))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_087_RERUN_AFTER_ChangeManagerReview_Reject_Approval ***");
                        isRecall = true;
                        Step_087_RERUN_AFTER_ChangeManagerReview_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_087_RERUN_AFTER_ChangeManagerReview_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_088_ChangeManagerReview_Click_Approve ***");
                        Step_088_ChangeManagerReview_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_088_ChangeManagerReview_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_093_ImpersonateUser_AffectedCIUser_IfNeed ***");
                        Step_093_ImpersonateUser_AffectedCIUser_IfNeed();
                    }
                    else
                        error = "Error when recall step: Step_093_ImpersonateUser_AffectedCIUser_IfNeed";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_094_SystemSetting ***");
                        Step_094_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_094_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_095_SearchAndOpenChange ***");
                        Step_095_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_095_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_096_SelectAffectedCIUserSignatureLine ***");
                        Step_096_SelectAffectedCIUserSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_096_SelectAffectedCIUserSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion AffectCIUser Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_099_AffectedCIUser_Click_Approve()
        {
            try
            {
                if (haveAffectedCIApproval.ToLower() == "yes" && affectedCIUser.ToLower() != "no")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag) error = "Error when click on button approve.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                        }
                    } error = "Cannot get button approve.";
                }
                else
                {
                    Console.WriteLine("*** This case has not affected ci approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_100_ImpersonateUser_AdhocApprover()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    flag = home.ImpersonateUser(adhocApprover_tech, true, rootuser);
                    if (!flag) error = "Error when impersonate user adhoc approver.";
                    else home.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_101_SystemSetting()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_102_SearchAndOpenChange()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    Step_021_Search_And_Open_Change();
                    if (!flag)
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_103_SelectChangeAdhocApproverSignatureLine()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    string conditions = "State=Requested|Approver=" + adhocApprover_tech;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_tech, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else { Thread.Sleep(5000); chg.WaitLoading(); }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region AdhocUser Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_104_AdhocUser_Click_Reject()
        {
            try
            {
                if (haveAdhocApprovalTech.ToLower() == "yes")
                {
                    if (rejectUserList_tech.Contains(adhocApprover_tech))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(adhocApprover_tech + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case no need adhoc approve. ***");
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_105_RERUN_AFTER_AdhocUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (haveAdhocApprovalTech == "yes" && rejectUserList_tech.Contains(adhocApprover_tech))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_098_RERUN_AFTER_AffectedCIUser_Reject_Approval ***");
                        isRecall = true;
                        Step_098_RERUN_AFTER_AffectedCIUser_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_098_RERUN_AFTER_AffectedCIUser_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_099_AffectedCIUser_Click_Approve ***");
                        Step_099_AffectedCIUser_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_099_AffectedCIUser_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_100_ImpersonateUser_AdhocApprover ***");
                        Step_100_ImpersonateUser_AdhocApprover();
                    }
                    else
                        error = "Error when recall step: Step_100_ImpersonateUser_AdhocApprover";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_101_SystemSetting ***");
                        Step_101_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_101_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_102_SearchAndOpenChange ***");
                        Step_102_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_102_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_103_SelectChangeAdhocApproverSignatureLine ***");
                        Step_103_SelectChangeAdhocApproverSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_103_SelectChangeAdhocApproverSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion AffectCIUser Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_106_AdhocUser_Click_Approve()
        {
            try
            {
                if (haveAdhocApprovalTech.ToLower() == "yes")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag) error = "Error when click on button approve.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                        }
                    } error = "Cannot get button approve.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_107_VerifyStateApproval_Approval()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Approval");
                        if (!flag) error = "Invalid state value.";
                    }
                    else { error = "Cannot get combobox state."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_108_VerifyApproversState_Technical_Approved()
        {
            try
            {
                Step_090_VerifyApprovers_Technical_Approved();
                if (!flag)
                    error = "Error when recall step: Step_090_VerifyApprovers_Technical_Approved";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_109_VerifyApproversState_Adhoc_Approved()
        {
            try
            {
                if (haveAdhocApprovalTech == "yes")
                {
                    string conditions = "State=Approved|Approver=" + adhocApprover_tech;
                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_tech, conditions);
                    if (!flag) error = "Invalid state of adhoc approver. Expected:(" + conditions + ")";
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_110_VerifyApproversState_Business_Requested()
        {
            try
            {
                if (changeType == "normal change")
                {
                    string conditions = "State=Requested|Approver=" + businessApprover;
                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + businessApprover, conditions);
                    if (!flag) error = "Invalid state of business approver. Expected:(" + conditions + ")";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_VerifyApproversState_ChangeCoor_NoLongerRequired()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string conditions = "State=No Longer Required|Approver=" + temp;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions);
                if (!flag) error = "Invalid state of change coornidator approver. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_112_ImpersonateUser_ChangeCoor()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (haveAdhocApprovalBus == "yes")
                    {
                        Step_019_ImpersonateUser_ChangeCoor();
                        if (!flag)
                            error = "Error when recall step: Step_019_ImpersonateUser_ChangeCoor()";
                        else home.WaitLoading();
                    }
                    else
                    {
                        Console.WriteLine("*** This case has not Adhoc Approver for Business phase ***");
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_113_SystemSetting()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (haveAdhocApprovalBus == "yes")
                    {
                        Step_004_SystemSetting();
                        if (!flag)
                            error = "Error when recall step: Step_004_SystemSetting";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_114_SearchAndOpenChange()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (haveAdhocApprovalBus == "yes")
                    {
                        Step_021_Search_And_Open_Change();
                        if (!flag)
                            error = "Error when recall step: Step_021_Search_And_Open_Change";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_115_Add_Adhoc_Approver_BusPhase()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (haveAdhocApprovalBus == "yes")
                    {
                        flag = chg.Add_Related_Members("Approvers", adhocApprover_bus);
                    }
                    else
                    {
                        Console.WriteLine("*** This case has not Adhoc Approver for Business phase ***");
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_116_VerifyAdhocApprover_NotYetRequested()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (haveAdhocApprovalBus == "yes")
                    {
                        string conditions = "State=Not Yet Requested|Approver=" + adhocApprover_bus;
                        flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_bus, conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_117_ImpersonateUser_BusinessApprover()
        {
            try
            {
                if (changeType == "normal change")
                {
                    string temp = Base.GData("BusinessApproverUserId");
                    if (temp.ToLower() == "no" || temp.Equals(""))
                    {
                        temp = Base.GData("BusinessApprover");
                    }
                    else
                    {
                        temp = Base.GData("BusinessApprover") + ";" + temp;
                    }

                    flag = home.ImpersonateUser(temp, true, rootuser);
                    if (!flag) error = "Error when impersonate user business approver.";
                    else home.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_118_SystemSetting()
        {
            try
            {
                if (changeType == "normal change")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                    else home.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_119_OpenMyApproval()
        {
            try
            {
                if (changeType == "normal change")
                {
                    flag = home.LeftMenuItemSelect("Service Desk", "My Change Approvals");
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else
                        error = "Error when open my change approvals.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_SelectBusinessApproverLine()
        {
            try
            {
                if (changeType == "normal change")
                {
                    //-- Input information
                    if (changeId == null || changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                        if (change_rejected)
                        {
                            addPara = new Auto.AddParameter("Please input Number of Times rejected (1 - 10)");
                            addPara.ShowDialog();
                            rejected_before = Int32.Parse(addPara.value);
                            addPara.Close();
                            addPara = null;
                        }
                    }
                    //-----------------------------------------------------------------------
                    string temp = approvalList.List_Title().MyText;
                    flag = temp.Contains("Approvals");
                    if (flag)
                    {
                        flag = approvalList.SearchAndOpen("Approval for", changeId, "State=Requested|Approver=" + businessApprover + "|Approval for=" + changeId, "State");
                        if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                        else approvalList.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                    }
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Business Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_BusinessApprover_Click_Reject()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (rejectUserList_business.Contains(businessApprover))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(businessApprover + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case no need business approve. ***");
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_122_RERUN_AFTER_BusinessApprover_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (changeType == "normal change" && rejectUserList_business.Contains(businessApprover))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_105_RERUN_AFTER_AdhocUser_Reject_Approval ***");
                        isRecall = true;
                        Step_105_RERUN_AFTER_AdhocUser_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_105_RERUN_AFTER_AdhocUser_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_106_AdhocUser_Click_Approve ***");
                        Step_106_AdhocUser_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_106_AdhocUser_Click_Approve";
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_117_ImpersonateUser_BusinessApprover ***");
                        Step_117_ImpersonateUser_BusinessApprover();
                    }
                    else
                        error = "Error when recall step: Step_117_ImpersonateUser_BusinessApprover";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_118_SystemSetting ***");
                        Step_118_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_118_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_119_OpenMyApproval ***");
                        Step_119_OpenMyApproval();
                    }
                    else
                        error = "Error when recall step: Step_119_OpenMyApproval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_120_SelectBusinessApproverLine ***");
                        Step_120_SelectBusinessApproverLine();
                    }
                    else
                        error = "Error when recall step: Step_120_SelectBusinessApproverLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion Business Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_123_BusinessApprover_Click_Approve()
        {
            try
            {
                if (changeType == "normal change")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag) error = "Error when click on button approve.";
                        else
                        {
                            Thread.Sleep(5000);
                            chglist.WaitLoading();
                        }
                    } error = "Cannot get button approve.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_124_Verify_BusinessApprover_Approved()
        {
            try
            {
                if (changeType == "normal change")
                {
                    //-- Input information
                    if (changeId == null || changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                        if (change_rejected)
                        {
                            addPara = new Auto.AddParameter("Please input Number of Times rejected (1 - 10)");
                            addPara.ShowDialog();
                            rejected_before = Int32.Parse(addPara.value);
                            addPara.Close();
                            addPara = null;
                        }
                    }
                    //-----------------------------------------------------------------------

                    Step_119_OpenMyApproval();

                    if (flag)
                    {
                        flag = chglist.Filter("Approval for;is;" + changeId + "|and|Approver;is;" + businessApprover);

                        if (flag)
                        {

                            string condition = "State=Approved|Approver=" + businessApprover + "|Approval for=" + changeId;
                            flag = chglist.VerifyRow(condition);
                            if (!flag) error = "Error when veify approved (id:" + changeId + ")";
                        }
                        else
                            error = "Cannot filter as condition.";
                    }
                    else { error = "Error when open my approval."; }

                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_125_ImpersonateUser_ImpactedCIUser()
        {
            try
            {
                if (changeType.ToLower() == "normal change")
                {
                    if (haveImpactedCIApproval.ToLower() == "yes")
                    {
                        flag = home.ImpersonateUser(impactedCIUser, true, rootuser);
                        if (!flag) error = "Error when impersonate user impacted ci approver.";
                        else home.WaitLoading();
                    }
                    else
                    {
                        Console.WriteLine("*** This case has not impacted ci approval ***");
                    }
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_126_SystemSetting()
        {
            try
            {
                if (changeType.ToLower() == "normal change" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                }
                else
                {
                    Console.WriteLine("*** This case no need Impacted CI approve. ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_127_SearchAndOpenChange()
        {
            try
            {
                if (changeType.ToLower() == "normal change" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    Step_021_Search_And_Open_Change();
                    if (!flag)
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                }
                else
                {
                    Console.WriteLine("*** This case no need Impacted CI approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_128_SelectImpactedCIUserSignatureLine()
        {
            try
            {
                if (changeType.ToLower() == "normal change" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    string conditions = "State=Requested|Approver=" + impactedCIUser + "|Assignment group=" + impactedCIGroup;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + impactedCIUser, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else chg.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case no need Impacted CI approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region ImpactedCIUser Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_129_ImpactedCIUser_Click_Reject()
        {
            try
            {
                if (changeType.ToLower() == "normal change")
                {
                    if (haveImpactedCIApproval.ToLower() == "yes" && rejectUserList_business.Contains(impactedCIUser))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(impactedCIUser + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case no need Impacted CI approve. ***");
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_130_RERUN_AFTER_ImpactedCIUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (changeType.ToLower() == "normal change" && haveImpactedCIApproval.ToLower() == "yes" && rejectUserList_business.Contains(impactedCIUser))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_122_RERUN_AFTER_BusinessApprover_Reject_Approval ***");
                        isRecall = true;
                        Step_122_RERUN_AFTER_BusinessApprover_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_122_RERUN_AFTER_BusinessApprover_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_123_BusinessApprover_Click_Approve ***");
                        Step_123_BusinessApprover_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_123_BusinessApprover_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_125_ImpersonateUser_ImpactedCIUser ***");
                        Step_125_ImpersonateUser_ImpactedCIUser();
                    }
                    else
                        error = "Error when recall step: Step_125_ImpersonateUser_ImpactedCIUser";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_126_SystemSetting ***");
                        Step_126_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_126_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_127_SearchAndOpenChange ***");
                        Step_127_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_127_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_128_SelectImpactedCIUserSignatureLine ***");
                        Step_128_SelectImpactedCIUserSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_128_SelectImpactedCIUserSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion ImpactedCIUser Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_131_ImpactedCIUser_Click_Approve()
        {
            try
            {
                if (changeType.ToLower() == "normal change" && haveImpactedCIApproval.ToLower() == "yes")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag) error = "Error when click on button approve.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                        }
                    } error = "Cannot get button approve.";
                }
                else
                {
                    Console.WriteLine("*** No need to run this step ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_132_ImpersonateUser_AdhocApprover()
        {
            try
            {
                if (changeType == "normal change")
                {
                    if (haveAdhocApprovalBus == "yes")
                    {
                        flag = home.ImpersonateUser(adhocApprover_bus, true, rootuser);
                        if (!flag) error = "Error when impersonate user adhoc approver.";
                        else home.WaitLoading();
                    }
                    else
                    {
                        Console.WriteLine("*** This case has NO Adhoc Approver for Business Approval ***");
                    }
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_133_SystemSetting()
        {
            try
            {
                if (changeType == "normal change" && haveAdhocApprovalBus == "yes")
                {
                    Step_004_SystemSetting();
                    if (!flag)
                        error = "Error when recall step: Step_004_SystemSetting";
                }
                else
                {
                    Console.WriteLine("*** This case no need adhoc approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_134_SearchAndOpenChange()
        {
            try
            {
                if (changeType == "normal change" && haveAdhocApprovalBus == "yes")
                {
                    Step_021_Search_And_Open_Change();
                    if (!flag)
                        error = "Error when recall step: Step_021_Search_And_Open_Change";
                }
                else
                {
                    Console.WriteLine("*** This case no need adhoc approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_135_SelectChangeAdhocApproverSignatureLine()
        {
            try
            {
                if (changeType == "normal change" && haveAdhocApprovalBus == "yes")
                {
                    string conditions = "State=Requested|Approver=" + adhocApprover_bus;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + adhocApprover_bus, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else { Thread.Sleep(5000); chg.WaitLoading(); }
                }
                else
                {
                    Console.WriteLine("*** This case no need adhoc approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Adhoc Approver Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_136_AdhocUser_Click_Reject()
        {
            try
            {
                if (changeType.ToLower() == "normal change" && haveAdhocApprovalBus.ToLower() == "yes")
                {
                    if (rejectUserList_business.Contains(adhocApprover_bus))
                    {
                        textarea = chg.Textarea_Approval_Comments();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            flag = textarea.SetText(adhocApprover_bus + " Reject Approval");
                            if (flag)
                            {
                                button = chg.Button_Approval_Reject();
                                flag = button.Existed;
                                if (flag)
                                {
                                    flag = button.Click();
                                    if (!flag) error = "Error when click on button reject.";
                                    else
                                    {
                                        Thread.Sleep(5000);
                                        chg.WaitLoading();
                                        rejected_runtime++;
                                    }
                                }
                                else error = "Cannot get button reject.";
                            }
                            else error = "Error when populate comment.";
                        }
                        else error = "Cannot get textarea comment.";
                    }
                    else Console.WriteLine("*** No need to run this step ***");
                }
                else
                {
                    Console.WriteLine("*** This case no need adhoc approve. ***");
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_137_RERUN_AFTER_AdhocUser_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (changeType == "normal change" && haveAdhocApprovalBus == "yes" && rejectUserList_business.Contains(adhocApprover_bus))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_130_RERUN_AFTER_ImpactedCIUser_Reject_Approval ***");
                        isRecall = true;
                        Step_130_RERUN_AFTER_ImpactedCIUser_Reject_Approval();
                        isRecall = false;
                    }
                    else
                        error = "Error when recall step: Step_130_RERUN_AFTER_ImpactedCIUser_Reject_Approval()";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_131_ImpactedCIUser_Click_Approve ***");
                        Step_131_ImpactedCIUser_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_131_ImpactedCIUser_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_132_ImpersonateUser_AdhocApprover ***");
                        Step_132_ImpersonateUser_AdhocApprover();
                    }
                    else
                        error = "Error when recall step: Step_132_ImpersonateUser_AdhocApprover";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_133_SystemSetting ***");
                        Step_133_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_133_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_134_SearchAndOpenChange ***");
                        Step_134_SearchAndOpenChange();
                    }
                    else
                        error = "Error when recall step: Step_134_SearchAndOpenChange";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_135_SelectChangeAdhocApproverSignatureLine ***");
                        Step_135_SelectChangeAdhocApproverSignatureLine();
                    }
                    else
                        error = "Error when recall step: Step_135_SelectChangeAdhocApproverSignatureLine";
                }
                else
                {
                    Console.WriteLine("*** Not run this step ***");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion Adhoc Approver Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_138_AdhocUser_Click_Approve()
        {
            try
            {
                if (changeType == "normal change" && haveAdhocApprovalBus == "yes")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag) error = "Error when click on button approve.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                        }
                    } error = "Cannot get button approve.";
                }
                else
                {
                    Console.WriteLine("*** This case no need adhoc approve. ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_139_ImpersonateUser_ChangeManager()
        {
            try
            {
                flag = home.ImpersonateUser(changeManager, true, rootuser);
                if (!flag) error = "Error when impersonate user change manager.";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_140_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_141_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Change Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_142_SelectManagerApproverLine()
        {
            try
            {
                //-- Input information
                if (changeId == null || changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                    if (change_rejected)
                    {
                        addPara = new Auto.AddParameter("Please input Number of Times rejected (1 - 10)");
                        addPara.ShowDialog();
                        rejected_before = Int32.Parse(addPara.value);
                        addPara.Close();
                        addPara = null;
                    }
                }
                //-----------------------------------------------------------------------
                string temp = approvalList.List_Title().MyText;

                flag = temp.Contains("Approvals");
                if (flag)
                {
                    flag = chglist.SearchAndOpen("Approval for", changeId, "State=Requested|Approver=" + changeManager + "|Approval for=" + changeId, "State");
                    if (!flag)
                        error = "Error when search and open approval (id:" + changeId + ")";
                    else
                    {
                        Thread.Sleep(4000);
                        chg.WaitLoading();
                    }
                        
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Change Manager Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_143_ChangeManager_Click_Reject()
        {
            try
            {
                if (rejectUserList_cab.Contains(changeManager))
                {
                    textarea = chg.Textarea_Approval_Comments();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(changeManager + " Reject Approval");
                        if (flag)
                        {
                            button = chg.Button_Approval_Reject();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                if (!flag) error = "Error when click on button reject.";
                                else
                                {
                                    Thread.Sleep(5000);
                                    chg.WaitLoading();
                                    rejected_runtime++;
                                }
                            }
                            else error = "Cannot get button reject.";
                        }
                        else error = "Error when populate comment.";
                    }
                    else error = "Cannot get textarea comment.";
                }
                else Console.WriteLine("*** No need to run this step ***");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_144_RERUN_AFTER_ChangeManager_Reject_Approval()
        {
            try
            {
                bool flagR = false;

                if (isRecall)
                    flagR = true;
                else
                {
                    if (rejectUserList_cab.Contains(changeManager))
                        flagR = true;
                }

                if (flagR)
                {
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_137_RERUN_AFTER_AdhocUser_Reject_Approval ***");
                        isRecall = true;
                        Step_137_RERUN_AFTER_AdhocUser_Reject_Approval();
                        isRecall = false;
                    }

                    else
                        error = "Error when recall step: Step_137_RERUN_AFTER_AdhocUser_Reject_Approval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_138_AdhocUser_Click_Approve ***");
                        Step_138_AdhocUser_Click_Approve();
                    }
                    else
                        error = "Error when recall step: Step_138_AdhocUser_Click_Approve";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_139_ImpersonateUser_ChangeManager ***");
                        Step_139_ImpersonateUser_ChangeManager();
                    }
                    else
                        error = "Error when recall step: Step_139_ImpersonateUser_ChangeManager";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_004_SystemSetting ***");
                        Step_004_SystemSetting();
                    }
                    else
                        error = "Error when recall step: Step_004_SystemSetting";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_141_OpenMyApproval ***");
                        Step_141_OpenMyApproval();
                    }
                    else
                        error = "Error when recall step: Step_141_OpenMyApproval";
                    //------------
                    if (flag)
                    {
                        Console.WriteLine("*** Call: Step_142_SelectManagerApproverLine ***");
                        Step_142_SelectManagerApproverLine();
                    }
                    else
                        error = "Error when recall step: Step_142_SelectManagerApproverLine";
                }
                else Console.WriteLine("*** No need to run this step ***");

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion Change Manager Reject if need

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_01_ChangeManager_Click_Approve()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) error = "Error when click on button approve.";
                    else
                    {
                        Thread.Sleep(5000);
                        chglist.WaitLoading();
                    }
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_02_Verify_Manager_Approved()
        {
            try
            {
                //-- Input information
                if (changeId == null || changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                    if (change_rejected)
                    {
                        addPara = new Auto.AddParameter("Please input Number of Times rejected (1 - 10)");
                        addPara.ShowDialog();
                        rejected_before = Int32.Parse(addPara.value);
                        addPara.Close();
                        addPara = null;
                    }
                }
                //-----------------------------------------------------------------------

                Step_141_OpenMyApproval();

                if (flag)
                {
                    flag = chglist.Filter("Approval for;is;" + changeId + "|and|Approver;is;" + changeManager);

                    if (flag)
                    {
                        string condition = "State=Approved|Approver=" + changeManager + "|Approval for=" + changeId;
                        flag = chglist.VerifyRow(condition);
                        if (!flag) error = "Error when veify approved (id:" + changeId + ")";
                    }
                    else
                        error = "Cannot filter as condition.";
                }
                else
                {
                    error = "Error when open my approval.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_03_ImpersonateUser_ChangeCoor()
        {
            try
            {
                Step_019_ImpersonateUser_ChangeCoor();
                if (!flag)
                    error = "Error when recall step: Step_019_ImpersonateUser_ChangeCoor()";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_04_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_05_SearchAndOpenChange()
        {
            try
            {
                Step_021_Search_And_Open_Change();
                if (!flag)
                    error = "Error when recall step: Step_021_Search_And_Open_Change";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_06_VerifyStateApproval_Scheduled()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Scheduled");
                    if (!flag) error = "Invalid state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_07_VerifyAutoGeneratedChangeTask_First()
        {
            try
            {
                string conditions = "Short description=1 Execute Implementation Plan|State=Open";
                flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found first change task: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_08_VerifyAutoGeneratedChangeTask_Second()
        {
            try
            {
                string conditions = "Short description=2 Execute Test Plan|State=Pending";
                flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found second change task: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_09_VerifyAutoGeneratedChangeTask_Third()
        {
            try
            {
                string conditions = "Short description=3 Record Test Results|State=Pending";
                flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found third change task: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_10_PopulateChangeState_InProgress()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("In Progress");
                    if (!flag) { error = "Cannot populate state value."; }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_11_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_146_1_Change_Verify_Activity_146_1()
        {
            try
            {
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    string temp = Base.GData("Activity_146_1");
                    flag = chg.Verify_Activity(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_146_2_Change_Verify_Activity_146_2()
        {
            try
            {
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    string temp = Base.GData("Activity_146_2");
                    flag = chg.Verify_Activity(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_147_OpenFirstChangeTask()
        {
            try
            {
                string conditions = "Short description=1 Execute Implementation Plan|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    error = "Error when open first change task: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_148_VerifyAssignmentOfTheChangeTask_1()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                Thread.Sleep(2000);
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (flag)
                    {
                        temp = Base.GData("ChangeCoor");
                        Thread.Sleep(1000);
                        lookup = chg.Lookup_AssignedTo();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(temp, true);
                            if (!flag)
                            {
                                error = "Invalid assigned to value.";
                            }
                            else { chg.Update(true); chg.WaitLoading(); }
                        }
                        else error = "Cannot get lookup assigned to.";
                    }
                    else { error = "Invalid assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_149_1_OpenSecondChangeTask()
        {
            try
            {
                string conditions = "Short description=2 Execute Test Plan|State=Pending";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    error = "Error when open second change task: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_149_2_VerifyAssignmentOfTheChangeTask_2()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                Thread.Sleep(2000);
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (flag)
                    {
                        temp = Base.GData("ChangeCoor");
                        Thread.Sleep(1000);
                        lookup = chg.Lookup_AssignedTo();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(temp, true);
                            if (!flag)
                            {
                                error = "Invalid assigned to value.";
                            }
                            else { chg.Update(true); chg.WaitLoading(); }
                        }
                        else error = "Cannot get lookup assigned to.";
                    }
                    else { error = "Invalid assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_149_3_OpenThirdChangeTask()
        {
            try
            {
                string conditions = "Short description=3 Record Test Results|State=Pending";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    error = "Error when open third change task: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_149_4_VerifyAssignmentOfTheChangeTask_3()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                Thread.Sleep(2000);
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (flag)
                    {
                        temp = Base.GData("ChangeCoor");
                        Thread.Sleep(1000);
                        lookup = chg.Lookup_AssignedTo();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(temp, true);
                            if (!flag)
                            {
                                error = "Invalid assigned to value.";
                            }
                            else { chg.Update(true); chg.WaitLoading(); }
                        }
                        else error = "Cannot get lookup assigned to.";
                    }
                    else { error = "Invalid assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_150_ImpersonateUser_ChangeImplementer()
        {
            try
            {
                string temp = Base.GData("ChangeImplementer");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user change implementer.";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_151_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_152_SearchAndOpenChange()
        {
            try
            {
                Step_021_Search_And_Open_Change();
                if (!flag)
                    error = "Error when recall step: Step_021_Search_And_Open_Change";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_153_1_Open_TheFirstChangeTask()
        {
            try
            {
                string conditions = "Short description=1 Execute Implementation Plan|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    error = "Error when open first change task 1: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_153_2_UpdateStateAndWorknotes_TheFirstChangeTask()
        {
            try
            {
                Thread.Sleep(2000);
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Work in Progress");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 1 - Work in Progress";
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Work in Progress)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_153_3_SaveChangeTask_1()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeTaskId_1 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change task 1 id:(" + changeTaskId_1 + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change task 1.";
                    else chg.WaitLoading();
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_154_1_ChangeTask_Verify_Activity_154_1()
        {
            try
            {
                string temp = Base.GData("Activity_154_1");
                flag = chg.Verify_Activity(temp);
                if (!flag) { error = "Invalid activity note 154_1. Expected:(" + temp + ")"; flagExit = false; } 
               
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_154_2_ChangeTask_Verify_Activity_154_2()
        {
            try
            {
                string temp = Base.GData("Activity_154_2");
                flag = chg.Verify_Activity(temp);
                if (!flag) { error = "Invalid activity note 154_2. Expected:(" + temp + ")"; flagExit = false; } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_155_CloseChangeTask_1()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 1 - Close Complete"; ;
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                            else
                            {
                                flag = chg.Update(true);
                                if (!flag) error = "Error when update change task 1.";
                            }
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Closed Complete)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_156_VerifyChangeTaskTab_ChangeTask_1()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeTaskId_1 == null || changeTaskId_1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change task Id 1.");
                    addPara.ShowDialog();
                    changeTaskId_1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + changeTaskId_1 + "|State=Closed Complete";
                flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found first change task: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_157_1_Open_TheSecondChangeTask()
        {
            try
            {
                string conditions = "Short description=2 Execute Test Plan|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    flagExit = false;
                    error = "Error when open first change task 1: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_157_2_UpdateStateAndWorknotes_TheSecondChangeTask()
        {
            try
            {
                Thread.Sleep(2000);
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Work in Progress");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 2 - Work in Progress";
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Work in Progress)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_157_3_SaveChangeTask_2()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeTaskId_2 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change task 2 id:(" + changeTaskId_2 + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change task 2.";
                    else chg.WaitLoading();
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_158_1_ChangeTask_Verify_Activity_158_1()
        {
            try
            {
                string temp = Base.GData("Activity_158_1");
                flag = chg.Verify_Activity(temp);
                if (!flag) { error = "Invalid activity note 158_1. Expected:(" + temp + ")"; flagExit = false; } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_158_2_ChangeTask_Verify_Activity_158_2()
        {
            try
            {
                string temp = Base.GData("Activity_158_2");
                flag = chg.Verify_Activity(temp);
                if (!flag) { error = "Invalid activity note 158_2. Expected:(" + temp + ")"; flagExit = false; } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_159_CloseChangeTask_2()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 2 - Close Complete"; ;
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                            else
                            {
                                flag = chg.Update(true);
                                if (!flag) error = "Error when update change task 2.";
                            }
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Closed Complete)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_160_VerifyChangeTaskTab_ChangeTask_2()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeTaskId_2 == null || changeTaskId_2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change task Id 2.");
                    addPara.ShowDialog();
                    changeTaskId_2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + changeTaskId_2 + "|State=Closed Complete";
                flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found second change task: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_161_1_Open_TheThirdChangeTask()
        {
            try
            {
                string conditions = "Short description=3 Record Test Results|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found thrid change task: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_161_2_UpdateStateAndWorknotes_TheThridChangeTask()
        {
            try
            {
                Thread.Sleep(2000);
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Work in Progress");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 3 - Work in Progress";
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Work in Progress)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_161_3_SaveChangeTask_3()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeTaskId_3 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change task 3 id:(" + changeTaskId_3 + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change task 3.";
                    else chg.WaitLoading();
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_162_1_ChangeTask_Verify_Activity_162_1()
        {
            try
            {
                string temp = Base.GData("Activity_162_1");
                flag = chg.Verify_Activity(temp);
                if (!flag) { error = "Invalid activity note 162_1. Expected:(" + temp + ")"; flagExit = false; } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_162_2_ChangeTask_Verify_Activity_162_2()
        {
            try
            {
                string temp = Base.GData("Activity_162_2");
                flag = chg.Verify_Activity(temp);
                if (!flag) { error = "Invalid activity note 162_2. Expected:(" + temp + ")"; flagExit = false; } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_163_CloseChangeTask_ChangeTask_3()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 3 - Close Complete"; ;
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                            else
                            {
                                flag = chg.Update(true);
                                if (!flag) error = "Error when update change task 3.";
                            }
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Closed Complete)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_164_VerifyChangeTaskTab_ChangeTask_3()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeTaskId_3 == null || changeTaskId_3 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change task Id 3.");
                    addPara.ShowDialog();
                    changeTaskId_3 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string conditions = "Number=" + changeTaskId_3 + "|State=Closed Complete";
                flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found second change task: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_165_ImpersonateUser_ChangeManager()
        {
            try
            {
                Step_139_ImpersonateUser_ChangeManager();
                if (!flag)
                    error = "Error when recall step: Step_139_ImpersonateUser_ChangeManager()";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_166_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_167_SearchAndOpenChange()
        {
            try
            {
                Step_021_Search_And_Open_Change();
                if (!flag)
                    error = "Error when recall step: Step_021_Search_And_Open_Change";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //Huong C update-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_168_01_PopulateActualDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    button = chg.Button_ActualEndDate_Calendar();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            button = chg.Button_Calendar_Select();
                            flag = button.Existed;
                            if (flag)
                            {
                                button.Click();
                                datetime = chg.Datetime_Actual_End_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    string end = datetime.Text.Trim();
                                    end = Convert.ToDateTime(end).AddDays(6).ToString("yyyy-MM-dd HH:mm:ss");
                                    Console.WriteLine("Actual end date:" + end);
                                    datetime.SetText(end, true);
                                    string start = Convert.ToDateTime(end).AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
                                    datetime = chg.Datetime_Actual_Start_Date();
                                    flag = datetime.Existed;
                                    if (flag)
                                    {
                                        flag = datetime.SetText(start, true);
                                        if (!flag)
                                            error = "Error when input actual start date.";
                                    }
                                    else error = "Cannot get datetime actual start date.";
                                }
                                else error = "Cannot get datetime actual end date.";
                                
                            }
                            else error = "Cannot get button select.";
                        }
                        else error = "Error when click on button actual end date calendar.";
                    }
                    else error = "Cannot get button actual end date calendar.";
                    ////for verification of Compliance : Change window exceeded checkbox
                    //string temp = Base.GData("Actual_End_Date");
                    //string[] totalday = temp.Split(':');
                    //string startDate = null;
                    //string endDate = null;

                    //// - Actual start date = today + 1
                    //startDate = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                    //endDate = chg.Datetime_Planned_End_Date().Text;
                    //switch (totalday[0])
                    //{
                    //    case "Greater":
                    //        // Actual End Date > Planned End Date
                    //        // - Actual end date = Planned end date + 1
                    //        endDate = Convert.ToDateTime(endDate).AddDays(Int32.Parse(totalday[1])).ToString("yyyy-MM-dd HH:mm:ss");
                    //        break;
                    //    case "Less":
                    //        // Actual End Date < Planned End Date
                    //        // - Actual end date = Planned end date - 1
                    //        endDate = Convert.ToDateTime(endDate).AddDays(-Int32.Parse(totalday[1])).ToString("yyyy-MM-dd HH:mm:ss");
                    //        break;
                    //}
                    //datetime = chg.Datetime_Actual_Start_Date();
                    //flag = datetime.Existed;
                    //if (flag)
                    //{
                    //    flag = datetime.SetText(startDate, true);
                    //    if (flag)
                    //    {
                    //        Thread.Sleep(2000);
                    //        datetime = chg.Datetime_Actual_End_Date();
                    //        flag = datetime.Existed;
                    //        if (flag)
                    //        {
                    //            flag = datetime.SetText(endDate, true);
                    //            if (!flag) error = "Cannot populate planned end date.";
                    //        }
                    //        else error = "Cannot get datetime planned end date.";
                    //    }
                    //    else error = "Cannot populate planned start date.";
                    //}
                    //else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_168_02_PopulateChangeState_Close()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeCloseState");
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate state value."; }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_168_03_ValidateCloseCodeANDInputCloseNotes()
        {
            try
            {
                flag = chg.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = chg.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("ChangeCloseCode");
                        flag = combobox.VerifyCurrentValue(temp);
                        if (flag)
                        {
                            textarea = chg.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("ChangeCloseNote") + ": " + Base.GData("ChangeCloseState");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                                else
                                {
                                    flag = chg.Save();
                                    if (flag) { chg.WaitLoading(); }
                                    else error = "Error when save change.";
                                }
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Invalid close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot click on Closure Information tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //HuongC-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_168_04_SelectCompliance_tab()
        {
            try
            {
                flag = chg.Select_Tab("Compliance");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //HuongC -----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_168_05_Verify_Compliance_TextboxValue()
        {
            try
            {
                //-- Input information
                if (change_rejected == true && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;

                    addPara = new Auto.AddParameter("Please input Number of Times rejected (1 - 10)");
                    addPara.ShowDialog();
                    rejected_before = Int32.Parse(addPara.value);
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //verify 'Rejected change' field
                textbox = chg.Textbox_Rejected_Change();
                if (textbox.Text == (rejected_before + rejected_runtime).ToString() || (textbox.Text == string.Empty && rejected_before + rejected_runtime == 0))
                    Console.WriteLine("***PASSED: Value of control ['" + textbox.MyName + "'] is [" + textbox.Text + "].");
                else
                {
                    flag = false;
                    flagExit = false;
                    Console.WriteLine("***FAILED: Value of control ['" + textbox.MyName + "'] is [" + textbox.Text + "]. Expectation is [" + (rejected_before + rejected_runtime).ToString() + "]");
                }

                //verify 'Rescheduled change' field
                textbox = chg.Textbox_Rescheduled_Change();
                if (textbox.Text == "")
                    Console.WriteLine("***PASSED: Value of control ['" + textbox.MyName + "'] is [" + textbox.Text + "].");
                else
                {
                    flag = false;
                    flagExit = false;
                    Console.WriteLine("***FAILED: Value of control ['" + textbox.MyName + "'] is [" + textbox.Text + "].");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_169_Change_Verify_Activity_169()
        {
            try
            {
                string temp = Base.GData("Activity_169");
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    flag = chg.Verify_Activity(temp);
                    if (!flag) { error = "Invalid activity note 169. Expected:(" + temp + ")"; flagExit = false; }
                }
                else error = "Cannot select tab (Notes).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_170_ImpersonateUser_SupportUser()
        {
            try
            {
                string temp = Base.GData("SupportUser");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user support.";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_171_SystemSetting()
        {
            try
            {
                Step_004_SystemSetting();
                if (!flag)
                    error = "Error when recall step: Step_004_SystemSetting";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_172_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_172_2_Filter_EmailSentToAssigned_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;has been assigned|and|" + "Recipients;contains;" + Base.GData("AssignedEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_172_3_Validate_EmailSentToAssigned_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) error = "Not found email sent to assigned (has been assigned).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_173_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_173_2_Filter_EmailSentToAssigned_Approved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;has been approved|and|" + "Recipients;contains;" + Base.GData("AssignedEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_173_3_Validate_EmailSentToAssigned_Approved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) error = "Not found email sent to assigned (has been approved).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_174_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_174_2_Filter_EmailSentToAssigned_Notification()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;notification|and|" + "Recipients;contains;" + Base.GData("AssignedEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_174_3_Validate_EmailSentToAssigned_Notification()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) error = "Not found email sent to assigned (notification).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_2_Filter_EmailSentToTechnical_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + Base.GData("TechnicalApproverEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_3_ValidateAndOpen_EmailSentToTechnical_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to technical (Approval request).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_4_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change Request " + changeId + " Approval request";
                flag = email.VerifySubject(expected);
                if (!flag) error = "Invalid subject value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_5_Verify_Email_Recipient_Technical()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag) error = "Invalid recipient value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_6_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag)
                {
                    error = "Error when click on priview html body.";
                }
                else Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_7_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string expected = Base.GData("AssignmentGroup") + "|Click here to approve " + changeId + "|Click here to reject " + changeId;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_175_8_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_2_Filter_EmailSentToAdhoc_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    string debug = Base.GData("Debug").ToLower();
                    if (debug == "yes" && changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string tech, bus;
                    switch (temp.ToLower())
                    {
                        case "yesno":
                            tech = Base.GData("AdhocApproverEmail_Tech");
                            temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + tech;
                            break;
                        case "noyes":
                            bus = Base.GData("AdhocApproverEmail_Bus");
                            temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + bus;
                            break;
                        case "yesyes":
                            tech = Base.GData("AdhocApproverEmail_Tech");
                            bus = Base.GData("AdhocApproverEmail_Bus");
                            temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + tech + "|or|" + "Recipients;contains;" + bus;
                            break;
                    }
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical or Business Phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_3_ValidateAndOpen_EmailSentToAdhoc_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    string debug = Base.GData("Debug").ToLower();
                    if (debug == "yes" && changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string tech, bus;
                    string conditions = "Subject=@@" + changeId;
                    switch (temp.ToLower())
                    {
                        case "noyes":
                            bus = Base.GData("AdhocApproverEmail_Bus");
                            conditions = conditions + "|" + "Recipients=@@" + bus;
                            break;
                        case "default":
                            tech = Base.GData("AdhocApproverEmail_Tech");
                            conditions = conditions + "|" + "Recipients=@@" + tech;
                            break;
                    }

                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to adhoc (Approval request).";
                    else email.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical or Business Phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_4_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string expected = "Change Request " + changeId + " Approval request";
                    flag = email.VerifySubject(expected);
                    if (!flag) error = "Invalid subject value.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical or Business Phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_5_Verify_Email_Recipient_Technical()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    string recipient = Base.GData("AdhocApproverEmail");
                    flag = email.VerifyRecipient(recipient);
                    if (!flag) error = "Invalid recipient value.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Adhoc Approver for Technical or Business Phase ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_6_ClickOn_HtmlReviewBody()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    flag = email.ClickOnPreviewHtmlBody();
                    if (!flag) error = "Error when click on priview html body.";
                    else Thread.Sleep(3000);
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_7_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    string expected = "Approval Group:";
                    flag = email.VerifyEmailBody(expected);
                    if (!flag) error = "Invalid value in body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_176_8_Close_EmailBody()
        {
            try
            {
                string temp = Base.GData("HaveAdhocApprovalTech") + Base.GData("HaveAdhocApprovalBus");
                if (temp.ToLower() == "yesno" || temp.ToLower() == "noyes" || temp.ToLower() == "yesyes")
                {
                    flag = email.ClickOnCloseButton();
                    if (!flag) error = "Error when close bemail body.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_2_Filter_EmailSentToChangeManager_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + Base.GData("ChangeManagerEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_3_ValidateAndOpen_EmailSentToChangeManager_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent to change manager (Approval request).";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_4_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change Request " + changeId + " Approval request";
                flag = email.VerifySubject(expected);
                if (!flag) error = "Invalid subject value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_5_Verify_Email_Recipient_Technical()
        {
            try
            {
                string recipient = Base.GData("ChangeManagerEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag) error = "Invalid recipient value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_6_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_7_Verify_Email_Body()
        {
            try
            {
                string expected = Base.GData("ManagerGroup");
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_177_8_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_1_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_2_Filter_EmailSentToBusinessApproval_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + Base.GData("BusinessApproverEmail");
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_3_ValidateAndOpen_EmailSentToBusinessApprover_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string conditions = "Subject=@@" + changeId;
                    flag = emailList.Open(conditions, "Created");
                    if (!flag) error = "Not found email sent to business (Approval request).";
                    else email.WaitLoading();
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_4_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && changeId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string expected = "Change Request " + changeId + " Approval request";
                    flag = email.VerifySubject(expected);
                    if (!flag) error = "Invalid subject value.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_5_Verify_Email_Recipient_Technical()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    string recipient = Base.GData("BusinessApproverEmail");
                    flag = email.VerifyRecipient(recipient);
                    if (!flag) error = "Invalid recipient value.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_6_ClickOn_HtmlReviewBody()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    flag = email.ClickOnPreviewHtmlBody();
                    if (!flag) error = "Error when click on priview html body.";
                    else Thread.Sleep(3000);
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_7_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    string expected = Base.GData("BusinessGroup");
                    flag = email.VerifyEmailBody(expected);
                    if (!flag) error = "Invalid value in body.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_178_8_Close_EmailBody()
        {
            try
            {
                string temp = Base.GData("ChangeType");
                if (temp.ToLower() == "normal change")
                {
                    flag = email.ClickOnCloseButton();
                    if (!flag) error = "Error when close bemail body.";
                }
                else
                {
                    Console.WriteLine("*** This case has not Business approval ***");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_179_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
    }
}
