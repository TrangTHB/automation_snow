﻿using Auto;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
namespace General
{
    [TestFixture]
    public class General_Portal_Catalog
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);
            System.Console.WriteLine("**** END CASE: " + Base.GData("Test_Case_Name"));
            System.Console.WriteLine("Finished - Request Id: " + REQiD);
            System.Console.WriteLine("Finished - Requested Item Id: " + RITMiD);
            System.Console.WriteLine("Finished - Catalog Task: " + CTaskList);

            System.Console.WriteLine("--------------------------------------------------");
            if (ldic.Count > 0) 
            {
                for (int i = 0; i < ldic.Count; i++) 
                {
                    System.Console.WriteLine("Finished - Close task required fields of Task " + (i + 1));
                    if (ldic[i] != null && ldic[i].Count > 0) 
                    {
                        foreach (var control in ldic[i])
                        {
                            string label = control.Key;
                            System.Console.WriteLine("---" + label);
                        }
                    }
                }                            
            }
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snobutton button;
        SNow.snotextarea textarea;
        SNow.snoelement ele;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Portal portal = null;
        SNow.SPortal sportal = null;
        SNow.Itil itil = null;
        SNow.ItilList list = null;
        SNow.Email email = null;
        SNow.EmailList emailList = null;
        
        //------------------------------------------------------------------
        string REQiD, RITMiD, RITMiD2, CTaskList;
        string testscript_name;
        bool isServicePortal;

        List<Dictionary<string, SNow.snoelement>> ldic = new List<Dictionary<string, SNow.snoelement>>();

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                portal = new SNow.Portal(Base, "Portal");
                sportal = new SNow.SPortal(Base, "Service portal");
                list = new SNow.ItilList(Base, "List");
                itil = new SNow.Itil(Base, "Itil");
                emailList = new SNow.EmailList(Base, "Email list");
                email = new SNow.Email(Base, "Email");
                //------------------------------------------------------------------
                REQiD = string.Empty;
                RITMiD = string.Empty;
                RITMiD2 = string.Empty;
                CTaskList = string.Empty;
                testscript_name = Base.GData("Test_Case_Name");

                if (Base.GData("ServicePortal").Trim().ToLower() == "yes")
                    isServicePortal = true;
                else
                    isServicePortal = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;

                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else 
                {
                    string landing_page = Base.GData("Customer_LandingPage");
                    
                    if (landing_page.Contains(";")) 
                    {
                        string[] arr = landing_page.Split(';');
                        landing_page = arr[0];
                    }
                        
                    if (landing_page.ToLower() != null && landing_page.ToLower() == "itil")
                    {
                        flag = home.SwitchToPortal();
                        if (flag) 
                        {
                            if (!isServicePortal)
                                portal.WaitLoading();
                            else
                                sportal.WaitLoading();
                        }
                    }
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_Verify_Login_Customer()
        {
            try
            {
                string temp = Base.GData("Customer");
                if (!isServicePortal)
                    ele = portal.UserFullName();
                else
                    ele = sportal.UserFullName();

                flag = ele.Existed;
                if (flag)
                {
                    if (!ele.MyText.Trim().ToLower().Equals(temp.Trim().ToLower()))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid the full name of login user.";
                    }
                }
                else error = "Cannot get control user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Select_Catalog_Link()
        {
            try
            {
                if(!isServicePortal)
                    ele = portal.GMenu("Catalog");
                else
                    ele = sportal.GHeaderMenu("Catalog");

                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag) error = "Error when click on menu issue";
                    else
                    {
                        if (!isServicePortal)
                            portal.WaitLoading();
                        else
                            sportal.WaitLoading();
                    }
                }
                else error = "Cannot get menu issue.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Select_Catalog_Item()
        {
            try
            {
                string catalog = Base.GData("Catalog");
                string category = Base.GData("Category");
                string item = Base.GData("Item");
                string path = string.Empty;
                if (category != null && category != string.Empty && category.ToLower() != "no")
                    path = catalog + ";" + category;
                else
                    path = catalog;

                if (!isServicePortal)
                    flag = portal.SearchAndOpenCatalogItem(item);
                else 
                {
                    flag = sportal.Search_Open_CatalogItem(item);
                }
                
                if (!flag)
                    error = "Cannot select catalog item.";
                else 
                {
                    if (!isServicePortal)
                    {
                        portal.WaitLoading();
                    }
                    else 
                    {
                        sportal.WaitLoading();
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_Verify_UserName_Field()
        {
            try
            {
                temp = Base.GData("Customer");
                if (!isServicePortal)
                {
                    lookup = portal.Lookup_UserName();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user name value.";
                    }
                    else error = "Cannot get lookup user name.";
                } 
                else 
                {
                    lookup = sportal.Lookup_RequestedFor();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.SP_VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user name value.";
                    }
                    else error = "Cannot get lookup user name.";
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_02_Verify_FirstName_Field()
        //{
        //    try
        //    {
        //        if (!isServicePortal)
        //            textbox = portal.Textbox_FirstName();
        //        else 
        //        {
        //            textbox = sportal.Textbox_FirstName();
        //        }

        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            temp = Base.GData("Customer_FirstName");
        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag) error = "Invalid user first name value.";
        //        }
        //        else error = "Cannot get textbox first name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_03_Verify_LastName_Field()
        //{
        //    try
        //    {

        //        if (!isServicePortal)
        //            textbox = portal.Textbox_LastName();
        //        else 
        //        {
        //            textbox = sportal.Textbox_LastName();
        //        }

        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            temp = Base.GData("Customer_LastName");
        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag) error = "Invalid user last name value.";
        //        }
        //        else error = "Cannot get textbox last name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_04_Verify_Email_Field()
        {
            try
            {
                if (!isServicePortal)
                    textbox = portal.Textbox_EmailAddress();
                else 
                {
                    textbox = sportal.Textbox_EmailAddress();
                } 

                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Customer_Email");
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) error = "Invalid user email value.";
                }
                else error = "Cannot get textbox email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_05_Verify_UserID_Field()
        //{
        //    try
        //    {
        //        if (!isServicePortal)
        //            textbox = portal.Textbox_UserID();
        //        else 
        //        {
        //            textbox = sportal.Textbox_UserID();
        //        }

        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            temp = Base.GData("Customer_UserID");
        //            flag = textbox.VerifyCurrentValue(temp, true);
        //            if (!flag) error = "Invalid user ID value.";
        //        }
        //        else error = "Cannot get textbox User ID.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_06_Verify_Phone_Field()
        {
            try
            {
                if (!isServicePortal)
                    textbox = portal.Textbox_UserTelephoneNumber();
                else 
                {
                    textbox = sportal.Textbox_TelephoneNumber();
                }

                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Customer_Phone");
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) error = "Invalid user phone value.";
                }
                else error = "Cannot get textbox Phone.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_07_Verify_Location_Field()
        {
            try
            {
                temp = Base.GData("Customer_Location");
                if (!isServicePortal)
                {
                    lookup = portal.Lookup_UserLocation();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user location value.";
                    }
                    else error = "Cannot get lookup location.";
                }
                else
                {
                    lookup = sportal.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.SP_VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user location value.";
                    }
                    else error = "Cannot get lookup location.";
                }
         
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_08_Verify_Price()
        {
            try
            {
                temp = Base.GData("Price");
                if (temp.ToLower() != "no")
                {
                    if (!isServicePortal)
                        ele = portal.Price();
                    else
                    {
                        ele = sportal.Price();
                    }
                        
                    flag = ele.Existed;
                    if (flag)
                    {
                        string str = ele.MyText;
                        str = str.Replace("Price:", "");
                        str = str.Replace("\r\n", "").Trim();
                        str = str.Replace("    ", " ");
                        if (temp.Trim().ToLower() != str.Trim().ToLower())
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid price.";
                        }
                    }
                    else error = "Cannot get label price.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_09_Verify_Recurring_Price()
        {
            try
            {
                temp = Base.GData("Recurring_Price");
                if (temp.ToLower() != "no")
                {
                    if (!isServicePortal)
                        ele = portal.Recurring_Price();
                    else { } //thuho

                    flag = ele.Existed;
                    if (flag)
                    {
                        if (temp.Trim().ToLower() != ele.MyText.Trim().ToLower())
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid recurring price.";
                        }
                    }
                    else error = "Cannot get label recurring price.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_10_Verify_Recurring_Frequency()
        {
            try
            {
                temp = Base.GData("Recurring_Frequency");
                if (temp.ToLower() != "no")
                {
                    if (!isServicePortal)
                        ele = portal.Recurring_Frequency();
                    else { } //thuho

                    flag = ele.Existed;
                    if (flag)
                    {
                        if (temp.Trim().ToLower() != ele.MyText.Trim().ToLower())
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid recurring frequency.";
                        }
                    }
                    else error = "Cannot get label recurring frequency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_11_Select_Quantity()
        {
            try
            {
                temp = Base.GData("Quantity");
                if (temp.ToLower() != "no")
                {
                    if (!isServicePortal)
                    {
                        combobox = portal.Combobox_Quantity();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                Thread.Sleep(10000);
                            }                     
                            else
                                error = "Error when select quantity.";
                        }
                        else error = "Cannot get combobox quantity.";
                    }                    
                    else { }//thuho

                    
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_12_Verify_SubTotal()
        {
            try
            {
                string quantity = Base.GData("Quantity");
                string price = Base.GData("Price");
                if (quantity.ToLower() != "no")
                {
                    if (!isServicePortal)
                        ele = portal.Subtotal();
                    else { } //thuho

                    flag = ele.Existed;
                    if (flag)
                    {
                        string dv = price.Substring(0, 1);
                        double t = double.Parse(price.Replace(dv, "")) * double.Parse(quantity);
                        string total = dv + t.ToString("0.00");
                        Console.WriteLine("Expect total:" + total);
                        if (total != ele.MyText.Trim())
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid subtotal. Runtime:" + ele.MyText;
                        }
                    }
                    else error = "Cannot get combobox quantity.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_13_Verify_Recurring_SubTotal()
        {
            try
            {
                string quantity = Base.GData("Quantity");
                string recurring = Base.GData("Recurring_Price");
                if (quantity.ToLower() != "no")
                {
                    if (!isServicePortal)
                        ele = portal.Recurring_Subtotal();
                    else { } //thuho

                    flag = ele.Existed;
                    if (flag)
                    {
                        recurring = recurring.Replace("+", "").Trim();
                        string dv = recurring.Substring(0, 1);
                        double t = double.Parse(recurring.Replace(dv, "")) * double.Parse(quantity);
                        string total = "+ " + dv + t.ToString("0.00");
                        Console.WriteLine("Expect total:" + total);
                        if (total != ele.MyText.Trim())
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid recurring subtotal. Runtime:" + ele.MyText;
                        }
                    }
                    else error = "Cannot get combobox quantity.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_14_Change_Customer2_IfNeed()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    if (!isServicePortal)
                    {
                        lookup = portal.Lookup_UserName();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.Select(temp);
                            if (!flag)
                                error = "Error when select value.";
                        }
                        else error = "Cannot get lookup user name.";
                    }
                    else 
                    {
                        lookup = sportal.Lookup_RequestedFor();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.SP_Select(temp);
                            if (!flag)
                                error = "Error when select value.";
                        }
                        else error = "Cannot get lookup user name.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_15_Verify_Cus2_FirstName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        if (temp.ToLower() != "no" && temp != "" && temp != null)
        //        {
        //            if (!isServicePortal)
        //                textbox = portal.Textbox_FirstName();
        //            else 
        //            {
        //                textbox = sportal.Textbox_FirstName();
        //            }

        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Customer2_FirstName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag) error = "Invalid user first name value.";
        //            }
        //            else error = "Cannot get textbox first name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_16_Verify_Cus2_LastName_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        if (temp.ToLower() != "no" && temp != "" && temp != null)
        //        {
        //            if (!isServicePortal)
        //                textbox = portal.Textbox_LastName();
        //            else 
        //            {
        //                textbox = sportal.Textbox_LastName();
        //            }
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Customer2_LastName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag) error = "Invalid user last name value.";
        //            }
        //            else error = "Cannot get textbox last name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_17_Verify_Cus2_Email_Field()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    if (!isServicePortal)
                        textbox = portal.Textbox_EmailAddress();
                    else 
                    {
                        textbox = sportal.Textbox_EmailAddress();
                    }
                    
                    flag = textbox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Customer2_Email");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user email value.";
                    }
                    else error = "Cannot get textbox email.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_007_18_Verify_Cus2_UserID_Field()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Customer2");
        //        if (temp.ToLower() != "no" && temp != "" && temp != null)
        //        {
        //            if (!isServicePortal)
        //                textbox = portal.Textbox_UserID();
        //            else 
        //            {
        //                textbox = sportal.Textbox_UserID();
        //            }

        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                temp = Base.GData("Customer2_UserID");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag) error = "Invalid user ID value.";
        //            }
        //            else error = "Cannot get textbox User ID.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_19_Verify_Cus2_Phone_Field()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    if (!isServicePortal)
                        textbox = portal.Textbox_UserTelephoneNumber();
                    else 
                    {
                        textbox = sportal.Textbox_TelephoneNumber();
                    }

                    flag = textbox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Customer2_Phone");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) error = "Invalid user phone value.";
                    }
                    else error = "Cannot get textbox Phone.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_20_Verify_Cus2_Location_Field()
        {
            try
            {
                string temp = Base.GData("Customer2");
                if (temp.ToLower() != "no" && temp != "" && temp != null)
                {
                    temp = Base.GData("Customer2_Location");
                    if (!isServicePortal)
                    {
                        lookup = portal.Lookup_UserLocation();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(temp, true);
                            if (!flag) error = "Invalid user location value.";
                        }
                        else error = "Cannot get lookup location.";
                    }                   
                    else 
                    {
                        lookup = sportal.Lookup_Location();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.SP_VerifyCurrentValue(temp, true);
                            if (!flag) error = "Invalid user location value.";
                        }
                        else error = "Cannot get lookup location.";
                    } 
                    
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Input_MandatoryFields()
        {
            try
            {
                if (!isServicePortal) 
                {
                    itil.Input_Mandatory_Fields("Auto value");
                }
                else
                    flag = sportal.Input_Mandatory_Fields("auto");
                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_OrderItem()
        {
            try
            {
                if (!isServicePortal)
                    button = portal.Button_Order();
                else 
                {
                    button = sportal.Button_Submit();
                }
                    
                if (button.Existed)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        if (!isServicePortal)
                            portal.WaitLoading();
                        else
                            sportal.WaitLoading();
                    }
                    else { error = "Cannot click button Order Now"; }
                }
                else
                {
                    flag = false;
                    error = "Order Now button does NOT exist";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_Get_Request_Number()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.TKP_Request_Number();            
                }
                else 
                {
                    ele = sportal.TKP_Request_Number();
                }

                flag = ele.Existed;
                if (flag)
                {
                    REQiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_Get_Request_Item_Number()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.TKP_Request_Item_Number();
                }
                else 
                {
                    ele = sportal.TKP_Request_Item_Number();
                }

                flag = ele.Existed;
                if (flag)
                {
                    RITMiD = ele.MyText.Trim();
                }
                else error = "Cannot get elemnet request item number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_03_Validate_Thank_You_Message()
        {
            try
            {
                if (!isServicePortal)
                { 
                    ele = portal.ThankYouMessage();
                    flag = ele.Existed;
                    if (flag)
                    {
                        if (ele.MyText.Trim() != "Thank you, your request has been submitted")
                        {
                            flag = false;
                            flagExit = false;
                        }
                    }
                }
                else
                {
                    //ele = sportal.ThankYouMessage();
                    //flag = ele.Existed;
                    //if (flag)
                    //{
                    //    if (!ele.MyText.Trim().Contains("REQ"))
                    //    {
                    //        flag = false;
                    //        flagExit = false;
                    //    }
                    //}
                }
                

                if (!flag) 
                {
                    flagExit = false;
                    error = "Invalid message.";
                }
                    
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_04_Validate_Thank_You_Table_Columns()
        {
            try
            {
                if (!isServicePortal)
                {
                    temp = Base.GData("ThankYou_Item_Columns");
                    flag = portal.TKP_ValidateItemColumns(temp);
                }
                else { Console.WriteLine("--- No need to verify this step. ---"); }
                
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid column.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Validate_Item_Stage()
        {
            try
            {
                temp = Base.GData("Item_Stage_1");
                if (!isServicePortal)
                {
                    flag = portal.TKP_Validate_ItemStage(temp);
                }
                else 
                {
                    flag = sportal.TKP_Validate_ItemStage(temp);
                }
                
                if (!flag)
                {
                    error = "Invalid stage or stage is not in blue color";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Back_To_Page_BrowseCatalog()
        {
            try
            {
                if (!isServicePortal)
                {
                    button = portal.Button_Back();
                    if (button.Existed)
                    {
                        flag = button.Click(true);
                        if (flag)
                        {
                            portal.WaitLoading();
                        }
                        else { error = "Cannot click button Back to Catalog"; }
                    }
                    else
                    {
                        flag = false;
                        error = "Back to Catalog button does NOT exist";
                    }
                }
                else { Console.WriteLine("--- No need to run this step. ---"); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Open_MyRequests_Or_CheckStatus()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.GDashBoardMenu("My Requests");
                    if (ele.Existed)
                    {
                        ele.Click();
                        portal.WaitLoading(true);
                        Thread.Sleep(4000);
                    }
                    else
                    {
                        flag = false;
                        error = "Not found My Requests on dashboard menu";
                    }
                }
                else 
                {
                    ele = sportal.GHeaderMenu("Check Status");
                    if (ele.Existed)
                    {
                        ele.Click();
                        Thread.Sleep(5000);
                        sportal.WaitLoading();
                        Thread.Sleep(1000);
                        ele = sportal.GLeftMenu("My Requests");
                        flag = ele.Existed;
                        if (flag) 
                        {
                            ele.Click();
                            sportal.WaitLoading();
                        }
                        else error = "Not found My Requests on left menu";
                    }
                    else
                    {
                        flag = false;
                        error = "Not found My Requests or Check Status on dashboard menu";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_ClickOn_MyActiveRequests()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.GLeftMenu("My Active Requests");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (flag) portal.WaitLoading(true);
                    }
                    else error = "Not found menu.";
                }
                else 
                {
                    ele = sportal.GLinkByText("My Active Requests");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (flag) sportal.WaitLoading();
                    }
                    else error = "Not found menu.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Verify_Stage_Completed_NotFound()
        {
            try
            {
                string condition = "Stage=Completed";
                if (!isServicePortal)
                {
                    flag = portal.SearchAndVerifyRow("Completed", condition, true);
                }
                else 
                {
                    flag = sportal.SearchAndVerifyRow("Completed", condition, true);
                }
                
                if (flag)
                {
                    System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
                }
                else
                {
                    error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Verify_Stage_RequestCancelled_NotFound()
        {
            try
            {
                string condition = "Stage=Request Cancelled";
                if (!isServicePortal)
                {
                    flag = portal.SearchAndVerifyRow("Request Cancelled", condition, true);
                }
                else
                {
                    flag = sportal.SearchAndVerifyRow("Request Cancelled", condition, true);
                }

                if (flag)
                {
                    System.Console.WriteLine("***PASSED: Not found any item with condition (" + condition + ").");
                }
                else
                {
                    error = "***ERROR: Found item has condition: (" + condition + "). Expected: [Not Found]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_To_040_Approve_OR_Reject_Item()
        {
            try
            {
                bool continueStep = true;
                error = "";
                string approval = Base.GData("Approval_Required").ToLower().Trim();
                if (approval.Contains("yes") || approval.Contains("x"))
                {
                    string[] arrApprovers = Base.GData("Approvers").Trim().Split(';');
                    string[] arrLanding_page = Base.GData("Approvers_LandingPage").Trim().Split(';');
                    string[] arrActions = Base.GData("Approvers_Action").Trim().Split(';');
                    /* Start here */
                    temp = Base.GData("Debug").ToLower();

                    int numTask = 0;
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input approver order (start with 1).");
                        addPara.ShowDialog();
                        numTask = Int32.Parse(addPara.value);
                        addPara.Close();
                        addPara = null;
                    }

                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }

                    int total = arrApprovers.Length;
                    Console.WriteLine("**************Total approval required: " + total);
                    /* For each Approver do the following action */
                    if (numTask != 0)
                        numTask = numTask - 1;
                    for (int i = numTask; i < total; i++)
                    {

                        #region Relogin

                        temp = Base.GData("Url");
                        Base.ClearCache();
                        Thread.Sleep(5000);
                        Base.Driver.Navigate().GoToUrl(temp);
                        Thread.Sleep(2000);
                        login.WaitLoading();
                        string user = Base.GData("User");
                        string pwd = Base.GData("Pwd");
                        flag = login.LoginToSystem(user, pwd);
                        if (flag)
                        {
                            home.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot login to system.";
                        }
                        #endregion

                        System.Console.WriteLine("//------------------- Action on approval " + (i + 1) + " -------------------//");

                        #region Impersonate User
                        if (arrLanding_page[i].ToLower() == "itil")
                        {
                            flag = home.ImpersonateUser(arrApprovers[i].Trim());
                            if (!flag)
                            {
                                error += "Cannot impersonate Approver " + arrApprovers[i].Trim();
                                continueStep = false;
                            }

                            if (continueStep)
                            {
                                flag = home.SwitchToPortal();
                                if (flag)
                                {
                                    if (!isServicePortal)
                                        portal.WaitLoading();
                                    else
                                        sportal.WaitLoading();
                                }
                                else { continueStep = false; error += "Cannot switch to portal"; }
                            }
                        }
                        else
                        {
                            flag = home.ImpersonateUser(arrApprovers[i].Trim(), false, null, true);
                            if (!flag)
                            {
                                error += "Cannot impersonate Approver " + arrApprovers[i].Trim();
                                continueStep = false;
                            }
                            else
                            {
                                if (!isServicePortal)
                                    portal.WaitLoading();
                                else
                                    sportal.WaitLoading();
                            }
                        }

                        #endregion

                        #region Click on My Approvals
                        if (continueStep)
                        {
                            if (!isServicePortal)
                            {
                                ele = portal.GDashBoardMenu("My Approvals");
                                if (ele.Existed)
                                {
                                    ele.Click();
                                    portal.WaitLoading(true);
                                    Thread.Sleep(4000);
                                }
                                else
                                {
                                    flag = false;
                                    continueStep = false;
                                    error = "Not found My Approvals on dashboard menu";
                                }
                            }
                            else
                            {
                                ele = sportal.GHeaderMenu("Approvals");
                                if (ele.Existed)
                                {
                                    ele.Click();
                                    Thread.Sleep(2000);
                                    //ele = sportal.GLinkByText("@@View all approvals");
                                    //flag = ele.Existed;
                                    //if (flag)
                                    //{
                                    //    ele.Click();
                                    //    sportal.WaitLoading();
                                    //}
                                    //else
                                    //{
                                    //    continueStep = false;
                                    //    error = "Not found link.";
                                    //}
                                }
                                else
                                {
                                    flag = false;
                                    continueStep = false;
                                    error = "Not found My Approvals on dashboard menu";
                                }
                            }

                        }
                        #endregion

                        #region Search and open Item
                        if (continueStep)
                        {
                            /* Input Requested Item ID */
                            temp = Base.GData("Debug").ToLower();
                            if (temp == "yes" && RITMiD == string.Empty)
                            {
                                AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                                addPara.ShowDialog();
                                RITMiD = addPara.value;
                                addPara.Close();
                                addPara = null;
                            }
                            //------------------------------------------------------------------------------------------
                            if (!isServicePortal)
                            {
                                flag = portal.SearchAndOpen(RITMiD, "Approval for=" + RITMiD, "State");
                            }
                            else 
                            {
                                flag = sportal.OpenMyApprovalItem(RITMiD);
                            }

                            if (flag)
                            {
                                if (!isServicePortal)
                                    portal.WaitLoading();
                                else
                                    sportal.WaitLoading();
                            }
                            else { continueStep = false; error += "Not found Requested Item in the approval list"; }
                        }
                        #endregion

                        #region Approve/Reject Item
                        if (continueStep)
                        {
                            if (arrActions[i].ToLower().Trim().Contains("reject"))
                            {
                                //Input the comment
                                if (!isServicePortal)
                                {
                                    textarea = portal.Textarea_Comments();
                                    if (textarea.Existed)
                                    {
                                        textarea.SetText("Rejected for " + RITMiD, false);
                                    }
                                    button = portal.Button_Reject();
                                    flag = button.Click(true);
                                    portal.WaitLoading();
                                    if (!flag)
                                    {
                                        continueStep = false;
                                        error += "Cannot click Reject";
                                    }
                                }
                                else 
                                {
                                    button = sportal.Button_Reject();
                                    flag = button.Click(true);
                                    Thread.Sleep(2000);
                                    if (!flag)
                                    {
                                        continueStep = false;
                                        error += "Cannot click Reject";
                                    }
                                    else 
                                    {
                                        textbox = sportal.ApprovalComment();
                                        flag = textbox.SetText("Rejected for " + RITMiD);
                                        if (!flag)
                                            error += "Error when add comment.";
                                        else
                                        {
                                            Thread.Sleep(2000);
                                            button = sportal.Button_OK();
                                            flag = button.Click();
                                            if(flag)
                                                sportal.WaitLoading();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (!isServicePortal)
                                {
                                    button = portal.Button_Approve();
                                    flag = button.Click(true);
                                    Thread.Sleep(5000);
                                    portal.WaitLoading();
                                    if (!flag)
                                    {
                                        continueStep = false;
                                        error += "Cannot click Approve";
                                    }
                                }
                                else 
                                {
                                    button = sportal.Button_Approve();
                                    flag = button.Click(true);
                                    Thread.Sleep(5000);
                                    sportal.WaitLoading();
                                    if (!flag)
                                    {
                                        continueStep = false;
                                        error += "Cannot click Approve";
                                    }
                                }
                            }

                            if (!flag) { continueStep = false; }
                        }
                        #endregion

                        if (!flag) { break; }

                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Close_AllCatalogTask()
        {
            try
            {
                string closetask = Base.GData("CloseTask_Required").ToLower().Trim();
                if (closetask.Contains("yes") || closetask.Contains("x"))
                {
                    string[] action = Base.GData("Approvers_Action").Split(';');
                    bool flagCheck = false;
                    
                    foreach (string act in action)
                    {
                        if (act.ToLower().Trim().Contains("reject"))
                        {
                            flagCheck = true;
                            break;
                        }
                    }
                    
                    if (flagCheck == false)
                    {
                        bool continueStep = true;
                        error = "";

                        string[] arrResolvers = null;
                        string resolvers = Base.GData("Fulfillment_Staff");
                        if (resolvers.Contains(";"))
                            arrResolvers = resolvers.Split(';');
                        else
                            arrResolvers = new string[] { resolvers };

                        string assignmentGroupTask = Base.GData("CTask_AssignmentGrp");
                        string[] arrAssignmentGroups = null;
                        if (assignmentGroupTask.Trim().ToLower() != "no")
                        {
                            if (assignmentGroupTask.Contains(";"))
                                arrAssignmentGroups = assignmentGroupTask.Split(';');
                            else
                                arrAssignmentGroups = new string[] { assignmentGroupTask };
                        }

                        string states = Base.GData("Ritem_State");
                        string[] arrState = null;
                        if (states.Contains(";"))
                            arrState = states.Split(';');
                        else
                            arrState = new string[] { states };


                        /* Start here */
                        temp = Base.GData("Debug").ToLower();
                        int numTask = 0;
                        if (temp == "yes" && RITMiD == string.Empty)
                        {
                            AddParameter addPara = new AddParameter("Please input task order (start with 1).");
                            addPara.ShowDialog();
                            numTask = Int32.Parse(addPara.value);
                            addPara.Close();
                            addPara = null;
                        }


                        if (temp == "yes" && RITMiD == string.Empty)
                        {
                            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                            addPara.ShowDialog();
                            RITMiD = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }

                        int total = arrResolvers.Length;

                        if (numTask != 0)
                            numTask = numTask - 1;
                        
                        Console.WriteLine("*** Total task need to close: " + total);

                        /* For each Catalog Task do the following action */
                        for (int i = numTask; i < total; i++)
                        {
                            #region Relogin

                            temp = Base.GData("Url");
                            Base.ClearCache();
                            Thread.Sleep(5000);
                            Base.Driver.Navigate().GoToUrl(temp);
                            login.WaitLoading();
                            string user = Base.GData("User");
                            string pwd = Base.GData("Pwd");
                            flag = login.LoginToSystem(user, pwd);
                            if (flag)
                            {
                                home.WaitLoading();
                            }
                            else
                            {
                                error = "Cannot login to system.";
                            }

                            #endregion

                            System.Console.WriteLine("//------------------- Action on Catalog task " + (i + 1) + " -------------------//");

                            #region Impersonate User

                            flag = home.ImpersonateUser(arrResolvers[i].Trim());
                            if (!flag)
                            {
                                error += "Cannot impersonate Fulfillment Staff " + arrResolvers[i].Trim();
                                continueStep = false;
                            }
                            else home.WaitLoading();
                            
                            #endregion

                            

                            #region Open My Groups Work page
                            if (continueStep)
                            {
                                Thread.Sleep(3000);
                                flag = home.LeftMenuItemSelect("Service Desk", "My Groups Work");
                                if (flag)
                                {
                                    list.WaitLoading();
                                }
                                else
                                {
                                    error += "Cannot open My Groups Work link.";
                                    continueStep = false;
                                }
                            }
                            #endregion

                            #region Search and Open the Assigned Catalog Item
                            if (continueStep)
                            {
                                /* If the Parent column is not in the table, then add it */
                                flag = list.Add_More_Columns("Parent|State");
                                if (!flag)
                                {
                                    error += "Cannot add 'Parent' column";
                                    continueStep = false;
                                }
                                if (flag)
                                {
                                    /* Set Filter */
                                    if (arrAssignmentGroups != null && arrAssignmentGroups.Length > 0)
                                    {
                                        string group = arrAssignmentGroups[i].Trim();
                                        System.Console.WriteLine("////// Group:" + group);
                                        temp = "Parent;is;" + RITMiD + "|and|Assignment group;contains;" + group + "|and|State;is;Open";
                                    }
                                    else
                                        temp = "Parent;is;" + RITMiD + "|and|State;is;Open";

                                    list.Filter(temp);
                                    list.WaitLoading();

                                    /* Select the Requested Item in the table */
                                    //no need to add condition because we have done the filter
                                    temp = "Parent=" + RITMiD;
                                    flag = list.Open(temp, "Number");
                                    if (flag)
                                    {
                                        itil.WaitLoading();
                                    }
                                    else
                                    {
                                        error += "Not found Requested Item in the My Groups Work list";
                                    }
                                    if (!flag) { continueStep = false; }
                                }
                            }
                            #endregion

                            #region Validate Catalog Task Page
                            if (continueStep)
                            {
                                /* Get Task ID */
                                textbox = itil.Textbox_Number();
                                flag = textbox.Existed;
                                if (flag)
                                {
                                    textbox.Click();
                                    CTaskList += (textbox.Text + ";");
                                }

                                /* Validate pre-populated Assignment Group */
                                if (arrAssignmentGroups != null && arrAssignmentGroups.Length > 0)
                                {
                                    lookup = itil.Lookup_AssignmentGroup();
                                    if (!lookup.Text.ToLower().Trim().Equals(arrAssignmentGroups[i].ToLower().Trim()))
                                    {
                                        error += "Incorrect default value for Assignment Group field populated. Actual is: [" + lookup.Text + "]";
                                    }
                                }

                                /* Validate Requested Item */
                                lookup = itil.Lookup_RequestItem();
                                if (lookup.Text != RITMiD)
                                {
                                    error += "Incorrect Requested Item ID. Actual is: [" + lookup.Text + "]";
                                }

                                /* Validate Requested For */
                                lookup = itil.Lookup_RequestedFor();
                                if (lookup.Text != Base.GData("Customer"))
                                {
                                    error += "Incorrect Requested For populated. Actual is: [" + lookup.Text + "";
                                }

                                /* Validate State */
                                combobox = itil.Combobox_State();
                                if (!combobox.VerifyCurrentValue("Open"))
                                {
                                    error += "Incorrect State populated. Actual is: [" + combobox.Text + "";
                                }

                                if (!error.Equals("")) { flag = false; flagExit = false; }
                            }
                            #endregion


                            #region Add Work Note and Additional Comment
                            if (continueStep)
                            {
                                /* Add a Work Note */
                                temp = Base.GData("CTask_WorkNote");
                                flag = itil.Add_Worknotes(temp);
                                
                                if (!flag)
                                {
                                    error += "Cannot add Work Note";
                                }

                                /* Add Additional Comment */
                                temp = Base.GData("CTask_AdditionalCom");
                                flag = itil.Add_AdditionComments(temp);
                                
                                if (!flag)
                                {
                                    error += "Cannot add Additional Comment";
                                }

                                if (!error.Equals("")) { flag = false; flagExit = false; }
                            }
                            #endregion

                            #region Save the Catalog Task
                            if (continueStep)
                            {
                                /* Save the catalog task */
                                flag = itil.Save();
                                if (!flag)
                                {
                                    error += "Cannot save Catalog Task.";
                                    continueStep = false;
                                }
                            }
                            #endregion

                            #region Check SLAs
                            if (continueStep)
                            {
                                string cTask_SLA = Base.GData("CTask_SLA");
                                string req_item_sla = Base.GData("RItem_SLA");

                                /* If need to check SLA, the CTask_SLA row in data sheet should not be 'no' */
                                if (cTask_SLA.ToLower() != "no")
                                {
                                    /* If need to check a pre-defined SLA */
                                    if (req_item_sla.ToLower() == "no")
                                    {
                                        flag = itil.VerifySLAs(temp);
                                        if (!flag)
                                        {
                                            flagExit = false;
                                            error += "Incorrect/No SLA is attached to the task";
                                        }

                                    }
                                    /* If we have not get the SLA name, verify SLA in Requested Item */
                                    else
                                    {
                                        ele = itil.View_RequestItem();
                                        if (ele.Existed)
                                        {
                                            ele.Click();
                                            itil.WaitLoading();
                                            button = itil.Button_OpenRecord();
                                            button.Click();
                                            itil.WaitLoading();
                                            flag = itil.VerifySLAs(req_item_sla);
                                            if (!flag)
                                            {
                                                flagExit = false;
                                                error += "Incorrect/No SLA is attached to the task";
                                            }

                                            button = itil.Button_Back();
                                            flag = button.Existed;
                                            if (flag)
                                            {
                                                button.Click();
                                                itil.WaitLoading();
                                            }
                                            else
                                            {
                                                continueStep = false;
                                                error += "Can not get Back button";
                                            }

                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Close the Catalog Task
                            if (continueStep)
                            {
                                combobox = itil.Combobox_State();
                                if (combobox.Existed)
                                {
                                    flag = combobox.SelectItem("Closed Complete");
                                    if (flag)
                                    {
                                        //---
                                        Dictionary<string, SNow.snoelement> dic = Base.GAllMandatoryControlWithLabelOnForm(true);
                                        if (dic.Count > 0) 
                                        {
                                            ldic.Add(dic);
                                            flag = itil.Input_Mandatory_Fields("Auto value");
                                        }
                                        //---
                                        if (flag) 
                                        {
                                            flag = itil.Save();
                                            if (!flag)
                                            {
                                                error += "Cannot save Catalog Task.";
                                            }
                                            else
                                            {
                                                itil.WaitLoading();
                                            }
                                        }                                    
                                    }
                                    else
                                    {
                                        error += "Cannot update Catalog Task's State";
                                    }
                                }
                                else
                                {
                                    error += "Cannot get State field";
                                }

                                if (!flag) { continueStep = false; }
                            }

                            #endregion

                            if (!flag) { break; }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Case_Auto_Approval_Reject_Return_LoginPage()
        {
            try
            {
                Thread.Sleep(3000);
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto"))
                {
                    temp = Base.GData("Url");
                    Base.ClearCache();
                    Base.Driver.Navigate().GoToUrl(temp);
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Case_Auto_Approval_Reject_ReLogin()
        {
            try
            {
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto"))
                {

                    string user = Base.GData("User");
                    string pwd = Base.GData("Pwd");
                    flag = login.LoginToSystem(user, pwd);
                    if (flag)
                    {
                        home.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot login to system.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_Case_Auto_Approval_Reject_ImpersonateUser_CSCSupport()
        {
            try
            {
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto"))
                {

                    temp = Base.GData("Support_User");
                    flag = home.ImpersonateUser(temp);
                    if (!flag)
                    { error = "Cannot impersonate CSC Support User"; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_Case_Auto_Approval_Reject_Search_RITM()
        {
            try
            {
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto"))
                {

                    temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && RITMiD == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                        addPara.ShowDialog();
                        RITMiD = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }

                    flag = itil.GlobalSearchItem(RITMiD, true);
                    if (!flag)
                        error = "Cannot open RITM.";
                    else
                        itil.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Case_Auto_Approval_Reject_Verify_Activity_Note()
        {
            try
            {
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto"))
                {
                    string customer = Base.GData("Customer");
                    string mess = customer + "|";
                    string expectedMessage = Base.GData("Auto_Approval_Reject_Activity_Log");
                    mess = mess + expectedMessage;

                    //if (temp.ToLower() == "auto approval")
                    //    mess = mess + "Opened by person is the approver - approval Auto-approved";
                    //else
                    //    mess = mess + "Approval rejected as no manager found for the requester and no default group defined";

                    flag = itil.Verify_Activity(mess);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Case_Auto_Approval_Open_Approval()
        {
            try
            {
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto approval"))
                {
                    string customer = Base.GData("Customer");
                    flag = itil.RelatedTableOpenRecord("Approvers", "State=Approved|Approver=" + customer, "State");
                    if (flag)
                        itil.WaitLoading();
                    else
                        error = "Cannot open approval.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Case_Auto_Approval_Verify_Activity_Note()
        {
            try
            {
                string temp = Base.GData("Approval_Required");
                if (temp.ToLower().Contains("auto approval"))
                {
                    string customer = Base.GData("Customer");
                    string mess = customer + "|Opened by person is the approver - approval Auto-approved";
                    flag = itil.Verify_Activity(mess);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_1_Return_LoginPage()
        {
            try
            {
                Thread.Sleep(3000);
                string closetask = Base.GData("CloseTask_Required").ToLower().Trim();
                if (closetask.Contains("no"))
                {
                    temp = Base.GData("Url");
                    Base.ClearCache();
                    Base.Driver.Navigate().GoToUrl(temp);
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_2_ReLogin()
        {
            try
            {
                string closetask = Base.GData("CloseTask_Required").ToLower().Trim();
                if (closetask.Contains("no"))
                {
                    string user = Base.GData("User");
                    string pwd = Base.GData("Pwd");
                    flag = login.LoginToSystem(user, pwd);
                    if (flag)
                    {
                        home.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot login to system.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_ImpersonateUser_Customer()
        {
            try
            {
                string customer2 = Base.GData("Customer2");
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    temp = customer2;
                }
                else temp = Base.GData("Customer");

                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag)
                { error = "Cannot impersonate Customer"; }
                else 
                {
                    string landing_page = Base.GData("Customer_LandingPage");

                    if (landing_page.Contains(";"))
                    {
                        string[] arr = landing_page.Split(';');
                        landing_page = arr[1];
                        if (landing_page.ToLower() != null && landing_page.ToLower() == "itil")
                        {
                            flag = home.SwitchToPortal();
                        }
                    }
                    else
                    {
                        if (landing_page.ToLower() != null && landing_page.ToLower() == "itil")
                        {
                            flag = home.SwitchToPortal();
                        }
                    }

                    if (!isServicePortal)
                        portal.WaitLoading();
                    else
                        sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_Verify_Login_Customer()
        {
            try
            {
                string customer2 = Base.GData("Customer2");
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    temp = customer2;
                }
                else temp = Base.GData("Customer");

                if (!isServicePortal)
                    ele = portal.UserFullName();
                else
                    ele = sportal.UserFullName();

                flag = ele.Existed;
                if (flag)
                {
                    if (!ele.MyText.Trim().ToLower().Equals(temp.Trim().ToLower()))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Invalid the full name of login user.";
                    }
                }
                else error = "Cannot get control user full name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_ClickOn_MyRequests()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.GDashBoardMenu("My Requests");
                    if (ele.Existed)
                    {
                        ele.Click();
                        portal.WaitLoading(true);
                        Thread.Sleep(4000);
                    }
                    else
                    {
                        flag = false;
                        error = "Not found My Requests on dashboard menu";
                    }
                }
                else 
                {
                    ele = sportal.GHeaderMenu("Check Status");
                    if (ele.Existed)
                    {
                        ele.Click();
                        Thread.Sleep(5000);
                        sportal.WaitLoading();
                        Thread.Sleep(1000);
                        ele = sportal.GLeftMenu("My Requests");
                        flag = ele.Existed;
                        if (flag)
                        {
                            ele.Click();
                            sportal.WaitLoading();
                        }
                        else error = "Not found My Requests on left menu";
                    }
                    else
                    {
                        flag = false;
                        error = "Not found My Requests or Check Status on dashboard menu";
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_Open_RITM_On_MyPriorRequests()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------
                string[] action = Base.GData("Approvers_Action").Split(';');
                bool flagCheck = false;
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        flagCheck = true;
                        break;
                    }
                }
                if (flagCheck == false)
                {
                    temp = "Stage=Completed";
                }
                else
                {
                    temp = "Stage=Request Cancelled";
                }
                //------------------------------------------------------------------------------
                if (!isServicePortal)
                {
                    ele = portal.GLeftMenu("My Prior Requests");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (flag) portal.WaitLoading(true);
                    }
                    else error = "Not found menu.";

                    if (flag)
                    {
                        portal.WaitLoading();

                        flag = portal.SearchAndOpen(RITMiD, temp, "Number");
                        if (flag)
                        {
                            portal.WaitLoading();
                        }
                        else { error = "Not found Requested Item"; }
                    }
                    else { error = "Not found My Prior Requests tab"; }
                }
                else 
                {
                    ele = sportal.GLinkByText("My Prior Requests");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (flag) 
                        {
                            sportal.WaitLoading();
                            flag = sportal.SearchAndOpen(RITMiD, "Number=" + RITMiD, "Number");
                            if (!flag)
                                error = "Cannot open RITM.";
                            else sportal.WaitLoading();
                        }
                    }
                    else error = "Not found menu.";
                }           
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_Validate_RITM()
        {
            try
            {
                //Input information
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------------------

                error = string.Empty;

                if (!isServicePortal)
                {
                    //Validate Number
                    textbox = portal.Textbox_Number();
                    if (!textbox.Existed || textbox.Text != RITMiD)
                    {
                        flag = false;
                        flagExit = false;
                        error = "The actual RITM # is :" + textbox.Text;
                    }

                    //Validate Approval
                    string[] action = null;
                    combobox = portal.Combobox_Approval();
                    if (combobox != null)
                    {
                        temp = null;
                        action = Base.GData("Approvers_Action").Split(';');
                        foreach (string act in action)
                        {
                            if (act.ToLower().Trim().Contains("reject"))
                            {
                                temp = "Rejected";
                                break;
                            }
                        }
                        if (temp == null)
                        {
                            temp = "Approved";
                        }
                        if (!combobox.Existed || !combobox.VerifyCurrentValue(temp))
                        {
                            flag = false;
                            flagExit = false;
                            error += "The actual approval state is :" + combobox.Text.Trim();
                        }
                    }
                    

                    //Validate Stage
                    combobox = portal.Combobox_Stage();
                    temp = null;
                    foreach (string act in action)
                    {
                        if (act.ToLower().Trim().Contains("reject"))
                        {
                            temp = "Request Cancelled";
                            break;
                        }
                    }
                    if (temp == null)
                    { temp = "Completed"; }
                    if (!combobox.VerifyCurrentValue(temp))
                    {
                        flag = false;
                        flagExit = false;
                        error += "The actual stage is :" + combobox.Text.Trim();
                    }
                }
                else 
                {
                    //Validate Number
                    textbox = sportal.Textbox_REQIT_Number();
                    if (!textbox.Existed || textbox.Text != RITMiD)
                    {
                        flag = false;
                        flagExit = false;
                        error = "The actual RITM # is :" + textbox.Text;
                    }

                    //Validate Approval
                    combobox = sportal.Combobox_Approval();
                    temp = null;
                    string[] action = Base.GData("Approvers_Action").Split(';');
                    foreach (string act in action)
                    {
                        if (act.ToLower().Trim().Contains("reject"))
                        {
                            temp = "Rejected";
                            break;
                        }
                    }
                    if (temp == null)
                    { temp = "Approved"; }
                    if (!combobox.Existed || !combobox.SP_VerifyCurrentValue(temp))
                    {
                        flag = false;
                        flagExit = false;
                        error += "The actual approval state is :" + combobox.SP_Text.Trim();
                    }

                    //Validate Stage
                    combobox = sportal.Combobox_Stage();
                    temp = null;
                    foreach (string act in action)
                    {
                        if (act.ToLower().Trim().Contains("reject"))
                        {
                            temp = "Request Cancelled";
                            break;
                        }
                    }
                    if (temp == null)
                    { temp = "Completed"; }
                    if (!combobox.SP_VerifyCurrentValue("@@" + temp))
                    {
                        flag = false;
                        flagExit = false;
                        error += "The actual stage is :" + combobox.SP_Text.Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_056_02_Case_Auto_Approval_Reject_Verify_Activity_Note()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Approval_Required");
        //        if (temp.ToLower().Contains("auto"))
        //        {
        //            string customer = Base.GData("Customer");
        //            string mess = customer + "|";
        //            if (temp.ToLower() == "auto approval")
        //                mess = mess + "Opened by person is the approver - approval Auto-approved";
        //            else
        //                mess = mess + "Approval rejected as no manager found for the requester and no default group defined";

        //            flag = sportal.Verify_Activity(mess);
        //            if (!flag)
        //            {
        //                flagExit = false;
        //                error = "Invalid activity";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_057_Return_LoginPage()
        {
            try
            {
                temp = Base.GData("Url");
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_ReLogin()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_059_ImpersonateUser_CSCSupport()
        {
            try
            {
                temp = Base.GData("Support_User");
                flag = home.ImpersonateUser(temp);
                if (!flag)
                { error = "Cannot impersonate CSC Support User"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_00_1_Filter_Submited_Email()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been submitted";
                flag = emailList.EmailFilter(temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_00_2_Verify_Quantity_Submited_Email()
        {
            try
            {
                string customer2 = Base.GData("Customer2");
                int rowCount = emailList.GetRowCount();
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    if (rowCount < 2)
                    {
                        flag = false;
                        error = "Found less than 2 submited email. Expected there is 2 submited email.";
                    }
                    else if (rowCount > 2)
                    {
                        flag = false;
                        error = "Found more than 2 submited email. Expected there is 2 submited email.";
                    }
                    else
                    {
                        string rqb_email = Base.GData("Customer_Email");
                        temp = "Recipients=@@" + rqb_email;
                        flag = emailList.VerifyRow(temp);
                        if (!flag)
                        {
                            error = "Not found submited email send to Requested by.";
                        }
                        else
                        {
                            string rqf_email = Base.GData("Customer2_Email");
                            temp = "Recipients=@@" + rqf_email;
                            flag = emailList.VerifyRow(temp);
                            if (!flag)
                                error = "Not found submited email send to Requested for.";
                        }
                    }
                }
                else
                {
                    if (rowCount < 1)
                    {
                        flag = false;
                        error = "Not found any submited email. Expected there is 1 submited email.";
                    }
                    else if (rowCount > 1)
                    {
                        flag = false;
                        error = "Found more than 1 submited email. Expected there is 1 submited email.";
                    }
                    else
                    {
                        string cus_email = Base.GData("Customer_Email");
                        temp = "Recipients=@@" + cus_email;
                        flag = emailList.VerifyRow(temp);
                        if (!flag)
                            error = "Not found submited email send to Requester.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_01_ClickOn_Email_Submitted()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                string customer2 = Base.GData("Customer2");
                string cus_email = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    cus_email = Base.GData("Customer2_Email");
                }
                else cus_email = Base.GData("Customer_Email");
                temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been submitted|and|Recipients;contains;" + cus_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    flag = emailList.Open("Recipients=@@" + cus_email, "Created");
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found email sent to Requested For (request submitted)";
                    }
                    else
                    { email.WaitLoading(); }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_02_Verify_Email_Submitted_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                string item = Base.GData("Item_Email");
                temp = "INFO: " + item + " (" + RITMiD + ") has been submitted.";
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: (" + temp + ")";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_03_Verify_Email_Submitted_Recipient()
        {
            try
            {
                string customer2 = Base.GData("Customer2");
                string recipient = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    recipient = Base.GData("Customer2_Email");
                }
                else recipient = Base.GData("Customer_Email");

                flag = email.VerifyRecipient(recipient);
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_04_Open_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_05_Verify_EmailHtmlBody_Note()
        {
            try
            {
                string item = Base.GData("Item_Email");
                temp = item + " has been successfully submitted and is awaiting approval.";
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_061_06_Verify_EmailHtmlBody_TicketId()
        //{
        //    try
        //    {
        //        temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && RITMiD == string.Empty)
        //        {
        //            AddParameter addPara = new AddParameter("Please input Requested Item Id.");
        //            addPara.ShowDialog();
        //            RITMiD = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }

        //        //--------------------------------------------------------------------------------
        //        temp = "Requested Item Number: " + RITMiD;
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Requested Item id.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_061_07_Verify_EmailHtmlBody_ItemName()
        //{
        //    try
        //    {
        //        temp = "Item: " + Base.GData("Item_Email");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid Item name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_061_08_Verify_EmailHtmlBody_Description()
        //{
        //    try
        //    {
        //        temp = Base.GData("Item_Description_Email");
        //        flag = email.VerifyEmailBody(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid description.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_09_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flag = false;
                    flagExit = false;
                    error = "Invalid description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_10_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Verify_Approval_Emails()
        {
            try
            {
                bool continueStep = true;
                error = "";
                string approverEmail = Base.GData("Approvers_Email");
                if (approverEmail.ToLower() != "no")
                {
                    string[] arrApprovers_Email;
                    if (approverEmail.Contains(";"))
                        arrApprovers_Email = approverEmail.Trim().Split(';');
                    else
                        arrApprovers_Email = new string[] { approverEmail };
                    /* Start here */
                    temp = Base.GData("Debug").ToLower();

                    for (int i = 0; i < arrApprovers_Email.Length; i++)
                    {

                        string approver_email = arrApprovers_Email[i].Trim();
                        #region Open Email log
                        flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                        if (flag)
                        {
                            emailList.WaitLoading();

                            if (!emailList.List_Title().MyText.Contains("Emails"))
                            {
                                flag = false;
                                error = "Cannot open email list.";
                            }
                        }
                        else
                        {
                            continueStep = false;
                            error = "Error when open email log.";
                        }
                        #endregion

                        #region Open Approval Email
                        if (continueStep)
                        {
                            if (temp == "yes" && RITMiD == string.Empty)
                            {
                                AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                                addPara.ShowDialog();
                                RITMiD = addPara.value;
                                addPara.Close();
                                addPara = null;
                            }
                            //------------------------
                            temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;Approval request|and|Recipients;contains;" + approver_email;
                            flag = emailList.EmailFilter(temp);
                            if (flag)
                            {
                                emailList.WaitLoading();
                                flag = emailList.Open("Recipients=@@" + approver_email, "Created");
                                if (!flag)
                                {
                                    continueStep = false;
                                    flagExit = false;
                                    error += "Not found email sent to Approver(request approval)";
                                }
                                else
                                { email.WaitLoading(); }
                            }
                        }
                        #endregion

                        #region Verify Email page
                        /* Verify Subject */
                        if (continueStep)
                        {
                            temp = "Requested Item " + RITMiD + " Approval request";
                            flag = email.VerifySubject(temp);
                            if (!flag)
                            {
                                continueStep = false;
                                flagExit = false;
                                error += "Invalid subject.";
                            }
                        }
                        /* Verify Recipient */
                        if (continueStep)
                        {
                            flag = email.VerifyRecipient(approver_email);
                            if (!flag)
                            {
                                continueStep = false;
                                flagExit = false;
                                error += "Invalid recipient.";
                            }
                        }
                        #endregion

                        #region Verify HTML Body
                        if (continueStep)
                        {
                            //Open Preview HTML Body link
                            flag = email.ClickOnPreviewHtmlBody();
                            if (!flag)
                            {
                                continueStep = false;
                                error += "Error when click on priview html body.";
                            }
                            if (continueStep)
                            {
                                //Verify Note
                                temp = "Please Approve or Reject the request by 1) selecting the appropriate link below AND 2) manually sending the email that is generated. You must send the auto-generated email to complete the processing.";
                                flag = email.VerifyEmailBody(temp);
                                if (!flag)
                                {
                                    flagExit = false;
                                    error += "Invalid note for the Requested Item.";
                                }

                                //Verify Approve ticket ID link
                                temp = "Click here to approve " + RITMiD;
                                flag = email.VerifyEmailBody(temp);
                                if (!flag)
                                {
                                    flagExit = false;
                                    error += "Invalid Approve message.";
                                }

                                //Verify Reject ticket ID link
                                temp = "Click here to reject " + RITMiD;
                                flag = email.VerifyEmailBody(temp);
                                if (!flag)
                                {
                                    flagExit = false;
                                    error += "Invalid Reject message.";
                                }
                            }

                        }
                        #endregion

                        #region Close HTML Body
                        if (continueStep)
                        {
                            flag = email.ClickOnCloseButton();
                            if (!flag) error += "Error when close email body.";
                            if (!flag) { continueStep = false; }
                        }
                        if (!flag) { break; }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------
        [Test]
        public void Step_063_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_01_ClickOn_Email_Item_Approved_OR_Rejected()
        {
            try
            {

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                string customer2 = Base.GData("Customer2");
                string cus_email = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    cus_email = Base.GData("Customer2_Email");
                }
                else cus_email = Base.GData("Customer_Email");


                string[] action = Base.GData("Approvers_Action").Split(';');
                foreach (string act in action)
                {
                    flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                    if (flag)
                    {
                        emailList.WaitLoading();

                        if (!emailList.List_Title().MyText.Contains("Emails"))
                        {
                            flag = false;
                            error = "Cannot open email list.";
                        }
                    }
                    else error = "Error when open email log.";
                    //-----------------------------------------------
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been rejected|and|Recipients;contains;" + cus_email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                            flag = emailList.Open("Recipients=@@" + cus_email, "Created");
                            if (!flag)
                            {
                                flagExit = false;
                                error = "Not found email sent to Requested For (has been rejected)";
                            }
                            else
                            { email.WaitLoading(); }
                        }
                    }
                    else
                    {
                        temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been approved|and|Recipients;contains;" + cus_email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                            flag = emailList.Open("Recipients=@@" + cus_email, "Created");
                            if (!flag)
                            {
                                flagExit = false;
                                error = "Not found email sent to Requested For (request submitted)";
                            }
                            else
                            { email.WaitLoading(); }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_02_Verify_Email_Item_Approved_OR_Rejected_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                string item_shortdes = Base.GData("Item_ShortDes");
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "Requested Item " + RITMiD + " " + item_shortdes + " has been rejected.";
                        break;
                    }
                }
                if (temp == null)
                { temp = "Item " + RITMiD + " has been approved."; }
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_03_Verify_Email_Item_Approved_OR_Rejected_Recipient()
        {
            try
            {
                string customer2 = Base.GData("Customer2");
                string temp = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    temp = Base.GData("Customer2_Email");
                }
                else temp = Base.GData("Customer_Email");

                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_04_Open_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag)
                {
                    error = "Error when click on priview html body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_05_Verify_EmailHtmlBody_Item()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                string item_shortdes = Base.GData("Item_ShortDes");
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "Requested Item Number: " + RITMiD;
                        break;
                    }
                }
                if (temp == null)
                {
                    temp = "Requested item " + RITMiD.Trim() + " " + Base.GData("Item_ShortDes") + " has been approved and will be fulfilled.";
                }
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_06_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                string item_shortdes = Base.GData("Item_ShortDes");
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "Click here to view the Requested Item: " + RITMiD;
                        break;
                    }
                }
                if (temp == null)
                { temp = "To view the item, click here: " + RITMiD; }
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Link.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_07_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag)
                {
                    error = "Error when close email body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_065_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_01_ClickOn_Email_Item_Completed_OR_Incompleted()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                string customer2 = Base.GData("Customer2");
                string cus_email = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    cus_email = Base.GData("Customer2_Email");
                }
                else cus_email = Base.GData("Customer_Email");

                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has not been completed|and|Recipients;contains;" + cus_email;
                        break;
                    }
                }
                if (temp == null)
                { temp = "Subject;contains;" + RITMiD + "|and|Subject;contains;has been completed|and|Recipients;contains;" + cus_email; }

                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    flag = emailList.Open("Recipients=@@" + cus_email, "Created");
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Not found email sent to Requested For (request completed/incompleted)";
                    }
                    else
                    { email.WaitLoading(); }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_02_Verify_Email_Item_Completed_OR_Incompleted_Subject()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "Requested Item " + RITMiD + " has not been completed.";
                        break;
                    }
                }
                if (temp == null)
                { temp = "Requested Item " + RITMiD + " has been completed."; }
                flag = email.VerifySubject(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid subject.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_03_Verify_Email_Item_Completed_OR_Incompleted_Recipient()
        {
            try
            {
                string customer2 = Base.GData("Customer2");
                string temp = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    temp = Base.GData("Customer2_Email");
                }
                else temp = Base.GData("Customer_Email");

                flag = email.VerifyRecipient(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid recipient.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_04_Open_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag)
                {
                    error = "Error when click on priview html body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_05_Verify_EmailHtmlBody_Content()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "This Requested Item has not been completed.";
                        break;
                    }
                }
                if (temp == null)
                { temp = "This Requested Item has been completed."; }
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid note for the Requested Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_06_Verify_EmailHtmlBody_ItemName()
        {
            try
            {
                temp = "Item: " + Base.GData("Item_Email");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_07_Verify_EmailHtmlBody_ItemState()
        {
            try
            {
                temp = null;
                string[] action = Base.GData("Approvers_Action").Split(';');
                foreach (string act in action)
                {
                    if (act.ToLower().Trim().Contains("reject"))
                    {
                        temp = "State: Closed Incomplete";
                        break;
                    }
                }
                if (temp == null)
                { temp = "State: Closed Complete"; }
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_08_Verify_EmailHtmlBody_ItemLink()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && RITMiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Requested Item Id.");
                    addPara.ShowDialog();
                    RITMiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view: " + RITMiD;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Item Link.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_09_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag)
                {
                    error = "Error when close email body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                flagExit = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_Ensure_No_RequestEmail_ToReqFor()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && REQiD == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Request Id.");
                    addPara.ShowDialog();
                    REQiD = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                string customer2 = Base.GData("Customer2");
                string cus_email = string.Empty;
                if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                {
                    cus_email = Base.GData("Customer2_Email");
                }
                else cus_email = Base.GData("Customer_Email");

                temp = "Subject;contains;" + REQiD + "|and|Subject;does not contain;survey|and|Recipients;contains;" + cus_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    flag = emailList.VerifyRow("Recipients=" + cus_email);
                    if (flag)
                    {
                        flag = false;
                        flagExit = false;
                        error = "There is/are email(s) sent to Requested For/Opened By regarding Request";
                    }
                    else { flag = true; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_Ensure_No_CTaskEmail_ToReqFor()
        {
            try
            {
                error = "";
                string act = Base.GData("Approvers_Action");
                string[] action;
                if (act.Contains(";"))
                    action = act.Split(';');
                else
                    action = new string[] { act };

                bool flagCheck = false;
                foreach (string ac in action)
                {
                    if (ac.ToLower().Trim().Contains("reject"))
                    {
                        flagCheck = true;
                        break;
                    }
                }
                if (flagCheck == false)
                {

                    temp = Base.GData("Debug").ToLower();

                    if (temp == "yes" && CTaskList == string.Empty)
                    {
                        AddParameter addPara = new AddParameter("Please input Catalog Task string");
                        addPara.ShowDialog();
                        CTaskList = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //------------------------------------------------------------------------------------------

                    if (CTaskList.Substring(CTaskList.Length - 1, 1).Equals(";"))
                        CTaskList = CTaskList.Substring(0, CTaskList.Length - 1);

                    string[] CTaskiD;
                    if (CTaskList.Contains(";"))
                        CTaskiD = CTaskList.Split(';');
                    else
                        CTaskiD = new string[] { CTaskList };

                    for (int i = 0; i < CTaskiD.Length; i++)
                    {
                        if (temp == "yes" && CTaskiD[i] == string.Empty)
                        {
                            AddParameter addPara = new AddParameter("Please input Catalog Task Id [" + i + "]");
                            addPara.ShowDialog();
                            CTaskiD[i] = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //------------------------------------------------------------------------------------------
                        home.LeftMenuItemSelect("CSC Run", "Email Log");
                        emailList.WaitLoading();

                        string customer2 = Base.GData("Customer2");
                        string cus_email = string.Empty;
                        if (customer2.ToLower() != null && customer2.ToLower() != "" && customer2.ToLower() != "no")
                        {
                            cus_email = Base.GData("Customer2_Email");
                        }
                        else cus_email = Base.GData("Customer_Email");

                        temp = "Subject;contains;" + CTaskiD[i] + "|and|Recipients;contains;" + cus_email;
                        bool flagT = true;
                        flagT = emailList.EmailFilter(temp);
                        if (flagT)
                        {
                            flagT = emailList.VerifyRow("Recipients=" + cus_email);
                            if (flagT)
                            {
                                error += "There is/are email(s) sent to Requested For/Opened By regarding Catalog Task";
                                if (flag)
                                    flag = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
