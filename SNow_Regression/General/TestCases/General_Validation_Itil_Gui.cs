﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace General
{
    [TestFixture, Description("Use to verify itil gui for multi platform")]
    public class General_Validation_Itil_Gui
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Description:" + Base.GData("TS_Description"));
            System.Console.WriteLine("Known defect:" + Base.GData("Known_Defect"));
            if (Type == "default" || Type == "ideas")
            {
                System.Console.WriteLine("Finished - [" + Form + "] number: " + Id);
                if(Type == "ideas")
                    System.Console.WriteLine("Finished - Current date time: " + CurrentDatetime);
            }
                
            else
            {
                System.Console.WriteLine("Finished - Current date time: " + CurrentDatetime);
                System.Console.WriteLine("Finished - [" + Form + " - " + Platform + "] name: " + Id);
            }

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        Dictionary<string, snoelement> listOfControl = null;
        string Id, CurrentDatetime, Form, Platform, SearchColumn, Module, Type, TestPermission, template;
        Login login;
        Home home;
        SNow.Itil gui;
        SNow.ItilList list;
        //-----------------------------
        
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                listOfControl = new Dictionary<string, snoelement>();
                //-------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                gui = new SNow.Itil(Base, "Gui");
                list = new SNow.ItilList(Base, "List");
                //-------------------------------------------------------
                Id = string.Empty;
                CurrentDatetime = DateTime.Now.ToString("yyyyMMddHHmmss");
                Form = Base.GData("FormName");
                Platform = Base.GData("PlatformName");
                TestPermission = Base.GData("TestPermission");
                template = Base.GData("Template");
                //-------------------------------------------------------
                switch (Form.ToLower())
                {
                    case "normal change":
                    case "standard change":
                    case "emergency change":
                        SearchColumn = "Number";
                        Module = "Change";
                        Type = "default";
                        break;
                    case "incident":
                    case "problem":
                    case "work order":
                    case "demands":
                    case "projects":
                    case "security incident":
                    case "case":
                    case "release":
                    case "knowledge":
                    case "calls":
                        SearchColumn = "Number";
                        Module = Form;
                        Type = "default";
                        break;
                    case "portfolio":
                    case "asset":
                    case "other assets":
                        switch (Platform.ToLower()) 
                        {
                            case "consumables":
                                SearchColumn = "Display name";
                                Module = Form;
                                Type = "consumables";
                                break;
                            default:
                                SearchColumn = "Asset tag";
                                Module = Form;
                                Type = "asset";
                                break;
                        }
                        break;
                    case "ideas":
                        SearchColumn = "Number";
                        Module = Form;
                        Type = "ideas";
                        break;
                    default:
                        SearchColumn = "Name";
                        Module = Form;
                        Type = "ci";
                        break;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else 
                {
                    login.WaitLoading();
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                
                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser()
        {
            try
            {
                string temp = Base.GData("ImpersonateUser");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Open_List()
        {
            try
            {
                string temp = "Open";
                if (Platform.ToLower() != "no" && Platform.ToLower() != "") 
                {
                    if (Module.ToLower() == "ideas")
                        temp = "All";
                    else if(Module.ToLower() == "other assets")
                        temp = Module;
                    else
                        temp = Platform;
                }
                    
                flag = home.LeftMenuItemSelect(Module, temp);
                if (!flag)
                    error = "Error when open list.";
                else list.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Verify_List_Title()
        {
            try
            {
                string expectedTitle = Base.GData("List_Title");
                if (expectedTitle.Trim().ToLower() != "no")
                {
                    snoelement label = list.List_Title();
                    string runtimeTitle = label.MyText;
                    if (!Base.StringCompare(runtimeTitle, expectedTitle))
                    {
                        flag = false;
                        flagExit = false;
                        error = "***[FAILED]: Invalid list tilte. Runtime: [" + runtimeTitle + "] - Expected: [" + expectedTitle + "]";
                    }
                    else Console.WriteLine("***[Passed]: Runtime [" + runtimeTitle + "] - Expected: [" + expectedTitle + "]");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Reset_List_Columns_To_Default_Value()
        {
            try
            {
                flag = list.ResetColumnToDefaultValue();
                if (!flag)
                    error = "Error when reset list columns to default value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Verify_List_Columns_Default_Value()
        {
            try
            {
                string expectedColumns = Base.GData("List_Default_Columns");
                if (expectedColumns.Trim().ToLower() != "no")
                {
                    flag = list.VerifyColumnHeader(expectedColumns, ref error);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid default value of columns list.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Verify_Item_List_Combobox_Goto()
        {
            try
            {
                string expectedColumns = Base.GData("List_Combobox_Goto_Items");
                if (expectedColumns.Trim().ToLower() != "no")
                {
                    flag = list.VerifyComboboxGoto(expectedColumns, ref error);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid item list value of combobox goto.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_ClickOn_New_Button()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes") 
                {
                    Base.SwitchToPage(0);
                    snobutton button = list.Button_New();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (flag)
                        {
                            Thread.Sleep(10000);
                            gui.WaitLoading();

                            if (Module.ToLower() == "change")
                            {
                                flag = gui.Select_Change_Type(Form);
                                Thread.Sleep(10000);
                                if (!flag) error = "Error when select change type.";
                                else gui.WaitLoading();
                            }

                            if (Module.ToLower() == "ideas")
                            {
                                flag = gui.Select_Ideas_Type(Platform);
                                Thread.Sleep(5000);
                                if (!flag) error = "Error when select ideas type.";
                                else gui.WaitLoading();
                            }

                            if (Module.ToLower() == "other assets") 
                            {
                                flag = gui.Select_OtherAssets_Type(Platform);
                                Thread.Sleep(5000);
                                if (!flag) error = "Error when select other assets type.";
                                else gui.WaitLoading();
                            }

                            if (template.Trim().ToLower() != string.Empty && template.Trim().ToLower() != "no") 
                            {
                                if (template.Contains(";"))
                                    flag = gui.SelectTemplate(template);
                                else
                                    flag = gui.SelectTemplate_1(template);
                                Thread.Sleep(5000);
                                if (flag)
                                    gui.WaitLoading();
                            }
                        }
                        else
                            error = "Error when click on button New.";
                    }
                    else error = "Cannot get button New.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_ClickOn_Submit_Button()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    snobutton button = gui.Button_Submit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                            error = "Error when click on submit button.";
                        else gui.WaitLoading();
                    }
                    else error = "Cannot get button submit.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_Message_Requite_Mandatory_Field()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string message = Base.GData("Message_Mandatory_Fields_Not_Filled");
                    flag = gui.Verify_ExpectedErrorMessages_Existed(message);
                    if (!flag)
                    {

                        error = "Invalid message error.";
                        flagExit = false;
                    }
                    else
                    {
                        snobutton button = gui.Button_Close_Error();
                        if (button.Existed)
                            button.Click(true);
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Input_Optional_Fields_1()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    Base.SwitchToPage(0);
                    string optionfield = Base.GData("Optional_Fields_1");
                    if (optionfield.ToLower() != "no")
                    {
                        flag = gui.Input_Value_For_Controls(optionfield);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Cannot fill optional fields.";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_GET_ALL_TEXTBOX_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("TextboxFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("TextboxFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("TextboxFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no") 
                    {
                        listOfControl = gui.GetControlOnForm("textbox");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_02_Verify_Textbox_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("TextboxFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("textbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_03_Verify_Textbox_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("TextboxFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("textbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_04_Verify_Textbox_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("TextboxFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("textbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_01_GET_ALL_COMBOBOX_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("ComboboxFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("ComboboxFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("ComboboxFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("combobox");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_02_Verify_Combobox_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("ComboboxFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("combobox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_03_Verify_Combobox_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("ComboboxFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("combobox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_04_Verify_Combobox_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("ComboboxFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("combobox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_01_GET_ALL_TEXTAREA_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("TextareaFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("TextareaFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("TextareaFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("textarea");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_02_Verify_Textarea_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("TextareaFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("textarea", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_03_Verify_Textarea_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("TextareaFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("textarea", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_04_Verify_Textarea_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("TextareaFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("textarea", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_01_GET_ALL_DATE_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("DateFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("DateFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("DateFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("date");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_02_Verify_Date_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("DateFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("date", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_03_Verify_Date_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("DateFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("date", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_04_Verify_Date_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("DateFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("date", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_GET_ALL_DATETIME_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("DatetimeFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("DatetimeFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("DatetimeFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("datetime");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_Verify_Datetime_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("DatetimeFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("datetime", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_03_Verify_Datetime_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("DatetimeFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("datetime", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_04_Verify_Datetime_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("DatetimeFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("datetime", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_GET_ALL_CURRENCY_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("CurrencyFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("CurrencyFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("CurrencyFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("currency");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Verify_Currency_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CurrencyFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("currency", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_Verify_Currency_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CurrencyFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("currency", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_04_Verify_Currency_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CurrencyFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("currency", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_GET_ALL_LOOKUP_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("LookupFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("LookupFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("LookupFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("lookup");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_Verify_Lookup_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("LookupFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("lookup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_03_Verify_Lookup_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("LookupFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("lookup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_04_Verify_Lookup_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("LookupFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("lookup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_01_GET_ALL_CONTROL_WATCH_LIST_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("CWList_Visible").Trim().ToLower();
                    string str2 = Base.GData("CWList_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("list");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_02_Verify_CWList_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CWList_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("list", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_03_Verify_CWList_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CWList_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("list", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_01_GET_ALL_CHECKBOX_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("CheckboxFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("CheckboxFields_Readonly").Trim().ToLower();
                    string str3 = Base.GData("CheckboxFields_Default_Value").Trim().ToLower();
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("checkbox");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_02_Verify_Checkbox_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("checkbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_03_Verify_Checkbox_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("checkbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_04_Verify_Checkbox_Default_Value()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxFields_Default_Value");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Default_Value_Controls("checkbox", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_GET_ALL_RADIO_GROUP_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("RadioGroup_Visible").Trim().ToLower();
                    string str2 = Base.GData("RadioGroup_Readonly").Trim().ToLower();
                    string str3 = Base.GData("RadioGroup_Items");
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("radiogroup");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Verify_RadioGroup_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("RadioGroup_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("radiogroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_03_Verify_RadioGroup_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("RadioGroup_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("radiogroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_04_Verify_RadioGroup_Items()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("RadioGroup_Items");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_RadioGroup_Items(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_GET_ALL_CHECKBOX_GROUP_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("CheckboxGroup_Visible").Trim().ToLower();
                    string str2 = Base.GData("CheckboxGroup_Readonly").Trim().ToLower();
                    string str3 = Base.GData("CheckboxGroup_Items");
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("checkboxgroup");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_Verify_CheckboxGroup_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxGroup_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("checkboxgroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_03_Verify_CheckboxGroup_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxGroup_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("checkboxgroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_04_Verify_CheckboxGroup_Items()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxGroup_Items");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_CheckboxGroup_Items(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_GET_ALL_MANDATORY_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("MandatoryFields_Visible").Trim().ToLower();
                    string str2 = Base.GData("MandatoryFields_Readonly").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetManatoryControlOnForm();
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_Verify_Mandatory_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("MandatoryFields_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("mandatory", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_Verify_Mandatory_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("MandatoryFields_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("mandatory", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_GET_ALL_BUTTON_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("Button_Visible").Trim().ToLower();
                    string str2 = Base.GData("Button_Not_Visible").Trim().ToLower();
                    if (str1 != "no" || str2 != "no")
                    {
                        listOfControl = gui.GetButtonControlOnForm();
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_Verify_Button_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("Button_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Button_Controls(false, expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_03_Verify_Button_Not_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("Button_Not_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Button_Controls(true, expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Verify_Combobox_Items()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("ComboboxFields_Items");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Combobox_Items(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Verify_Combobox_Parent_Child_Items()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("Combobox_Parent_Child_Items");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Combobox_Parent_Child_Items(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Verify_Lookup_Columns_Search_Table()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    Base.SwitchToPage(0);
                    string expectedControlList = Base.GData("Lookup_Columns_Search_Table");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Lookup_Columns_Search_Table(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_01_Attach_File()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string attachmentFile = Base.GData("Attachment_File");
                    if (attachmentFile.Trim().ToLower() != "no")
                    {
                        flag = gui.Add_AttachmentFile(attachmentFile);
                        if (flag == false)
                        {
                            error = "Error when attachment file.";
                            flagExit = false;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_02_Verify_Attachment_File()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string attachmentFile = Base.GData("Attachment_File");
                    if (attachmentFile.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Attachment_File(attachmentFile);
                        if (!flag)
                        {
                            error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                            flagExit = false;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_01_Input_Optional_Fields_2()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    Base.SwitchToPage(0);
                    string optionfield = Base.GData("Optional_Fields_2");
                    if (optionfield.ToLower() != "no")
                    {
                        flag = gui.Input_Value_For_Controls(optionfield);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Cannot fill optional fields.";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_02_Input_Mandatory_Fields()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string caller = Base.GData("Caller");
                    string company = Base.GData("Company");
                    string temp = string.Empty;
                    if (caller.ToLower() != "no" || company.ToLower() != "no") 
                    {
                        if (caller.ToLower() == "no")
                        {
                            temp = "Company=" + company;
                            flag = gui.Input_Value_For_Controls(temp);
                            if (!flag)
                                error = "Cannot populate company.";
                        }
                        else
                        {
                            temp = "Caller=" + caller;
                            flag = gui.Input_Value_For_Controls(temp);
                            if (!flag)
                                error = "Cannot populate caller.";
                        }
                    }
                    
                    Thread.Sleep(2000);
                    if (flag)
                    {
                        if (Type == "default")
                            flag = gui.Input_Mandatory_Fields("Automation value");
                        else
                        {
                            flag = gui.Input_Mandatory_Fields("Automation value_" + CurrentDatetime);
                        }

                        if (!flag)
                        {
                            error = "Have error when auto fill mandatory fields.";
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Store_Id_Or_Name_And_Submit()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    switch (Type.ToLower()) 
                    {
                        case "default":
                            Id = gui.Textbox_Number().Text;
                            break;
                        case "ci":
                            Id = gui.Textbox_Name_CI().Text;
                            break;
                        case "asset":
                            Id = gui.Textbox_AssetTag().Text;
                            break;
                        case "consumables":
                            Id = gui.Lookup_Model().Text;
                            break;
                    }


                    if (Module.ToLower() == "knowledge")
                        flag = gui.Save(false, true);
                    else
                    {
                        snobutton button = gui.Button_Submit();
                        flag = button.Existed;
                        if (flag)
                            flag = button.Click();
                        else error = "Cannot get button submit.";
                    }

                    if (flag)
                    {
                        list.WaitLoading();
                        if (Module.ToLower() == "change")
                        {
                            flag = home.LeftMenuItemSelect(Module, "Open");
                            if (flag)
                                list.WaitLoading();
                        }

                        if (Module.ToLower() == "ideas")
                        {

                            string tmp = "div[class='outputmsg_text']>a";
                            snoelement sne = (snoelement)Base.SNGObject("id", "element", By.CssSelector(tmp), null, snobase.MainFrame);
                            if (sne.Existed)
                                Id = sne.MyText.Trim();

                            flag = home.LeftMenuItemSelect(Module, "All");
                            if (flag)
                            {
                                list.WaitLoading();
                                if (Id == null || Id == string.Empty)
                                {
                                    string condition = "Short Description=Automation value_" + CurrentDatetime;
                                    flag = list.SearchAndVerify("Short Description", "Automation value_" + CurrentDatetime, condition);
                                    Id = list.RelatedTableGetCell(condition, "Number");
                                }
                            }

                        }

                        if (Module.ToLower() == "other assets")
                        {
                            flag = home.LeftMenuItemSelect(Module, Module);
                            if (flag)
                                list.WaitLoading();
                        }

                        if (Module.ToLower() == "security incident")
                        {
                            flag = home.LeftMenuItemSelect(Module, Platform);
                            if (flag)
                                list.WaitLoading();
                        }

                        if (Module.ToLower() == "knowledge")
                        {
                            flag = false;
                            snobutton publish = gui.Button_Publish();
                            if (publish != null && publish.Existed)
                            {
                                flag = publish.Click();
                                list.WaitLoading();
                            }
                            else
                            {
                                snobutton review = gui.Button_Review();
                                if (review != null && review.Existed)
                                {
                                    flag = review.Click();
                                    list.WaitLoading();
                                }
                            }

                            if (!flag)
                                error = "Error when click on publish button.";
                            else
                            {
                                flag = home.LeftMenuItemSelect(Module, Platform);
                                if (flag)
                                    list.WaitLoading();
                            }
                        }
                    }
                    else { error = "Cannot click button or some required fields are not populate to proceed next step."; }


                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Verify_Back_To_List()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedTitle = Base.GData("List_Title");
                    snoelement label = list.List_Title();
                    string runtimeTitle = label.MyText;
                    if (!Base.StringCompare(runtimeTitle, expectedTitle))
                    {
                        flag = false;
                        flagExit = false;
                        error = "***[FAILED]: Invalid list tilte. Runtime: [" + runtimeTitle + "] - Expected: [" + expectedTitle + "]";
                    }
                    else Console.WriteLine("***[Passed]: Runtime [" + runtimeTitle + "] - Expected: [" + expectedTitle + "]");
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Add_More_Columns_If_Need()
        {
            try
            {
                switch (Module.Trim().ToLower())
                {
                    case "demands":
                        flag = list.Add_More_Columns("Number|Name");
                        break;
                    case "change":
                        flag = list.Add_More_Columns("Type|State");
                        break;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Search_And_ClickOn_Checkbox_Record()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (Id == null || Id == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input Id or Name.");
                        addPara.ShowDialog();
                        Id = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //--------------------------------------------------------------------------------
                    string condition = SearchColumn + "=" + Id; 
                    if (Type.ToLower() != "consumables")
                    {
                        flag = list.SearchAndVerify(SearchColumn, Id, condition);
                    }
                    else 
                    {
                        flag = list.SearchAndVerify("@@" + SearchColumn, Id, condition);
                    }

                    if (flag)
                    {
                        snotable tb = list.Table_List();
                        flag = tb.Existed;
                        if (flag)
                        {
                            flag = tb.FindRowAndClickCheckbox(condition);
                            if (!flag)
                                error = "Error when click on checkbox of record.";
                        }
                    }
                    else error = "Cannot get table list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Verify_Actions_Items_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string temp = Base.GData("List_Action_Items_Visible");
                    if (temp.ToLower() != "no")
                    {
                        flag = list.Verify_Action_Items_Visible(temp);
                    }

                    if (!flag)
                    {
                        error = "Invalid action items visible.";
                        flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Verify_Actions_Items_Enable()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string temp = Base.GData("List_Action_Items_Enable");
                    if (temp.ToLower() != "no")
                    {
                        flag = list.Verify_Action_Items_Enable(temp, true);
                    }

                    if (!flag)
                    {
                        error = "Invalid action items enable.";
                        flagExit = false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Search_And_Open()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (Id == null || Id == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input Id or Name.");
                        addPara.ShowDialog();
                        Id = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    string conditions = SearchColumn + "=" + Id;
                    if (Type.ToLower() != "consumables")
                    {
                        if (Form.ToLower() == "demands")
                            flag = list.SearchAndOpen(SearchColumn, Id, conditions, "Number");
                        else
                            flag = list.SearchAndOpen(SearchColumn, Id, conditions, SearchColumn);
                    }
                    else 
                    {
                        flag = list.SearchAndOpen("@@" + SearchColumn, Id, conditions, SearchColumn);
                    }

                    Thread.Sleep(2000);

                    if (!flag) error = "Error when search and open record with contions: [" + conditions + "]";
                    else gui.WaitLoading();
                }
                else 
                {
                    string filterCondition = Base.GData("FilterCondition");
                    flag = list.Add_More_Columns("Company");
                    if (flag)
                    {
                        if (filterCondition.Trim() != string.Empty)
                        {
                            flag = list.Filter(filterCondition);
                            if (flag)
                            {
                                string column = Base.GData("ColumnSort");
                                if (column.Trim() != string.Empty)
                                {
                                    list.Sort_Items_By_Column(column);
                                    string searchCondition = Base.GData("SearchCondition");
                                    if (searchCondition.Trim() != string.Empty)
                                    {
                                        flag = list.Open(searchCondition, column);
                                        if (!flag)
                                            error = "Cannot open.";
                                    }
                                    else
                                    {
                                        flag = false;
                                        error = "Please input search condition into column 'SearchCondition' excel file data.";
                                    }
                                }
                            }
                        }
                    }
                    else 
                    {
                        flag = false;
                        error = "Please input filter condition into column 'FilterCondition' excel file data.";
                    } 
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_01_Rename_Attachment_File()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string attachmentFile = Base.GData("Attachment_File");
                    if (attachmentFile.Trim().ToLower() != "no")
                    {
                        flag = gui.Rename_AttachmentFile(attachmentFile);
                        if (flag == false)
                        {
                            error = "Error when rename attachment file.";
                            flagExit = false;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_02_Verify_Attachment_File_Renamed()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string temp = Base.GData("Attachment_File");
                    if (temp.Trim().ToLower() != "no")
                    {
                        string[] arr = temp.Split('.');
                        string attachmentFile = arr[0] + "_rename" + "." + arr[1];
                        flag = gui.Verify_Attachment_File(attachmentFile);
                        if (!flag)
                        {
                            error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                            flagExit = false;
                        }
                    }
                }
                
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_Delete_Attachment_File_And_Save()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string temp = Base.GData("Attachment_File");
                    if (temp.Trim().ToLower() != "no")
                    {
                        string[] arr = temp.Split('.');
                        string attachmentFile = arr[0] + "_rename" + "." + arr[1];
                        flag = gui.Delete_AttachmentFile(attachmentFile);
                        if (flag == false)
                        {
                            error = "Error when delete attachment file.";
                            flagExit = false;
                        }
                        else
                        {
                            flag = gui.Save();
                            gui.WaitLoading();
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Verify_Attachment_File_Deleted()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string temp = Base.GData("Attachment_File");
                    if (temp.Trim().ToLower() != "no")
                    {
                        string[] arr = temp.Split('.');
                        string attachmentFile = arr[0] + "_rename" + "." + arr[1];
                        flag = gui.Verify_Attachment_File(attachmentFile, true);
                        if (flag)
                        {
                            error = "Found attachment file (" + attachmentFile + ") in attachment container.";
                            flag = false;
                            flagExit = false;
                        }
                        else flag = true;
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_AFTER_SAVE_GET_ALL_TEXTBOX_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("TextboxFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("TextboxFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("TextboxFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("textbox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_Verify_After_Save_Textbox_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("TextboxFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("textbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_03_Verify_After_Save_Textbox_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("TextboxFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("textbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_04_Verify_After_Save_Textbox_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("TextboxFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("textbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_01_AFTER_SAVE_GET_ALL_COMBOBOX_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("ComboboxFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("ComboboxFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("ComboboxFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("combobox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_02_Verify_After_Save_Combobox_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("ComboboxFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("combobox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_03_Verify_After_Save_Combobox_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("ComboboxFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("combobox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_04_Verify_After_Save_Combobox_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("ComboboxFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("combobox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_01_AFTER_SAVE_GET_ALL_TEXTAREA_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("TextareaFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("TextareaFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("TextareaFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("textarea");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_Verify_After_Save_Textarea_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("TextareaFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("textarea", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_03_Verify_After_Save_Textarea_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("TextareaFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("textarea", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_04_Verify_After_Save_Textarea_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("TextareaFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("textarea", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_01_AFTER_SAVE_GET_ALL_DATE_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("DateFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("DateFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("DateFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("date");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_02_Verify_After_Save_Date_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("DateFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("date", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_03_Verify_After_Save_Date_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("DateFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("date", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_04_Verify_After_Save_Date_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("DateFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("date", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_AFTER_SAVE_GET_ALL_DATETIME_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("DatetimeFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("DatetimeFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("DatetimeFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("datetime");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_Verify_After_Save_Datetime_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("DatetimeFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("datetime", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Verify_After_Save_Datetime_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("DatetimeFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("datetime", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_04_Verify_After_Save_Datetime_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("DatetimeFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("datetime", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_AFTER_SAVE_GET_ALL_CURRENCY_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("CurrencyFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("CurrencyFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("CurrencyFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("currency");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_Verify_After_Save_Currency_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("CurrencyFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("currency", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_03_Verify_After_Save_Currency_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("CurrencyFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("currency", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_04_Verify_After_Save_Currency_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("CurrencyFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("currency", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_AFTER_SAVE_GET_ALL_LOOKUP_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("LookupFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("LookupFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("LookupFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("lookup");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Verify_After_Save_Lookup_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("LookupFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("lookup", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_03_Verify_After_Save_Lookup_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("LookupFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("lookup", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_04_Verify_After_Save_Lookup_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("LookupFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("lookup", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_01_AFTER_SAVE_GET_ALL_CONTROL_WATCH_LIST_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("CWList_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("CWList_After_Save_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetControlOnForm("list");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_02_Verify_After_Save_CWList_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("CWList_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("list", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_03_Verify_After_Save_CWList_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("CWList_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("list", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_01_AFTER_SAVE_GET_ALL_CHECKBOX_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("CheckboxFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("CheckboxFields_After_Save_Readonly").Trim().ToLower();
                string str3 = Base.GData("CheckboxFields_After_Save_Default_Value").Trim().ToLower();
                if (str1 != "no" || str2 != "no" || str3 != "no")
                {
                    listOfControl = gui.GetControlOnForm("checkbox");
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_02_Verify_After_Save_Checkbox_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("CheckboxFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("checkbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_03_Verify_After_Save_Checkbox_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("CheckboxFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("checkbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_04_Verify_After_Save_Checkbox_Default_Value()
        {
            try
            {
                string expectedControlList = Base.GData("CheckboxFields_After_Save_Default_Value");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Default_Value_Controls("checkbox", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_01_AFTER_SAVE_GET_ALL_RADIO_GROUP_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("RadioGroup_After_Save_Visible").Trim().ToLower();
                    string str2 = Base.GData("RadioGroup_After_Save_Readonly").Trim().ToLower();
                    string str3 = Base.GData("RadioGroup_After_Save_Items");
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("radiogroup");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_02_Verify_After_Save_RadioGroup_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("RadioGroup_After_Save_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("radiogroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_03_Verify_After_Save_RadioGroup_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("RadioGroup_After_Save_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("radiogroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_04_Verify_After_Save_RadioGroup_Items()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("RadioGroup_After_Save_Items");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_RadioGroup_Items(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_01_AFTER_SAVE_GET_ALL_CHECKBOX_GROUP_ON_FORM()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string str1 = Base.GData("CheckboxGroup_After_Save_Visible").Trim().ToLower();
                    string str2 = Base.GData("CheckboxGroup_After_Save_Readonly").Trim().ToLower();
                    string str3 = Base.GData("CheckboxGroup_After_Save_Items");
                    if (str1 != "no" || str2 != "no" || str3 != "no")
                    {
                        listOfControl = gui.GetControlOnForm("checkboxgroup");
                        if (snobase.dicDupplicateControl.Count > 0)
                        {
                            flag = false;
                            flagExit = false;
                            error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                            foreach (string key in snobase.dicDupplicateControl.Keys)
                            {
                                error = error + key + "\n";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_02_Verify_After_Save_CheckboxGroup_Visible()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxGroup_After_Save_Visible");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Visible_Controls("checkboxgroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_03_Verify_After_Save_CheckboxGroup_ReadOnly()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxGroup_After_Save_Readonly");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_Readonly_Controls("checkboxgroup", expectedControlList, ref error, listOfControl);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_04_Verify_After_Save_CheckboxGroup_Items()
        {
            try
            {
                if (TestPermission.Trim().ToLower() != "yes")
                {
                    string expectedControlList = Base.GData("CheckboxGroup_After_Save_Items");
                    if (expectedControlList.Trim().ToLower() != "no")
                    {
                        flag = gui.Verify_CheckboxGroup_Items(expectedControlList, ref error);
                        if (!flag)
                            flagExit = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_AFTER_SAVE_GET_ALL_MANDATORY_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("MandatoryFields_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("MandatoryFields_After_Save_Readonly").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetManatoryControlOnForm();
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_02_Verify_After_Save_Mandatory_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("MandatoryFields_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Visible_Controls("mandatory", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_03_Verify_After_Save_Mandatory_ReadOnly()
        {
            try
            {
                string expectedControlList = Base.GData("MandatoryFields_After_Save_Readonly");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Readonly_Controls("mandatory", expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_01_AFTER_SAVE_GET_ALL_BUTTON_ON_FORM()
        {
            try
            {
                string str1 = Base.GData("Button_After_Save_Visible").Trim().ToLower();
                string str2 = Base.GData("Button_After_Save_Not_Visible").Trim().ToLower();
                if (str1 != "no" || str2 != "no")
                {
                    listOfControl = gui.GetButtonControlOnForm();
                    if (snobase.dicDupplicateControl.Count > 0)
                    {
                        flag = false;
                        flagExit = false;
                        error = "Found " + snobase.dicDupplicateControl.Count + " label of control(s) dupplicated." + "\n";
                        foreach (string key in snobase.dicDupplicateControl.Keys)
                        {
                            error = error + key + "\n";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_02_Verify_After_Save_Button_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("Button_After_Save_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Button_Controls(false, expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_03_Verify_After_Save_Button_Not_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("Button_After_Save_Not_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Button_Controls(true, expectedControlList, ref error, listOfControl);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Verify_After_Save_Combobox_Items()
        {
            try
            {
                string expectedControlList = Base.GData("ComboboxFields_After_Save_Items");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Combobox_Items(expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_Verify_After_Save_Combobox_Parent_Child_Items()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_Combobox_Parent_Child_Items");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Combobox_Parent_Child_Items(expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_Verify_After_Save_Lookup_Columns_Search_Table()
        {
            try
            {
                Base.SwitchToPage(0);
                string expectedControlList = Base.GData("After_Save_Lookup_Columns_Search_Table");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Lookup_Columns_Search_Table(expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Verify_After_Save_Tabs_Visilbe()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_Tabs_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Tabs_Visilbe(expectedControlList);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_Verify_After_Save_RelatedTables_Default_Columns()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_RelatedTables_Default_Columns");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Default_Columns(expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Verify_After_Save_RelatedTables_Combobox_Goto()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_RelatedTables_Combobox_Goto");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Combobox_Goto(expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_Verify_After_Save_RelatedTables_Button_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_RelatedTables_Button_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Button_Controls(false, expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_Verify_After_Save_RelatedTables_Button_Not_Visible()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_RelatedTables_Button_Not_Visible");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_RelatedTables_Button_Controls(true, expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Verify_After_Save_Controls_Dependent_Additional()
        {
            try
            {
                string expectedControlList = Base.GData("After_Save_Control_Dependent_Additional");
                if (expectedControlList.Trim().ToLower() != "no")
                {
                    flag = gui.Verify_Controls_Dependent_Additional(expectedControlList, ref error);
                    if (!flag)
                        flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_01_Input_Optional_Fields_3()
        {
            try
            {
                Base.SwitchToPage(0);
                string optionfield = Base.GData("Optional_Fields_3");
                if (optionfield.ToLower() != "no")
                {
                    flag = gui.Input_Value_For_Controls(optionfield);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Cannot fill optional fields.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_02_After_Save_Input_Mandatory_Fields()
        {
            try
            {
                if (Type == "default")
                    flag = gui.Input_Mandatory_Fields("Automation value");
                else
                {
                    flag = gui.Input_Mandatory_Fields("Automation value_" + CurrentDatetime);
                }

                if (!flag)
                {
                    error = "Have error when auto fill mandatory fields.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_Save_Again()
        {
            try
            {
                string needSave = Base.GData("SaveBeforeLogout");
                if (needSave.Trim().ToLower() == "yes") 
                {
                    flag = gui.Save();
                    if (!flag)
                    {
                        error = "Error when save.";
                        flagExit = false;
                    }
                    else
                        gui.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
    }
}
