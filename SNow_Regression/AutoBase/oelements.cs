﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Threading;

namespace Auto
{
    public class oelements
    {
        #region Private variables

        private string myName = string.Empty;
        private IWebDriver myDriver = null;
        private By myBy = null;
        private oelement myParent = null;
        private List<oelement> myList = null;

        private string myframe;
        private bool myGetNoDisplay;
        private bool myNoWait;

        #endregion End - Private variables

        #region Properties

        public IWebDriver MyDriver
        {
            get { return myDriver; }
            set { myDriver = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string MyName
        {
            get { return myName; }
            set { myName = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public By MyBy
        {
            get { return myBy; }
            set { myBy = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public oelement MyParent
        {
            get { return myParent; }
            set { myParent = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public List<oelement> MyList
        {
            get { return myList; }
            set { myList = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int Count
        {
            get { return this.myList.Count; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        #endregion End - Properties

        #region Constructor

        public oelements(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool getNoDisplay, [Optional] bool noWait)
        {
            MyName = name;
            MyDriver = driver;
            MyBy = by;
            MyParent = parent;
            myframe = frame;
            myGetNoDisplay = getNoDisplay;
            myNoWait = noWait;
            MyList = GetElements(driver, by, parent, frame, getNoDisplay, noWait);
        }

        #endregion End - Constructor

        #region Private method

        private List<oelement> GetElements(IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool getNoDisplay, [Optional] bool noWait)
        {
            List<oelement> result = new List<oelement>();
            ReadOnlyCollection<IWebElement> iwes = null;
            uint timeout = 20;
            ISearchContext context = driver;

            Console.WriteLine("***[Finding]:" + MyName + "|" + by.ToString());

            if (noWait)
                timeout = 5;

            if (frame != string.Empty && frame != null)
            {
                SwitchTo(frame);
            }
            else 
            {
                driver.SwitchTo().DefaultContent();
            }

            if (parent != null)
            {
                context = parent.MyElement;
            }

            try { iwes = FindMyElements(context, by, timeout); }
            catch { }

            if (iwes != null && iwes.Count > 0)
            {
                Console.WriteLine("-*-[Found]:(" + iwes.Count + ") iweb element(s).");
                try
                {
                    foreach (IWebElement e in iwes)
                    {
                        oelement ele;
                        if (!getNoDisplay)
                        {
                            if (e.Displayed)
                            {
                                ele = new oelement("item", driver, e);
                                result.Add(ele);
                            }
                        }
                        else
                        {
                            ele = new oelement("item", driver, e);
                            result.Add(ele);
                        }
                    }
                }
                catch { }

                Console.WriteLine("-*-[Found]:(" + result.Count + ") element(s).");
            }
            else
            {
                if (!noWait)
                    Console.WriteLine("-*-[Info]: Not found any iweb element.");
            }
            
            Console.WriteLine(".................................................................................................................");

            return result;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private ReadOnlyCollection<IWebElement> FindMyElements(ISearchContext context, By by, uint timeout)
        {
            var wait = new DefaultWait<ISearchContext>(context);
            wait.Timeout = TimeSpan.FromSeconds(timeout);
            wait.PollingInterval = TimeSpan.FromSeconds(1);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ctx =>
            {
                var elems = ctx.FindElements(by);
                if (elems.Count > 0)
                    return elems;
                else
                    return null;
            });
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private int GetItemIndex(string item, [Optional] int expindex)
        {
            int index = -1;
            bool flagM = true;
            string temp = string.Empty;

            if (item.Substring(0, 2) == "@@")
            {
                temp = item.Substring(2);
                flagM = false;
            }
            else
            {
                temp = item;
            }

            int count = 0;
            
            foreach (oelement e in MyList)
            {
                if (flagM)
                {
                    if (e.MyText.ToLower() == temp.ToLower())
                    {
                        if (count == expindex)
                        {
                            index = this.myList.IndexOf(e);
                            break;
                        }
                        count++;
                    }
                }
                else
                {
                    if (e.MyText.ToLower().Contains(temp.ToLower()))
                    {
                        if (count == expindex)
                        {
                            index = this.myList.IndexOf(e);
                            break;
                        }
                        count++;
                    }   
                }
            }
            if (index == -1) { Console.WriteLine("-*-ERROR: Not found item (" + temp + ") in list. Input param:(" + item + ")"); }
            Console.WriteLine("-*-Return index:" + index);
            return index;
        }
		//-----------------------------------------------------------------------------------------------------------------------------------
		private bool HaveItem(string item)
		{
			bool flag = false;
			bool flagM = true;

			if (item.Contains("@@"))
			{
				item = item.Substring(2, item.Length - 2);
				flagM = false;
				Console.WriteLine("-*-[Finding.@@] <" + item + "> in item list.");
			}
			else
			{
				Console.WriteLine("-*-[Finding..M] <" + item + "> in item list.");
			}

			foreach (oelement e in MyList)
			{
                e.MoveToElement();
                if (flagM)
				{
					if (e.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD) == item.ToLower().Normalize(System.Text.NormalizationForm.FormKD))
					{
						flag = true;
						break;
					}
				}
				else
				{
					if (e.MyText.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD).Contains(item.ToLower().Normalize(System.Text.NormalizationForm.FormKD)))
					{
						flag = true;
						break;
					}
				}
			}

			if (flag)
				Console.WriteLine("-*-[Found] <" + item + "> in item list.");
			else
				Console.WriteLine("-*-[NOT found] <" + item + "> in item list.");

			return flag;
		}

		#endregion End - Private method

		#region Public method

		public bool ClickOnItem(string item, [Optional] bool javaClick, [Optional] bool rightClick, [Optional] int expindex)
        {
            bool flag = true;
            int index = GetItemIndex(item, expindex);

            if (index == -1)
            {
                flag = false;
            }
            else
            {
                IWebElement ele = this.myList[index].MyElement;
                if (rightClick)
                {
                    Actions ac = new Actions(MyDriver);
                    ac.MoveToElement(ele);
                    ac.ContextClick(ele);
                    ac.Build().Perform();
                }
                else
                {
                    Actions ac = new Actions(MyDriver);
                    ac.MoveToElement(ele);
                    ac.Build().Perform();
                    flag = this.myList[index].Click(javaClick);
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool HaveItemsInlist(string items, string separator, [Optional] bool noWait)
        {
            bool flag = true;
            string[] arr;

            if (items.Contains(separator))
            {
                arr = items.Split(new string[] { separator }, StringSplitOptions.None);
            }
            else 
            {
                arr = new string[] { items };
            }

            foreach (string item in arr) 
            {
                bool flagF = false;
                int count = 0;
                
                if (noWait)
                    count = 8;
 
                try { flagF = HaveItem(item); }
                catch { flagF = false; }
                
                int c = 0;

                while (!flagF && count < 10) 
                {
                    c++;
                    Thread.Sleep(1000);
                    MyList = GetElements(MyDriver, MyBy, MyParent, myframe, myGetNoDisplay, myNoWait);
                    Thread.Sleep(1000);
                    try { flagF = HaveItem(item); }
                    catch { flagF = false; }
                    Console.WriteLine("-*-[Loop]:" + c + " time(s).");
                    count++;
                }
                
                if (!flagF && flag) 
                {
                    flag = false;
                }
            }

            return flag;
        }

        private bool SwitchTo(string frame)
        {
            bool flag = true;

            try
            {
                MyDriver.SwitchTo().DefaultContent();
            }
            catch { flag = false; }

            if (frame != null && frame != string.Empty && flag)
            {
                string[] arr = null;

                if (frame.Contains(";"))
                {
                    arr = frame.Split(';');
                }
                else
                {
                    arr = new string[] { frame };
                }

                bool flagT = true;

                foreach (string fr in arr)
                {
                    try
                    {
                        MyDriver.SwitchTo().Frame(fr);
                    }
                    catch { flagT = false; }

                    if (!flagT && flag)
                        flag = false;
                }
            }

            return flag;
        }
        #endregion End - Public method

    }
}
