﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Runtime.InteropServices;
using System.Threading;

namespace Auto
{
    public class otextbox : oelement
    {
        #region Constructor

        public otextbox(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }

        public otextbox(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait) 
            : base(name, driver, by, parent, frame, noWait) { }

        #endregion End - Constructor

        #region Properties

        public string Text 
        {
            get 
            {
                if (MyElement != null)
                    return MyElement.GetAttribute("value");
                else
                    return null;
            }
        }

        #endregion End - Properties

        #region Public methods

        public bool VerifyCurrentValue(string expValue, [Optional] bool wait) 
        {
            bool flag = true;
            Console.WriteLine("***[Call function]: VerifyCurrentValue");

            if (MyElement != null)
            {
                if (expValue.Substring(0, 2) == "@@")
                {
                    string temp = expValue.Trim().Substring(2);

                    if (wait)
                    {
                        int count = 0;
                        while (!Text.Trim().ToLower().Contains(temp.ToLower()) && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = Text.Trim().ToLower().Contains(temp.ToLower());
                }
                else
                {
                    if (wait)
                    {
                        int count = 0;
                        while (Text.Trim().ToLower() != expValue.Trim().ToLower() && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = (Text.Trim().ToLower() == expValue.Trim().ToLower());
                }
                
                if (flag)
                    Console.WriteLine("***[Passed] - Runtime [" + Text + "] - Expected [" + expValue + "]");
                else
                    Console.WriteLine("***[FAILED] - Runtime [" + Text + "] - Expected [" + expValue + "]");
            }
            else 
            { 
                flag = false;
                Console.WriteLine("***[ERROR] - Element is NULL");
            }
            
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SetText(string textValue, [Optional] bool pressEnter, [Optional] bool javaClick)
        {
            bool flag = true;
            Console.WriteLine("[Call function]: SetText - Value: [" + textValue + "]");

            if (MyElement != null) 
            {
                try
                {
                    IJavaScriptExecutor js = null;
                    MyElement.Clear();
                    if (javaClick)
                    {
                        js = MyDriver as IJavaScriptExecutor;
                        js.ExecuteScript("arguments[0].click();", MyElement);
                    }
                    else
                    {
                        MyElement.Click();
                    }
                    MyElement.SendKeys(textValue);

                    int count = 0;

                    while (count < 5 && Text != textValue)
                    {
                        Thread.Sleep(1000);

                        MyElement.Clear();
                        if (javaClick)
                        {
                            js.ExecuteScript("arguments[0].click();", MyElement);
                        }
                        else
                        {
                            MyElement.Click();
                        }
                        MyElement.SendKeys(textValue);

                        count++;
                    }

                    Console.WriteLine("-*-[Loop]:(" + count + ") time(s).");

                    if (Text != textValue)
                    {
                        flag = false;
                        Console.WriteLine("-*-[ERROR]: Cannot set text. Current: " + Text + "| Expected:" + textValue);
                    }
                    else
                    {
                        if (pressEnter)
                        {
                            Thread.Sleep(1000);
                            MyElement.SendKeys(Keys.Enter);
                        }
                    }

                    if (flag)
                    {
                        Console.WriteLine("***[Set text ok]");
                    }
                    Console.WriteLine(".................................................................................................................");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("***[ERROR]:" + ex.Message);
                    Console.WriteLine(".................................................................................................................");
                    flag = false;
                }
            }
            else
            {
                Console.WriteLine("***[ERROR]: Control is NULL");
                Console.WriteLine(".................................................................................................................");
                flag = false;
            }

            return flag;
        }
        
        #endregion End - Public methods
    }
}
