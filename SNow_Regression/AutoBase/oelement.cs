﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Runtime.InteropServices;
using OpenQA.Selenium.Interactions;

namespace Auto
{
    public class oelement
    {
        #region Private variables

        private string myName = null;
        private string myType = null;
        private int myIndex;
        private bool myNowait;
        private IWebElement myElement = null;
        private IWebDriver myDriver = null;
        private By myBy = null;
        private oelement myParent = null;

        #endregion End - Private variables

        #region Properties
        
        public IWebElement MyElement 
        {
            get { return myElement; }
            set { myElement = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public IWebDriver MyDriver
        {
            get { return myDriver; }
            set { myDriver = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string MyName
        {
            get { return myName; }
            set { myName = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string MyText
        {
            get
            {
                if (MyElement != null)
                {
                    var text = MyElement.GetAttribute("textContent");
                    text = text.Replace("  ", " ");
                    return text;
                }
                else return null;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public By MyBy
        {
            get { return myBy; }
            set { myBy = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public oelement MyParent
        {
            get { return myParent; }
            set { myParent = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Existed
        {
            get 
            {
                if (MyElement != null)
                {
                    if(!this.myName.ToLower().Contains("wait control"))
                        MoveToElement();
                    return MyElement.Displayed;
                } 
                else return false;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool ReadOnly
        {
            get
            {
                if (MyElement != null)
                {
                    string temp = MyElement.GetAttribute("readonly");
                    if (temp != null && (temp.Trim().ToLower() == "true" || temp.Trim().ToLower() == "readonly"))
                        return true;
                    else
                        return false;
                }
                else return false;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string MyType
        {
            get 
            {
                if (myType != null && myType != string.Empty)
                    return myType;
                else
                    return null;
            }
            set { myType = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int MyIndex
        {
            get { return myIndex; }
            set { myIndex = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool MyNowait
        {
            get { return myNowait; }
            set { myNowait = value; }
        }

        #endregion End - Properties

        #region Constructor

        public oelement(string name, IWebDriver driver, IWebElement iwe, [Optional] string type) 
        {
            MyName = name;
            MyDriver = driver;
            MyElement = iwe;
            MyType = type;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public oelement(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait, [Optional] string type)
        {
            MyName = name;
            MyType = type;
            MyDriver = driver;
            MyBy = by;
            MyParent = parent;
            MyElement = GetElement(driver, by, parent, frame, noWait);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public oelement(string name, By by, IWebDriver driver, oelement parent, int index, bool noWait)
        {
            MyName = name;
            MyDriver = driver;
            MyBy = by;
            MyIndex = index;
            MyParent = parent;
            MyNowait = noWait;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public oelement(IWebDriver driver, IWebElement e)
        {
            MyElement = e;
            MyDriver = driver;
        }
        #endregion End - Constructor

        #region Private method

        private IWebElement GetElement(IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait)
        {
            IWebElement iwe = null;
            uint timeout = 20;
            ISearchContext context = driver;
            if (!MyName.ToLower().Contains("parent") && !MyName.ToLower().Contains("|parent"))
                Console.WriteLine("***[Finding]:" + MyName + "|" + by.ToString());

            if (noWait)
                timeout = 5;

            if (SwitchTo(frame))
            {
                if (parent != null)
                {
                    context = parent.MyElement;
                }

                try
                {
                    iwe = FindMyElement(context, by, timeout);
                }
                catch { iwe = null; }
            }
            else 
            {
                Console.WriteLine("***[Error]: Error when switch to frame.");
            }

            if(iwe != null)
                if (!MyName.ToLower().Contains("parent") && !MyName.ToLower().Contains("|parent"))
                    Console.WriteLine("***[Found]:" + MyName);
            else
                if (!MyName.ToLower().Contains("parent") && !MyName.ToLower().Contains("|parent"))
                    Console.WriteLine("***[Not found]:" + MyName);
            if (!MyName.ToLower().Contains("parent") && !MyName.ToLower().Contains("|parent"))
                Console.WriteLine(".................................................................................................................");
            
            return iwe;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private  IWebElement FindMyElement(ISearchContext context, By by, uint timeout)
        {
            var wait = new DefaultWait<ISearchContext>(context);
            wait.Timeout = TimeSpan.FromSeconds(timeout);
            wait.PollingInterval = TimeSpan.FromSeconds(1);
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ctx =>
            {
                var elem = ctx.FindElement(by);
                return elem;
            });
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool SwitchTo(string frame) 
        {
            bool flag = true;

            try
            {
                MyDriver.SwitchTo().DefaultContent();
            }
            catch { flag = false; }

            if (frame != null && frame != string.Empty && flag)
            {
                string[] arr = null;

                if (frame.Contains(";"))
                {
                    arr = frame.Split(';');
                }
                else
                {
                    arr = new string[] { frame };
                }

                bool flagT = true;

                foreach (string fr in arr)
                {
                    try
                    {
                        MyDriver.SwitchTo().Frame(fr);
                    }
                    catch { flagT = false; }

                    if (!flagT && flag)
                        flag = false;
                }
            }

            return flag;
        }

        #endregion End - Private method

        #region Public methods

        public bool Click([Optional] bool javaClick)
        {
            bool flag = true;
            Console.WriteLine("***[Call click]");
            if (MyElement != null)
            {
                try
                {
                    if (MyDriver != null && javaClick)
                    {
                        IJavaScriptExecutor js = MyDriver as IJavaScriptExecutor;
                        js.ExecuteScript("arguments[0].click();", MyElement);
                    }
                    else
                    {
                        MyElement.Click();
                    }
                    Console.WriteLine("***[Click ok]");
                    Console.WriteLine(".................................................................................................................");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("***[ERROR]:" + ex.Message);
                    Console.WriteLine(".................................................................................................................");
                    flag = false;
                }
            }
            else 
            {
                Console.WriteLine("***[ERROR]: Control is NULL");
                Console.WriteLine(".................................................................................................................");
                flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public void Highlight()
        {
            if (MyElement != null && MyElement.GetAttribute("class") != "context_menu" && MyElement.TagName != "label")
            {
                try
                {
                    IJavaScriptExecutor js = this.myDriver as IJavaScriptExecutor;
                    js.ExecuteScript("arguments[0].setAttribute('style', 'border-width: 1px; border-style: solid; border-color: lightblue')", MyElement);
                }
                catch { }
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public void MoveToElement() 
        {
            try
            {
                Actions ac = new Actions(MyDriver);
                ac.MoveToElement(MyElement);
                ac.Perform();
            }
            catch { }
        }
        #endregion End - Public methods

    }
}
