﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.Configuration;
using NUnit.Framework;
using ExcelReader;
using Spire.Xls;
using System.Text;
using System.Security.Cryptography;
using System.Data;
using System.Drawing.Imaging;
using System.Reflection;
using System.Threading;

namespace Auto
{
    public class obase
    {
        #region Variables

        public static string BrowserType = string.Empty;
        public static int CurrentWindowIndex = 0;
        public static string MainFrame = string.Empty;

        private const uint PAGETIMEOUT = 120;
        private Dictionary<string, string> dict = new System.Collections.Generic.Dictionary<string, string>();
        private Stopwatch swStep, swCase;
        private string _browserFolderPath = string.Empty;
        private string _profileFolderPath = string.Empty;
        private string _testFolderPath = string.Empty;
        private string _tempFolderPath = string.Empty;
        private string _useGlobalPass = string.Empty;
        private string _caseName = string.Empty;
        private IWebDriver _driver = null;

        private string readType = string.Empty;
        private string row_config = string.Empty;
        private string evr_config = string.Empty;

        #endregion End - Variables

        #region Properties

        public string BrowserFolderPath
        {
            get { return _browserFolderPath; }
            set { _browserFolderPath = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string ProfileFolderPath
        {
            get { return _profileFolderPath; }
            set { _profileFolderPath = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string TestFolderPath
        {
            get { return _testFolderPath; }
            set { _testFolderPath = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string TempFolderPath
        {
            get { return _tempFolderPath; }
            set { _tempFolderPath = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string UseGlobalPass
        {
            get { return _useGlobalPass; }
            set { _useGlobalPass = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public IWebDriver Driver
        {
            get { return _driver; }
            set { _driver = value; }
        }
        
        #endregion End - Properties

        #region NUnit setup functions
        
        public void BeforeRunTestCase(string caseName, ref obase Base, ref bool flagExit, ref bool flagW, ref bool flag, ref bool flagC, [Optional] string sRow)
        {
            flagExit = true;
            flagW = true;
            flag = true;
            flagC = true;

            string startUpPath = string.Empty;
            string dataFilePath = string.Empty;
            string error = string.Empty;
            string debugMode = string.Empty;
            string headless = string.Empty;
            try
            {
                _caseName = caseName;
                swCase = Stopwatch.StartNew();

                Console.WriteLine("[Run test case - " + caseName + "]");
                Console.WriteLine("*****************************************************************************************************************");
                Console.WriteLine("[Start - Init]");
                //--------
                var appConfig = ConfigurationManager.OpenExeConfiguration("AutoBase.dll");
                readType = appConfig.AppSettings.Settings["read_data"].Value;
                row_config = appConfig.AppSettings.Settings["row_config"].Value;
                evr_config = appConfig.AppSettings.Settings["evr_config"].Value;
                //--------
                startUpPath = Application.StartupPath;
                TestFolderPath = startUpPath.Replace(@"\NUnit 2.6.3\bin", "");

                if (readType.ToLower() == "xml")
                    dataFilePath = TestFolderPath + @"\Data\" + caseName + ".xml";
                else
                    dataFilePath = TestFolderPath + @"\Data\" + caseName + ".xlsx";

                ProfileFolderPath = TestFolderPath + @"\Profile\";
                TempFolderPath = TestFolderPath + @"\Temp\";

                if (File.Exists(dataFilePath) == true)
                {
                    Base.LoadDictionary(dataFilePath, sRow);
                    BrowserType = Base.GData("Type").ToLower();
                    debugMode = Base.GData("Debug");
                    UseGlobalPass = Base.GData("UseGlobalPass");
                    headless = Base.GData("Headless");

                    flag = Base.GetDriver(BrowserType, debugMode, headless);

                    if (flag == false)
                    {
                        error = "Cannot get driver.";
                    }
                }
                else
                {
                    flag = false;
                    error = "Data file [" +  dataFilePath + "] not found.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

            if (flag == false)
            {
                Console.WriteLine("*** Data file path: " + dataFilePath);
                Console.WriteLine("*** Browser Type: " + BrowserType);
                Console.WriteLine("***[End - Init] - ERROR: " + error);
                Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            }
            else
            {
                Console.WriteLine("*** Data file path: " + dataFilePath);
                Console.WriteLine("*** Browser Type: " + BrowserType);
                Console.WriteLine("[End - Init] - OK");
                Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public void BeforeRunTestStep(ref bool flag, ref bool flagExit, ref string error)
        {
            swStep = Stopwatch.StartNew();

            if (flag == false && flagExit == true)
            {
                Assert.Ignore("Previous step had critical error, Ignore this step.");
            }
            else
            {
                flag = true;
                flagExit = true;
                error = string.Empty;
            }
        }

        #endregion End - NUnit setup functions

        #region NUnit tearDown functions
        
        public void AfterRunTestStep(bool flag, ref bool flagExit, ref bool flagW, ref bool flagC, string error, [Optional] string mess)
        {
            swStep.Stop();
            TimeSpan seconds = TimeSpan.FromSeconds(swStep.Elapsed.TotalSeconds);
            string time = seconds.ToString(@"hh\:mm\:ss");

            if (error.ToLower() == "warning" && flag)
            {
                Assert.Inconclusive();
            }
            else
            {
                if (flag == false && flagC == true)
                {
                    flagC = false;
                }

                if (flag == true)
                {
                    Console.WriteLine("RESULT: PASSED | ELAPSED TIME: " + time);
                    Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
                    if (mess != null)
                        Assert.Pass();
                    else
                        Assert.Pass(mess);
                }
                else
                {
                    if (flagExit == false)
                    {
                        //-- Take Screenshot failed step
                        //try
                        //{
                        //    var temp = GData("SaveErrorPicture");
                        //    if (temp.ToLower() == "yes")
                        //    {
                        //        SaveErrorPicture();
                        //    }
                        //}
                        //catch { }
                        Console.WriteLine("RESULT: FAILED | ELAPSED TIME: " + time + " | ERRORS: " + error);
                        Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");

                        Assert.Fail(error);
                    }
                    else
                    {
                        if (flagW == true)
                        {
                            ////-- Take Screenshot failed step
                            //try
                            //{
                            //    var temp = GData("SaveErrorPicture");
                            //    if (temp.ToLower() == "yes")
                            //    {
                            //        SaveErrorPicture();
                            //    }
                            //}
                            //catch { }
                            Console.WriteLine("RESULT: FAILED | ELAPSED TIME: " + time + " | ERRORS: " + error);
                            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");

                            flagW = false;

                            Assert.Fail(error);
                        }
                        else
                        {
                            Console.WriteLine("RESULT: IGNORE");
                            Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
                            Assert.Ignore();
                        }
                    }
                }
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public void AfterRunTestCase(bool flagC, string caseName)
        {
            swCase.Stop();

            TimeSpan seconds = TimeSpan.FromSeconds(swCase.Elapsed.TotalSeconds);
            string time = seconds.ToString(@"hh\:mm\:ss");

            Console.WriteLine("[End test case - " + caseName + "]");

            if (flagC == true)
            {
                Console.WriteLine("RESULT: PASSED | ELAPSED TIME: " + time);
                Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            }
            else
            {
                Console.WriteLine("RESULT: FAILED | ELAPSED TIME: " + time);
                Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            }

            Console.WriteLine("*****************************************************************************************************************");
        }

        //***********************************************************************************************************************************
        #endregion End - NUnit tearDown functions

        #region Switch page method
        //***********************************************************************************************************************************

        /// <summary>
        /// Switch to page with index
        /// </summary>
        /// <param name="windowIndex">Start with 0</param>
        /// <returns>none</returns>
        public bool SwitchToPage(int windowIndex, [Optional] bool maximize)
        {
            int windowCount, count;
            bool flag = true;

            if (Driver != null)
            {
                try
                {
                    windowCount = Driver.WindowHandles.Count;

                    count = 0;

                    //-- wait if need
                    while ((windowCount < (windowIndex + 1)) && count < 5)
                    {
                        System.Threading.Thread.Sleep(2000);
                        windowCount = Driver.WindowHandles.Count;
                        count = count + 1;

                    }

                    System.Console.WriteLine("Window count: " + windowCount);

                    if (windowCount >= windowIndex + 1)
                    {
                        Driver.SwitchTo().Window(Driver.WindowHandles[windowIndex]);
                        if (maximize)
                            Driver.Manage().Window.Maximize();
                        CurrentWindowIndex = windowIndex;
                    }
                    else
                    {
                        System.Console.WriteLine("***WARN:Index out of range.");
                    }
                }
                catch { flag = false; CurrentWindowIndex = 0; }
            }
            else
            {
                flag = false;
            }

            return flag;
        }

        //***********************************************************************************************************************************
        #endregion End - Switch window page method

        #region Get data method

        public string GData(string columnName)
        {
            string temp = string.Empty;
            Console.WriteLine("[GData] - <" + columnName + ">");
            try
            {
                temp = dict[columnName.ToLower()];
            }
            catch (Exception ex)
            {
                Console.WriteLine("[GData] - ERRORS: <" + ex.Message + ">");
            }

            Console.WriteLine("[GData] - RESULT: <" + temp + ">");
            Console.WriteLine(".................................................................................................................");

            return temp;
        }

        #endregion End - Get data method

        #region Dictionary method

        private void LoadD_Dll(string path, string srow)
        {
            string domain;
            int iRow, i;
            i = 0;

            domain = xlReader.ReadAllDataInColumn("Domain", "Config", path)[0].Trim();

            if (srow == null || srow == string.Empty)
            {
                if (row_config.Trim() == string.Empty)
                {
                    iRow = Convert.ToInt32(xlReader.ReadAllDataInColumn("Row", "Config", path)[0].ToString().Trim());
                }
                else
                {
                    iRow = Convert.ToInt32(row_config);
                }
            }
            else
            {
                iRow = Convert.ToInt32(srow);
            }

            string[] array = xlReader.ReadAllDataBetweenRows(0, iRow, domain, path);
            string[] ColumnHeaders = array[0].Split('~');
            string[] ColumnValues = array[iRow].Split('~');

            foreach (string colH in ColumnHeaders)
            {
                string value = string.Empty;

                if (colH.Trim().ToLower() == "url" && evr_config != string.Empty)
                    value = evr_config.Trim();
                else
                    value = ColumnValues[i].Trim();

                dict.Add(colH.Trim().ToLower(), value);
                i++;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private void LoadDictionary(string path, string srow)
        {
            try
            {
                switch (readType.Trim().ToLower())
                {
                    case "nodll":
                        break;
                    case "xml":
                        break;
                    default:
                        LoadD_Dll(path, srow);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion End - Dictionary method

        #region Driver methods

        public bool GetDriver(string browserType, string debugMode, [Optional] string headless)
        {
            bool flag = true;
            
            switch (browserType.ToLower())
            {
                case "ff":
                    flag = Get_FF_Driver(debugMode);
                    break;
                case "chr":
                    flag = Get_CHR_Driver(TestFolderPath + @"\", debugMode, headless);
                    break;
                case "ie":
                    flag = Get_IE_Driver(TestFolderPath + @"\", debugMode);
                    break;
            }

            if (flag == true)
            {
                Console.WriteLine("[GetDriver] - OK");
            }
            else
            {
                Console.WriteLine("[GetDriver] - ERROR: Cannot get driver.");
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Get_FF_Driver(string debugMode)
        {
            bool flag = true;
            System.Uri uri;
            try
            {
                if (debugMode != null && debugMode == "yes")
                {
                    uri = new System.Uri("http://localhost:7055/hub");
                    Driver = new RemoteWebDriver(uri, DesiredCapabilities.Firefox());

                    if (Driver != null)
                    {
                        Console.WriteLine("[Driver] - <FireFox> - Existed");
                        //-- Maximize window
                        Driver.Manage().Window.Maximize();
                        //-- Set time out for page load
                        Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(PAGETIMEOUT));
                    }
                    else { flag = false; }
                }
                else
                {
                    var str = Path.GetTempPath();
                    Console.WriteLine("***[Temp path] " + str);
                    string[] arr = Directory.GetDirectories(str);

                    foreach (string s in arr)
                    {
                        if ((s.Contains("anonymous") && s.Contains("webdriver-profile")) || (s.Contains("rust_mozprofile")))
                        {
                            Console.WriteLine("Delete folder *** [" + s + "]");
                            try
                            {
                                Directory.Delete(s, true);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }

                        }
                    }

                    FirefoxProfile profile = new FirefoxProfile();

                    profile.AddExtension(ProfileFolderPath + "firebug@software.joehewitt.com.xpi");
                    profile.AddExtension(ProfileFolderPath + "FireXPath@pierre.tholence.com.xpi");
                    profile.SetPreference("extensions.firebug.showFirstRunPage", false);

                    Driver = new FirefoxDriver(new FirefoxBinary() { Timeout = TimeSpan.FromSeconds(120) }, profile);

                    if (Driver != null)
                    {
                        Console.WriteLine("[Driver] - <FireFox> - New");
                        //-- Clear cache
                        ClearCache();
                        //-- Maximize window
                        Driver.Manage().Window.Maximize();
                        //-- Set time out for page load
                        Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(PAGETIMEOUT));
                    }
                    else { flag = false; }
                }

                return flag;
            }
            catch
            {
                var str = Path.GetTempPath();
                Console.WriteLine("***[Temp path] " + str);
                string[] arr = Directory.GetDirectories(str);

                foreach (string s in arr)
                {
                    if ((s.Contains("anonymous") && s.Contains("webdriver-profile")) || (s.Contains("rust_mozprofile")))
                    {
                        Console.WriteLine("Delete folder *** [" + s + "]");
                        try
                        {
                            Directory.Delete(s, true);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                    }
                }

                FirefoxProfile profile = new FirefoxProfile();

                profile.AddExtension(ProfileFolderPath + "firebug@software.joehewitt.com.xpi");
                profile.AddExtension(ProfileFolderPath + "FireXPath@pierre.tholence.com.xpi");
                profile.SetPreference("extensions.firebug.showFirstRunPage", false);

                Driver = new FirefoxDriver(new FirefoxBinary() { Timeout = TimeSpan.FromSeconds(120) }, profile);

                if (Driver != null)
                {
                    Console.WriteLine("[Driver] - <FireFox> - New");
                    //-- Clear cache
                    ClearCache();
                    //-- Maximize window
                    Driver.Manage().Window.Maximize();
                    //-- Set time out for page load
                    Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(PAGETIMEOUT));
                }
                else { flag = false; }
                return flag;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Get_IE_Driver(string path, string debugMode)
        {
            bool flag = true;
            try
            {
                if (Environment.Is64BitOperatingSystem == true)
                {
                    path = path + @"W64";
                }
                else
                {
                    path = path + @"W32";
                }

                Console.WriteLine("[Get_IE_Driver] - <" + path + ">");

                var pr = Process.GetProcessesByName("IEDriverServer");

                if (pr.Length > 0 && debugMode != null && debugMode == "yes")
                {
                    MyRemoteWebDriver.newSession = false;

                    var configuration = ConfigurationManager.OpenExeConfiguration("AutoBase.dll");
                    var url = configuration.AppSettings.Settings["ie_url"].Value;

                    Driver = new MyRemoteWebDriver(new Uri(url), DesiredCapabilities.InternetExplorer());

                    Console.WriteLine("***-- Existed Driver.");
                    Console.WriteLine(((RemoteWebDriver)Driver).SessionId);
                }
                else
                {
                    var options = new InternetExplorerOptions();

                    options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;

                    Driver = new InternetExplorerDriver(path.Trim(), options, TimeSpan.FromSeconds(120));

                    var url = Driver.Url.ToString();
                    var session = ((RemoteWebDriver)Driver).SessionId;

                    var configuration = ConfigurationManager.OpenExeConfiguration("AutoBase.dll");
                    configuration.AppSettings.Settings["ie_url"].Value = url;
                    configuration.AppSettings.Settings["ie_session"].Value = session.ToString();
                    configuration.Save();

                    ConfigurationManager.RefreshSection("appSettings");

                    Console.WriteLine("***-- New Driver.");
                    Console.WriteLine("Url:" + url);
                    Console.WriteLine("Session Id:" + session.ToString());
                    //-- Clear cache
                    ClearCache();
                }

                //-------------------------------------------------------------------------------------

                if (Driver != null)
                {
                    Driver.Manage().Window.Maximize();
                    //-- Set time out for page load
                    Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(PAGETIMEOUT));
                }
                else
                {
                    flag = false;
                }
                return flag;
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return false;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Get_CHR_Driver(string path, string debugMode, [Optional] string headless)
        {
            bool flag = true;
            try
            {
                if (Environment.Is64BitOperatingSystem == true)
                {
                    path = path + @"W64";
                }
                else
                {
                    path = path + @"W32";
                }

                Console.WriteLine("[G_CHR_Driver] - <" + path + ">");
                BrowserFolderPath = path;

                var pr = Process.GetProcessesByName("ChromeDriver");
                bool isnewdriver = true;
                if (pr.Length > 0 && debugMode != null && debugMode.ToLower() == "yes")
                {
                    isnewdriver = false;
                    MyRemoteWebDriver.newSession = false;
                    var configuration = ConfigurationManager.OpenExeConfiguration("AutoBase.dll");
                    var url = configuration.AppSettings.Settings["chr_url"].Value;

                    Driver = new MyRemoteWebDriver(new Uri(url), DesiredCapabilities.Chrome());

                    Console.WriteLine("***-- Existed Driver.");
                    Console.WriteLine(((RemoteWebDriver)Driver).SessionId);
                }
                else
                {
                    var str = Path.GetTempPath();
                    Console.WriteLine("///*** " + str);
                    string[] arr = Directory.GetDirectories(str);

                    foreach (string s in arr)
                    {
                        if (s.Contains("chrome_url_fetcher") || s.Contains("scoped"))
                        {
                            Console.WriteLine("Delete folder *** [" + s + "]");
                            try
                            {
                                Directory.Delete(s, true);

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }

                        }
                    }

                    

                    var options = new ChromeOptions();
                    
                    options.AddArgument("--start-maximized");
                    options.AddExcludedArgument("enable-automation");
                    options.AddAdditionalCapability("useAutomationExtension", false);
                    options.AddArgument("--ignore-certificate-errors");
                    options.AddArgument("--ignore-ssl-errors");
                    options.AddArgument("--disable-images");
                    options.AddExcludedArgument("enable-automation");
                    options.AddAdditionalCapability("useAutomationExtension", false);
                    if(headless != null && headless.ToLower() == "yes")
                        options.AddArgument("headless");
                    options.AddUserProfilePreference("credentials_enable_service", false);
                    options.AddUserProfilePreference("profile.password_manager_enabled", false);
                    options.Proxy = null;
                    
                    ChromeDriverService crService = ChromeDriverService.CreateDefaultService(path.Trim());
                    //crService.Port = 55560;
                    Driver = new ChromeDriver(crService, options, TimeSpan.FromSeconds(120));


                    var url = "http://localhost:" + crService.Port + "/";
                    var session = ((RemoteWebDriver)Driver).SessionId;

                    var configuration = ConfigurationManager.OpenExeConfiguration("AutoBase.dll");
                    configuration.AppSettings.Settings["chr_url"].Value = url;
                    configuration.AppSettings.Settings["chr_session"].Value = session.ToString();
                    configuration.Save();

                    ConfigurationManager.RefreshSection("appSettings");

                    Console.WriteLine("***-- New Driver.");
                    Console.WriteLine("Url:" + url);
                    Console.WriteLine("Session Id:" + session.ToString());
                    //-- Clear cache
                    ClearCache();
                }

                //-------------------------------------------------------------------------------------

                if (Driver != null)
                {
                    if(isnewdriver)
                        Driver.Manage().Window.Maximize();
                    //-- Set time out for page load
                    Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(PAGETIMEOUT));
                }
                else
                {
                    flag = false;
                }
                return flag;
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                return false;
            }
        }

        #endregion End - Get driver method

        #region Window form method

        public bool WFileUploadSelect(string filename)
        {
            bool flag = true;

            try
            {
                string fullPath = TempFolderPath + filename;
                if (File.Exists(fullPath))
                {
                    AutoItX3Lib.AutoItX3 ait = new AutoItX3Lib.AutoItX3();
                    string title = string.Empty;
                    string browser = GData("Type");
                    if (browser.ToLower() == "ff")
                        title = "File Upload";
                    else if (browser.ToLower() == "chr")
                        title = "Open";
                    Thread.Sleep(5000);
                    ait.WinActive(title, "");
                    ait.WinActivate(title, "");
                    ait.WinWaitActive(title, "", 20);
                    ait.WinActive(title, "");
                    ait.WinActivate(title, "");
                    Thread.Sleep(2000);
                    ait.ControlSetText(title, "", "[CLASS:Edit; INSTANCE:1]", fullPath);
                    Thread.Sleep(2000);
                    if (browser.ToLower() == "chr")
                        SendKeys.SendWait("%{O}");
                    ait.ControlClick(title, "", "[CLASS:Button; INSTANCE:1]");
                    if (browser.ToLower() == "chr")
                        SendKeys.SendWait("%{O}");
                    Thread.Sleep(2000);
                    ait = null;
                }
                else
                {
                    flag = false;
                    Console.WriteLine("***[ERROR]: Not found file (" + fullPath + ")");
                }
            }
            catch (Exception e)
            {
                flag = false;
                Console.WriteLine(e.Message);
            }

            //-----------------------------------------------------------------------------------------------------------

            if (flag == true)
            {
                Console.WriteLine("***[OK]: File upload selected.");
            }

            Console.WriteLine(".......................................................................................");

            return flag;
        }

        public void ActiveWindow()
        {
            IJavaScriptExecutor jscript = Driver as IJavaScriptExecutor;
            jscript.ExecuteScript("alert('Test')");
            Driver.SwitchTo().Alert().Accept();
        }
        #endregion End - Window form method

        #region Clear cache method

        public void ClearCache()
        {
            if (Driver == null) return;

            Driver.Manage().Cookies.DeleteAllCookies();

            if (Driver.GetType() == typeof(RemoteWebDriver) || Driver.GetType() == typeof(InternetExplorerDriver) || Driver.GetType() == typeof(FirefoxDriver) || Driver.GetType() == typeof(ChromeDriver))
            {
                ProcessStartInfo psInfo = new ProcessStartInfo();
                psInfo.FileName = Path.Combine(Environment.SystemDirectory, "RunDll32.exe");
                psInfo.Arguments = "InetCpl.cpl, ClearMyTracksByProcess 2";
                psInfo.CreateNoWindow = true;
                psInfo.UseShellExecute = false;
                Process p = new Process { StartInfo = psInfo };
                p.Start();
                p.WaitForExit(10000);
            }
        }

        #endregion End - Clear cache method

        #region Decrypt function

        public string DecryptString(string cipherText)
        {

            string initVector = "hoanhth2uhoanh44";
            // This constant is used to determine the keysize of the encryption algorithm.
            int keysize = 256;
            string passPhrase = "tho25";

            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null);
            byte[] keyBytes = password.GetBytes(keysize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        #endregion End - Decrupt function

        #region Excel function

        public bool CreateExcelFile(string path, List<string> columns, List<List<string>> rows) 
        {
            bool flag = true;
            try
            {
                Workbook workbook = new Workbook();
                Worksheet worksheet = workbook.Worksheets[0];
                //-- Add columns
                int i = 1;
                foreach (string col in columns)
                {
                    worksheet.Range[1, i].Value = col;
                    i++;
                }
                //-- Add rows
                int j = 2;
                foreach (List<string> row in rows) 
                {
                    i= 1;
                    foreach (string cell in row) 
                    {
                        worksheet.Range[j, i].Value = cell;
                        i++;
                    }
                    j++;
                }
                //-- Save
                workbook.SaveToFile(path, ExcelVersion.Version2010);
            }
            catch { flag = false; }

            return flag;
        }

        #endregion End - Excel funtion

        #region Save error picture

        public void SaveErrorPicture([Optional] string cuspath)
        {
            var folderPath = TempFolderPath + "ErrorPic\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + _caseName;
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var stepname = TestContext.CurrentContext.Test.Name;
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            var path = folderPath + "\\" + DateTime.Now.ToString("HHmmss") + "_" + stepname + ".png";
            if(cuspath == null)
                screenshot.SaveAsFile(path, ImageFormat.Png);
            else
                screenshot.SaveAsFile(cuspath, ImageFormat.Png);
        }

        #endregion End - Save error picture

        #region Command function

        public bool StringCompare(string str1, string str2) 
        {
            bool flag = false;

            if (str1.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD) == str2.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD))
            {
                flag = true;
            }
            
            return flag;
        }

        #endregion Command function

        #region Other function

        public bool IsDllRegistered()
        {
            bool flag = false;
            try
            {
                AutoItX3Lib.AutoItX3 ait = new AutoItX3Lib.AutoItX3();
                flag = true;
            }
            catch
            {
                flag = false;
            }

            return flag;
        }

        #endregion End - Other function
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #region MyRemoteWebDriver class

    public class MyRemoteWebDriver : RemoteWebDriver
    {
        public static bool newSession = false;

        public MyRemoteWebDriver(Uri remoteAddress, DesiredCapabilities dd)
            : base(remoteAddress, dd)
        { }
        
        protected override Response Execute(string driverCommandToExecute, Dictionary<string, object> parameters)
        {
            try
            {
                Thread.Sleep(10);
            }
            catch { }

            if (driverCommandToExecute == DriverCommand.NewSession)
            {
                if (!newSession)
                {
                    string sidText = string.Empty;
                    var configuration = ConfigurationManager.OpenExeConfiguration("AutoBase.dll");
                    switch (obase.BrowserType.ToLower())
                    {
                        case "ie":
                            sidText = configuration.AppSettings.Settings["ie_session"].Value;
                            break;
                        case "chr":
                            sidText = configuration.AppSettings.Settings["chr_session"].Value;
                            break;
                    }
                    return new Response
                    {
                        SessionId = sidText,
                    };
                }
                else
                {
                    var response = base.Execute(driverCommandToExecute, parameters);
                    return response;
                }
            }
            else
            {
                var response = base.Execute(driverCommandToExecute, parameters);
                return response;
            }
        }
    }

    #endregion End - MyRemoteWebDriver class

}


