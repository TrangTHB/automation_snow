﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Runtime.InteropServices;
using System.Threading;

namespace Auto
{
    public class obutton : oelement
    {
        #region Constructor

        public obutton(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }

        public obutton(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait) 
            : base(name, driver, by, parent, frame, noWait) { }

        #endregion End - Constructor

        #region Properties

        public string Text 
        {
            get
            {
                if (MyElement != null)
                    return MyText;
                else return null;
            }
        }
        
        #endregion End - Properties

        #region Public methods

        #endregion End - Public methods
    }
}
