﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Text.RegularExpressions;

namespace Auto
{
    public class otable : oelement
    {
        private string _frame = string.Empty;
        //***********************************************************
        private string _columnDefine = string.Empty;
        private string _columnAttribute = string.Empty;
        private string _columnTextReplace = string.Empty;
        private string _rowDefine = string.Empty;
        private string _cellDefine = string.Empty;
        //***********************************************************
        private bool getNoDisplay = false;

        #region Constructor

        public otable(string name, IWebDriver driver, IWebElement iwe, [Optional] string frame) 
            : base(name, driver, iwe) 
        {
            _frame = frame;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public otable(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait) 
            : base(name, driver, by, parent, frame, noWait)
        { 
            _frame = frame;
        }

        #endregion End - Constructor

        #region Properties

        public string ColumnDefine
        {
            get { return _columnDefine; }
            set { _columnDefine = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string RowDefine
        {
            get { return _rowDefine; }
            set { _rowDefine = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string CellDefine
        {
            get { return _cellDefine; }
            set { _cellDefine = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int ColumnCount 
        {
            get 
            {
                if (ColumnDefine != null && ColumnDefine != string.Empty)
                {
                    List<string> cols = GColumns();
                    return cols.Count;
                }
                else
                    return -1;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int RowCount
        {
            get
            {
                if (RowDefine != null & RowDefine != string.Empty)
                {
                    oelements rows = GRows();
                    return rows.MyList.Count;
                }
                else
                    return -1;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public List<string> ColumnHeader
        {
            get
            {
                return GColumns();
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public oelements RowsList
        {
            get
            {
                return GRows();
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string ColumnAttribute
        {
            get { return _columnAttribute; }
            set { _columnAttribute = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string ColumnTextReplace
        {
            get { return _columnTextReplace; }
            set { _columnTextReplace = value; }
        }

        public bool GetNoDisplay 
        {
            get { return getNoDisplay; }
            set { getNoDisplay = value; }
        }

        public string Frame
        {
            get { return _frame; }
            set { _frame = value; }
        }
        #endregion End - Properties

        #region Private methods

        private List<string> GColumns() 
        {
            List<string> columns = new List<string>();

            if (_columnDefine != null && _columnDefine != string.Empty) 
            {
                try { Refresh(); }
                catch { }                
                
                By by = By.CssSelector(_columnDefine);
                oelements cols = new oelements("Columns", MyDriver, by, this, _frame, getNoDisplay);

                foreach (oelement col in cols.MyList)
                {
                    string temp = string.Empty;
                    if (_columnAttribute == null || _columnAttribute == string.Empty)
                    {
                        temp = col.MyText.Trim();
                    }
                    else
                    {
                        if (col.MyElement.GetAttribute(_columnAttribute) != null)
                            temp = col.MyElement.GetAttribute(_columnAttribute).Trim();
                        else temp = col.MyElement.Text;
                        if (_columnTextReplace != string.Empty && _columnTextReplace != null)
                        {
                            string[] arr = null;
                            if (_columnTextReplace.Contains(";"))
                                arr = _columnTextReplace.Split(';');
                            else
                                arr = new string[] {_columnTextReplace};
                            
                            foreach (string str in arr) 
                            {
                                temp = temp.Replace(str, "").Trim();
                            }
                        }
                    }

                    columns.Add(temp);
                }
            }
            return columns;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private oelements GRows()
        {
            oelements rows = null;
            if (_rowDefine != null && _rowDefine != string.Empty)
            {
                Refresh();
                By by = By.CssSelector(_rowDefine);
                rows = new oelements("Rows", MyDriver, by, this, _frame);
                
            }
            return rows;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private oelements GCells(oelement row) 
        {
            oelements cells = null;
            if (_cellDefine != null && _cellDefine != string.Empty && row != null)
            {
                By by = By.CssSelector(_cellDefine);
                cells = new oelements("Cells", MyDriver, by, row, _frame);
            }
            return cells;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyRow(List<string> cols, oelements cells, string findCondition) 
        {
            bool flag = false;

            string[] arrCondition;

            if (findCondition.Contains("|"))
            {
                arrCondition = findCondition.Split('|');

                foreach (string condition in arrCondition)
                {
                    flag = Verify(cols, cells, condition);

                    if (!flag)
                    {
                        break;
                    }
                }
            }
            else
            {
                flag = Verify(cols, cells, findCondition);
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool Verify(List<string> cols, oelements cells, string condition)
        {
            bool flag = false;
            string[] arr = condition.Split('=');
            string value = arr[1].Trim();
            int index = cols.IndexOf(arr[0].Trim());
            if (index >= 0)
            {
                if (value.Length > 2 && value.Substring(0, 2).Trim() == "@@")
                {
                    value = value.Substring(2, value.Length - 2);
                    if (cells.MyList[index].MyText.ToLower().Trim().Contains(value.ToLower().Trim()))
                    {
                        cells.MyList[index].Highlight();
                        flag = true;
                    }
                }
                else
                {
                    Regex r = new Regex("[0-9][0-9][0-9][0-9][-][0-9][0-9][-][0-9][0-9] [0-9][0-9][:][0-9][0-9][:][0-9][0-9]");
                    if (r.IsMatch(value))
                    {
                        string str = cells.MyList[index].MyText;
                        
                        if (str.ToLower().Trim().Contains(value.ToLower().Trim()))
                        {
                            cells.MyList[index].Highlight();
                            flag = true;
                        }
                    }
                    else
                    {
                        if (cells.MyList[index].MyText.ToLower().Trim().Equals(value.ToLower().Trim()))
                        {
                            cells.MyList[index].Highlight();
                            flag = true;
                        }
                    }
                    
                }
            }
            return flag;
        }
        
        #endregion End - Private methods

        #region Public methods

        public void Refresh() 
        {
            if(MyParent == null)
                MyElement = MyDriver.FindElement(MyBy);
            else
                MyElement = MyParent.MyElement.FindElement(MyBy);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int ColumnIndex(string columnName, [Optional] List<string> columns) 
        {
            int index = -1;
            string temp;
            bool flagM = true;
            Console.WriteLine("*** Get column index of column:[" + columnName + "]");
            if (columnName != null && columnName != string.Empty) 
            {
                if (columnName.Substring(0, 2) == "@@")
                {
                    temp = columnName.Substring(2, columnName.Length - 2);
                    flagM = false;
                }
                else 
                {
                    temp = columnName;
                }
                List<string> cols = null;

                if (columns == null)
                    cols = GColumns();
                else
                    cols = columns;
                
                foreach (string col in cols) 
                {
                    if (flagM)
                    {
                        if (col.Trim().ToLower() == temp.Trim().ToLower())
                        {
                            index = cols.IndexOf(col);
                            break;
                        }
                    }
                    else 
                    {
                        if (col.Trim().ToLower().Contains(temp.Trim().ToLower()))
                        {
                            index = cols.IndexOf(col);
                            break;
                        }
                    }
                }
            }
            Console.WriteLine("*** Index of column:[" + columnName + "]: " + index);
            return index;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public int RowIndex(string findCondition, [Optional] List<string> columns, [Optional] oelements _rows) 
        {
            int index = -1;
            oelements rows;
            List<string> cols;

            if(columns == null)
                cols = GColumns();
            else
                cols = columns;

            if (_rows == null)
                rows = GRows();
            else
                rows = _rows;

            if (rows.MyList != null && rows.MyList.Count > 0)
            {
                foreach(oelement row in rows.MyList) 
                {
                    oelements cells = GCells(row);
                    
                    if (cells.MyList != null && cells.MyList.Count > 0) 
                    {
                        bool flag = VerifyRow(cols, cells, findCondition);
                        if (flag) 
                        {
                            index = rows.MyList.IndexOf(row);
                            break;
                        }
                    }
                }
            }
            return index;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool CellClick(int rowIndex, int colIndex, string childItemDefine, [Optional] List<string> columns, [Optional] oelements _rows, [Optional] bool javaClick, [Optional] bool doubleClick, [Optional] bool rightClick)
        {
            bool flag = true;
            try
            {
                oelements rows;
                List<string> cols;

                if (rowIndex < 0 || colIndex < 0)
                    return false;

                if (columns == null)
                    cols = GColumns();
                else
                    cols = columns;

                if (_rows == null)
                    rows = GRows();
                else
                    rows = _rows;

                oelement row = rows.MyList[rowIndex];

                oelements cells = GCells(row);
                oelement cell = cells.MyList[colIndex];
                
                flag = cell.Existed;
                
                if (flag)
                {
                    if (doubleClick)
                    {
                        if (javaClick)
                        {
                            string doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',";
                            doubleClickJS = doubleClickJS + "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)";
                            doubleClickJS = doubleClickJS + "{ arguments[0].fireEvent('ondblclick');}";
                            IJavaScriptExecutor js = MyDriver as IJavaScriptExecutor;
                            js.ExecuteScript(doubleClickJS, cell.MyElement);
                        }
                        else
                        {
                            Actions ac = new Actions(MyDriver);
                            ac.MoveToElement(cell.MyElement, 1, 1);
                            ac.DoubleClick();
                            ac.Build().Perform();
                        }
                    }
                    else if (rightClick)
                    {
                        Actions ac = new Actions(MyDriver);
                        ac.MoveToElement(cell.MyElement, 1, 1);
                        ac.ContextClick(cell.MyElement);
                        ac.Build().Perform();
                    }
                    else
                    {
                        if (childItemDefine == null || childItemDefine == string.Empty)
                        {
                            flag = cell.Click(javaClick);
                        }
                        else
                        {
                            By by = By.CssSelector(childItemDefine);
                            oelement child = new oelement("Cell child item", MyDriver, by, cell, _frame);
                            flag = child.Existed;
                            if (flag)
                            {
                                flag = child.Click(javaClick);
                            }
                            else Console.WriteLine("***[INFO]: Cell child item not existed.");
                        }
                    }
                }
                else Console.WriteLine("***[INFO]: Cell not existed.");
            }
            catch { flag = false; }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool CellClick(int colIndex, oelement row)
        {
            bool flag = true;
            try
            {
                oelements cells = GCells(row);
                oelement cell = cells.MyList[colIndex];

                flag = cell.Existed;

                if (flag)
                {
                    cell.Click();
                }
                else Console.WriteLine("***[INFO]: Cell not existed.");
            }
            catch { flag = false; }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string CellText(int rowIndex, int colIndex, [Optional] string childCell)
        {
            string text = null;
            try
            {
                List<string> cols = GColumns();
                oelements rows = GRows();
                oelement row = rows.MyList[rowIndex];
                oelements cells = GCells(row);
                oelement cell = cells.MyList[colIndex];
                if (cell.MyElement != null)
                {
                    if (childCell == null)
                        text = cell.MyText;
                    else
                    {
                        IWebElement child = cell.MyElement.FindElement(By.CssSelector(childCell));
                        if (child != null)
                            text = child.Text;
                    }
                }
            }
            catch { }
            return text;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string CellText(oelement row, int colIndex)
        {
            string text = null;
            try
            {
                oelements cells = GCells(row);
                oelement cell = cells.MyList[colIndex];
                if (cell.MyElement != null)
                {
                    text = cell.MyText;
                }
            }
            catch { }
            return text;
        }
        #endregion End - Public methods
    }
}
