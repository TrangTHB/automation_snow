﻿using System;
using OpenQA.Selenium;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.ObjectModel;

namespace Auto
{
    public class olookup : oelement
    {
        private string _popupDefine = string.Empty;
        private string _popupItemDefine = string.Empty;
        private string _expectedControlDefine = string.Empty;
        private string _unexpectedControlDefine = string.Empty;

        #region Constructor

        public olookup(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }

        public olookup(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait) 
            : base(name, driver, by, parent, frame, noWait) { }

        #endregion End - Constructor

        #region Properties

        public string Text 
        {
            get 
            {
                if (MyElement != null)
                    return MyElement.GetAttribute("value");
                else
                    return null;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string PopupDefine 
        {
            get { return _popupDefine; }
            set { _popupDefine = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string PopupItemDefine
        {
            get { return _popupItemDefine; }
            set { _popupItemDefine = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string ExpectedControlDefine
        {
            get { return _expectedControlDefine; }
            set { _expectedControlDefine = value; }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string UnexpectedControlDefine
        {
            get { return _unexpectedControlDefine; }
            set { _unexpectedControlDefine = value; }
        }
        
        #endregion End - Properties

        #region Public methods

        public bool VerifyCurrentValue(string expValue, [Optional] bool wait) 
        {
            bool flag = true;
            Console.WriteLine("***[Call function]: VerifyCurrentValue");

            if (MyElement != null)
            {
                if (expValue.Substring(0, 2) == "@@")
                {
                    string temp = expValue.Trim().Substring(2);

                    if (wait)
                    {
                        int count = 0;
                        while (!Text.Trim().ToLower().Contains(temp.ToLower()) && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = Text.Trim().ToLower().Contains(temp.ToLower());
                }
                else
                {
                    if (wait)
                    {
                        int count = 0;
                        while (Text.Trim().ToLower() != expValue.Trim().ToLower() && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = (Text.Trim().ToLower() == expValue.Trim().ToLower());
                }

                if (flag)
                    Console.WriteLine("***[Passed] - Runtime [" + Text + "] - Expected [" + expValue + "]");
                else
                    Console.WriteLine("***[FAILED] - Runtime [" + Text + "] - Expected [" + expValue + "]");
            }
            else 
            { 
                flag = false;
                Console.WriteLine("***[ERROR] - Element is NULL");
            }
            
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SetText(string textValue, [Optional] bool pressEnter, [Optional] bool javaClick)
        {
            bool flag = true;
            Console.WriteLine("[Call function]: SetText - Value: [" + textValue + "]");

            if (MyElement != null) 
            {
                try
                {
                    IJavaScriptExecutor js = null;
                    MyElement.Clear();
                    if (javaClick)
                    {
                        js = MyDriver as IJavaScriptExecutor;
                        js.ExecuteScript("arguments[0].click();", MyElement);
                    }
                    else
                    {
                        MyElement.Click();
                    }
                    MyElement.SendKeys(textValue);

                    int count = 0;

                    while (count < 5 && Text != textValue)
                    {
                        Thread.Sleep(1000);

                        MyElement.Clear();
                        if (javaClick)
                        {
                            js.ExecuteScript("arguments[0].click();", MyElement);
                        }
                        else
                        {
                            MyElement.Click();
                        }
                        MyElement.SendKeys(textValue);

                        count++;
                    }

                    Console.WriteLine("-*-[Loop]:(" + count + ") time(s).");

                    if (Text != textValue)
                    {
                        flag = false;
                        Console.WriteLine("-*-[ERROR]: Cannot set text. Current: " + Text + "| Expected:" + textValue);
                    }
                    else
                    {
                        if (pressEnter)
                        {
                            Thread.Sleep(1000);
                            MyElement.SendKeys(Keys.Enter);
                        }
                    }

                    if (flag)
                    {
                        Console.WriteLine("***[Set text ok]");
                    }
                    Console.WriteLine(".................................................................................................................");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("***[ERROR]:" + ex.Message);
                    Console.WriteLine(".................................................................................................................");
                    flag = false;
                }
            }
            else
            {
                Console.WriteLine("***[ERROR]: Control is NULL");
                Console.WriteLine(".................................................................................................................");
                flag = false;
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool Select(string text, [Optional] bool noFrame, [Optional] bool noVerify)
        {
            Console.WriteLine("[Call function]: Select");
            bool flag = true;
            try
            {
                Console.WriteLine("***[Select value]:(" + text + ")");
                this.MoveToElement();
                Thread.Sleep(500);
                MyElement.Clear();
                Click();
                Thread.Sleep(500);
                //-- Switch to current page
                try
                {
                    MyDriver.SwitchTo().Window(MyDriver.WindowHandles[obase.CurrentWindowIndex]);
                    MyDriver.SwitchTo().DefaultContent();
                    if (!noFrame)
                        MyDriver.SwitchTo().Frame(obase.MainFrame);
                }
                catch { Console.WriteLine("***[Info]: Switch page error"); }
                //--------------------------
                oelements items = null;
                By by = null;
                if (PopupWaiting())
                {
                    by = By.CssSelector(PopupItemDefine);
                    items = new oelements("Item list", MyDriver, by, null, obase.MainFrame);

                    if (items.HaveItemsInlist(text, ";", true))
                    {
                        if (obase.BrowserType.ToLower() == "ff")
                            flag = items.ClickOnItem(text, false, true);
                        else
                            flag = items.ClickOnItem(text);
                    }
                    else
                    {
                        SetText(text);
                        Thread.Sleep(2000);
                        if (PopupWaiting())
                        {
                            if (items.HaveItemsInlist(text, ";"))
                            {
                                if (obase.BrowserType.ToLower() == "ff")
                                    flag = items.ClickOnItem(text, false, true);
                                else
                                    flag = items.ClickOnItem(text);
                            }
                            else SetText(text, true);
                        }
                        else SetText(text, true);
                    }
                }
                else
                {
                    SetText(text);
                    Thread.Sleep(2000);
                    if (PopupWaiting())
                    {
                        by = By.CssSelector(PopupItemDefine);
                        items = new oelements("Item list", MyDriver, by, null, obase.MainFrame);

                        if (items.HaveItemsInlist(text, ";"))
                        {
                            if (obase.BrowserType.ToLower() == "ff")
                                flag = items.ClickOnItem(text, false, true);
                            else
                                flag = items.ClickOnItem(text);
                        }
                        else SetText(text, true);
                    }
                    else SetText(text, true);
                }

                //----- Verify -----
                if (!noVerify) 
                {
                    if (!VerifyExpectedControl())
                    {
                        flag = false;
                        Console.WriteLine("***[ERROR]: Invalid refernce.");
                    }

                    if (flag)
                    {
                        Console.WriteLine("***[Select OK]");
                    }
                }
                
                Console.WriteLine(".................................................................................................................");
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("***[ERROR]:" + ex.Message);
                Console.WriteLine(".................................................................................................................");
                flag = false;
            }
            return flag;
        }

        
        #endregion End - Public methods

        #region Private methods

        private bool PopupWaiting()
        {
            bool flag = false;

            By by = By.CssSelector(PopupDefine);
            ReadOnlyCollection<IWebElement> popups = MyDriver.FindElements(by);

            int count = 0;
            while (count < 10 && popups.Count <= 0)
            {
                Thread.Sleep(1000);
                popups = MyDriver.FindElements(by);
                count++;
            }

            if (popups.Count > 0)
            {
                flag = popups[0].Displayed;
            }

            if (flag) Console.WriteLine("-*- Found poup.");
            else Console.WriteLine("-*- NOT Found poup.");

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedControl()
        {
            bool flag = true;
            IWebElement parent = MyParent.MyElement;

            ReadOnlyCollection<IWebElement> eles = parent.FindElements(By.CssSelector(ExpectedControlDefine));
            int count = 0;

            while (count < 5 && eles.Count <= 0)
            {
                Thread.Sleep(1000);
                eles = parent.FindElements(By.CssSelector(ExpectedControlDefine));
                count++;
            }

            if (eles.Count > 0)
            {
                count = 0;
                while (!eles[0].Displayed && count < 5)
                {
                    Thread.Sleep(1000);
                    count++;
                }
                flag = eles[0].Displayed;
            }
            else
            {
                flag = false;
            }

            if (flag)
            {
                Console.WriteLine("-*- Verify Passed.");
            }
            else 
            {
                Console.WriteLine("-*- Verify FAILED.");
            }

            return flag;
        }
        
        #endregion End - Private methods
    }
}
