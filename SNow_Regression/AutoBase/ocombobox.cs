﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Runtime.InteropServices;
using System.Threading;

namespace Auto
{
    public class ocombobox : oelement
    {
        #region Variables
        
        private SelectElement myCombo = null;

        #endregion End - Variables

        #region Constructor

        public ocombobox(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) 
        {
            if (MyElement != null && MyElement.TagName == "select")
                MyCombo = new SelectElement(MyElement);
        }

        public ocombobox(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait) 
            : base(name, driver, by, parent, frame, noWait) 
        {
            if (MyElement != null && MyElement.TagName == "select")
                MyCombo = new SelectElement(MyElement);
        }

        #endregion End - Constructor

        #region Properties

        public SelectElement MyCombo 
        {
            get 
            {
                return myCombo;
            }
            set 
            {
                myCombo = value;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public string Text 
        {
            get
            {
                if (MyCombo != null)
                    try
                    {
                        return MyCombo.SelectedOption.Text;
                    }
                    catch { return null; } 
                else return null;
            }
        }
        #endregion End - Properties

        #region Public methods

        public bool VerifyCurrentValue(string expValue, [Optional] bool wait) 
        {
            bool flag = true;
            Console.WriteLine("***[Call function]: VerifyCurrentValue");

            if (MyElement != null)
            {
                if (expValue.Substring(0, 2) == "@@")
                {
                    string temp = expValue.Trim().Substring(2);

                    if (wait)
                    {
                        int count = 0;
                        while (!Text.Trim().ToLower().Contains(temp.ToLower()) && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = Text.Trim().ToLower().Contains(temp.ToLower());
                }
                else
                {
                    if (wait)
                    {
                        int count = 0;
                        while (Text.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD) != expValue.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD) && count < 10)
                        {
                            Thread.Sleep(1000);
                            count++;
                        }
                    }

                    flag = (Text.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD) == expValue.Trim().ToLower().Normalize(System.Text.NormalizationForm.FormKD));
                }

                if (flag)
                    Console.WriteLine("***[Passed] - Runtime [" + Text + "] - Expected [" + expValue + "]");
                else
                    Console.WriteLine("***[FAILED] - Runtime [" + Text + "] - Expected [" + expValue + "]");
            }
            else 
            { 
                flag = false;
                Console.WriteLine("***[FAILED] - Element is NULL");
            }
            
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SelectItem(string item)
        {
            bool flag = true;
            
            int index = GetItemIndex(item);
            
            if (index < 0)
            {
                flag = false;
            }
            else
            {
                try
                { MyCombo.SelectByIndex(index); }
                catch { }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool SelectItem(int index)
        {
            bool flag = true;

            if (index < 0 )
            {
                flag = false;
            }
            else
            {
                try
                { MyCombo.SelectByIndex(index); }
                catch { }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyExpectedItemsExisted(string items)
        {
            bool flag = true;
            string[] arr = null;
            if (items.Contains(";"))
            {
                arr = items.Split(';');
            }
            else { arr = new string[] { items }; }

            if (arr.Length > 0 && MyCombo.Options.Count <= 0)
            {
                flag = false;
                System.Console.WriteLine("[ERROR]: NOT found any item(s) of combobox.");
            }

            if (flag)
            {
                if (arr.Length != MyCombo.Options.Count)
                {
                    System.Console.WriteLine("[WARNING]: NOT match items count. Expected:(" + arr.Length + "). Runtime:(" + this.MyCombo.Options.Count + ")");
                }
                else System.Console.WriteLine("[Passed]: Match items count. Expected:(" + arr.Length + "). Runtime:(" + this.MyCombo.Options.Count + ")");

                bool flagF = true;
                foreach (string item in arr)
                {
                    flagF = VerifyExpectedItem(item);
                    if (!flagF && flag) { flag = false; }
                }
            }

            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        public bool VerifyActualItemsExisted(string items)
        {
            bool flag = true;
            string[] arr = null;
            if (items.Contains(";"))
            {
                arr = items.Split(';');
            }
            else { arr = new string[] { items }; }

            if (arr.Length > 0 && MyCombo.Options.Count <= 0)
            {
                flag = false;
                System.Console.WriteLine("[ERROR]: NOT found any item(s) of combobox.");
            }
            if (flag)
            {
                if (arr.Length != MyCombo.Options.Count)
                {
                    System.Console.WriteLine("[WARNING]: NOT match items count. Expected:(" + arr.Length + "). Runtime:(" + this.MyCombo.Options.Count + ")");
                }
                else System.Console.WriteLine("[Passed]: Match items count. Expected:(" + arr.Length + "). Runtime:(" + this.MyCombo.Options.Count + ")");

                bool flagF = true;
                foreach (IWebElement ele in MyCombo.Options)
                {
                    string temp = null;
                    temp = ele.Text.Trim();
                    if (temp != null)
                        flagF = VerifyActualItem(temp, arr);
                    else flagF = false;

                    if (!flagF && flag) { flag = false; }
                }
            }
            return flag;
        }

        #endregion End - Public methods

        #region Private methods

        private int GetItemIndex(string item)
        {
            int index = -1;
            bool flagM = true;
            string temp = string.Empty;

            if (item.Contains("@@"))
            {
                temp = item.Substring(2);
                flagM = false;
            }
            else
            {
                temp = item;
            }

            foreach (IWebElement e in MyCombo.Options)
            {
                if (flagM)
                {
                    if (e.Text.ToLower() == temp.ToLower())
                    {
                        index = MyCombo.Options.IndexOf(e);
                        break;
                    }
                }
                else
                {
                    if (e.Text.ToLower().Contains(temp.ToLower()))
                    {
                        index = MyCombo.Options.IndexOf(e);
                        break;
                    }
                }
            }
            if (index == -1) 
            { 
                Console.WriteLine("-*-ERROR: Not found item (" + temp + ") in list. Input param:(" + item + ")"); 
            }
            return index;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyExpectedItem(string item)
        {
            bool flag = false;
            bool flagM = true;

            if (item.Contains("@@"))
            {
                item = item.Substring(2, item.Length - 2);
                flagM = false;
                Console.WriteLine("---finding.@@ <" + item + ">");
            }
            else
            {
                Console.WriteLine("---finding..M <" + item + ">");
            }

            foreach (IWebElement ele in MyCombo.Options)
            {
                string temp = null;
                temp = ele.Text.Trim();
                if (flagM)
                {
                    if (temp != null && temp.ToLower().Equals(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }  
                }
                else
                {
                    if (temp != null && temp.ToLower().Contains(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found] <" + item + "> in actual items.");
            else
                Console.WriteLine("-*-[NOT FOUND] <" + item + "> in actual items.");
            
            return flag;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        private bool VerifyActualItem(string item, string[] arr)
        {
            bool flag = false;

            Console.WriteLine("---finding... <" + item + "> in expected list.");
            foreach (string exp in arr)
            {
                string it= exp;
                if (it.Contains("@@"))
                {
                    it = exp.Substring(2, it.Length - 2);
                    if (item.Trim().ToLower().Contains(it.Trim().ToLower()))
                    {
                        flag = true;
                        Console.WriteLine("-*-[INFO]: Runtime <" + item + "> - Expected: <" + exp + ">");
                        break;
                    }
                }
                else
                {
                    if (it.Trim().ToLower().Equals(item.Trim().ToLower()))
                    {
                        flag = true;
                        break;
                    }
                }
            }

            if (flag)
                Console.WriteLine("-*-[Found] <" + item + "> in expected items.");
            else
                Console.WriteLine("-*-[NOT FOUND] <" + item + "> in expected items.");

            return flag;
        }

        #endregion End - Private methods
    }
}
