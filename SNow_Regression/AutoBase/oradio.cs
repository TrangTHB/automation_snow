﻿using OpenQA.Selenium;
using System.Runtime.InteropServices;

namespace Auto
{
    public class oradio : oelement
    {
        IWebDriver myDriver = null;
        string mainframe;
        public oradio(string name, IWebDriver driver, IWebElement iwe) 
            : base(name, driver, iwe) { }

        public oradio(string name, IWebDriver driver, By by, [Optional] oelement parent, [Optional] string frame, [Optional] bool noWait) 
            : base(name, driver, by, parent, frame, noWait) 
        {
            mainframe = frame;
        }

        #region Properties
        //***********************************************************************************************************************************

        public bool Checked 
        {
            get 
            {
                string check = string.Empty;
                bool flag = true;
                oelement parent = new oelement("Parent", this.myDriver, By.XPath(".."), this, mainframe, true);
                flag = parent.Existed;
                if (flag) 
                {
                    oelement item = new oelement("Item", this.myDriver, By.CssSelector("input"), parent, mainframe);
                    flag = item.Existed;
                    if (flag)
                    {
                       check = item.MyElement.GetAttribute("checked");
                       if (check != null && check == "checked")
                       {flag = true;}
                       else { flag = false; }
                    }
                }

                return flag;
            }
        }

        public string IsReadOnly
        {
            get
            {
                string result = string.Empty;
                oelement parent = new oelement("Parent", this.myDriver, By.XPath(".."), this, mainframe, true);
                bool flag = parent.Existed;
                if (flag)
                {
                    oelement item = new oelement("Item", this.myDriver, By.CssSelector("input"), parent, mainframe);
                    flag = item.Existed;
                    if (flag)
                        result = item.MyElement.GetAttribute("readonly");
                }

                return result;
            }
        }

        

        //***********************************************************************************************************************************
        #endregion End - Properties

    }
}
