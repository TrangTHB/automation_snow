﻿using Auto;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using SNow;

namespace Change
{
    [TestFixture]
    class CHG_normal_bulk_ci_9
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Change Task Id 1: " + changeTaskId_1);
            System.Console.WriteLine("Finished - Change Task Id 2: " + changeTaskId_2);
            System.Console.WriteLine("Finished - Change Task Id 3: " + changeTaskId_3);
            System.Console.WriteLine("Finished - Please note the Postfix: " + postfix);
            //lle34 remove line code
            //System.Console.WriteLine("Finished - Defect number FYI Ticket: DFCT0016251");

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snotextarea textarea;
        
        snodatetime datetime;
        snobutton button;
        
        

        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;
        RiskAssessment riskAss = null;
        Member member = null;
        GlobalSearch globalSearch = null;
        Itil computer = null;
        ItilList computerList = null;
        EmailList emailList = null;
        Email email = null;

        //------------------------------------------------------------------
        string changeId, postfix, changeTaskId_1, changeTaskId_2, changeTaskId_3;
        Dictionary<string, oelement> listOfControl = null;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                globalSearch = new GlobalSearch(Base, "Global search");
                riskAss = new RiskAssessment(Base);
                member = new Member(Base);
                emailList = new EmailList(Base,"Email List");
                email = new Email(Base, "Email");
                computer = new Itil(Base, "Computer");
                computerList = new ItilList(Base, "Computer List");
                //------------------------------------------------------------------
                changeId = string.Empty;
                changeTaskId_1 = string.Empty;
                changeTaskId_2 = string.Empty;
                changeTaskId_3 = string.Empty;
                postfix = string.Empty;
                listOfControl = new Dictionary<string, oelement>();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_001_OpenSystem()
        {
            try
            {
                postfix = DateTime.Now.ToString("yyyy-MM-dd_HHmmss");
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Pre_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_003_Impersonate_AssetManager()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("AssetMgr"));
                if (!flag)
                {
                    error = "Cannot impersonate Asset Manager";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_004_Open_NewComputer()
        {
            try
            {
                // Open  Computer list page
                string temp = Base.GData("Computer_SubClass");
                flag = home.LeftMenuItemSelect("Computer", temp);
                if (flag)
                {
                    computerList.WaitLoading();

                    // Click on New button
                    flag = computerList.Button_New().Existed;
                    if (flag)
                    {
                        flag = computerList.Button_New().Click(true);
                        if (flag)
                            computer.WaitLoading();
                        else
                            error = "Cannot click on New button";
                    }
                    else
                        error = "Cannot get button New";
                }
                else
                    error = "Error when create new Problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_005_Create_NewCI()
        {
            try
            {
                postfix = DateTime.Now.ToString("yyyy-MM-dd_HHmmss");
                error = string.Empty;
                string temp = string.Empty;

                #region Populate Name

                textbox = computer.Textbox_Name_CI();
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("Name") + "_" + postfix);
                    if (!flag)
                    {
                        error += "Cannot populate Name value.";
                    }
                }
                else
                {
                    error += "Cannot get textbox Name.";
                }

                #endregion

                #region Populate Asset tag

                textbox = computer.Textbox_AssetTag();
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("AssetTag") + "_" + postfix);
                    if (!flag)
                    {
                        error += "Cannot populate Asset tag value.";
                    }
                }
                else
                {
                    error += "Cannot get textbox Asset tag.";
                }

                #endregion

                #region Populate Serial Number

                textbox = computer.Textbox_SerialNumber();
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("SerialNumber") + "_" + postfix);
                    if (!flag)
                    {
                        error += "Cannot populate Serial Number value.";
                    }
                }
                else
                {
                    error += "Cannot get textbox Serial Number.";
                }

                #endregion

                #region Populate Company

                lookup = computer.Lookup_Company();
                if (lookup.Existed)
                {
                    flag = lookup.Select(Base.GData("Company"));
                    if (!flag)
                    {
                        error += "Cannot populate Company value.";
                    }
                }
                else
                {
                    error += "Cannot get lookup Company.";
                }

                #endregion

                #region Save Configuration

                flag = computer.Save();
                if (!flag)
                {
                    error += "Error when save Problem.";
                }
                else computer.WaitLoading();
                #endregion

                if (error != "") { flag = false; }

            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Pre_End_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
                login.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_Impersonate_ChangeCoordinator()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("ChangeCoordinator"));
                if (!flag)
                {
                    error = "Cannot impersonate Change Coordinator";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_005_Open_NewChange()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();

                    // Select Change by Type
                    flag = chg.Select_Change_Type(Base.GData("ChangeType"));
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else
                    {
                        error = "Error when select change type.";
                    }
                }
                else
                {
                    error = "Error when create new change.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Populate_AllRequiredFields()
        {
            try
            {
                error = "";
                string temp = "";

                #region Get the Change id

                textbox = chg.Textbox_Number();
                if (textbox.Existed)
                {
                    textbox.Click();

                    //-- Store change id
                    changeId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change Id:(" + changeId + ")");
                }
                else
                {
                    error += "Cannot get the Number textbox";
                }

                #endregion

                #region Populate Company

                lookup = chg.Lookup_Company();
                if (lookup.Existed)
                {
                    flag = lookup.Select(Base.GData("Company"));
                    if (!flag)
                    {
                        error += "Cannot populate Company value.";
                    }
                }
                else
                {
                    error += "Cannot get lookup Company.";
                }

                #endregion

                #region Select Category

                combobox = chg.Combobox_Category();
                if (combobox.Existed)
                {
                    flag = combobox.SelectItem(Base.GData("Category"));
                    if (!flag)
                    {
                        error += "Cannot populate category value.";
                    }
                }
                else
                {
                    error += "Cannot get combobox category.";
                }

                #endregion

                #region Seleec Impact

                combobox = chg.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error += "Cannot populate impact value."; }
                }
                else
                {
                    error += "Cannot get combobox impact.";
                }

                #endregion

                #region Seleec Urgency

                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error += "Cannot populate urgency value."; }
                }
                else
                {
                    error += "Cannot get combobox urgency.";
                }

                #endregion

                #region Select Priority

                temp = Base.GData("Priority");
                combobox = chg.Combobox_Priority();
                if (combobox.Existed)
                {
                    flag = combobox.SelectItem(Base.GData("Priority"));
                    if (!flag)
                    {
                        error += "Cannot populate Priority value.";
                    }
                }
                else
                {
                    error += "Cannot get combobox Priority.";
                }

                #endregion

                #region Populate Short Description

                textbox = chg.Textbox_ShortDescription();
                if (textbox.Existed)
                {
                    flag = textbox.SetText(Base.GData("ShortDescription"));
                    if (!flag)
                    {
                        error += "Cannot populate short description value.";
                    }
                }
                else
                {
                    error += "Cannot get textbox short description.";
                }

                #endregion

                #region Populate Justification
                
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    textarea = chg.Textarea_Justification();
                    if (textarea.Existed)
                    {
                        flag = textarea.SetText(Base.GData("Justification"));
                        if (!flag)
                        {
                            error += "Cannot populate justification value.";
                        }
                    }
                    else
                    {
                        error += "Cannot get textarea justification.";
                    }  
                }
                else
                {
                    error += "Cannot select tab (Planning).";
                }
                #endregion

                #region Populate Planned dates
                
                flag = chg.Select_Tab("Schedule");
                if(flag)
                {
                    string startDate = DateTime.Today.AddDays(4).ToString("yyyy-MM-dd HH:mm:ss");
                    string endDate = DateTime.Today.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    if (datetime.Existed)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            if (datetime.Existed)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag)
                                {
                                    error += "Cannot populate planned end date.";
                                }
                            }
                            else
                            {
                                error += "Cannot get datetime planned end date.";
                            }
                        }
                        else
                        {
                            error += "Cannot populate planned start date.";
                        }
                    }
                    else
                    {
                        error += "Cannot get datetime planned start date.";
                    }
                }
                else
                {
                    error += "Cannot select tab (Planning).";
                }
                #endregion

                #region Save Change

                flag = chg.Save();
                if (!flag)
                {
                    error += "Error when save Change.";
                }

                #endregion

                if (error != "") { flag = false; }

            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_00_Click_RiskAssessment_Tab()
        {
            try
            {
                flag = chg.Select_Tab("Risk Assessment");
                if (!flag)
                {
                    error = "Cannot open (Risk Assessment) tab";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Open_RiskAssessmentForm()
        {
            try
            {
                button = chg.Button_FillOutRiskAssessment();
                flag = button.Existed;
                if (!flag)
                    error = "Cannot get button fill out risk assessment.";
                else
                {
                    flag = button.Click();
                    if (flag)
                    {
                        riskAss.WaitLoading();

                        Thread.Sleep(2000);

                        // Verify Dialog title
                        snoelement ele = riskAss.Title();
                        flag = ele.Existed;
                        if (flag)
                        {
                            flag = ele.MyText.Equals("Risk Assessment");
                            if (!flag)
                            {
                                error = "Invalid title.";
                            }
                        }
                        else
                        {
                            error = "Cannot get label.";
                        }
                    }
                    else error = "Error when click on button fill out risk assessment";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Fill_RiskAssessmentForm()
        {
            try
            {
                flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                if (!flag)
                {
                    error = "Cannot answer Risk Assessment questions";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_Submit_RiskAssessmentForm()
        {
            try
            {
                oelement ele = riskAss.Form_Submit();
                ele.MyElement.Submit();
                Thread.Sleep(2000);
                button = riskAss.Button_Close();
                flag = button.Existed;
                if (flag)
                    button.Click();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Reload_Form()
        {
            try
            {
                flag = chg.ReloadForm();
                if (flag)
                    chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }     
        ////-------------------------------------------------------------------------------------------------
        ////[Test]
        ////public void Step_011_Save_Change()
        ////{
        ////    try
        ////    {
        ////        flag = chg.Save();
        ////        if (!flag)
        ////        {
        ////            error = "Error when save Change.";
        ////        }
        ////    }
        ////    catch (Exception wex)
        ////    {
        ////        flag = false;
        ////        error = wex.Message;
        ////    }
        ////}
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_Select_AssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                if (lookup.Existed)
                {
                    string temp = Base.GData("AssignmentGroup");
                    if (!lookup.VerifyCurrentValue(temp))
                    {
                        flag = lookup.Select(temp);
                        if (!flag)
                        {
                            error += "Cannot populate assignment group value.";
                        }
                    }
                }
                else
                {
                    error += "Cannot get lookup assignment group.";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_PopulateAssignedTo_ChgImplementor()
        {
            try
            {
                string temp = Base.GData("ChangeImplementor");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_SelectCIClass()
        {
            try
            {
                string temp = Base.GData("CIClass01");
                combobox = chg.Combobox_CIClass();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "-*-ERROR: Cannot select value for CI Class field";
                }
                else
                    error = "-*-ERROR: Cannot get CI class combobox";
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Save_Change()
        {
            try
            {
                flag = chg.Save(true);
                if (!flag)
                {
                    error = "Error when save Change.";
                }
                else
                    Thread.Sleep(2000);
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Add_AffectedCI01()
        {
            try
            {
                string temp = Base.GData("AffectedCI_01");
                flag = chg.Add_Related_Members("Affected CIs", temp);
                if (!flag)
                    error = "Cannot open add ci";
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Select_Another_CIClass()
        {
            try
            {
                string temp = Base.GData("CIClass02");
                combobox = chg.Combobox_CIClass();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "-*-ERROR: Cannot select value for CI Class field";
                    else
                    {
                        #region handle Confirmation
                        temp = Base.GData("Confirm_Mess");
                        flag = chg.CHG_VerifyConfirmDialog(temp, "yes");
                        
                        chg.WaitLoading();
                        if (!flag)
                            error = "Some features on the popup is missing or incorrect.";

                        #endregion
                    }
                }
                else
                    error = "-*-ERROR: Cannot get CI class combobox";
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Add_AffectedCI02()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix (Date).");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                string ci = Base.GData("Name") + "_" + postfix;
                flag = chg.Add_Related_Members("Affected CIs", ci);
                if (!flag)
                    error = "Cannot open add ci";
                else chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Select_ProposedChangeField_Description()
        {
            try
            {
                Thread.Sleep(5000);
                var today = DateTime.Now;
                string temp = Base.GData("Proposed_FieldValue");
                flag = chg.SelectField_ProposedChange(temp);

                if (!flag)
                    error = "***ERROR: Cannot select field of Proposed change.";
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Save_AND_Verify_Message()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save Change.";
                else
                {
                    chg.WaitLoading();
                    flag = chg.VerifyMessageInfo("Proposed change data has been applied to the affected CIs");
                    if (!flag)
                        error = "***ERROR: Incorrect info message displays.";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_01_Verify_VerifyProposedChangesButton()
        {
            try
            {
                button = chg.Button_VerifyProposedChanges();
                flag = button.Existed;

                if (!flag)
                    error = "***ERROR: Cannot get verify proposed changes button.";
                else
                    System.Console.WriteLine("***Passed: verify proposed changes button is visible on form.");
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_02_Click_VerifyProposedChangesButton()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == string.Empty || changeId == null))
                {
                    AddParameter addPara = new AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                button = chg.Button_VerifyProposedChanges();
                flag = button.Existed;

                if (!flag)
                    error = "***ERROR: Cannot get verify proposed changes button.";
                else
                {
                    button.Click(true);
                    Thread.Sleep(5000);
                    flag = chg.VerifyMessageInfo("Proposed changes verification passed for change '" + changeId + "'.");
                    if (!flag)
                        error = "***ERROR: Incorrect info message displays.";
                    else
                    {
                        Thread.Sleep(5000);
                        chg.WaitLoading();
                    }
                        
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_1_RequestForReview()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                        chg.WaitLoading();
                    else error = "Error when click on request approval button.";
                }
                else error = "Cannot get button request approval.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_2_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                        chg.WaitLoading();
                    else error = "Error when click on request approval button.";
                }
                else error = "Cannot get button request approval.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_3_Verify_State_Approval()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Approval");
                    if (!flag) error = "Invalid change state value. Expectation: Approval.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_Impersonate_ChangeManager()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("ChangeManager"), true, Base.GData("UserFullName"));
                if (!flag)
                {
                    error = "Cannot impersonate Change Coordinator";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_OpenCIClassList()
        {
            try
            {
                string temp = Base.GData("CIClassName");
                flag = home.LeftMenuItemSelect("Configuration", "Computers");
                if (flag)
                {
                    computerList.WaitLoading();
                }
                else
                    error = "Cannot select left menu item.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_SearchANDOpenAffectedCI02()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix (Date).");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                temp = Base.GData("Name") + "_" + postfix;
                flag = computerList.SearchAndOpen("Name", temp, "Name=" + temp, "Name");
                if (!flag)
                    error = "Cannot search CI name.";
                else computer.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_03_VerifyScheduleChangesSection()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input ChangeId");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                temp = Base.GData("Proposed_FieldValue");
                string[] fieldvalue = temp.Split(';');
                temp = "Scheduled changes;Scheduled changes" + changeId + " scheduled to change fields " + fieldvalue[0] + fieldvalue[0] + ": " + fieldvalue[1];
                flag = computer.VerifyScheduleChangesSection(temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_027_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_to_030_Manager_Override_Approve_Technical()
        {
            try
            {
                string temp = Base.GData("Approval_Technical");
                if (temp.Contains("|"))
                {
                    string[] approver = temp.Split('|');
                    if (approver[0] != "no" && approver[1] != "no")
                    {
                        //Search and open approval form
                        string conditions = "State=Requested|Approver=" + approver[0] + "|Assignment group=" + approver[1];
                        System.Console.WriteLine("*******OPEN and APPROVE record of apprroval group: " + approver[1]);
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + approver[0], conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                            //approve the request
                            button = chg.Button_Approve();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click(true);
                                if (!flag)
                                    error = "Error when click on button approve.";
                                else
                                    chg.WaitLoading();
                            }
                            else
                                error = "Cannot get button approve.";
                        }
                    }
                }
                else
                { flag = false; error = "***ERROR: The format of input data is incorrect. Please validate again."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_to_033_Manager_Override_Approve_ManagerRev()
        {
            try
            {
                string temp = Base.GData("Approval_ManagerRev");
                if (temp.Contains("|"))
                {
                    string[] approver = temp.Split('|');
                    if (approver[0] != "no" && approver[1] != "no")
                    {
                        //Search and open approval form
                        string conditions = "State=Requested|Approver=" + approver[0] + "|Assignment group=" + approver[1];
                        System.Console.WriteLine("*******OPEN and APPROVE record of apprroval group: " + approver[1]);
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + approver[0], conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                            //approve the request
                            button = chg.Button_Approve();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click(true);
                                if (!flag)
                                    error = "Error when click on button approve.";
                                else
                                    chg.WaitLoading();
                            }
                            else
                                error = "Cannot get button approve.";
                        }
                    }
                    else
                        System.Console.WriteLine("*****Following input data, NO info regarding Change Manager Review Approval");
                }
                else
                { flag = false; error = "***ERROR: The format of input data is incorrect. Please validate again."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_to_036_Manager_Override_Approve_Business()
        {
            try
            {
                string temp = Base.GData("Approval_Business");
                if (temp.Contains("|"))
                {
                    string[] approver = temp.Split('|');
                    if (approver[0] != "no" && approver[1] != "no")
                    {
                        //Search and open approval form
                        string conditions = "State=Requested|Approver=" + approver[0] + "|Assignment group=" + approver[1];
                        System.Console.WriteLine("*******OPEN and APPROVE record of apprroval group: " + approver[1]);
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + approver[0], conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else
                        {
                            Thread.Sleep(5000);
                            chg.WaitLoading();
                            //approve the request
                            button = chg.Button_Approve();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click(true);
                                if (!flag)
                                    error = "Error when click on button approve.";
                                else
                                    chg.WaitLoading();
                            }
                            else
                                error = "Cannot get button approve.";
                        }
                    }
                }
                else
                { flag = false; error = "***ERROR: The format of input data is incorrect. Please validate again."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_to_039_Manager_Override_Approve_Cab()
        {
            try
            {
                string temp = Base.GData("Approval_cab");
                if (temp.Contains("|"))
                {
                    string[] approver = temp.Split('|');
                    if (approver[0] != "no" && approver[1] != "no")
                    {
                        //Search and open approval form
                        string conditions = "State=Requested|Approver=" + approver[0] + "|Assignment group=" + approver[1];
                        System.Console.WriteLine("*******OPEN and APPROVE record of apprroval group: " + approver[1]);
                        flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + approver[0], conditions, "State");
                        if (!flag) error = "Error when open record.";
                        else
                        {
                            chg.WaitLoading();
                            //approve the request
                            button = chg.Button_Approve();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click(true);
                                if (!flag)
                                    error = "Error when click on button approve.";
                                else
                                {
                                    chg.WaitLoading();
                                    flag = chg.Select_Tab("Affected CIs");
                                    if (!flag)
                                        error = "Cannot find Affected CIs tab.";
                                }

                            }
                            else
                                error = "Cannot get button approve.";
                        }
                    }
                }
                else
                { flag = false; error = "***ERROR: The format of input data is incorrect. Please validate again."; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_ImpersonateUser_ChangeCoordinator()
        {
            try
            {
                string temp = Base.GData("ChangeCoordinator");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user change implementer.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_042_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_044_045_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_Open_TheFirstChangeTask()
        {
            try
            {
                string conditions = "Short description=1 Execute Implementation Plan|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    error = "Error when open first change task 1: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_UpdateStateAndWorknotes_TheFirstChangeTask()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Work in Progress");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 1 - Work in Progress";
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Work in Progress)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_03_SaveChangeTask_1()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeTaskId_1 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change task 1 id:(" + changeTaskId_1 + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change task 1.";
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_04_CloseChangeTask_1()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 1 - Close Complete"; ;
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                            else
                            {
                                flag = chg.Update(true);
                                if (!flag) error = "Error when update change task 1.";
                            }
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Closed Complete)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_VerifyChangeState_InProgress()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Progress");
                    if (!flag) { error = "Invalid change state. Expected result: In Progress"; }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Verify_VerifyProposedChangesButton()
        {
            try
            {
                button = chg.Button_VerifyProposedChanges();
                flag = button.Existed;

                if (!flag)
                    error = "***ERROR: Cannot get verify proposed changes button.";
                else
                    System.Console.WriteLine("***Passed: verify proposed changes button is visible on form.");
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_03_Verify_ApplyproposedChangesButton()
        {
            try
            {
                button = chg.Button_ApplyProposedChange();
                flag = button.Existed;

                if (!flag)
                    error = "***ERROR: Cannot get apply proposed Changes button.";
                else
                    System.Console.WriteLine("***Passed: verify proposed changes button is visible on form.");
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Click_VerifyProposedChangesButton()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == string.Empty || changeId == null))
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                button = chg.Button_VerifyProposedChanges();
                flag = button.Existed;

                if (!flag)
                    error = "***ERROR: Cannot get verify proposed changes button.";
                else
                {
                    button.Click(true);
                    flag = chg.VerifyMessageInfo("Proposed changes verification passed for change '" + changeId + "'.");
                    if (!flag)
                        error = "***ERROR: Incorrect info message displays.";
                    else
                        chglist.WaitLoading();
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_01_Click_ApplyProposedChanges()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == string.Empty || changeId == null))
                {
                    AddParameter addPara = new AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                button = chg.Button_ApplyProposedChange();

                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);

                    if (flag)
                    {
                        chglist.WaitLoading();
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag)
                            error = "Cannot search and open the change.";
                    }
                    else
                        error = "***ERROR: Error happened when click on the button";
                }
                else
                    error = "***ERROR: Cannot get apply proposed changes button.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_02_Verify_VerifyProposedChanges()
        {
            try
            {
                bool Cflag = false;
                try
                {
                    button = chg.Button_VerifyProposedChanges();
                }
                catch { Cflag = button.Existed; }

                if (Cflag)
                {
                    flag = false;
                    flagExit = false;
                    error = "The button Verify Proposed Changes is appeared on form. Expectation - the button is not appeared on form.";
                }
                else
                {
                    flag = true;
                    System.Console.WriteLine("Passed: Verify Proposed Changes button is not appreared on form.");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_03_Verify_VerifyProposedChanges()
        {
            try
            {
                bool Cflag = false;
                try
                {
                    button = chg.Button_ApplyProposedChange();
                }
                catch { Cflag = button.Existed; }

                if (Cflag)
                {
                    flag = false;
                    flagExit = false;
                    error = "The button Apply Proposed Change is appeared on form. Expectation - the button is not appeared on form.";
                }
                else
                {
                    flag = true;
                    System.Console.WriteLine("Passed: Apply Proposed Change button is not appreared on form.");
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_01_Open_TheSecondChangeTask()
        {
            try
            {
                string conditions = "Short description=2 Execute Test Plan|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    flagExit = false;
                    error = "Error when open first change task 1: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_02_UpdateStateAndWorknotes_TheSecondChangeTask()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Work in Progress");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 2 - Work in Progress";
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Work in Progress)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_03_SaveChangeTask_2()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeTaskId_2 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change task 2 id:(" + changeTaskId_2 + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change task 2.";
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_04_CloseChangeTask_2()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 2 - Close Complete"; ;
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                            else
                            {
                                flag = chg.Update(true);
                                if (!flag) error = "Error when update change task 2.";
                            }
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Closed Complete)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_01_Open_TheThirdChangeTask()
        {
            try
            {
                string conditions = "Short description=3 Record Test Results|State=Open";
                flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found thrid change task: " + conditions;
                }
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_02_UpdateStateAndWorknotes_TheThridChangeTask()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Work in Progress");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 3 - Work in Progress";
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Work in Progress)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_03_SaveChangeTask_3()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeTaskId_3 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Change task 3 id:(" + changeTaskId_3 + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change task 3.";
                }
                else error = "Cannot get textbox number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_04_CloseChangeTask_ChangeTask_3()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed Complete");
                    if (flag)
                    {
                        textarea = chg.Textarea_Task_Worknotes();
                        flag = textarea.Existed;
                        if (flag)
                        {
                            string temp = Base.GData("TaskWorkNotes") + " - 3 - Close Complete"; ;
                            flag = textarea.SetText(temp);
                            if (!flag) error = "Error when populate task work notes.";
                            else
                            {
                                flag = chg.Update(true);
                                if (!flag) error = "Error when update change task 3.";
                            }
                        }
                        else error = "Cannot get textarea task work notes.";
                    }
                    else { error = "Error when select combobox item (Closed Complete)"; }
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_Impersonate_ChangeCoordinator()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("ChangeCoordinator"), true, Base.GData("UserFullName"));
                if (!flag)
                {
                    error = "Cannot impersonate Change Coordinator";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_NavigateTo_ComputerCIList()
        {
            try
            {
                string temp = Base.GData("Computer_SubClass");
                flag = home.LeftMenuItemSelect("Computer", temp);
                if (flag)
                {
                    computerList.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_SearchAndVerify_CIUpdated()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix (Date).");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = computerList.Add_More_Columns("Description");
                if (flag)
                {
                    string[] arr = Base.GData("Proposed_FieldValue").Split(';');
                    string conditions = arr[0] + "=" + arr[1];
                    flag = computerList.SearchAndVerify("Name", Base.GData("Name") + "_" + postfix, conditions);
                    if (!flag) error = "Error when search and verify computer CI (id:" + Base.GData("Name") + "_" + postfix + ")";
                }
                else
                    error = "Error when adding header column.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_ImpersonateUser_SupportUser()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp);
                if (!flag) error = "Error when impersonate user support.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_01_Filter_EmailSentToAssigned_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;has been assigned|and|" + "Recipients;contains;" + Base.GData("ChangeImplementorEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_02_OpenEmail_Assigned()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == string.Empty || changeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input KFTid04.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Cannot open email sent to change assignee.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_03_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_04_Verify_Email_Body_Assinged()
        {
            try
            {
                string temp = "Assigned to: " + Base.GData("ChangeImplementor");
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                    error = "Incorrect email information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_05_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close bemail body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_Filter_EmailSentToAssigned_Approved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;has been approved|and|" + "Recipients;contains;" + Base.GData("ChangeImplementorEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_01_Filter_EmailSentToTechnical_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + Base.GData("ChgCoorEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_02_OpenEmail_Approval()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == string.Empty || changeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change number.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Cannot open email sent to change assignee.";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_03_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------Linh update R11.4- update step 72 -----------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_04_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == string.Empty || changeId == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change number.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                string expected = "Category: " + Base.GData("Category") + "|Impact: 3 - Low" + "|Click here to approve " + changeId + "|Click here to reject " + changeId;
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_05_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_01_Filter_Task01EmailSentToAssignee_ClosedTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeTaskId_1 == string.Empty || changeTaskId_1 == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task Id 1.");
                    addPara.ShowDialog();
                    changeTaskId_1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                temp = "Subject;contains;" + changeTaskId_1 + "|and|Subject;contains;has been Closed|and|" + "Recipients;contains;" + Base.GData("ChangeImplementorEmail");
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_02_Open_EmailSentToAssignee_CloseTask()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeTaskId_1 == string.Empty || changeTaskId_1 == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task Id 1.");
                    addPara.ShowDialog();
                    changeTaskId_1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + changeTaskId_1;
                flag = emailList.Open(conditions, "Created");
                if (!flag) error = "Not found email sent change's assignee regarding task closed";
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_03_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeTaskId_1 == string.Empty || changeTaskId_1 == null))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task Id 1.");
                    addPara.ShowDialog();
                    changeTaskId_1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change task " + changeTaskId_1 + " has been Closed";
                flag = email.VerifySubject(expected);
                if (!flag) error = "Invalid subject value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------       

        [Test]
        public void Step_End_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
