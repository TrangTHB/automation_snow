﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Change
{
    [TestFixture]
    public class CHG_normal_review_resche_11
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Change task Id Adhoc: " + changeTaskId_Adhoc);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snobutton button;
        SNow.snotextarea textarea;
        SNow.snodatetime datetime;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;
        EmailList emailList = null;
        Email email = null;
        RiskAssessment riskAss;
        ItilList approvalList;
        //------------------------------------------------------------------
        string changeId, changeTaskId_Adhoc;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                riskAss = new RiskAssessment(Base);
                approvalList = new ItilList(Base, "Approval list");
                //------------------------------------------------------------------
                changeId = string.Empty;
                changeTaskId_Adhoc = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewChange()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_1_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_2_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_3_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_4_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_5_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Moderate");
                    if (!flag) { error = "Cannot populate priority value."; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_6_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    var date = DateTime.Now;
                    var nextSaturday = date.AddDays(6 - (int)date.DayOfWeek);
                    string startDate = nextSaturday.ToString("yyyy-MM-dd 01:mm:ss");
                    string endDate = nextSaturday.AddMinutes(1).ToString("yyyy-MM-dd 01:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_SaveChange()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeId = textbox.Text;
                    Console.WriteLine("-*-[STORE]: Change Id:(" + changeId + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change.";
                }
                else error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Verify_State_Draft()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (!flag)
                    {
                        error = "Invalid state";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_1_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_2_PopulateAssignedTo_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_3_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_014_015_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_AddWorkNotes()
        {
            try
            {
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    string temp = Base.GData("WorkNotes");
                    flag = chg.Add_Worknotes(temp);
                    if (!flag)
                        error = "Error when add work notes.";
                }
                else error = "Error when select tab Notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_ReAssignToChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeRequester.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_022_023_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_AddACI()
        {
            try
            {
                string temp = Base.GData("CI");
                lookup = chg.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot add CI."; }
                }
                else
                    error = "Cannot get lookup CI.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_ReAssignToCoordinator()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_026_027_CheckConflicts()
        //{
        //    try
        //    {
        //        flag = chg.Select_Tab("Conflicts");
        //        if (flag)
        //        {
        //            button = chg.Button_CheckConflicts;
        //            flag = button.Existed;
        //            if (flag)
        //            {
        //                flag = button.Click();
        //                if (flag)
        //                {
        //                    flag = chg.WaitCheckConflicts_Completed_And_CloseDialog();
        //                    if (flag)
        //                    {
        //                        combobox = chg.Combobox_ConflictStatus;
        //                        flag = combobox.Existed;
        //                        if (flag)
        //                        {
        //                            string expected = Base.GData("ConflictStatus");
        //                            flag = combobox.VerifyCurrentText(expected);
        //                            if (!flag)
        //                            {
        //                                error = "Invalid conflict status. Runtime:(" + combobox.Text + "). Expected:(" + expected + ")";
        //                                flagExit = false;
        //                            }

        //                        }
        //                        else error = "Cannot get combobox conflict status.";
        //                    }
        //                    else error = "Error when process check conflicts.";
        //                }
        //                else error = "Error when click on button check conflicts.";
        //            }
        //            else error = "Cannot get button check conflicts.";
        //        }
        //        else error = "Cannot select conflicts tab.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeRequester.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_032_033_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_PopulatePlannedDateTOReschedule()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    string startDate = DateTime.Today.AddDays(3).ToString("yyyy-MM-dd HH:mm:ss");
                    string endDate = DateTime.Today.AddDays(4).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_1_Click_RiskAssessmentTab()
        {
            try
            {
                flag = chg.Select_Tab("Risk Assessment");
                if (!flag)
                {
                    error = "Cannot click Risk Assessment Tab.";
                }
                else chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_2_Open_RiskAssessmentForm()
        {
            try
            {
                button = chg.Button_FillOutRiskAssessment();
                flag = button.Existed;
                if (!flag)
                    error = "Cannot get button fill out risk assessment.";
                else
                {
                    flag = button.Click();
                    if (flag)
                    {
                        riskAss.WaitLoading();

                        Thread.Sleep(2000);

                        // Verify Dialog title
                        snoelement ele = riskAss.Title();
                        flag = ele.Existed;
                        if (flag)
                        {
                            flag = ele.MyText.Equals("Risk Assessment");
                            if (!flag)
                            {
                                error = "Invalid title.";
                            }
                        }
                        else
                        {
                            error = "Cannot get label.";
                        }
                    }
                    else error = "Error when click on button fill out risk assessment";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_3_Fill_RiskAssessmentForm()
        {
            try
            {
                flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                if (!flag)
                {
                    error = "Cannot answer Risk Assessment questions";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_4_Submit_RiskAssessmentForm()
        {
            try
            {
                snoelement ele = riskAss.Form_Submit();
                ele.MyElement.Submit();
                Thread.Sleep(2000);
                button = riskAss.Button_Close();
                flag = button.Existed;
                if (flag)
                    button.Click();
                Thread.Sleep(2000);
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_5_Reload_Form()
        {
            try
            {
                flag = chg.ReloadForm();
                if (flag)
                    chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click button request approval";
                    else
                        chg.WaitLoading();
                }
                else error = "Cannot get button request approval";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Verify_RFC_Approvers_Requested()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCChangeManager");
                    string group = Base.GData("RFCManagerGroup");
                    string conditions = "State=Requested|Approver=" + temp + "|Assignment group=" + group;
                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions);
                    if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_ImpersonateUser_RFC_ChangeManager()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCChangeManager");
                    string rootuser = Base.GData("UserFullName");
                    flag = home.ImpersonateUser(temp, true, rootuser);
                    if (!flag) error = "Error when impersonate user RFCChangeManager.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_RFC_SystemSetting()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    flag = home.SystemSetting();
                    if (!flag) error = "Error when config system.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_042_RFC_SearchAndOpenChange()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = home.LeftMenuItemSelect("Change", "Open");
                    if (flag)
                    {
                        chglist.WaitLoading();
                        temp = chglist.List_Title().MyText;
                        flag = temp.Equals("Change Requests");
                        if (flag)
                        {
                            flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                            if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                            else chg.WaitLoading();
                        }
                        else
                        {
                            error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                        }
                    }
                    else error = "Error when select open change.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Select_RFCChangeManager_SignatureLine()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCChangeManager");
                    string group = Base.GData("RFCManagerGroup");
                    string conditions = "State=Requested|Approver=" + temp + "|Assignment group=" + group;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else chg.WaitLoading();
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_RFC_ClickApproveButton()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (!flag) error = "Error when click on button approve.";
                        else chg.WaitLoading();
                    } error = "Cannot get button approve.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Verify_RFC_Approvers()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCChangeManager");
                    string group1 = Base.GData("RFCApproverGroup");
                    string group2 = Base.GData("RFCAccountCSC");
                    string group3 = Base.GData("RFCManagerGroup");

                    string conditions = "State=Approved|Approver=" + temp + "|Assignment group=" + group1;

                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "!=Requested", conditions, true);
                    if (flag)
                    {
                        conditions = "State=Approved|Approver=" + temp + "|Assignment group=" + group2;
                        flag = chg.Verify_RelatedTable_Row("Approvers", conditions, true);
                        if (flag)
                        {
                            conditions = "State=Approved|Approver=" + temp + "|Assignment group=" + group3;
                            flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                            if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                        }
                        else error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";
                    }
                    else error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_ImpersonateUser_RFCAccountApprover()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCCSCApprover");
                    string rootuser = Base.GData("UserFullName");
                    flag = home.ImpersonateUser(temp, true, rootuser);
                    if (!flag) error = "Error when impersonate user RFCCSCApprover.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_RFC_SystemSetting()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    flag = home.SystemSetting();
                    if (!flag) error = "Error when config system.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_049_050_RFC_SearchAndOpenChange()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = home.LeftMenuItemSelect("Change", "Open");
                    if (flag)
                    {
                        chglist.WaitLoading();
                        temp = chglist.List_Title().MyText;
                        flag = temp.Equals("Change Requests");
                        if (flag)
                        {
                            flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                            if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                            else chg.WaitLoading();
                        }
                        else
                        {
                            error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                        }
                    }
                    else error = "Error when select open change.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Select_RFCCSCApprover_SignatureLine()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCCSCApprover");
                    string group = Base.GData("RFCAccountCSC");
                    string conditions = "State=Requested|Approver=" + temp + "|Assignment group=" + group;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else chg.WaitLoading();
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_RFC_ClickApproveButton()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (!flag) error = "Error when click on button approve.";
                        else chg.WaitLoading();
                    } error = "Cannot get button approve.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Verify_RFC_Approvers()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp1 = Base.GData("RFCCSCApprover");
                    string temp3 = Base.GData("RFCChangeManager");

                    string group1 = Base.GData("RFCAccountCSC");
                    string group2 = Base.GData("RFCApproverGroup");
                    string group3 = Base.GData("RFCManagerGroup");

                    string conditions = "Assignment group=" + group2;

                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "!=Requested", conditions, true);
                    if (flag)
                    {
                        conditions = "State=Approved|Approver=" + temp1 + "|Assignment group=" + group1;
                        flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                        if (flag)
                        {
                            conditions = "State=Approved|Approver=" + temp3 + "|Assignment group=" + group3;
                            flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                            if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                        }
                        else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                    }
                    else error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_ImpersonateUser_RFCChangeApprover()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCChangeApprover");
                    string rootuser = Base.GData("UserFullName");
                    flag = home.ImpersonateUser(temp, true, rootuser);
                    if (!flag) error = "Error when impersonate user RFCChangeApprover.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_RFC_SystemSetting()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    flag = home.SystemSetting();
                    if (!flag) error = "Error when config system.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_057_058_RFC_SearchAndOpenChange()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    flag = home.LeftMenuItemSelect("Change", "Open");
                    if (flag)
                    {
                        chglist.WaitLoading();
                        temp = chglist.List_Title().MyText;
                        flag = temp.Equals("Change Requests");
                        if (flag)
                        {
                            flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                            if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                            else chg.WaitLoading();
                        }
                        else
                        {
                            error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                        }
                    }
                    else error = "Error when select open change.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_RFC_VerifyNoRescheduleButtonOnForm()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    button = chg.Button_Reschedule();
                    flag = button.Existed;
                    if (!flag) flag = true;
                    else
                    {
                        flag = false;
                        error = "Reschedule button is displayed on Change form at incorrect state.";
                    }
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_Select_RFCChangeApprover_SignatureLine()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp = Base.GData("RFCChangeApprover");
                    string group = Base.GData("RFCApproverGroup");
                    string conditions = "State=Requested|Approver=" + temp + "|Assignment group=" + group;
                    flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions, "State");
                    if (!flag) error = "Error when open record.";
                    else chg.WaitLoading();
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_RFC_ClickApproveButton()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (!flag) error = "Error when click on button approve.";
                        else chg.WaitLoading();
                    } error = "Cannot get button approve.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Verify_RFC_Approvers()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp1 = Base.GData("RFCChangeManager");
                    string temp2 = Base.GData("RFCChangeApprover");
                    string temp3 = Base.GData("RFCCSCApprover");

                    string group1 = Base.GData("RFCApproverGroup");
                    string group2 = Base.GData("RFCAccountCSC");
                    string group3 = Base.GData("RFCManagerGroup");

                    string conditions = "State=Approved|Approver=" + temp2 + "|Assignment group=" + group1;

                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "!=Requested", conditions);
                    if (flag)
                    {
                        conditions = "State=Approved|Approver=" + temp3 + "|Assignment group=" + group2;
                        flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                        if (flag)
                        {
                            conditions = "State=Approved|Approver=" + temp1 + "|Assignment group=" + group3;
                            flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                            if (flag)
                            {
                                conditions = "State=Requested";
                                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", conditions, true);
                                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";

                            }
                            else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                        }
                        else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                    }
                    else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_RFC_VerifyNoRescheduleButtonOnForm()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    button = chg.Button_Reschedule();
                    flag = button.Existed;
                    if (!flag) flag = true;
                    else
                    {
                        flag = false;
                        error = "Reschedule button is displayed on Change form at incorrect state.";
                    }
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_1_RFC_RequestForReview()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click button request approval";
                    else
                        chg.WaitLoading();
                }
                else error = "Cannot get button request approval";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_2_RFC_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click button request approval";
                    else
                        chg.WaitLoading();
                }
                else error = "Cannot get button request approval";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_VerifyPlannedDateIsReadOnly()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.ReadOnly;
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.ReadOnly;
                                if (!flag) error = "Planned end date is not readonly.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Planned start date is not readonly.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else { System.Console.WriteLine("***[ERROR[: Cannot click on tab Schedule."); }

                if (!flag) flagExit = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_ImpersonateUser_TechnicalApprover()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user TechnicalApprover.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_FindANDSelectTechnicaleApproverWithAssignmentGroup()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = approvalList.List_Title().MyText;
                flag = temp.Equals("Approvals");
                if (flag)
                {
                    string approver = Base.GData("TechnicalApprover");
                    string group = Base.GData("AssignmentGroup");
                    string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                    flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                    else approvalList.WaitLoading();
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_1_ClickRejectButton()
        {
            try
            {
                string temp = "This change need to be reviewed. Reject the approval.";
                textarea = chg.Textarea_Approval_Comments();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (flag)
                    {
                        button = chg.Button_Approval_Reject();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click(true);
                            if (flag)
                            {
                                approvalList.WaitLoading();
                            }
                            else error = "Error when click on button reject.";
                        }
                        else error = "Cannot get button reject.";
                    }
                    else error = "Error when populate comment.";
                }
                else error = "Cannot get textarea comment.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_2_VerifyApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string approver = Base.GData("TechnicalApprover");
                string group = Base.GData("AssignmentGroup");
                string conditions = "State=Not Yet Requested|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                flag = approvalList.SearchAndVerify("Approval for", changeId, conditions);
                if (!flag) error = "Invalid state of Technical approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_074_075_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_1_Verify_ChangeState_Review()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Review");
                    if (!flag)
                        error = "The state is incorrect.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_2_Verify_ApprovalState_Requested()
        {
            try
            {
                string conditions = "State=Requested";
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", conditions, true);
                if (!flag)
                    error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_3_Verify_RFC_Approval_Approved()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp1 = Base.GData("RFCChangeManager");
                    string temp2 = Base.GData("RFCChangeApprover");
                    string temp3 = Base.GData("RFCCSCApprover");

                    string group1 = Base.GData("RFCManagerGroup");
                    string group2 = Base.GData("RFCApproverGroup");
                    string group3 = Base.GData("RFCAccountCSC");

                    string conditions = "State=Approved|Approver=" + temp1 + "|Assignment group=" + group1;

                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "=Approved", conditions);
                    if (flag)
                    {
                        conditions = "State=Approved|Approver=" + temp2 + "|Assignment group=" + group2;
                        flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                        if (flag)
                        {
                            conditions = "State=Approved|Approver=" + temp3 + "|Assignment group=" + group3;
                            flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                            if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                        }
                        else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                    }
                    else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else error = "Error when click on button request approval.";
                }
                else error = "Cannot get button request approval.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_VerifySelfAuthorizationIsNotApplicable_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                string conditions = "State=Not Valid|Approver=" + temp;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_ImpersonateUser_CustomerChangeManager()
        {
            try
            {
                string temp = Base.GData("CustomerChangeManager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user CustomerChangeManager.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_082_083_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_084_DoubleClickOnRequestedStateOfTechApproval()
        //{
        //    try
        //    {
        //        string temp = Base.GData("TechnicalApprover");
        //        string conditions = "State=Requested|Approver=" + temp;
        //        flag = chg.RelatedTableSearchAndVerifyCellReadOnly("Approvers", "Approver", "=" + temp, conditions, "State");
        //        if (!flag) error = "The cell is not as expected (ReadOnly)";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_085_CustomerChangeMrgSelectTechnicalApproverLine()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string conditions = "State=Requested|Approver=" + temp;
                flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", temp, conditions, "State");
                if (!flag) error = "Error when open record (" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_1_VerifyRequestedApprovalState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Requested");
                    if (!flag) error = "Invalid state value.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_2_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) error = "Error when click on button approve.";
                    else chg.WaitLoading();
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_3_VerifyApprovalState()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string conditions = "State=Approved|Approver=" + temp;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_087_ImpersonateUser_ChangeMangerReviewApprover()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    string temp = Base.GData("ChangeManagerReview");
                    string rootuser = Base.GData("UserFullName");
                    flag = home.ImpersonateUser(temp, true, rootuser);
                    if (!flag) error = "Error when impersonate user ChangeManagerReview.";
                }
                else 
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_088_SystemSetting()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    flag = home.SystemSetting();
                    if (!flag) error = "Error when config system.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_OpenMyApproval()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else
                        error = "Error when open my approvals.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_FindANDSelectManagerReviewLine()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    temp = approvalList.List_Title().MyText;
                    flag = temp.Equals("Approvals");
                    if (flag)
                    {
                        string approver = Base.GData("ChangeManagerReview");
                        string group = Base.GData("ManagerReviewGroup");
                        string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                        flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                        if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                    }
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_1_ClickApproveButton()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (!flag) error = "Error when click on button approve.";
                        else approvalList.WaitLoading();
                    } error = "Cannot get button approve.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_2_VerifyApproval()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    string approver = Base.GData("ChangeManagerReview");
                    string group = Base.GData("ManagerReviewGroup");
                    string conditions = "State=Approved|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                    flag = approvalList.SearchAndVerify("Approval for", changeId, conditions);
                    if (!flag) error = "Invalid state of manager review approval.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_ImpersonateUser_BusinessApprover()
        {
            try
            {
                string sId = Base.GData("BusinessApproverUserId");
                string temp = Base.GData("BusinessApprover");
                if (sId.ToLower() != "no")
                {
                    temp = temp + ";" + sId;
                }
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user Business Approver.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_093_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_094_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_095_FindANDSelectBusinessApproverLine()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = approvalList.List_Title().MyText;
                flag = temp.Equals("Approvals");
                if (flag)
                {
                    string approver = Base.GData("BusinessApprover");
                    string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId;
                    flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_096_1_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) error = "Error when click on button approve.";
                    else approvalList.WaitLoading();
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_096_2_VerifyApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string approver = Base.GData("BusinessApprover");

                string conditions = "State=Approved|Approver=" + approver + "|Approval for=" + changeId;
                flag = chglist.SearchAndVerify("Approval for", changeId, conditions);
                if (!flag) error = "Invalid state of business approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_097_ImpersonateUser_ChageManager()
        {
            try
            {
                string temp = Base.GData("ChangeManager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeManager.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_098_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_099_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_100_FindANDSelectManagerApproverLine()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = approvalList.List_Title().MyText;
                flag = temp.Equals("Approvals");
                if (flag)
                {
                    string approver = Base.GData("ChangeManager");
                    string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId;
                    flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_101_1_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) error = "Error when click on button approve.";
                    else approvalList.WaitLoading();
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_101_2_VerifyApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string approver = Base.GData("ChangeManager");

                string conditions = "State=Approved|Approver=" + approver + "|Approval for=" + changeId;
                flag = approvalList.SearchAndVerify("Approval for", changeId, conditions);
                if (!flag) error = "Invalid state of change manager approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_102_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_103_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_104_105_106_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_107_00_Verify_Delete_Button_NotVisible()
        {
            try
            {
                button = chg.Button_Delete();
                flag = button.Existed;
                if (flag)
                {
                    error = "Delete button is Visible. Expected is NOT Visible.";
                    flag = false;
                    flagExit = false;
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_107_01_SubmitAdhocTaskForChange()
        {
            try
            {
                flag = chg.Select_Tab("Change Tasks");
                if (flag)
                {
                    flag = chg.RelatedTab_Click_Button("Change Tasks", "New");
                    if (flag)
                    {
                        chg.WaitLoading();
                        textbox = chg.Textbox_Number();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            changeTaskId_Adhoc = textbox.Text;
                            textbox = chg.Textbox_ShortDescription();
                            flag = textbox.Existed;
                            if (flag)
                            {
                                flag = textbox.SetText("Test adding Adhoc Task to change.", true);
                                if (flag)
                                {
                                    button = chg.Button_Submit();
                                    flag = button.Existed;
                                    if (flag)
                                    {
                                        flag = button.Click(true);
                                        if (!flag) error = "Error when click on submit button.";
                                        else chg.WaitLoading();
                                    }
                                    else error = "Cannot get button submit.";
                                }
                                else error = "Cannot populate short description value.";
                            }
                            else error = "Cannot get textbox short description.";

                        }
                        else error = "Cannot get textbox number.";
                    }
                    else error = "Error when click on button New.";
                }
                else error = "Cannot select change tasks tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_107_02_VerifyChangeStateAndPlannedDateIsReadOnly()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Scheduled");
                    if (flag)
                    {
                        flag = chg.Select_Tab("Schedule");
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_Start_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.ReadOnly;
                                if (flag)
                                {
                                    datetime = chg.Datetime_Planned_End_Date();
                                    flag = datetime.Existed;
                                    if (flag)
                                    {
                                        flag = datetime.ReadOnly;
                                        if (!flag) error = "Planned end date is not readonly.";
                                    }
                                    else error = "Cannot get datetime planned end date.";
                                }
                                else error = "Planned start date is not readonly.";
                            }
                            else error = "Cannot get datetime planned start date.";
                        }
                        else { System.Console.WriteLine("***[ERROR[: Cannot click on tab Schedule."); }
                    }
                    else error = "Invalid change state value.";
                }
                else error = "Cannot get combobox state.";

                if (!flag) flagExit = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_108_ClickRescheduleButtonANDVerifyPopUp()
        {
            try
            {
                button = chg.Button_Reschedule();
                flag = button.Existed;
                if (flag) 
                {
                    flag = button.Click();
                    if (flag) 
                    {
                        string temp = "This will reschedule the change. Do you want to continue?";
                        flag = chg.VerifyConfirmDialog(temp);
                        if (!flag)
                            error = "Some features on the popup is missing or incorrect.";
                    } else error = "Error when click on button reschedule.";
                } else error = "Cannot get button reschedule.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_109_ClickNoButtonANDVerifyTheChange()
        {
            try
            {
                string temp = "This will reschedule the change. Do you want to continue?";
                flag = chg.VerifyConfirmDialog(temp, "no");
                if (!flag)
                    error = "Some features on the popup is missing or incorrect.";
                else
                {
                    combobox = chg.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.VerifyCurrentValue("Scheduled");
                        if (!flag) error = "Having change on the form although reschedule comfirmation is NO. Missing Reschedule button.";
                    }
                    else error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_110_ClickRescheduleAndClickYes()
        {
            try
            {
                button = chg.Button_Reschedule();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        string temp = "This will reschedule the change. Do you want to continue?";
                        flag = chg.VerifyConfirmDialog(temp, "yes");
                        if (!flag)
                            error = "Some features on the popup is missing or incorrect.";
                    }
                    else error = "Error when click on button reschedule.";
                }
                else error = "Cannot get button reschedule.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_1_VerifyChangeStateAndPlannedDateIsNOTReadOnly()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Review");
                    if (flag)
                    {
                        flag = chg.Select_Tab("Schedule");
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_Start_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.ReadOnly;
                                if (!flag)
                                {
                                    datetime = chg.Datetime_Planned_End_Date();
                                    flag = datetime.Existed;
                                    if (flag)
                                    {
                                        flag = datetime.ReadOnly;
                                        if (flag) error = "Planned end date is readonly. Expected it is NOT readonly.";
                                        else { flag = true; }
                                    }
                                    else error = "Cannot get datetime planned end date.";
                                }
                                else error = "Planned start date is readonly. Expected it is NOT readonly.";
                            }
                            else error = "Cannot get datetime planned start date.";
                        }
                        else { System.Console.WriteLine("***[ERROR[: Cannot click on tab Schedule."); }
                    }
                    else error = "Invalid change state value.";
                }
                else error = "Cannot get combobox state.";

                if (!flag) flagExit = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_2_ValidateApproverTabs()
        {
            try
            {
                string temp1 = Base.GData("RFCChangeManager");
                string temp2 = Base.GData("RFCChangeApprover");
                string temp3 = Base.GData("RFCCSCApprover");

                string group1 = Base.GData("RFCManagerGroup");
                string group2 = Base.GData("RFCApproverGroup");
                string group3 = Base.GData("RFCAccountCSC");

                string conditions = "State=Approved|Approver=" + temp1 + "|Assignment group=" + group1;

                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "!=Requested", conditions);
                if (flag)
                {
                    conditions = "State=Approved|Approver=" + temp2 + "|Assignment group=" + group2;
                    flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                    if (flag)
                    {
                        conditions = "State=Approved|Approver=" + temp3 + "|Assignment group=" + group3;
                        flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                        if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                        else
                        {
                            conditions = "State=Requested";
                            flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", conditions, true);
                            if (!flag)
                            {
                                error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";
                                flagExit = false;
                            }
                        }
                    }
                    else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                }
                else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_3_ValidateChangeTaskTab()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeTaskId_Adhoc == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input adhoc change task Id.");
                    addPara.ShowDialog();
                    changeTaskId_Adhoc = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                string conditions = "Number=" + changeTaskId_Adhoc + "|State=Open";
                flag = chg.Search_Verify_RelatedTable_Row("Change Tasks", "Number", changeTaskId_Adhoc, conditions);
                if (!flag) error = "Not found change task (" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_4_Change_Verify_Activity_111_04()
        {
            try
            {
                string temp = Base.GData("Activity_111_04");
                flag = chg.Select_Tab("Notes");
                if (flag)
                {
                    flag = chg.Verify_Activity(temp);
                    if (!flag) error = "Invalid activity note 111_04. Expected:(" + temp + ")";
                }
                else error = "Cannot select tab (Notes).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_112_RePopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    string startDate = DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss");
                    string endDate = DateTime.Today.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_113_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                        combobox = chg.Combobox_State();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.VerifyCurrentValue("Approval");
                            if (!flag) error = "Invalid state value.";

                        }
                        else error = "Cannot get combobox state.";
                    }
                    else error = "Error when click on button request approval.";

                }
                else error = "Cannot get button request approval.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_114_ImpersonateUser_TechnicalApprover()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user TechnicalApprover.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_115_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_116_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_117_FindANDSelectTechnicaleApproverWithAssignmentGroup()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = approvalList.List_Title().MyText;
                flag = temp.Equals("Approvals");
                if (flag)
                {
                    string approver = Base.GData("TechnicalApprover");
                    string group = Base.GData("AssignmentGroup");
                    string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                    flag = chglist.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_118_1_ClickRejectButton()
        {
            try
            {
                string temp = "This change need to be reviewed. Reject the approval.";
                textarea = chg.Textarea_Approval_Comments();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (flag)
                    {
                        button = chg.Button_Approval_Reject();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click(true);
                            if (flag)
                            {
                                chglist.WaitLoading();
                            }
                            else error = "Error when click on button reject.";
                        }
                        else error = "Cannot get button reject.";
                    }
                    else error = "Error when populate comment.";
                }
                else error = "Cannot get textarea comment.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_118_2_VerifyApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string approver = Base.GData("TechnicalApprover");
                string group = Base.GData("AssignmentGroup");
                string conditions = "State=Not Yet Requested|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                flag = chglist.SearchAndVerify("Approval for", changeId, conditions);
                if (!flag) error = "Invalid state of Technical approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_119_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        public void Step_120_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_122_123_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_124_1_Verify_ChangeState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Review");
                    if (!flag)
                        error = "The state is incorrect.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_124_2_Verify_ApprovalState_Requested()
        {
            try
            {
                string conditions = "State=Requested";
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "=Requested", conditions, true);
                if (!flag)
                    error = "Invalid approver in list. Expected:(" + conditions + ") NOT FOUND.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_124_3_Verify_RFC_Approval_Approved()
        {
            try
            {
                string rfc = Base.GData("RFC");
                if (rfc.ToLower() == "yes")
                {
                    string temp1 = Base.GData("RFCChangeManager");
                    string temp2 = Base.GData("RFCChangeApprover");
                    string temp3 = Base.GData("RFCCSCApprover");

                    string group1 = Base.GData("RFCManagerGroup");
                    string group2 = Base.GData("RFCApproverGroup");
                    string group3 = Base.GData("RFCAccountCSC");

                    string conditions = "State=Approved|Approver=" + temp1 + "|Assignment group=" + group1;

                    flag = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "=Approved", conditions);
                    if (flag)
                    {
                        conditions = "State=Approved|Approver=" + temp2 + "|Assignment group=" + group2;
                        flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                        if (flag)
                        {
                            conditions = "State=Approved|Approver=" + temp3 + "|Assignment group=" + group3;
                            flag = chg.Verify_RelatedTable_Row("Approvers", conditions);
                            if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                        }
                        else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                    }
                    else error = "Invalid approver in list. Expected:(" + conditions + ") FOUND.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: This change is not configured for RFC Approval.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_125_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else error = "Error when click on button request approval.";
                }
                else error = "Cannot get button request approval.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_126_VerifySelfAuthorizationIsNotApplicable_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                string conditions = "State=Not Valid|Approver=" + temp;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_127_ImpersonateUser_CustomerChangeManager()
        {
            try
            {
                string temp = Base.GData("CustomerChangeManager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user CustomerChangeManager.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_128_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_129_130_131_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_132_CustomerChangeMrgSelectTechnicalApproverLine()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string conditions = "State=Requested|Approver=" + temp;
                flag = chg.Search_Open_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions, "State");
                if (!flag) error = "Error when open record (" + conditions + ")";
                else chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_133_VerifyRequestedApprovalState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Requested");
                    if (!flag) error = "Invalid state value.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_134_1_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) error = "Error when click on button approve.";
                    else chg.WaitLoading();
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_134_2_VerifyApprovalState()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string conditions = "State=Approved|Approver=" + temp;
                flag = chg.Search_Verify_RelatedTable_Row("Approvers", "Approver", "=" + temp, conditions);
                if (!flag) error = "Invalid approver in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_135_ImpersonateUser_ChangeMangerReviewApprover()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    string temp = Base.GData("ChangeManagerReview");
                    string rootuser = Base.GData("UserFullName");
                    flag = home.ImpersonateUser(temp, true, rootuser);
                    if (!flag) error = "Error when impersonate user ChangeManagerReview.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_136_SystemSetting()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    flag = home.SystemSetting();
                    if (!flag) error = "Error when config system.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_137_OpenMyApproval()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else
                        error = "Error when open my approvals.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_138_FindANDSelectManagerReviewLine()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    temp = approvalList.List_Title().MyText;
                    flag = temp.Equals("Approvals");
                    if (flag)
                    {
                        string approver = Base.GData("ChangeManagerReview");
                        string group = Base.GData("ManagerReviewGroup");
                        string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                        flag = chglist.SearchAndOpen("Approval for", changeId, conditions, "State");
                        if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                    }
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_139_1_ClickApproveButton()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    button = chg.Button_Approve();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click(true);
                        if (!flag) error = "Error when click on button approve.";
                        else approvalList.WaitLoading();
                    } error = "Cannot get button approve.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_139_2_VerifyApproval()
        {
            try
            {
                string reviewGroup = Base.GData("ManagerReviewGroup");
                if (reviewGroup.ToLower() != "no")
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (changeId == null || changeId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                        addPara.ShowDialog();
                        changeId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------
                    string approver = Base.GData("ChangeManagerReview");
                    string group = Base.GData("ManagerReviewGroup");
                    string conditions = "State=Approved|Approver=" + approver + "|Approval for=" + changeId + "|Assignment group=" + group;
                    flag = approvalList.SearchAndVerify("Approval for", changeId, conditions);
                    if (!flag) error = "Invalid state of manager review approval.";
                }
                else
                {
                    System.Console.WriteLine("-*-[INFO]: There is no ManagerReview Group approval configured for test account.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_140_ImpersonateUser_BusinessApprover()
        {
            try
            {
                string sId = Base.GData("BusinessApproverUserId");
                string temp = Base.GData("BusinessApprover");
                if (sId.ToLower() != "no")
                {
                    temp = temp + ";" + sId;
                }
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user Business Approver.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_141_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_142_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_143_FindANDSelectBusinessApproverLine()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = approvalList.List_Title().MyText;
                flag = temp.Equals("Approvals");
                if (flag)
                {
                    string approver = Base.GData("BusinessApprover");
                    string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId;
                    flag = chglist.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_144_1_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) error = "Error when click on button approve.";
                    else approvalList.WaitLoading();
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_144_2_VerifyApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string approver = Base.GData("BusinessApprover");

                string conditions = "State=Approved|Approver=" + approver + "|Approval for=" + changeId;
                flag = chglist.SearchAndVerify("Approval for", changeId, conditions);
                if (!flag) error = "Invalid state of business approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_145_ImpersonateUser_ChageManager()
        {
            try
            {
                string temp = Base.GData("ChangeManager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeManager.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_146_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_147_OpenMyApproval()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Service Desk", "My Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                }
                else
                    error = "Error when open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_148_FindANDSelectManagerApproverLine()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                temp = approvalList.List_Title().MyText;
                flag = temp.Equals("Approvals");
                if (flag)
                {
                    string approver = Base.GData("ChangeManager");
                    string conditions = "State=Requested|Approver=" + approver + "|Approval for=" + changeId;
                    flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open approval (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else
                {
                    error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_149_1_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag) error = "Error when click on button approve.";
                    else approvalList.WaitLoading();
                } error = "Cannot get button approve.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_149_2_VerifyApproval()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string approver = Base.GData("ChangeManager");

                string conditions = "State=Approved|Approver=" + approver + "|Approval for=" + changeId;
                flag = chglist.SearchAndVerify("Approval for", changeId, conditions);
                if (!flag) error = "Invalid state of change manager approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_150_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_151_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_152_153_154_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------
        [Test]
        public void Step_155_VerifyChangeForm()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Scheduled");
                    if (flag)
                    {
                        string conditions = "Short description=Test adding Adhoc Task to change.|State=Open";
                        flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                        if (flag)
                        {
                            conditions = "Short description=1 Execute Implementation Plan|State=Open";
                            flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                            if (!flag)
                                error = "Not found change task (" + conditions + ")";
                        }
                        else error = "Not found adhoc change task (" + conditions + ")";
                    }
                    else error = "Invalid state value.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_156_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
