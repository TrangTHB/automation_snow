﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Change
{
    [TestFixture]
    public class CHG_normal_cancel_13
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change NotYetRequested Id: " + change_notyetrequested_Id);
            System.Console.WriteLine("Finished - Change Requested Id: " + change_requested_Id);
            System.Console.WriteLine("Finished - Change Approved Id: " + change_approved_Id);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snobutton button;
        SNow.snotextarea textarea;
        SNow.snodatetime datetime;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;
        EmailList emailList = null;
        Email email = null;
        RiskAssessment riskAss;
        ItilList approvalList;
        //------------------------------------------------------------------
        string change_notyetrequested_Id, change_requested_Id, change_approved_Id;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                riskAss = new RiskAssessment(Base);
                approvalList = new ItilList(Base, "Approval list");
                //------------------------------------------------------------------
                change_notyetrequested_Id = string.Empty;
                change_requested_Id = string.Empty;
                change_approved_Id = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Cancel Not Yet Requested Change

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Create_Change_NotYetRequested()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("Normal");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_1_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_2_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_3_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_4_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_5_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Moderate");
                    if (!flag) { error = "Cannot populate priority value."; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_6_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_7_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    var date = DateTime.Now;
                    var nextSaturday = date.AddDays(6 - (int)date.DayOfWeek);
                    string startDate = nextSaturday.ToString("yyyy-MM-dd 01:mm:ss");
                    string endDate = nextSaturday.AddMinutes(1).ToString("yyyy-MM-dd 01:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_SaveChange_NotYetRequested()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    change_notyetrequested_Id = textbox.Text;
                    Console.WriteLine("-*-[STORE]: Change Id:(" + change_notyetrequested_Id + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change.";
                }
                else error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_SearchAndOpen_Change_NotYetRequested()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_notyetrequested_Id == null || change_notyetrequested_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    change_notyetrequested_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    flag = chglist.SearchAndOpen("Number", change_notyetrequested_Id, "Number=" + change_notyetrequested_Id + "|Approval=Not Yet Requested", "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_notyetrequested_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Click_CancelButton()
        {
            try
            {
                button = chg.Button_Cancel();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot click Cancel button.";
                    }
                }
                else { error = "Cannot get Cancel button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_VerifyConfirmDialog_OK()
        {
            try
            {
                string temp = "If you cancel this change it cannot be undone - Click OK to cancel the change, click Cancel if you do not want to continue";
                flag = chg.HandleAlert("accept", temp);
                if (!flag)
                {
                    error = "Cannot verify confirm dialog or Invalid message.";
                }
                else { chglist.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SearchAndOpen_Change_NotYetRequested()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_notyetrequested_Id == null || change_notyetrequested_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change NotYetRequested Id.");
                    addPara.ShowDialog();
                    change_notyetrequested_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "All");
                if (flag)
                {
                    chglist.WaitLoading();
                    string condition = "Number=" + change_notyetrequested_Id + "|State=Cancelled";
                    flag = chglist.SearchAndOpen("Number", change_notyetrequested_Id, condition, "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_notyetrequested_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open Change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_State_CancelButton()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.ReadOnly;
                    if (flag)
                    {
                        string temp = "Cancelled";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid value. Expected: [" + temp + "]. Actual: [" + combobox.Text + "]";
                        }
                        else
                        {
                            button = chg.Button_Cancel();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = false;
                                flagExit = false;
                                error = "Cancel button is existed. Expected: NOT";
                            }
                            else
                            {
                                flag = true;
                                System.Console.WriteLine("***PASSED: Cancel button is NOT existed.");
                            }
                        }
                    }
                    else
                    {
                        flagExit = false;
                        error = "State combobox is not readonly for ITIL Role.";
                    }
                }
                else
                {
                    error = "Cannot get State combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Cancel Requested Change

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Create_Change_Requested()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("Normal");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_1_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_2_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_3_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_4_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_5_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Moderate");
                    if (!flag) { error = "Cannot populate priority value."; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_6_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_7_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    var date = DateTime.Now;
                    var nextSaturday = date.AddDays(6 - (int)date.DayOfWeek);
                    string startDate = nextSaturday.ToString("yyyy-MM-dd 01:mm:ss");
                    string endDate = nextSaturday.AddMinutes(1).ToString("yyyy-MM-dd 01:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_8_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignGroup");
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_SaveChange_Requested()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    change_requested_Id = textbox.Text;
                    Console.WriteLine("-*-[STORE]: Change Id:(" + change_requested_Id + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change.";
                }
                else error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_1_Click_RiskAssessmentTab()
        {
            try
            {
                flag = chg.Select_Tab("Risk Assessment");
                if (!flag)
                {
                    error = "Cannot click Risk Assessment Tab.";
                }
                else chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_2_Open_RiskAssessmentForm()
        {
            try
            {
                button = chg.Button_FillOutRiskAssessment();
                flag = button.Existed;
                if (!flag)
                    error = "Cannot get button fill out risk assessment.";
                else
                {
                    flag = button.Click();
                    if (flag)
                    {
                        riskAss.WaitLoading();

                        Thread.Sleep(2000);

                        // Verify Dialog title
                        snoelement ele = riskAss.Title();
                        flag = ele.Existed;
                        if (flag)
                        {
                            flag = ele.MyText.Equals("Risk Assessment");
                            if (!flag)
                            {
                                error = "Invalid title.";
                            }
                        }
                        else
                        {
                            error = "Cannot get label.";
                        }
                    }
                    else error = "Error when click on button fill out risk assessment";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_3_Fill_RiskAssessmentForm()
        {
            try
            {
                flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                if (!flag)
                {
                    error = "Cannot answer Risk Assessment questions";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_4_Submit_RiskAssessmentForm()
        {
            try
            {
                snoelement ele = riskAss.Form_Submit();
                ele.MyElement.Submit();
                Thread.Sleep(2000);
                button = riskAss.Button_Close();
                flag = button.Existed;
                if (flag)
                    button.Click();
                Thread.Sleep(2000);
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_5_Reload_Form()
        {
            try
            {
                flag = chg.ReloadForm();
                if (flag)
                    chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click button request approval";
                    else
                    {
                        chg.WaitLoading();
                        button = chg.Button_RequestApproval_RequestReview();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            chg.WaitLoading();
                        }
                    }
                }
                else error = "Cannot get button request approval";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_SearchAndOpen_Change_Requested()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_requested_Id == null || change_requested_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    change_requested_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    flag = chglist.SearchAndOpen("Number", change_requested_Id, "Number=" + change_requested_Id + "|Approval=Requested", "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_requested_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Click_CancelButton()
        {
            try
            {
                button = chg.Button_Cancel();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot click Cancel button.";
                    }
                }
                else { error = "Cannot get Cancel button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_VerifyConfirmDialog_OK()
        {
            try
            {
                string temp = "If you cancel this change it cannot be undone - Click OK to cancel the change, click Cancel if you do not want to continue";
                flag = chg.HandleAlert("accept", temp);
                if (!flag)
                {
                    error = "Cannot verify confirm dialog or Invalid message.";
                }
                else { chglist.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_SearchAndOpen_Change_Requested()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_requested_Id == null || change_requested_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Requested Id.");
                    addPara.ShowDialog();
                    change_requested_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "All");
                if (flag)
                {
                    chglist.WaitLoading();
                    string condition = "Number=" + change_requested_Id + "|State=Cancelled";
                    flag = chglist.SearchAndOpen("Number", change_requested_Id, condition, "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_requested_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open Change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Verify_State_CancelButton()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.ReadOnly;
                    if (flag)
                    {
                        string temp = "Cancelled";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid value. Expected: [" + temp + "]. Actual: [" + combobox.Text + "]";
                        }
                        else
                        {
                            button = chg.Button_Cancel();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = false;
                                flagExit = false;
                                error = "Cancel button is existed. Expected: NOT";
                            }
                            else
                            {
                                flag = true;
                                System.Console.WriteLine("***PASSED: Cancel button is NOT existed.");
                            }
                        }
                    }
                    else
                    {
                        flagExit = false;
                        error = "State combobox is not readonly for ITIL Role.";
                    }
                }
                else
                {
                    error = "Cannot get State combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion

        #region Cancel Approved Change
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Create_Change_Approved()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("Normal");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_1_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_2_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_3_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_4_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_5_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Moderate");
                    if (!flag) { error = "Cannot populate priority value."; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_6_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_7_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    var date = DateTime.Now;
                    var nextSaturday = date.AddDays(6 - (int)date.DayOfWeek);
                    string startDate = nextSaturday.ToString("yyyy-MM-dd 01:mm:ss");
                    string endDate = nextSaturday.AddMinutes(1).ToString("yyyy-MM-dd 01:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_8_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignGroup");
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_SaveChange_Approved()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    change_approved_Id = textbox.Text;
                    Console.WriteLine("-*-[STORE]: Change Id:(" + change_approved_Id + ")");
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change.";
                }
                else error = "Cannot get textbox number.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_1_Click_RiskAssessmentTab()
        {
            try
            {
                flag = chg.Select_Tab("Risk Assessment");
                if (!flag)
                {
                    error = "Cannot click Risk Assessment Tab.";
                }
                else chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_2_Open_RiskAssessmentForm()
        {
            try
            {
                button = chg.Button_FillOutRiskAssessment();
                flag = button.Existed;
                if (!flag)
                    error = "Cannot get button fill out risk assessment.";
                else
                {
                    flag = button.Click();
                    if (flag)
                    {
                        riskAss.WaitLoading();

                        Thread.Sleep(2000);

                        // Verify Dialog title
                        snoelement ele = riskAss.Title();
                        flag = ele.Existed;
                        if (flag)
                        {
                            flag = ele.MyText.Equals("Risk Assessment");
                            if (!flag)
                            {
                                error = "Invalid title.";
                            }
                        }
                        else
                        {
                            error = "Cannot get label.";
                        }
                    }
                    else error = "Error when click on button fill out risk assessment";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_3_Fill_RiskAssessmentForm()
        {
            try
            {
                flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                if (!flag)
                {
                    error = "Cannot answer Risk Assessment questions";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_4_Submit_RiskAssessmentForm()
        {
            try
            {
                snoelement ele = riskAss.Form_Submit();
                ele.MyElement.Submit();
                Thread.Sleep(2000);
                button = riskAss.Button_Close();
                flag = button.Existed;
                if (flag)
                    button.Click();
                Thread.Sleep(2000);
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_5_Reload_Form()
        {
            try
            {
                flag = chg.ReloadForm();
                if (flag)
                    chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click button request approval";
                    else
                    {
                        chg.WaitLoading();
                        button = chg.Button_RequestApproval_RequestReview();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            chg.WaitLoading();
                        }
                    }
                }
                else error = "Cannot get button request approval";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_ImpersonateUser_ChangeManager()
        {
            try
            {
                string temp = Base.GData("ChangeManager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_SearchAndOpen_Change_Approved()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_approved_Id == null || change_approved_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    change_approved_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    flag = chglist.SearchAndOpen("Number", change_approved_Id, "Number=" + change_approved_Id , "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_approved_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Auto_Approve_Change()
        {
            try
            {
                bool flagFound = true;
                while (flagFound && flag)
                {
                    flagFound = chg.Search_Verify_RelatedTable_Row("Approvers", "State", "Requested", "State=Requested");
                    if (flagFound)
                    {
                        flag = chg.RelatedTableOpenRecord("Approvers", "State=Requested", "State");
                        if (flag)
                        {
                            chg.WaitLoading();
                            button = chg.Button_Approve();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = button.Click();
                                chg.WaitLoading();
                            }
                        }  
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_SearchAndOpen_Change_Approved()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_approved_Id == null || change_approved_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    change_approved_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    flag = chglist.SearchAndOpen("Number", change_approved_Id, "Number=" + change_approved_Id + "|Approval=Approved", "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_approved_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Click_CancelButton()
        {
            try
            {
                button = chg.Button_Cancel();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot click Cancel button.";
                    }
                }
                else { error = "Cannot get Cancel button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_VerifyConfirmDialog_OK()
        {
            try
            {
                string temp = "If you cancel this change it cannot be undone - Click OK to cancel the change, click Cancel if you do not want to continue";
                flag = chg.HandleAlert("accept", temp);
                if (!flag)
                {
                    error = "Cannot verify confirm dialog or Invalid message.";
                }
                else { chglist.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_SearchAndOpen_Change_Approved()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (change_approved_Id == null || change_approved_Id == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Requested Id.");
                    addPara.ShowDialog();
                    change_approved_Id = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "All");
                if (flag)
                {
                    chglist.WaitLoading();
                    string condition = "Number=" + change_approved_Id + "|State=Cancelled";
                    flag = chglist.SearchAndOpen("Number", change_approved_Id, condition, "Number");
                    if (!flag) error = "Error when search and open change (id:" + change_approved_Id + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open Change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Verify_State_CancelButton()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.ReadOnly;
                    if (flag)
                    {
                        string temp = "Cancelled";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid value. Expected: [" + temp + "]. Actual: [" + combobox.Text + "]";
                        }
                        else
                        {
                            button = chg.Button_Cancel();
                            flag = button.Existed;
                            if (flag)
                            {
                                flag = false;
                                flagExit = false;
                                error = "Cancel button is existed. Expected: NOT";
                            }
                            else
                            {
                                flag = true;
                                System.Console.WriteLine("***PASSED: Cancel button is NOT existed.");
                            }
                        }
                    }
                    else
                    {
                        flagExit = false;
                        error = "State combobox is not readonly for ITIL Role.";
                    }
                }
                else
                {
                    error = "Cannot get State combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
