﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using SNow;
namespace Change
{
    [TestFixture]
    public class CHG_standard_not_mod_e2e_10
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Change Task 1 Id: " + changeTask1);
            System.Console.WriteLine("Finished - Change Task 2 Id: " + changeTask2);
            System.Console.WriteLine("Finished - Change Task 3 Id: " + changeTask3);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snodatetime datetime;
        
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;
        EmailList emailList = null;
        ItilList approvalList = null;
        Email email = null;
        RiskAssessment riskAss = null;
        //------------------------------------------------------------------
        string changeId;
        string changeTask1, changeTask2, changeTask3;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                emailList = new EmailList(Base, "Email list");
                approvalList = new ItilList(Base, "Approval list");
                email = new Email(Base, "Email");
                riskAss = new RiskAssessment(Base);
                //------------------------------------------------------------------
                changeId = string.Empty;
                changeTask1 = string.Empty;
                changeTask2 = string.Empty;
                changeTask3 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewChange()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_00_ApplyTemplate()
        {
            try
            {
                string templateMenu = Base.GData("Template_Menu");
                string template = Base.GData("Template");

                if (templateMenu.ToLower() != "no")
                {
                    flag = chg.SelectTemplate(templateMenu);
                    if (!flag) error = "Error when select template.";
                    else chg.WaitLoading();
                }
                else
                {
                    flag = chg.SelectTemplate("Templates;Apply Template");
                    if (flag)
                    {
                        Base.SwitchToPage(1);

                        

                        ItilList tpl = new ItilList(Base, "Search template");
                        flag = tpl.SearchAndOpen("Name", template, "Name=" + template, "Name", null, true);
                        if (!flag)
                            error = "Cannot select template";
                        Base.SwitchToPage(0);

                    }
                    else error = "Error when open template form.";
                }

                if (flag)
                {
                    lookup = chg.Lookup_Template();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(template, true);
                        if (!flag) error = "Invalid template name.";
                    }
                    else error = "Cannot get lookup template.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_Verify_AutoPopulate_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Verify_AutoPopulate_RequestedBy()
        {
            try
            {
                string temp = Base.GData("ChangeRequesterTemplate");
                lookup = chg.Lookup_RequestedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid requested by value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup requested by."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Verify_AutoPopulate_CI()
        {
            try
            {
                string temp = Base.GData("CI");
                lookup = chg.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid CI value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup CI."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_04_Verify_AutoPopulate_Impact()
        {
            try
            {
                string temp = Base.GData("Impact");
                combobox = chg.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid impact value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_05_Verify_AutoPopulate_Urgency()
        {
            try
            {
                string temp = Base.GData("Urgency");
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid urgency value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get combobox urgency."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_06_Verify_AutoPopulate_Priority()
        {
            try
            {
                string temp = Base.GData("Priority");
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid priority value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_07_Verify_AutoPopulate_ShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid short description value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_08_Verify_AutoPopulate_Type()
        {
            try
            {
                string temp = Base.GData("Typefield");
                combobox = chg.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid type value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get combobox type."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_10_Verify_AutoPopulate_Description()
        {
            try
            {
                string temp = Base.GData("Description");
                textarea = chg.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid description value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_11_SelectPlanningTab()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (!flag)
                    error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_12_Verify_AutoPopulate_Justification()
        {
            try
            {
                string temp = Base.GData("Justification");
                textarea = chg.Textarea_Justification();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid justification value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textarea justification."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_13_Verify_AutoPopulate_ChangePlan()
        {
            try
            {
                string temp = Base.GData("ChangePlan");
                textarea = chg.Textarea_ChangePlan();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid change plan value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textarea chang plan."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_14_Verify_AutoPopulate_ImplementationPlan()
        {
            try
            {
                string temp = Base.GData("ImplementationPlan");
                textarea = chg.Textarea_ImplementationPlan();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid implementation plan value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textarea implementation plan."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_15_Verify_AutoPopulate_BackoutPlan()
        {
            try
            {
                string temp = Base.GData("BackoutPlan");
                textarea = chg.Textarea_BackoutPlan();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid backout plan value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textarea backout plan."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_16_Verify_AutoPopulate_TestPlan()
        {
            try
            {
                string temp = Base.GData("TestPlan");
                textarea = chg.Textarea_TestPlan();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid test plan value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get textarea test plan."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_17_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    var date = DateTime.Now;
                    var nextSaturday = date.AddDays(6 - (int)date.DayOfWeek);
                    string startDate = nextSaturday.ToString("yyyy-MM-dd 01:mm:ss");
                    string endDate = nextSaturday.AddMinutes(5).ToString("yyyy-MM-dd 01:mm:ss");

                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            }
                            else error = "Cannot get datetime planned end date.";
                        }
                        else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_CheckTemplateIsPopulated()
        {
            try
            {
                string temp = Base.GData("Template");
                lookup = chg.Lookup_Template();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) error = "Invalid template name.";
                }
                else error = "Cannot get lookup template.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Populate_AssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                if (lookup.Existed)
                {
                    string temp = Base.GData("AssignmentGroup");
                    if (!lookup.VerifyCurrentValue(temp))
                    {
                        flag = lookup.Select(temp);
                        if (!flag)
                        {
                            error = "Cannot populate assignment group value.";
                        }
                        else
                        {
                            flag = chg.Save();
                            if (flag)
                                chg.WaitLoading();
                            else error = "Error when save change";
                        }
                    }
                }
                else
                {
                    error += "Cannot get lookup assignment group.";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_SaveChange()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeId = textbox.Text;
                    flag = chg.Save();
                    if (!flag)
                        error = "Error when save change.";
                    else
                        chg.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_VerifyValueInAffectedCIsTab()
        {
            try
            {
                string temp = Base.GData("CI");
                string conditions = "Configuration Item=" + temp;
                flag = chg.Verify_RelatedTable_Row("Affected CIs", conditions);
                if (!flag) error = "Invalid ci in list. Expected:(" + conditions + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Verify_ChangeTask_Tab()
        {
            try
            {
                string temp = Base.GData("TemplateHaveTask");
                if (temp.ToLower() != "no")
                {
                    /*Change Task 1*/
                    string condition = "Short description=" + Base.GData("ShortDescriptionTask1");
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", condition);
                    if (flag)
                    {
                        changeTask1 = chg.RelatedTable_Get_Cell_Text("Change Tasks", condition, "Number");
                    }
                    else
                    {
                        error = "Not found change task with condition: " + condition;
                        flagExit = false;
                    }

                    /*Change Task 2*/
                    condition = "Short description=" + Base.GData("ShortDescriptionTask2");
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", condition);
                    if (flag)
                    {
                        changeTask2 = chg.RelatedTable_Get_Cell_Text("Change Tasks", condition, "Number");
                    }
                    else
                    {
                        error = "Not found change task with condition: " + condition;
                        flagExit = false;
                    }

                    /*Change Task 3*/
                    condition = "Short description=" + Base.GData("ShortDescriptionTask3");
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", condition);
                    if (flag)
                    {
                        changeTask3 = chg.RelatedTable_Get_Cell_Text("Change Tasks", condition, "Number");
                    }
                    else
                    {
                        error = "Not found change task with condition: " + condition;
                        flagExit = false;
                    }
                }
                else
                    Console.WriteLine("*** This case change template have not change task ***.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_03_Verify_Cancel_Button_Visible()
        {
            try
            {
                button = chg.Button_Cancel();
                flag = button.Existed;
                if (!flag)
                    error = "Button Cancel is NOT visible.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_04_Verify_Schedule_Button_Visible()
        {
            try
            {
                button = chg.Button_Schedule();
                flag = button.Existed;
                if (!flag)
                    error = "Button Schedule is NOT visible.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_05_Verify_StateDropdown_Item_9_5()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Expected_Item_Of_State_9_5");
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                        error = "Invalid items of state combobox.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Click_Cancel_Button()
        {
            try
            {
                button = chg.Button_Cancel();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click on button cancel.";
                }
                else error = "Cannot get button cancel.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_Verify_Alert_Message()
        {
            try
            {
                string temp = "If you cancel this change it cannot be undone - Click OK to cancel the change, click Cancel if you do not want to continue";
                IAlert alert = Base.Driver.SwitchTo().Alert();
                if (temp != alert.Text)
                {
                    flag = false;
                    flagExit = false;
                    error = "Invalid message.";
                }
                alert.Dismiss();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_04_Click_Schedule_Button()
        {
            try
            {
                button = chg.Button_Schedule();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                        error = "Error when click on button cancel.";
                    else
                        chg.WaitLoading();
                }
                else error = "Cannot get button cancel.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_05_Verify_StateDropdown_Item_12_2()
        {
            try
            {
                Thread.Sleep(2000);
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Expected_Item_Of_State_12_2");
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                        error = "Invalid items of state combobox.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_06_Verify_State()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Scheduled");
                    if (!flag) error = "Invalid init state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////------------UPDATED by Thanh Tran-Rel 11.3-----------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_013_07_Verify_Missing_CI_field()
        //{
        //    try
        //    {
        //        checkbox = chg.Checkbox_Missing_Ci;
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            bool flagTemp = checkbox.Checked;
        //            if (flagTemp)
        //            {
        //                flag = false;
        //                error = "'Missing CI' field should NOT check.";
        //            }
        //        }
        //        else { error = "Cannot get checkbox Missing CI."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_014_CheckConflicts()
        //{
        //    try
        //    {
        //        tab = chg.GTab("Conflicts");
        //        //---------------------------------------
        //        int i = 0;
        //        while (tab == null && i < 5)
        //        {
        //            Thread.Sleep(2000);
        //            tab = chg.GTab("Conflicts", true);
        //            i++;
        //        }
        //        flag = tab.Header.Click(true);
        //        if (flag)
        //        {
        //            button = chg.Button_CheckConflicts;
        //            flag = button.Existed;
        //            if (flag)
        //            {
        //                flag = button.Click(true);
        //                if (flag)
        //                {
        //                    flag = chg.WaitCheckConflicts_Completed_And_CloseDialog();
        //                    if (flag)
        //                    {
        //                        combobox = chg.Combobox_ConflictStatus;
        //                        flag = combobox.Existed;
        //                        if (flag)
        //                        {
        //                            string expected = Base.GData("ConflictStatus");
        //                            flag = combobox.VerifyCurrentText(expected);
        //                            if (!flag)
        //                            {
        //                                error = "Invalid conflict status. Runtime:(" + combobox.Text + "). Expected:(" + expected + ")";
        //                                flagExit = false;
        //                            }

        //                        }
        //                        else error = "Cannot get combobox conflict status.";
        //                    }
        //                    else error = "Error when process check conflicts.";
        //                }
        //                else error = "Error when click on button check conflicts.";
        //            }
        //            else error = "Cannot get button check conflicts.";
        //        }
        //        else error = "Cannot select conflicts tab.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_015_ImpersonateUser_ChangeManager()
        //{
        //    try
        //    {
        //        string temp = Base.GData("ChangeManager");
        //        string rootuser = Base.GData("UserFullName");
        //        flag = home.ImpersonateUser(temp, true, rootuser);
        //        if (!flag) error = "Error when impersonate user change manager.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_016_SystemSetting()
        //{
        //    try
        //    {
        //        flag = home.SystemSetting();
        //        if (!flag) error = "Error when config system.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_017_Search_And_Open_Change()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (changeId == null || changeId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
        //            addPara.ShowDialog();
        //            changeId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        flag = home.LeftMenuItemSelect("Change", "Open");
        //        if (flag)
        //        {
        //            chglist.WaitLoading();
        //            temp = chglist.List_Title().MyText;
        //            flag = temp.Equals("Change Requests");
        //            if (flag)
        //            {
        //                flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
        //                if (!flag) error = "Error when search and open change (id:" + changeId + ")";
        //                else chg.WaitLoading();
        //            }
        //            else
        //            {
        //                error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
        //            }
        //        }
        //        else error = "Error when select open change.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_018_Verify_Delete_Button_NotVisible()
        //{
        //    try
        //    {
        //        button = chg.Button_Delete();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            flag = false;
        //            flagExit = false;
        //            error = "Button Delete is Vissible";
        //        }
        //        else flag = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_019_VerifyReadOnlyFields()
        //{
        //    try
        //    {
        //        string expectedReadOnlyField = Base.GData("Expected_ReadonlyFields");
        //        Dictionary<string, Auto.oelement> listNeed = new Dictionary<string, Auto.oelement>();
        //        string types = "textbox;textarea;lookup;combobox;checkbox;datetime";
        //        Auto.oelementlist list = null;

        //        list = chg.GControlOnForm(types);
        //        foreach (Auto.oelement e in list.MyList)
        //        {
        //            listNeed.Add(e.Label.ToLower(), e);
        //        }

        //        flag = chg.VerifyControls(expectedReadOnlyField, "all", listNeed, ref error, "readonly");
        //        if (!flag)
        //        {
        //            flagExit = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_020_ImpersonateUser_ChangeImplementer()
        //{
        //    try
        //    {
        //        string temp = Base.GData("ChangeImplementer");
        //        string rootuser = Base.GData("UserFullName");
        //        flag = home.ImpersonateUser(temp, true, rootuser);
        //        if (!flag) error = "Error when impersonate user change implementer.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_021_SystemSetting()
        //{
        //    try
        //    {
        //        flag = home.SystemSetting();
        //        if (!flag) error = "Error when config system.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_022_023_SearchAndOpenChange()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (changeId == null || changeId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
        //            addPara.ShowDialog();
        //            changeId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        flag = home.LeftMenuItemSelect("Change", "Open");
        //        if (flag)
        //        {
        //            chglist.WaitLoading();
        //            temp = chglist.List_Title().MyText;
        //            flag = temp.Equals("Change Requests");
        //            if (flag)
        //            {
        //                flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
        //                if (!flag) error = "Error when search and open change (id:" + changeId + ")";
        //                else chg.WaitLoading();
        //            }
        //            else
        //            {
        //                error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
        //            }
        //        }
        //        else error = "Error when select open change.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_024_VerifyReadOnlyFields()
        //{
        //    try
        //    {
        //        string expectedReadOnlyField = Base.GData("Expected_ReadonlyFields");
        //        Dictionary<string, Auto.oelement> listNeed = new Dictionary<string, Auto.oelement>();
        //        string types = "textbox;textarea;lookup;combobox;checkbox;datetime";
        //        Auto.oelementlist list = null;
        //        list = chg.GControlOnForm(types);
        //        foreach (Auto.oelement e in list.MyList)
        //        {
        //            listNeed.Add(e.Label.ToLower(), e);
        //        }
        //        flag = chg.VerifyControls(expectedReadOnlyField, "all", listNeed, ref error, "readonly");
        //        if (!flag)
        //        {
        //            flagExit = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_028_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_029_VerifyReadOnlyFields()
        //{
        //    try
        //    {
        //        string expectedReadOnlyField = Base.GData("Expected_ReadonlyFields");
        //        Dictionary<string, Auto.oelement> listNeed = new Dictionary<string, Auto.oelement>();
        //        string types = "textbox;textarea;lookup;combobox;checkbox;datetime";
        //        Auto.oelementlist list = null;
        //        list = chg.GControlOnForm(types);
        //        foreach (Auto.oelement e in list.MyList)
        //        {
        //            listNeed.Add(e.Label.ToLower(), e);
        //        }
        //        flag = chg.VerifyControls(expectedReadOnlyField, "all", listNeed, ref error, "readonly");
        //        if (!flag)
        //        {
        //            flagExit = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_1_ChangeStateToInProgress()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("In Progress");
                    if (!flag) error = "Error when combobox selec item.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_2_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error when save change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_ImpersonateUser_ChangeImplementer()
        {
            try
            {
                string temp = Base.GData("ChangeImplementer");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeImplementer.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_034_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_CloseAllChangeTask()
        {
            try
            {
                bool flagF = false;
                while (!flagF && flag)
                {
                    flagF = chg.Search_Verify_RelatedTable_Row("Change Tasks", "State", "=1", "State=Open", true);
                    if (!flagF)
                    {
                        chg.RelatedTableOpenRecord("Change Tasks", "State=Open", "Number");
                        chg.WaitLoading();
                        combobox = chg.Combobox_State();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem("Closed Complete");
                            if (flag)
                            {
                                button = chg.Button_CloseTask();
                                flag = button.Existed;
                                if (flag)
                                    flag = button.Click();
                            }
                            else error = "Cannot select state.";
                        }
                        else error = "Not found combobox state.";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_039_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_ChangeStateOfChange()
        {
            try
            {
                string state = Base.GData("ChangeCloseState");
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(state);
                    if (!flag) error = "Error when combobox selec item.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_PopulateActualDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    datetime = chg.Datetime_Planned_End_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        string start = Convert.ToDateTime(datetime.Text.Trim()).AddMinutes(-3).ToString("yyyy-MM-dd HH:mm:ss");
                        string end = Convert.ToDateTime(start).AddMinutes(1).ToString("yyyy-MM-dd HH:mm:ss");
                        datetime = chg.Datetime_Actual_Start_Date();
                        flag = datetime.Existed;
                        if (flag)
                        {
                            flag = datetime.SetText(start, true);
                            if (flag)
                            {
                                datetime = chg.Datetime_Actual_End_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    flag = datetime.SetText(end, true);
                                    if (!flag) error = "Error when input acutal end date.";
                                }
                                else error = "Cannot get datetime actual end date.";
                            }
                            else error = "Error when input value for actual start date.";
                        }
                        else error = "Cannot get datetime actual start date.";
                    }
                    else error = "Cannot get datetime planned end date.";
                }
                else error = "Cannot select tab (Schedule).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_ValidateCloseCodeANDInputCloseNotes()
        {
            try
            {
                flag = chg.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = chg.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("ChangeCloseCode");
                        flag = combobox.VerifyCurrentValue(temp);
                        if (flag)
                        {
                            textarea = chg.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("ChangeCloseNote") + ": " + Base.GData("ChangeCloseState");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                                else
                                {
                                    flag = chg.Save();
                                    if (flag) { chg.WaitLoading(); }
                                    else error = "Error when save change.";
                                }
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Invalid close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot click on Closure Information tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_43_01_Click_Compliance_tab()
        //{
        //    try
        //    {
        //        tab = chg.GTab("Compliance");
        //        int i = 0;
        //        while (tab == null && i < 5)
        //        {
        //            Thread.Sleep(2000);
        //            tab = chg.GTab("Compliance", true);
        //            i++;
        //        }
        //        flag = tab.Header.Click();
        //        if (!flag)
        //        {
        //            error = "Cannot click tab [Compliance]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_43_02_Verify_Checkbox_CWE()
        //{
        //    try
        //    {
        //        checkbox = chg.Checkbox_Change_window_exceeded;
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            bool flagTemp = checkbox.Checked;
        //            if (flagTemp)
        //            {
        //                flag = false;
        //                flagExit = false;
        //                error = "'Change window exceeded' box should NOT check.";
        //            }
        //        }
        //        else { error = "Cannot get checkbox Missing CI."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_44_01_Click_Schedule_tab()
        //{
        //    try
        //    {
        //        tab = chg.GTab("Schedule");
        //        int i = 0;
        //        while (tab == null && i < 10)
        //        {
        //            Thread.Sleep(2000);
        //            tab = chg.GTab("Schedule", true);
        //            i++;
        //        }
        //        flag = tab.Header.Click();
        //        if (!flag)
        //        {
        //            error = "Cannot click tab [Schedule]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_44_02_Update_Actual_end_date()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag) 
                {
                    datetime = chg.Datetime_Planned_End_Date();
                    string plannedEndDate = datetime.Text;

                    var date = DateTime.Parse(plannedEndDate);
                    var Sday = date.AddMinutes(-1);

                    string actualEndDate = Sday.ToString("yyyy-MM-dd hh:mm:ss");

                    datetime = chg.Datetime_Actual_End_Date();
                    flag = datetime.SetText(actualEndDate);
                    if (!flag) error = "Cannot populate Actual End Date.";
                }          

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_44_03_Save_Change()
        {
            try
            {
                flag = chg.Save();
                if (!flag)
                    error = "Error while save change.";
                else
                    chg.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_44_04_Click_Compliance_tab()
        //{
        //    try
        //    {
        //        tab = chg.GTab("Compliance");
        //        int i = 0;
        //        while (tab == null && i < 10)
        //        {
        //            Thread.Sleep(2000);
        //            tab = chg.GTab("Compliance", true);
        //            i++;
        //        }
        //        flag = tab.Header.Click();
        //        if (!flag)
        //        {
        //            error = "Cannot click tab [Compliance]";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_44_05_Verify_Checkbox_CWE()
        //{
        //    try
        //    {
        //        //Checkbox "Change window exceeded"
        //        checkbox = chg.Checkbox_Change_window_exceeded;
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            flag = checkbox.Checked;
        //            if (!flag)
        //            {
        //                error = "'Change window exceeded' box should BE checked.";
        //            }
        //        }
        //        else { error = "Cannot get checkbox Change window exceeded."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////----------UPDATED by Thanh Tran-Rel 11.3-------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_44_06_Verify_Checkbox_SCBWE()
        //{
        //    try
        //    {
        //        //Checkbox "Successful change but window exceeded"
        //        checkbox = chg.Checkbox_Change_window_exceeded;
        //        flag = checkbox.Existed;
        //        if (flag)
        //        {
        //            flag = checkbox.Checked;
        //            if (!flag)
        //            {
        //                error = "'Successful change but window exceeded' box should BE checked.";
        //            }
        //        }
        //        else { error = "Cannot get checkbox Successful change but window exceeded."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_45_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
