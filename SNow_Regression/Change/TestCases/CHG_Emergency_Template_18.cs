﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
namespace Change
{
    [TestFixture]
    public class CHG_Emergency_Template_18
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Change Task 1 Id: " + changeTask1);
            System.Console.WriteLine("Finished - Change Task 2 Id: " + changeTask2);
            System.Console.WriteLine("Finished - Change Task 3 Id: " + changeTask3);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snotextarea textarea;
        snodatetime datetime;
       
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;



        //------------------------------------------------------------------
        string changeId;
        string changeTask1, changeTask2, changeTask3;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                //------------------------------------------------------------------
                changeId = string.Empty;
                changeTask1 = string.Empty;
                changeTask2 = string.Empty;
                changeTask3 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Open system*/
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Log into SNOW*/
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Impersonate Change Coordinator*/
        public void Step_003_ImpersonateUser_ChangeCoordinator()
        {
            try
            {
                string temp = Base.GData("ChangeCoordinator");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*Change Domain*/
        public void Step_004_SystemSetting()
        {
            try
            {
                string temp = Base.GData("FullPathDomain");
                flag = home.SystemSetting(temp);
                if (!flag)
                {
                    error = "Error when config system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /* Select change type*/
        public void Step_005_SelectChangeType()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select Change Type";
                }
                else error = "Error When Open Change";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Apply Template*/
        public void Step_006_007_ApplyTemplate()
        {
            try
            {
                string templateMenu = Base.GData("Template_Menu");
                string template = Base.GData("Template");

                if (templateMenu.ToLower() != "no")
                {
                    flag = chg.SelectTemplate(templateMenu);
                    if (!flag) error = "Error when select template.";
                    else chg.WaitLoading();
                }
                else
                {
                    flag = chg.SelectTemplate("Templates;Apply Template");
                    if (flag)
                    {
                        Base.SwitchToPage(1);
                        ItilList tpl = new ItilList(Base, "Search template");
                        flag = tpl.SearchAndOpen("Name", template, "Name=" + template, "Name", null, true);
                        if (!flag)
                            error = "Cannot select template";
                        Base.SwitchToPage(0);

                    }
                    else error = "Error when open template form.";
                }

                /*Verify template field*/
                if (flag)
                {
                    lookup = chg.Lookup_Template();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(template, true);
                        if (!flag)
                        {
                            error = "Invalid template name.";
                            flagExit = false;
                        }
                    }
                    else error = "Cannot get lookup template.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verfiy company*/
        public void Step_008_01_Verify_Company()
        {
            try
            {
                /*Get Change ID number*/
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    changeId = textbox.Text;
                }
                else { error = "Cannot get Number textbox."; }

                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Company"), true);
                    if (!flag)
                    {
                        error = "Cannot verify Companny value.";
                        flagExit = false;
                    }
                }
                else error = "can not get lookup Company";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Category*/
        public void Step_008_02_Verify_Category()
        {
            try
            {
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(Base.GData("Category"), true);
                    if (!flag)
                    {
                        error = "Cannot verify Category value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox category";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify CI*/
        public void Step_008_03_Verify_CI()
        {
            try
            {
                lookup = chg.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("CI"));
                    if (!flag)
                    {
                        error = "Cannot verify CI value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get lookup CI";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Impact*/
        public void Step_008_04_Verify_Impact()
        {
            try
            {
                combobox = chg.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(Base.GData("Impact"));
                    if (!flag)
                    {
                        error = "Cannot verify Impact value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox Impact";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Urgency*/
        public void Step_008_05_Verify_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(Base.GData("Urgency"));
                    if (!flag)
                    {
                        error = "Cannot verify Urgency value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox Urgency";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Priority*/
        public void Step_008_06_Verify_Priority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(Base.GData("Priority"));
                    if (!flag)
                    {
                        error = "Cannot verify Priority value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox Priority";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;

            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify ShortDescription*/
        public void Step_008_07_Verify_ShortDescription()
        {
            try
            {
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(Base.GData("ShortDescription"));
                    if (!flag)
                    {
                        error = "Cannot verify Short description value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox ShortDescription";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Justification*/
        public void Step_008_08_Verify_Justification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.VerifyCurrentValue(Base.GData("Justification"));
                        if (!flag)
                        {
                            error = "Cannot verify Justification value.";
                            flagExit = false;
                        }
                    }
                    else error = "Cannot get textarea Justification";
                }
                else error = "Error when select (Planning) tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Type*/
        public void Step_008_09_Verify_Type()
        {
            try
            {
                combobox = chg.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(Base.GData("ChgType"));
                    if (!flag)
                    {
                        error = "Cannot verify Type value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox Type";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Verify Assignment Group*/
        public void Step_008_10_Verify_AssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("AssignmentGroup"));
                    if (!flag)
                    {
                        error = "Cannot verify Assignment group value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get Lookup Assignment Group";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*verify Description*/
        public void Step_008_11_Verfiy_Description()
        {
            try
            {
                textarea = chg.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.VerifyCurrentValue(Base.GData("Description"));
                    if (!flag)
                    {
                        error = "Cannot verify Description value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textarea Description";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*populate Planned Start date/ Planned End date*/
        public void Step_009_01_Populate_PlannedStartDate_PlannedEnddate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not populate Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot populate planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        [Test]
        /*Update category*/
        public void Step_009_02_Update_Category()
        {
            try
            {
                string temp = Base.GData("Category2");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Cannot populate Category value.";
                }
                else error = "Cannot get combobox category";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Update Short Description*/
        public void Step_009_03_Update_ShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription2");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) error = "Cannot update Short Description";
                }
                else error = "Cannot get textbox short description";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Update Assignment Group*/
        public void Step_009_04_Update_AssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup2");
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) error = "Cannot populate Assignment group value.";
                }
                else error = "Cannot get lookup Assignment group";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Update Asignee*/
        public void Step_009_05_Update_Asignee()
        {
            try
            {
                string temp = Base.GData("Assignee");
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) error = "Cannot populate Assignee.";
                }
                else error = "Cannot get lookup Assignee lookup.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        /*update Planned Start date / Planned End date*/
        public void Step_009_06_Update_PlannedStartDate_PlannedEndDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    string Startdate = DateTime.Now.AddDays(7).ToString("yyyy-MM-dd HH:mm:ss");
                    string EndDate = DateTime.Now.AddDays(8).ToString("yyyy-MM-dd HH:mm:ss");
                    datetime = chg.Datetime_Planned_Start_Date();
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(Startdate);
                        if (flag)
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag)
                            {
                                flag = datetime.SetText(EndDate);
                                if (!flag) error = "Can not update Planned End Date";
                            }
                            else error = "Cannot get datetime planned end date";
                        }
                        else error = "Cannot update planned start date";
                    }
                    else error = "Cannot get DateTime Planned Start Date";
                }
                else error = "Error when select (Schedule) tab";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        /*Save Change*/
        public void Step_009_07_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag) error = "Error when save change";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_01_Verify_State()
        {
            try
            {
                string temp = "Review";
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify State value.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get State combobox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_02_Verify_ChangeTask_Generated()
        {
            try
            {
                string temp = Base.GData("TemplateHaveTask");
                if (temp.ToLower() != "no")
                {
                    /*Change Task 1*/
                    string condition = "Short description=" + Base.GData("ShortDescriptionTask1");
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", condition);
                    if (flag)
                    {
                        changeTask1 = chg.RelatedTable_Get_Cell_Text("Change Tasks", condition, "Number");
                    }
                    else
                    {
                        error = "Cannot verify row with condition: " + condition;
                        flagExit = false;
                    }

                    /*Change Task 2*/
                    condition = "Short description=" + Base.GData("ShortDescriptionTask2");
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", condition);
                    if (flag)
                    {
                        changeTask2 = chg.RelatedTable_Get_Cell_Text("Change Tasks", condition, "Number");
                    }
                    else
                    {
                        error = "Cannot verify row with condition: " + condition;
                        flagExit = false;
                    }

                    /*Change Task 3*/
                    condition = "Short description=" + Base.GData("ShortDescriptionTask3");
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", condition);
                    if (flag)
                    {
                        changeTask3 = chg.RelatedTable_Get_Cell_Text("Change Tasks", condition, "Number");
                    }
                    else
                    {
                        error = "Cannot verify row with condition: " + condition;
                        flagExit = false;
                    }
                }
                else
                    Console.WriteLine("*** This Change does not have Change Task generated.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
