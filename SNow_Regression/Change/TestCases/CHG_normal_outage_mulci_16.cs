﻿using Auto;
using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
namespace Change
{
    [TestFixture]
    public class CHG_normal_outage_mulci_16
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);
            System.Console.WriteLine("Finished - Change Task 1 Id: " + cTaskId1);
            System.Console.WriteLine("Finished - Change Task 2 Id: " + cTaskId2);
            System.Console.WriteLine("Finished - Change Task 3 Id: " + cTaskId3);
            System.Console.WriteLine("Finished - Change Task 4 Id: " + cTaskId4);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snodatetime datetime;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;
        EmailList emailList = null;
        Email email = null;
        ItilList approvalList = null;       
        Member member = null;
        RiskAssessment riskAss = null;
        //------------------------------------------------------------------
        string changeId;
        string scheduleDt;
        string approvalRequestURL;
        string cTaskId1, cTaskId2, cTaskId3, cTaskId4;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                emailList = new EmailList(Base, "Email list");
                approvalList = new  ItilList(Base, "Approval list");                
                member = new Member(Base);
                email = new Email(Base, "Email");
                emailList = new EmailList(Base, "Email List");
                riskAss = new RiskAssessment(Base);
                //------------------------------------------------------------------
                changeId = string.Empty;
                approvalRequestURL = string.Empty;                
                scheduleDt = Base.GData("ScheduleDate");
                cTaskId1 = string.Empty;
                cTaskId2 = string.Empty;
                cTaskId3 = string.Empty;
                cTaskId4 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_003_ImpersonateUser_ChangeCoordinator()
        {
            try
            {
                string temp = Base.GData("ChangeCoordinator");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_004_005_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_01_OpenNewChange()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_02_Store_Number()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        changeId = textbox.Text;
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_03_Verify_State_Draft()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (!flag) error = "Invalid init state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_00_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_02_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_03_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_PopulateCI()
        {
            try
            {

                string temp = Base.GData("ConfigurationItem1");
                lookup = chg.Lookup_ConfigurationItem();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate CI value."; }
                }
                else { error = "Cannot get combobox CIs."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_01_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    if (scheduleDt.ToLower() != "no" && scheduleDt.ToLower() != string.Empty)
                    {
                        if (scheduleDt.Contains("|") == true)
                        {
                            string weekday = scheduleDt.Split('|')[0].Trim();
                            string Stime = scheduleDt.Split('|')[1].Trim();

                            if (weekday == string.Empty || Stime == string.Empty)
                            {
                                flag = false;
                                error = "Cannot identify the schedule as missing the day OR start time the data file.";
                            }
                            else
                            {
                                int sDayOfWeek = (int)Enum.Parse(typeof(DayOfWeek), weekday, true);

                                var date = DateTime.Now;
                                var Sday = date.AddDays(7 - (int)date.DayOfWeek + sDayOfWeek);
                                string startDate = Sday.ToString("yyyy-MM-dd " + Stime + ":mm:ss");
                                string endDate = Sday.AddMinutes(1).ToString("yyyy-MM-dd " + Stime + ":mm:ss");

                                datetime = chg.Datetime_Planned_Start_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    flag = datetime.SetText(startDate, true);
                                    if (flag)
                                    {
                                        datetime = chg.Datetime_Planned_End_Date();
                                        flag = datetime.Existed;
                                        if (flag)
                                        {
                                            flag = datetime.SetText(endDate, true);
                                            if (!flag) error = "Cannot populate planned end date.";
                                        }
                                        else { error = "Cannot get datetime planned end date."; }
                                    }
                                    else { error = "Cannot populate planned start date."; }
                                }
                                else { error = "Cannot get datetime planned start date."; }
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "The schedule is input with wrong format in the data file.";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Cannot identify the schedule as missing the day AND start time in the data file.";
                    }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_02_Populate_BackoutPlan()
        {
            try
            {              
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    textarea = chg.Textarea_BackoutPlan();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("BackoutPlan");
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Cannot populate Backout plan value.";
                        }
                    }
                    else { error = "Cannot get Backout Plan textarea."; }
                }
                else
                {
                    error = "Cannot switch to (Planning) tab.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_03_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("4 - Low");
                    if (!flag) { error = "Cannot populate priority value."; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_04_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Cannot save Change."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_01_Populate_AssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment group value."; }
                }
                else { error = "Cannot get Assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_02_Populate_AssignTo()
        {
            try
            {
                lookup = chg.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("TechnicalApprover");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assignment to value."; }
                }
                else { error = "Cannot get Assignment to control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_03_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Cannot save Change."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_AddCI_in_AffectedCIs()
        {
            try
            {
                string temp = Base.GData("ConfigurationItem2");
                flag = chg.Add_Related_Members("Affected CIs", temp);
                if (!flag)
                    error = "Cannot open add ci";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Cannot save Change."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_CheckConflicts()
        {
            try
            {
                flag = chg.Select_Tab("Conflicts");
                if (flag == true)
                {
                    button = chg.Button_CheckConflict();
                    if (button.MyElement.Enabled == true)
                    {
                        button.Click();
                        chg.WaitLoading();
                        flag = chg.WaitCheckConflicts_Completed_And_CloseDialog();
                        if (flag == true)
                        {
                            changeId = chg.Textbox_Number().Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_Verify_BlackOutInConflictList()
        {
            try
            {
                string tabDefine = "span[id^='section'][class$='tab_section']>span:not([style='display: none;'])";
                flag = chg.Select_Tab("Conflicts");
                if (flag == true)
                {
                    string affectedCI = Base.GData("ConfigurationItem1");
                    string conditions = "Affected CI=" + affectedCI + "|Type=Blackout";
                    flag = chg.Verify_RelatedTable_Row("Conflicts", conditions, false, tabDefine);

                    if (flag == false)
                    {
                        error = "There is no blackout record in Conflict list.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_01_Click_RiskAssessment_Tab()
        {
            try
            {
                flag = chg.Select_Tab("Risk Assessment");
                if (!flag)
                {
                    error = "Cannot open (Risk Assessment) tab";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_Open_RiskAssessmentForm()
        {
            try
            {
                button = chg.Button_FillOutRiskAssessment();
                flag = button.Existed;
                if (!flag)
                    error = "Cannot get button fill out risk assessment.";
                else
                {
                    flag = button.Click();
                    if (flag)
                    {
                        riskAss.WaitLoading();

                        Thread.Sleep(2000);

                        // Verify Dialog title
                        snoelement ele = riskAss.Title();
                        flag = ele.Existed;
                        if (flag)
                        {
                            flag = ele.MyText.Equals("Risk Assessment");
                            if (!flag)
                            {
                                error = "Invalid title.";
                            }
                        }
                        else
                        {
                            error = "Cannot get label.";
                        }
                    }
                    else error = "Error when click on button fill out risk assessment";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_03_Fill_RiskAssessmentForm()
        {
            try
            {
                flag = riskAss.SelectRiskQuestionsAndAnswers(Base.GData("RiskAssessment_QA"));
                if (!flag)
                {
                    error = "Cannot answer Risk Assessment questions";
                }
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_04_Submit_RiskAssessmentForm()
        {
            try
            {
                oelement ele = riskAss.Form_Submit();
                ele.MyElement.Submit();
                Thread.Sleep(2000);
                button = riskAss.Button_Close();
                flag = button.Existed;
                if (flag)
                    button.Click();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_05_Reload_Form()
        {
            try
            {
                flag = chg.ReloadForm();
                if (flag)
                    chg.WaitLoading();
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_06_VerifyCompleted_RiskAssessment()
        {
            try
            {
                datetime = chg.Datetime_Risk_Assessment_Last_Run();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.Text.Trim() != "";
                }

                if (!flag) Console.WriteLine("*** Error: Cannot fill and execute risk assessment.");
            }
            catch (Exception wex)
            {
                flag = false;
                error = wex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_07_RequestForReview()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else error = "Error when click on request approval button.";
                }
                else error = "Cannot get button request approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_08_Verify_State_Review()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Review";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) error = "Invalid init state value. Expect: " + temp;
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_09_RequestForApproval()
        {
            try
            {
                button = chg.Button_RequestApproval_RequestReview();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else error = "Error when click on request approval button.";
                }
                else error = "Cannot get button request approval.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_10_Verify_State_Approval()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Approval";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) error = "Invalid init state value. Expect: " + temp;
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_ImpersonateUser_TechnicalApprover()
        {
            try
            {
                string temp = Base.GData("TechnicalApprover");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_022_SearchAndOpen_Change()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Service Desk", "My Change Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                    string conditions = "State=Requested";
                    flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open incident (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_Click_ApproveButoon()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else { error = "Cannot click Approve button"; }
                }
                else { error = "Cannot get Approve button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_ImpersonateUser_ChangeManagerReview()
        {
            try
            {
                string temp = Base.GData("ChangeManager");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_027_SearchAndOpen_Change()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Service Desk", "My Change Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                    temp = approvalList.List_Title().MyText;
                    flag = temp.Equals("Approvals");
                    if (flag)
                    {
                        string conditions = "State=Requested";
                        flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                        if (!flag) error = "Error when search and open incident (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_Click_ApproveButoon()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else { error = "Cannot click Approve button"; }
                }
                else { error = "Cannot get Approve button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_ImpersonateUser_BusinessApprover()
        {
            try
            {
                string temp = Base.GData("BusinessApprover");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_032_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Service Desk", "My Change Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                    temp = approvalList.List_Title().MyText;
                    flag = temp.Equals("Approvals");
                    if (flag)
                    {
                        string conditions = "State=Requested";
                        flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                        if (!flag) error = "Error when search and open incident (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Approvals)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else { error = "Cannot click Approve button"; }
                }
                else { error = "Cannot get Approve button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_ImpersonateUser_CABApprover()
        {
            try
            {
                string temp = Base.GData("CABApprover");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_037_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Service Desk", "My Change Approvals");
                if (flag)
                {
                    approvalList.WaitLoading();
                    string conditions = "State=Requested";
                    flag = approvalList.SearchAndOpen("Approval for", changeId, conditions, "State");
                    if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                    else chg.WaitLoading();
                }
                else error = "Error when select open my approvals.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_ClickApproveButton()
        {
            try
            {
                button = chg.Button_Approve();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        approvalList.WaitLoading();
                    }
                    else { error = "Cannot click Approve button"; }
                }
                else { error = "Cannot get Approve button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_ImpersonateUser_ChangeCoordinator()
        {
            try
            {

                string temp = Base.GData("ChangeCoordinator");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_042_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Change Requests)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_VerifyStateValue_Scheduled()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Scheduled";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_01_OpenChangeTask_1()
        {
            try
            {
                flag = chg.Select_Tab("Change Tasks");
                if (flag)
                {
                    /*Get Chang Task 1 Id*/
                    string conditions = "Short description=1 Execute Implementation Plan|State=Open";
                    cTaskId1 = chg.RelatedTable_Get_Cell_Text("Change Tasks", conditions, "Number");

                    flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                    if (!flag)
                    {
                        error = "Error when open first change task: " + conditions;
                    }
                    else chg.WaitLoading();
                }
                else { error = "Cannot select Change Tasks tab."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_02_PopulateState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_03_PopulateAssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assignment group value.";
                    }
                }
                else { error = "Cannot get assignment group control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_04_UpdateChangeTask()
        {
            try
            {

                flag = chg.Update(true);
                if (!flag)
                {
                    error = "Cannot update Task.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_05_OpenChangeTask_2()
        {
            try
            {
                
                flag = chg.Select_Tab("Change Tasks");
                if (flag)
                {
                    /*Get Change Task 2 ID*/
                    string conditions = "Short description=2 Execute Test Plan|State=Open";
                    cTaskId2 = chg.RelatedTable_Get_Cell_Text("Change Tasks", conditions, "Number");

                    flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                    if (!flag)
                    {
                        error = "Error when open second change task: " + conditions;
                    }
                    else chg.WaitLoading();
                }
                else { error = "Cannot select Change Tasks tab."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_06_PopulateState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_07_PopulateAssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assignment group value.";
                    }
                }
                else { error = "Cannot get Assingment group control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_08_UpdateChangeTask()
        {
            try
            {
                flag = chg.Update(true);
                if (!flag)
                {
                    error = "Cannot update Task.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_09_OpenChangeTask_3()
        {
            try
            {
                flag = chg.Select_Tab("Change Tasks");
                if (flag)
                {
                    /*Get Change Task 3 Id*/
                    string conditions = "Short description=3 Record Test Results|State=Open";
                    cTaskId3 = chg.RelatedTable_Get_Cell_Text("Change Tasks", conditions, "Number");

                    flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                    if (!flag)
                    {
                        error = "Error when open third change task: " + conditions;
                    }
                    else chg.WaitLoading();
                }
                else { error = "Cannot select Change Tasks tab."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_10_PopulateState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_11_PopulateAssignmentGroup()
        {
            try
            {
                lookup = chg.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("AssignmentGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate assignment group value.";
                    }
                }
                else { error = "Cannot get assignment group control."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_12_UpdateChangeTask()
        {
            try
            {
                flag = chg.Update(true);
                if (!flag)
                {
                    error = "Cannot update Task.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_Verify_State_InProgress()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In Progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify State value. Actual: (" + combobox.Text + "). Expected: (" + temp + ")";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_Populate_ActualOutageDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                Thread.Sleep(3000);
                if (flag)
                {
                    if (scheduleDt.ToLower() != "no" && scheduleDt.ToLower() != string.Empty)
                    {
                        if (scheduleDt.Contains("|") == true)
                        {
                            string weekday = scheduleDt.Split('|')[0].Trim();
                            string Stime = scheduleDt.Split('|')[1].Trim();

                            if (weekday == string.Empty || Stime == string.Empty)
                            {
                                flag = false;
                                error = "Cannot identify the schedule as missing the day OR start time the data file.";
                            }
                            else
                            {
                                int sDayOfWeek = (int)Enum.Parse(typeof(DayOfWeek), weekday, true);

                                var date = DateTime.Now;
                                var Sday = date.AddDays(7 - (int)date.DayOfWeek + sDayOfWeek);
                                string startDate = Sday.ToString("yyyy-MM-dd " + Stime + ":mm:ss");
                                string endDate = Sday.AddMinutes(1).ToString("yyyy-MM-dd " + Stime + ":mm:ss");

                                datetime = chg.Datetime_Actual_Outage_Start_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    flag = datetime.SetText(startDate, true);
                                    if (flag)
                                    {
                                        datetime = chg.Datetime_Actual_Outage_End_Date();
                                        flag = datetime.Existed;
                                        if (flag)
                                        {
                                            flag = datetime.SetText(endDate, true);
                                            if (!flag) error = "Cannot populate actual outage end date.";
                                        }
                                        else { error = "Cannot get datetime actual outage end date."; }
                                    }
                                    else { error = "Cannot populate actual outage start date."; }
                                }
                                else { error = "Cannot get datetime actual outage start date."; }
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "The schedule is input with wrong format in the data file.";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Cannot identify the schedule as missing the day AND start time in the data file.";
                    }
                }
                else error = "Cannot select tab (Schedule).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_Populate_ActualDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    if (scheduleDt.ToLower() != "no" && scheduleDt.ToLower() != string.Empty)
                    {
                        if (scheduleDt.Contains("|") == true)
                        {
                            string weekday = scheduleDt.Split('|')[0].Trim();
                            string Stime = scheduleDt.Split('|')[1].Trim();

                            if (weekday == string.Empty || Stime == string.Empty)
                            {
                                flag = false;
                                error = "Cannot identify the schedule as missing the day OR start time the data file.";
                            }
                            else
                            {
                                int sDayOfWeek = (int)Enum.Parse(typeof(DayOfWeek), weekday, true);

                                var date = DateTime.Now;
                                var Sday = date.AddDays(7 - (int)date.DayOfWeek + sDayOfWeek);
                                string startDate = Sday.ToString("yyyy-MM-dd " + Stime + ":mm:ss");
                                string endDate = Sday.AddMinutes(1).ToString("yyyy-MM-dd " + Stime + ":mm:ss");

                                datetime = chg.Datetime_Actual_Start_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    flag = datetime.SetText(startDate, true);
                                    if (flag)
                                    {
                                        datetime = chg.Datetime_Actual_End_Date();
                                        flag = datetime.Existed;
                                        if (flag)
                                        {
                                            flag = datetime.SetText(endDate, true);
                                            if (!flag) error = "Cannot populate actual end date.";
                                        }
                                        else { error = "Cannot get datetime actual end date."; }
                                    }
                                    else { error = "Cannot populate actual start date."; }
                                }
                                else { error = "Cannot get datetime actual start date."; }
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "The schedule is input with wrong format in the data file.";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Cannot identify the schedule as missing the day AND start time in the data file.";
                    }
                }
                else error = "Cannot select tab (Schedule).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_01_Populate_State()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Completed with Issues");
                    if (!flag) error = "Invalid state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_02_VerifyCloseCode()
        {
            try
            {
                flag = chg.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = chg.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("ChangeCloseCode");
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag) { error = "Cannot verify Close Code value. Expected: " + temp; }
                    }
                    else { error = "Cannot get combobox Close Code."; }
                }
                else { error = "Cannot select Closure Information tab."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_03_PopulateCloseNotes()
        {
            try
            {
                textarea = chg.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("ChangeCloseNote");
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot verify Close Notes value"; }
                }
                else { error = "Cannot get combobox Close Notes."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_04_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (!flag) { error = "Cannot save Change."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_VerifyAndOpen_PIRTask()
        {
            try
            {
                flag = chg.Select_Tab("Change Tasks");
                if (flag)
                {
                    /*Get Change Task 4 Id*/
                    string conditions = "Short description=6 PIR Remediation|State=Open";
                    cTaskId4 = chg.RelatedTable_Get_Cell_Text("Change Tasks", conditions, "Number");

                    flag = chg.RelatedTableOpenRecord("Change Tasks", conditions, "Number");
                    if (!flag)
                    {
                        error = "Error when open PIR task: " + conditions;
                    }
                    else chg.WaitLoading();
                }
                else error = "Cannot select tab (Change Tasks).";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_01_Populate_TaskState()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_02_UpdateChangeTask()
        {
            try
            {
                flag = chg.Update(true);
                if (!flag) { error = "Cannot update Task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_03_Verify_State_PIRTask()
        {
            try
            {
                flag = chg.Select_Tab("Change Tasks");
                if (flag)
                {
                    string conditions = "Short description=6 PIR Remediation|State=Closed Complete";
                    flag = chg.Verify_RelatedTable_Row("Change Tasks", conditions);
                    if (!flag)
                    {
                        error = "Error when verify PIR task: " + conditions;
                        flagExit = false;
                    }
                    else chg.WaitLoading();
                }
                else error = "Cannot select tab (Change Tasks).";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_ImpersonateUser_TestUser()
        {
            try
            {
                string temp = Base.GData("TestUser");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate support user (" + temp + ")";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_02_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_01_FilterAndOpen_Email_CABApprover_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("CABApproverEmail");
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change Request " + changeId + " Approval request";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("CABApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDescription");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_06_Verify_Email_Company()
        {
            try
            {
                string expected = "Company: " + Base.GData("Company");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Company value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_07_Verify_Email_ApprovalGroup()
        {
            try
            {
                string expected = "Approval Group: " + Base.GData("CABGroup");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Approvae Group value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_08_Verify_Email_CI()
        {
            try
            {
                string expected = "Configuration Item: " + Base.GData("ConfigurationItem1");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid CI value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_09_Verify_Email_RequestedBy()
        {
            try
            {
                string expected = "Requested By: " + Base.GData("ChangeCoordinator");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Requested By value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_10_Verify_Emai_ApproveTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to approve " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approve message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_11_Verify_Email_RejectTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to reject " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Reject message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_12_Verify_Email_ApprovalLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Approval Request: Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approval link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_13_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_14_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_055_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_01_FilterAndOpen_Email_BusinessApprover_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("BusinessApproverEmail");
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change Request " + changeId + " Approval request";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("BusinessApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDescription");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_06_Verify_Email_Company()
        {
            try
            {
                string expected = "Company: " + Base.GData("Company");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Company value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_07_Verify_Email_ApprovalGroup()
        {
            try
            {
                string expected = "Approval Group: " + Base.GData("BusinessApproverGroup");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Approval Group value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_08_Verify_Email_CI()
        {
            try
            {
                string expected = "Configuration Item: " + Base.GData("ConfigurationItem1");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid CI value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_09_Verify_Email_RequestedBy()
        {
            try
            {
                string expected = "Requested By: " + Base.GData("ChangeCoordinator");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Requested By value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_10_Verify_Emai_ApproveTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to approve " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approve message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_11_Verify_Email_RejectTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to reject " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Reject message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_12_Verify_Email_ApprovalLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Approval Request: Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approval link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_13_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_14_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_057_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_01_FilterAndOpen_Email_ChangeManager_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("ChangeManagerEmail");
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change Request " + changeId + " Approval request";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("ChangeManagerEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDescription");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_06_Verify_Email_Company()
        {
            try
            {
                string expected = "Company: " + Base.GData("Company");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Company value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_07_Verify_Email_ApprovalGroup()
        {
            try
            {
                string expected = "Approval Group: " + Base.GData("ChangeManagerGroup");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Approval Group value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_08_Verify_Email_CI()
        {
            try
            {
                string expected = "Configuration Item: " + Base.GData("ConfigurationItem1");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid CI value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_09_Verify_Email_RequestedBy()
        {
            try
            {
                string expected = "Requested By: " + Base.GData("ChangeCoordinator");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Requested By value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_10_Verify_Emai_ApproveTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to approve " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approve message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_11_Verify_Email_RejectTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to reject " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Reject message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_12_Verify_Email_ApprovalLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Approval Request: Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approval link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_13_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_14_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_059_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_01_FilterAndOpen_Email_TechnicalApprover_ApprovalRequest()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("TechnicalApproverEmail");
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;Approval request|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change Request " + changeId + " Approval request";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDescription");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_06_Verify_Email_Company()
        {
            try
            {
                string expected = "Company: " + Base.GData("Company");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Company value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_07_Verify_Email_ApprovalGroup()
        {
            try
            {
                string expected = "Approval Group: " + Base.GData("AssignmentGroup");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Approval Group value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_08_Verify_Email_CI()
        {
            try
            {
                string expected = "Configuration Item: " + Base.GData("ConfigurationItem1");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid CI value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_09_Verify_Email_RequestedBy()
        {
            try
            {
                string expected = "Requested By: " + Base.GData("ChangeCoordinator");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Requested By value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_10_Verify_Emai_ApproveTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to approve " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approve message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_11_Verify_Email_RejectTicketId()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------

                temp = "Click here to reject " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Reject message.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_12_Verify_Email_ApprovalLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Approval Request: Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Approval link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_13_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_14_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_01_FilterAndOpen_Email_AssignToGroup()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("TechnicalApproverEmail");
                temp = "Subject;contains;" + changeId + "|and|Subject;contains;has been assigned|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change " + changeId + " has been assigned to you";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDescription");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_06_Verify_Email_AssignmentGroup()
        {
            try
            {
                string expected = "Assignment Group: " + Base.GData("AssignmentGroup");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Approval Group value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_07_Verify_Email_CI()
        {
            try
            {
                string expected = "Configuration Item: " + Base.GData("ConfigurationItem1");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid CI value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_08_Verify_Email_OpenedBy()
        {
            try
            {
                string expected = "Opened by: " + Base.GData("ChangeCoordinator");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid Opened By value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_09_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && changeId == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Request: " + changeId;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_10_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_063_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_01_FilterAndOpen_Email_cTask1_Closed()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId1 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 1 Id.");
                    addPara.ShowDialog();
                    cTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("TechnicalApproverEmail");
                temp = "Subject;contains;" + cTaskId1 + "|and|Subject;contains;has been Closed|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId1 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 1 Id.");
                    addPara.ShowDialog();
                    cTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change task " + cTaskId1 + " has been Closed";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDes_PIR1");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_06_Verify_Email_State()
        {
            try
            {
                string expected = "State: Closed Complete";
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid email value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------


        [Test]
        public void Step_064_07_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId1 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Task 1 Id.");
                    addPara.ShowDialog();
                    cTaskId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Task: " + cTaskId1;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_08_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_065_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_01_FilterAndOpen_Email_cTask2_Closed()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId2 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 2 Id.");
                    addPara.ShowDialog();
                    cTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("TechnicalApproverEmail");
                temp = "Subject;contains;" + cTaskId2 + "|and|Subject;contains;has been Closed|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId2 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 2 Id.");
                    addPara.ShowDialog();
                    cTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change task " + cTaskId2 + " has been Closed";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDes_PIR2");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_06_Verify_Email_State()
        {
            try
            {
                string expected = "State: Closed Complete";
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid email value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------


        [Test]
        public void Step_066_07_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId2 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Task 2 Id.");
                    addPara.ShowDialog();
                    cTaskId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Task: " + cTaskId2;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_08_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_01_FilterAndOpen_Email_cTask3_Closed()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId3 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 3 Id.");
                    addPara.ShowDialog();
                    cTaskId3 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("TechnicalApproverEmail");
                temp = "Subject;contains;" + cTaskId3 + "|and|Subject;contains;has been Closed|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId3 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 3 Id.");
                    addPara.ShowDialog();
                    cTaskId3 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change task " + cTaskId3 + " has been Closed";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDes_PIR3");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_06_Verify_Email_State()
        {
            try
            {
                string expected = "State: Closed Complete";
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid email value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------


        [Test]
        public void Step_068_07_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId3 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Task 3 Id.");
                    addPara.ShowDialog();
                    cTaskId3 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Task: " + cTaskId3;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_08_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_01_FilterAndOpen_Email_cTask4_Closed()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId4 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 4 Id.");
                    addPara.ShowDialog();
                    cTaskId4 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string cab_email = Base.GData("TechnicalApproverEmail");
                temp = "Subject;contains;" + cTaskId4 + "|and|Subject;contains;has been Closed|and|" + "Recipients;contains;" + cab_email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                    string condition = "Recipients=" + cab_email;
                    flag = emailList.Open(condition, "Created");
                    if (!flag)
                    {
                        error = "Cannot open email with condition: " + condition;
                    }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_02_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId4 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Change Task 4 Id.");
                    addPara.ShowDialog();
                    cTaskId4 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "Change task " + cTaskId4 + " has been Closed";
                flag = email.VerifySubject(expected);
                if (!flag)
                {
                    error = "Invalid subject value. Expected: " + expected;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_03_Verify_Email_Recipient()
        {
            try
            {
                string recipient = Base.GData("TechnicalApproverEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag)
                {
                    error = "Invalid recipient value. Expected: " + recipient;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_05_Verify_Email_ShortDescription()
        {
            try
            {
                string expected = "Short Description: " + Base.GData("ShortDes_PIR6");
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid short description value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_06_Verify_Email_State()
        {
            try
            {
                string expected = "State: Closed Complete";
                flag = email.VerifyEmailBody(expected);
                if (!flag)
                {
                    error = "Invalid email value in body.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------


        [Test]
        public void Step_070_07_Verify_Email_ChangeLink()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && cTaskId4 == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input Change Task 4 Id.");
                    addPara.ShowDialog();
                    cTaskId4 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //--------------------------------------------------------------------------------
                temp = "Click here to view Change Task: " + cTaskId4;
                flag = email.VerifyEmailBody(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid Change link detail.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_08_Close_PreviewEmailHtml()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
