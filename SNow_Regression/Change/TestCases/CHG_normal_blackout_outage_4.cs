﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using SNow;
namespace Change
{
    [TestFixture]
    public class CHG_normal_blackout_outage_4
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Change Id: " + changeId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snobutton button;
        snotextarea textarea;
        snodatetime datetime;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Change chg = null;
        ChangeList chglist = null;
        EmailList emailList = null;
        ItilList approvalList = null;
        Email email = null;
        //------------------------------------------------------------------
        string changeId, scheduleDt;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                chg = new SNow.Change(Base, "Change");
                chglist = new ChangeList(Base, "Change list");
                approvalList = new ItilList(Base, "Approval list");
                emailList = new EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                //------------------------------------------------------------------
                changeId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ChangeRequester()
        {
            try
            {
                string temp = Base.GData("ChangeRequester");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_0_OpenNewChange()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Change", "Create New");
                if (flag)
                {
                    chg.WaitLoading();
                    string temp = Base.GData("ChangeType");
                    flag = chg.Select_Change_Type(temp);
                    if (!flag) error = "Error when select change type.";
                    else chg.WaitLoading();
                }
                else
                    error = "Error when create new change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_1_Verify_Number()
        {
            try
            {
                textbox = chg.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "CHG*");
                        if (!flag) error = "Invalid format of change number.";
                        else { changeId = temp; Console.WriteLine("-*-[STORE]: Change Id:(" + changeId + ")"); } 
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_2_Verify_Impact()
        {
            try
            {
                combobox = chg.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("-- None --");
                    if (!flag) error = "Invalid init impact value.";
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_3_Verify_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("-- None --");
                    if (!flag) error = "Invalid init urgency value.";
                }
                else { error = "Cannot get combobox urgency."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_4_Verify_Priority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("-- None --");
                    if (!flag) error = "Invalid init priority value.";
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_5_Verify_Category()
        {
            try
            {
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("-- None --");
                    if (!flag) error = "Invalid init category value.";
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_6_Verify_State_Draft()
        {
            try
            {
                combobox = chg.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Draft");
                    if (!flag) error = "Invalid init state value.";
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_7_Verify_Risk()
        {
            try
            {
                combobox = chg.Combobox_Risk();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("None");
                    if (!flag) error = "Invalid init risk value.";
                }
                else { error = "Cannot get combobox risk."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_8_Verify_Type()
        {
            try
            {
                combobox = chg.Combobox_Type();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Normal");
                    if (!flag) error = "Invalid init type value.";
                }
                else { error = "Cannot get combobox type."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_1_PopulateCompany()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = chg.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_2_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = chg.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_3_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = chg.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_4_PopulateJustification()
        {
            try
            {
                flag = chg.Select_Tab("Planning");
                if (flag)
                {
                    string temp = Base.GData("Justification");
                    textarea = chg.Textarea_Justification();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) { error = "Cannot populate justification value."; }
                    }
                    else { error = "Cannot get textarea justification."; }
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_AddACI()
        {
            try
            {
                string temp = Base.GData("CI");
                lookup = chg.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot add CI."; }
                }
                else
                    error = "Cannot get lookup CI.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Populate_Urgency()
        {
            try
            {
                combobox = chg.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Low");
                    if (flag)
                    {
                        chg.WaitLoading();
                    }
                    else { error = "Cannot populate urgency value."; }
                }
                else
                {
                    error = "Cannot get combobox urgency.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulatePriority()
        {
            try
            {
                combobox = chg.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("3 - Moderate");
                    if (!flag) { error = "Cannot populate category value."; }
                }
                else { error = "Cannot get combobox category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulatePlannedDate()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag)
                {
                    var date = DateTime.Now;
                    var nextWednesday = date.AddDays(10 - (int)date.DayOfWeek);
                    string startDate = nextWednesday.ToString("yyyy-MM-dd 01:mm:ss");
                    string endDate = nextWednesday.AddMinutes(1).ToString("yyyy-MM-dd 01:mm:ss");

                    datetime = chg.Datetime_Planned_Start_Date();
                    
                    flag = datetime.Existed;
                    if (flag)
                    {
                        flag = datetime.SetText(startDate, true);
                        if (flag) 
                        {
                            datetime = chg.Datetime_Planned_End_Date();
                            flag = datetime.Existed;
                            if (flag) 
                            {
                                flag = datetime.SetText(endDate, true);
                                if (!flag) error = "Cannot populate planned end date.";
                            } else error = "Cannot get datetime planned end date.";
                        } else error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime planned start date.";
                }
                else error = "Cannot select tab (Planning).";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SubmitChange()
        {
            try
            {
                textbox = chg.Textbox_Number();

                flag = textbox.Existed;
                if (flag)
                {
                    changeId = textbox.Text;
                }

                button = chg.Button_Submit();

                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    chglist.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_ImpersonateUser_ChangeCoor()
        {
            try
            {
                string temp = Base.GData("ChangeCoor");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user ChangeCoor.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_014_015_SearchAndOpenChange()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (changeId == null || changeId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input change Id.");
                    addPara.ShowDialog();
                    changeId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Change", "Open");
                if (flag)
                {
                    chglist.WaitLoading();
                    temp = chglist.List_Title().MyText;
                    flag = temp.Equals("Change Requests");
                    if (flag)
                    {
                        flag = chglist.SearchAndOpen("Number", changeId, "Number=" + changeId, "Number");
                        if (!flag) error = "Error when search and open change (id:" + changeId + ")";
                        else chg.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Changes)";
                    }
                }
                else error = "Error when select open change.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_AssignTheChangeToGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                lookup = chg.Lookup_AssignmentGroup();

                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText(temp, true);

                    chg.WaitLoading();

                    if (flag == true)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (flag == false)
                        {
                            error = "Input invalid assignmentgroup value.";

                        }
                    }
                    else
                    {
                        error = "Cannot assign change to group";
                    }
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_CheckConflict()
        {
            try
            {
                flag = chg.Select_Tab("Conflicts");
                if (flag == true)
                {
                    button = chg.Button_CheckConflict();
                    if (button.MyElement.Enabled == true)
                    {
                        button.Click();
                        chg.WaitLoading();
                        flag = chg.WaitCheckConflicts_Completed_And_CloseDialog();
                        if (flag == true)
                        {
                            changeId = chg.Textbox_Number().Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_ValidateBlackOutInConflictList()
        {
            try
            {
                string tabDefine = "span[id^='section'][class$='tab_section']>span:not([style='display: none;'])";
                flag = chg.Select_Tab("Conflicts");
                if (flag == true)
                {
                    flag = chg.Verify_RelatedTable_Row("Conflicts", "Type=Blackout", false, tabDefine);

                    if (flag == false)
                    {
                        error = "There is no blackout record in Conflict list.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_InputPlannedOutageDates()
        {
            try
            {
                scheduleDt = Base.GData("ScheduleDate");
                flag = chg.Select_Tab("Schedule");
                if (flag == true)
                {
                    if (scheduleDt.ToLower() != "no" && scheduleDt.ToLower() != string.Empty)
                    {
                        if (scheduleDt.Contains("|") == true)
                        {
                            string weekday = scheduleDt.Split('|')[0].Trim();
                            string Stime = scheduleDt.Split('|')[1].Trim();

                            if (weekday == string.Empty || Stime == string.Empty)
                            {
                                flag = false;
                                error = "Cannot identify the schedule as missing the day OR start time in the data file.";
                            }
                            else
                            {
                                int sDayOfWeek = (int)Enum.Parse(typeof(DayOfWeek), weekday, true);

                                System.Console.WriteLine("the day of week of schedule day " + weekday + " is " + sDayOfWeek);

                                var date = DateTime.Now;
                                var Sday = date.AddDays(7 - (int)date.DayOfWeek + sDayOfWeek);
                                string sdate = Sday.ToString("yyyy-MM-dd " + Stime + ":mm:ss");
                                string edate = Sday.AddMinutes(1).ToString("yyyy-MM-dd " + Stime + ":mm:ss");


                                datetime = chg.Datetime_Planned_Outage_Start_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    flag = datetime.SetText(sdate);

                                    if (flag == true)
                                    {
                                        datetime = chg.Datetime_Planned_Outage_End_Date();
                                        flag = datetime.Existed;
                                        if (flag)
                                        {
                                            flag = datetime.SetText(edate);
                                            if (flag == false)
                                            {
                                                error = "Cannot input planned end date.";
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        error = "Cannot input planned start date.";
                                    }
                                }
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "The schedule is input with wrong format in the data file.";
                        }
                    }
                    else
                    {
                        flag = false;
                        error = "Cannot identify the schedule as missing the day AND the start time in the data file.";
                    }
                }
                else
                {
                    error = "Cannot select tab shedule";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_InputActualOutageDates()
        {
            try
            {
                flag = chg.Select_Tab("Schedule");
                if (flag == true)
                {
                    if (scheduleDt.ToLower() != "no" && scheduleDt.ToLower() != string.Empty)
                    {
                        if (scheduleDt.Contains("|") == true)
                        {
                            string weekday = scheduleDt.Split('|')[0].Trim();
                            string Stime = scheduleDt.Split('|')[1].Trim();

                            if (weekday == string.Empty || Stime == string.Empty)
                            {
                                flag = false;
                                error = "Cannot identify the schedule as missing the day OR start time in the data file.";
                            }
                            else
                            {
                                int sDayOfWeek = (int)Enum.Parse(typeof(DayOfWeek), weekday, true);

                                System.Console.WriteLine("the day of week of schedule day " + weekday + " is " + sDayOfWeek);

                                var date = DateTime.Now;
                                var Sday = date.AddDays(7 - (int)date.DayOfWeek + sDayOfWeek);
                                string sdate = Sday.ToString("yyyy-MM-dd " + Stime + ":mm:ss");
                                string edate = Sday.AddMinutes(1).ToString("yyyy-MM-dd " + Stime + ":mm:ss");


                                datetime = chg.Datetime_Actual_Outage_Start_Date();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    flag = datetime.SetText(sdate);

                                    if (flag == true)
                                    {
                                        datetime = chg.Datetime_Actual_Outage_End_Date();
                                        flag = datetime.Existed;
                                        if (flag)
                                        {
                                            flag = datetime.SetText(edate);
                                            if (flag == false)
                                            {
                                                error = "Cannot input planned end date.";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        error = "Cannot input planned start date.";
                                    }
                                }
                                
                            }
                        }
                        else
                        {
                            flag = false;
                            error = "The schedule is input with wrong format in the data file.";
                        }

                    }
                    else
                    {
                        flag = false;
                        error = "Cannot identify the schedule as missing the day AND the start time in the data file.";
                    }
                }
                else
                {
                    error = "Cannot select tab shedule";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_SaveChange()
        {
            try
            {
                flag = chg.Save();
                if (flag == true)
                {
                    changeId = chg.Textbox_Number().Text;
                    System.Console.WriteLine("Change Number: " + changeId);
                    chg.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Logout()
        {
            try
            {
                try
                {
                    string temp = Base.GData("Url");
                    Base.ClearCache();
                    Thread.Sleep(2000);
                    Base.Driver.Navigate().GoToUrl(temp);
                    Thread.Sleep(2000);
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
