﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Incident
{
    [TestFixture]
    public class Incident_e2e_7
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************
        
        string incidentId;
        Login login;
        Home home;
        SNow.Incident inc;
        SNow.IncidentList incList;
        SNow.KnowledgeSearch knSearch;
        SNow.Portal portal;
        SNow.EmailList emailList;
        SNow.Email email;
        //-----------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snodatetime datetime = null;
        snobutton button = null;
        snolist list = null;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                knSearch = new SNow.KnowledgeSearch(Base);
                portal = new SNow.Portal(Base, "Portal home");
                emailList = new SNow.EmailList(Base, "Email list");
                email = new Email(Base, "Email");
                //-----------------------------------------------------
                incidentId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else 
                {
                    login.WaitLoading();
                } 
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                
                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Open_New_Incident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (!flag)
                    error = "Error when create new incident.";
                else
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Store_Incident_Id()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    incidentId = textbox.Text;
                }
                else error = "Textbox number is not existed.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Populate_Caller()
        {
            try
            {
                string caller = Base.GData("Caller");
                lookup = inc.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(caller);
                    inc.WaitLoading();
                }
                else error = "Cannot get lookup caller.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Select_CI_AssignedTo_Caller()
        {
            try
            {
                string q = Base.GData("Quantity_Of_Caller_CI");
                string caller_ci = Base.GData("Caller_CI_Select");
                if (q == "1")
                {
                    if (caller_ci.ToLower() != "no")
                    {
                        lookup = inc.Lookup_ConfigurationItem();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(caller_ci, true);
                            if (!flag)
                            {
                                error = "Invalid ci or the value is not auto populate. Runtime value [:" + lookup.Text + "] - Expected value: [" + caller_ci + "]";
                                flagExit = false;
                            }
                        }
                        else
                            error = "Cannot get lookup ci.";
                    }
                    else Console.WriteLine("Not verify.");
                }
                else 
                {
                    if (caller_ci.ToLower() != "no")
                    {
                        SNow.ItilList search_ci = new ItilList(Base, "Search configuration item");
                        Base.SwitchToPage(1);
                        search_ci.WaitLoading(false, true);
                        snotable table = search_ci.Table_List(null, true);
                        table.CellDefine = "tr[id] td[style]:not([class])";
                        int rowIndex = table.RowIndex("Name=" + caller_ci);
                        int colIndex = table.ColumnIndex("Name");
                        flag = table.CellClick(rowIndex, colIndex, "a");
                        Base.SwitchToPage(0);
                    }
                    else Console.WriteLine("Not select.");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");

                if (temp.ToLower() == "no")
                {
                    lookup = inc.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag) 
                        { 
                            error = "Invalid company value or the value is not auto populate."; 
                            flagExit = false; 
                        }
                    }
                    else { error = "Cannot get lookup company."; }
                }
                else Console.WriteLine("*** No need verify this step.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Verify_Caller_Email()
        {
            try
            {
                string temp = Base.GData("CallerEmail");
                if (temp.ToLower() != "no")
                {
                    textbox = inc.Textbox_Email();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) 
                        { 
                            error = "Invalid caller email or the value is not auto populate. Runtime value [:" + textbox.Text + "] - Expected value: [" + temp + "]";
                            flagExit = false; 
                        }
                    }
                    else
                        error = "Cannot get caller email.";
                }
                else Console.WriteLine("Not verify.");

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Verify_Caller_Phone()
        {
            try
            {
                string temp = Base.GData("CallerPhone");
                if (temp.ToLower() != "no")
                {
                    textbox = inc.Textbox_BusinessPhone();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) 
                        { 
                            error = "Invalid caller phone or the value is not auto populate.";
                            flagExit = false; 
                        }
                    }
                    else
                        error = "Cannot get textbox business phone.";
                }
                else Console.WriteLine("Not verify.");

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Verify_Location()
        {
            try
            {
                string temp = Base.GData("CallerLocation");
                if (temp.ToLower() != "no")
                {
                    lookup = inc.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag)
                        { 
                            error = "Invalid location or the value is not auto populate. Runtime value [:" + lookup.Text + "] - Expected value: [" + temp + "]"; 
                            flagExit = false; 
                        }
                    }
                    else
                        error = "Cannot get lookup location.";
                }
                else Console.WriteLine("Not verify.");

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Change_Difference_Location()
        {
            try
            {
                string temp = Base.GData("Location_Update");
                lookup = inc.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Error when select location."; }
                }
                else
                    error = "Cannot get lookup location.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Populate_Business_Service()
        {
            try
            {
                string temp = Base.GData("BusinessService");
                lookup = inc.Lookup_BusinessService();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate business service value."; }
                }
                else
                    error = "Cannot get lookup business service.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Populate_Category()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Populate_Subcategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                    
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Populate_Contact_Type()
        {
            try
            {
                string temp = Base.GData("ContactType");
                combobox = inc.Combobox_ContactType();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate contact type value."; }
                }
                else
                    error = "Cannot get combobox contact type.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_Populate_Short_Description()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Populate_Assignment_Group_SDG()
        {
            try
            {
                string temp = Base.GData("ServiceDeskGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Search_And_Open_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Verify_Current_Process_Flow_New()
        {
            try
            {
                string temp = "New";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_ClickOn_Show_Related_Lockers()
        {
            try
            {
                string temp = Base.GData("StockroomName");
                if (temp.ToLower() != "no")
                {
                    button = inc.Button_Location_Show_Locker();
                    flag = button.Existed;
                    if (!flag)
                        error = "Button location show locker is NOT existed.";
                    else
                        flag = button.Click();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Verify_Stockroom_Record()
        {
            try
            {
                string temp = Base.GData("StockroomName");
                if (temp.ToLower() != "no")
                {
                    SNow.ItilList stockroom = new ItilList(Base, "Search stockroom");
                   
                    snotable table = stockroom.Table_List("div[id='show_list'] table table table");
                    table.ColumnDefine = "th[class^='text']";
                    table.ColumnAttribute = "glide_label";
                    table.RowDefine = "tr[class^='list_row'";
                    table.CellDefine = "td[class='vt']";

                    string[] array = null;
                    if (temp.Contains(";"))
                        array = temp.Split(';');
                    else
                        array = new string[] { temp };

                    foreach (string val in array)
                    {
                        string condition = "Name=" + val + "|Type=Locker";
                        int rowIndex = table.RowIndex(condition);
                        if (rowIndex < 0)
                        {
                            Console.WriteLine("NOT FOUND row:" + condition);
                            if(flag)
                                flag = false;
                        }
                        else
                            Console.WriteLine("Found row:" + condition);
                    }
                    
                    button = inc.Button_Locker_Close();
                    button.Click();
                    
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_Verify_Impact()
        {
            try
            {
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("3 - Medium", true);
                    if (!flag) { error = "Invalid impact value."; flagExit = false; }
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_Verify_Urgency()
        {
            try
            {
                combobox = inc.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("3 - Medium", true);
                    if (!flag) { error = "Invalid urgency value."; flagExit = false; }
                }
                else { error = "Cannot get combobox urgency."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_Verify_Priority()
        {
            try
            {
                combobox = inc.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("3 - Medium", true);
                    if (!flag) { error = "Invalid priority value."; flagExit = false; }
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Verify_State_New()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("New", true);
                    if (!flag) { error = "Invalid state value."; flagExit = false; }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_027_Verify_NotFound_Follow_Up_Date()
        //{
        //    try
        //    {
        //        datetime = inc.Datetime_Followupdate();
        //        flag = datetime.Existed;
        //        if (flag)
        //        {
        //            error = "Follow up date field is visible. Expected is NOT visible";
        //            flag = false;
        //            flagExit = false;
        //        }
        //        else
        //        {
        //            flag = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Verify_Activity_Note_28()
        {
            try
            {
                string temp = Base.GData("Activity_28");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Verify_Related_Business_Service()
        {
            try
            {
                string conditions = "Business Service=" + Base.GData("BusinessService");
                flag = inc.Verify_RelatedTable_Row("Impacted Services", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found business service: " + conditions + "- Deffect: [DFCT0020752 - STRY0279208]";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Verify_Related_CI()
        {
            try
            {
                string conditions = "Configuration Item=" + Base.GData("Caller_CI_Select");
                flag = inc.Verify_RelatedTable_Row("Affected CIs", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found CI: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Populate_AssignedTo_SDA2_Assignee()
        {
            try
            {
                string temp = Base.GData("SDA2_Assignee");
                lookup = inc.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Update_Incident()
        {
            try
            {
                flag = inc.Update();
                if (!flag) { error = "Error when update incident."; }
                else { inc.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Search_And_Open_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Verify_Current_Process_Flow_Active()
        {
            try
            {
                string temp = "Active";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_01_Verify_State_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Active", true);
                    if (!flag) { error = "Invalid state value."; flagExit = false; }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_02_Verify_Existed_Button_Resolve_Incident()
        {
            try
            {
                button = inc.Button_ResolveIncident();
                flag = button.Existed;
                if (!flag)
                { flagExit = false; error = "Button Resolve Incident NOT visible."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_03_Verify_Checkbox_Responsed_ReadOnly_And_Checked()
        {
            try
            {
                snocheckbox checkbox = inc.Checkbox_Responded();
                flag = checkbox.Existed;
                if (!flag)
                {
                    error = "Cannot get checkbox Responsed.";
                }
                else
                {
                    string str = checkbox.MyReadOnly;
                    if (str != "true")
                    {
                        flag = false;
                        error = "Checkbox Responsed is NOT readonly.";
                    }
                    else
                    {
                        flag = checkbox.Checked;
                        if (!flag)
                            error = "Checkbox Responsed is NOT checked.";
                    }
                }
                if (!flag) flagExit = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Verify_Activity_Note_36()
        {
            try
            {
                string temp = Base.GData("Activity_36");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Verify_State_Items_37()
        {
            try
            {
                string items = Base.GData("State_Items_37");
                flag = inc.Verify_Combobox_Items(items, ref error);
                if (!flag)
                {
                    error = "Invalid items of combobox State.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Populate_Worknotes()
        {
            try
            {
                string temp = Base.GData("WorkNote");
                flag = inc.Add_Worknotes(temp);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Verify_Activity_Note_40()
        {
            try
            {
                string temp = Base.GData("Activity_40");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        /* Huong C - remove steps for adding/removing the attachment
        [Test]
        public void Step_041_Attach_File()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Verify_Attachment_File()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Rename_Attachment_File()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Rename_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when rename attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Verify_Attachment_File_Renamed()
        {
            try
            {
                string attachmentFile = "incidentAttachment_rename.txt";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Delete_Attachment_File()
        {
            try
            {
                string attachmentFile = "incidentAttachment_rename.txt";
                flag = inc.Delete_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when delete attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Verify_Attachment_File_Deleted()
        {
            try
            {
                string attachmentFile = "incidentAttachment_rename.txt";
                flag = inc.Verify_Attachment_File(attachmentFile, true);
                if (flag)
                {
                    error = "Found attachment file (" + attachmentFile + ") in attachment container.";
                    flag = false;
                    flagExit = false;
                }
                else flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }*/
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Add_Two_Users_To_Watchlist()
        {
            try
            {
                string users = Base.GData("User_WatchList");
                list = inc.List_Watchlist();
                flag = inc.Add_List_Value(list, users);
                if (!flag)
                    error = "Error when add users to watch list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Populate_Addition_Comments()
        {
            try
            {
                string temp = Base.GData("AdditionalComment");
                flag = inc.Add_AdditionComments(temp);
                if (!flag)
                    error = "Cannot populate value for additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Verify_Activity_Note_50()
        {
            try
            {
                string temp = Base.GData("Activity_50");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_Clickon_Button_Search_Knowledge()
        {
            try
            {
                button = inc.Button_SearchKnowledge();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        Base.SwitchToPage(1);
                        knSearch.WaitLoading();
                    }
                }
                else { error = "Cannot get button search knowledge."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Verify_Value_Of_Search_Textbox()
        {
            try
            {
                Base.SwitchToPage(1);
                string temp = Base.GData("ShortDescription");
                textbox = knSearch.Textbox_Search();
                flag = textbox.VerifyCurrentValue(temp);
                if (!flag)
                    error = "Invalid default value of search textbox.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Search_And_Open_Knowledge_1()
        {
            try
            {
                Base.SwitchToPage(1);
                string temp = Base.GData("KnowledgeArticle01");
                flag = knSearch.SearchAndOpen(temp);
                if (!flag)
                    error = "Cannot open knowledge artical 1.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_Attach_Knowledge_1_To_Incident()
        {
            try
            {
                Base.SwitchToPage(1);
                button = knSearch.Button_Attach();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    Thread.Sleep(2000);
                    Base.SwitchToPage(0);
                }
                else error = "Cannot get button Attach.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_Verify_Knowledge_Was_Attached()
        {
            try
            {
                string temp = Base.GData("KnowledgeArticle01");
                flag = inc.VerifyKnowledgeWasAttached(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found knowledge attached to incident.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_ImpersonateUser_SDA2_Assignee()
        {
            try
            {
                string temp = Base.GData("SDA2_Assignee");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Search_And_Open_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_Update_Impact()
        {
            try
            {
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Impact_Update");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select impact.";
                }
                else error = "Cannot get combobox Impact.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_Update_Urgency()
        {
            try
            {
                combobox = inc.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Urgency_Update");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Error when select urgency.";
                }
                else error = "Cannot get combobox urgency.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_Add_CI_2()
        {
            try
            {
                string temp = Base.GData("CI2");
                flag = inc.Add_Related_Members("affected cis", temp);
                if (!flag)
                    error = "Error when add ci 2.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //--------------------------------------------------------------------F---------------------------------------------------------------
        [Test]
        public void Step_064_Verify_Related_CI_2_Added()
        {
            try
            {
                string conditions = "Configuration Item=" + Base.GData("CI2");
                flag = inc.Verify_RelatedTable_Row("Affected CIs", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found CI: " + conditions;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_Delete_CI_2()
        {
            try
            {
                string temp = Base.GData("CI2");
                flag = inc.Delete_Related_Members("affected cis", temp);
                if (!flag)
                    error = "Error when delete ci 2.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_Verify_Related_CI_2_Deleted()
        {
            try
            {
                string conditions = "Configuration Item=" + Base.GData("CI2");
                flag = inc.Verify_RelatedTable_Row("Affected CIs", conditions, true);
                if (!flag)
                {
                    flagExit = false;
                    error = "Found CI: " + conditions + " - Expected: NOT FOUND";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_01_Change_State_To_Awaiting_Change()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Change");
                    if (!flag)
                        error = "Error when select state.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_02_Click_Save()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_03_Verify_Current_Process_Flow_Awaiting()
        {
            try
            {
                string temp = "Awaiting";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_070_Verify_Found_Follow_Up_Date()
        //{
        //    try
        //    {
        //        datetime = inc.Datetime_Followupdate();
        //        flag = datetime.Existed;
        //        if (!flag)
        //        {
        //            error = "Follow up date field is NOT visible. Expected is visible";
        //            flagExit = false;
        //        }
        //        else 
        //        {
        //            var date = DateTime.Now;
        //            var next = date.AddDays(1);
        //            string temp = next.ToString("yyyy-MM-dd hh:mm:ss");
        //            flag = datetime.SetText(temp);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_01_Change_State_To_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                    if (!flag)
                        error = "Error when select state.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_02_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_03_Verify_Current_Process_Flow_Active()
        {
            try
            {
                string temp = "Active";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_01_Change_State_To_Awaiting_Customer()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Customer");
                    if (!flag)
                        error = "Error when select state.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_02_Verify_Found_Follow_Up_Date_Visible()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (!flag)
                {
                    error = "Follow up date field is NOT visible. Expected is visible";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_03_Populate_Follow_Up_Date_Invalid_Value()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    DateTime date = DateTime.Now;
                    string today = date.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(today);
                    if (!flag)
                        error = "Error when populate follow up date value.";
                }
                else error = "Cannot get datetime follow up date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_04_Save_Incident_Expected_Show_Error_Message()
        {
            try
            {
                flag = inc.Save(false, true);
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_05_Verify_Error_Message_About_Follow_Up_Date()
        {
            try
            {
                string expMessage = "The Follow up date must be greater than the Current date";
                flag = inc.Verify_ExpectedErrorMessages_Existed(expMessage);
                if (!flag)
                    error = "Invalid error message.";
                else
                {
                    button = inc.Button_Close_Error();
                    if (button.Existed)
                        button.Click();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_06_Populate_Follow_Up_Date_Valid_Value()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    DateTime date = DateTime.Now;
                    string today = date.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(today);
                    if (!flag)
                        error = "Error when populate follow up date value.";
                }
                else error = "Cannot get datetime follow up date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_Verify_State_Items_73()
        {
            try
            {
                string items = Base.GData("State_Items_73");
                flag = inc.Verify_Combobox_Items(items, ref error);
                if (!flag)
                {
                    error = "Invalid items of combobox State.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_Verify_Current_Process_Flow_Awaiting()
        {
            try
            {
                string temp = "Awaiting";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_075_Save_Incident()
        //{
        //    try
        //    {
        //        flag = inc.Save();
        //        if (!flag) { error = "Error when save incident."; }
        //        else inc.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_076_Verify_Current_Process_Flow_Awaiting()
        //{
        //    try
        //    {
        //        string temp = "Awaiting";
        //        flag = inc.Verify_CurrentProcess(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid current process flow. Expected: [" + temp + "].";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_01_Change_State_To_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                    if (!flag)
                        error = "Error when select state.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_02_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_03_Verify_Current_Process_Flow_Active()
        {
            try
            {
                string temp = "Active";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_Change_State_To_Awaiting_Vendor()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Vendor");
                    if (!flag)
                        error = "Error when select state.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_Populate_Vendor()
        {
            try
            {
                string temp = Base.GData("Vendor");
                lookup = inc.Lookup_Vendor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate vendor value."; }
                }
                else { error = "Cannot get textbox vendor."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_Verify_Found_Follow_Up_Date()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (!flag)
                {
                    error = "Follow up date field is NOT visible. Expected is visible";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }     
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_Verify_Current_Process_Flow_Awaiting()
        {
            try
            {
                string temp = "Awaiting";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_081_Change_State_To_Awaiting_Vendor()
        //{
        //    try
        //    {
        //        combobox = inc.Combobox_State();
        //        flag = combobox.Existed;
        //        if (flag)
        //        {
        //            flag = combobox.SelectItem("Awaiting Vendor");
        //            if (!flag)
        //                error = "Error when select state.";
        //        }
        //        else error = "Cannot get combobox state.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_082_Populate_Vendor()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Vendor");
        //        lookup = inc.Lookup_Vendor();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {
        //            flag = lookup.Select(temp);
        //            if (!flag) { error = "Cannot populate vendor value."; }
        //        }
        //        else { error = "Cannot get textbox vendor."; }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_083_Verify_Found_Follow_Up_Date()
        //{
        //    try
        //    {
        //        datetime = inc.Datetime_Followupdate();
        //        flag = datetime.Existed;
        //        if (!flag)
        //        {
        //            error = "Follow up date field is NOT visible. Expected is visible";
        //            flagExit = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_084_Save_Incident()
        //{
        //    try
        //    {
        //        flag = inc.Save();
        //        if (!flag) { error = "Error when save incident."; }
        //        else inc.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_085_Verify_Current_Process_Flow_Awaiting()
        //{
        //    try
        //    {
        //        string temp = "Awaiting";
        //        flag = inc.Verify_CurrentProcess(temp);
        //        if (!flag)
        //        {
        //            flagExit = false;
        //            error = "Invalid current process flow. Expected: [" + temp + "].";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_086_Populate_Follow_Up_Date_Invalid_Value()
        //{
        //    try
        //    {
        //        datetime = inc.Datetime_Followupdate();
        //        flag = datetime.Existed;
        //        if (flag)
        //        {
        //            DateTime date = DateTime.Now;
        //            string today = date.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
        //            flag = datetime.SetText(today);
        //            if (!flag)
        //                error = "Error when populate follow up date value.";
        //        }
        //        else error = "Cannot get datetime follow up date.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_087_Save_Incident_Expected_Show_Error_Message()
        //{
        //    try
        //    {
        //        flag = inc.Save(false, true);
        //        if (!flag) { error = "Error when save incident."; }
        //        else inc.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_088_Verify_Error_Message_About_Follow_Up_Date()
        //{
        //    try
        //    {
        //        string expMessage = "The Follow up date must be greater than the Current date";
        //        flag = inc.Verify_ExpectedErrorMessages_Existed(expMessage);
        //        if (!flag)
        //            error = "Invalid error message.";
        //        else 
        //        {
        //            button = inc.Button_Close_Error();
        //            if (button.Existed)
        //                button.Click();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_089_Populate_Follow_Up_Date_Valid_Value()
        //{
        //    try
        //    {
        //        datetime = inc.Datetime_Followupdate();
        //        flag = datetime.Existed;
        //        if (flag)
        //        {
        //            DateTime date = DateTime.Now;
        //            string today = date.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
        //            flag = datetime.SetText(today);
        //            if (!flag)
        //                error = "Error when populate follow up date value.";
        //        }
        //        else error = "Cannot get datetime follow up date.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_090_Save_Incident()
        //{
        //    try
        //    {
        //        flag = inc.Save();
        //        if (!flag) { error = "Error when save incident."; }
        //        else 
        //        {
        //            inc.WaitLoading();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_ImpersonateUser_SDA3()
        {
            try
            {
                string temp = Base.GData("SDA3");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_092_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        /* Huong C - Verify a field readonly/edit on the list should be moved to GUI Validation ts (Steps 069_070_071)
        [Test]
        public void Step_093_Search_DoubleClick_StateColumn_SelectClosed_Expected_HaveError()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndEdit("Number", incidentId, "Number=" + incidentId, "State", "Closed");
                        if (flag)
                        {
                            Thread.Sleep(2000);
                            try
                            {
                                IAlert alert = Base.Driver.SwitchTo().Alert();
                                temp = "Incidents cannot be Resolved or Closed from the list view.";
                                string runtime = alert.Text.Replace("\r\n", "");
                                if (!runtime.Equals(temp))
                                {
                                    flag = false;
                                    flagExit = false;
                                    error = "Invalid alert message. Runtime:(" + runtime + "). Expexted:(" + temp + ")";
                                }
                                alert.Accept();
                                inc.WaitLoading();
                            }
                            catch
                            {
                                flag = false;
                                flagExit = false;
                                error = "Alert is not visible.";
                            }
                        }
                        else error = "Error when search and edit cell.";
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }*/
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_094_Search_Open_Incident()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                //if (flag)
                //    inc.WaitLoading();
                //else error = "Error when open incident.";
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_095_Add_Me_To_WatchList()
        {
            try
            {
                Thread.Sleep(5000);
                snolist wlist = inc.List_Watchlist();
                button = wlist.Button_AddMe_Locked();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        inc.WaitLoading();
                        string temp = Base.GData("SDA3");
                        snoelement ele = wlist.Showlist_Added();
                        if (!ele.Existed || !ele.MyText.Trim().ToLower().Contains(temp.Trim().ToLower()))
                        {
                            flag = false;
                            error = "Cannot add me to watch list.";
                        }
                    }
                    else error = "Canno click on button add me to watch list.";
                }
                else
                {
                    error = "Cannot get button add me to watch list.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_096_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_097_Open_SelfService_WatchedIncident()
        //{
        //    try
        //    {
        //        flag = home.LeftMenuItemSelect("Self-Service", "Watched Incidents");
        //        if (flag)
        //            incList.WaitLoading();
        //        else
        //            error = "Error when open watched incident list.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_098_Verify_Incident_Have_In_List()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
        //            addPara.ShowDialog();
        //            incidentId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        flag = incList.SearchAndVerify("Number", incidentId, "Number=" + incidentId);
        //        if (!flag)
        //            error = "Not found incident [" + incidentId + "]";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_099_ImpersonateUser_SD2()
        //{
        //    try
        //    {
        //        string temp = Base.GData("SDA2_Assignee");
        //        string rootuser = Base.GData("UserFullName");
        //        flag = home.ImpersonateUser(temp, true, rootuser);
        //        if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
        //        else home.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_100_P_Verify_Login_User_Name()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Caller");
        //        snoelement user = portal.UserFullName();
        //        flag = user.Existed;
        //        if (flag)
        //        {
        //            if (!user.MyText.Trim().ToLower().Equals(temp.Trim().ToLower()))
        //            {
        //                flag = false;
        //                flagExit = false;
        //                error = "Invalid the full name of login user.";
        //            }
        //        }
        //        else error = "Cannot get control user full name.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_101_P_Find_My_Incident_User_Global_Search()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
        //            addPara.ShowDialog();
        //            incidentId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        textbox = portal.Textbox_Global_Search();
        //        flag = textbox.Existed;
        //        if (flag)
        //        {
        //            flag = textbox.SetText(incidentId, true);
        //            if (!flag)
        //                error = "Error when set text on global search.";
        //            else portal.WaitLoading(true);
        //        }
        //        else error = "Cannot get textbox global search.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_102_P_Open_Incident()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
        //            addPara.ShowDialog();
        //            incidentId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        flag = portal.OpenItemFromSearchGroupResult("Incident", incidentId);
        //        if (!flag)
        //            error = "Error when opent incident [" + incidentId + "]";
        //        else portal.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_103_P_Populate_Addition_Comments()
        //{
        //    try
        //    {
        //        string temp = "Caller add addition comment.";
        //        textarea = portal.Textarea_AdditionalComments();
        //        flag = textarea.Existed;
        //        if (flag)
        //        {
        //            flag = textarea.SetText(temp);
        //            if (!flag)
        //                error = "Cannot populate value for additional comments.";
        //        }
        //        else error = "Cannot get textarea additional comments.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_104_P_ClickOn_ResolveIncident()
        //{
        //    try
        //    {
        //        button = portal.Button_ResolveIncident();
        //        flag = button.Existed;
        //        if (flag)
        //        {
        //            flag = button.Click();
        //            if (!flag) error = "Error when click button resolve incident.";
        //            else portal.WaitLoading(true);
        //        }
        //        else error = "Cannot get button resolve incident.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_105_Return_LoginPage()
        //{
        //    try
        //    {
        //        string temp = Base.GData("Url");
        //        Base.ClearCache();
        //        Thread.Sleep(5000);
        //        Base.Driver.Navigate().GoToUrl(temp);
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_106_ReLogin()
        //{
        //    try
        //    {
        //        string user = Base.GData("User");
        //        string pwd = Base.GData("Pwd");

        //        flag = login.LoginToSystem(user, pwd);

        //        if (flag)
        //        {
        //            home.WaitLoading();
        //        }
        //        else
        //        {
        //            error = "Cannot login to system.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_107_ImpersonateUser_SDA2_Assignee()
        {
            try
            {
                string temp = Base.GData("SDA2_Assignee");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_108_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_109_Search_And_Open_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_110_01_Change_State_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Active";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Invalid state selected.";
                    }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_110_02_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_110_03_ResolvingTicket()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Invalid state selected.";
                    }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_110_04_PopulateCloseCodeAndCloseNotes()
        {
            try
            {


                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Solved (Work Around)";
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = "Test Close Note";
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_01_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_02_CloseIncident()
        {
            try
            {
                button = inc.Button_CloseIncident();
                flag = button.Existed;
                if (!flag)
                    error = "Not found button Close Incident.";
                else
                {
                    flag = button.Click();
                    if (flag)
                        inc.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_03_Open_Incident()
        {
            try
            {
                // --Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                flag = inc.GlobalSearchItem(incidentId, true);
                if (!flag)
                    error = "Cannot open incident";
                else
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_111_04_Verify_State_Closed()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Closed");
                }
                else error = "Cannot get combobx State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_111_Verify_CloseCode_And_CloseNotes()
        //{
        //    try
        //    {
        //        flag = inc.Select_Tab("Closure Information");
        //        if (flag)
        //        {
        //            combobox = inc.Combobox_CloseCode();
        //            flag = combobox.Existed;
        //            if (flag)
        //            {
        //                flag = combobox.VerifyCurrentValue("Closed/Resolved by Caller", true);
        //                if (flag)
        //                {
        //                    textarea = inc.Textarea_CloseNotes();
        //                    flag = textarea.Existed;
        //                    if (flag)
        //                    {
        //                        flag = textarea.VerifyCurrentValue("Closed by Caller", true);
        //                        if (!flag) error = "Invalid close notes.";
        //                    }
        //                    else error = "Cannot get textarea close notes.";
        //                }
        //                else error = "Invalid close code value.";
        //            }
        //            else error = "Cannot get combobox close code.";
        //        }
        //        else error = "Cannot select tab Closure Information.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_112_ImpersonateUser_Support_User()
        {
            try
            {
                string temp = Base.GData("SupportUser");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_113_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_114_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_114_02_Filter_Email_For_Incident_Opened_For_Me()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("CallerEmail");
                temp = "Subject;contains;" + incidentId + "|and|Subject;contains;opened|and|" + "Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_114_03_Validate_Email_For_Incident_Opened_For_Me()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + incidentId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) { error = "Not found email sent to caller (opened)."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_115_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_115_02_Filter_Email_For_Incident_Assigned_To_My_Group()
        {
            try
            {
                string group = Base.GData("ServiceDeskGroup");
                string groupEmail = Base.GData("SDG_Email");
                string groupEmailMember = Base.GData("SDG_Email_Member");
                if (groupEmail.ToLower() != "no" && groupEmailMember != "no")
                {
                    string recipient = groupEmailMember;

                    if (groupEmail.ToLower() != "no")
                        recipient = groupEmail;

                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------

                    temp = "Subject;contains;" + incidentId + "|and|Subject;contains;has been assigned to group " + group + "|and|" + "Recipients;contains;" + recipient;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_115_03_Validate_Email_For_Incident_Assigned_To_My_Group()
        {
            try
            {
                string groupEmail = Base.GData("SDG_Email");
                string groupEmailMember = Base.GData("SDG_Email_Member");
                if (groupEmail.ToLower() != "no" && groupEmailMember != "no")
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------
                    string conditions = "Subject=@@" + incidentId;
                    flag = emailList.VerifyRow(conditions);
                    if (!flag) { error = "Not found email sent to assignment group."; flagExit = false; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_116_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_116_02_Filter_Email_For_Incident_Assigned_To_Me()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string recipient = Base.GData("SDA2_Assignee_Email");
                temp = "Subject;contains;" + incidentId + "|and|Subject;contains;assigned to you|and|" + "Recipients;contains;" + recipient;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_116_03_Validate_Email_For_Incident_Assigned_To_Me()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + incidentId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) { error = "Not found email sent to assignee."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_117_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_117_02_Filter_Email_For_Incident_Commented_SendTo_Caller()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string recipient = Base.GData("CallerEmail");
                temp = "Subject;contains;" + incidentId + "|and|Subject;contains;comments added|and|Recipients;contains;" + recipient;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_117_03_Validate_Email_For_Incident_Commented_SendTo_Caller()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + incidentId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) { error = "Not found email sent to caller."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_118_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_118_02_Filter_Email_For_Incident_Commented_SendTo_AssignedTo()
        {
            try
            {
                string recipient = Base.GData("SDA2_Assignee_Email");
                if (recipient.ToLower() != "no")
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------

                    temp = "Subject;contains;" + incidentId + "|and|Subject;contains;comments added|and|Recipients;contains;" + recipient;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_118_03_Validate_Email_For_Incident_Commented_SendTo_AssignedTo()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + incidentId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) { error = "Not found email sent to assigned to."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_119_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_119_02_Filter_Email_For_Incident_Commented_SendTo_WatchedList()
        {
            try
            {
                string recipient = Base.GData("User_WatchList_Email");
                if (recipient.ToLower() != "no")
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------

                    temp = "Subject;contains;" + incidentId + "|and|Subject;contains;comments added|and|Recipients;contains;" + recipient;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_119_03_Validate_Email_For_Incident_Commented_SendTo_WatchedList()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + incidentId;
                flag = emailList.VerifyRow(conditions);
                if (!flag) { error = "Not found email sent to watched list."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_02_Filter_Email_For_Incident_Resolved()
        {
            try
            {
                string recipient = Base.GData("CallerEmail");
                if (recipient.ToLower() != "no")
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------

                    temp = "Subject;contains;" + incidentId + "|and|Subject;contains;has been resolved|and|Recipients;contains;" + recipient;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_03_Open_Email_Incident_Resolved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + incidentId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to caller."; flagExit = false; }
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_04_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_05_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string autoCloseDay = Base.GData("AutoCloseDay");
                string expected = "Your incident " + incidentId;
                expected = expected + " has been resolved. If you have already confirmed resolution with the Support agent, this record will now be closed. If you have not confirmed resolution yet, and feel your issue is not resolved, please click the link below to reactivate the incident; otherwise, the record will automatically close in";
                expected = expected + " " + autoCloseDay + " days.";


                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_120_06_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_02_Filter_Email_For_Incident_Closed()
        {
            try
            {
                string recipient = Base.GData("CallerEmail");
                if (recipient.ToLower() != "no")
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------

                    temp = "Subject;contains;" + incidentId + "|and|Subject;contains;has been closed|and|Recipients;contains;" + recipient;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_03_Validate_And_Open_Email_For_Incident_Closed()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + incidentId;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to caller."; flagExit = false; }
                else email.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_04_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "@@incident " + incidentId + " has been closed";
                flag = email.VerifySubject(expected);
                if (!flag) error = "Invalid subject value.";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_05_Verify_Email_Recipient_Caller()
        {
            try
            {
                string recipient = Base.GData("CallerEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag) error = "Invalid recipient value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_06_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_07_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string autoCloseDay = Base.GData("AutoCloseDay");
                string expected = "Your incident " + incidentId;
                expected = expected + " has been closed. Please contact the service desk if you have any questions.";
                
                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_121_08_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_122_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
    }
}
