﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Windows.Forms;

namespace Incident
{
    [TestFixture]
    public class Incident_create_and_fcr_9
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id01: " + incidentId01);
            System.Console.WriteLine("Finished - Incident Id02: " + incidentId02);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        string incidentId01, incidentId02;
        Login login;
        Home home;
        SNow.Incident inc;
        SNow.IncidentList incList;
        SNow.KnowledgeSearch knSearch;
        SNow.Portal portal;
        SNow.EmailList emailList;
        SNow.Email email;
        SNow.Member member;

        //-----------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;


        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                knSearch = new SNow.KnowledgeSearch(Base);
                portal = new SNow.Portal(Base, "Portal home");
                emailList = new SNow.EmailList(Base, "Email list");
                email = new SNow.Email(Base, "email");
                member = new SNow.Member(Base);
                //-----------------------------------------------------
                incidentId01 = string.Empty;
                incidentId02 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident01()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId01 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id01:(" + incidentId01 + ")");
                    string temp = Base.GData("Caller01");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                if (temp.ToLower() == "no")
                {
                    lookup = inc.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            error = "Invalid company value or the value is not auto populate.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get lookup company."; }
                }
                else Console.WriteLine("*** No need verify this step.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("CallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_PopulateBusinessService()
        {
            try
            {
                string temp = Base.GData("BusinessService");
                lookup = inc.Lookup_BusinessService();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate business service value."; }
                }
                else
                    error = "Cannot get lookup business service.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }

                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateContactType()
        {
            try
            {
                string temp = Base.GData("ContactType");
                combobox = inc.Combobox_ContactType();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate contact type value."; }
                }
                else
                    error = "Cannot get combobox contact type.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_01_AttachAFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Add_AttachmentFile(attachmentFile);
                if (flag == false)
                {
                    error = "Error when attachment file.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_02_Verify_AttachmentFile()
        {
            try
            {
                string attachmentFile = "incidentAttachment.txt";
                flag = inc.Verify_Attachment_File(attachmentFile);
                if (!flag)
                {
                    error = "Not found attachment file (" + attachmentFile + ") in attachment container.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_03_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_AddAdditionalComment()
        {
            try
            {
                string temp = Base.GData("AdditionalComment");

                flag = inc.Add_AdditionComments(temp);
                if (!flag)
                    error = "Cannot populate value for additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_PopulateAssignmentGroup_SDG()
        {
            try
            {
                string temp = Base.GData("ServiceDeskGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_PopulateAssignedTo_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                lookup = inc.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assigned to value."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_ChangeStateToResolve()
        {
            try
            {
                snobutton button = inc.Button_ResolveIncident();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                }
                else error = "Cannot get button Resolve Incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_017_PopulateCloseCodeAndCloseNotes()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("CloseCode");
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("CloseNote");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;

            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_ResolvingTicket()
        {
            try
            {
                snobutton button = inc.Button_ResolveIncident();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                }
                else error = "Cannot get button Resolve Incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_20_VerifyFCRsetFalse()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId01 == null || incidentId01 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Resolved");
                if (flag)
                {
                    Thread.Sleep(5000);
                    incList.WaitLoading();

                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.Add_More_Columns("FCR");
                        if (!flag) error = "Error when adding the 'FCR' colum.";
                        else
                        {
                            flag = incList.SearchAndVerify("Number", incidentId01, "FCR=false");
                            if (!flag) error = "The 'FCR' colum is not set to false";
                            else inc.WaitLoading();
                        }
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_022_023_SearchAndOpenIncident01()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId01 == null || incidentId01 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Resolved");
                if (flag)
                {
                    Thread.Sleep(5000);
                    incList.WaitLoading();

                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId01, "Number=" + incidentId01, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId01 + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_VerifyIncidentState()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Resolved");
                    if (!flag) error = "Invalid state value.";
                }
                else error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_CheckActivityNote()
        {
            try
            {
                flag = inc.Select_Tab("Notes");
                if (!flag)
                {
                    string temp = Base.GData("Activity_24");
                    flag = inc.Verify_Activity(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid activity";
                    }
                    error = "Cannot Select  Tab Notes";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_CloseIncident01()
        {
            try
            {
                snobutton button = inc.Button_CloseIncident();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                }
                else { error = "Cannot click the 'Close Incident' button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_027_CreateNewOtherIncident02()
        {
            try
            {
                //Recall some previous steps to create other one.
                Step_005_OpenNewIncident01();
                #region Populate Caller to INC02
                {
                    try
                    {
                        textbox = inc.Textbox_Number();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            textbox.Click();
                            //-- Store incident id
                            incidentId02 = textbox.Text;
                            Console.WriteLine("-*-[Store]: Incident Id02:(" + incidentId02 + ")");
                            string temp = Base.GData("Caller02");
                            lookup = inc.Lookup_Caller();
                            flag = lookup.Existed;
                            if (flag)
                            {
                                flag = lookup.Select(temp);
                                if (!flag) { error = "Cannot populate caller value."; }
                            }
                            else { error = "Cannot get lookup caller."; }
                        }
                        else
                        {
                            error = "Cannot get texbox number.";
                        }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                #endregion 
                Step_006_02_Verify_Company();
                Step_008_01_PopulateCategory();
                Step_008_02_PopulateSubCategory();
                Step_009_PopulateContactType();
                Step_010_01_PopulateShortDescription();
                Step_010_02_PopulateDescription();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                snolookup lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText(temp);
                    if (!flag) { error = "Cannot populate Assignment Group value."; }
                }
                else { error = "Cannot get lookup Assignment Group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_ChangeStateINC02ToResolve()
        {
            try
            {
                snobutton button = inc.Button_ResolveIncident();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                }
                else error = "Cannot get button Resolve Incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_01_AddAdditionalComment()
        {
            try
            {
                string temp = Base.GData("AdditionalComment");

                flag = inc.Add_AdditionComments(temp);
                if (!flag)
                    error = "Cannot populate value for additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_02_PopulateAssignmentGroup()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup");
                snolookup lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText(temp);
                    if (!flag) { error = "Cannot populate Assignment Group value."; }
                }
                else { error = "Cannot get lookup Assignment Group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_03_PopulateCloseCodeAndCloseNotes()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("CloseCode");
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("CloseNote");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;

            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_ResolveIncident02()
        {
            try
            {
                snobutton button = inc.Button_ResolveIncident();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                }
                else error = "Cannot get button Resolve Incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_VerifyFCRsetTrue()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId02 == null || incidentId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id02.");
                    addPara.ShowDialog();
                    incidentId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Resolved");
                if (flag)
                {
                    Thread.Sleep(5000);
                    incList.WaitLoading();

                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.Add_More_Columns("FCR");
                        if (!flag) error = "Error when adding the 'FCR' colum.";
                        else
                        {
                            flag = incList.SearchAndVerify("Number", incidentId02, "FCR=true");
                            if (!flag) error = "The 'FCR' colum is not set to true";
                            else inc.WaitLoading();
                        }
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_SearchAndOpenIncident02()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId02 == null || incidentId02 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id02.");
                    addPara.ShowDialog();
                    incidentId02 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Resolved");
                if (flag)
                {
                    Thread.Sleep(5000);
                    incList.WaitLoading();

                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId02, "Number=" + incidentId02, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId02 + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_CloseIncident02()
        {
            try
            {
                snobutton button = inc.Button_CloseIncident();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                }
                else { error = "Cannot click the 'Close Incident' button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_ImpersonateUser_TU()
        {
            try
            {
                string temp = Base.GData("TestUser");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate user sda2.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_02_Filter_Email_For_Incident_Opened_For_Me()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string email = Base.GData("CallerEmail");
                temp = "Subject;contains;" + incidentId01 + "|and|Subject;contains;opened|and|" + "Recipients;contains;" + email;
                flag = emailList.EmailFilter(temp);
                if (flag)
                {
                    emailList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_03_Validate_Email_For_Incident_Opened_For_Me()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string conditions = "Subject=@@" + incidentId01;
                flag = emailList.VerifyRow(conditions);
                if (!flag) { error = "Not found email sent to caller (opened)."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_01_OpenEmailLog()
        {
            try
            {
                flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                if (flag)
                {
                    emailList.WaitLoading();

                    if (!emailList.List_Title().MyText.Contains("Emails"))
                    {
                        flag = false;
                        error = "Cannot open email list.";
                    }
                }
                else error = "Error when open email log.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_02_Filter_Email_For_Incident_Resolved()
        {
            try
            {
                string recipient = Base.GData("CallerEmail");
                if (recipient.ToLower() != "no")
                {
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && incidentId01 == string.Empty)
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                        addPara.ShowDialog();
                        incidentId01 = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //---------------------------------------------------------------------------------------------------

                    temp = "Subject;contains;" + incidentId01 + "|and|Subject;contains;has been resolved|and|Recipients;contains;" + recipient;
                    flag = emailList.EmailFilter(temp);
                    if (flag)
                    {
                        emailList.WaitLoading();
                    }
                    else { error = "Error when filter."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_03_Validate_And_Open_Email_For_Incident_Resolved()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------

                string conditions = "Subject=@@" + incidentId01;
                flag = emailList.Open(conditions, "Created");
                if (!flag) { error = "Not found email sent to caller."; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_04_Verify_Email_Subject()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string expected = "@@incident " + incidentId01 + " has been resolved";
                flag = email.VerifySubject(expected);
                if (!flag) error = "Invalid subject value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_05_Verify_Email_Recipient_Caller()
        {
            try
            {
                string recipient = Base.GData("CallerEmail");
                flag = email.VerifyRecipient(recipient);
                if (!flag) error = "Invalid recipient value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_06_ClickOn_HtmlReviewBody()
        {
            try
            {
                flag = email.ClickOnPreviewHtmlBody();
                if (!flag) error = "Error when click on priview html body.";
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_07_Verify_Email_Body()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && incidentId01 == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id01.");
                    addPara.ShowDialog();
                    incidentId01 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------------------------------------------------------------
                string autoCloseDay = Base.GData("AutoCloseDay");
                string expected = "Your incident " + incidentId01;
                expected = expected + " has been resolved. If you have already confirmed resolution with the Support agent, this record will now be closed. If you have not confirmed resolution yet, and feel your issue is not resolved, please click the link below to reactivate the incident; otherwise, the record will automatically close in";
                expected = expected + " " + autoCloseDay + " days.";


                flag = email.VerifyEmailBody(expected);
                if (!flag) error = "Invalid value in body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_08_Close_EmailBody()
        {
            try
            {
                flag = email.ClickOnCloseButton();
                if (!flag) error = "Error when close email body.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

    }


}