﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Incident
{
    [TestFixture]
    public class Incident_lbs_wo_24
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            System.Console.WriteLine("Finished - Work Order Id: " + workorderId);
            System.Console.WriteLine("Finished - Work Order Task Id: " + wotaskId);
            System.Console.WriteLine("Current time: " + currentDateTime);



            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        string incidentId, currentDateTime, workorderId, wotaskId;
        Login login;
        Home home;
        Portal portal;
        SNow.Incident inc;
        SNow.IncidentList incList;
        SNow.WorkOrder wo;
        SNow.WorkOrder_AssignedToSearch woAssigned;
        SNow.WorkOrderTask woTask;
        SNow.SPortal sportal = null;
        //-----------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        
        snobutton button = null;
        snoelement ele = null;
        
        snolist list = null;
        bool isServicePortal;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                portal = new Portal(Base, "Portal");
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                wo = new WorkOrder(Base, "Work Order");
                woTask = new SNow.WorkOrderTask(Base, "Work Order Task");
                woAssigned = new WorkOrder_AssignedToSearch(Base, "Work Order Assigned to Search");
                //-----------------------------------------------------
                incidentId = string.Empty;
                currentDateTime = string.Empty;
                workorderId = string.Empty;
                wotaskId = string.Empty;
                sportal = new SPortal(Base, "Service portal");

                if (Base.GData("ServicePortal").Trim().ToLower() == "yes")
                    isServicePortal = true;
                else
                    isServicePortal = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_Customer()
        {
            try
            {
                string temp = Base.GData("Customer_UserID");
                if (temp.ToLower() != "no")
                    temp = Base.GData("Customer") + ";" + temp;
                flag = home.ImpersonateUser(temp, false, null, true);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else
                {
                    if (!isServicePortal)
                        portal.WaitLoading(true);
                    else
                        sportal.WaitLoading();
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_ClickOn_Issue_Link()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.GMenu("Issues");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag) error = "Error when click on menu issue";
                        else portal.WaitLoading();
                    }
                    else error = "Cannot get menu issue.";
                }
                else
                {
                    ele = sportal.GHeaderMenu("Report an Issue");
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (flag)
                        {
                            sportal.WaitLoading();
                            flag = sportal.Verify_Page_Open("Home;Report an Issue");
                            if (!flag)
                                error = "Cannot open page [Report an Issue]";
                        }
                        else
                            error = "Error when click on menu [Report and Issue].";
                    }
                    else error = "Not found [Report an Issue] in header menu.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_02_Verify_UserName()
        {
            try
            {
                if (!isServicePortal)
                {
                    lookup = portal.Lookup_UserName();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_FullName");
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid user name value.";
                        }
                    }
                    else error = "Cannot get lookup user name.";
                }
                else
                {
                    lookup = sportal.Lookup_RequestedFor();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_FullName");
                        flag = lookup.SP_VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid user name value.";
                        }
                    }
                    else error = "Cannot get lookup user name.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_005_03_Verify_FirstName()
        //{
        //    try
        //    {
        //        if (!isServicePortal)
        //        {
        //            textbox = portal.Textbox_FirstName();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                string temp = Base.GData("Customer_FirstName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid first name value.";
        //                }
        //            }
        //            else error = "Cannot get textbox first name.";
        //        }
        //        else
        //        {
        //            textbox = sportal.Textbox_FirstName();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                string temp = Base.GData("Customer_FirstName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid first name value.";
        //                }
        //            }
        //            else error = "Cannot get textbox first name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_005_04_Verify_LastName()
        //{
        //    try
        //    {
        //        if (!isServicePortal)
        //        {
        //            textbox = portal.Textbox_LastName();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                string temp = Base.GData("Customer_LastName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid last name value.";
        //                }
        //            }
        //            else error = "Cannot get textbox last name.";
        //        }
        //        else
        //        {
        //            textbox = sportal.Textbox_LastName();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                string temp = Base.GData("Customer_LastName");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid last name value.";
        //                }
        //            }
        //            else error = "Cannot get textbox last name.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_05_Verify_Email()
        {
            try
            {
                if (!isServicePortal)
                {
                    textbox = portal.Textbox_EmailAddress();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_Email");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid email value.";
                        }
                    }
                    else error = "Cannot get textbox email.";
                }
                else
                {
                    textbox = sportal.Textbox_EmailAddress();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_Email");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid email value.";
                        }
                    }
                    else error = "Cannot get textbox email.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_005_06_Verify_UserID()
        //{
        //    try
        //    {
        //        if (!isServicePortal)
        //        {
        //            textbox = portal.Textbox_UserID();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                string temp = Base.GData("Customer_UserID");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid user id value.";
        //                }
        //            }
        //            else error = "Cannot get textbox user id.";
        //        }
        //        else
        //        {
        //            textbox = sportal.Textbox_UserID();
        //            flag = textbox.Existed;
        //            if (flag)
        //            {
        //                string temp = Base.GData("Customer_UserID");
        //                flag = textbox.VerifyCurrentValue(temp, true);
        //                if (!flag)
        //                {
        //                    flagExit = false;
        //                    error = "Invalid user id value.";
        //                }
        //            }
        //            else error = "Cannot get textbox user id.";
        //        }
                
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_07_Verify_UserTelephoneNumber()
        {
            try
            {
                if (!isServicePortal)
                {
                    textbox = portal.Textbox_UserTelephoneNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_Phone");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid user telephone number value.";
                        }
                    }
                    else error = "Cannot get textbox user telephone number.";
                }
                else
                {
                    textbox = sportal.Textbox_TelephoneNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_Phone");
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid user telephone number value.";
                        }
                    }
                    else error = "Cannot get textbox user telephone number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_08_Verify_UserLocation()
        {
            try
            {
                if (!isServicePortal)
                {
                    lookup = portal.Lookup_UserLocation();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_Location");
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid user location value.";
                        }
                    }
                    else error = "Cannot get lookup user location.";
                }
                else
                {
                    lookup = sportal.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_Location");
                        flag = lookup.SP_VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid user location value.";
                        }
                    }
                    else error = "Cannot get lookup user location.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_09_Verify_TypeLevel_1()
        {
            try
            {
                if (!isServicePortal)
                {
                    combobox = portal.Combobox_Type_1();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "--None--";
                        flag = combobox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid type level 1 value.";
                        }
                    }
                    else error = "Cannot get combobox type level 1.";
                }
                else
                {
                    snocombobox combobox = sportal.Combobox_Type_1();
                    string temp = "--None--";

                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SP_VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid default value of combobox [What kind of issue are you having?].";
                        }
                    }
                    else error = "Cannot get combobox [What kind of issue are you having?].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_10_Verify_TypeLevel_2()
        {
            try
            {
                if (!isServicePortal)
                {
                    combobox = portal.Combobox_Type_2();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "--None--";
                        flag = combobox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid type level 2 value.";
                        }
                    }
                    else error = "Cannot get combobox type level 2.";
                }
                else
                {
                    snocombobox combobox = sportal.Combobox_Type_2();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SP_VerifyCurrentValue("-- None --", true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid default value of combobox [What is the nature of the issue?].";
                        }
                    }
                    else error = "Cannot get combobox [What is the nature of the issue?].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Populate_BriefDescription()
        {
            try
            {
                if (!isServicePortal)
                {
                    currentDateTime = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                    string temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                    textbox = portal.Textbox_BriefDescription();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(temp, true);
                        if (!flag) error = "Error when populate brief description.";
                    }
                    else error = "Cannot get textbox brief description.";
                }
                else
                {
                    currentDateTime = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                    string temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                    textbox = sportal.Textbox_BriefDescription();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(temp);
                        if (!flag) error = "Error when populate brief description.";
                    }
                    else error = "Cannot get textbox brief description.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_Populate_TypeLevel_1()
        {
            try
            {
                if (!isServicePortal)
                {
                    string temp = Base.GData("Type_Level_1");
                    combobox = portal.Combobox_Type_1();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (!flag) error = "Error when populate type level 1";
                    }
                    else error = "Cannot get combobox type level 1.";
                }
                else
                {
                    string temp = Base.GData("Type_Level_1");
                    combobox = sportal.Combobox_Type_1();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SP_SelectItem(temp);
                        if (!flag) error = "Error when populate type level 1";
                    }
                    else error = "Cannot get combobox type level 1.";
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_Populate_TypeLevel_2()
        {
            try
            {
                if (!isServicePortal)
                {
                    Thread.Sleep(5000);
                    string temp = Base.GData("Type_Level_2");
                    combobox = portal.Combobox_Type_2();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (!flag) error = "Error when populate type level 2";
                    }
                    else error = "Cannot get combobox type level 2.";
                }
                else
                {
                    string temp = Base.GData("Type_Level_2");
                    combobox = sportal.Combobox_Type_2();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SP_SelectItem(temp);
                        if (!flag) error = "Error when populate type level 2";
                    }
                    else error = "Cannot get combobox type level 2.";
                    sportal.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Populate_DetailedInformation()
        {
            try
            {
                if (!isServicePortal)
                {
                    string temp = "Auto - Detailed Information";
                    textarea = portal.Textarea_DetailedInformation();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag) error = "Error when populate detailed information.";
                    }
                    else error = "Cannot get textarea detailed information.";
                }
                else
                {
                    string temp = "Auto - Detailed Information";

                    snotextarea textarea = sportal.Textarea_DetailedInformation();

                    flag = textarea.Existed;

                    if (flag)
                    {
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Can't set text [Detailed_Information]";
                        }
                    }
                    else
                    {
                        error = "The field [Detailed_Information] is NOT found";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Verify_PriorityAssessment_Items()
        {
            try
            {
                if (!isServicePortal)
                {
                    combobox = portal.Combobox_PriorityAssessment();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("PriorityAssessment_Items");
                        flag = combobox.VerifyExpectedItemsExisted(temp);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid Priority Assessment. Expected: [" + temp + "].";
                        }
                    }
                    else error = "Cannot get Priority Assessment combobox.";
                }
                else
                {
                    string temp = Base.GData("PriorityAssessment_Items");
                    snocombobox combobox = sportal.Combobox_PriorityAssessment();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SP_VerifyCurrentValue("-- None --", true);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid default value of combobox [Priority Assessment].";
                        }
                    }
                    else error = "Cannot get combobox [Priority Assessment].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_03_Populate_PriorityAssessment()
        {
            try
            {
                if (!isServicePortal)
                {
                    string temp = Base.GData("PriorityAssessment");
                    combobox = portal.Combobox_PriorityAssessment();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SelectItem(temp);
                        if (!flag) error = "Error when populate priority assessment";
                    }
                    else error = "Cannot get combobox priority assessment.";
                }
                else
                {
                    string temp = Base.GData("PriorityAssessment");
                    combobox = sportal.Combobox_PriorityAssessment();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        flag = combobox.SP_SelectItem(temp);
                        if (!flag) error = "Error when populate priority assessment";
                    }
                    else error = "Cannot get combobox priority assessment.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_Submit_Incident()
        {
            try
            {
                if (!isServicePortal)
                {
                    ele = portal.Button_Submit();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag)
                        {
                            error = "Error when click on button submit.";
                        }
                        else
                        {
                            inc.WaitLoading();
                            textbox = inc.Textbox_Number();
                            flag = textbox.Existed;
                            if (flag)
                            {
                                incidentId = textbox.Text;
                                Console.WriteLine("-*-[STORE]: Incident id (" + incidentId + ")");
                            }
                            else error = "Cannot get textbox number.";
                        }
                    }
                    else error = "Cannot get button submit.";
                }
                else
                {
                    button = sportal.Button_Submit();
                    flag = button.Existed;
                    if (flag)
                    {
                        flag = button.Click();
                        if (!flag)
                        {
                            error = "Error when click on button submit.";
                        }
                        else
                        {
                            sportal.WaitLoading();
                            button = sportal.Button_Toggle("Options");
                            flag = button.Existed;

                            if (flag)
                            {
                                flag = button.Click();
                                if (!flag) error = "Cannot click toggle button [Options]";
                                else
                                {
                                    incidentId = sportal.INC_Record_Details("Number");
                                }
                            }
                            else error = "Cannot get toggle button [Options]";
                        }
                    }
                    else error = "Cannot get button submit.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Verify_Caller()
        {
            try
            {
                if (!isServicePortal)
                {
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("Customer_FullName");
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag) { error = "Invalid caller value."; flagExit = false; }
                    }
                    else error = "Cannot get lookup caller.";
                }
                else
                {
                    string caller = sportal.INC_Options("User name:");
                    string temp = Base.GData("Customer_FullName");

                    if (caller.Equals(String.Empty))
                    {
                        error = "Cannot get [Caller].";
                    }
                    else
                    {
                        flag = caller.Equals(temp);
                        if (!flag)
                        {
                            error = "Invalid [Caller] value. Run time [" + caller + "]";
                            flagExit = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_03_Verify_ShortDescription()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && currentDateTime == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input date time value.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------
                if (!isServicePortal)
                {
                    textbox = inc.Textbox_ShortDescription();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag) { error = "Invalid short description value."; flagExit = false; }
                    }
                    else error = "Cannot get textbox short description.";
                }
                else
                {
                    string brief_description = sportal.INC_Options("Brief Description");
                    if (brief_description.Equals(String.Empty))
                    {
                        error = "Cannot get textbox brief description.";
                    }
                    else
                    {
                        temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                        flag = brief_description.Equals(temp);
                        if (!flag)
                        {
                            error = "Invalid [Brief Description] value. Run time [" + brief_description + "]";

                            flagExit = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_04_Verify_State()
        {
            try
            {
                if (!isServicePortal)
                {
                    combobox = inc.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "New";
                        flag = combobox.VerifyCurrentValue(temp, true);
                        if (!flag) { error = "Invalid state value."; flagExit = false; }
                    }
                    else error = "Cannot get combobox state.";
                }
                else
                {
                    string state = sportal.INC_Record_Details("State");
                    string temp = "New";

                    if (state.Equals(String.Empty))
                    {
                        flag = state.Equals(temp);
                        if (!flag)
                        {
                            error = "Invalid state value. Run time [" + state + "]";
                            flagExit = false;
                        }
                    }
                    else error = "Cannot get [State] label.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_Navigate_To_Itil_Page()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(temp);
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_03_ImpersonateUser_SD_Agent()
        {
            try
            {
                string temp = Base.GData("ServiceDeskAgent");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_04_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_GlobalSearch_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(incidentId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_01_Assign_To_LBS()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup01");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Add_CI()
        {
            try
            {
                Thread.Sleep(2000);
                string temp = Base.GData("ConfigurationItem");
                lookup = inc.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate configuration item value."; }
                }
                else { error = "Cannot get lookup configuration item."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_03_Populate_ReassignmentReason()
        {
            try
            {
                combobox = inc.Combobox_ReassignmentReason();
                flag = combobox.Existed;
                if (flag)
                {
                    string RR = Base.GData("ReassignmentReason");
                    flag = combobox.SelectItem(RR);
                    if (!flag)
                        error = "Cannot populate Reassignment reason value.";
                }
                else
                {
                    error = "Cannot get combobox Reassignment reason.";
                }

            }
            catch(Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
            
        }
        
       
        [Test]
        public void Step_012_04_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else { inc.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Verify_Created_WorkOrder()
        {
            try
            {
                string conditions = "Number=@@WO";
                workorderId = inc.RelatedTable_Get_Cell_Text("Work Orders", conditions, "Number");
                if (workorderId == string.Empty)
                    error = "Cannot get work order id.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_Assign_To_Another_Group()
        {
            try
            {
                string temp = Base.GData("AssignmentGroup02");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_02_Populate_ReassignmentReason()
        {
            try
            {
                combobox = inc.Combobox_ReassignmentReason();
                flag = combobox.Existed;
                if (flag)
                {
                    string RR = Base.GData("ReassignmentReason");
                    flag = combobox.SelectItem(RR);
                    if (!flag)
                        error = "Cannot populate Reassignment reason value.";
                }
                else
                {
                    error = "Cannot get combobox Reassignment reason.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        [Test]
        public void Step_014_03_SaveIncident()
        {
            try
            {
                flag = inc.Save(false, true);
                if (!flag) { error = "Error when save incident."; }
                else { inc.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_04_Verify_Error_Message()
        {
            try
            {
                string expMessage = "Work Notes are required to reassign this Incident;Invalid update";
                flag = inc.Verify_ExpectedErrorMessages_Existed(expMessage);

                if (!flag)
                {
                    error = "Can not verify error message.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_01_Open_Incident_list()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when open incident list.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_02_Add_Reassignment_Count_Column()
        {
            try
            {
                flag = incList.Add_More_Columns("Reassignment count");
                if (!flag)
                {
                    error = "Error when adding Personalize column.";
                }
                else incList.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_SearchAndVerifyIncident()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                string condition = "Reassignment count=" + Base.GData("ReassignmentCount");

                flag = incList.SearchAndVerify("Number", incidentId, condition);
                if (!flag)
                {
                    error = "Error when search and verify Incident Reassignment Count.";
                    flagExit = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }






        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Search_And_Open_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string condition = "Number=" + incidentId;
                flag = incList.SearchAndOpen("Number", incidentId, condition, "Number");
                if (!flag)
                {
                    error = "Error when search and open Incident .";
                }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Add_Attachment()
        {
            try
            {
                flag = inc.Add_AttachmentFile("incidentAttachment.txt");
                if (!flag)
                {
                    error = "Can not add attachment.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else { inc.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_ImpersonateUser_Logistic_Coordinator()
        {
            try
            {
                string temp = Base.GData("LogisticCoordinator");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }



        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Global_Search_Work_Order()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (workorderId == null || workorderId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input workorder Id");
                    addPara.ShowDialog();
                    workorderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(workorderId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Can not search work order."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_Verify_Work_Order_Caller()
        {
            try
            {
                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_FullName");
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Caller name value. Expected: [" + temp + "]. Runtime: [" + lookup.Text + "]";
                    }
                }
                else error = "Cannot get lookup Caller name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Verify_Work_Order_Location()
        {
            try
            {
                lookup = wo.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_Location");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Caller Location value. Expected: [" + temp + "]. Runtime: [" + lookup.Text + "]";
                    }
                }
                else error = "Cannot get lookup Caller Location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_03_Verify_Work_Order_ShortDescription()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && currentDateTime == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input date time value.");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------
                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Inc_ShortDescription") + currentDateTime;
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid short description value. Expected: [" + temp + "]. Runtime: [" + textbox.Text + "]";
                        flagExit = false;
                    }
                }
                else error = "Cannot get textbox short description.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Verify_Work_Order_Initiated_From()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                lookup = wo.Lookup_Initiatedfrom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(incidentId);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Wrong Incident number. Expected: [" + incidentId + "]. Runtime: [" + textbox.Text + "]";
                    }
                }
                else error = "Cannot get lookup Incident number.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Verify_Work_Order_Attachment()
        {
            try
            {
                flag = wo.Verify_Attachment_File("incidentAttachment.txt");
                if (!flag)
                {
                    error = "Attachment file is not correct.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_Verify_Category_ItemList()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("VerifyCategory");
                    flag = combobox.VerifyExpectedItemsExisted(temp);
                    if (!flag)
                    {
                        error = "Can not verify Item in the list.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Category combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_Populate_Category()
        {
            try
            {
                combobox = wo.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select [" + temp + "] in Category";
                    }
                }
                else
                {
                    error = "Cannot get Category combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Add_Attachment()
        {
            try
            {
                flag = wo.Add_AttachmentFile("wordAttachment.docx");
                if (!flag)
                {
                    error = "Cannot add work order attachment.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Click_ReadyForQualification_Button()
        {
            try
            {
                button = wo.Button_ReadyForQualification();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot click on Ready for Qualification button.";
                    }
                    else { wo.WaitLoading(); Thread.Sleep(2000); }
                }
                else
                {
                    error = "Cannot get WO Qualification button.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Verify_BlueAssignedTo_Button()
        {
            try
            {
                /*Get Work Order Task Id*/
                textbox = wo.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    wotaskId = textbox.Text;
                }
                else
                {

                    error = "Can not get Work Order Task number.";
                }

                //-----------------------------------------------------------

                ele = woTask.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (!flag)
                {
                    error = "The blue Assigned To button shoud be existed.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_Verify_WOT_Parent()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && workorderId == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input Work Order Id");
                    addPara.ShowDialog();
                    workorderId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------
                lookup = wo.Lookup_Parent();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(workorderId);
                    if (!flag)
                    {
                        error = "Can not verify Parent value.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get Parent look up.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Verify_WOT_ShortDescription()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && currentDateTime == string.Empty)
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input date time value");
                    addPara.ShowDialog();
                    currentDateTime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                textbox = wo.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Inc_ShortDescription");
                    string des = temp + currentDateTime;
                    flag = textbox.VerifyCurrentValue(des);
                    if (!flag)
                    {
                        error = "Can not verify Work Order task Short Description.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can not get Short Description text box.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Verify_WOT_Caller()
        {
            try
            {
                lookup = wo.Lookup_Caller();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Customer_FullName");
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid Caller name value.";
                    }
                }
                else error = "Cannot get lookup Caller name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_04_Verify_WOT_Company()
        {
            try
            {
                lookup = wo.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid Company name value.";
                    }
                }
                else error = "Cannot get lookup Company name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_05_Verify_WOT_Dispatch_Group()
        {
            try
            {
                lookup = wo.Lookup_DispatchGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("DispatchGroup");
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        error = "Invalid Dispatch Group name value.";
                    }
                }
                else error = "Cannot get look up Dispatch Group name.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_01_Verify_Configuration_Item_Field()
        {
            try
            {
                lookup = wo.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (!flag)
                {
                    error = "Cannot get look up Configuration Item.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_02_Verify_Configuration_Item()
        {
            try
            {
                lookup = wo.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ConfigurationItem");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid Configuration Item value.";
                    }
                }
                else
                {
                    error = "Cannot get look up Configuration Item.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_01_Save_WOT()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save Work Order Task."; }
                else { wo.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_02_Verify_Affected_Cis()
        {
            try
            {
                string temp = Base.GData("ConfigurationItem");
                string condition = "Configuration Item=" + temp;
                flag = wo.Verify_RelatedTable_Row("Affected CIs", condition);
                if (!flag)
                {
                    error = "Canot verify Affected CIs with conditon: " + condition;
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Verify_WOT_Attachments()
        {
            try
            {
                flag = wo.Verify_Attachment_File("incidentAttachment.txt");
                if (flag)
                {
                    flag = wo.Verify_Attachment_File("wordAttachment.docx");
                    if (!flag)
                    {
                        error = "The Work Order attachment is not correct.";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "The Incident attachment is not correct.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_01_Click_Qualified_Button()
        {
            try
            {
                button = woTask.Button_Qualified();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag) { error = "Cannot click Qualified button."; }
                    else wo.WaitLoading();
                }
                else
                {
                    error = "Cannot get Qualified button.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_02_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.VerifyCurrentValue("Pending Dispatch");
                if (!flag)
                {
                    error = "Cannot verify WOT State value. Expected: [Pending Dispatch]. Runtime: [" + combobox.Text + "].";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_01_Verify_WOT_Assignment_Group()
        {
            try
            {
                string temp = Base.GData("WOTAssignmentGroup");
                lookup = wo.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify WOT Assignment group value. Expected: [" + temp + "]. Runtime: [" + lookup.Text + "]";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get Assignment Group lookup.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_02_Verify_Caller_Location()
        {
            try
            {
                string temp = Base.GData("Customer_Location");
                lookup = wo.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Caller Location value. Expected: [" + temp + "]. Runtime: [" + lookup.Text + "]";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get Assignment Group lookup.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_Verify_BlueAssignedTo_Button()
        {
            try
            {
                ele = woTask.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (!flag)
                {
                    error = "The blue Assigned To button shoud be existed.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_01_Add_Skill()
        {
            try
            {
                string temp = Base.GData("Skill");
                list = wo.List_Skills();
                flag = wo.Add_List_Value(list, temp);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }



        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_02_Click_BlueAssignedTo_Button()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                ele = woTask.GImage_AssignedToPickupFromGroup();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (flag)
                    {
                        wo.WaitLoading();
                    }
                    else { error = "Cannot click Assigned To Pick Up button."; }
                }
                else { error = "Cannot get Assigned To Pick Up button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_03_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_04_Verify_SelectAssignee_Column()
        {
            try
            {
                string expectedColumn = Base.GData("ColString");
                flag = woAssigned.VerifyColumnHeader(expectedColumn, ref error, true, null, woAssigned.colDefine);
                if (!flag)
                {
                    error = "The column header is not displayed as expected. Expected: " + expectedColumn;
                }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_05_Choose_WOTechnician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                string conditions = "Name=" + temp;
                flag = woAssigned.Open(conditions, "Name", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine);
                //flag = woAssigned.SearchAndSelectContextMenu("Name", temp, conditions, "Name", "Name");
                if (!flag)
                {
                    error = "Cannot choose WO Technician with condition: " + temp;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_06_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }




        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_Save_WOT()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save Work Order Task."; flagExit = false; }
                else { wo.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.VerifyCurrentValue("Assigned");
                if (!flag)
                {
                    error = "WOT State is not correct.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_03_Verify_WOT_Substate()
        {
            try
            {
                combobox = wo.Combobox_Substate();
                flag = combobox.VerifyCurrentValue("-- None --");
                if (!flag)
                {
                    error = "WOT Substate is not correct.";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_GlobalSearch_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(incidentId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Verify_Incident_State_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                string temp = "Active";
                flag = combobox.VerifyCurrentValue(temp);
                if (!flag)
                {
                    error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_01_Global_Search_WOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = wo.GlobalSearchItem(wotaskId, true);
                if (!flag) error = "Error when search global.";
                else wo.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_Populate_Substate()
        {

            try
            {
                string temp = "Awaiting customer";
                combobox = wo.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate substate value."; }

                }
                else
                {
                    error = "Cannot get combobox substate.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_03_Populate_Additional_Comment()
        {
            try
            {
                string temp = Base.GData("AdditionalComment");
                flag = inc.Add_AdditionComments(temp);
                if (!flag)
                    error = "Cannot populate value for additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_04_Populate_FollowUpDate()
        {
            try
            {
                snodatetime datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(dt);
                    if (!flag)
                        error = "Cannot populate follow up date.";
                }
                else error = "Cannot get datetime follow up date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_05_Save_WOT()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save Work Order Task."; }
                else { wo.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_05_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                string temp = "Assigned";
                flag = combobox.VerifyCurrentValue(temp);
                if (!flag)
                {
                    error = "Invalid WOT State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_06_Verify_WOT_Substate()
        {
            try
            {
                combobox = wo.Combobox_Substate();
                string temp = "Awaiting customer";
                flag = combobox.VerifyCurrentValue(temp);
                if (!flag)
                {
                    error = "Invalid WOT Substate value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_01_GlobalSearch_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(incidentId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_02_Verify_Incident_State_AwaitingCustomer()
        {
            try
            {
                combobox = inc.Combobox_State();
                string temp = "Awaiting Customer";
                flag = combobox.VerifyCurrentValue(temp);
                if (!flag)
                {
                    error = "Invalid WOT State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                    flagExit = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_Global_Search_WOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(wotaskId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Can not search work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_Populate_WOT_Substate()
        {
            try
            {
                combobox = wo.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "-- None --";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid WOT Substate value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                    }
                }
                else
                {
                    error = "Can not get combobox Substate.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Save_WOT()
        {
            try
            {
                flag = wo.Save();
                if (!flag) { error = "Error when save Work Order Task."; }
                else { wo.WaitLoading(); }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_04_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Assigned";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid WOT State value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get State combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_05_Verify_WOT_Substate()
        {
            try
            {
                combobox = wo.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "-- None --";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid WOT Substate value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_ImpersonateUser_WO_Technician()
        {
            try
            {
                string temp = Base.GData("WOTechnician");
                string loginUser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, loginUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Global_Search_WOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(wotaskId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Can not search work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_01_Click_Accept_WOT()
        {
            try
            {
                button = wo.Button_Accept();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag)
                    {
                        error = "Can not click on Accept button.";
                    }
                    else { wo.WaitLoading(); }
                }
                else
                {
                    error = "Cannot get button Accept.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        ////----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_048_02_Verify_WOT_Progress_Bar_Accepted()
        //{
        //    try
        //    {
        //        Thread.Sleep(3000);
        //        string temp = "Accepted";
        //        flag = wo.CheckCurrentState(temp);
        //        if (!flag)
        //        {
        //            error = "Invalid current state. Expected: [" + temp + "]";
        //            flagExit = false;
        //        }
        //        else
        //        {
        //            error = "Can not get Accepted State in Progress Bar ";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
    

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_048_03_Verify_WOT_State()
            {
                try
                {
                    combobox = wo.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Accepted";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag)
                        {
                            error = "Invalid WOT State value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get State combobox."; }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_048_04_Verify_WOT_Substate()
            {
                try
                {
                    combobox = wo.Combobox_Substate();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "-- None --";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (!flag)
                        {
                            error = "Invalid WOT Substate value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get Substate combobox."; }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_049_01_GlobalSearch_Incident()
            {
                try
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                        addPara.ShowDialog();
                        incidentId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                    //-----------------------------------------------------------------------

                    flag = inc.GlobalSearchItem(incidentId, true);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Error when set text into textbox search."; }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            ////----------------------------------------------------------------------------------------------------------------------------------
            //[Test]
            //public void Step_049_02_Verify_Responded()
            //{
            //    try
            //    {
            //        checkbox = inc.Checkbox_Responded();
            //        flag = checkbox.Existed;
            //        if (flag)
            //        {
            //            flag = checkbox.Checked;
            //            if (!flag)
            //            {
            //                error = "Responded check box is not checked.";
            //                flagExit = false;
            //            }
            //        }
            //        else
            //        {
            //            error = "Cannot get checkbox Responded.";
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        flag = false;
            //        error = ex.Message;
            //    }
            //}

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_050_01_Global_Search_WOT()
            {
                try
                {
                    //-- Input information
                    string temp = Base.GData("Debug").ToLower();
                    if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                    {
                        Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id");
                        addPara.ShowDialog();
                        wotaskId = addPara.value;
                        addPara.Close();
                        addPara = null;
                    }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(wotaskId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Can not search work order task."; }
            }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_050_02_Add_Work_Notes_WOT()
            {
                try
                {
                    textarea = wo.Textarea_Worknotes();
                    flag = textarea.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("WOTWorkNotes");
                        flag = textarea.SetText(temp);
                        if (!flag)
                        {
                            error = "Can not set text for WOT Work Notes.";
                        }
                    }
                    else
                    {
                        error = "Can not get textarea Work Notes.";
                    }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_050_03_Save_WOT()
            {
                try
                {
                    flag = wo.Save();
                    if (!flag) { error = "Error when save Work Order Task."; }
                    else { wo.WaitLoading(); }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_050_04_Verify_WOT_WorkNote()
            {
                try
                {
                    string temp = Base.GData("VerifyWOTWorkNotes");
                    flag = wo.Verify_Activity(temp);
                    if (!flag)
                    {
                        error = "WOT Work Note is not correct in Activity.";
                        flagExit = false;
                    }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_051_01_Open_Quick_Comment()
            {
                try
                {
                    Base.SwitchToPage(0);
                    ele = woTask.OpenQuickComment();
                    flag = ele.Existed;
                    if (flag)
                    {
                        flag = ele.Click();
                        if (!flag)
                        {
                            error = "Can not open Quick Comment.";
                        }
                    }
                    else
                    {
                        error = "Cannot get look up Quick Comment.";
                    }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_051_02_SwitchToPage_1()
            {
                try
                {
                    Thread.Sleep(3000);
                    flag = Base.SwitchToPage(1);
                    if (!flag) { error = "Cannot switch to page (1)."; }
                }
                catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

            //----------------------------------------------------------------------------------------------------------------------------------
            [Test]
            public void Step_051_03_Select_Comment()
            {
                try
                {
                string temp = Base.GData("QuickComment");
                string conditions = "Label=" + temp;
                flag = woAssigned.SearchByColumnAndChoose("Label", conditions);
                //SearchAndSelectContextMenu("Name", temp, conditions, "Name", "Name");
                //flag = woAssigned.Open(conditions, "Label", true, null, woAssigned.colDefine, woAssigned.rowDefine, woAssigned.cellDefine);
                if (!flag)
                {
                    error = "Cannot choose Quick Comment.";
                }

            }
            catch (Exception ex)
                {
                    flag = false;
                    error = ex.Message;
                }
            }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_04_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
            }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_Click_Start_Travel_WOT()
        {
            try
            {
                ele = woTask.Button_StartTravel();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag)
                    {
                        error = "Cannot click on Start Travel button.";
                    }
                    else { wo.WaitLoading(); }
                }
                else
                {
                    error = "Cannot get button Start Travel.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_03_Verify_WOT_State()
        {
            try
            {
                Thread.Sleep(3000);
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Work In Progress";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid WOT State value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_04_Verify_WOT_Substate()
        {
            try
            {
                combobox = wo.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "-- None --";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid WOT Substate value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_01_GlobalSearch_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = inc.GlobalSearchItem(incidentId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_02_Verify_Incident_State_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Active";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid WOT State value. Expected: [" + temp + "].Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_01_Global_Search_WOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(wotaskId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Can not search work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_02_Populate_WorkNote()
        {
            try
            {
                string temp = Base.GData("WOTWorkNotes");
                flag = wo.Add_Worknotes(temp);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_03_Select_Closure_Information_tab()
        {
            try
            {

                flag = wo.Select_Tab("Closure Information");
                if (!flag)
                    error = "Cannot select Closure Information Tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

         
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_04_Verify_And_Select_CIUpdateStatus()
        {
            try
            {
                combobox = wo.Combobox_CIUpdate_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("CIUpdateStatus");
                    flag = combobox.VerifyActualItemsExisted(temp);
                    if (flag)
                    {
                        flag = combobox.SelectItem("CI Updated");
                        if (!flag)
                        {
                            error = "Can not select item in combobox CI Update Status.";
                        }
                    }
                    else { error = "Cannot verify CI Update Status item list."; }
                }
                else
                {
                    error = "Cannot get combobox CI Update Status combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_05_Populate_Close_Notes()
        {
            try
            {
                textarea = wo.Textarea_CloseNotes();
                flag = textarea.Existed;
                if (flag)
                {
                    string temp = Base.GData("CloseNotes");
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Close Notes value.";
                    }
                }
                else
                {
                    error = "Cannot get textare Close Notes.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_06_Click_Close_Complete()
        {
            try
            {
                button = wo.Button_CloseComplete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (!flag)
                    {
                        error = "Can not click on Close Complete button.";
                    }
                    else { wo.WaitLoading(); }
                }
                else
                {
                    error = "Can not get button Close Complete.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_Global_Search_WOT()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (wotaskId == null || wotaskId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input work order task Id");
                    addPara.ShowDialog();
                    wotaskId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = inc.GlobalSearchItem(wotaskId, true);
                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Can not search work order task."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //---------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_01_Verify_WOT_Progress_Bar_Complete()
        {
            try
            {
                Thread.Sleep(3000);
                string temp = "Complete";
                flag = wo.CheckCurrentState(temp);
                if (!flag)
                {
                    error = "Invalid current state. Expected: [" + temp + "]";
                    flagExit = false;
                }
                else
                {
                    error = "Can not get Complete State in Progress Bar ";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

       // ----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_02_Verify_WOT_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Open_WO_Parent_Field()
        {
            try
            {
                ele = wo.Button_WO_Parent();
                flag = ele.Existed;
                if (flag)
                {
                    flag = ele.Click();
                    if (!flag)
                    {
                        error = "Can not click on button WO Parent.";
                    }
                    else { wo.WaitLoading(); }
                }
                else
                {
                    error = "Can not get button WO_Parent";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_Verify_WO_State()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Closed Complete";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can not get combobox WO State.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_01_GlobalSearch_Incident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag= inc.GlobalSearchItem(incidentId, true);

                if (flag)
                {
                    inc.WaitLoading();
                }
                else { error = "Error when set text into textbox search."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_02_Verify_Incident_State_Resolved()
        {
            try
            {
                combobox = wo.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Resolved";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Invalid State value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot get State combobox.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        }
    }

    
    

