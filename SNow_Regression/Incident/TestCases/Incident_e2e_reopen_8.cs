﻿using System;
using NUnit.Framework;
using System.Reflection;
using SNow;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Incident
{
    [TestFixture]
    public class Incident_e2e_reopen_8
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {

            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        string incidentId;
        Login login;
        Home home;
        SNow.Incident inc;
        SNow.IncidentList incList;
        SNow.KnowledgeSearch knSearch;
        SNow.Portal portal;
        SNow.EmailList emailList;
        SNow.Member member;

        //-----------------------------
        snotextbox textbox = null;
        snotextarea textarea = null;
        snolookup lookup = null;
        snocombobox combobox = null;
        snodatetime datetime = null;
        snobutton button = null; 

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        [Test]
        public void ClassInit()
        {
            try
            {
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                incList = new SNow.IncidentList(Base, "Incident list");
                knSearch = new SNow.KnowledgeSearch(Base);
                portal = new SNow.Portal(Base, "Portal home");
                emailList = new SNow.EmailList(Base, "Email list");
                member = new SNow.Member(Base);
                //-----------------------------------------------------
                incidentId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);
                
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    Thread.Sleep(1000);
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id:(" + incidentId + ")");
                    string temp = Base.GData("Caller");
                    lookup = inc.Lookup_Caller();
                    lookup.Click();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");

                if (temp.ToLower() == "no")
                {
                    lookup = inc.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            error = "Invalid company value or the value is not auto populate.";
                            flagExit = false;
                        }
                    }
                    else { error = "Cannot get lookup company."; }
                }
                else Console.WriteLine("*** No need verify this step.");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("CallerEmail");
                if (temp.ToLower() != "no")
                {
                    textbox = inc.Textbox_Email();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.VerifyCurrentValue(temp, true);
                        if (!flag)
                        {
                            error = "Invalid caller email or the value is not auto populate. Runtime value [:" + textbox.Text + "] - Expected value: [" + temp + "]";
                            flagExit = false;
                        }
                    }
                    else
                        error = "Cannot get caller email.";
                }
                else Console.WriteLine("Not verify.");

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_PopulateBusinessService()
        {
            try
            {
                string temp = Base.GData("BusinessService01");
                lookup = inc.Lookup_BusinessService();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate business service value."; }
                }
                else
                    error = "Cannot get lookup business service.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }

                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_PopulateContactType()
        {
            try
            {
                string temp = Base.GData("ContactType");
                combobox = inc.Combobox_ContactType();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate contact type value."; }
                }
                else
                    error = "Cannot get combobox contact type.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_AddACI()
        {
            try
            {
                string temp = Base.GData("CI");
                lookup = inc.Lookup_ConfigurationItem();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot add CI ."; }
                }
                else
                    error = "Cannot get lookup CI.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_AddWorkNote()
        {
            try
            {
                string temp = Base.GData("WorkNote");
                flag = inc.Add_Worknotes(temp);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------       
        [Test]
        public void Step_018_PopulateAssignmentGroup_RG()
        {
            try
            {
                string temp = Base.GData("ResolverGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_020_SearchAndOpenIncident()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
        //            addPara.ShowDialog();
        //            incidentId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        flag = home.LeftMenuItemSelect("Incident", "Open");
        //        if (flag)
        //        {
        //            incList.WaitLoading();
        //            temp = incList.List_Title().MyText.ToLower();
        //            flag = temp.Equals("incidents");
        //            if (flag)
        //            {
        //                flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
        //                if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
        //                else inc.WaitLoading();
        //            }
        //            else
        //            {
        //                error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
        //            }
        //        }
        //        else error = "Error when select open incident.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_ImpersonateUser_RV_1()
        {
            try
            {
                string temp = Base.GData("Resolver1");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate resolver user (" + temp + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------       
        [Test]
        public void Step_024_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_025_SearchAndChangeStateInlist()
        //{
        //    try
        //    {
        //        //-- Input information
        //        string temp = Base.GData("Debug").ToLower();
        //        if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
        //        {
        //            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
        //            addPara.ShowDialog();
        //            incidentId = addPara.value;
        //            addPara.Close();
        //            addPara = null;
        //        }
        //        //-----------------------------------------------------------------------
        //        flag = home.LeftMenuItemSelect("Incident", "Open");
        //        if (flag)
        //        {

        //            string conditions = "Number=" + incidentId;
        //            flag = incList.SearchAndEdit("Number", incidentId, conditions, "State", "Awaiting Vendor");
        //            if (flag)
        //            {
        //                IAlert alert = Base.Driver.SwitchTo().Alert();
        //                if (alert != null)
        //                {
        //                    temp = "You can only select Awaiting Vendor state on the Incident form";
        //                    if (!alert.Text.Equals(temp))
        //                    {
        //                        flag = false;
        //                        error = "Invalid alert message. Runtime:(" + alert.Text + "). Expexted:(" + temp + ")";
        //                    }
        //                    alert.Accept();
        //                }
        //                else error = "Alert is not visible.";
        //            }
        //            else error = "Cannot change state value in list.";
        //        }
        //        else error = "Error when select open incident.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_OpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_01_PopulateState_Active()
        {
            try
            {
                string temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_02_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_03_PopulateState_AwaitingVendor()
        {
            try
            {
                string temp = "Awaiting Vendor";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_04_PopulateVendor()
        {
            try
            {
                string temp = Base.GData("Vendor");
                lookup = inc.Lookup_Vendor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate vendor value."; }
                }
                else
                    error = "Cannot get lookup vendor field.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_05_Populate_FollowUpDate()
        {
            try
            {
                string d = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.SetText(d);
                    if (!flag) { error = "Cannot populate follow up date value."; }
                }
                else
                    error = "Cannot get datetime follow up date field.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_06_UpdateIncident()
        {
            try
            {
                button = inc.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                }
                else { error = "Cannot save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SearchAndVerifyIncident()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string vendor = Base.GData("Vendor");
                string conditions = "Number=" + incidentId + "|State=Awaiting Vendor|Vendor=" + vendor;
                flag = incList.SearchAndVerify("Number", incidentId, conditions);
                if (!flag)
                    error = "Not found incident [" + conditions + "]";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_029_ChangeStateInList()
        //{
        //    try
        //    {

        //        string conditions = "Number=" + incidentId;
        //        flag = incList.SearchAndEdit("Number", incidentId, conditions, "State", "Awaiting Evidence");
        //        incList.WaitLoading();
        //        if (!flag)
        //        {
        //            //flag = incList.SearchAndEdit("Number", incidentId, conditions, "State", "Awaiting Vendor");
        //            //if (flag)
        //            //{
        //            //IAlert alert = Base.Driver.SwitchTo().Alert();
        //            //if (alert != null)
        //            //{
        //            //    string temp = "You can only select Awaiting Vendor state on the Incident form";
        //            //    if (!alert.Text.Equals(temp))
        //            //    {
        //            //        flag = false;
        //            //        error = "Invalid alert message. Runtime:(" + alert.Text + "). Expexted:(" + temp + ")";
        //            //    }
        //            //    else
        //            //    {
        //            //        alert.Accept();
        //            //        string vendor = Base.GData("Vendor");
        //            //        flag = incList.SearchAndVerify("Number", incidentId, "Vendor=" + vendor);
        //            //        if (!flag)
        //            //        {
        //            //            flagExit = false;
        //            //            error = "Vendor field is not read-only field";
        //            //        }
        //            //    }
        //            //}
        //            //else error = "Alert is not visible.";
        //            //}
        //            //else error = "Cannot change state Awaiting Vendor value in list.";
        //                        flagExit = false;
        //                        error = "Cannot change state Awaiting Evidence value in list.";
        //        }
        //        // else error = "Cannot change state Awaiting Evidence value in list.";
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_OpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_031_01_VerifyVendorField_NotVisible()
        //{
        //    try
        //    {
        //        lookup = inc.Lookup_Vendor();
        //        flag = lookup.Existed;
        //        if (flag)
        //        {
        //            flag = false;
        //            error = "Vendor field is on the form. This field is only visible when state is awaiting vendor."; ;
        //        }
        //        else
        //        {
        //            flag = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        /* Huong C - Removed Validation steps
        [Test]
        public void Step_031_02_VeifyFollowUpDate_Visible()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (!flag)
                {
                    flag = false;
                    flagExit = false;
                    error = "-*- ERROR: Follow up date is not visible on form. Should be displayed as incident's state is Awaiting Evidence.";
                }
                else
                    System.Console.WriteLine("-*- Passed: Follow up date is visible on form. Incident's state is Awaiting Evidence");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        } */
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_Populate_FollowUpdate_Invalid_Value()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    DateTime date = DateTime.Now;
                    string today = date.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(today);
                    if (!flag)
                        error = "Error when populate follow up date value.";
                }
                else error = "Cannot get datetime follow up date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Save_Expected_CannotSave()
        {
            try
            {
                flag = inc.Save(false, true);
                if (flag)
                {

                    flag = inc.Verify_ExpectedErrorMessages_Existed("The Follow up date must be greater than the Current date" + ";Invalid update");
                    if (!flag)
                        error = "Not found error message.";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_01_Populate_FollowUpdate_Valid_Value()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (flag)
                {
                    DateTime date = DateTime.Now;
                    string today = date.AddDays(+1).ToString("yyyy-MM-dd HH:mm:ss");
                    flag = datetime.SetText(today);
                    if (!flag)
                        error = "Error when populate follow up date value.";
                }
                else error = "Cannot get datetime follow up date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_02_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_01_PopulateState_Active()
        {
            try
            {
                string temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_02_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_03_PopulateState_Awaiting_Change()
        {
            try
            {
                string temp = "Awaiting Change";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_04_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_05_PopulateState_Active()
        {
            try
            {
                string temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_06_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_07_PopulateState_BackTo_Awaiting_Vendor()
        {
            try
            {
                string temp = "Awaiting Vendor";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_08_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_09_Verify_Correct_Vendor()
        {
            try
            {
                string temp = Base.GData("Vendor");
                lookup = inc.Lookup_Vendor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Invalid vendor value."; }
                }
                else
                    error = "Cannot get lookup vendor field.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        /* Huong C - Removed Validation steps
        [Test]
        public void Step_034_02_ClearDataVendor()
        {
            try
            {
                string temp = string.Empty;
                lookup = inc.Lookup_Vendor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText(temp);
                    if (flag)
                    {
                        lookup.MyElement.Clear();
                        Thread.Sleep(5000);
                        textbox = inc.Textbox_Number();
                        flag = textbox.Existed;
                        if (flag)
                        {
                            textbox.MyElement.Click();
                        }
                        else
                        {
                            error = "Cannot click incident number field";
                        }
                    }
                    else { error = "Cannot clear vendor value."; }
                }
                else
                    error = "Cannot get lookup vendor field.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_03_SaveIncidentAndVerifyAlert()
        {
            try
            { 
                //Save Incident and Verify alert message
               
                flag = inc.Save(false, true);

                if (flag)
                {
                    try
                    {
                        IAlert alert = Base.Driver.SwitchTo().Alert();
                        string temp = "The following mandatory fields are not filled in: Vendor";
                        if (!alert.Text.Equals(temp))
                        {
                            flag = false;
                            error = "Invalid alert message. Runtime:(" + alert.Text + "). Expexted:(" + temp + ")";
                        }
                        alert.Accept();
                    }
                    catch
                    {
                        flagExit = false;
                        error = "Alert is not visible.";
                    }
                }

                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_04_PopulateVendor()
        {
            try
            {
                string temp = Base.GData("Vendor");
                lookup = inc.Lookup_Vendor();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate vendor value."; }
                }
                else
                    error = "Cannot get lookup vendor field.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        } */
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_034_05_SaveIncident()
        //{
        //    try
        //    {
        //        flag = inc.Save();
        //        if (!flag) { error = "Error when save incident."; }
        //        else inc.WaitLoading();
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //----------------------------------------------------------------------------------------------------------------------------------- 
        [Test]
        public void Step_036_037_SearchAndAssignToMe()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null; ;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {

                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {

                        flag = incList.SearchAndSelectContextMenu("Number", incidentId, "Number=" + incidentId, "Number", "Assign to me");
                        if (!flag) error = "Error when search and select assign to me.(id:" + incidentId + ")";
                        else
                            incList.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_OpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_40_VerifyGroupAndAssignee()
        {
            try
            {
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ResolverGroup");
                    if (lookup.Text == temp)
                    {
                        lookup = inc.Lookup_AssignedTo();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Resolver1");
                            //lookup = inc.Lookup_AssignedTo();
                            if (lookup.Text != temp)
                            {
                                flag = false;
                                error = "Resolver value does not match";
                            }
                        }
                        else
                        {
                            error = "Cannot get Assgin to field";
                        }
                    }
                    else
                    {
                        error = "Resolver group does not match";
                    }
                }
                else
                {
                    error = "Cannot get Assginment group field";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_ImpersonateUser_RV_A()
        {
            try
            {
                string temp = Base.GData("Resolver_A");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate resolver user (" + temp + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_044_SearchAndAssignToMe()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null; ;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {

                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {

                        flag = incList.SearchAndSelectContextMenu("Number", incidentId, "Number=" + incidentId, "Number", "Assign to me");
                        if (!flag) error = "Error when search and select assign to me.(id:" + incidentId + ")";
                        else
                            incList.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_VerifyErrorMessage()
        {
            try
            {
                string temp = Base.GData("ErrorMessage");
                //   flag = incList.(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "The error message is not expected";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_ImpersonateUser_RV_1()
        {
            try
            {
                string temp = Base.GData("Resolver1");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate resolver user (" + temp + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_03_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_PopulateResolver2()
        {
            try
            {
                string temp = Base.GData("Resolver2");
                lookup = inc.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment to value."; }
                }
                else { error = "Cannot get lookup assignment to field."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

       
        
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_CheckActivityNote()
        {
            try
            {
                string temp = Base.GData("Activity_48");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }



        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_AddWorkNote()
        {
            try
            {
                string temp = Base.GData("WorkNote");
                flag = inc.Add_Worknotes(temp);
                if (!flag)
                    error = "Cannot populate value for work notes.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_052_CheckActivityNote()
        {
            try
            {
                string temp = Base.GData("Activity_52");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_ImpersonateUser_RV_2()
        {
            try
            {
                string temp = Base.GData("Resolver2");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate resolver user (" + temp + ")";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_056_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_ClickOnSearchKnowledge()
        {
            try
            {
                button = inc.Button_SearchKnowledge();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        Base.SwitchToPage(1);
                        knSearch.WaitLoading();
                    }
                }
                else { error = "Cannot get button search knowledge."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_SearchAndSelectArticle()
        {
            try
            {
                Base.SwitchToPage(1);
                string temp = Base.GData("KnowledgeArticle");
                flag = knSearch.SearchAndOpen(temp);
                if (!flag)
                    error = "Cannot open knowledge artical .";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_ClickOnAttachToIncident()
        {
            try
            {
                Base.SwitchToPage(1);
                button = knSearch.Button_Attach();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    Thread.Sleep(2000);
                    Base.SwitchToPage(0);
                }
                else error = "Cannot get button Attach.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag)
                    error = "Error when save incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_VerifyKnowledgeArticle()
        {
            try
            {
                string temp = Base.GData("KnowledgeArticle01");
                flag = inc.VerifyKnowledgeWasAttached(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found knowledge attached to incident.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_02_AddAdditionalComment()
        {

            try
            {
                string temp = Base.GData("AdditionalComment");
                flag = inc.Add_AdditionComments(temp);
                if (!flag)
                    error = "Cannot populate value for additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        /* Huong C - Removed Validation steps
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_Verify_Found_FollowUpdate()
        {
            try
            {
                datetime = inc.Datetime_Followupdate();
                flag = datetime.Existed;
                if (!flag)
                    error = "Follow up date field is NOT visible.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        } */
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_01_PopulateStateActive()
        {
            try
            {
                string temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();

                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_067_02_VeifyFollowUpDate_NotVisible()
        //{
        //    try
        //    {
        //        datetime = inc.Datetime_Followupdate();
        //        flag = datetime.Existed;
        //        if (flag)
        //        {
        //            error = "Follow up date field is visible. Expected is NOT visible";
        //            flag = false;
        //            flagExit = false;
        //        }
        //        else
        //        {
        //            flag = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_03_AddAdditionalComment2()
        {
            try
            {
                string temp = Base.GData("AdditionalComment2");
                
                flag = inc.Add_AdditionComments(temp);
                if (!flag)
                    error = "Cannot populate value for additional comments.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_CheckActivityNote()
        {
            try
            {
                string temp = Base.GData("Activity_69");
                flag = inc.Verify_Activity(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid activity";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_OpenImpactedServicesTab_1()
        {
            try
            {
                flag = inc.Select_Tab("Impacted Services");
                if (!flag)
                {
                    error = "Cannot click on tab (Impacted Services)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_AddTwoImpactService()
        {
            try
            {
                string temp = Base.GData("BusinessService02") + ";" + Base.GData("BusinessService03");
                flag = inc.Add_Related_Members("Impacted Services", temp);

                if (!flag)
                {
                    error = "Cannot add two Impact Service";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_OpenImpactedServicesTab_2()
        {
            try
            {
                flag = inc.Select_Tab("Impacted Services");
                if (!flag)
                {
                    error = "Cannot click on tab (Impacted Services)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_Delete2ndImpactService()
        {
            try
            {
                string temp = Base.GData("BusinessService03");
                flag = inc.Delete_Related_Members("Impacted Services", temp);

                if (!flag)
                {
                    error = "Cannot delete two Impact Service";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_OpenImpactedServicesTab_3()
        {
            try
            {
                flag = inc.Select_Tab("Impacted Services");
                if (!flag)
                {
                    error = "Cannot click on tab (Impacted Services)";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_Verify1sImpactService()
        {
            try
            {
                string condition = "Business Service=" + Base.GData("BusinessService01");
                flag = inc.Verify_RelatedTable_Row("Impacted Services", condition);
                if (!flag)
                {
                    flagExit = false;
                    error = "Not found Impacted Service response: " + condition;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_01_ResolvingTicket()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Invalid state selected.";
                    }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_02_PopulateCloseCodeAndCloseNotes()
        {
            try
            {


                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = Base.GData("CloseCode");
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = Base.GData("CloseNote");
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_01_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_02_Verify_ProcessFlow_Resolved()
        {
            try
            {
                string temp = "Resolved";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current process flow. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_ImpersonateUser_RV_3()
        {
            try
            {
                string temp = Base.GData("Resolver3");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) error = "Error when impersonate resolver user (" + temp + ")";
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) error = "Error when config system.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_081_SearchAndOpenIncident()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    incList.WaitLoading();
                    temp = incList.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = incList.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_089_ReopeningTicket()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Active";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        flag = false;
                        error = "Invalid state selected.";
                    }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_01_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_090_02_Verify_ProcessFlow_Active()
        {
            try
            {
                string temp = "Active";
                flag = inc.Verify_CurrentProcess(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid current state or Cannot check current state. Expected: [" + temp + "].";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_091_01_ResolvingTicket()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Resolved";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        flag = false;
                        error = "Invalid state selected.";
                    }
                }
                else { error = "Cannot get combobox state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_091_02_Verify_CloseCodeCloseNote_NotKeepOldValue()
                {
                    try
                    {
                        
                        flag = inc.Select_Tab("Closure Information");
                        if (flag)
                        {
                            combobox = inc.Combobox_CloseCode();
                            flag = combobox.Existed;
                            if (flag)
                            {                        
                                string temp = "-- None --";
                                flag = combobox.VerifyCurrentValue(temp);
                                if (flag)
                                {
                                    textarea = inc.Textarea_CloseNotes();
                                    flag = textarea.Existed;
                                    if (flag)
                                    {
                                        if (!textarea.Text.Equals(""))
                                        {
                                            flag = false;
                                            flagExit = false;
                                            error = "Invalid expected value. Expected: [" + "]. Runtime: [" + textarea.Text + "]";
                                        }
                                    }
                                    else error = "Cannot get textarea close notes.";
                                }
                                else { error = "Cannot verify or invalid value. Expected: [" + temp + "]. Runtime: [" + combobox.Text + "]"; flagExit = false; }
                            }
                            else error = "Cannot get combobox close code.";
                        }
                        else error = "Cannot select tab Closure Information.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------

                [Test]
                public void Step_092_Populate_CloseCode_CloseNote()
                {
                    try
                    {
                        flag = inc.Select_Tab("Closure Information");
                        if (flag)
                        {
                            combobox = inc.Combobox_CloseCode();
                            flag = combobox.Existed;
                            if (flag)
                            {
                                string temp = Base.GData("CloseCode");
                                flag = combobox.SelectItem(temp);
                                if (flag)
                                {
                                    textarea = inc.Textarea_CloseNotes();
                                    flag = textarea.Existed;
                                    if (flag)
                                    {
                                        temp = Base.GData("CloseNote");
                                        flag = textarea.SetText(temp);
                                        if (!flag) error = "Cannot populate close notes.";
                                    }
                                    else error = "Cannot get textarea close notes.";
                                }
                                else error = "Cannot populate close code value.";
                            }
                            else error = "Cannot get combobox close code.";
                        }
                        else error = "Cannot select tab Closure Information.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_093_01_SaveIncident()
                {
                    try
                    {
                        flag = inc.Save();
                        if (!flag) { error = "Error when save incident."; }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_093_02_Verify_ProcessFlow_Resolved()
                {
                    try
                    {
                        string temp = "Resolved";
                        flag = inc.Verify_CurrentProcess(temp);
                        if (!flag)
                        {
                            flagExit = false;
                            error = "Invalid current state or Cannot check current state. Expected: [" + temp + "].";
                        }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------

                [Test]
                public void Step_094_ValidateCloseState()
                {
                    try
                    {
                        string temp = "Closed";
                        combobox = inc.Combobox_State();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            bool flagTemp;
                            flagTemp = combobox.SelectItem(temp);
                            if (!flagTemp)
                            {
                                
                                flag = true;
                            }
                            else
                            {
                                
                                flag = false;
                                error = "The state [" + temp + "] is in the list";
                            }
                        }
                        else
                        {
                            error = "Cannot get combobox state.";
                        }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_095_ImpersonateUser_TestUser()
                {
                    try
                    {
                        string temp = Base.GData("TestUser");
                        string rootuser = Base.GData("UserFullName");
                        flag = home.ImpersonateUser(temp, true, rootuser);
                        if (!flag) error = "Error when impersonate resolver user (" + temp + ")";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_096_SystemSetting()
                {
                    try
                    {
                        flag = home.SystemSetting();
                        if (!flag) error = "Error when config system.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_097_1_OpenEmailLog()
                {
                    try
                    {
                        flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                        if (flag)
                        {
                            emailList.WaitLoading();

                            if (!emailList.List_Title().MyText.Contains("Emails"))
                            {
                                flag = false;
                                error = "Cannot open email list.";
                            }
                        }
                        else error = "Error when open email log.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_097_2_Filter_EmailSentToCaller_Opened()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string email = Base.GData("Caller_Email");

                        temp = "Subject;contains;" + incidentId + "|and|Subject;contains;opened|and|" + "Recipients;contains;" + email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                        }
                        else { error = "Error when filter."; }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------     
                [Test]
                public void Step_097_3_Validate_EmailSentToCaller_Opened()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string conditions = "Subject=@@" + incidentId;
                        flag = emailList.VerifyRow(conditions);
                        if (!flag) error = "Not found email sent to caller (opened).";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_098_1_OpenEmailLog()
                {
                    try
                    {
                        flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                        if (flag)
                        {
                            emailList.WaitLoading();

                           
                             if (!emailList.List_Title().MyText.Contains("Emails"))
                            {
                                flag = false;
                                error = "Cannot open email list.";
                            }
                        }
                        else error = "Error when open email log.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_098_2_Filter_EmailSentGroup_Assigned()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string group = Base.GData("ResolverGroup");
                        string email = Base.GData("ResolverGroup_Email");

                        if (email.ToLower() == "no" || email.ToLower() == "empty")
                        {
                            email = Base.GData("ResolverGroup_Email_Member");
                        }

                        temp = "Subject;contains;" + incidentId + "|and|Subject;contains;has been assigned to group " + group + "|and|" + "Recipients;contains;" + email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                        }
                        else { error = "Error when filter."; }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_098_3_Validate_EmailSentGroup_Assigned()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string conditions = "Subject=@@" + incidentId;
                        flag = emailList.VerifyRow(conditions);
                        if (!flag) error = "Not found email sent to group (assigned)";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_099_1_OpenEmailLog()
                {
                    try
                    {
                        flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                        if (flag)
                        {
                            emailList.WaitLoading();

                           if (!emailList.List_Title().MyText.Contains("Emails"))
                            {
                                flag = false;
                                error = "Cannot open email list.";
                            }
                        }
                        else error = "Error when open email log.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_099_2_Filter_EmailSentToCaller_CommentAdd()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------

                        string email = Base.GData("Caller_Email");

                        temp = "Subject;contains;" + incidentId + "|and|Subject;contains;comments added|and|" + "Recipients;contains;" + email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                        }
                        else { error = "Error when filter."; }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_099_3_Validate_EmailSentToCaller_CommentAdd()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string conditions = "Subject=@@" + incidentId;
                        flag = emailList.VerifyRow(conditions);
                        if (!flag) error = "Not found email sent to caller with additional comments";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_100_1_OpenEmailLog()
                {
                    try
                    {
                        flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                        if (flag)
                        {
                            emailList.WaitLoading();

                           if (!emailList.List_Title().MyText.Contains("Emails"))
                            {
                                flag = false;
                                error = "Cannot open email list.";
                            }
                        }
                        else error = "Error when open email log.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_100_2_Filter_EmailSentToResolver2_Assigned()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------

                        string email = Base.GData("Resolver2_Email");

                        if (email.ToLower() == "no" || email.ToLower() == "empty")
                        {
                            email = Base.GData("ResolverGroup_Email_Member");
                        }

                        temp = "Subject;contains;" + incidentId + "|and|Subject;contains;has been assigned to you|and|" + "Recipients;contains;" + email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                        }
                        else { error = "Error when filter."; }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_100_3_Validate_EmailSentToResolver2_Assigned()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string conditions = "Subject=@@" + incidentId;
                        flag = emailList.VerifyRow(conditions);
                        if (!flag) error = "Not found email sent to caller (Assigned).";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
        //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_102_1_OpenEmailLog()
                {
                    try
                    {
                        flag = home.LeftMenuItemSelect("CSC Run", "Email Log");
                        if (flag)
                        {
                            emailList.WaitLoading();

                            if (!emailList.List_Title().MyText.Contains("Emails"))
                            {
                                flag = false;
                                error = "Cannot open email list.";
                            }
                        }
                        else error = "Error when open email log.";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_102_2_Filter_EmailSentToCaller_Resolved()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------

                        string email = Base.GData("Caller_Email");

                        temp = "Subject;contains;" + incidentId + "|and|Subject;contains;has been resolved|and|" + "Recipients;contains;" + email;
                        flag = emailList.EmailFilter(temp);
                        if (flag)
                        {
                            emailList.WaitLoading();
                        }
                        else { error = "Error when filter."; }
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
                //-----------------------------------------------------------------------------------------------------------------------------------
                [Test]
                public void Step_102_3_Validate_EmailSentToCaller_Resolved()
                {
                    try
                    {
                        string temp = Base.GData("Debug").ToLower();
                        if (temp == "yes" && incidentId == string.Empty)
                        {
                            Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                            addPara.ShowDialog();
                            incidentId = addPara.value;
                            addPara.Close();
                            addPara = null;
                        }
                        //---------------------------------------------------------------------------------------------------
                        string conditions = "Subject=@@" + incidentId;
                        flag = emailList.VerifyRow(conditions);
                        if (!flag) error = "Not found email sent to caller (Resolved).";
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        error = ex.Message;
                    }
                }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_103_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
            }
        }
    

