﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
namespace Security
{
    [TestFixture]
    public class security_prb
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;
        
        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            System.Console.WriteLine("*|||*[Run step:" + TestContext.CurrentContext.Test.Name + "]");
            System.Console.WriteLine("-----------------------------------------------------------------------------------------------------------------");
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Problem Id 1: " + PrbId1);
            System.Console.WriteLine("Finished - Problem Id 2: " + PrbId2);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Problem prb = null;
        SNow.ProblemList prblist = null;
        //------------------------------------------------------------------
        string PrbId1, PrbId2;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                prb = new SNow.Problem(Base, "Incident");
                prblist = new SNow.ProblemList(Base, "Incident list");
                //------------------------------------------------------------------
                PrbId1 = string.Empty;
                PrbId2 = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                string driver = Base.GData("Url");
                Base.Driver.Navigate().GoToUrl(driver);
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Can not login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_ProblemManager_1()
        {
            try
            {
                string temp = Base.GData("ProblemManager1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewProblem_1()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                    prb.WaitLoading();
                else
                    error = "Error when create new problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "PRB*");
                        if (!flag) error = "Invalid format of problem number.";
                        else { PrbId1 = temp; Console.WriteLine("-*-[STORE]: Problem Id:(" + PrbId1 + ")"); }
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }
                string comp = Base.GData("Company1");
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(comp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_PopulateProblemStatement()
        {
            try
            {
                string temp = Base.GData("PRB1ProbStatement");
                textbox = prb.Textbox_ProblemStatement();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }     
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("PRB1Category");
                combobox = prb.Combobox_Category();
                flag = combobox.Existed; 
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_PopulateSubCategory()
        {
            try
            {
               string temp = Base.GData("PRB1SubCat");
                combobox = prb.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }

                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_03_PopulateImpact()
        {
            try
            {
                string temp = Base.GData("PRB1Impact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Impact combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_04_PopulateProblemSource()
        {
            try
            {
                string temp = Base.GData("PRB1Source");
                combobox = prb.Combobox_ProblemSource();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Impact combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
      
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_SearchAndOpenProblem_ExpectFound()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (PrbId1 == null || PrbId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem 1 Id.");
                    addPara.ShowDialog();
                    PrbId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prblist.WaitLoading();
                    temp = prblist.List_Title().MyText;
                    flag = temp.Equals("Problems");
                    if (flag)
                    {
                        flag = prblist.SearchAndOpen("Number", PrbId1, "Number=" + PrbId1, "Number");
                        if (!flag) error = "Error when search and open problem (id:" + PrbId1 + ")";
                        else prb.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_ImpersonateUser_ProblemManager_2()
        {
            try
            {
                string rootUser = Base.GData("UserFullName");
                string temp = Base.GData("ProblemManager2");
                flag = home.ImpersonateUser(temp,true,rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_SearchAndOpenProblem_ExpectNotFound()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (PrbId1 == null || PrbId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem 1 Id.");
                    addPara.ShowDialog();
                    PrbId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prblist.WaitLoading();
                    temp = prblist.List_Title().MyText;
                    flag = temp.Equals("Problems");
                    if (flag)
                    {
                        flag = prblist.SearchAndVerify("Number", PrbId1, "Number=" + PrbId1, true);
                        if (!flag)
                            error = "Found (id:" + PrbId1 + ")";
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_OpenNewProblem_2()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Problem", "Create New");
                if (flag)
                    prb.WaitLoading();
                else
                    error = "Error when create new problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_PopulateCompany()
        {
            try
            {
                textbox = prb.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.Click();
                    if (flag)
                    {
                        string temp = textbox.Text;
                        flag = Regex.IsMatch(temp, "PRB*");
                        if (!flag) error = "Invalid format of problem number.";
                        else { PrbId2 = temp; Console.WriteLine("-*-[STORE]: Problem Id:(" + PrbId2 + ")"); }
                    }
                    else error = "Error when click on textbox number.";
                }
                else { error = "Cannot get textbox number."; }
                string comp = Base.GData("Company2");
                lookup = prb.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(comp);
                    if (!flag) { error = "Cannot populate company value."; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_PopulateProblemStatement()
        {
            try
            {
                string temp = Base.GData("PRB2ProbStatement");
                textbox = prb.Textbox_ProblemStatement();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("PRB2Category");
                combobox = prb.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("PRB2SubCat");
                combobox = prb.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        prb.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_03_PopulateImpact()
        {
            try
            {
                string temp = Base.GData("PRB2Impact");
                combobox = prb.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Impact combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_04_PopulateProblemSource()
        {
            try
            {
                string temp = Base.GData("PRB2Source");
                combobox = prb.Combobox_ProblemSource();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item: [" + temp + "].";
                    }
                }
                else { error = "Cannot get Impact combobox."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_SaveProblem()
        {
            try
            {
                flag = prb.Save();
                if (!flag) { error = "Error when save problem."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SearchAndOpenProblem_ExpectFound()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (PrbId2 == null || PrbId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem 2 Id.");
                    addPara.ShowDialog();
                    PrbId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prblist.WaitLoading();
                    temp = prblist.List_Title().MyText;
                    flag = temp.Equals("Problems");
                    if (flag)
                    {
                        flag = prblist.SearchAndOpen("Number", PrbId2, "Number=" + PrbId2, "Number");
                        if (!flag) error = "Error when search and open problem (id:" + PrbId2 + ")";
                        else prb.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_ImpersonateUser_ProblemManager_1()
        {
            try
            {
                string rootUser = Base.GData("UserFullName");
                string temp = Base.GData("ProblemManager1");
                flag = home.ImpersonateUser(temp, true, rootUser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_SearchAndOpenProblem_ExpectNotFound()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (PrbId2 == null || PrbId2 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input problem 2 Id.");
                    addPara.ShowDialog();
                    PrbId2 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = home.LeftMenuItemSelect("Problem", "Open");
                if (flag)
                {
                    prblist.WaitLoading();
                    temp = prblist.List_Title().MyText;
                    flag = temp.Equals("Problems");
                    if (flag)
                    {
                        flag = prblist.SearchAndVerify("Number", PrbId2, "Number=" + PrbId2, true);
                        if (!flag)
                            error = "Found (id:" + PrbId2 + ")";
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Problems)";
                    }
                }
                else error = "Error when select open problem.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
