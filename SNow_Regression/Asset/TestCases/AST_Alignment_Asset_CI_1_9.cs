﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using Auto;

namespace Asset
{
    [TestFixture]
    public class AST_Alignment_Asset_CI_1_9
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Asset Tag: " + assetTag);
            System.Console.WriteLine("Finished - Serial Number: " + serialNumber);
            System.Console.WriteLine("Finished - Current Time: " + currentTime);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Auto.otextbox textbox;
        Auto.olookup lookup;
        Auto.ocombobox combobox;       
        Auto.obutton button;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Itil hrdAsset = null;
        SNow.ItilList astList = null;
        SNow.Itil netGear = null;
        SNow.ItilList netList = null;
        SNow.Itil computer = null;
        SNow.ItilList comList = null;
        SNow.Itil dialog = null;
        //------------------------------------------------------------------
        string assetTag, currentTime, serialNumber;
        string assigned = string.Empty;
        string installed = string.Empty;
        string ConfigItem = string.Empty;
        string sRetiredDate = string.Empty;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                hrdAsset = new SNow.Itil(Base, "Hardware Asset");
                astList = new SNow.ItilList(Base, "Asset list");
                netGear = new SNow.Itil(Base, "Network Gear");
                netList = new SNow.ItilList(Base, "Network Gear List");
                comList = new SNow.ItilList(Base, "Computer list");
                computer = new SNow.Itil(Base, "Computer");
                dialog = new SNow.Itil(Base, "dialog");
                //------------------------------------------------------------------
                assetTag = string.Empty;
                serialNumber = string.Empty;
                currentTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_Open_System()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Verify_Left_Menu_Items()
        {
            try
            {
                string[] p_leftitems = null;
                string[] leftitems = null;

                string temp = Base.GData("Nav_Menu");
                if (temp.Contains("|"))
                    p_leftitems = temp.Split('|');
                else
                    p_leftitems = new string[] { temp };

                foreach (string pitem in p_leftitems)
                {
                    bool iFlag = true;
                    if (pitem.Contains("::"))
                    {
                        leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
                        if (flag && !iFlag)
                            flag = false;
                    }
                    else
                    {
                        error = "incorrect format of given data [" + pitem + "]";
                        flag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Select_Hardware_Assets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Portfolio", "Hardware Assets");
                if (flag)
                {
                    astList.WaitLoading();
                }
                else { error = "Cannot find Hardware Assets link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Click_New_button()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else { error = "Cannot click New button."; }
                }
                else { error = "Cannot get button New."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_SelectCategory_And_Model()
        {
            try
            {
                lookup = hrdAsset.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("Category");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        lookup = hrdAsset.Lookup_Model();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Model");
                            flag = lookup.Select(temp);
                            if (!flag)
                            { error = "Cannot select Model value."; }
                        }
                        else { error = "Cannot found lookup Model."; }
                    }
                    else { error = "Cannot select Model Category value."; }
                }
                else { error = "Cannot get lookup Model Category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Verify_Manufacturer()
        {
            try
            {
                Thread.Sleep(3000);
                lookup = hrdAsset.Lookup_Manufacturer();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Manufacturer");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect value of Manufacturer."; flagExit = false; }
                }
                else { error = "Cannot get textbox Manufacturer."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_InputValidValues()
        {
            try
            {
                assetTag = "Auto_Hardware" + "-" + System.DateTime.Now.ToString("yyyyMMddHHmmss");

                //Asset Tag
                textbox = hrdAsset.Textbox_AssetTag();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(assetTag);
                    if (flag)
                    {
                        //Company
                        lookup = hrdAsset.Lookup_Company();
                        string temp = Base.GData("Company");
                        flag = lookup.Existed;
                        if (flag)
                        {

                            flag = lookup.Select(temp);
                            if (flag)
                            {
                                //Department
                                lookup = hrdAsset.Lookup_Department();
                                flag = lookup.Existed;
                                if (flag)
                                {
                                    temp = Base.GData("Department");
                                    flag = lookup.Select(temp);
                                    if (flag)
                                    {
                                        //Location
                                        lookup = hrdAsset.Lookup_Location();
                                        flag = lookup.Existed;
                                        if (flag)
                                        {
                                            temp = Base.GData("Location");
                                            flag = lookup.Select(temp);
                                            if (flag)
                                            {
                                                //Assigned to
                                                lookup = hrdAsset.Lookup_AssignedTo();
                                                flag = lookup.Existed;
                                                if (flag)
                                                {
                                                    temp = Base.GData("AssignedTo");
                                                    flag = lookup.Select(temp);
                                                    if (flag)
                                                    {
                                                        hrdAsset.WaitLoading();
                                                        //Managed By Company
                                                        lookup = hrdAsset.Lookup_ManagedByCompany();
                                                        flag = lookup.Existed;
                                                        if (flag)
                                                        {
                                                            temp = Base.GData("ManagedByCompany");
                                                            flag = lookup.Select(temp);
                                                            if (flag)
                                                            {
                                                                //Managed by
                                                                lookup = hrdAsset.Lookup_ManagedBy();
                                                                flag = lookup.Existed;
                                                                if (flag)
                                                                {
                                                                    temp = Base.GData("ManagedBy");
                                                                    flag = lookup.Select(temp);
                                                                    if (flag)
                                                                    {
                                                                        //Owned By Company
                                                                        lookup = hrdAsset.Lookup_OwnedByCompany();
                                                                        flag = lookup.Existed;
                                                                        if (flag)
                                                                        {
                                                                            temp = Base.GData("OwnedByCompany");
                                                                            flag = lookup.Select(temp);
                                                                            if (flag)
                                                                            {
                                                                                //Owned by
                                                                                lookup = hrdAsset.Lookup_OwnedBy();
                                                                                flag = lookup.Existed;
                                                                                if (flag)
                                                                                {
                                                                                    temp = Base.GData("OwnedBy");
                                                                                    flag = lookup.Select(temp);
                                                                                    if (!flag)
                                                                                    {
                                                                                        error = "Cannot populate Owned by value.";
                                                                                    }
                                                                                }
                                                                                else { error = "Cannot get lookup Owned by."; }
                                                                            }
                                                                            else { error = "Cannot populate Owned By Company value."; }
                                                                        }
                                                                        else { error = "Cannot get lookup Owned By Company."; }
                                                                    }
                                                                    else { error = "Cannot populate Managed by value."; }
                                                                }
                                                                else { error = "Cannot get lookup Managed by."; }
                                                            }
                                                            else { error = "Cannot populate Managed by Company value."; }
                                                        }
                                                        else { error = "Cannot get lookup Managed by Company."; }
                                                    }
                                                    else { error = "Cannot populate Assigned to value."; }
                                                }
                                                else { error = "Cannot get lookup Assigned to."; }
                                            }
                                            else { error = "Cannot populate Location value."; }
                                        }
                                        else { error = "Cannot get lookup Location."; }
                                    }
                                    else { error = "Cannot populate Deparment value."; }
                                }
                                else { error = "Cannot get lookup Department."; }
                            }
                            else { error = "Cannot populate Company value."; }
                        }
                        else { error = "Cannot get lookup Company."; }
                    }
                    else { error = "Cannot populate Asset Tag value."; }
                }
                else { error = "Cannot get textbox Asset Tag."; }

                //Support Group
                lookup = hrdAsset.Lookup_SupportGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("SupportGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Support Group value.";
                    }
                }
                else { error = "Cannot get lookup Support Group."; }

                //Serial Nummber 
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    serialNumber = "SN" + " - " + System.DateTime.Now.ToString("yyyyy-MM-dd HH:mm:ss");
                    flag = textbox.SetText(serialNumber);
                    if (!flag) { error = "Cannot populate Serial Number value."; }
                }
                else { error = "Cannot get textbox Serinal Number."; }

                //Alternate Asset Tag
                textbox = hrdAsset.Textbox_AlternateAssetTag();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText("Alternate " + assetTag);
                    if (!flag)
                    {
                        error = "cannot populate Alternate Asset Tag";
                    }
                }
                else { error = "Cannot get textbox Alternate Asset Tag."; }

                //Floor               
                textbox = hrdAsset.Textbox_Floor();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Floor");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "cannot populate Floor";
                    }
                }
                else { error = "Cannot get textbox Floor."; }

                //Room
                textbox = hrdAsset.Textbox_Room();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Room");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "cannot populate Room";
                    }
                }
                else { error = "Cannot get textbox Room."; }

                //Seat
                textbox = hrdAsset.Textbox_Seat();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = Base.GData("Seat");
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "cannot populate Seat";
                    }
                }
                else { error = "Cannot get textbox Seat."; }

                //Ownership
                combobox = hrdAsset.Combobox_Ownership();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = Base.GData("Ownership");
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Ownership";
                    }
                }
                else { error = "Cannot get combobox Ownership"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_SelectState_SubState_Submit()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = Base.GData("Init_State");
                    flag = combobox.SelectItem(temp);
                    hrdAsset.WaitLoading();
                    if (flag)
                    {
                        button = hrdAsset.Button_Submit();
                        if (button != null)
                        {
                            button.Click();
                            hrdAsset.WaitLoading();
                        }
                    }
                    else { error = "Cannot select State"; }
                }
                else { error = "Cannot get combobox State"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SearchHardwareAssets()
        {
            try
            {
                //Open dialog for input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (assetTag == null || assetTag == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input asset tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------
                string filterCondition = "Asset tag;contains;" + assetTag;
                flag = astList.Filter(filterCondition);
                if (flag)
                {
                    string searchCondition = "Asset tag=" + assetTag;
                    flag = astList.Open(searchCondition, "Acquisition method");
                    if (!flag)
                        error = "Cannot open.";
                }
                else { error = "Cannot filter Asset tag: " + assetTag + " "; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_01_Open_PrivateWindow()
        {
            try
            {
                textbox = hrdAsset.Textbox_Seat();
                if (textbox != null)
                {
                    textbox.MyElement.SendKeys(Keys.LeftControl + Keys.Shift + "P");
                    Thread.Sleep(5000);
                    Base.SwitchToPage(1);
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_02_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_03_LoginOnPrivate_FFInstance()
        {
            try
            {
                flag = login.LoginToSystem(Base.GData("User"), Base.GData("Pwd"));

                if (flag == true)
                {
                    login.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_04_ImpersonateUser_AssetManager()
        {
            try
            {

                flag = home.ImpersonateUser(Base.GData("Asset_Manager"));
                if (!flag)
                {
                    error = "Cannot impersonate Asset Manager";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_05_Open_ComputerCI_List()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Computer", "Computers");
                if (flag == true)
                {
                    comList.WaitLoading();
                }
                else
                { error = "Cannot open Computer list"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SearchBy_SerialNumber()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && serialNumber == "")
                {
                    AddParameter addPara = new AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                Base.SwitchToPage(1);
                //-------------------------------------------------------
                string conditions = "Serial number;is;" + serialNumber;
                flag = comList.Filter(conditions);
                if (flag)
                {
                    comList.WaitLoading();
                    string CI_Name = Base.GData("CIName");

                    flag = comList.SearchAndOpen("Name", CI_Name, "Name=" + CI_Name, "Name");
                    if (flag)
                    {
                        computer.WaitLoading();
                    }
                    else
                    {
                        error = "Unable to find the CI with provided Serial Number";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_VerifyInfo_ofComputerCI()
        {
            string expectedValue, currentValue;
            bool flagVerified = false;
            try

            {

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && assetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input asset Tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                Base.SwitchToPage(1);
                //Verify Class
                currentValue = computer.Combobox_Class().Text;
                expectedValue = Base.GData("Category");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = "Class;";
                    flag = false;
                    flagExit = false;
                }
                //Verify Model ID
                currentValue = computer.Lookup_ModelID().MyElement.GetAttribute("value");
                expectedValue = Base.GData("Model");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Model ID;";
                    flag = false;
                    flagExit = false;
                }
                //Verify Support Group
                currentValue = computer.Lookup_SupportGroup().MyElement.GetAttribute("value");
                expectedValue = Base.GData("SupportGroup");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Support Group;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Asset Tag
                currentValue = computer.Textbox_AssetTag().MyElement.GetAttribute("value");
                expectedValue = assetTag;
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Asset Tag;";
                    flag = false;
                    flagExit = false;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && serialNumber == "")
                {
                    AddParameter addPara = new AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                //Verify Serial Number
                currentValue = computer.Textbox_SerialNumber().MyElement.GetAttribute("value");
                expectedValue = serialNumber;
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Serial Number;";
                    flag = false;
                    flagExit = false;
                }
                //Verify Status
                currentValue = computer.Combobox_Status().Text;
                expectedValue = "Installed";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Status;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Company
                currentValue = computer.Lookup_Company().MyElement.GetAttribute("value");
                expectedValue = Base.GData("Company");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Company;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Manufacturer
                currentValue = computer.Lookup_Manufacturer().MyElement.GetAttribute("value");
                expectedValue = Base.GData("Manufacturer");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Manufacturer;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Name
                currentValue = computer.Textbox_Name().MyElement.GetAttribute("value");
                expectedValue = Base.GData("CIName");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Name;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Location
                currentValue = computer.Lookup_Location().MyElement.GetAttribute("value");
                expectedValue = Base.GData("Location");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Location;";
                    flag = false;
                    flagExit = false;
                }

                currentValue = computer.Lookup_Asset().MyElement.GetAttribute("value");
                expectedValue = assetTag + " - " + Base.GData("Model");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Model;";
                    flag = false;
                    flagExit = false;
                }

                currentValue = computer.Lookup_AssignedTo().MyElement.GetAttribute("value");
                expectedValue = Base.GData("AssignedTo");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "AssignedTo;";
                    flag = false;
                    flagExit = false;
                }

                currentValue = computer.Textbox_Floor().Text;
                expectedValue = Base.GData("Floor");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Floor;";
                    flag = false;
                    flagExit = false;
                }

                currentValue = computer.Textbox_Room().Text;
                expectedValue = Base.GData("Room");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Room;";
                    flag = false;
                    flagExit = false;
                }

                currentValue = computer.Textbox_Seat().Text;
                expectedValue = Base.GData("Seat");
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Seat;";
                    flag = false;
                    flagExit = false;
                }

                currentValue = computer.Combobox_Usedfor().Text;
                expectedValue = "Production";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Production;";
                    flag = false;
                    flagExit = false;
                }

                if (flag == false)
                    error = error + " value do not match";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_BackTo_CIList()
        {
            try
            {
                button = computer.Button_Back();
                if (button != null)
                {
                    button.Click();
                    comList.WaitLoading();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Add_Columns_ToCIList()
        {
            try
            {
                //----Check if Domain column is not in the table, then add it---------------------------------
                flag = Base.SwitchToPage(1);
                string columns = "Domain|Assigned|Department|Managed by|Serial number";
                flag = comList.Add_More_Columns(columns);
                if (!flag)
                {
                    error += "Cannot add the following columns: [" + columns + "]";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Validate_AddedColumns_Value()
        {
            try
            {
                Base.SwitchToPage(1);
                comList.WaitLoading();
                comList.Filter("Domain;is;" + Base.GData("Domain") + "|and|Department;is;" + Base.GData("Department") + "|and|Managed by;is;" + Base.GData("ManagedBy"));

                int iRow = comList.Table_List().RowCount;
                if (iRow < 0)
                {
                    flag = false;
                    error = "Unable to find the CI with provided Domain/Department/Assigned/ManagedBy";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_BackToAssetform_A_Update_AssetTag()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && assetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input asset Tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                Base.SwitchToPage(0);
                textbox = hrdAsset.Textbox_AssetTag();

                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(assetTag + "_Updated");
                    if (!flag)
                    {
                        error = "Cannot update Asset Tag field";
                    }
                }
                else error = "Cannot get textbox asset tag.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_BackToAssetform_A_Clear_AssignedTo()
        {
            try
            {
                //Clear Assigned To field
                lookup = hrdAsset.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Click();
                    if (flag)
                    {
                        flag = lookup.SetText("", true);
                        if (flag)
                        {
                            Thread.Sleep(3000);
                        }
                        else { error = "Cannot clear Assigned to field."; }
                    }
                    else { error = "Cannot click Assigned to field."; }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_03_BackToAssetform_A_Update_Company()
        {
            try
            {
                //Update Company
                lookup = hrdAsset.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("New_Company"));
                    if (!flag)
                        error = "Error when select company.";
                }
                else error = "Cannot get lookup company.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_04_BackToAssetform_A_Update_Location()
        {
            try
            {
                lookup = hrdAsset.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("New_Location"));
                    if (!flag)
                        error = "Error when select location.";
                }
                else error = "Cannot get lookup location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_05_BackToAssetform_A_Update_Department()
        {
            try
            {
                lookup = hrdAsset.Lookup_Department();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("New_Department"));
                    if (!flag)
                        error = "Error when select department.";
                }
                else error = "Cannot get lookup department.";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_06_BackToAssetform_A_Update_AssignedTo()
        {
            try
            {
                lookup = hrdAsset.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("New_AssignedTo"));
                    if (!flag)
                    {
                        error += "Cannot select Assigned to field";
                    }
                }
                else error = "Cannot get lookup assigned to.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_07_UpdateFields()
        {
            try
            {
                Base.SwitchToPage(0);
                //Open dialog to input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && assetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input asset tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------
                if (temp == "yes" && serialNumber == "")
                {
                    AddParameter addPara = new AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.SetText(serialNumber + "_Updated");
                if (flag)
                {
                    lookup = hrdAsset.Lookup_ManagedBy();
                    flag = lookup.Select(Base.GData("New_ManagedBy"));
                    if (!flag)
                    {
                        { error = "cannot set the Managed By field"; }
                    }
                }
                else
                { error += "Cannot update Serial Number field"; }



                if (flag)
                {
                    lookup = hrdAsset.Lookup_SupportGroup();
                    flag = lookup.Select(Base.GData("New_SupportGroup"));
                    if (!flag)
                    {
                        { error += "Cannot update Support Group field"; }
                    }

                    lookup = hrdAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(Base.GData("New_Model"));
                        if (!flag)
                        {
                            error = "Cannot update new Model";
                        }
                    }
                }
                if (flag)
                {
                    temp = "Travel";
                    combobox = hrdAsset.Combobox_Usedfor();
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot update new Used For";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Save_Asset()
        {
            try
            {
                Base.SwitchToPage(0);
                flag = hrdAsset.Save();
                if (!flag)
                {
                    error = "Cannot save asset";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Return_to_ComputerCI_Form()
        {
            try
            {
                Base.SwitchToPage(1);

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && assetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input Serial Number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                string conditions = "Serial number;is;" + serialNumber + "_Updated";
                flag = comList.Filter(conditions);

                string CI_Name = Base.GData("CIName");
                flag = comList.SearchAndOpen("Name", CI_Name, "Name=" + CI_Name, "Name");
                if (flag)
                {
                    computer.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Unable to find CI";
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_023_VerifyInfo_ofComputerCI()
        {
            string expectedValue, currentValue;
            bool flagVerified = false;
            try
            {

                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && assetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input asset Tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                Base.SwitchToPage(1);

                //Verify Model ID
                currentValue = computer.Lookup_ModelID().MyElement.GetAttribute("value");
                expectedValue = Base.GData("New_Model");

                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Model ID;";
                    flag = false;
                    flagExit = false;
                }
                //Verify Support Group
                currentValue = computer.Lookup_SupportGroup().MyElement.GetAttribute("value");
                expectedValue = Base.GData("New_SupportGroup");

                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Support Group;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Asset Tag
                currentValue = computer.Textbox_AssetTag().MyElement.GetAttribute("value");
                expectedValue = assetTag + "_Updated";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Asset Tag;";
                    flag = false;
                    flagExit = false;
                }
                //-----------------------------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && serialNumber == "")
                {
                    AddParameter addPara = new AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-------------------------------------------------------------------------
                //Verify Serial Number
                currentValue = computer.Textbox_SerialNumber().MyElement.GetAttribute("value");
                expectedValue = serialNumber + "_Updated";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Serial Number;";
                    flag = false;
                    flagExit = false;
                }
                //Verify Status
                currentValue = computer.Combobox_Status().Text;
                expectedValue = "Installed";

                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Status;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Company
                currentValue = computer.Lookup_Company().MyElement.GetAttribute("value");
                expectedValue = Base.GData("New_Company");

                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Company;";
                    flag = false;
                    flagExit = false;
                }


                //Verify Location
                currentValue = computer.Lookup_Location().MyElement.GetAttribute("value");
                expectedValue = Base.GData("New_Location");

                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Location;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Assigned to
                currentValue = computer.Lookup_AssignedTo().MyElement.GetAttribute("value");
                expectedValue = Base.GData("New_AssignedTo");

                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Assigned To;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Room
                currentValue = computer.Textbox_Room().Text;
                expectedValue = "";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Room;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Floor
                currentValue = computer.Textbox_Floor().Text;
                expectedValue = "";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Floor;";
                    flag = false;
                    flagExit = false;
                }
                if (flag == false)
                    error = error + " value do not match";

                //Verify Seat
                currentValue = computer.Textbox_Seat().Text;
                expectedValue = "";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Seat;";
                    flag = false;
                    flagExit = false;
                }

                //Verify Used for
                currentValue = computer.Combobox_Usedfor().Text;
                expectedValue = "Travel";
                flagVerified = currentValue.Equals(expectedValue);
                if (flagVerified == false)
                {
                    error = error + "Used For;";
                    flag = false;
                    flagExit = false;
                }
            }


            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_BackTo_CIList()
        {
            try
            {
                Base.SwitchToPage(1);
                button = computer.Button_Back();
                if (button != null)
                {
                    button.Click();
                    comList.WaitLoading();
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Validate_Columns_Value()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && serialNumber == "")
                {
                    AddParameter addPara = new AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                Base.SwitchToPage(1);
                //-------------------------------------------------------
                string new_serial = serialNumber + "_Updated";
                temp = "Serial number;is;" + new_serial;
                comList.Filter(temp);
                comList.WaitLoading();
                Base.SwitchToPage(1);

                flag = comList.SearchAndVerify("Department", Base.GData("New_Department"), "Department =" + Base.GData("New_Department") + "|Managed by=" + Base.GData("New_ManagedBy"));
                if (!flag)
                {
                    error = "Unable to find the CI with provided Department/Assigned/ManagedBy";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_BackToAssetform_A_DeleteAsset_Cancel()
        {
            try
            {
                Base.SwitchToPage(0);
                button = hrdAsset.Button_Delete();
                if (button != null)
                {
                    button.Click();
                    dialog.WaitLoading();
                    string temp = Base.GData("Confirmation");
                    flag = dialog.VerifyConfirmDialog(temp,"no");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_BackToAssetform_A_DeleteAsset_OK()
        {
            try
            {
                Base.SwitchToPage(0);
                button = hrdAsset.Button_Delete();
                if (button != null)
                {
                    button.Click();
                    dialog.WaitLoading();
                    string temp = Base.GData("Confirmation");
                    flag = dialog.VerifyConfirmDialog(temp, "yes");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_BackTo_CIList_A_Search()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && serialNumber == "")
                {
                    AddParameter addPara = new AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                Base.SwitchToPage(1);
                //-------------------------------------------------------
                string new_serial = serialNumber + "_Updated";
                temp = "Serial number;is;" + new_serial;
                comList.Filter(temp);
                comList.WaitLoading();

                int iRow = comList.Table_List().RowCount;
                if (iRow > 0)
                {
                    flag = false;
                    error = "The CI is not deleted.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_ImpersonateUser_ServiceDesk()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("ServiceDesk"));
                if (!flag)
                {
                    error = "Cannot impersonate Service Desk";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Verify_SDA_Cannot_Access_AssetMngModules()
        {
            try
            {
                string MenuList = "Overview;All Assets;Hardware Assets;Consumables;Other Assets;Create Transform Order";
                textbox = home.Textbox_Filter();
                if (textbox != null)
                {
                    textbox.SetText("Asset Management");
                    flag = home.LeftMenuItemValidate("Asset Management", MenuList);
                    if (flag)
                    {
                        flag = false;
                        error = "Service Desk is able to access the Asset Management module";
                    }
                    else
                    {
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_030_Logout_SDA_Window2()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------     
        [Test]
        public void Step_031_Logout_AssetMgr_Window1()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
