﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using Auto;

namespace Asset
{
    [TestFixture]
    public class AST_validate_duplication_15
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("[Pre-condition Hardware Asset] AssetTag :" + PreconHWAssetTag + "/ Serial Number:" + PreconHWSerialNumber);
            System.Console.WriteLine("[Pre-condition Other Asset] AssetTag :" + PreconOtherAssetTag + "/ Serial Number:" + PreconOtherSerialNumber);
            System.Console.WriteLine("[Hardware Asset] AssetTag :" + HWAssetTag + "/ Serial Number:" + HWSerialNumber);
            System.Console.WriteLine("[Other Asset] AssetTag :" + OtherAssetTag + "/ Serial Number:" + OtherSerialNumber);

            System.Console.WriteLine("Finished - Error Other Asset Tag: " + errOtherAssetTag);
            System.Console.WriteLine("Finished - Error Other Serial Number: " + errOtherSerialNumber);
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        Auto.otextbox textbox;
        Auto.olookup lookup;
        Auto.ocombobox combobox;       
        Auto.obutton button;
        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Itil hrdAsset = null;
        SNow.ItilList astList = null;
        SNow.Itil otherAsset = null;
        //------------------------------------------------------------------
        string PreconHWSerialNumber = "";
        string PreconHWAssetTag = "";
        string PreconOtherSerialNumber = "";
        string PreconOtherAssetTag = "";
        string HWSerialNumber = "";
        string HWAssetTag = "";
        string OtherSerialNumber = "";
        string OtherAssetTag = "";
        string errHWAssetTag = "";
        string errHWSerialNumber = "";
        string errOtherAssetTag = "";
        string errOtherSerialNumber = "";
        string hwModel = "";
        

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                hrdAsset = new SNow.Itil(Base, "Hardware Asset");
                astList = new SNow.ItilList(Base, "Asset list");
                otherAsset = new SNow.Itil(Base, "Other Asset");
                //------------------------------------------------------------------
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Pre-condition - Create a Hardware Asset

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_003_Impersonate_AssetManager()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("AssetManager"));
                if (!flag)
                {
                    error = "Cannot impersonate Asset Manager";
                }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void PreStep_004_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_005_Open_HardwareAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Hardware Assets");
                if (flag)
                {
                    astList.WaitLoading();
                }
                else { error = "Cannot find Hardware Assets link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_006_Click_NewButton()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else { error = "Cannot click New button."; }
                }
                else { error = "Cannot get button New."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_007_Input_ValidInfomation()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                PreconHWAssetTag = "TAG-" + temp;
                PreconHWSerialNumber = "SN-" + temp;

                #region Select Model Category
                lookup = hrdAsset.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("HWModelCategory");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot select Model Catagory";
                    }
                }
                else
                {
                    error = "Cannot find Model Category field";
                }
                #endregion
                #region Select Model
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("HWModel");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model field";
                    }
                }
                #endregion
                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion
                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion
                #region Select Company
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Company");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Company";
                        }
                    }
                    else
                    {
                        error = "Cannot find Company field";
                    }
                }
                #endregion
                #region Select Location
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Location");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Location";
                        }
                    }
                    else
                    {
                        error = "Cannot find Location field";
                    }
                }
                #endregion
                #region Select State
                if (flag)
                {
                    combobox = hrdAsset.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("HWState");
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select State value";
                        }
                    }
                    else
                    {
                        error = "Cannot get State field";
                    }
                }
                #endregion
                #region Select Ownership
                if (flag)
                {
                    combobox = hrdAsset.Combobox_Ownership();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Ownership");
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select Ownership value";
                        }
                    }
                    else
                    {
                        error = "Cannot get Ownership field";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_008_Save_Hardware()
        {
            try
            {
                // Try to remove the Manufacturer out of Model value
                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();

                //errHWAssetTag = "Asset tag " + PreconHWAssetTag + " already exists for Company: " + Base.GData("Company");
                //errHWSerialNumber = "Serial Number " + PreconHWSerialNumber + " already exists for Model: " + hwModel;
                //errHWAssetTag = errHWAssetTag.Trim();
                //errHWSerialNumber = errHWSerialNumber.Trim();

                //System.Console.WriteLine("Error Message for Hardware Asset Tag" + errHWAssetTag);
                //System.Console.WriteLine("Error Message for Hardware Serial Number" + errHWSerialNumber);

                flag = hrdAsset.Save();
                if (!flag)
                {
                    error = "Cannot save Hardware";
                }
                else hrdAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_009_Open_OtherAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Portfolio", "Other Assets");
                if (flag)
                {
                    astList.WaitLoading();
                    //flag = astList.Label_Title.Text.Equals("Assets");
                }
                else
                {
                    error = "Cannot find Other Assets list";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_010_Click_NewButton()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    otherAsset.WaitLoading();

                    /* Choose a type of Asset to create */
                    temp = Base.GData("OtherAssetType");
                    flag = otherAsset.Select_OtherAssets_Type(temp);
                    if (!flag)
                    {
                        error = "Cannot select Asset Other type";
                    }
                }
                else
                {
                    error = "Cannot get the New button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_011_Input_ValidInfomation()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                PreconOtherAssetTag = "TAG-" + temp;
                PreconOtherSerialNumber = "SN-" + temp;

                #region Select Model Category
                temp = Base.GData("OtherAssetType");
                if (!temp.Trim().ToLower().Contains("license"))
                {
                    lookup = otherAsset.Lookup_ModelCategory();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("OtherAssetsModelCategory");
                        flag = lookup.SetText(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model Catagory";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model Category field";
                    }
                }
                #endregion

                #region Select Model
                if (flag)
                {
                    lookup = otherAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("OtherAssetsModel");
                        flag = lookup.SetText(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model field";
                    }
                }
                #endregion

                #region Input Serial Number
                if (flag)
                {
                    textbox = otherAsset.Textbox_SerialNumber();

                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconOtherSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = otherAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconOtherAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Company
                if (flag)
                {
                    lookup = otherAsset.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Company");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Company";
                        }
                    }
                    else
                    {
                        error = "Cannot find Company field";
                    }
                }
                #endregion

                #region Select Location
                if (flag)
                {
                    lookup = otherAsset.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Location");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Location";
                        }
                    }
                    else
                    {
                        error = "Cannot find Location field";
                    }
                }
                #endregion

                #region Select State
                if (flag)
                {
                    combobox = otherAsset.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("OtherState");
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select State value";
                        }
                    }
                    else
                    {
                        error = "Cannot get State field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_012_Save_OtherAsset()
        {
            try
            {

                // Try to remove the Manufacturer out of Model value
                //string manufacturer = otherAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("OtherModel");
                //int index = model.IndexOf(manufacturer);
                //otherModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //otherModel = otherModel.Trim();

                //errOtherAssetTag = "Asset tag " + PreconOtherAssetTag + " already exists for Company: " + Base.GData("Company");
                //errOtherSerialNumber = "Serial Number " + PreconOtherSerialNumber + " already exists for Model: " + otherModel;
                //errOtherAssetTag = errOtherAssetTag.Trim();
                //errOtherSerialNumber = errOtherSerialNumber.Trim();

                //System.Console.WriteLine("Error Message for Other Asset Tag" + errOtherAssetTag);
                //System.Console.WriteLine("Error Message for Other Serial Number" + errOtherSerialNumber);
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Cannot save Hardware";
                }
                else otherAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion


        #region Validate duplication Hardware Assets when create new

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_Verify_AssetManagementModule()
        {
            try
            {
                string[] p_leftitems = null;
                string[] leftitems = null;

                string temp = Base.GData("Nav_Menu");
                if (temp.Contains("|"))
                    p_leftitems = temp.Split('|');
                else
                    p_leftitems = new string[] { temp };

                foreach (string pitem in p_leftitems)
                {
                    bool iFlag = true;
                    if (pitem.Contains("::"))
                    {
                        leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
                        if (flag && !iFlag)
                            flag = false;
                    }
                    else
                    {
                        error = "incorrect format of given data [" + pitem + "]";
                        flag = false;
                    }
                }
                //string NavMenu = "Asset Management";
                //textbox = home.Textbox_Filter();

                //flag = textbox.SetText(NavMenu);
                //if (flag)
                //{
                //    string expectedMenu = Base.GData("Nav_Menu");
                //    flag = home.LeftMenuItemValidate(NavMenu, expectedMenu);
                //    if (flag)
                //    {
                //        System.Console.WriteLine("Menu items match");
                //    }
                //    else
                //    {
                //        error = "Menu items does not match";
                //        System.Console.WriteLine("Menu items do not match.Expected Menu is " + expectedMenu);
                //    }
                //}
                //else
                //    error = "cannot filter the left menu";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            } 
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_Open_HardwareAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Hardware Assets");
                if (flag)
                {
                    astList.WaitLoading();
                }
                else { error = "Cannot find Hardware Assets link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_Click_NewButton()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the New button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_Input_Duplicate_All_Precondition_Information()
        {
            try
            {

                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                #region Select Model Category
                lookup = hrdAsset.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("HWModelCategory");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot select Model Catagory";
                    }
                }
                else
                {
                    error = "Cannot find Model Category field";
                }
                #endregion

                #region Select Model
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("HWModel");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model field";
                    }
                }
                #endregion

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Company
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Company");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Company";
                        }
                    }
                    else
                    {
                        error = "Cannot find Company field";
                    }
                }
                #endregion

                #region Select Location
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Location");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Location";
                        }
                    }
                    else
                    {
                        error = "Cannot find Location field";
                    }
                }
                #endregion

                #region Select State
                if (flag)
                {
                    combobox = hrdAsset.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("HWState");
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select State value";
                        }
                    }
                    else
                    {
                        error = "Cannot get State field";
                    }
                }
                #endregion

                #region Select Ownership
                if (flag)
                {
                    combobox = hrdAsset.Combobox_Ownership();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Ownership");
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select Ownership value";
                        }
                    }
                    else
                    {
                        error = "Cannot get Ownership field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_01_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_02_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconHWAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                
                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();

                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");

                errHWSerialNumber = "Serial Number " + PreconHWSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_011_Close_ErrorMessage()
        {
            try
            {
                button = hrdAsset.Button_Close_Error();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (!flag)
                    {
                        error = "Cannot Close Error Massage";
                    }

                }
                else { error = "Cannot get the Close Error button"; }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_01_Input_PreTag_PreCompany_PreModel_DifferentSerialNumber()
        {
            try
            {

                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWSerialNumber = "SN-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWSerialNumber);
                        if (!flag)
                        {
                            hrdAsset.WaitLoading();
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Company
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Company");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Company";
                        }
                    }
                    else
                    {
                        error = "Cannot find Company field";
                    }
                }
                #endregion

                #region Select Location
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Location");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Location";
                        }
                    }
                    else
                    {
                        error = "Cannot find Location field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_02_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_03_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconHWAssetTag + " already exists for Company: " + Base.GData("Company");

                // Check if the Asset Tag error is displayed
                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);
                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_013_Close_ErrorMessage()
        {
            try
            {
                ele = hwAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    hwAsset.ElementClickJava(ele, Base.Driver);
                    hwAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_01_Input_PreSerialNumber_PreTag_PreCompany_DifferentModel()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                
                //------------------------------------------------------------------------------------------
                                
                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            hrdAsset.WaitLoading();
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Another Model
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("AnotherHWModel");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_02_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_03_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconHWAssetTag + " already exists for Company: " + Base.GData("Company");

                // Check if the Asset Tag error is displayed
                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);
                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_Input_PreSerialNumber_PreCompany_DifferentModel_DifferentTag()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWAssetTag = "TAG-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_02_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + PreconHWSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_015_Close_ErrorMessage()
        {
            try
            {
                ele = hwAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    hwAsset.ElementClickJava(ele, Base.Driver);
                    hwAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_04_Input_PreSerialNumber_PreModel_DifferentCompany_DifferentTag()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWAssetTag = "TAG-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Another Model
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("HWModel");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model field";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_05_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_06_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + PreconHWSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_01_Input_PreSerialNumber_PreTag_PreModel_DifferentCompany()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWAssetTag = "TAG-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Company
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Company2");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Company2";
                        }
                    }
                    else
                    {
                        error = "Cannot find Company field";
                    }
                }
                #endregion

                #region Select Location
                if (flag)
                {
                    lookup = hrdAsset.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Location2");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            hrdAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Location2";
                        }
                    }
                    else
                    {
                        error = "Cannot find Location field";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_02_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_015_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + PreconHWSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_01_Input_ValidInfomation()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWAssetTag = "TAG-" + temp;
                HWSerialNumber = "SN-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_02_Submit_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Validate duplication Asset for existing Hardware Assets

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_SearchAndOpen_HardwareAssets()
        {
            try
            {
                //--------------------------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && HWAssetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input asset tag");
                    addPara.ShowDialog();
                    HWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------

                string condition = "Asset tag;is;" + HWAssetTag;
                flag = astList.Filter(condition);
                if (flag)
                {
                    astList.WaitLoading();

                    //check AssetTag 
                    flag = astList.SearchAndOpen("Asset tag", HWAssetTag, "Asset tag=" + HWAssetTag, "Acquisition method");
                    if (!flag)
                    {
                        error = "Cannot click on Asset Tag";
                    }
                    else hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot input asset tag into search field";
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_01_RightClick_Insert()
        {
            try
            {
                button = hrdAsset.Button_AdditionalActions();
                if(button.Existed)
                {
                    button.Click();                    
                }
                else
                {
                    flag = false;
                    error = "Could not find additional button";
                }

                flag = hrdAsset.Select_ContextMenu_Items("Insert");
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot select Insert on the Context Menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_02_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && HWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input HWAssetTag.");
                    addPara.ShowDialog();
                    HWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + HWAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && HWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input HWSerialNumber.");
                    addPara.ShowDialog();
                    HWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + HWSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_019_Close_ErrorMessage()
        {
            try
            {
                ele = hwAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    hwAsset.ElementClickJava(ele, Base.Driver);
                    hwAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_01_Input_Dupplicate_AssetTag()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWSerialNumber = "SN-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWSerialNumber);
                        hrdAsset.WaitLoading();
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWAssetTag);
                        hrdAsset.WaitLoading();
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_02_Update_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_03_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWAssetTag.");
                    addPara.ShowDialog();
                    PreconHWAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconHWAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_021_Close_ErrorMessage()
        {
            try
            {
                ele = hwAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    hwAsset.ElementClickJava(ele, Base.Driver);
                    hwAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }*/

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_01_Input_Dupplicate_SerialNumber()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWAssetTag = "TAG-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconHWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_02_Update_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_022_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconHWSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconHWSerialNumber.");
                    addPara.ShowDialog();
                    PreconHWSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + PreconHWSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_023_Close_ErrorMessage()
        {
            try
            {
                ele = hwAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    hwAsset.ElementClickJava(ele, Base.Driver);
                    hwAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_01_Input_ValidInfomation()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                HWAssetTag = "TAG-" + temp;
                HWSerialNumber = "SN-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = hrdAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = hrdAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(HWAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_02_Update_HardwareAsset()
        {
            try
            {
                button = hrdAsset.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    hrdAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Validate Hardware Assets Insert and Stay function for duplication

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_029_RedundantTestStep()
        {
            try
            {
                System.Console.WriteLine("These Test Steps are the same as Step 18 -> Step 24");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Validate duplication Other Assets when create new

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_Verify_AssetManagementModule()
        {
            try
            {
                string[] p_leftitems = null;
                string[] leftitems = null;

                string temp = Base.GData("Nav_Menu");
                if (temp.Contains("|"))
                    p_leftitems = temp.Split('|');
                else
                    p_leftitems = new string[] { temp };

                foreach (string pitem in p_leftitems)
                {
                    bool iFlag = true;
                    if (pitem.Contains("::"))
                    {
                        leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
                        if (flag && !iFlag)
                            flag = false;
                    }
                    else
                    {
                        error = "incorrect format of given data [" + pitem + "]";
                        flag = false;
                    }
                }
                //string NavMenu = "Asset Management";
                //textbox = home.Textbox_Filter();

                //flag = textbox.SetText(NavMenu);
                //if (flag)
                //{
                //    string expectedMenu = Base.GData("Nav_Menu");
                //    flag = home.LeftMenuItemValidate(NavMenu, expectedMenu);
                //    if (flag)
                //    {
                //        System.Console.WriteLine("Menu items match");
                //    }
                //    else
                //    {
                //        error = "Menu items does not match";
                //        System.Console.WriteLine("Menu items do not match.Expected Menu is " + expectedMenu);
                //    }
                //}
                //else
                //    error = "cannot filter the left menu";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_Open_OtherAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Portfolio", "Other Assets");
                if (flag)
                {
                    astList.WaitLoading();
                    //flag = astList.Label_Title.Text.Equals("Assets");
                }
                else
                {
                    error = "Cannot find Other Assets list";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_Click_NewButton()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    astList.WaitLoading();

                    /* Choose a type of Asset to create */
                    temp = Base.GData("OtherAssetType");
                    flag = otherAsset.Select_OtherAssets_Type(temp);
                    if (!flag)
                    {
                        error = "Cannot select Asset Other type";
                    }
                }
                else
                {
                    error = "Cannot get the New button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_Input_Dupplicate_AssetTag_SerialNumber()
        {
            try
            {

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherSerialNumber.");
                    addPara.ShowDialog();
                    PreconOtherSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                #region Select Model Category
                if (!temp.Trim().ToLower().Contains("license"))
                {
                    lookup = otherAsset.Lookup_ModelCategory();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("OtherAssetsModelCategory");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model Catagory";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model Category field";
                    }
                }
                #endregion

                #region Select Model
                if (flag)
                {
                    lookup = otherAsset.Lookup_Model();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("OtherAssetsModel");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Model";
                        }
                    }
                    else
                    {
                        error = "Cannot find Model field";
                    }
                }
                #endregion

                #region Input Serial Number
                if (flag)
                {
                    textbox = otherAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconOtherSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = otherAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconOtherAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

                #region Select Company
                if (flag)
                {
                    lookup = otherAsset.Lookup_Company();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Company");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Company";
                        }
                    }
                    else
                    {
                        error = "Cannot find Company field";
                    }
                }
                #endregion

                #region Select Location
                if (flag)
                {
                    lookup = otherAsset.Lookup_Location();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        temp = Base.GData("Location");
                        flag = lookup.Select(temp);
                        if (flag)
                        {
                            otherAsset.WaitLoading();
                        }
                        else
                        {
                            error = "Cannot select Location";
                        }
                    }
                    else
                    {
                        error = "Cannot find Location field";
                    }
                }
                #endregion

                #region Select State
                if (flag)
                {
                    combobox = otherAsset.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = Base.GData("OtherState");
                        flag = combobox.SelectItem(temp);
                        if (!flag)
                        {
                            error = "Cannot select State value";
                        }
                    }
                    else
                    {
                        error = "Cannot get State field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_01_Submit_OtherAsset()
        {
            try
            {
                button = otherAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_02_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconOtherAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherSerialNumber.");
                    addPara.ShowDialog();
                    PreconOtherSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + PreconOtherSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_035_Close_ErrorMessage()
        {
            try
            {
                ele = otherAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    otherAsset.ElementClickJava(ele, Base.Driver);
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }*/

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_01_Input_Duppicate_AssetTag()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                OtherSerialNumber = "SN-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = otherAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(OtherSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = otherAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconOtherAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_02_Submit_OtherAsset()
        {
            try
            {
                button = otherAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Submit button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_037_Close_ErrorMessage()
        {
            try
            {
                ele = otherAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    otherAsset.ElementClickJava(ele, Base.Driver);
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_03_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconOtherAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_01_Input_ValidInfomation()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                OtherAssetTag = "TAG-" + temp;
                OtherSerialNumber = "SN-" + temp;
                #region Input Asset Tag
                if (flag)
                {
                    textbox = otherAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(OtherAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion
                #region Input Serial Number
                if (flag)
                {
                    textbox = otherAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(OtherSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_02_Submit_OtherAsset()
        {
            try
            {
                button = otherAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Validate duplication Asset for existing Other Assets

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_Search_OtherAssets()
        {
            try
            {
                //--------------------------------------------------------------------
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && OtherAssetTag == "")
                {
                    AddParameter addPara = new AddParameter("Please input Other Asset Tag");
                    addPara.ShowDialog();
                    OtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //--------------------------------------------------------------------

                flag = astList.Filter("Asset tag;is;" + OtherAssetTag);
                
                if (flag)
                {
                    astList.WaitLoading();

                    //check AssetTag 
                    flag = astList.SearchAndOpen("Asset tag", OtherAssetTag, "Asset tag=" + OtherAssetTag, "Asset tag");
                    if (!flag)
                    {
                        error = "Cannot click on Asset Tag";
                    }
                    else otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot input asset tag into search field";
                }
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_01_RightClick_Insert()
        {
            try
            {
                button = otherAsset.Button_AdditionalActions();
                if(button.Existed)
                {
                    button.Click();
                    otherAsset.WaitLoading();
                }
                else
                {
                    flag = false;
                    error = "Could not find Additional button";
                }
                flag = otherAsset.Select_ContextMenu_Items("Insert");
                if (flag)
                {
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot select Insert on the Context Menu";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_02_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconOtherAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_03_Verify_Serial_Number_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherSerialNumber == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherSerialNumber.");
                    addPara.ShowDialog();
                    PreconOtherSerialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //string manufacturer = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                //string model = Base.GData("HWModel");
                //int index = model.IndexOf(manufacturer);
                //hwModel = (index < 0) ? model : model.Remove(index, manufacturer.Length);
                //hwModel = hwModel.Trim();
                hwModel = hrdAsset.Lookup_Manufacturer().MyElement.GetAttribute("value");
                errHWSerialNumber = "Serial Number " + PreconOtherSerialNumber + " already exists for Model: " + hwModel;

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWSerialNumber);

                if (!flag)
                {
                    error += "There is no Serial Number error message";
                }

                flag = hrdAsset.Textbox_SerialNumber().MyElement.GetAttribute("value").Equals("");
                if (!flag)
                {
                    error += "Serial Number is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_041_Close_ErrorMessage()
        {
            try
            {
                ele = otherAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    otherAsset.ElementClickJava(ele, Base.Driver);
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }*/

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_01_Input_Dupplicate_AssetTag()
        {
            try
            {
                //------------------------------------------------------------------------------------------

                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                //------------------------------------------------------------------------------------------

                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                OtherSerialNumber = "SN-" + temp;

                #region Input Serial Number
                if (flag)
                {
                    textbox = otherAsset.Textbox_SerialNumber();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(OtherSerialNumber);
                        if (!flag)
                        {
                            error = "Cannot input Serial Number";
                        }
                    }
                    else
                    {
                        error = "Cannot find Serial Number field";
                    }
                }
                #endregion

                #region Input Asset Tag
                if (flag)
                {
                    textbox = otherAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(PreconOtherAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_02_Update_OtherAsset()
        {
            try
            {
                button = otherAsset.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_03_Verify_Asset_Tag_Error_Message()
        {
            try
            {
                temp = Base.GData("Debug").ToLower();

                if (temp == "yes" && PreconOtherAssetTag == string.Empty)
                {
                    AddParameter addPara = new AddParameter("Please input PreconOtherAssetTag.");
                    addPara.ShowDialog();
                    PreconOtherAssetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }

                errHWAssetTag = "Asset tag " + PreconOtherAssetTag + " already exists for Company: " + Base.GData("Company");

                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(errHWAssetTag);

                if (!flag)
                {
                    error += "There is no Asset Tag error message";
                }

                flag = hrdAsset.Textbox_AssetTag().MyElement.GetAttribute("value").Equals("");

                if (!flag)
                {
                    error += "Asset Tag is not empty";
                }

                if (error != "") { flag = false; flagExit = false; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        /*
        [Test]
        public void Step_043_Close_ErrorMessage()
        {
            try
            {
                ele = otherAsset.GButton_CloseMessage();
                flag = (ele != null);
                if (flag)
                {
                    otherAsset.ElementClickJava(ele, Base.Driver);
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Close Error button";
                }
            }
            catch (WebDriverException wex)
            {
                flag = false;
                error = wex.Message;
            }
        }
        */
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_01_Input_ValidInfomation()
        {
            try
            {
                temp = System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
                OtherAssetTag = "TAG-" + temp;

                #region Input Asset Tag
                if (flag)
                {
                    textbox = otherAsset.Textbox_AssetTag();
                    flag = textbox.Existed;
                    if (flag)
                    {
                        flag = textbox.SetText(OtherAssetTag);
                        if (flag)
                        {
                            error = "Cannot input Asset Tag";
                        }
                    }
                    else
                    {
                        error = "Cannot find Asset Tag field";
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_02_Submit_OtherAsset()
        {
            try
            {
                button = otherAsset.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    button.Click();
                    otherAsset.WaitLoading();
                }
                else
                {
                    error = "Cannot get the Update button";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Validate Other Asset Insert and Stay function for duplication

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_049_RedundantTestStep()
        {
            try
            {
                System.Console.WriteLine("These Test Steps are the same as Step 40 -> Step 44");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_End_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************

    }
}
