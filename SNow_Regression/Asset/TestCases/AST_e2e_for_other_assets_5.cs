﻿using NUnit.Framework;
using Auto;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;

namespace Asset
{
    [TestFixture]
    public class AST_e2e_for_other_assets_5
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName,  error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Consumable 1's Category: " + Base.GData("Category"));
            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        olookup lookup;
        otextbox textbox;
        ocombobox combobox;
        odate date;
        //------------------------------------------------------------------

        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Itil otherAsset = null;
        SNow.ItilList assList = null;  
        //StockroomList stList = null;
        //Stockroom stockroom = null;

        //------------------------------------------------------------------

        string assetTag, serialNumber;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                otherAsset = new SNow.Itil(Base, "Other Assets");
                assList = new SNow.ItilList(Base, "Asset list");
                //stList = new StockroomList(Base, "Stockroom List");
                //stockroom = new Stockroom(Base, "Stockroom");
                //------------------------------------------------------------------
                assetTag = string.Empty;
                serialNumber = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }        
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_Open_System()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_Impersonate_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Verify_VerifyAssetMngModules()
        {
            try
            {
                string[] p_leftitems = null;
                string[] leftitems = null;

                string temp = Base.GData("Nav_Menu");
                if (temp.Contains("|"))
                    p_leftitems = temp.Split('|');
                else
                    p_leftitems = new string[] { temp };

                foreach (string pitem in p_leftitems)
                {
                    bool iFlag = true;
                    if (pitem.Contains("::"))
                    {
                        leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
                        if (flag && !iFlag)
                            flag = false;
                    }
                    else
                    {
                        error = "incorrect format of given data [" + pitem + "]";
                        flag = false;
                    }
                }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Open_OtherAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Other Assets");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.Equals("Assets");
                    if (!flag)
                    {
                        error = "The displayed page is Other Assets page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Other Assets link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Click_NewButton()
        {
            try
            {
                flag = assList.Button_New().Click();
                if(flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot click on New button";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Select_Hardware()
        {
            try
            {
                flag = otherAsset.Select_OtherAssets_Type("Hardware");
                if (flag)
                {
                    otherAsset.WaitLoading();
                }
                else { error = "Cannot select Asset Type. Expected: Hardware"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------      
        [Test]
        public void Step_009_Select_ModelCategory()
        {
            try
            {
                string temp = Base.GData("Category");

                lookup = otherAsset.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                    else { Thread.Sleep(1000); }
                }
                else
                {
                    error = "Cannot get Model Category lookup.";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Select_Model()
        {
            try
            {
                lookup = otherAsset.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Model");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        lookup = otherAsset.Lookup_Manufacturer();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            if (lookup.Text.Equals(""))
                            {
                                flag = false;
                                flagExit = false;
                                error = "Manufacturer was EMPTY.";
                            }
                            else { flag = true; }
                        }
                        else { error = "Cannot get Manufacturer textbox."; }
                    }
                    else { error = "Cannot select item [" + temp + "]"; }
                }
                else { error = "Cannot get Model lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }

        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Populate_Company()
        {
            try
            {
                // Input Company
                string temp = Base.GData("Company");
                lookup = otherAsset.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get lookup Company."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_Populate_AssetTag()
        {
            try
            {
                string temp = DateTime.Now.ToString("yyyymmddhhmmss");
                textbox = otherAsset.Textbox_AssetTag();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get lookup Company."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Populate_SerialNumber()
        {
            try
            {
                string temp = DateTime.Now.ToString("yyyymmddhhmmss");
                textbox = otherAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get lookup Company."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Populate_Ownership()
        {
            try
            {
                // Input Ownership
                string temp = Base.GData("Ownership");
                combobox = otherAsset.Combobox_Ownership();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get Ownership combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Populate_State_InStock()
        {
            try
            {
                string temp = Base.GData("InStock_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Populate_StockRoom()
        {
            try
            {
                lookup = otherAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("StockRoom");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select stockroom [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Stockroom lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }       
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_00_Verify_StockroomLocation_Generated()
        {
            try
            {
                lookup = otherAsset.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("StockRoom_Location");
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "The Location is populated not as expected. Expected: " + temp;
                    }
                }
                else { error = "Cannot get Location lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_02_Verify_Substate_Available()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Available_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_00_Populate_SubState_Reserved()
        {
            try
            {
                string temp = Base.GData("Reserved_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_02_Verify_Substate_Reserved()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Reserved_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_00_Populate_SubState_Assembled()
        {
            try
            {
                string temp = Base.GData("Assembled_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_02_Verify_Substate_Assembled()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Assembled_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_00_Populate_SubState_Defective()
        {
            try
            {
                string temp = Base.GData("Defective_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_02_Verify_Substate_Defective()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Defective_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_00_Populate_SubState_Pending_Repair()
        {
            try
            {
                string temp = Base.GData("PendingRepair_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_02_Verify_Substate_Pending_Repair()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingRepair_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_00_Populate_SubState_Pending_Install()
        {
            try
            {
                string temp = Base.GData("PendingInstall_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_02_Verify_Substate_Pending_Install()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingInstall_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_00_Populate_SubState_Pending_Disposal()
        {
            try
            {
                string temp = Base.GData("PendingDisposal_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_02_Verify_Substate_Pending_Disposal()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingDisposal_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_00_Populate_SubState_Pending_Transfer()
        {
            try
            {
                string temp = Base.GData("PendingTransfer_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_Verify_Substate_Pending_Transfer()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingTransfer_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_00_Populate_SubState_Pending_Customer_Disposal()
        {
            try
            {
                string temp = Base.GData("PendingCustomerDisposal_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_Verify_Substate_Pending_Customer_Disposal()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingCustomerDisposal_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_Populate_SubState_PreAllocated()
        {
            try
            {
                string temp = Base.GData("PreAllocated_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Save_Other_Assets_Expected_Show_Error_Message()
        {
            try
            {
                flag = otherAsset.Save(true,true);
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_SaveVerifyErrorMessage()
        {
            try
            {
                string temp = Base.GData("ErrorMessage");
                if (flag)
                {
                    flag = otherAsset.Verify_ExpectedErrorMessages_Existed(temp);
                    if (!flag)
                    {
                        error = "The error message does not display";
                    }
                }
                else
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Populate_State_OnOrder()
        {
            try
            {
                string temp = Base.GData("OnOrder_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Verify_Substate_None()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("None_State");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_00_Populate_State_InTransit()
        {
            try
            {
                string temp = Base.GData("InTransit_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_01__Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save(true, true);
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_02_Verify_SubstateValue()
        {
            try
            {
                string temp = Base.GData("SubstateOfInTransit");
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyActualItemsExisted(temp);
                }
                else
                {
                    error = "Cannot get Model Substate combobox.";
                }
                
            }
            catch (Exception e)
            {

                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_00_Populate_SubState_Available()
        {
            try
            {
                string temp = Base.GData("Available_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_02_Verify_Substate_Available()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Available_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_00_Populate_SubState_Assembled()
        {
            try
            {
                string temp = Base.GData("Assembled_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_02_Verify_Substate_Assembled()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Assembled_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_00_Populate_SubState_Reserved()
        {
            try
            {
                string temp = Base.GData("Reserved_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_02_Verify_Substate_Reserved()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Reserved_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_00_Populate_SubState_Defective()
        {
            try
            {
                string temp = Base.GData("Defective_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_02_Verify_Substate_Defective()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Defective_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_00_Populate_SubState_Pending_Install()
        {
            try
            {
                string temp = Base.GData("PendingInstall_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_02_Verify_Substate_Pending_Install()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingInstall_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_00_Populate_SubState_Pending_Disposal()
        {
            try
            {
                string temp = Base.GData("PendingDisposal_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_02_Verify_Substate_Pending_Disposal()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingDisposal_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_00_Populate_SubState_Pending_Transfer()
        {
            try
            {
                string temp = Base.GData("PendingTransfer_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_02_Verify_Substate_Pending_Transfer()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingTransfer_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_00_Populate_SubState_Pending_Repair()
        {
            try
            {
                string temp = Base.GData("PendingRepair_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Verify_Substate_Pending_Repair()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("PendingRepair_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_00_Populate_SubState_PreAllocated()
        {
            try
            {
                string temp = Base.GData("PreAllocated_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_Save_Other_Assets_Expected_Show_Error_Message()
        {
            try
            {
                flag = otherAsset.Save(true, true);
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_SaveVerifyErrorMessage()
        {
            try
            {
                string temp = Base.GData("ErrorMessage");
                if (flag)
                {
                    flag = otherAsset.Verify_ExpectedErrorMessages_Existed(temp);
                    if (!flag)
                    {
                        error = "The error message does not display";
                    }
                }
                else
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_00_Populate_State_InUse()
        {
            try
            {
                string temp = Base.GData("InUse_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_01_Verify_Substate_None()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("None_State");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_02_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_00_Populate_State_InMaintenance()
        {
            try
            {
                string temp = Base.GData("InMaintenance_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_01_Verify_Substate_None()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("None_State");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_00_Populate_State_Retired()
        {
            try
            {
                string temp = Base.GData("Retired_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_01_Verify_SubstateValue()
        {
            try
            {
                string temp = Base.GData("SubstateOfRetired");
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyActualItemsExisted(temp);
                }
                else
                {
                    error = "Cannot get Model Substate combobox.";
                }

            }
            catch (Exception e)
            {

                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_00_Populate_SubState_Disposed()
        {
            try
            {
                string temp = Base.GData("Disposed_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_Populate_Retireddate()
        {
            try
            {
                otherAsset.Select_Tab("Disposal");
                string temp = DateTime.Now.ToString("yyyymmdd");
                date = otherAsset.Date_RetiredDate();
                flag = date.Existed;
                if (flag)
                {
                    flag = date.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot select Retired date [" + temp + "]";
                    }
                }
                else { error = "Cannot get Retired date."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Verify_Substate_Disposed()
        {
            try
            {
                otherAsset.Select_Tab("General");
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Disposed_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_00_Populate_SubState_Sold()
        {
            try
            {
                string temp = Base.GData("Sold_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_Verify_Substate_Sold()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Sold_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_00_Populate_SubState_Donated()
        {
            try
            {
                string temp = Base.GData("Donated_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Verify_Substate_Donated()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Donated_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_00_Populate_SubState_Vendorcredit()
        {
            try
            {
                string temp = Base.GData("Vendorcredit_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_02_Verify_Substate_Vendorcredit()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Vendorcredit_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_00_Populate_State_Missing()
        {
            try
            {
                string temp = Base.GData("Missing_State");
                combobox = otherAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_01_Verify_SubstateValue()
        {
            try
            {
                string temp = Base.GData("SubstateOfMissing");
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyActualItemsExisted(temp);
                }
                else
                {
                    error = "Cannot get Model Substate combobox.";
                }

            }
            catch (Exception e)
            {

                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_00_Populate_SubState_Lost()
        {
            try
            {
                string temp = Base.GData("Lost_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_02_Verify_Substate_Lost()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Lost_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_00_Populate_SubState_Stolen()
        {
            try
            {
                string temp = Base.GData("Stolen_Substate");
                combobox = otherAsset.Combobox_Substate();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_01_Save_Other_Assets()
        {
            try
            {
                flag = otherAsset.Save();
                if (!flag)
                {
                    error = "Could not save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_02_Verify_Substate_Stolen()
        {
            try
            {
                combobox = otherAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Stolen_Substate");
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot select Substate [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }       
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_52_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
