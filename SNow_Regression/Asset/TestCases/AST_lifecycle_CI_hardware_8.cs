﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;

namespace Asset
{
    [TestFixture]
    public class AST_lifecycle_CI_hardware_8
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Postfix: " + postfix);
            System.Console.WriteLine("Finished - Configuration Item: " + CIName);

            System.Console.WriteLine("Finished - Asset Tag: " + assetTag);
            System.Console.WriteLine("Finished - Serial Number: " + serialNumber);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************


        //------------------------------------------------------------------
        SNow.Login login = null;
        SNow.Home home = null;
        SNow.Itil hrdAsset = null;
        SNow.ItilList astList = null;        
        SNow.Itil server = null;
        SNow.ItilList srvList = null;

        SNow.snobutton button;
        SNow.snolookup lookup;
        SNow.snotextbox textbox;
        SNow.snocombobox combobox;
        
        SNow.snodate date;
        //------------------------------------------------------------------
        string assetTag, serialNumber;
        string postfix, CIName;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************
        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                hrdAsset = new SNow.Itil(Base, "Hardware Asset");
                astList = new SNow.ItilList(Base, "Asset list");
                srvList = new SNow.ItilList(Base, "Server list");
                server = new SNow.Itil(Base, "Server");
                //------------------------------------------------------------------
                assetTag = string.Empty;
                serialNumber = string.Empty;
                postfix = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_Open_System()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                string temp = Base.UseGlobalPass;
                if (temp.ToLower() == "yes")
                {
                    Thread.Sleep(5000);
                }
                else
                {
                    login.WaitLoading();
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_Verify_Left_Menu_Items()
        {
            try
            {
                string[] p_leftitems = null;
                string[] leftitems = null;

                string temp = Base.GData("Nav_Menu");
                if (temp.Contains("|"))
                    p_leftitems = temp.Split('|');
                else
                    p_leftitems = new string[] { temp };

                foreach (string pitem in p_leftitems)
                {
                    bool iFlag = true;
                    if (pitem.Contains("::"))
                    {
                        leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
                        if (flag && !iFlag)
                            flag = false;
                    }
                    else
                    {
                        error = "incorrect format of given data [" + pitem + "]";
                        flag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Select_Hardware_Assets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Hardware Assets");
                if (flag)
                {
                    astList.WaitLoading();
                }
                else { error = "Can't find Hardware Assets link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Validate_HardwareAssets_Title()
        {
            try
            {
                flag = astList.List_Title().MyText.Equals("Hardware");
                if (!flag)
                {
                    error = "Label title 'Hardware' is NOT correct";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Click_New_button()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        astList.WaitLoading();
                    }
                    else { error = "Cannot click New button."; }
                }
                else { error = "Cannot get New button."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Populate_ModelCategory()
        {
            try
            {
                lookup = hrdAsset.Lookup_Modelcategory();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("ModelCategory");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Model category value."; }
                }
                else { error = "Cannot get lookup Model category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_Populate_Model()
        {
            try
            {
                lookup = hrdAsset.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Model");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Model value."; }
                }
                else { error = "Cannot get lookup Model."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_Verify_Manufacturer()
        {
            try
            {
                Thread.Sleep(3000);
                lookup = hrdAsset.Lookup_Manufacturer();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Manufacturer");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect value of Manufacturer."; flagExit = false; }
                }
                else { error = "Cannot get textbox Manufacturer."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_PopulateMandatoryFields()
        {
            try
            {
                error = " ";                
                string CurrentDatetime = DateTime.Now.ToString("yyyyMMddHHmmss");
                postfix = CurrentDatetime;
                //-----------------------------------------------------------------------
                //CI Name
                temp = Base.GData("CI Prefix Name");
                CIName = temp + "01 " + postfix;
                //**************************************************************
                #region Populate asset tag
                textbox = hrdAsset.Textbox_AssetTag();
                flag = textbox.Existed;

                if (flag)
                {
                    temp = "ASTAG_" + postfix;
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = error + " Cannot input Asset Tag";
                }
                else
                    error = error + " Cannot get textbox asset tag.";
                #endregion

                #region Populate Assigned
                lookup = hrdAsset.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("AssignedTo"));
                    if (!flag)
                        error = error + " Cannot select AssignedTo";
                }
                else
                    error = error + " Cannot get lookup AssignedTo.";
                #endregion

                #region Populate installed
                //string installed = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd hh:mm:ss");
                //datetime = hrdAsset.Datetime_Installed();
                //flag = datetime.Existed;
                //if (flag)
                //{
                //    flag = datetime.SetText(installed);
                //    if (!flag)
                //        error = error + " Cannot input installed datetime.";
                //}
                //else
                //    error = error + " Cannot get datetime installed.";
                #endregion

                #region Populate company
                lookup = hrdAsset.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("Company"));
                    if (!flag)
                        error = error + " Cannot input Company";
                }
                else
                    error = error + " Cannot get lookup company.";
                #endregion

                #region Populate department
                lookup = hrdAsset.Lookup_Department();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("Department"));
                    if (!flag)
                        error = error + " Cannot input Department";
                }
                else
                    error = error + " Cannot get lookup department.";
                #endregion

                #region Populate location
                lookup = hrdAsset.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText(Base.GData("Location"));
                    if (!flag)
                        error = error + " Cannot input Location";
                }
                else
                    error = error + " Cannot get lookup location.";
                #endregion

                #region Populate Managed by Company
                lookup = hrdAsset.Lookup_ManagedByCompany();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("ManagedByCompany"));
                    if (!flag)
                        error = error + " Cannot input ManagedByCompany";
                }
                else
                    error = error + " Cannot get lookup ManagedByCompany.";
                #endregion

                #region Populate Managed by 
                lookup = hrdAsset.Lookup_ManagedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("ManagedBy"));
                    if (!flag)
                        error = error + " Cannot input Managed By";
                }
                else
                    error = error + " Cannot get lookup Managed By.";
                #endregion

                #region Populate Owned By Company
                lookup = hrdAsset.Lookup_OwnedByCompany();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("OwnedByCompany"));
                    if (!flag)
                        error = error + " Cannot input Owned By Company";
                }
                else
                    error = error + " Cannot get lookup Owned By Company.";
                #endregion

                #region Populate Owned By
                lookup = hrdAsset.Lookup_OwnedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("OwnedBy"));
                    if (!flag)
                        error = error + " Cannot input Owned By";
                }
                else
                    error = error + " Cannot get lookup Owned By.";
                #endregion           

                #region Populate Serial Number
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = "SN_" + postfix;
                    flag = textbox.SetText(temp);
                    if (!flag)
                        error = error + " Cannot input Serial Number.";
                }
                else
                    error = error + " Cannot get textbox serial number.";
                #endregion

                #region Populate Support group
                lookup = hrdAsset.Lookup_SupportGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(Base.GData("SupportGroup"));
                    if (!flag)
                        error = error + " Cannot input Support group.";
                }
                else
                    error = error + " Cannot get lookup support group.";
                #endregion

                #region Populate Ownership
                combobox = hrdAsset.Combobox_Ownership();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(Base.GData("Ownership"));
                    if (!flag)
                        error = error + " Cannot select Ownership.";
                }
                else
                    error = error + " Cannot get combobox Ownership.";
                #endregion

                if (error != " ")
                    flag = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Select_State_OnOrder_ReservedFor()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.SelectItem("On Order");
                    Thread.Sleep(2000);
                    if (flag)
                    {
                        lookup = hrdAsset.Lookup_ReservedFor();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.Select(Base.GData("ReservedFor"));
                            Thread.Sleep(2000);
                            if (!flag)
                                error = "Cannot input data for ReservedFor";
                        }
                        else
                            error = "Cannot get lookup reserved for";
                    }
                    else
                        error = "Cannot select On Order state.";
                }
                else
                    error = "Cannot get combobox state.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_SubmitAsset()
        {
            try
            {
                button = hrdAsset.Button_Submit();
                flag = button.Existed;

                if (flag)
                {
                    button.Click(true);
                    astList.WaitLoading();
                }
                else
                    error = "Cannot get button submit.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_SearchAsset()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                flag = astList.Filter("Asset tag;is;ASTAG_" + postfix);
                flag = astList.SearchAndOpen("Asset tag", "ASTAG_" + postfix, "Asset tag=ASTAG_" + postfix, "Asset tag");
                if (!flag)
                    error = "Cannot search asset with asset tag - " + Base.GData("AssetTag") + postfix;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_VerifyAssetInfor()
        {
            try
            {
                error = " ";
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //verify model category
                lookup = hrdAsset.Lookup_Modelcategory();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("ModelCategory"));
                    if (!flag)
                        error = error + " Incorrect value. Expected - " + Base.GData("ModelCategory");
                }

                //verify model
                lookup = hrdAsset.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Model"));
                    if (!flag)
                        error = error + " Incorrect value. Expected - " + Base.GData("Model");
                }

                //verify asset tag
                textbox = hrdAsset.Textbox_AssetTag();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = "ASTAG_" + postfix;
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                        error = error + " Incorrect value. Expected - " + temp;
                }

                //verify serial number
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    temp = "SN_" + postfix;
                    flag = textbox.VerifyCurrentValue(temp);
                    if (!flag)
                        error = error + " Incorrect value. Expected - " + temp;
                }
                //verify company
                lookup = hrdAsset.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Company"));
                    if (!flag)
                        error = error + " Incorrect value. Expected - " + Base.GData("Company");
                }

                //verify location
                lookup = hrdAsset.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Location"));
                    if (!flag)
                        error = error + " Incorrect value. Expected - " + Base.GData("Location");
                }

                //verify state
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("On Order");
                    if (!flag)
                        error = error + " Incorrect value. Expected - On Order";
                }

                //verify substate is Readonly
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("-- None --");
                    if (!flag)
                        error = error + " Incorrect value. Expected - -- None --";
                    else
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                            error = error + "The Substate field is not read only when State is On Order.";
                    }
                }
                if (error != " ")
                    flag = false;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_OpenPrivateBrowser()
        {
            try
            {
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Type").ToLower();
                    if (temp == "ff")
                    {
                        textbox.MyElement.SendKeys(Keys.LeftControl + Keys.Shift + "P");
                        flag = Base.SwitchToPage(1);
                    }
                    else if (temp == "chr")
                    {
                        System.Windows.Forms.SendKeys.SendWait("^+{N}");
                        flag = Base.SwitchToPage(1);
                    }
                    if (!flag) { error = "Cannot switch to page (1)."; }
                }
                else { error = "Cannot get textbox Serial Number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Login()
        {
            try
            {
                //Open the URL
                //Base.ClearCache();
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
                //--------------------------------------------------
                //Log IN
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                string userfullname = Base.GData("UserFullName");
                flag = login.LoginToSystem(user, pwd);
                if (flag)
                {
                    home.WaitLoading();
                    flag = home.Verify_User_FullName(userfullname);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Welcome login account is NOT correct.";
                    }
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_ImpersonateUser_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Open_ServerList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Servers", "Servers");
                if (flag)
                {
                    srvList.WaitLoading();
                    flag = srvList.List_Title().MyText.Equals("Servers");
                    if (!flag)
                        error = "The list's header is incorrect. Expected -  Server.";
                }
                else { error = "Can't find Server link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_FilterAndOpen_CI_Created()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (postfix == null || postfix == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input postfix.");
                    addPara.ShowDialog();
                    postfix = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string serialNumber = "SN_" + postfix;
                srvList.WaitLoading();
                //-- Open Asset
                flag = srvList.Filter("Serial number;is;" + serialNumber);
                flag = srvList.SearchAndOpen("Serial number", serialNumber, "Serial number="+serialNumber, "Name");
                if (flag)
                {
                    server.WaitLoading();
                }
                else { error = "Error when open Asset with condition: " + temp; }      
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Verify_CIStatus()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "On Order";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify Status value or Invalid Status."; flagExit = false; }
                }
                else { error = "Cannot get combobox Status."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_02_PopulateState_InStock()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In Stock";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_03_Populate_SubState_Available()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Available";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_04_Populate_Stockroom_Verify_Location()
        {
            try
            {
                lookup = hrdAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Stockroom");
                    flag = lookup.SetText(temp, true);
                    if (!flag)
                        error = "Cannot populate Stock room value.";
                    else
                    {
                        Thread.Sleep(3000);
                        lookup = hrdAsset.Lookup_Location();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(Base.GData("StockroomLocation"));
                            if (!flag)
                                error = "Incorrect Location value. Expected - " + Base.GData("StockroomLocation");
                        }
                        else
                            error = "Cannot get lookup location.";
                    }
                }
                else { error = "Cannot get lookup Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_05_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
                else hrdAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_03_Verify_CI_State_InStock()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Stock");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }        
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_04_Verify_CI_Stockroom()
        {
            try
            {
                lookup = server.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("Stockroom"));
                    if (!flag)
                        error = "Incorrect Location value. Expected - " + Base.GData("Stockroom");
                }
                else
                    error = "Cannot get lookup location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_05_Verify_CI_Stockroom_Location()
        {
            try
            {
                lookup = server.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(Base.GData("StockroomLocation"));
                    if (!flag)
                        error = "Incorrect Location value. Expected - " + Base.GData("StockroomLocation");
                }
                else
                    error = "Cannot get lookup location.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_Populate_SubState_Assembled()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Assembled";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_03_Verify_CI_State_InStock()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Stock");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_02_Populate_SubState_Reserved()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Reserved";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_03_Verify_CI_State_InStock()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Stock");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_02_Populate_SubState_Defective()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Defective";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_03_Verify_CI_State_Defective()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Defective");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_02_Populate_SubState_Pendingrepair()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending repair";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_03_Verify_CI_State_Pendingrepair()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending repair");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_02_Populate_SubState_PendingCustomerdisposal()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending Customer Disposal";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combpbox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_03_Verify_CI_State_PendingCustomerdisposal()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In stock");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_02_Populate_SubState_Pendingdisposal()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending disposal";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_03_Verify_CI_State_Pendingdisposal()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Disposition");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_02_Populate_SubState_Pendinginstall()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending install";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_03_Verify_CI_State_Pendinginstall()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending Install");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_02_Populate_SubState_Pendingtransfer()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending transfer";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_03_Verify_CI_State_Pendingtransfer()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending transfer");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_02_Populate_SubState_Preallocated_Stockroom()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pre-allocated";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Cannot populate Sub State value.";
                    else
                    {

                        lookup = hrdAsset.Lookup_Stockroom();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.SetText(Base.GData("Stockroom2"), true);
                            Thread.Sleep(2000);
                            if (flag)
                            {
                                Thread.Sleep(3000);
                                lookup = hrdAsset.Lookup_Location();
                                flag = lookup.Existed;
                                if (flag)
                                {
                                    flag = lookup.VerifyCurrentValue(Base.GData("StockroomLocation2"));
                                    if (!flag)
                                        error = "Incorrect stockroom's location.";
                                }
                                else
                                    error = "Cannot get lookup location.";
                            }
                            else
                                error = "Cannot populate stock room.";
                        }
                        else
                            error = "Cannot get lookup stockroom";
                    }
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save(true,true);
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_04_Verify_Error_Message()
        {
            try
            {
                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed("Cannot set to pre-allocated on an update. Please create a new record;Invalid update");
                if (!flag) { error = "Incorrect error message displays."; }
                else
                {
                    combobox = hrdAsset.Combobox_Substate();
                    if (combobox.Existed)
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                            error = "The sub state is not read only";
                    }
                    else
                        error = "Cannot get combobox sub state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_03_Verify_CI_State_PendingTransfer()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending Transfer");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_02_Populate_State_OnOrder()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "On order";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_03_Verify_CI_State_OnOrder()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("On Order");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_02_PopulateState_InTransit()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In transit";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_03_Populate_SubState_Available()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Available";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_04_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_03_Verify_CI_State_InTransit()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Transit");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_02_Populate_SubState_Assembled()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Assembled";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_03_Verify_CI_State_Assembled()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Stock");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_02_Populate_SubState_Defective()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Defective";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_03_Verify_CI_State_InStock()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Defective");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_02_Populate_SubState_PendingInstall()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending install";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_03_Verify_CI_State_PendingInstall()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending Install");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_02_Populate_SubState_PendingTransfer()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending transfer";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_03_Verify_CI_State_PendingTransfer()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending Transfer");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_02_Populate_SubState_PendingRepair()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending repair";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_03_Verify_CI_State_PendingRepair()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending Repair");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_02_Populate_SubState_PendingDisposal()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending disposal";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_03_Verify_CI_State_InDisposition()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Disposition");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_02_Populate_SubState_Pendingrepair()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pending repair";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_03_Verify_CI_State_Pendingrepair()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending repair");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_02_Populate_SubState_Preallocated_Stockroom()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Pre-allocated";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Cannot populate Sub State value.";
                    else
                    {
                        lookup = hrdAsset.Lookup_Stockroom();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.SetText(Base.GData("Stockroom2"), true);
                            if (flag)
                            {
                                Thread.Sleep(3000);
                                lookup = hrdAsset.Lookup_Location();
                                flag = lookup.Existed;
                                if (flag)
                                {
                                    flag = lookup.VerifyCurrentValue(Base.GData("StockroomLocation2"));
                                    if (!flag)
                                        error = "Incorrect stockroom's location.";
                                }
                                else
                                    error = "Cannot get lookup location.";
                            }
                            else
                                error = "Cannot populate stock room.";
                        }
                        else
                            error = "Cannot get lookup stockroom";
                    }
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save(true,true);
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_063_04_Verify_Error_Message()
        {
            try
            {
                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed("Cannot set to pre-allocated on an update. Please create a new record;Invalid update");
                if (!flag) { error = "Incorrect error message displays."; }
                else
                {
                    combobox = hrdAsset.Combobox_Substate();
                    if (combobox.Existed)
                    {
                        flag = combobox.ReadOnly;
                        if (!flag)
                            error = "The sub state is not read only";
                    }
                    else
                        error = "Cannot get combobox sub state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_064_03_Verify_CI_State_InDisposition()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Pending Repair");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_02_Populate_State_OnOrder()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "On order";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag) { error = "Cannot populate State value."; }
                    else
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.ReadOnly;
                            if (flag)
                            {
                                flag = combobox.VerifyCurrentValue("-- None --");
                                if (!flag)
                                    error = "The default text of substate is incorrect.";
                            }
                            else
                                error = "The substate is not read only field.";
                        }
                        else
                            error = "Cannot get combobox sub state.";
                    }
                }
                else { error = "Cannot get lookup State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_065_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_066_03_Verify_CI_State_OnOrder()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("On Order");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_02_Populate_State_InTransit()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In transit";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                }
                else { error = "Cannot get lookup State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_067_03_Populate_SubState_Reserved()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Reserved";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_04_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_068_03_Verify_CI_State_InTransit()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Transit");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_02_Populate_State_InUse()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In use";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag)
                        error = "Cannot populate State value.";
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_069_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_070_03_Verify_CI_State_Installed()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Installed");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_02_Populate_State_InMaintenance()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In maintenance";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag)
                        error = "Cannot populate State value.";
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_071_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_072_03_Verify_CI_State_InMaintenance()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In Maintenance");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_02_Populate_State_Missing()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Missing";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag)
                        error = "Cannot populate State value.";
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_03_Populate_SubState_Lost()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Lost";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                        error = "Cannot populate Sub State value.";
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox sub state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_073_04_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_074_03_Verify_CI_State_Absent()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Absent");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_02_Populate_SubState_Stolen()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Stolen";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox sub state."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_075_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_076_03_Verify_CI_State_Stolen()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Stolen");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_02_PopulateState_Retired()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Retired";
                    flag = combobox.SelectItem(temp);
                    Thread.Sleep(2000);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_03_Populate_SubState_Disposed()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Disposed";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_077_04_Select_RetiredDate()
        {
            try
            {
                flag = hrdAsset.Select_Tab("Disposal");
                if (flag)
                {
                    var now = DateTime.Now;
                    string fdate = now.AddDays(2).ToString("yyyy-MM-dd");
                    date = hrdAsset.Date_RetiredDate();
                    flag = date.Existed;
                    if (flag)
                    {
                        flag = date.SetText(fdate, true);
                        if (!flag)
                            error = "Cannot populate planned start date.";
                    }
                    else error = "Cannot get datetime retired date.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_077_05_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_078_03_Verify_CI_State_Retired()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Retired");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_02_Populate_SubState_Donated()
        {
            try
            {
                flag = hrdAsset.Select_Tab("General");
                if (flag)
                {
                    combobox = hrdAsset.Combobox_Substate();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Donated";
                        flag = combobox.SelectItem(temp);
                        if (!flag) { error = "Cannot populate Sub State value."; }
                    }
                    else { error = "Cannot get combobox Sub State."; }
                }
                else
                    error = "Cannot click on General tab";

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_079_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_080_03_Verify_CI_State_Retired()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Retired");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_02_Populate_SubState_Sold()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Sold";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Sub State value."; }
                    else Thread.Sleep(2000);
                }
                else { error = "Cannot get combobox Sub State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_081_03_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_082_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_082_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_082_03_Verify_CI_State_Retired()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Retired");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_083_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_083_02_Populate_SubState_VendorCredit()
        {
            try
            {
                string temp = string.Empty;
                flag = hrdAsset.Select_Tab("Disposal");
                if (flag)
                {
                    date = hrdAsset.Date_RetiredDate();
                    if (date.Existed)
                    {
                        temp = date.Text;
                        flag = hrdAsset.Select_Tab("General");
                        if (flag)
                        {
                            combobox = hrdAsset.Combobox_Substate();
                            flag = combobox.Existed;
                            if (flag)
                            {
                                string temp1 = "Vendor credit";
                                flag = combobox.SelectItem(temp1);
                                if (!flag) { error = "Cannot populate Sub State value."; }
                                else
                                {
                                    flag = hrdAsset.Save();
                                    if (!flag) { error = "Cannot save Asset."; }
                                    else
                                    {
                                        flag = hrdAsset.Select_Tab("Disposal");
                                        if (flag)
                                        {
                                            date = hrdAsset.Date_RetiredDate();
                                            if (date.Existed)
                                            {
                                                if (date.Text != temp)
                                                    error = "The Retired date value is changed. Expected: The value is not change.";
                                            }
                                            else
                                            { error = "Cannot get lookup Sub State."; }
                                        }
                                        else
                                            error = "Cannot click on Disposal tab.";
                                    }
                                }
                            }
                            else { error = "Cannot get combobox Sub State."; }
                        }
                        else
                            error = "Cannot click on General tab.";
                    }
                    else
                        error = "Cannot get datime retired date.";
                }
                else
                    error = "Cannot click on Disposal tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_084_03_Verify_CI_State_Retired()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Retired");
                    if (!flag)
                        error = "Incorrect Status value";
                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_085_01_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_085_02_Populate_State_Received()
        {
            try
            {
                flag = hrdAsset.Select_Tab("General");
                if (flag)
                {
                    combobox = hrdAsset.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Received";
                        flag = combobox.SelectItem(temp);
                        if (!flag) { error = "Cannot populate State value."; }
                    }
                    else { error = "Cannot get combobox State."; }
                }
                else
                    error = "Cannot select general tab";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_085_03_Populate_Stockroom()
        {
            try
            {
                lookup = hrdAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Stockroom_Received");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Stockroom value (" + temp + ").";
                    }
                }
                else { error = "Cannot get Stockroom lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_085_04_Save_Asset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_01_SwitchToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag)
                {
                    error = "Cannot switch to page 1.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_02_ReloadForm()
        {
            try
            {
                flag = hrdAsset.ReloadForm();
                if (flag)
                {
                    hrdAsset.WaitLoading();
                }
                else { error = "Cannot reload form."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_086_03_Verify_CI_State_Recieved()
        {
            try
            {
                combobox = server.Combobox_Status();
                flag = combobox.Existed;

                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("Received");
                    if (flag)
                    {
                        combobox = server.Combobox_Status();
                    }
                    error = "Incorrect Status value";

                }
                else
                    error = "Cannot get combobox status.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_87_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag)
                {
                    error = "Cannot switch to page 0.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        [Test]
        public void Step_88_Logout()
        {
            try
            {
                home.Logout();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}

