﻿using NUnit.Framework;
using System;
using System.Reflection;
using System.Threading;
using OpenQA.Selenium;
using SNow;
namespace Asset
{
    [TestFixture]
    public class AST_e2e_for_hardware_6
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, temp, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            Console.WriteLine("Finished - Asset Tag: " + assetTag);
            Console.WriteLine("Finished - Serial Number: " + serialNumber);
            Console.WriteLine("Finished - Current Time: " + currentTime);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        SNow.snotextbox textbox;
        SNow.snolookup lookup;
        SNow.snocombobox combobox;
        SNow.snobutton button;
        SNow.snodatetime datetime;
        SNow.snodate date;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        Itil hrdAsset = null;
        ItilList astList = null;
        Itil netGear = null;
        ItilList netList = null;
        //------------------------------------------------------------------
        string assetTag, currentTime, serialNumber;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new SNow.Login(Base);
                home = new SNow.Home(Base);
                hrdAsset = new SNow.Itil(Base, "Hardware Asset");
                astList = new SNow.ItilList(Base, "Asset list");
                netGear = new SNow.Itil(Base, "Network Gear");
                netList = new SNow.ItilList(Base, "Network Gear List");
                //------------------------------------------------------------------
                assetTag = string.Empty;
                serialNumber = string.Empty;
                currentTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_Open_System()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        ////-----------------------------------------------------------------------------------------------------------------------------------
        //[Test]
        //public void Step_005_Verify_VerifyAssetMngModules()
        //{
        //    try
        //    {
        //        string[] p_leftitems = null;
        //        string[] leftitems = null;

        //        string temp = Base.GData("Nav_Menu");
        //        if (temp.Contains("|"))
        //            p_leftitems = temp.Split('|');
        //        else
        //            p_leftitems = new string[] { temp };

        //        foreach (string pitem in p_leftitems)
        //        {
        //            bool iFlag = true;
        //            if (pitem.Contains("::"))
        //            {
        //                leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
        //                iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
        //                if (flag && !iFlag)
        //                    flag = false;
        //            }
        //            else
        //            {
        //                error = "incorrect format of given data [" + pitem + "]";
        //                flag = false;
        //            }
        //        }


        //        string itemList = Base.GData("Nav_Menu");
        //        flag = home.LeftMenuItemValidate("Asset Management", itemList);
        //        if (!flag)
        //        {
        //            error = "Cannot find some items. PLEASE CHECK EITHER RUN TIME AND DATA";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        flag = false;
        //        error = ex.Message;
        //    }
        //}
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_Select_Hardware_Assets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Hardware Assets");
                if (flag)
                {
                    astList.WaitLoading();
                }
                else { error = "Cannot find Hardware Assets link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_Click_New_button()
        {
            try
            {
                button = astList.Button_New();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else { error = "Cannot click New button."; }
                }
                else { error = "Cannot get button New."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_SelectCategory_And_Model()
        {
            try
            {
                ///*Click*/
                //textbox = hrdAsset.Textbox_DisplayName;
                //flag = textbox.Existed;
                //if (flag)
                //{
                //    flag = textbox.Click();
                //}
                //else { error = "Cannot get Display name textbox."; }

                lookup = hrdAsset.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("Category");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        lookup = hrdAsset.Lookup_Model();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            temp = Base.GData("Model");
                            flag = lookup.Select(temp);
                            if (!flag)
                            { error = "Cannot select Model value."; }
                        }
                        else { error = "Cannot found lookup Model."; }
                    }
                    else { error = "Cannot select Model Category value."; }
                }
                else { error = "Cannot get lookup Model Category."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Verify_AutoPopulate_State_InUse()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue("In use");
                    if (!flag)
                    {
                        error = "Invalid state.";
                        flagExit = false;
                    }
                }
                else error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_03_Verify_Installed_Date_AutoPopulate()
        {
            try
            {
                datetime = hrdAsset.Datetime_Installed_Date();
                flag = datetime.Existed;
                if (flag)
                {
                    temp = datetime.Text;
                    if (temp == "")
                    {
                        error = "Installed date is not auto populate.";
                        flag = false;
                        flagExit = false;
                    }
                }
                else error = "Cannot get datetime installed date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_04_Verify_AssignedDate_ReadOnly()
        {
            try
            {
                datetime = hrdAsset.Datetime_Assigned_Date();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.ReadOnly;
                    if (!flag) error = "Assigned Date is not readonly.";
                }
                else error = "Cannot get datetime installed date.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_InputValidValues()
        {
            try
            {
                assetTag = "Auto Hardware" + " - " + System.DateTime.Now.ToString("yyyyyMMdd HHmmss");

                //Asset Tag
                textbox = hrdAsset.Textbox_AssetTag();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(assetTag);
                    if (flag)
                    {
                        //Company
                        lookup = hrdAsset.Lookup_Company();
                        string temp = Base.GData("Company");
                        flag = lookup.Existed;
                        if (flag)
                        {

                            flag = lookup.Select(temp);
                            if (flag)
                            {
                                //Department
                                lookup = hrdAsset.Lookup_Department();
                                flag = lookup.Existed;
                                if (flag)
                                {
                                    temp = Base.GData("Department");
                                    flag = lookup.Select(temp);
                                    if (flag)
                                    {
                                        //Location
                                        lookup = hrdAsset.Lookup_Location();
                                        flag = lookup.Existed;
                                        if (flag)
                                        {
                                            temp = Base.GData("Location");
                                            flag = lookup.Select(temp);
                                            if (flag)
                                            {
                                                //Assigned 
                                                datetime = hrdAsset.Datetime_Assigned();
                                                flag = datetime.Existed;
                                                if (flag)
                                                {
                                                    temp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                    flag = datetime.SetText(temp, true);
                                                    if (flag)
                                                    {
                                                        //Installed 
                                                        datetime = hrdAsset.Datetime_Installed();
                                                        flag = datetime.Existed;
                                                        if (flag)
                                                        {
                                                            temp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                            flag = datetime.SetText(temp, true);
                                                            if (flag)
                                                            {
                                                                //Managed By Company
                                                                lookup = hrdAsset.Lookup_ManagedByCompany();
                                                                flag = lookup.Existed;
                                                                if (flag)
                                                                {
                                                                    temp = Base.GData("ManagedByCompany");
                                                                    flag = lookup.Select(temp);
                                                                    if (flag)
                                                                    {
                                                                        //Managed by
                                                                        lookup = hrdAsset.Lookup_ManagedBy();
                                                                        flag = lookup.Existed;
                                                                        if (flag)
                                                                        {
                                                                            temp = Base.GData("ManagedBy");
                                                                            flag = lookup.Select(temp);
                                                                            if (flag)
                                                                            {
                                                                                //Owned By Company
                                                                                lookup = hrdAsset.Lookup_OwnedByCompany();
                                                                                flag = lookup.Existed;
                                                                                if (flag)
                                                                                {
                                                                                    temp = Base.GData("OwnedByCompany");
                                                                                    flag = lookup.Select(temp);
                                                                                    if (flag)
                                                                                    {
                                                                                        //Owned by
                                                                                        lookup = hrdAsset.Lookup_OwnedBy();
                                                                                        flag = lookup.Existed;
                                                                                        if (flag)
                                                                                        {
                                                                                            temp = Base.GData("OwnedBy");
                                                                                            flag = lookup.Select(temp);
                                                                                            if (!flag)
                                                                                            {
                                                                                                error = "Cannot populate Owned by value.";
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                combobox = hrdAsset.Combobox_Ownership();
                                                                                                flag = combobox.Existed;
                                                                                                if (flag)
                                                                                                {
                                                                                                    flag = combobox.SelectItem(Base.GData("Ownership"));
                                                                                                    if (!flag)
                                                                                                        error = error + " Cannot input Ownership";
                                                                                                }
                                                                                                else
                                                                                                    error = error + " Cannot get combobox Ownership.";
                                                                                            }
                                                                                        }
                                                                                        else { error = "Cannot get lookup Owned by."; }
                                                                                    }
                                                                                    else { error = "Cannot populate Owned By Company value."; }
                                                                                }
                                                                                else { error = "Cannot get lookup Owned By Company."; }
                                                                            }
                                                                            else { error = "Cannot populate Managed by value."; }
                                                                        }
                                                                        else { error = "Cannot get lookup Managed by."; }
                                                                    }
                                                                    else { error = "Cannot populate Managed by Company value."; }
                                                                }
                                                                else { error = "Cannot get lookup Managed by Company."; }
                                                            }
                                                            else { error = "Cannot populate Installed value."; }
                                                        }
                                                        else { error = "Cannot get lookup Installed."; }
                                                    }
                                                    else { error = "Cannot populate Assigned value."; }
                                                }
                                                else { error = "Cannot get lookup Assigned."; }
                                            }
                                            else { error = "Cannot populate Location value."; }
                                        }
                                        else { error = "Cannot get lookup Location."; }
                                    }
                                    else { error = "Cannot populate Deparment value."; }
                                }
                                else { error = "Cannot get lookup Department."; }
                            }
                            else { error = "Cannot populate Company value."; }
                        }
                        else { error = "Cannot get lookup Company."; }
                    }
                    else { error = "Cannot populate Asset Tag value."; }
                }
                else { error = "Cannot get textbox Asset Tag."; }

                //Support Group
                lookup = hrdAsset.Lookup_SupportGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("SupportGroup");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Support Group value.";
                    }
                }
                else { error = "Cannot get lookup Support Group."; }

                //Serial Nummber 
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    serialNumber = "SN" + " - " + System.DateTime.Now.ToString("yyyyyMMdd HHmmss");
                    flag = textbox.SetText(serialNumber);
                    if (!flag) { error = "Cannot populate Serial Number value."; }
                }
                else { error = "Cannot get textbox Serinal Number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_Populate_AssignedTo()
        {
            try
            {
                lookup = hrdAsset.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("AssignedTo");
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate Assigned to value."; }
                }
                else { error = "Cannot get Assigned to lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_03_Verify_Department()
        {
            try
            {
                lookup = hrdAsset.Lookup_Department();
                flag = lookup.Existed;
                if (flag)
                {
                    temp = Base.GData("Department");
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify Department value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Department lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_04_Select_Ownership()
        {
            try
            {
                temp = Base.GData("Ownership");
                combobox = hrdAsset.Combobox_Ownership();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) error = "Cannot select ownership.";
                }
                else error = "Cannot get combobox ownership.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_05_SelectState_SubState()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = Base.GData("State");
                    flag = combobox.SelectItem(temp);
                    hrdAsset.WaitLoading();
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = Base.GData("SubState");
                            flag = combobox.SelectItem(temp);
                            if (!flag)
                            { error = "Cannot select Substate"; }
                        }
                        else { error = "Cannot get combobox SubState"; }
                    }
                    else { error = "Cannot select State"; }
                }
                else { error = "Cannot get combobox State"; }

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_01_Select_StockRoom()
        {
            try
            {

                //Select Stockroom
                temp = Base.GData("StockRoom");
                lookup = hrdAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        //Verify 
                        hrdAsset.WaitLoading();
                        Thread.Sleep(2000);
                        temp = Base.GData("StockRoom_Location");
                        lookup = hrdAsset.Lookup_Location();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            flag = lookup.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Incorrect Location populated"; flagExit = false;
                            }
                        }
                        else { error = "Cannot get Stockroom Location."; }
                    }
                    else { error = "Cannot select Stockroom"; }
                }
                else { error = "Not found lookup Stockroom"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_02_Submit_HardwareAsset()
        {
            try
            {
                //Submit Asset
                button = hrdAsset.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        astList.WaitLoading();
                    }
                    else { error = "Cannot click button submit"; }
                }
                else { error = "Cannot get button Submit"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_SearchAndOpen_HardwareAssets()
        {
            try
            {
                //Open dialog for input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (assetTag == null || assetTag == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input asset tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //---------------------------------------------
                string conditions = "Asset tag;is;" + assetTag;
                flag = astList.Filter(conditions);
                if (flag)
                {
                    flag = astList.Open("Asset tag=" + assetTag, "Asset tag");
                    if (!flag)
                    {
                        error = "Cannot open Asset tag with condition: " + temp;
                    }
                }
                else { error = "Not found Asset tag record. "; }


            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_VerifyAsset()
        {
            try
            {
                //Open dialog for input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (assetTag == null || assetTag == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input asset tag");
                    addPara.ShowDialog();
                    assetTag = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------------------------
                //Open dialog for input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (serialNumber == null || serialNumber == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------------------------

                //Verify Category
                temp = Base.GData("Category");
                lookup = hrdAsset.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Model Category"; flagExit = false; }
                }
                else { error = "Cannot get combobox Category."; }

                //Verify Model
                temp = Base.GData("Model");
                lookup = hrdAsset.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Model."; flagExit = false; }
                }
                else { error = "Cannot get combobox Model."; }

                //Verify Support Group
                temp = Base.GData("SupportGroup");
                lookup = hrdAsset.Lookup_SupportGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Support Group."; flagExit = false; }
                }
                else { error = "Cannot get combobox Support Group."; }

                //Verify Asset Tag                    
                textbox = hrdAsset.Textbox_AssetTag();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(assetTag);
                    if (!flag) { error = "Incorrect Asset Tag."; flagExit = false; }
                }
                else { error = "Cannot get combobox Asset Tag."; }

                //Verify Serial Number
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(serialNumber);
                    if (!flag) { error = "Incorrect Serial Number."; flagExit = false; }
                }
                else { error = "Cannot get combobox Serial Number."; }

                //Verify State
                temp = Base.GData("State");
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect State."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }

                //Verify SubState
                temp = Base.GData("SubState");
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Substate."; flagExit = false; }
                }
                else { error = "Cannot get combobox Substate."; }

                //Verify Stock Room
                temp = Base.GData("StockRoom");
                lookup = hrdAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Stockroom."; flagExit = false; }
                }
                else { error = "Cannot get combobox Stockroom."; }

                //Verify Company
                temp = Base.GData("Company");
                lookup = hrdAsset.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Company."; flagExit = false; }
                }
                else { error = "Cannot get combobox Company."; }

                //Verify Managed By Company
                temp = Base.GData("ManagedByCompany");
                lookup = hrdAsset.Lookup_ManagedByCompany();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Managed By Company."; flagExit = false; }
                }
                else { error = "Cannot get combobox Managed By Company."; }

                //Verify Managed By
                temp = Base.GData("ManagedBy");
                lookup = hrdAsset.Lookup_ManagedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Managed By."; flagExit = false; }
                }
                else { error = "Cannot get combobox Managed By."; }

                //Verify Owned By Company
                temp = Base.GData("OwnedByCompany");
                lookup = hrdAsset.Lookup_OwnedByCompany();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Owned By Company."; flagExit = false; }
                }
                else { error = "Cannot get combobox Owned By Company."; }

                //Verify Owned By
                temp = Base.GData("OwnedBy");
                lookup = hrdAsset.Lookup_OwnedBy();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Owned By."; flagExit = false; }
                }
                else { error = "Cannot get combobox Owned By."; }

                //Verify Assigned
                datetime = hrdAsset.Datetime_Assigned();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.VerifyCurrentValue(currentTime);
                    if (!flag) { error = "Incorrect Assigned value."; }
                }
                else { error = "Cannot get datetime Assigned."; }

                //Verify Installed
                datetime = hrdAsset.Datetime_Installed();
                flag = datetime.Existed;
                if (flag)
                {
                    flag = datetime.VerifyCurrentValue(currentTime);
                    if (!flag) { error = "Incorrect Installed value."; }
                }
                else { error = "Cannot get datetime Installed."; }

                //Verify Department
                temp = Base.GData("Department");
                lookup = hrdAsset.Lookup_Department();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Department"; flagExit = false; }
                }
                else { error = "Cannot get lookup Department."; }

                //Verify Ownership
                temp = Base.GData("Ownership");
                combobox = hrdAsset.Combobox_Ownership();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Incorrect Ownership"; flagExit = false; }
                }
                else { error = "Cannot get combobox Ownership."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------        

        #region State = In stock
        [Test]
        public void Step_013_Open_NewPrivateBrower()
        {
            try
            {
                Base.SwitchToPage(0);
                Base.ActiveWindow();
                Thread.Sleep(1000);
                textbox = hrdAsset.Textbox_SerialNumber();
                flag = textbox.Existed;
                if (flag)
                {
                    string temp = Base.GData("Type").ToLower();
                    if (temp == "ff")
                    {
                        textbox.MyElement.SendKeys(Keys.LeftControl + Keys.Shift + "P");
                        flag = Base.SwitchToPage(1);
                    }
                    else if (temp == "chr")
                    {
                        System.Windows.Forms.SendKeys.SendWait("^(+N)");
                        flag = Base.SwitchToPage(1);
                    }

                    if (!flag) { error = "Cannot switch to page (1)."; }
                }
                else { error = "Cannot get textbox Serial number."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_014_Login()
        {
            try
            {
                //Open the URL
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
                //--------------------------------------------------
                //Log IN
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //----------------------------------------------------------------------------------

        [Test]
        public void Step_015_ImpersonateUser_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_System_Setting()
        {
            //try
            //{
            //    flag = home.SystemSetting();
            //    if (!flag) { error = "Error when config system."; }
            //}
            //catch (Exception ex)
            //{
            //    flag = false;
            //    error = ex.Message;
            //}
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_017_Select_Network_Gear()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Network Gear", "Network Gear");
                if (flag)
                {
                    netList.WaitLoading();
                }
                else { error = "Cannot find Hardware Assets link"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        // [Huong C Nguyen 06 Nov] - DON'T DELETE - rem code for steps 18 - 21_01 due to defect DFCT0017242
        [Test]
        public void Step_018__NoCIGenerated()
        {
            try
            {
                //Open dialog for input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (serialNumber == null || serialNumber == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------------------------
                temp = "Serial Number;is;" + serialNumber;
                flag = netList.Filter(temp);
                if (flag)
                {
                    netList.WaitLoading();
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_01_ClickAllocateButton()
        {
            try
            {
                button = hrdAsset.Button_Allocate();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else { error = "Cannot click button Allocate."; }
                }
                else { error = "Cannot get button Allocate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_02_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_03_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Available";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_02_Filter_SearchandOpen_CI()
        {
            try
            {
                //Open dialog for input
                temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (serialNumber == null || serialNumber == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input serial number");
                    addPara.ShowDialog();
                    serialNumber = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------------------------
                temp = "Serial Number;is;" + serialNumber;
                flag = netList.Filter(temp);
                if (flag)
                {
                    netList.WaitLoading();
                    string cat = Base.GData("Category");
                    string classCondition = "Class=" + cat;
                    flag = netList.Open(classCondition, "Name");
                    if (flag)
                    {
                        netGear.WaitLoading();
                    }
                    else { error = "Error when open Asset with condition: " + temp; }
                }
                else { error = "Error when filter."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_03_Verify_CIStatus()
        {
            try
            {
                Thread.Sleep(3000);
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In Stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag) { error = "Cannot verify Status value or Invalid Status."; flagExit = false; }
                }
                else { error = "Cannot get combobox Status."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_02_Populate_SubState_Reserved()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Reserved";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_024_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Reserved";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_02_Verify_State_InStock()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_02_Populate_SubState_Defective()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Defective";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
                else hrdAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Defective";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_027_02_Verify_State_Defective()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Defective";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_02_Populate_SubState_PendingRepair()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending repair";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending repair";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_029_02_Verify_State_PendingRepair()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending repair";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_02_Populate_SubState_PendingDisposal()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending disposal";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending disposal";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_02_Verify_State_InDisposition()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Disposition";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_02_Populate_SubState_PendingInstall()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending install";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending install";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_02_Verify_State_PendingInstall()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending Install";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_02_Populate_SubState_PendingTransfer()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending transfer";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In stock";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending transfer";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_02_Verify_State_PendingTransfer()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending Transfer";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_02_Populate_State_OnOrder()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "On order";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "On order";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "-- None --";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_02_Verify_State_OnOrder()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "On Order";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        #endregion State = In stock

        #region State =  In transit
        [Test]
        public void Step_038_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_02_Populate_State_Substate_Available()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Available";
                            flag = combobox.SelectItem(temp);
                            if (!flag) { error = "Cannot populate Substate value."; }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_038_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Available";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_02_Verify_State_InTransit()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_02_Populate_SubState_Defective()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Defective";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Defective";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_02_Verify_State_Defective()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Defective";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_02_Populate_SubState_PendingInstall()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending install";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending install";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_02_Verify_State_PendingInstall()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending install";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_02_Populate_SubState_PendingDisposal()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending disposal";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending disposal";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_02_Verify_State_InDisposition()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Disposition";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_02_Populate_SubState_PendingTransfer()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending transfer";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending transfer";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_02_Verify_State_PendingTransfer()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending transfer";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_02_Populate_SubState_PendingRepair()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending repair";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pending repair";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_02_Verify_State_PendingRepair()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending repair";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_02_Populate_SubState_PreAllocated()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pre-allocated";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        hrdAsset.WaitLoading();
                    }
                    else { error = "Cannot populate Substate value."; }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_03_Populate_Stockroom()
        {
            try
            {
                lookup = hrdAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("StockRoom");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                    }
                    else { error = "Cannot populate Stockroom value."; }
                }
                else { error = "Cannot get lookup Stockroom."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_04_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save(false, true);
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_05_Verify_ErrorMessage()
        {
            try
            {
                string message = Base.GData("ErrorMessage");
                flag = hrdAsset.Verify_ExpectedErrorMessages_Existed(message);
                if (!flag) { error = "Cannnot verify error message or Invalid message. Expected: " + message; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_06_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Pre-allocated";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_02_Verify_State_PendingRepair()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Pending repair";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_02_Populate_State_OnOrder()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "On order";
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate State value."; }
                    else hrdAsset.WaitLoading();
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_03_SaveHardwareAsset()
        {
            try
            {
                Thread.Sleep(2000);
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
                else hrdAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "On order";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "-- None --";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_055_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_055_02_Verify_State_OnOrder()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "On Order";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_02_Populate_State_SubState()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Transit";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Reserved";
                            flag = combobox.SelectItem(temp);
                            if (!flag)
                            {
                                error = "Cannot populate Substate value.";
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
                else hrdAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Reserved";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_057_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_057_02_Verify_State_InTransit()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Transit";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        #endregion State =  In transit

        #region State = In Use

        [Test]
        public void Step_058_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_02_Populate_State()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Use";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In use";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "-- None --";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_059_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_059_02_Verify_State_Installed()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Installed";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        #endregion State = In Use

        #region State = In Maintenance

        [Test]
        public void Step_060_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_02_Populate_State_Maintenance()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In maintenance";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
                else hrdAsset.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In maintenance";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "-- None --";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_02_Verify_State_InMaintenance()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "In Maintenance";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------
        #endregion State = In Maintenance

        #region State = Missing

        [Test]
        public void Step_062_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_02_Populate_State_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Missing";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Lost";
                            flag = combobox.SelectItem(temp);
                            if (!flag)
                            {
                                error = "Cannot populate Substate value.";
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Missing";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Lost";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_063_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_063_02_Verify_State_Absent()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Absent";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_02_Populate_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Stolen";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Substate value.";
                    }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Missing";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Stolen";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_065_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_065_02_Verify_State_Stolen()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Stolen";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------
        #endregion State = Missing

        #region State = Retired

        [Test]
        public void Step_066_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_02_Populate_State_Substate_Disposed()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Disposed";
                            flag = combobox.SelectItem(temp);
                            if (!flag)
                            {
                                error = "Cannot populate Substate value.";
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot populate State value."; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_03_Populate_RetiredDate()
        {
            try
            {
                flag = hrdAsset.Select_Tab("Disposal");
                if (!flag)
                    error = "Cannot select 'Disposal' Tab.";
                else
                {
                    string temp = DateTime.Now.AddDays(2).ToString("yyyy-MM-dd");
                    date = hrdAsset.Date_RetiredDate();
                    flag = date.Existed;
                    if (flag)
                    {
                        flag = date.SetText(temp, true);
                        //Thread.Sleep(1000);
                        if (!flag)
                        {
                            error = "Cannot populate retired date value.";
                        }
                        //else
                        //{
                        //   textbox = hrdAsset.Textbox_DisplayName();
                        //   flag = textbox.Click();
                        //}
                    }
                    else error = "Cannot get datetime Retired date.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_04_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_05_VerifyState_Substate()
        {
            try
            {
                flag = hrdAsset.Select_Tab("General");
                if (!flag)
                    error = "Cannot select 'General' Tab.";
                else
                {
                    combobox = hrdAsset.Combobox_State();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        temp = "Retired";
                        flag = combobox.VerifyCurrentValue(temp);
                        if (flag)
                        {
                            combobox = hrdAsset.Combobox_Substate();
                            flag = combobox.Existed;
                            if (flag)
                            {
                                temp = "Disposed";
                                flag = combobox.VerifyCurrentValue(temp);
                                if (!flag)
                                {
                                    error = "Cannot verify or invalid value Substate.";
                                    flagExit = false;
                                }
                            }
                            else { error = "Cannot get combobox Substate."; }
                        }
                        else { error = "Cannot verify or invalid value state."; flagExit = false; }
                    }
                    else { error = "Cannot get combobox State."; }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_02_Verify_State_Retired()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_02_Populate_Substate_Donated()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Donated";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Substate value.";
                    }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Donated";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_02_Verify_State_Retired()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_02_Populate_Substate_Sold()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Sold";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Substate value.";
                    }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Sold";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value Substate.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_02_Verify_State_Retired()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_02_Populate_Substate_VendorCredit()
        {
            try
            {
                combobox = hrdAsset.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Vendor credit";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Substate value.";
                    }
                }
                else { error = "Cannot get combobox Substate."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_03_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_04_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "Vendor credit";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_02_Verify_State_Retired()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Retired";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_074_01_SwichToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page (0)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_074_02_Populate_State_Received()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Received";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot populate State value.";
                    }
                    else Thread.Sleep(3000);
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_074_03_Populate_Stockroom()
        {
            try
            {
                lookup = hrdAsset.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Stockroom_Received");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Stockroom value (" + temp + ").";
                    }
                }
                else { error = "Cannot get Stockroom lookup."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_074_04_SaveHardwareAsset()
        {
            try
            {
                flag = hrdAsset.Save();
                if (!flag) { error = "Cannot save Asset."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_074_05_VerifyState_Substate()
        {
            try
            {
                combobox = hrdAsset.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Received";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (flag)
                    {
                        combobox = hrdAsset.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            temp = "-- None --";
                            flag = combobox.VerifyCurrentValue(temp);
                            if (!flag)
                            {
                                error = "Cannot verify or invalid value Substate.";
                                flagExit = false;
                            }
                        }
                        else { error = "Cannot get combobox Substate."; }
                    }
                    else { error = "Cannot verify or invalid value state."; flagExit = false; }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_01_SwichToPage_1()
        {
            try
            {
                flag = Base.SwitchToPage(1);
                if (!flag) { error = "Cannot switch to page (1)."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_02_Verify_State_Received()
        {
            try
            {
                combobox = netGear.Combobox_Status();
                flag = combobox.Existed;
                if (flag)
                {
                    temp = "Received";
                    flag = combobox.VerifyCurrentValue(temp);
                    if (!flag)
                    {
                        error = "Cannot verify or invalid value State.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get combobox State."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_03_Close_Page_1()
        {
            try
            {
                Base.Driver.Close();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_04_SwitchToPage_0()
        {
            try
            {
                flag = Base.SwitchToPage(0);
                if (!flag) { error = "Cannot switch to page 0."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //------------------------------------------------------------------------------------------------

        #endregion State = Retired

        [Test]
        public void Step_076_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
