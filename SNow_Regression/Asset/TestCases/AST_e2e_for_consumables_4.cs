﻿using NUnit.Framework;
using Auto;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;
using SNow;

namespace Asset
{
    [TestFixture]
    public class AST_e2e_for_consumables_4
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Consumable 1's Category: " + Base.GData("Category"));
            System.Console.WriteLine("Finished - Consumable 2's Category: " + Base.GData("Category2"));

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snolookup lookup;
        snocombobox combobox;
        snotextarea textarea;
        snobutton button;
        //------------------------------------------------------------------

        Login login = null;
        Home home = null;
        Itil csAss = null;
        ItilList assList = null;
        ItilList stList = null;
        Itil stockroom = null;

        //------------------------------------------------------------------

        string assetTag, serialNumber;

        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                csAss = new Itil(Base, "Consumable Asset");
                assList = new ItilList(Base, "Asset list");
                stList = new ItilList(Base, "Stockroom List");
                stockroom = new Itil(Base, "Stockroom");
                //------------------------------------------------------------------
                assetTag = string.Empty;
                serialNumber = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #region Login to ServiceNow as an Asset Manager

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_001_Open_System()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");
                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_003_Impersonate_AssetManager()
        {
            try
            {
                string temp = Base.GData("Asset_Manager");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_004_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        #region Create an Asset so users can delete it at the end

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_005_Open_ConsumableList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Consumables");
                if (!flag)
                    error = "Error when open Consumables list.";
                else
                    assList.WaitLoading();
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_006_Click_NewButton()
        {
            try
            {
                flag = assList.Button_New().Click(true);
                if(flag)
                {
                    csAss.WaitLoading();
                }
                else
                {
                    error = "Cannot click on New button";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_007_Select_ModelCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                lookup = csAss.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else { error = "Cannot populate Model Category value."; }
                }
                else
                {
                    error= "Cannot get Model Category lookup.";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_008_Select_Model()
        {
            try
            {
                lookup = csAss.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Model");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        lookup = csAss.Lookup_Manufacturer();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            if (lookup.Text.Equals(""))
                            {
                                flag = false;
                                flagExit = false;
                                error = "Manufacturer was EMPTY.";
                            }
                            else { flag = true; }
                        }
                        else { error = "Cannot get Manufacturer lookup."; }
                    }
                    else { error = "Cannot select item [" + temp + "]"; }
                }
                else { error = "Cannot get Model lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }

        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_009_01_Populate_Company()
        {
            try
            {
                // Input Company
                string temp = Base.GData("Company");
                lookup = csAss.Lookup_Company();
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]"; 
                    }
                }
                else { error = "Cannot get lookup Company."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_009_02_Populate_Location()
        {
            try
            {
                // Input Location
                string temp = Base.GData("Location");
                lookup = csAss.Lookup_Location();
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get Location lookup."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_009_03_Populate_State()
        {
            try
            {               
                string temp = "On Order";
                combobox = csAss.Combobox_State();
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select State [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_009_04_Populate_Comment()
        {
            try
            {
                string temp = Base.GData("Comment");
                textarea = csAss.Textarea_Comments();
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Comment value.";
                    }
                }
                else { error = "Cannot get Comment textarea."; }

            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void PreStep_010_Submit_ConsumableAsset()
        {
            try
            {
                flag = csAss.Save(true);
                if(!flag)
                {
                    error = "Cannot submit Consumables";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        #endregion

        #region Validate Asset Manager can see Asset Management application but not Model Category / Model application

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_006_Verify_AssetManagementModule()
        {
            try
            {
                string[] p_leftitems = null;
                string[] leftitems = null;

                string temp = Base.GData("Nav_Menu");
                if (temp.Contains("|"))
                    p_leftitems = temp.Split('|');
                else
                    p_leftitems = new string[] { temp };

                foreach (string pitem in p_leftitems)
                {
                    bool iFlag = true;
                    if (pitem.Contains("::"))
                    {
                        leftitems = pitem.Split(new string[] { "::" }, StringSplitOptions.RemoveEmptyEntries);
                        iFlag = home.LeftMenuItemValidate(leftitems[0], leftitems[1]);
                        if (flag && !iFlag)
                            flag = false;
                    }
                    else
                    {
                        error = "incorrect format of given data [" + pitem + "]";
                        flag = false;
                    }
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_007_Click_AllAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "All Assets");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("assets");
                    if (!flag)
                    {
                        error = "The displayed page is NOT All Assets list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find All Assets link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_008_Click_HardwareAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Hardware Assets");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("hardware");
                    if (!flag)
                    {
                        error = "The displayed page is NOT Hardware Assets list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Hardware Assets link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_009_Click_Consumables()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Consumables");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("consumables");
                    if (!flag)
                    {
                        error = "The displayed page is NOT Consumables list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Consumables link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_010_Click_OtherAssets()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Other Assets");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("assets");
                    if (!flag)
                    {
                        error = "The displayed page is NOT Other Assets list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Other Assets link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_011_Verify_ModelLink_NotAvailable()
        {
            try
            {
                bool flagTemp;
                flagTemp = home.LeftMenuItemSelect("Asset Management", "Model");
                if (flagTemp)
                {
                    flag = false;
                    flagExit = false;
                    error = "There should NOT be a Model link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        #endregion

        #region Validate filter between Model Category and Model

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_012_Click_Consumables()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Consumables");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("consumables");
                    if (!flag)
                    {
                        error = "The displayed page is NOT Consumables list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Consumables link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_013_014_015_Add_RandomColumn()
        {
            try
            {
                string temp = Base.GData("RandomColumn_Step13");
                flag = assList.Add_More_Columns(temp);
                if (!flag)
                {
                    error = "Cannot add random any column";
                    flagExit = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_016_017_ResetToColumnDefaults()
        {
            try
            {
                flag = assList.ResetColumnToDefaultValue();
                if (!flag)
                {
                    error = "Cannot reset to Column Default";
                    flagExit = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_018_Verify_GotoCombobox()
        {
            try
            {
                flag = assList.VerifyComboboxGoto(Base.GData("DefaultColumnGoto_Step18"), ref error);
                if (!flag)
                {
                    error = "There is something wrong when verifying combobox [Go to]";
                    flagExit = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_019_Update_PersonalizedList()
        {
            try
            {
                string temp = "Asset tag;Alternate Asset Tag;Serial number";
                flag = assList.Add_More_Columns(temp);
                if (!flag)
                {
                    error = "Cannot add [Asset Tag, Alternative Asset Tag and Serial Number]";
                    flagExit = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_020_Click_NewButton()
        {
            try
            {
                flag = assList.Button_New().Click(true);
                if (flag)
                {
                    csAss.WaitLoading();
                }
                else
                {
                    error = "Cannot click on New button";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_021_022_Select_ModelCategory()
        {
            try
            {
                lookup = csAss.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Model Category value.";
                    }
                    else
                    {
                        csAss.WaitLoading();
                    }
                }
                else { error = "Cannot get Model Category lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_023_024_SelectModel()
        {
            try
            {
                lookup = csAss.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Model");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        Thread.Sleep(3000);
                        lookup = csAss.Lookup_Manufacturer();
                        if (flag)
                        {
                            if (lookup.Text.Equals(""))
                            {
                                flag = false;
                                flagExit = false;
                                error = "Manufacturer was EMPTY.";
                            }
                            else { flag = true; }
                        }
                        else { error = "Cannot get Manufacturer lookup."; }
                    }
                    else { error = "Cannot select item [" + temp + "]"; }
                }
                else
                {
                    error = "Cannot get Model lookup.";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_025_Clear_Model_ModelCategory()
        {
            try
            {
                //Clear Model field
                lookup = csAss.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText("", true);
                    if (!flag)
                    {
                        error = "Cannot clear Model value.";
                    }
                    else { csAss.WaitLoading(); }
                }
                else { error = "Cannot get Model lookup."; }


                //Clear Model Category field
                lookup = csAss.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText("", true);
                    if (!flag)
                    {
                        error = "Cannot clear Model Category value.";
                    }
                    else { csAss.WaitLoading(); }
                }
                else { error = "Cannot get Model Category lookup."; }


                //Verify Manufaturer field
                lookup = csAss.Lookup_Manufacturer();
                flag = lookup.Existed;
                if (flag)
                {
                    if (!lookup.Text.Equals(""))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Manufacturer is NOT EMPTY.";
                    }
                    else { flag = true; }
                }
                else { error = "Cannot get Manufacturer lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_026_027_Select_Model2()
        {
            try
            {

                lookup = csAss.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Model2");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        csAss.WaitLoading();
                        Thread.Sleep(3000);

                        lookup = csAss.Lookup_Manufacturer();
                        flag = lookup.Existed;
                        if (flag)
                        {
                            if (lookup.Text.Equals(""))
                            {
                                flag = false;
                                flagExit = false;
                                error = "Manufacturer was EMPTY.";
                            }
                            else { flag = true; }
                        }
                        else
                        {
                            error = "Cannot get Manufacturer lookup.";
                        }

                    }
                    else { error = "Cannot select item [" + temp + "]"; }
                }
                else
                {
                    error = "Cannot get Model lookup.";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_028_029_Select_ModelCategory2()
        {
            try
            {
                lookup = csAss.Lookup_ModelCategory();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Category2");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else { error = "Cannot select Model Category 2 value."; }
                }
                else { error = "Cannot get Model Category lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_01_Populate_Company()
        {
            try
            {
                lookup = csAss.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Company");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Company value.";
                    }
                }
                else { error = "Cannot get Company lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_02_Populate_Location()
        {
            try
            {
                lookup = csAss.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Location");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Location value.";
                    }
                }
                else { error = "Cannot get Location lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_030_03_Populate_State()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "New";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select state [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_031_Populate_Department()
        {
            try
            {
                lookup = csAss.Lookup_Department();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Department");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Department value.";
                    }
                }
                else { error = "Cannot get Department lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_01_Populate_AssignedTo()
        {
            try
            {
                lookup = csAss.Lookup_AssignedTo();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Assignedto");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot populate Assigned To value.";
                    }
                }
                else { error = "Cannot get Assigned To lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_032_02_Verify_Department_Unchanged()
        {
            try
            {
                lookup = csAss.Lookup_Department();
                flag = lookup.Existed;
                if (flag)
                {
                    string department1 = lookup.Text;
                    string deparment2 = Base.GData("Department");
                    flag = department1.Equals(deparment2);
                    if (!flag)
                    {
                        error += "We expect the Department [" + department1 + "] is NOT changed to [" + deparment2 + "]";
                    }
                }
                else { error = "Cannot get Department lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_033_Submit_ConsumablesAsset()
        {
            try
            {
                button = csAss.Button_Submit();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        assList.WaitLoading();
                    }
                    else { error = "Cannot click Submit button."; }
                }
                else { error = "Cannot get Submit button."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        #endregion

        #region Change State Life Cycle from New to On Order

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_01_Click_Consumables()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Consumables");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("consumables");
                    if (!flag)
                    {
                        error = "The displayed page is NOT Consumables list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Consumables link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }
        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_034_02_SearchAndOpen_ConsumablesAsset()
        {
            try
            {
                //Filter the list
                string filter = "Display name;contains;" + Base.GData("Model2") + "|and|Location;is;" + Base.GData("Location") + "|and|Model category;is;" + Base.GData("Category2") + "|and|State;is;New";
                flag = assList.Filter(filter);
                if (flag)
                {
                    assList.WaitLoading();
                    string condition = "Display name=@@" + Base.GData("Model2") + "|State=New|Model category=" + Base.GData("Category2");
                    flag = assList.Open(condition, "Display name");
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot search and open Consumable Asset with condition: " + condition;
                    }
                }
                else
                {
                    error = "Cannot filter the table with condition: " + filter;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_035_Populate_State()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "On order";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select state [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
                flag = csAss.Textarea_Comments().SetText(Base.GData("Comment"));
                if (!flag)
                {
                    error += "Cannot input Comment";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_036_Update_ConsumablesAsset()
        {
            try
            {
                button = csAss.Button_Update();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        assList.WaitLoading();
                    }
                    else { error = "Cannot click Update button."; }
                }
                else { error = "Cannot get Update button."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        #endregion

        #region Change State Life Cycle from Order to Missing

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_037_038_SearchAndOpen_ConsumablesAsset()
        {
            try
            {
                //Filter the list
                string filter = "Display name;contains;" + Base.GData("Model2") + "|and|Location;is;" + Base.GData("Location") + "|and|Model category;is;" + Base.GData("Category2") + "|and|State;is;On order";
                flag = assList.Filter(filter);
                if(flag)
                {
                    assList.WaitLoading();
                    string condition = "Display name=@@" + Base.GData("Model2") + "|State=On order|Model category=" + Base.GData("Category2");
                    flag = assList.Open(condition, "Display name");
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot search and open Consumable Asset with condition: " + condition;
                    }
                }
                else
                {
                    error = "Cannot filter the table with condition: " + filter;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_039_01_Clear_Model_Value()
        {
            try
            {
                lookup = csAss.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.SetText("", true);
                    if (flag)
                    {
                        Thread.Sleep(3000);
                    }
                    else
                    {
                        error = "Cannot clear Model value.";
                        flagExit = false;
                    }
                }
                else { error = "Cannot get Model lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        //[Test]
        public void Step_039_02_Verify_Manufacturer_Value()
        {
            try
            {
                lookup = csAss.Lookup_Manufacturer();
                flag = lookup.Existed;
                if (flag)
                {
                    if (!lookup.Text.Equals(""))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Manufacturer was NOT EMPTY.";
                    }
                    else { flag = true; }
                }
                else { error = "Cannot get Manufacturer textbox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_01_Select_Model3()
        {
            try
            {
                lookup = csAss.Lookup_Model();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Model3");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select Model [" + temp + "]";
                    }
                    else
                    {
                        Thread.Sleep(2000);
                    }
                }
                else { error = "Cannot get Model lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_040_02_Verify_Manufacturer_Value()
        {
            try
            {
                lookup = csAss.Lookup_Manufacturer();
                flag = lookup.Existed;
                if (flag)
                {
                    if (lookup.Text.Equals(""))
                    {
                        flag = false;
                        flagExit = false;
                        error = "Manufacturer was EMPTY.";
                    }
                    else { flag = true; }
                }
                else { error = "Cannot get Manufacturer textbox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_01_Populate_Reservedfor()
        {
            try
            {
                flag = csAss.Save(true);
                if (!flag)
                {
                    error = "Cannot save the Asset";
                }

                lookup = csAss.Lookup_ReservedFor();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("Reservedfor");
                    flag = lookup.Select(temp);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot populate Reserved For value.";
                    }
                }
                else { error = "Cannot get Reserved For lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_041_02_Save_Asset()
        {
            try
            {
                flag = csAss.Save(true);
                if (!flag)
                {
                    error = "Cannot save the Asset";
                }
                else { csAss.WaitLoading(); }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_042_ChangeState_InStock()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "In stock";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_043_Save_And_VerifyMessage()
        {
            try
            {
                flag = csAss.Save(true);
                if (!flag)
                {
                    string temp = "The following mandatory fields are not filled in: Stockroom";
                    flag = csAss.Verify_ExpectedErrorMessages_Existed(temp);
                    if (!flag)
                    {
                        error = "The error message is not as expected. Expected: " + temp;
                    }
                }
                else { error = "Cannot save Asset"; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_01_Edit_StockRoom()
        {
            try
            {
                lookup = csAss.Lookup_Stockroom();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("StockRoom");
                    flag = lookup.Select(temp);
                    if (!flag)
                    {
                        error = "Cannot select stockroom [" + temp + "]";
                    }
                    else { Thread.Sleep(2000); }
                }
                else { error = "Cannot get Stockroom lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        //[Test]
        public void Step_044_02_Populate_Substate()
        {
            try
            {
                combobox = csAss.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Available";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item [" + temp + "]";
                    }
                    else { csAss.WaitLoading(); }
                }
                else { error = "Cannot get Substate combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_03_Verify_StockroomLocation_Generated()
        {
            try
            {
                lookup = csAss.Lookup_Location();
                flag = lookup.Existed;
                if (flag)
                {
                    string temp = Base.GData("StockRoom_Location");
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "The Location is populated not as expected. Expected: " + temp;
                    }
                }
                else { error = "Cannot get Location lookup."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_044_04_Save_Asset()
        {
            try
            {
                flag = csAss.Save(true);
                if (!flag)
                {
                    error = "Cannot save the Asset.";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_045_Open_StockRoomListPage()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Stock", Base.GData("StockroomMenu"));
                if (flag)
                {
                    flag = stList.List_Title().MyText.ToLower().Trim().Equals("stockrooms");
                    if (!flag)
                    {
                        error = "Cannot click on Stockrooms menu link";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Cannot select Stockroom from Left Menu";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_01_Search_StockRoom()
        {
            try
            {
                flag = stList.SearchAndOpen("Name", "=" + Base.GData("StockRoom"), "Name=" + Base.GData("StockRoom"), "Name");
                if (flag)
                {
                    stockroom.WaitLoading();
                }
                else
                {
                    error = "Cannot search Stockroom by Name";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_046_02_Verify_Consumable()
        {
            try
            {
                string condition = "Display name=@@" + Base.GData("Model3") + "|Substate=Available|State=In stock|Model category=" + Base.GData("Category2");
                flag = stockroom.Verify_RelatedTable_Row("Consumables", condition);
                if (!flag)
                {
                    error = "Cannot find Consumable";
                    flagExit = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_047_Back_ConsumableAsset()
        {
            try
            {
                string condition = "Display name=@@" + Base.GData("Model3") + "|State=In stock|Model category=" + Base.GData("Category2");
                flag = stockroom.RelatedTableOpenRecord("Consumables", condition, "Display name");
                if (flag)
                {
                    csAss.WaitLoading();
                }
                else
                {
                    error = "Cannot find Consumable";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_048_ChangeSubState_Assembled()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Assembled");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Assembled";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_049_ChangeSubState_Reserved()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Reserved");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Reserved";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_050_ChangeSubState_Defective()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Defective");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Defective";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_051_ChangeSubState_PendingRepair()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending repair");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending repair";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_052_ChangeSubState_PendingInstall()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending install");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending install";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_053_ChangeSubState_PendingDisposal()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending disposal");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending disposal";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_054_ChangeSubState_PendingTransfer()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending transfer");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending transfer";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }



        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_055_01_ChangeSubState_PreAllocated()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pre-allocated");
                if (flag)
                {
                    csAss.WaitLoading();
                }
                else
                {
                    error = "Cannot select SubState = Pre-allocated";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_055_02_Verify_MessageError()
        {
            try
            {
                flag = csAss.Verify_Field_MessageInfo(Base.GData("FieldError_Step50"));
                if (flag)
                {
                    flag = csAss.Combobox_Substate().VerifyCurrentValue("-- None --");
                    if (!flag)
                    {
                        error = "The Substate value is NOT changed to [-- None --]";
                    }
                }
                else
                {
                    error = "There is no field error message bellow Substate field";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_056_ChangeState_InTransit()
        {
            try
            {
                string temp = "In Transit";
                flag = csAss.Combobox_State().SelectItem(temp);
                if (flag)
                {
                    csAss.WaitLoading();
                }
                else
                {
                    error = "Cannot select State = " + temp;
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_057_ChangeSubState_Available()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Available");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Available";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_058_ChangeSubState_Assembled()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Assembled");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Assembled";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_059_ChangeSubState_Reserved()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Reserved");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Reserved";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_060_ChangeSubState_Defective()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Defective");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Defective";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_061_ChangeSubState_PendingInstall()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending install");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending install";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_062_ChangeSubState_PendingTransfer()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending transfer");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending transfer";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_063_ChangeSubState_PendingRepair()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending repair");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending repair";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_064_ChangeSubState_PendingDisposal()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pending disposal");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Save(true);
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot save Asset";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pending disposal";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_065_ChangeSubState_PreAllocated()
        {
            try
            {
                flag = csAss.Combobox_Substate().SelectItem("Pre-allocated");
                if (flag)
                {
                    csAss.WaitLoading();

                    flag = csAss.Verify_Field_MessageInfo(Base.GData("FieldError_Step50"));
                    if (flag)
                    {
                        flag = csAss.Combobox_Substate().VerifyCurrentValue("-- None --");
                        if (!flag)
                        {
                            error = "The Substate value is NOT changed to [-- None --]";
                        }
                    }
                    else
                    {
                        error = "There is no field error message bellow Substate field";
                    }
                }
                else
                {
                    error = "Cannot select SubState = Pre-allocated";
                }
                if (!flag) { flagExit = false; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_066_ChangeState_InMaintenance()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string item = "In Maintenance";
                    flag = combobox.SelectItem(item);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        flag = csAss.Save(true);
                        if (!flag)
                        {
                            error = "Cannot save Asset.";
                        }
                    }
                    else { error = "Cannot select item [" + item + "]"; }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_01_ChangeState_Retired()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string item = "Retired";
                    flag = combobox.SelectItem(item);
                    if (!flag)
                    {
                        error = "Cannot select item [" + item + "]";       
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_067_02_Save_And_VerifyErrorMessage()
        {
            try
            {
                flag = csAss.Save(true);
                if (!flag)
                {
                    string temp = "The following mandatory fields are not filled in: Substate, Retired date";
                    flag = csAss.Verify_ExpectedErrorMessages_Existed(temp);
                    if (!flag)
                    {
                        error = "The error message is not as expected. Expected: " + temp;
                    }
                }
                else { error = "Cannot save Asset"; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_068_Populate_RetiredDate()
        {
            try
            {

                flag = csAss.Select_Tab("Disposal");
                if (flag)
                {
                    snodate date = csAss.Date_RetiredDate();
                    flag = date.Existed;
                    if (flag)
                    {
                        flag = date.SetText(DateTime.Today.ToString("yyyy-MM-dd"), true);
                        if (!flag)
                        {
                            error = "Cannot populate Retired Date value.";
                        }
                    }
                    else { error = "Cannot get Retired Date datetime."; }
                }
                else
                {
                    error = "Cannot open Disposal tab";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_069_SwitchTo_GeneralTab()
        {
            try
            {
                flag = csAss.Select_Tab("General");
                if (!flag)
                {
                    error = "Cannot open General tab";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_070_ChangeSubstate_Sold()
        {
            try
            {
                combobox = csAss.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string item = "Sold";
                    flag = combobox.SelectItem(item);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        flag = csAss.Save(true);
                        if (!flag)
                        {
                            error = "Cannot save Asset.";
                        }
                    }
                    else { error = "Cannot select item [" + item + "]"; }
                }
                else { error = "Cannot get SubState combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_071_ChangeSubstate_Donated()
        {
            try
            {
                combobox = csAss.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string item = "Donated";
                    flag = combobox.SelectItem(item);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        flag = csAss.Save(true);
                        if (!flag)
                        {
                            error = "Cannot save Asset.";
                        }
                    }
                    else { error = "Cannot select item [" + item + "]"; }
                }
                else { error = "Cannot get SubState combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_072_ChangeSubstate_VendorCredit()
        {
            try
            {
                combobox = csAss.Combobox_Substate();
                flag = combobox.Existed;
                if (flag)
                {
                    string item = "Vendor Credit";
                    flag = combobox.SelectItem(item);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        flag = csAss.Save(true);
                        if (!flag)
                        {
                            error = "Cannot save Asset.";
                        }
                    }
                    else { error = "Cannot select item [" + item + "]"; }
                }
                else { error = "Cannot get SubState combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_02_ChangeState_Missing_SubState_Lost()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string item = "Missing";
                    flag = combobox.SelectItem(item);
                    if (flag)
                    {
                        Thread.Sleep(2000);
                        combobox = csAss.Combobox_Substate();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            item = "Lost";
                            flag = combobox.SelectItem(item);
                            if (flag)
                            {
                                Thread.Sleep(2000);
                                flag = csAss.Save(true);
                                if (!flag)
                                {
                                    error = "Cannot save Asset.";
                                }
                            }
                            else { error = "Cannot select item [" + item + "]"; }
                        }
                        else { error = "Cannot get SubState combobox."; }
                    }
                    else { error = "Cannot select item [" + item + "]"; }
                }
                else { error = "Cannot get SubState combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }


        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_01_ChangeState_Consumed()
        {
            try
            {
                combobox = csAss.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    string temp = "Consumed";
                    flag = combobox.SelectItem(temp);
                    if (!flag)
                    {
                        error = "Cannot select item[" + temp + "]";
                    }
                    else
                    {
                        Thread.Sleep(4000);
                    }
                }
                else { error = "Cannot get State combobox."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_073_03_Save_Asset()
        {
            try
            {
                flag = csAss.Save(true);
                if (flag)
                {
                    csAss.WaitLoading();
                }
                else
                {
                    error = "Cannot save Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        #endregion

        #region Delete Consumable Asset

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_074_Open_ConsumableList()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Asset Management", "Consumables");
                if (flag)
                {
                    assList.WaitLoading();

                    flag = assList.List_Title().MyText.ToLower().Trim().Equals("consumables");
                    if (!flag)
                    {
                        error = "The displayed page is NOT Consumables list page";
                        flagExit = false;
                    }
                }
                else
                {
                    error = "Can't find Consumables link";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_075_076_SearchAndOpen_Precon_ConsumablesAsset()
        {
            try
            {
                 //Filter the list
                string filter = "Display name;contains;" + Base.GData("Model") + "|and|Location;is;" + Base.GData("Location") + "|and|Model category;is;" + Base.GData("Category") + "|and|State;is;On order";
                flag = assList.Filter(filter);
                if (flag)
                {
                    string condition = "Display name=@@" + Base.GData("Model") + "|State=On order|Model category=" + Base.GData("Category");
                    flag = assList.Open(condition, "Display name");
                    if (flag)
                    {
                        csAss.WaitLoading();
                    }
                    else
                    {
                        error = "Cannot search and open Consumable Asset";
                    }
                }
                else
                {
                    error = "Cannot filter the table";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_077_078_Delete_ConsumableAsset_ClickCancel()
        {
            try
            {
                button = csAss.Button_Delete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        flag = csAss.VerifyConfirmDialog(Base.GData("ErrorMessage_Step77"), "no");
                        if (!flag)
                        {
                            error = "Cannot close Confirm dialog";
                        }
                    }
                    else { error = "Cannot click on Delete button"; }
                }
                else { error = "Cannot get Delete button."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_079_Delete_ConsumableAsset_ClickOK()
        {
            try
            {
                button = csAss.Button_Delete();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click(true);
                    if (flag)
                    {
                        flag = csAss.VerifyConfirmDialog(Base.GData("ErrorMessage_Step77"), "yes");
                        if (!flag)
                        {
                            error = "Cannot close Confirm dialog";
                        }
                    }
                    else { error = "Cannot click on Delete button"; }
                }
                else { error = "Cannot get Delete button."; }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        #endregion

        #region Personate as Non Asset Manager and check this user can't see Asset and can't do anything

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_080_Impersonate_ServiceDesk()
        {
            try
            {
                flag = home.ImpersonateUser(Base.GData("Service_Desk"), true, Base.GData("UserFullName"));
                if (!flag)
                {
                    error = "Cannot impersonate Service Desk";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_081_082_System_Setting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------
        [Test]
        public void Step_083_Check_AssetManagement_NotDisplay()
        {
            try
            {
                bool flagTemp = home.LeftMenuItemValidate("Asset Management", "Asset Management;Model");
                if (flagTemp)
                {
                    flag = false;
                    flagExit = false;
                    error = "Service Desk can find Asset";
                }
            }
            catch (Exception e)
            {
                flag = false;
                error = e.Message;
            }
        }

        //-------------------------------------------------------------------------------------------------

        [Test]
        public void Step_084_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion

        //***********************************************************************************************************************************
        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
