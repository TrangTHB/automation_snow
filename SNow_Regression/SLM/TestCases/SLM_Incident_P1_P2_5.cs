﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Reflection;
using System.Threading;
using SNow;
using OpenQA.Selenium.Interactions;

namespace SLM
{
    [TestFixture]
    public class SLM_Incident_P1_P2_5
    {
        #region Define default variables for test case (No need to update)
        //***********************************************************************************************************************************
        public bool flagC;
        public bool flag, flagExit, flagW;
        string caseName, error;
        SNow.snobase Base;

        //***********************************************************************************************************************************
        #endregion End - Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)
        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }
        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }
        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }
        //***********************************************************************************************************************************
        #endregion End - Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)
        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id: " + incidentId);
            
            System.Console.WriteLine("Finished - SLA start time: " + starttime);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }
        //***********************************************************************************************************************************
        #endregion End - Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)
        //***********************************************************************************************************************************

        snotextbox textbox;
        snolookup lookup;
        snocombobox combobox;
        snotextarea textarea;
        snocheckbox checkbox;
        snodatetime datetime;
        snobutton button;
        //------------------------------------------------------------------
        Login login = null;
        Home home = null;
        SNow.Incident inc = null;
        Change chg = null;
        Problem prb = null;
        KnowledgeSearch knls = null;
        IncidentList inclist = null;
        EmailList emailList = null;
        Member member = null;
        //------------------------------------------------------------------
        string incidentId;
        
        string starttime = string.Empty;
        //***********************************************************************************************************************************
        #endregion End - Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)
        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                chg = new Change(Base, "Change");
                prb = new Problem(Base, "Problem");
                knls = new KnowledgeSearch(Base);
                inclist = new IncidentList(Base, "Incident list");
                emailList = new EmailList(Base, "Email list");
                member = new Member(Base);
                //------------------------------------------------------------------
                incidentId = string.Empty;
                

            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("SDA1");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id 1:(" + incidentId + ")");
                    string temp = Base.GData("Caller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_03_Verify_CallerEmail()
        {
            try
            {
                string temp = Base.GData("CallerEmail");
                textbox = inc.Textbox_Email();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid caller email or the value is not auto populate."; flagExit = false; }
                }
                else
                    error = "Cannot get caller email.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("Category");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("Subcategory");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }

                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_01_Update_Impact_Urgency_1_Critical()
        {
            try
            {
                string temp = "1 - Critical";
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                        combobox = inc.Combobox_Urgency();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                inc.WaitLoading();
                            }
                            else { error = "Cannot populate urgency value"; }
                        }
                        else { error = "Cannot get combobox urgency"; }
                    }
                    else { error = "Cannot populate impact value."; }
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_02_Verify_Priority_1_Critical()
        {
            try
            {
                string temp = "1 - Critical";
                combobox = inc.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp, true);
                    
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_01_PopulateContactType()
        {
            try
            {
                string temp = Base.GData("ContactType");
                combobox = inc.Combobox_ContactType();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot populate contact type value."; }
                }
                else
                    error = "Cannot get combobox contact type.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_02_PopulateAssignmentGroup_RG()
        {
            try
            {
                string temp = Base.GData("ResolverGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_PopulateShortDescription()
        {
            try
            {
                string temp = Base.GData("ShortDescription_P1");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_PopulateDescription()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Change_State_To_Active()
        {
            try
            {
                string temp = "Active";
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (!flag) { error = "Cannot add State."; }
                }
                else
                    error = "Cannot get combobox State.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_SaveIncidentAndVerifyAlert()
        {
            try
            {
                //Save Incident and Verify alert message
                flag = inc.Save(false, true);
                if (flag)
                {
                    try
                    {
                        IAlert alert = Base.Driver.SwitchTo().Alert();
                        string temp = "This Incident Record is now a Major Incident!Please contact your Service Desk to make sure it gets the attention it needs.";
                        string runtime = alert.Text.Replace("\r\n", "");
                        if (!runtime.Equals(temp))
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid alert message. Runtime:(" + runtime + "). Expexted:(" + temp + ")";
                        }
                        alert.Accept();
                        Thread.Sleep(10000);
                        inc.WaitLoading();
                    }
                    catch
                    {
                        flag = false;
                        flagExit = false;
                        error = "Alert is not visible.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Verify_SLAs_13()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_13");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_Verify_SLAs_Start_Time_14()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_StartTime_14");
                flag = inc.Select_Tab("Notes");
                if (flag)
                {
                    starttime = inc.Get_Activity_Date(0);
                    string[] array = null;
                    if (slas.Contains(";"))
                        array = slas.Split(';');
                    else
                        array = new string[] { slas };
                    string temp = string.Empty;
                    int count = 1;
                    foreach (string str in array)
                    {
                        if (count < array.Length)
                            temp = temp + str + "|Start time=" + starttime + ";";
                        else
                            temp = temp + str + "|Start time=" + starttime;
                        count++;
                    }

                    flag = inc.VerifySLAs(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid start time slas.";
                    }
                }
                else error = "Cannot select tab.";
                
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Open_SLA_Response_P1()
        {
            try
            {
                string sla = Base.GData("SLA_Response_P1");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA="+sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Verify_Task_SLA_StartTime()
        {
            try
            {
                //--Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (starttime == null || starttime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input start time 1.");
                    addPara.ShowDialog();
                    starttime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                datetime = inc.Datetime_StartTime();
                flag = datetime.VerifyCurrentValue(starttime, true);
                if (!flag)
                    error = "Invalid start time value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Back_To_Incident()
        {
            try
            {
                Base.Driver.Navigate().Back();
                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Check_Checkbox_Responded()
        {
            try
            {
                checkbox = inc.Checkbox_Responded();
                flag = checkbox.Existed;
                if (flag)
                {
                    if (!checkbox.Checked)
                        flag = checkbox.Click();
                }
                else error = "Cannot get checkbox responded.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Verify_SLAs_20()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_20");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Open_SLA_Response_P1()
        {
            try
            {
                string sla = Base.GData("SLA_Response_P1");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA=" + sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Verify_Task_SLA_StopTime()
        {
            try
            {
                datetime = inc.Datetime_StopTime();
                if (datetime.Text.Trim() == "")
                {
                    flag = false;
                    error = "Stop time is not auto populate.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Back_To_Incident()
        {
            try
            {
                Base.Driver.Navigate().Back();
                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Change_Incident_State_To_AwaitingCustomer()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Customer");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_Populate_FollowUpDate()
        {
            try
            {
                button = inc.Button_FollowUpDate_Calendar();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        button = inc.Button_Calendar_Select();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                datetime = inc.Datetime_Followupdate();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    string followup = datetime.Text.Trim();
                                    followup = Convert.ToDateTime(followup).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                                    flag = datetime.SetText(followup, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Verify_SLAs_27()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_27");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_Change_Incident_State_To_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Verify_SLAs_30()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_30");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Change_Impact_To_2High()
        {
            try
            {
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("2 - High");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate impact value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Change_Urgency_To_2High()
        {
            try
            {
                combobox = inc.Combobox_Urgency();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("2 - High");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate impact value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_SaveIncidentAndVerifyAlert()
        {
            try
            {
                //Save Incident and Verify alert message
                flag = inc.Save(false, true);
                if (flag)
                {
                    try
                    {
                        IAlert alert = Base.Driver.SwitchTo().Alert();
                        string temp = "This Incident Record is now a Major Incident!Please contact your Service Desk to make sure it gets the attention it needs.";
                        string runtime = alert.Text.Replace("\r\n", "");
                        if (!runtime.Equals(temp))
                        {
                            flag = false;
                            flagExit = false;
                            error = "Invalid alert message. Runtime:(" + runtime + "). Expexted:(" + temp + ")";
                        }
                        alert.Accept();
                        Thread.Sleep(10000);
                        inc.WaitLoading();
                    }
                    catch
                    {
                        flag = false;
                        flagExit = false;
                        error = "Alert is not visible.";
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Verify_SLAs_34()
        {
            try
            {
               
                string slas = Base.GData("Verify_SLAs_34");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Open_SLA_Resolution_P2()
        {
            try
            {
                string sla = Base.GData("SLA_Resolution_P2");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA=" + sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Verify_Task_SLA_StartTime()
        {
            try
            {
                //--Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (starttime == null || starttime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input start time 1.");
                    addPara.ShowDialog();
                    starttime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                datetime = inc.Datetime_StartTime();
                flag = datetime.VerifyCurrentValue(starttime, true);
                if (!flag)
                    error = "Invalid start time value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Back_To_Incident()
        {
            try
            {
                Base.Driver.Navigate().Back();
                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Change_Incident_State_To_AwaitingProblem()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Problem");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Verify_SLAs_40()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_40");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_Change_Incident_State_To_AwaitingCustomer()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Customer");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_Verify_SLAs_43()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_43");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Change_Incident_State_To_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_Verify_SLAs_46()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_46");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_Change_Incident_State_To_AwaitingEvidence()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Evidence");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_Verify_SLAs_49()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_49");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Change_Incident_State_To_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_051_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_052_Verify_SLAs_52()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_52");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_053_Change_Incident_State_To_Resolved()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Resolved");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_054_PopulateCloseCodeAndCloseNotes()
        {
            try
            {


                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Solved (Work Around)";
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = "Test Close Note";
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_055_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_056_ImpersonateUser_Inc_Manager()
        {
            try
            {
                string temp = Base.GData("Inc_Manager");
                string rootuser = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp, true, rootuser);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_057_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_058_Search_Open_Incident()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId == null || incidentId == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                
                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    temp = inclist.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = inclist.SearchAndOpen("Number", incidentId, "Number=" + incidentId, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_059_Change_Incident_State_To_Closed()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_060_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_061_Verify_SLAs_61()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_61");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_062_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        #endregion End - Scenario of test case (NEED TO UPDATE)
    }
}
