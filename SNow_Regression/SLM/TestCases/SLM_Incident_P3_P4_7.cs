﻿using NUnit.Framework;
using OpenQA.Selenium;
using SNow;
using System;
using System.Reflection;
using System.Threading;

namespace SLM
{
    [TestFixture]
    public class SLM_Incident_P3_P4_7
    {
        #region Define default variables for test case (No need to update)

        //***********************************************************************************************************************************
        public bool flagC;

        public bool flag, flagExit, flagW;
        private string caseName, error;
        private SNow.snobase Base;

        //***********************************************************************************************************************************

        #endregion Define default variables for test case (No need to update)

        #region Setup test case, set up and tear down test steps (No need to update)

        //***********************************************************************************************************************************
        [TestFixtureSetUp]
        public void Setup()
        {
            caseName = MethodBase.GetCurrentMethod().DeclaringType.Name;
            Base = new SNow.snobase();
            Base.SNBeforeRunTestCase(caseName, ref Base, ref flagExit, ref flagW, ref flag, ref flagC);
        }

        //-------------------------------------------------------------------------------------------------
        [SetUp]
        public void RunBeforeAnyTests()
        {
            Base.BeforeRunTestStep(ref flag, ref flagExit, ref error);
        }

        //-------------------------------------------------------------------------------------------------
        [TearDown]
        public void RunAfterAnyTests()
        {
            Base.AfterRunTestStep(flag, ref flagExit, ref flagW, ref flagC, error);
        }

        //***********************************************************************************************************************************

        #endregion Setup test case, set up and tear down test steps (No need to update)

        #region Tear down test case (NEED TO UPDATE: write result)

        //***********************************************************************************************************************************
        [TestFixtureTearDown()]
        public void TearDown()
        {
            Base.AfterRunTestCase(flagC, caseName);

            System.Console.WriteLine("Finished - Incident Id 1: " + incidentId1);
            System.Console.WriteLine("Finished - SLA start time: " + starttime);

            //----------------------------------------------------------------

            string temp = Base.GData("Debug").ToLower();

            if (Base.Driver != null && temp != "yes")
            {
                Base.Driver.Close();
                Base.Driver.Quit();
            }
        }

        //***********************************************************************************************************************************

        #endregion Tear down test case (NEED TO UPDATE: write result)

        #region Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        //***********************************************************************************************************************************

        private snotextbox textbox;
        private snolookup lookup;
        private snocombobox combobox;
        private snotextarea textarea;
        private snocheckbox checkbox;
        private snodatetime datetime;
        private snobutton button;
        //------------------------------------------------------------------
        private Login login = null;

        private Home home = null;
        private SNow.Incident inc = null;
        private Change chg = null;
        private Problem prb = null;
        private KnowledgeSearch knls = null;
        private IncidentList inclist = null;
        private EmailList emailList = null;
        private Member member = null;

        //------------------------------------------------------------------
        private string incidentId1, incidentId2, incidentId3;

        private string incidentAlert1, incidentAlert2, incidentAlert3, incidentAlert4;
        private string changeId, problemId;

        private string starttime = string.Empty;
        //***********************************************************************************************************************************

        #endregion Define variables and objects (class) are used in test cases (NEED TO UPDATE: This case variables)

        #region Scenario of test case (NEED TO UPDATE)

        //***********************************************************************************************************************************

        [Test]
        public void ClassInit()
        {
            try
            {
                //------------------------------------------------------------------
                login = new Login(Base);
                home = new Home(Base);
                inc = new SNow.Incident(Base, "Incident");
                chg = new Change(Base, "Change");
                prb = new Problem(Base, "Problem");
                knls = new KnowledgeSearch(Base);
                inclist = new IncidentList(Base, "Incident list");
                emailList = new EmailList(Base, "Email list");
                member = new Member(Base);
                //------------------------------------------------------------------
                incidentId1 = string.Empty;
                incidentId2 = string.Empty;
                incidentId3 = string.Empty;
                incidentAlert1 = string.Empty;
                incidentAlert2 = string.Empty;
                incidentAlert3 = string.Empty;
                incidentAlert4 = string.Empty;
                changeId = string.Empty;
                problemId = string.Empty;
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_001_OpenSystem()
        {
            try
            {
                Base.Driver.Navigate().GoToUrl(Base.GData("Url"));
                login.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_002_Login()
        {
            try
            {
                string user = Base.GData("User");
                string pwd = Base.GData("Pwd");

                flag = login.LoginToSystem(user, pwd);

                if (flag)
                {
                    home.WaitLoading();
                }
                else
                {
                    error = "Cannot login to system.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_003_ImpersonateUser_SDA1()
        {
            try
            {
                string temp = Base.GData("ServiceDesk");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_004_SystemSetting()
        {
            try
            {
                flag = home.SystemSetting();
                if (!flag) { error = "Error when config system."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_005_OpenNewIncident()
        {
            try
            {
                flag = home.LeftMenuItemSelect("Incident", "Create New");
                if (flag)
                    inc.WaitLoading();
                else
                    error = "Error when create new incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_01_PopulateCallerName()
        {
            try
            {
                textbox = inc.Textbox_Number();
                flag = textbox.Existed;
                if (flag)
                {
                    textbox.Click();
                    //-- Store incident id
                    incidentId1 = textbox.Text;
                    Console.WriteLine("-*-[Store]: Incident Id 1:(" + incidentId1 + ")");
                    string temp = Base.GData("Caller");
                    lookup = inc.Lookup_Caller();
                    flag = lookup.Existed;
                    if (flag)
                    {
                        flag = lookup.Select(temp);
                        if (!flag) { error = "Cannot populate caller value."; }
                    }
                    else { error = "Cannot get lookup caller."; }
                }
                else
                {
                    error = "Cannot get texbox number.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_006_02_Verify_Company()
        {
            try
            {
                string temp = Base.GData("Company");
                lookup = inc.Lookup_Company();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.VerifyCurrentValue(temp, true);
                    if (!flag) { error = "Invalid company value or the value is not auto populate."; flagExit = false; }
                }
                else { error = "Cannot get lookup company."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_01_PopulateCategory()
        {
            try
            {
                string temp = Base.GData("IncCat");
                combobox = inc.Combobox_Category();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate category value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_007_02_PopulateSubCategory()
        {
            try
            {
                string temp = Base.GData("IncSubCat");
                combobox = inc.Combobox_Subcategory();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate sub category value."; }
                }
                else
                {
                    error = "Cannot get combobox sub category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_008_Verify_Priority_3_Medium()
        {
            try
            {
                string temp = "3 - Medium";
                combobox = inc.Combobox_Priority();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.VerifyCurrentValue(temp, true);
                }
                else { error = "Cannot get combobox priority."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_009_Populate_Assignment_Group()
        {
            try
            {
                string temp = Base.GData("ResolverGroup");
                lookup = inc.Lookup_AssignmentGroup();
                flag = lookup.Existed;
                if (flag)
                {
                    flag = lookup.Select(temp);
                    if (!flag) { error = "Cannot populate assignment group value."; }
                }
                else { error = "Cannot get lookup assignment group."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_01_Populate_Short_Description()
        {
            try
            {
                string temp = Base.GData("IncShortDescription");
                textbox = inc.Textbox_ShortDescription();
                flag = textbox.Existed;
                if (flag)
                {
                    flag = textbox.SetText(temp);
                    if (!flag) { error = "Cannot populate short description value."; }
                }
                else { error = "Cannot get textbox short description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_010_02_Populate_Description()
        {
            try
            {
                string temp = "Auto test description";
                textarea = inc.Textarea_Description();
                flag = textarea.Existed;
                if (flag)
                {
                    flag = textarea.SetText(temp);
                    if (!flag) { error = "Cannot populate description value."; }
                }
                else { error = "Cannot get textarea description."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_011_Save_Incident()
        {
            try
            {
                flag = inc.Save(true);
                if (flag)
                {
                    Thread.Sleep(2000);
                    inc.WaitLoading();
                    incidentId1 = inc.Textbox_Number().Text;
                }
                else { error = "Cannot save INCIDENT."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        [Test]
        public void Step_012_01_Expose_columns_of_related_list_SLA()
        {
            try
            {
                string col = "Task|SLA|Stage|Start time";
                flag = inclist.Add_More_Columns(col);
                Thread.Sleep(2000);
                if (!flag)
                {
                    error = "CANNOT add 2 columns: SLA and Stage";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_012_02_Verify_SLAs_12()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_12");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_013_Verify_SLAs_Start_Time_13()
        {
            try
            {
                //Verify SLA Start times of both SLAs (Response and Resolution)
                string slas = Base.GData("Verify_SLAs_StartTime_13");
                flag = inc.Select_Tab("Notes");
                if (flag)
                {
                    starttime = inc.Get_Activity_Date(0);
                    string[] array = null;
                    if (slas.Contains(";"))
                        array = slas.Split(';');
                    else
                        array = new string[] { slas };
                    string temp = string.Empty;
                    int count = 1;
                    foreach (string str in array)
                    {
                        if (count < array.Length)
                            temp = temp + str + "|Start time=" + starttime + ";";
                        else
                            temp = temp + str + "|Start time=" + starttime;
                        count++;
                    }

                    flag = inc.VerifySLAs(temp);
                    if (!flag)
                    {
                        flagExit = false;
                        error = "Invalid start time slas.";
                    }
                }
                else error = "Cannot select tab.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_01_Open_SLAs_P3_Response()
        {
            try
            {
                string sla = Base.GData("SLA_Response_P3");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA=" + sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_014_02_Verify_SLA_Start_time()
        {
            try
            {
                //--Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (starttime == null || starttime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input start time 1.");
                    addPara.ShowDialog();
                    starttime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                datetime = inc.Datetime_StartTime();
                flag = datetime.VerifyCurrentValue(starttime, true);
                if (!flag)
                    error = "Invalid start time value.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_015_Back_To_Incident()
        {
            try
            {
                Base.Driver.Navigate().Back();
                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_016_Update_Impact_Urgency_4_Low()
        {
            try
            {
                string temp = "4 - Low";
                combobox = inc.Combobox_Impact();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem(temp);
                    if (flag)
                    {
                        inc.WaitLoading();
                        combobox = inc.Combobox_Urgency();
                        flag = combobox.Existed;
                        if (flag)
                        {
                            flag = combobox.SelectItem(temp);
                            if (flag)
                            {
                                flag = inc.Save();
                                if (flag) { inc.WaitLoading(); }
                                else error = "Cannot save INCIDENT";
                            }
                            else { error = "Cannot populate urgency value"; }
                        }
                        else { error = "Cannot get combobox urgency"; }
                    }
                    else { error = "Cannot populate impact value."; }
                }
                else { error = "Cannot get combobox impact."; }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_017_Verify_SLAs_P4_17()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_17");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid SLA.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_018_Verify_SLAs_P3_Resolution_Cancelled()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId1 == null || incidentId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input #ID of Incident.");
                    addPara.ShowDialog();
                    incidentId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string slaP3Reso = Base.GData("SLA_Resolution_P3");
                //condition is : looking for a row in tab 'Task SLAs' that the SLA P3 will have Stage = Cancelled. Stage is hardcoded
                string conditions = "Task=" + incidentId1 + "|" + "SLA=" + slaP3Reso + "|" + "Stage=Cancelled";

                flag = inc.Verify_RelatedTable_Row("Task SLAs", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "The SLA is NOT updated to Cancelled or the SLA's name is NOT correct.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_019_Verify_SLAs_P3_Response_Cancelled()
        {
            try
            {
                //-- Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId1 == null || incidentId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input #ID of Incident.");
                    addPara.ShowDialog();
                    incidentId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                string slaP3Resp = Base.GData("SLA_Response_P3");
                //condition is : looking for a row in tab 'Task SLAs' that the SLA P3 will have Stage = Cancelled. Stage is hardcoded
                string conditions = "Task=" + incidentId1 + "|" + "SLA=" + slaP3Resp + "|" + "Stage=Cancelled";

                flag = inc.Verify_RelatedTable_Row("Task SLAs", conditions);
                if (!flag)
                {
                    flagExit = false;
                    error = "The SLA is NOT updated to Cancelled or the SLA's name is NOT correct.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_020_Open_SLA_Response_P3()
        {
            try
            {
                string sla = Base.GData("SLA_Response_P3");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA=" + sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_021_Verify_Task_SLA_StopTime()
        {
            try
            {
                datetime = inc.Datetime_StopTime();
                if (datetime.Text.Trim() == "")
                {
                    flag = false;
                    error = "Stop time is not auto populate.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_022_Back_To_Incident()
        {
            try
            {
                Base.Driver.Navigate().Back();
                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_023_Verify_SLAs_Start_Time_P4_23()
        {
            try
            {
                //--Input information
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (starttime == null || starttime == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input start time 1.");
                    addPara.ShowDialog();
                    starttime = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------
                //Verify SLA Start times of both SLAs (Response and Resolution)
                string slas = Base.GData("Verify_SLAs_StartTime_23");
                string[] array = null;

                if (slas.Contains(";"))
                    array = slas.Split(';');
                else
                    array = new string[] { slas };
                temp = string.Empty;
                int count = 1;
                foreach (string str in array)
                {
                    if (count < array.Length)
                        temp = temp + str + "|Start time=" + starttime + ";";
                    else
                        temp = temp + str + "|Start time=" + starttime;
                    count++;
                }

                flag = inc.VerifySLAs(temp);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid start time slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_024_Populate_Assigned_to_field()
        {
            try
            {
                Base.Driver.Navigate().Back();
                inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_01_Check_Responded_box()
        {
            try
            {
                checkbox = inc.Checkbox_Responded();
                flag = checkbox.Existed;
                if (flag)
                {
                    if (!checkbox.Checked)
                        flag = checkbox.Click();
                }
                else error = "Cannot get checkbox responded.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_025_02_Save_Incident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_01_Verify_SLAs_P4_26()
        {
            try
            {
                //Response = Completed
                //Resolutio = In progress
                string slas = Base.GData("Verify_SLAs_26");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid SLA.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_02_Open_SLA_P4_Response()
        {
            try
            {
                string sla = Base.GData("SLA_Response_P4");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA=" + sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_03_Validate_SLA_Stop_time()
        {
            try
            {
                Step_021_Verify_Task_SLA_StopTime();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_026_04_Back_to_Incident()
        {
            try
            {
                Step_022_Back_To_Incident();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_027_Change_State_to_Awaiting_Customer()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Customer");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_01_Populate_Follow_up_date()
        {
            try
            {
                button = inc.Button_FollowUpDate_Calendar();
                flag = button.Existed;
                if (flag)
                {
                    flag = button.Click();
                    if (flag)
                    {
                        button = inc.Button_Calendar_Select();
                        flag = button.Existed;
                        if (flag)
                        {
                            flag = button.Click();
                            if (flag)
                            {
                                datetime = inc.Datetime_Followupdate();
                                flag = datetime.Existed;
                                if (flag)
                                {
                                    string followup = datetime.Text.Trim();
                                    followup = Convert.ToDateTime(followup).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                                    flag = datetime.SetText(followup, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_028_02_Save_Incident()
        {
            try
            {
                Step_011_Save_Incident();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_029_Verify_SLA_29()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_29");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_030_Change_State_to_Active()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Active");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_031_Save_Incident()
        {
            try
            {
                Step_011_Save_Incident();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_032_Verify_SLA_32()
        {
            try
            {
                Step_026_01_Verify_SLAs_P4_26();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_033_Change_State_to_Awaiting_Evidence()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Awaiting Evidence");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_034_Save_Incident()
        {
            try
            {
                Step_011_Save_Incident();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_035_Verify_SLA_35()
        {
            try
            {
                //Resolution SLA is PAUSED
                Step_029_Verify_SLA_29();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_036_Change_State_to_Active()
        {
            try
            {
                Step_030_Change_State_to_Active();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_037_Save_Incident()
        {
            try
            {
                Step_011_Save_Incident();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_038_Verify_SLA_38()
        {
            try
            {
                //Resolution SLA is IN PROGRESS
                Step_026_01_Verify_SLAs_P4_26();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_039_Change_State_to_Resolved()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Resolved");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox category.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_040_Populate_Close_Code_and_Close_Notes()
        {
            try
            {
                flag = inc.Select_Tab("Closure Information");
                if (flag)
                {
                    combobox = inc.Combobox_CloseCode();
                    flag = combobox.Existed;
                    if (flag)
                    {
                        string temp = "Solved (Work Around)";
                        flag = combobox.SelectItem(temp);
                        if (flag)
                        {
                            textarea = inc.Textarea_CloseNotes();
                            flag = textarea.Existed;
                            if (flag)
                            {
                                temp = "Test Close Note";
                                flag = textarea.SetText(temp);
                                if (!flag) error = "Cannot populate close notes.";
                            }
                            else error = "Cannot get textarea close notes.";
                        }
                        else error = "Cannot populate close code value.";
                    }
                    else error = "Cannot get combobox close code.";
                }
                else error = "Cannot select tab Closure Information.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_041_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_042_Verify_SLA_42()
        {
            try
            {
                //Resolution SLA is PAUSED
                Step_029_Verify_SLA_29();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_043_ImpersonateUser_Inc_Manager()
        {
            try
            {
                string temp = Base.GData("UserFullName");
                flag = home.ImpersonateUser(temp);
                if (!flag) { error = "Cannot impersonate user (" + temp + ")"; }
                else home.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_044_Search_and_Open_Incident()
        {
            try
            {
                string temp = Base.GData("Debug").ToLower();
                if (temp == "yes" && (incidentId1 == null || incidentId1 == string.Empty))
                {
                    Auto.AddParameter addPara = new Auto.AddParameter("Please input incident Id.");
                    addPara.ShowDialog();
                    incidentId1 = addPara.value;
                    addPara.Close();
                    addPara = null;
                }
                //-----------------------------------------------------------------------

                flag = home.LeftMenuItemSelect("Incident", "Open");
                if (flag)
                {
                    inclist.WaitLoading();
                    temp = inclist.List_Title().MyText.ToLower();
                    flag = temp.Equals("incidents");
                    if (flag)
                    {
                        flag = inclist.SearchAndOpen("Number", incidentId1, "Number=" + incidentId1, "Number");
                        if (!flag) error = "Error when search and open incident (id:" + incidentId1 + ")";
                        else inc.WaitLoading();
                    }
                    else
                    {
                        error = "Invalid title of page. Runtime:(" + temp + "). Expected:(Incidents)";
                    }
                }
                else error = "Error when select open incident.";
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_045_Change_Incident_State_To_Closed()
        {
            try
            {
                combobox = inc.Combobox_State();
                flag = combobox.Existed;
                if (flag)
                {
                    flag = combobox.SelectItem("Closed");
                    if (flag)
                    {
                        inc.WaitLoading();
                    }
                    else { error = "Cannot populate state value."; }
                }
                else
                {
                    error = "Cannot get combobox state.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_046_SaveIncident()
        {
            try
            {
                flag = inc.Save();
                if (!flag) { error = "Error when save incident."; }
                else inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_01_Verify_SLAs_47()
        {
            try
            {
                Step_012_01_Expose_columns_of_related_list_SLA();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_047_02_Verify_SLAs_47()
        {
            try
            {
                string slas = Base.GData("Verify_SLAs_47");
                flag = inc.VerifySLAs(slas);
                if (!flag)
                {
                    flagExit = false;
                    error = "Invalid slas.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_048_Open_SLA_Resolution_P4()
        {
            try
            {
                string sla = Base.GData("SLA_Resolution_P4");
                flag = inc.RelatedTable_Open_Information_Record("Task SLAs", "SLA=" + sla);
                if (flag)
                    inc.WaitLoading();
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_049_Verify_Task_SLA_StopTime()
        {
            try
            {
                datetime = inc.Datetime_StopTime();
                if (datetime.Text.Trim() == "")
                {
                    flag = false;
                    error = "Stop time is not auto populate.";
                }
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------
        [Test]
        public void Step_050_Logout()
        {
            try
            {
                string temp = Base.GData("Url");
                Base.ClearCache();
                Thread.Sleep(2000);
                Base.Driver.Navigate().GoToUrl(temp);
                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                flag = false;
                error = ex.Message;
            }
        }
        #endregion Scenario of test case (NEED TO UPDATE)
    }
}